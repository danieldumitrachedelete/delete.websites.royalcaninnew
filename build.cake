#addin "Cake.Yarn&version=0.4.6"
#addin "Cake.Incubator&version=5.0.1"
#addin "Cake.XdtTransform&version=0.17.0"
#addin "Cake.Watch&version=0.2.3"
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

var target = Argument("Target", "Default");
var configuration = Argument("Configuration", "Local");
var role = Argument("Role", "Standalone");
var instance = Argument("Instance", "");
var pathToDeploy = "../Sitecore/website";

const string solution = "./Delete.Websites.RoyalCanin.sln";

pathToDeploy = pathToDeploy.EndsWith("/") ? pathToDeploy : pathToDeploy + "/";

void CopyFast(string source, string destination)
{
    Information(source);
    var returnCode = StartProcess("robocopy", new ProcessSettings {
        Arguments = new ProcessArgumentBuilder()
            .Append(@"/MT:128 /NFL /NDL /ETA /NJH /NJS /NP /S")
            .Append('"'+ source + '"')
            .Append('"'+ destination + '"')
        }
    );
    if (returnCode > 7) {
        throw new Exception(string.Format("Robocopy failed. Return code: {0}", returnCode));
    }
}

Task("Build")
  .Does(() =>
{
    NuGetRestore(solution);
    MSBuild(solution, 
        new MSBuildSettings {
            Verbosity = Verbosity.Minimal,
            Configuration = "Debug",
            PlatformTarget = PlatformTarget.MSIL,
            MaxCpuCount = 8,
            NodeReuse = true
        }
        .WithRestore()
        .WithTarget("Build"));
});

Task("BuildAssets")
    .Does(() => 
    {
        Yarn.FromPath("./src").Install();
        Yarn.FromPath("./src").RunScript("prod");
    });

Action<DirectoryPath> CopyProjectFiles = (projectDir) => 
{
    Information(projectDir);
    var folders = new [] {"App_Config", "Views", "layouts", "dist", "bin", "jscript", "layouts", "sitecore", "sitecore modules"};
    Parallel.ForEach(folders, (folder) => 
    {
        var sourceDir = projectDir.Combine(folder);
        var destDir = DirectoryPath.FromString("./obj/website").Combine(folder);
        if (DirectoryExists(sourceDir))
        {
            EnsureDirectoryExists(destDir);
            //CopyDirectory(sourceDir, destDir);
            CopyFast(sourceDir.ToString(), destDir.ToString());
        }
    });
};



Task("ClearPackage")
    .Does(() =>
    {
        CleanDirectories("./obj/website");
    });

Task("Package")    
    .IsDependentOn("ClearPackage")
    .IsDependentOn("UnicornPackage")
    .Does(() =>
    {
        var parsedSolution = ParseSolution(solution);
        foreach(var project in parsedSolution.Projects
            .Where(project => project.Path.ToString().EndsWith(".csproj"))
            .Where(project => project.Path.ToString().Contains("/src/"))
            .Where(project => project.Path.ToString().Contains("/code/")))
        {
            CopyProjectFiles(project.Path.GetDirectory());
            CopyAspxHandlers(project.Path.GetDirectory());
        }
        
		CopyFast(DirectoryPath.FromString("./src/dist").ToString(), DirectoryPath.FromString("./obj/website/dist").ToString());
        EnsureDirectoryExists("./obj/website/images");
    });

Action<DirectoryPath> CopyAspxHandlers = (projectDir) => 
{
    var handlers = GetFiles(projectDir.ToString() + "/*.aspx");

    foreach(var handler in handlers)
    {
        var fileName = handler.GetFilename().FullPath.ToString();
        var destDir = FilePath.FromString("./obj/website" + "/" + fileName);
        CopyFile(handler.ToString(), destDir.ToString());
    }        
};

Action<DirectoryPath> CopyUnicornProject = (dir) =>
    {
        var relative = DirectoryPath.FromString("./src").MakeAbsolute(Context.Environment).GetRelativePath(dir);
        var output = DirectoryPath.FromString("./obj/website/unicorn/").Combine(relative);
        EnsureDirectoryExists(output);
        CopyDirectory(dir, output);
        Information(relative);
    };

Task("UnicornPackage")
    .Does(() => {
        CleanDirectories("./obj/website/unicorn");
        EnsureDirectoryExists("./obj/website/unicorn");
        EnsureDirectoryExists("./obj/website/unicorn/Project");
        EnsureDirectoryExists("./obj/website/unicorn/Feature");
        EnsureDirectoryExists("./obj/website/unicorn/Foundation");
    })
    .DoesForEach(GetDirectories("./src/Project/**/serialization"), CopyUnicornProject)
    .DoesForEach(GetDirectories("./src/Feature/**/serialization"), CopyUnicornProject)
    .DoesForEach(GetDirectories("./src/Foundation/**/serialization"), CopyUnicornProject)
    ;

Task("ConfigPackage")
    .Does(() => {
        EnsureDirectoryExists("./obj/config");
        CleanDirectories("./obj/config");
        CopyFiles("./configuration/website/**/*.config", "./obj/config", true);
    })
    .DoesForEach(GetFiles("./obj/config/**/*.config"), file => {
        Information(file);
        var relative = DirectoryPath.FromString("./obj/config").MakeAbsolute(Context.Environment).GetRelativePath(file);
        var transformDir = DirectoryPath.FromString("./configuration/website").MakeAbsolute(Context.Environment);

        var transforms = new [] { 
            configuration,
            role,
            configuration + "." + role
        }
        .Select(transform => transformDir.CombineWithFilePath(relative + "." + transform + ".transform"))
        .Where(FileExists) ;

        foreach (var transformFile in transforms) {
            Information("Transformation: " + transformFile);
            XdtTransformConfig(file, transformFile, file);
        }
    });

Task("PackageHtml")
    .Does(() => {
        CopyFiles("./src/Project/RoyalCanin/code/*.html", "./obj/website", true);
    });


Task("PublishPackageToLocal")
    .Does(() => 
    {
        CopyFast("./obj/website", pathToDeploy);
        CopyFast("./obj/config", pathToDeploy);
    });

Task("BuildPublishLocal")
    .IsDependentOn("Build")
    .IsDependentOn("Package")
    .IsDependentOn("ConfigPackage")
    .IsDependentOn("PublishPackageToLocal");

Task("PublishLocal")
    .IsDependentOn("Package")
    .IsDependentOn("ConfigPackage")
    .IsDependentOn("PackageHtml")
    .IsDependentOn("PublishPackageToLocal");


Task("Default")
    .IsDependentOn("Build")
    .IsDependentOn("BuildAssets")
    .IsDependentOn("Package")
    .IsDependentOn("UnicornPackage")
    .IsDependentOn("ConfigPackage")
    .IsDependentOn("PackageHtml")
    .Does(() => {});

Task("Watch")
    .Does(() => 
    {
        var destRoot = pathToDeploy;

        Watch(
            new WatchSettings 
            {
                Recursive = true,
                Path = "./src",
                Pattern = "*.cshtml"
            }, 
            (changes) => 
            {
                changes.ToList().ForEach(change => 
                {
                    if (change.FullPath.EndsWith(".TMP")) return;
                    if (!FileExists(change.FullPath)) return;
                    var dest = Regex.Replace(change.FullPath.ToString(), @".*\\code\\", destRoot);
                    CopyFile(change.FullPath, dest);
                    Information(DateTime.Now + " " + dest);
                });
            });
    });

RunTarget(target);
