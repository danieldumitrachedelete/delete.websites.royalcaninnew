# Prerequisites
- Visual Studio 2017
- PowerShell v3 or later
- Visual Studio Powershell Tools
- MS-Build PS module for build script (https://www.powershellgallery.com/packages/Invoke-MsBuild/2.6.0)
- .Net Framework 4.6.2

# Initial setup (required only for the first project)
1. Install latest version of Power Shell Tools for VS using Extension Manager. Make sure that Scripts.pssproj loaded correctly within the VS after Tool installation.

2. Use $profile variable in the Package Manager Console to define the location of the profile. 
	Edit (if paths are different) and copy d:\Websites\RoyalCanin\Latest\configuration\scripts\NuGet_profile.ps1 to this folder. 
	Reload VS after modification with ADMIN rights.

3. 	Use "Install-Module -Name Invoke-MsBuild" to install module for deploy.

4. 	Go to Tools => NuGet Package Manager => Package Manager Settings => Package Sources
	Add "https://sitecore.myget.org/F/sc-packages/api/v3/index.json" as Sitecore Repository to Package Sources.

# Individual project setup

1. Modify configs:

```	
    Add-helix-module-configuration.json
		featureNamespacePrefix
		projectNamespacePrefix
		foundationNamespacePrefix
	Publishsettings.targets
        	<publishUrl>YOUR_PATH_HERE</publishUrl>

	Build-publish-configuration.json
		"slnName": "YOUR_SLN_NAME_HERE.sln",
		filesToExclude. Here I have[ ".Test.csproj", ".ModuleName.csproj", ".ProjectName.csproj", ".FeatureName.csproj", "FoundationName.csproj", ".ModuleName.Tests.csproj", "Scripts.pssproj" ].
```
2. Add sourceFolder variable to Sitecore.config with full path to Helix project. Required for Unicorn serialization.
<sc.variable name="sourceFolder" value="D:\Websites\RandA\Helix\src" />

3. Update files with connection strings:
	For UI developer (Shared Database & SOLR Instance): 
		src\Foundation\DeleteFoundationCore\code\App_Config\ConnectionStrings.config.template
		src\Foundation\DeleteFoundationCore\code\App_Config\Sitecore\ContentSearch\Sitecore.ContentSearch.Solr.DefaultIndexConfiguration.config.template
		
	For Dev create and update file (Individual Database & SOLR Instance): 
		src\Foundation\DeleteFoundationCore\code\App_Config\ConnectionStrings.config.user
		src\Foundation\DeleteFoundationCore\code\App_Config\Sitecore\ContentSearch\Sitecore.ContentSearch.Solr.DefaultIndexConfiguration.config.user

4. Run auto-install sitecore packages (if individual databases or the first time):
		- Go to \src\Foundation\DeleteFoundationCore\code\PackageInstaller:
			Ammend PackageInstallerConfig.json:
				- Replace Packages.Path variable with actual full path to packages.
				All zipped packages are assumed to be part of Web project and located in the configuration folder.
				Example of path: "D:\\Websites\\RoyalCanin\\Latest\\configuration\\packages\\"
				Additional packages can be added for installation to the InstallPackages task.
				
		- Deploy project to the Sitecore
		- Run Powershell ISE as !Administrator!
		- Change active directory to PackageInstaller folder in your Sitecore instance:
			Example: cd D:\Websites\RoyalCanin\Sitecore\website\PackageInstaller
		- Run command to install packages:
			Replace parameters:
				D:\Websites\RoyalCanin\Sitecore\website => Path to your Sitecore instance
				royalcanin.deletedev.com => Name of your website
			Then execute the command:
			Install-SitecoreConfiguration -Path .\PackageInstallerConfig.json -InstallDirectory D:\Websites\RoyalCanin\Sitecore\website -SiteName royalcanin.deletedev.com
			
		Packages will be installed automatically and all temp files fill be removed after the installation automatically.
		
5.OPTIONAL:
Go to Visual studio and open Package Manager Console.
Use "Run-Watcher" command to run watcher which automatically sync changes in cshtml and config files of your project and Sitecore instance.
Log file with the list of all updated files will be saved to D:\WatcherTest.txt

