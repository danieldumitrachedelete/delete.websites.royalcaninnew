﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Bazaarvoice.Models.Bazaarvoice;
using Newtonsoft.Json;

namespace Bazaarvoice
{
    class Program
    {
        static void Main(string[] args)
        {
            var example = string.Empty;

            //using (var sr = new StreamReader(@"..\..\example.json"))
            //{
            //    example = sr.ReadToEnd();
            //}

            var test = GetAllProductStatistics();

            var test2 = test.Results.ToDictionary(product => product.Id,
                product => product.ReviewStatistics.AverageOverallRating)["60434"];

            var reviews = GetProductReviews("2588");
        }

        public static ProductsBazaarvoiceResponse GetAllProductStatistics()
        {
            var requestBaseUrl =
                @"https://api.bazaarvoice.com/data/products.json?ApiVersion=5.4&Passkey=ca6df8b3a801c111e6a7300eff10819c09&Stats=Reviews&Limit={0}&Offset={1}";

            long limit = 100;
            long offset = 0;
            long totalResults = long.MaxValue;
            ProductsBazaarvoiceResponse allProducts = null;

            while (offset < totalResults)
            {
                var request = WebRequest.Create(String.Format(requestBaseUrl, limit, offset));
                var responseStream = request.GetResponse().GetResponseStream();
                string responseText;
                using (var sr = new StreamReader(responseStream))
                {
                    responseText = sr.ReadToEnd();
                }

                var pageOfProducts = JsonConvert.DeserializeObject<ProductsBazaarvoiceResponse>(responseText);

                if (allProducts == default(ProductsBazaarvoiceResponse))
                {
                    allProducts = pageOfProducts;
                    allProducts.Offset += pageOfProducts.Limit;
                }
                else
                {
                    allProducts.Merge(pageOfProducts);
                }

                limit = allProducts.Limit ?? 100;
                offset = allProducts.Offset ?? 0;
                totalResults = allProducts.TotalResults ?? long.MaxValue;

                if (allProducts.HasErrors.HasValue && allProducts.HasErrors.Value) break;
            }

            return allProducts;
        }

        public static ReviewsBazaarvoiceResponse GetProductReviews(string productId)
        {
            var requestBaseUrl =
                @"https://api.bazaarvoice.com/data/reviews.json?ApiVersion=5.4&Passkey=ca6df8b3a801c111e6a7300eff10819c09&Filter=ProductID:{0}&Limit={1}&Offset={2}";

            long limit = 100;
            long offset = 0;
            long totalResults = long.MaxValue;
            ReviewsBazaarvoiceResponse allReviews = null;

            while (offset < totalResults)
            {
                var request = WebRequest.Create(String.Format(requestBaseUrl, productId, limit, offset));
                var responseStream = request.GetResponse().GetResponseStream();
                string responseText;
                using (var sr = new StreamReader(responseStream))
                {
                    responseText = sr.ReadToEnd();
                }

                var pageOfReviews = JsonConvert.DeserializeObject<ReviewsBazaarvoiceResponse>(responseText);

                if (allReviews == default(ReviewsBazaarvoiceResponse))
                {
                    allReviews = pageOfReviews;
                    allReviews.Offset += pageOfReviews.Limit;
                }
                else
                {
                    allReviews.Merge(pageOfReviews);
                }

                limit = allReviews.Limit ?? 100;
                offset = allReviews.Offset ?? 0;
                totalResults = allReviews.TotalResults ?? long.MaxValue;

                if (allReviews.HasErrors.HasValue && allReviews.HasErrors.Value) break;
            }

            return allReviews;
        }
    }
}
