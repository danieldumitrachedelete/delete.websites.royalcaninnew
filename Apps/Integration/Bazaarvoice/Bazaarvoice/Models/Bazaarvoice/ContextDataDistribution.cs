﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Bazaarvoice.Models.Bazaarvoice
{
    public partial class ContextDataDistribution
    {
        [JsonProperty("Id", NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        [JsonProperty("Values", NullValueHandling = NullValueHandling.Ignore)]
        public List<ValueDistribution> ValueDistributions { get; set; }

        [JsonProperty("Label", NullValueHandling = NullValueHandling.Ignore)]
        public string Label { get; set; }
    }
}