﻿using Newtonsoft.Json;

namespace Bazaarvoice.Models.Bazaarvoice
{
    public partial class Video
    {
        [JsonProperty("VideoId", NullValueHandling = NullValueHandling.Ignore)]
        public string VideoId { get; set; }

        [JsonProperty("VideoHost", NullValueHandling = NullValueHandling.Ignore)]
        public string VideoHost { get; set; }

        [JsonProperty("VideoThumbnailUrl")]
        public string VideoThumbnailUrl { get; set; }

        [JsonProperty("VideoIframeUrl")]
        public string VideoIframeUrl { get; set; }

        [JsonProperty("Caption")]
        public string Caption { get; set; }

        [JsonProperty("VideoUrl", NullValueHandling = NullValueHandling.Ignore)]
        public string VideoUrl { get; set; }
    }
}