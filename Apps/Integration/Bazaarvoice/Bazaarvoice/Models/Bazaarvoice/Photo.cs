﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Bazaarvoice.Models.Bazaarvoice
{
    public partial class Photo
    {
        [JsonProperty("Sizes", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, Size> Sizes { get; set; }

        [JsonProperty("Id", NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        [JsonProperty("SizesOrder", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> SizesOrder { get; set; }

        [JsonProperty("Caption", NullValueHandling = NullValueHandling.Ignore)]
        public string Caption { get; set; }
    }
}