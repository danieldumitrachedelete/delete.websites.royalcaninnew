﻿using Newtonsoft.Json;

namespace Bazaarvoice.Models.Bazaarvoice
{
    public partial class Badge
    {
        [JsonProperty("Id", NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        [JsonProperty("ContentType", NullValueHandling = NullValueHandling.Ignore)]
        public string ContentType { get; set; }

        [JsonProperty("BadgeType", NullValueHandling = NullValueHandling.Ignore)]
        public string BadgeType { get; set; }
    }
}