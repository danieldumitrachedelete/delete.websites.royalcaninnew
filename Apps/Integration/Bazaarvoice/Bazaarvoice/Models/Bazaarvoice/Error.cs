﻿using Newtonsoft.Json;

namespace Bazaarvoice.Models.Bazaarvoice
{
    public partial class Error
    {
        [JsonProperty("Message", NullValueHandling = NullValueHandling.Ignore)]
        public string Message { get; set; }

        [JsonProperty("Code", NullValueHandling = NullValueHandling.Ignore)]
        public string Code { get; set; }
    }
}