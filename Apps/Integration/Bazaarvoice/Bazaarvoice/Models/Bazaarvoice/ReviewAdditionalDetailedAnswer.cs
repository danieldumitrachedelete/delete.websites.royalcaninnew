﻿using Newtonsoft.Json;

namespace Bazaarvoice.Models.Bazaarvoice
{
    public partial class ReviewAdditionalDetailedAnswer
    {
        [JsonProperty("Value", NullValueHandling = NullValueHandling.Ignore)]
        public string Value { get; set; }

        [JsonProperty("Id", NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        [JsonProperty("Label", NullValueHandling = NullValueHandling.Ignore)]
        public string Label { get; set; }
    }
}