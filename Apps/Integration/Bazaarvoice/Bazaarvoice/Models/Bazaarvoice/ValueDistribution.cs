﻿using Newtonsoft.Json;

namespace Bazaarvoice.Models.Bazaarvoice
{
    public partial class ValueDistribution
    {
        [JsonProperty("Count", NullValueHandling = NullValueHandling.Ignore)]
        public long? Count { get; set; }

        [JsonProperty("Value", NullValueHandling = NullValueHandling.Ignore)]
        public string Value { get; set; }
    }
}