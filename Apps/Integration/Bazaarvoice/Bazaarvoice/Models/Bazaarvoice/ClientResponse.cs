﻿using System;
using Newtonsoft.Json;

namespace Bazaarvoice.Models.Bazaarvoice
{
    public partial class ClientResponse
    {
        [JsonProperty("Department", NullValueHandling = NullValueHandling.Ignore)]
        public string Department { get; set; }

        [JsonProperty("Response", NullValueHandling = NullValueHandling.Ignore)]
        public string Response { get; set; }

        //[JsonProperty("ResponseType", NullValueHandling = NullValueHandling.Ignore)]
        //public string ResponseType { get; set; }

        [JsonProperty("ResponseSource", NullValueHandling = NullValueHandling.Ignore)]
        public string ResponseSource { get; set; }

        [JsonProperty("Name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        [JsonProperty("Date", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? Date { get; set; }
    }
}