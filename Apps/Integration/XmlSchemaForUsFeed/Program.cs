﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;
using XmlSchemaForUsFeed.XmlModels;

namespace XmlSchemaForUsFeed
{
    class Program
    {
        static void Main(string[] args)
        {
            var a = new ImportFeed();



            //old version with non-fixed constants
            /*
                        var product1 = new Product
                        {
                            Culture = "en-US",
                            DateLastUpdated = DateTime.Parse("2014-05-19"),
                            MainItemId = "2487",
                            Species = Species.Dog,
                            BreedKeyName = "Labrador Retriever",
                            AnimalSize = "LARGE",
                            Lifestage = new []{ "Adult" },
                            IsVetProduct = false,
                            ProductName = "Royal Canin® Breed Health Nutrition™ Labrador Retriever Adult Dry Dog Food, 5.5 lb",
                            IsActive = true,
                            ProductType = ProductType.Dry,
                            MainImage = "filename001.jpg",
                            Images = new string[]{ "filename002.jpg", "filename003.jpg", "filename004.jpg" },
                            ShortProductDescription = "ROYAL CANIN ® BREED HEALTH NUTRITION™ Labrador Retriever Adult dry dog food features a combination of nutrients to meet the unique needs of the pure breed Labrador Retreiver dog",
                            Benefits = new string[]
                            {
                                "The kibble’s texture and formula are adapted to the Labrador Retriever and its exclusive shape helps reduce the rate of food intake.",
                                "His northern ancestors needed plenty of calories before diving into the icy waters of the Atlantic Ocean, and the Labrador Retriever has inherited his legendary appetite from them. As a result, he needs a carefully controlled diet to avoid becoming overweight. This exclusive formula helps maintain the adult Labrador Retriever’s ideal weight with an appropriate calorie content.",
                                "As a determined athlete, the Labrador Retriever gives his all. His solid body, strong bones and his natural predisposition to gain weight are among the factors which can put stress on his joints. The LABRADOR RETRIEVER ADULT formula helps support healthy bones and joints and helps maintain ideal weight. Enriched with EPA and DHA.",
                                "The skin and coat reflect the dog’s state of health. An appropriate food is important in maintaining healthy skin and coat. The LABRADOR RETRIEVER ADULT formula helps support the skin’s role as a barrier with an exclusive complex of nutrients and helps maintain skin and coat health (EPA and DHA).",
                            },
                            ProductDescription = "Tailor-made Nutrition for Pure Breed Dogs. An EXCLUSIVE KIBBLEfor each breed: a concentration of scientific and nutritional expertise born from the unique knowledge of Royal Canin combined with the practical experience of Breeders.",
                            Ingredients = "Chicken by-product meal, brown rice, oat groats, brewers rice, corn gluten meal, natural flavors, chicken fat, pork meal, dried plain beet pulp, powdered cellulose, grain distillers dried yeast, wheat gluten, fish oil, vegetable oil, sodium silico aluminate, potassium chloride, calcium carbonate, psyllium seed husk, vitamins [DL-alpha tocopherol acetate (source of vitamin E), inositol, niacin supplement, L-ascorbyl-2-polyphosphate (source of vitamin C), D-calcium pantothenate, biotin, pyridoxine hydrochloride (vitamin B6), riboflavin supplement, thiamine mononitrate (vitamin B1), vitamin A acetate, folic acid, vitamin B12 supplement, vitamin D3 supplement, menadione sodium bisulfite complex], fructooligosaccharides, sodium tripolyphosphate, salt, taurine, trace minerals [zinc proteinate, ferrous sulfate, manganous oxide, sodium selenite, zinc oxide, calcium iodate, copper sulfate], choline chloride, glucosamine hydrochloride, marigold extract (Tagetes erecta L.), L-carnitine, green tea extract, chondroitin sulfate, rosemary extract, preserved with mixed tocopherols and citric acid.",
                            PackagingSizes = new List<PackagingSize>
                            {
                                new PackagingSize { Size = 5.5f, UnitName = "Pound"},
                                new PackagingSize { Size = 17f, UnitName = "Pound"},
                                new PackagingSize { Size = 30f, UnitName = "Pound"},
                            }
                        };
            */

            var product1 = new Product
            {
                Culture = "en-US",
                DateLastUpdated = DateTime.Parse("2014-05-19"),
                MainItemId = "2487",
                SKU= "030111453709",
                EAN= "030111453709",
                Species = Species.Dog,
                BreedKeyName = "labrador_retriever",
                AnimalSize = AnimalSize.maxi_26_44_kg,
                Lifestages = new[] { Lifestage.adult, Lifestage.mature },
                PillarIdCode = PillarIdCode.POS,
                ProductName = "Royal Canin® Breed Health Nutrition™ Labrador Retriever Adult Dry Dog Food, 5.5 lb",
                IsActive = true,
                ProductType = ProductType.Dry,
                Images = new []
                {
                    new Image {Filename = "Filename1.jpg", Alt = "Alt Text 1", ImageType = ImageType.Bag},
                    new Image {Filename = "Filename2.jpg", Alt = "Alt Text 2", ImageType = ImageType.Kibble},
                    new Image {Filename = "Filename3.jpg", Alt = "Alt Text 3", ImageType = ImageType.Emblematic},
                    new Image {Filename = "Filename4.jpg", Alt = "Alt Text 4", ImageType = ImageType.General}
                },
                ShortProductDescription = "ROYAL CANIN ® BREED HEALTH NUTRITION™ Labrador Retriever Adult dry dog food features a combination of nutrients to meet the unique needs of the pure breed Labrador Retreiver dog",
                Benefits = new string[]
                {
                    "The kibble’s texture and formula are adapted to the Labrador Retriever and its exclusive shape helps reduce the rate of food intake.",
                    "His northern ancestors needed plenty of calories before diving into the icy waters of the Atlantic Ocean, and the Labrador Retriever has inherited his legendary appetite from them. As a result, he needs a carefully controlled diet to avoid becoming overweight. This exclusive formula helps maintain the adult Labrador Retriever’s ideal weight with an appropriate calorie content.",
                    "As a determined athlete, the Labrador Retriever gives his all. His solid body, strong bones and his natural predisposition to gain weight are among the factors which can put stress on his joints. The LABRADOR RETRIEVER ADULT formula helps support healthy bones and joints and helps maintain ideal weight. Enriched with EPA and DHA.",
                    "The skin and coat reflect the dog’s state of health. An appropriate food is important in maintaining healthy skin and coat. The LABRADOR RETRIEVER ADULT formula helps support the skin’s role as a barrier with an exclusive complex of nutrients and helps maintain skin and coat health (EPA and DHA).",
                },
                ProductDescription = "Tailor-made Nutrition for Pure Breed Dogs. An EXCLUSIVE KIBBLEfor each breed: a concentration of scientific and nutritional expertise born from the unique knowledge of Royal Canin combined with the practical experience of Breeders.",
                Ingredients = "Chicken by-product meal, brown rice, oat groats, brewers rice, corn gluten meal, natural flavors, chicken fat, pork meal, dried plain beet pulp, powdered cellulose, grain distillers dried yeast, wheat gluten, fish oil, vegetable oil, sodium silico aluminate, potassium chloride, calcium carbonate, psyllium seed husk, vitamins [DL-alpha tocopherol acetate (source of vitamin E), inositol, niacin supplement, L-ascorbyl-2-polyphosphate (source of vitamin C), D-calcium pantothenate, biotin, pyridoxine hydrochloride (vitamin B6), riboflavin supplement, thiamine mononitrate (vitamin B1), vitamin A acetate, folic acid, vitamin B12 supplement, vitamin D3 supplement, menadione sodium bisulfite complex], fructooligosaccharides, sodium tripolyphosphate, salt, taurine, trace minerals [zinc proteinate, ferrous sulfate, manganous oxide, sodium selenite, zinc oxide, calcium iodate, copper sulfate], choline chloride, glucosamine hydrochloride, marigold extract (Tagetes erecta L.), L-carnitine, green tea extract, chondroitin sulfate, rosemary extract, preserved with mixed tocopherols and citric acid.",
                PackagingSizes = new List<PackagingSize>
                {
                    new PackagingSize { Size = 5.5f, SKU= "030111453709", UnitName = MeasureUnit.Pound},
                    new PackagingSize { Size = 17f, SKU= "030111416735", UnitName = MeasureUnit.Pound},
                    new PackagingSize { Size = 30f, SKU= "030111453778", UnitName = MeasureUnit.Pound},
                },
                LegalStatement = @"100% <HEAD></HEAD>COMPLETE AND BALANCED NUTRITION

LABRADOR RETRIEVER ADULT Breed Health Nutrition is formulated to meet the nutritional levels established by the AAFCO (Association of American Feed Control Officials) Dog Food Nutrient Profiles for maintenance."
            };

/*
            var product2 = new Product
            {
                Culture = "en-US",
                DateLastUpdated = DateTime.Parse("2017-03-14"),
                MainItemId = "2487",
                Species = Species.Dog,
                BreedKeyName = "Labrador Retriever",
                AnimalSize = "LARGE",
                Lifestage = new[] { "Adult" },
                IsVetProduct = false,
                ProductName = "Royal Canin® Breed Health Nutrition™ Labrador Retriever Adult Dry Dog Food, 17 lb",
                IsActive = true,
                ProductType = ProductType.Dry,
                MainImage = "filename001.jpg",
                Images = new string[] { "filename002.jpg", "filename003.jpg", "filename004.jpg" },
                ShortProductDescription = "ROYAL CANIN ® BREED HEALTH NUTRITION™ Labrador Retriever Adult dry dog food features a combination of nutrients to meet the unique needs of the pure breed Labrador Retreiver dog",
                Benefits = new string[]
                {
                    "The kibble’s texture and formula are adapted to the Labrador Retriever and its exclusive shape helps reduce the rate of food intake.",
                    "His northern ancestors needed plenty of calories before diving into the icy waters of the Atlantic Ocean, and the Labrador Retriever has inherited his legendary appetite from them. As a result, he needs a carefully controlled diet to avoid becoming overweight. This exclusive formula helps maintain the adult Labrador Retriever’s ideal weight with an appropriate calorie content.",
                    "As a determined athlete, the Labrador Retriever gives his all. His solid body, strong bones and his natural predisposition to gain weight are among the factors which can put stress on his joints. The LABRADOR RETRIEVER ADULT formula helps support healthy bones and joints and helps maintain ideal weight. Enriched with EPA and DHA.",
                    "The skin and coat reflect the dog’s state of health. An appropriate food is important in maintaining healthy skin and coat. The LABRADOR RETRIEVER ADULT formula helps support the skin’s role as a barrier with an exclusive complex of nutrients and helps maintain skin and coat health (EPA and DHA).",
                },
                ProductDescription = "Tailor-made Nutrition for Pure Breed Dogs. An EXCLUSIVE KIBBLEfor each breed: a concentration of scientific and nutritional expertise born from the unique knowledge of Royal Canin combined with the practical experience of Breeders.",
                Ingredients = "Chicken by-product meal, brown rice, oat groats, brewers rice, corn gluten meal, natural flavors, chicken fat, pork meal, dried plain beet pulp, powdered cellulose, grain distillers dried yeast, wheat gluten, fish oil, vegetable oil, sodium silico aluminate, potassium chloride, calcium carbonate, psyllium seed husk, vitamins [DL-alpha tocopherol acetate (source of vitamin E), inositol, niacin supplement, L-ascorbyl-2-polyphosphate (source of vitamin C), D-calcium pantothenate, biotin, pyridoxine hydrochloride (vitamin B6), riboflavin supplement, thiamine mononitrate (vitamin B1), vitamin A acetate, folic acid, vitamin B12 supplement, vitamin D3 supplement, menadione sodium bisulfite complex], fructooligosaccharides, sodium tripolyphosphate, salt, taurine, trace minerals [zinc proteinate, ferrous sulfate, manganous oxide, sodium selenite, zinc oxide, calcium iodate, copper sulfate], choline chloride, glucosamine hydrochloride, marigold extract (Tagetes erecta L.), L-carnitine, green tea extract, chondroitin sulfate, rosemary extract, preserved with mixed tocopherols and citric acid.",
                PackagingSizes = new List<PackagingSize>
                {
                    new PackagingSize { Size = 17f, UnitName = "Pound"},
                }
            };
*/
/*
            var product3 = new Product
            {
                Culture = "en-US",
                DateLastUpdated = DateTime.Parse("2016-08-08"),
                MainItemId = "2487",
                Species = Species.Dog,
                BreedKeyName = "Labrador Retriever",
                AnimalSize = "LARGE",
                Lifestage = new[] { "Adult" },
                IsVetProduct = false,
                ProductName = "Royal Canin® Breed Health Nutrition™ Labrador Retriever Adult Dry Dog Food, 30 lb",
                IsActive = true,
                ProductType = ProductType.Dry,
                MainImage = "filename001.jpg",
                Images = new string[] { "filename002.jpg", "filename003.jpg", "filename004.jpg" },
                ShortProductDescription = "ROYAL CANIN ® BREED HEALTH NUTRITION™ Labrador Retriever Adult dry dog food features a combination of nutrients to meet the unique needs of the pure breed Labrador Retreiver dog",
                Benefits = new string[]
                {
                    "The kibble’s texture and formula are adapted to the Labrador Retriever and its exclusive shape helps reduce the rate of food intake.",
                    "His northern ancestors needed plenty of calories before diving into the icy waters of the Atlantic Ocean, and the Labrador Retriever has inherited his legendary appetite from them. As a result, he needs a carefully controlled diet to avoid becoming overweight. This exclusive formula helps maintain the adult Labrador Retriever’s ideal weight with an appropriate calorie content.",
                    "As a determined athlete, the Labrador Retriever gives his all. His solid body, strong bones and his natural predisposition to gain weight are among the factors which can put stress on his joints. The LABRADOR RETRIEVER ADULT formula helps support healthy bones and joints and helps maintain ideal weight. Enriched with EPA and DHA.",
                    "The skin and coat reflect the dog’s state of health. An appropriate food is important in maintaining healthy skin and coat. The LABRADOR RETRIEVER ADULT formula helps support the skin’s role as a barrier with an exclusive complex of nutrients and helps maintain skin and coat health (EPA and DHA).",
                },
                ProductDescription = "Tailor-made Nutrition for Pure Breed Dogs. An EXCLUSIVE KIBBLEfor each breed: a concentration of scientific and nutritional expertise born from the unique knowledge of Royal Canin combined with the practical experience of Breeders.",
                Ingredients = "Chicken by-product meal, brown rice, oat groats, brewers rice, corn gluten meal, natural flavors, chicken fat, pork meal, dried plain beet pulp, powdered cellulose, grain distillers dried yeast, wheat gluten, fish oil, vegetable oil, sodium silico aluminate, potassium chloride, calcium carbonate, psyllium seed husk, vitamins [DL-alpha tocopherol acetate (source of vitamin E), inositol, niacin supplement, L-ascorbyl-2-polyphosphate (source of vitamin C), D-calcium pantothenate, biotin, pyridoxine hydrochloride (vitamin B6), riboflavin supplement, thiamine mononitrate (vitamin B1), vitamin A acetate, folic acid, vitamin B12 supplement, vitamin D3 supplement, menadione sodium bisulfite complex], fructooligosaccharides, sodium tripolyphosphate, salt, taurine, trace minerals [zinc proteinate, ferrous sulfate, manganous oxide, sodium selenite, zinc oxide, calcium iodate, copper sulfate], choline chloride, glucosamine hydrochloride, marigold extract (Tagetes erecta L.), L-carnitine, green tea extract, chondroitin sulfate, rosemary extract, preserved with mixed tocopherols and citric acid.",
                PackagingSizes = new List<PackagingSize>
                {
                    new PackagingSize { Size = 30f, UnitName = "Pound"},
                }
            };
*/


            var b = new List<Product>();
            b.Add(product1);
            /*b.Add(product1);*/
            a.Products = b.ToArray();
            /*a.Products.Add(product2);
            a.Products.Add(product3);*/

            var json = JsonConvert.SerializeObject(a);

            XmlSerializer serializer =
                new XmlSerializer(typeof(ImportFeed));
            TextWriter writer = new StringWriter();

            serializer.Serialize(writer, a);

            var w = new StreamWriter(@".\output.xml");
            w.WriteLine(writer.ToString());
            w.Close();

            Console.WriteLine(writer.ToString());
            Console.WriteLine(writer);

            /*var serializer = new XmlSerializer(typeof(ImportFeed));
            byte[] utf8;
            using (var memStm = new MemoryStream())
            using (var xw = XmlWriter.Create(memStm))
            {
                serializer.Serialize(xw, a);
                utf8 = memStm.ToArray();
            }

            var output = System.Text.Encoding.UTF8.GetString(utf8);

            Console.WriteLine(output);
            var w = new StreamWriter(@".\output.xml");
            w.WriteLine(output);
            w.Close();*/
        }
    }
}
