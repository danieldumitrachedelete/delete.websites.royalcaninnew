﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace XmlSchemaForUsFeed.XmlModels
{

    [XmlRoot("ImportFeed", IsNullable = false)]
    public class ImportFeed
    {
        [XmlArray("Products")]
        [XmlArrayItem("Product")]
        public Product[] Products { get; set; }
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum Species
    {
        Cat, Dog
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum ProductType
    {
        Wet, Dry, Treat
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum Lifestage
    {
        puppy,
        kitten,
        adult,
        mature
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum MeasureUnit
    {
        Pound,
        Ounce,
        Kilogram,
        Gram
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum PillarIdCode
    {
        POS,
        VET,
        PRO
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum AnimalSize
    {
        xsmall,
        mini_1_10_kg,
        medium_11_25_kg,
        maxi_26_44_kg,
        giant_from_45_kgmature
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum ImageType
    {
        Bag, Kibble, Emblematic, General
    }

    public class Product
    {
        public string Culture { get; set; }
        public DateTime DateLastUpdated { get; set; }
        public string MainItemId { get; set; }
        public string SKU { get; set; }
        public string EAN { get; set; }
        public Species Species { get; set; }
        public string BreedKeyName { get; set; }
        public AnimalSize AnimalSize { get; set; }

        [XmlArray("Lifestages")]
        [XmlArrayItem("Lifestage")]
        public Lifestage[] Lifestages { get; set; }
        public PillarIdCode PillarIdCode { get; set; }
        public ProductType ProductType { get; set; }
        public string ProductName { get; set; }
        public bool IsActive { get; set; }
        public Image[] Images { get; set; }

        [XmlArray("Benefits")]
        [XmlArrayItem("Benefit")]
        public string[] Benefits { get; set; }
        public string ProductDescription { get; set; }
        public string Ingredients { get; set; }
        public string ShortProductDescription { get; set; }
        public List<PackagingSize> PackagingSizes { get; set; }

        public string LegalStatement { get; set; }
    }

    public class PackagingSize
    {
        public float Size { get; set; }
        public string SKU { get; set; }
        public MeasureUnit UnitName { get; set; }
    }

    public class Image
    {
        public string Filename { get; set; }
        public string Alt { get; set; }
        public ImageType ImageType { get; set; }
    }
}