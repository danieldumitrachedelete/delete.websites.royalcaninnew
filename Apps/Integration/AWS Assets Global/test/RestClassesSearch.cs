﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Search
{

    public class Shards
    {

        [JsonProperty("total")] public int total { get; set; }

        [JsonProperty("successful")] public int successful { get; set; }

        [JsonProperty("skipped")] public int skipped { get; set; }

        [JsonProperty("failed")] public int failed { get; set; }
    }

    public class Keywords
    {

        [JsonProperty("Category first level")] public string Categoryfirstlevel { get; set; }

        [JsonProperty("country")] public string country { get; set; }

        [JsonProperty("Brand")] public string Brand { get; set; }

        [JsonProperty("Breed")] public string Breed { get; set; }

        [JsonProperty("Lifestage")] public string Lifestage { get; set; }

        [JsonProperty("Environment")] public string Environment { get; set; }

        [JsonProperty("Category second level")] public string Categorysecondlevel { get; set; }

        [JsonProperty("Category third level")] public string Categorythirdlevel { get; set; }

        [JsonProperty("Animal")] public string Animal { get; set; }

        [JsonProperty("type")] public string type { get; set; }
    }

    public class MainInformation
    {

        [JsonProperty("Highlighted")] public string Highlighted { get; set; }

        [JsonProperty("Associated Category")] public string AssociatedCategory { get; set; }

        [JsonProperty("Is public")] public string Ispublic { get; set; }
    }

    public class AssociatedBreeds
    {

        [JsonProperty("Associated Breeds")] public string _AssociatedBreeds { get; set; }
    }

    public class General
    {

        [JsonProperty("Regulatory Zone")] public string RegulatoryZone { get; set; }

        [JsonProperty("Supplier Type")] public string SupplierType { get; set; }
    }

    public class AssociatedProducts
    {

        [JsonProperty("Associated Product")] public string AssociatedProduct { get; set; }
    }

    public class TechnicalInformation
    {

        [JsonProperty("File / File size")] public string Filesize { get; set; }

        [JsonProperty("File / Resolution")] public string FileResolution { get; set; }

        [JsonProperty("File / Color space")] public string FileColorspace { get; set; }

        [JsonProperty("File / Image size")] public string FileImagesize { get; set; }

        [JsonProperty("File / MIME type")] public string FileMIMEtype { get; set; }
    }

    public class Hidden
    {

        [JsonProperty("Category first level")] public string Categoryfirstlevel { get; set; }

        [JsonProperty("Brand")] public string Brand { get; set; }

        [JsonProperty("Breed")] public string Breed { get; set; }

        [JsonProperty("Pillar")] public string Pillar { get; set; }

        //[JsonProperty("Product ID")] public IList<string> Product_ID { get; set; }

        //[JsonProperty("Product Name")] public IList<string> Product_Name { get; set; }

        [JsonProperty("Lifestage")] public string Lifestage { get; set; }

        [JsonProperty("Category second level")] public string Categorysecondlevel { get; set; }

        [JsonProperty("Category third level")] public string Categorythirdlevel { get; set; }

        [JsonProperty("Animal")] public string Animal { get; set; }

        [JsonProperty("Size")] public string Size { get; set; }
    }

    public class PhotoInformation
    {

        [JsonProperty("Environment")] public string Environment { get; set; }

        [JsonProperty("Emblematic")] public string Emblematic { get; set; }
    }

    public class Metadata
    {

        [JsonProperty("Main Information")] public MainInformation MainInformation { get; set; }

        [JsonProperty("Associated Breeds")] public AssociatedBreeds AssociatedBreeds { get; set; }

        [JsonProperty("General")] public General General { get; set; }

        [JsonProperty("Associated Products")] public AssociatedProducts AssociatedProducts { get; set; }

        [JsonProperty("Technical Information")]
        public TechnicalInformation TechnicalInformation { get; set; }

        [JsonProperty("Hidden")] public Hidden Hidden { get; set; }

        [JsonProperty("Photo Information")] public PhotoInformation PhotoInformation { get; set; }
    }

    public class StorageItems
    {

        [JsonProperty("preview_1")] public string preview_1 { get; set; }

        [JsonProperty("thumbnail_1")] public string thumbnail_1 { get; set; }

        [JsonProperty("master")] public string master { get; set; }

        [JsonProperty("rc-jpg-jpg-2000x1320-72-RGB")]
        public string rcjpgjpg2000x132072RGB { get; set; }

        [JsonProperty("rc-jpg-jpg-2000x1320-150-RGB")]
        public string rcjpgjpg2000x1320150RGB { get; set; }
    }

    public class Thumbnail
    {
        [JsonProperty("mime_type")] public string mime_type { get; set; }

        [JsonProperty("base64")] public string base64 { get; set; }
    }

    public class Source
    {

        [JsonProperty("name")] public string name { get; set; }
        [JsonProperty("author")] public string author { get; set; }
        [JsonProperty("public_url")] public string public_url { get; set; }
        [JsonProperty("status")] public string status { get; set; }

        [JsonProperty("country")] public string country { get; set; }

        [JsonProperty("visibility")] public string visibility { get; set; }

        [JsonProperty("thumbnail")] public Thumbnail thumbnail { get; set; }

        [JsonProperty("keywords")] public Keywords keywords { get; set; }

        [JsonProperty("metadata")] public Metadata metadata { get; set; }

        [JsonProperty("type")] public string type { get; set; }

        [JsonProperty("id")] public long id { get; set; }

        [JsonProperty("storage_items_keys")] public IList<string> storage_items_keys { get; set; }

        [JsonProperty("last_indexed_date")] public DateTime last_indexed_date { get; set; }
        [JsonProperty("updated")] public DateTime updated { get; set; }
    }

    public class Hit
    {

        [JsonProperty("_index")] public string _index { get; set; }

        [JsonProperty("_type")] public string _type { get; set; }

        [JsonProperty("_id")] public string _id { get; set; }

        [JsonProperty("_score")] public double _score { get; set; }

        [JsonProperty("_source")] public Source _source { get; set; }
    }

    public class Hits
    {

        [JsonProperty("total")] public int total { get; set; }

        [JsonProperty("max_score")] public double? max_score { get; set; }

        [JsonProperty("hits")] public IList<Hit> hits { get; set; }
    }

    public class Example
    {

        [JsonProperty("took")] public int took { get; set; }

        [JsonProperty("timed_out")] public bool timed_out { get; set; }

        [JsonProperty("_shards")] public Shards _shards { get; set; }

        [JsonProperty("hits")] public Hits hits { get; set; }
    }
}