﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Mapping
{

    public class Mapping
    {

        [JsonProperty("type")] public string type { get; set; }
    }

    public class Keywords
    {

        [JsonProperty("path_match")] public string path_match { get; set; }

        [JsonProperty("match_mapping_type")] public string match_mapping_type { get; set; }

        [JsonProperty("mapping")] public Mapping mapping { get; set; }
    }

    public class NotIndexed
    {

        [JsonProperty("path_match")] public string path_match { get; set; }

        [JsonProperty("match_mapping_type")] public string match_mapping_type { get; set; }

        [JsonProperty("mapping")] public Mapping mapping { get; set; }
    }

    public class Texts
    {

        [JsonProperty("path_unmatch")] public string path_unmatch { get; set; }

        [JsonProperty("match_mapping_type")] public string match_mapping_type { get; set; }

        [JsonProperty("mapping")] public Mapping mapping { get; set; }
    }

    public class Thumbnail
    {

        [JsonProperty("match")] public string match { get; set; }

        [JsonProperty("mapping")] public Mapping mapping { get; set; }
    }

    public class DynamicTemplate
    {

        [JsonProperty("keywords")] public Keywords keywords { get; set; }

        [JsonProperty("not_indexed")] public NotIndexed not_indexed { get; set; }

        [JsonProperty("texts")] public Texts texts { get; set; }

        [JsonProperty("thumbnail")] public Thumbnail thumbnail { get; set; }
    }

    public class Country
    {

        [JsonProperty("type")] public string type { get; set; }

        [JsonProperty("analyzer")] public string analyzer { get; set; }
    }

    public class EmbeddedThumbnail
    {

        [JsonProperty("type")] public string type { get; set; }

        [JsonProperty("analyzer")] public string analyzer { get; set; }
    }

    public class Id
    {

        [JsonProperty("type")] public string type { get; set; }
    }

    public partial class AssociatedBreeds
    {

        [JsonProperty("type")] public string type { get; set; }

        [JsonProperty("analyzer")] public string analyzer { get; set; }
    }

    public partial class AssociatedBreeds
    {

        [JsonProperty("properties")] public Properties properties { get; set; }
    }

    public class AssociatedProducts
    {

        [JsonProperty("properties")] public Properties properties { get; set; }
    }

    public class General
    {

        [JsonProperty("properties")] public Properties properties { get; set; }
    }

    public class Hidden
    {

        [JsonProperty("properties")] public Properties properties { get; set; }
    }

    public class MainInformation
    {

        [JsonProperty("properties")] public Properties properties { get; set; }
    }

    public class PhotoInformation
    {

        [JsonProperty("properties")] public Properties properties { get; set; }
    }

    public class TechnicalInformation
    {

        [JsonProperty("properties")] public Properties properties { get; set; }
    }

    public partial class Properties
    {

        [JsonProperty("Associated Breeds")] public AssociatedBreeds AssociatedBreeds { get; set; }

        [JsonProperty("Associated Products")] public AssociatedProducts AssociatedProducts { get; set; }

        [JsonProperty("General")] public General General { get; set; }

        [JsonProperty("Hidden")] public Hidden Hidden { get; set; }

        [JsonProperty("Main Information")] public MainInformation MainInformation { get; set; }

        [JsonProperty("Photo Information")] public PhotoInformation PhotoInformation { get; set; }

        [JsonProperty("Technical Information")]
        public TechnicalInformation TechnicalInformation { get; set; }
    }

    public class Metadata
    {

        [JsonProperty("properties")] public Properties properties { get; set; }
    }

    public class Name
    {

        [JsonProperty("type")] public string type { get; set; }

        [JsonProperty("analyzer")] public string analyzer { get; set; }
    }

    public class StorageItems
    {

        [JsonProperty("properties")] public Properties properties { get; set; }
    }

    public class Type
    {

        [JsonProperty("type")] public string type { get; set; }

        [JsonProperty("analyzer")] public string analyzer { get; set; }
    }

    public class Visibility
    {

        [JsonProperty("type")] public string type { get; set; }

        [JsonProperty("analyzer")] public string analyzer { get; set; }
    }

    public partial class Properties
    {

        [JsonProperty("country")] public Country country { get; set; }

        [JsonProperty("embedded_thumbnail")] public EmbeddedThumbnail embedded_thumbnail { get; set; }

        [JsonProperty("id")] public Id id { get; set; }

        [JsonProperty("keywords")] public Keywords keywords { get; set; }

        [JsonProperty("metadata")] public Metadata metadata { get; set; }

        [JsonProperty("name")] public Name name { get; set; }

        [JsonProperty("storage_items")] public StorageItems storage_items { get; set; }

        [JsonProperty("type")] public Type type { get; set; }

        [JsonProperty("visibility")] public Visibility visibility { get; set; }
    }

    public class Asset
    {

        [JsonProperty("dynamic_templates")] public IList<DynamicTemplate> dynamic_templates { get; set; }

        [JsonProperty("properties")] public Properties properties { get; set; }
    }

    public class Mappings
    {

        [JsonProperty("asset")] public Asset asset { get; set; }
    }

    public class Assets
    {

        [JsonProperty("mappings")] public Mappings mappings { get; set; }
    }

    public class Example
    {

        [JsonProperty("assets")] public Assets assets { get; set; }
    }

}