<# 
    Loads the add-helixmodule.ps1 script to enable the creation of Feature and Foundation project in Sitecore Helix solutions.
    
    You need to change this path to the location where the script is located on your local machine. 
    
    Once the script is loaded the Add-Feature and Add-Foundation methods are available in the Package Manager Console in Visual Studio.
#>
. "D:\Websites\RoyalCanin\Latest\configuration\scripts\Helix\add-helixmodule.ps1"
. "D:\Websites\RoyalCanin\Latest\configuration\scripts\Build\build-publish-local.ps1"