import './styles.scss';
import BaseComponent from 'BaseComponent';

export default class ModuleName extends BaseComponent {
    constructor(el) {
        super(el);
        this.init();
        this.isInited = true;
    }

    static get tagName() {
        return 'ModuleName';
    }

    setVariables() {}

    addListeners() {}

    init() {
        this.setVariables();
        this.addListeners();
        this.initContentBlock();
    }

    initContentBlock() {
        console.log(`Module inited: ${this.tagName}`);
    }
}
