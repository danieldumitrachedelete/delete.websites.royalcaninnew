﻿using AutoMapper;
using Delete.Foundation.DeleteFoundationCore.Controllers;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Glass.Mapper.Sc;

namespace NamespacePrefix.ModuleType.ModuleName.Controllers
{
    using System;
    using System.Web.Mvc;
    using Sitecore.Data;
    using Repositories;

    public class ModuleNameController : BaseController
    {
        private readonly IModuleNameRepository _moduleNameRepository;
        private string ModuleNameCacheKey = "ModuleName_Component_CacheParametersShouldBeAddedHere";

        public ModuleNameController(IModuleNameRepository moduleNameRepository, ISitecoreContext sitecoreContext, IMapper mapper, ICacheManager cacheManager) : base(sitecoreContext, mapper, cacheManager)
        {
            _moduleNameRepository = moduleNameRepository;
        }

        public ActionResult ModuleName()
        {
            var cacheKey = string.Format(ModuleNameCacheKey);
            /* 
              TODO: Use the repository to retrieve model data 
              which can be passed into the view.
            */

            //var datasourceItem = GetDataSourceItem<Models.ModuleName>(false, true);
            //var model = _cacheManager.CacheResults(() => _mapper.Map<ModuleNameViewModel>(datasourceItem), string.Format(ModuleNameCacheKey));
            //return View("~/Views/ModuleType/ModuleName/ModuleName.cshtml", model);

            throw new NotImplementedException();
        }
    }
}