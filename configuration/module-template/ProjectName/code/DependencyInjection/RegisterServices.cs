﻿using Delete.Foundation.InitializeDependencyInjection.Extensions;
using Microsoft.Extensions.DependencyInjection;
using NamespacePrefix.ModuleType.ModuleName.Repositories;
using Sitecore.DependencyInjection;

namespace NamespacePrefix.ModuleType.ModuleName.DependencyInjection
{
    public class RegisterServices : IServicesConfigurator
    {
        public void Configure(IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IModuleNameRepository, ModuleNameRepository>();

            serviceCollection.AddMvcControllersInCurrentAssembly();
        }
    }
}