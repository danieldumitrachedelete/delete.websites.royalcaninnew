$string = 'put json array here'
$breeds = $string | ConvertFrom-Json

$localBreeds = Get-ChildItem -Path "master:\sitecore\content\Royal Canin\France\Home\Cats\Breeds\Breed Library" -Recurse

foreach ($breed in $localBreeds){
    $sameBreed = $breeds | Where { $_.EnName -eq $breed.Name } | Select -First 1
    
    if ($sameBreed -ne $null){
        $breed.Editing.BeginEdit()
        
        $breed["VitalBreedCode"] = $sameBreed.Id
        
        $breed.Editing.EndEdit()
    }
}