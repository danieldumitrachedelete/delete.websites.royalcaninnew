# Uncomment next lines if modueles are not installed

#Register-PSRepository -Name SitecoreGallery -SourceLocation https://sitecore.myget.org/F/sc-powershell/api/v2 
#Install-Module SitecoreInstallFramework
#Install-Module SitecoreFundamentals
 
# Import the modules into your current PowerShell context (if necessary)
Import-Module SitecoreFundamentals
Import-Module SitecoreInstallFramework


#define parameters
$prefix = "royalcanin"

$SolrUrl = "https://solr:8983/solr"
$SolrRoot = "D:\Solr\solr-6.6.2"
$SolrService = "Solr-6.6.2"


# install solr cores for sitecore 
$solrParams = 
@{
    Path = ".\sitecore-solr.json"
    SolrUrl = $SolrUrl 
    SolrRoot = $SolrRoot 
    SolrService = $SolrService 
    CorePrefix = $prefix
}
Install-SitecoreConfiguration @solrParams


# Run Sitecore / Control Panel / Indexing / "Populate Solr Managed Schema" after the script 
# then rebuild indexes