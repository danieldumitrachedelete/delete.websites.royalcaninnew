$rootPath = "master:/sitecore/content/Global Configuration/Breeds/Global Breeds"
$startTime = $(get-date)

	try {
		Write-Host "Retrieving items..."
		$buckets = Get-ChildItem -Path $rootPath -Recurse  | Where-Object { $_.Name -eq "Ageing Text Content With Link" }
		$totalBuckets = $buckets.Length
		Write-Host "There are $($totalBuckets) items to update."
		$counter = 0;
		$buckets | ForEach-Object {
			Write-Host "Processing item $($_.Name) ..."
			$title = $_['Title']
			Write-Host $title
			$title = $title -replace "Ageing ", ""
			$title = $title + " health"
			Write-Host $title
			$_.Editing.BeginEdit()
			$_['Title']=$title
			$_.Editing.EndEdit()
			$counter++
			Write-Host "item $($_.ItemPath) is done."
			Write-Host "Processed $($counter) of $($totalBuckets)"#>
		}
	}
	finally { 
		Write-Host "Done."
		$currentTime = $(get-date)
		$elapsedTime = new-timespan $startTime $currentTime
		Write-Host "Elapsed time: $($elapsedTime.ToString("hh\:mm\:ss"))"
	}
