// Styles

import '../Foundation/src/scss/index.scss';
import '../Feature/GenericComponents/code/src/richtext-editor';
import '../Feature/GenericComponents/code/src/tags-container';
import '../Feature/GenericComponents/code/src/definition-list';
import '../Feature/GenericComponents/code/src/form-elements';
import '../Feature/GenericComponents/code/src/preloader';
import '../Feature/GenericComponents/code/src/list';
import '../Feature/GenericComponents/code/src/text-with-icon';
import '../Feature/GenericComponents/code/src/info-window';
import '../Feature/GenericComponents/code/src/blockquote';
import '../Feature/GenericComponents/code/src/author-panel';
import '../Feature/GenericComponents/code/src/content-block';
import '../Feature/GenericComponents/code/src/autocomplete-places';
import '../Feature/Breeds/code/src/size-table';
import '../Feature/Footer/code/src';
import '../Feature/GenericComponents/code/src/hot-fixes';
import '../Feature/GenericComponents/code/src/wechat';
import '../Feature/Products/code/src/product-details';

// intersection observer polyfill
import 'intersection-observer';

// Features

import LanguagePicker from '../Feature/LanguagePicker/Code/src';
import Filter from '../Feature/GenericComponents/code/src/filter';
import AutoComplete from '../Feature/GenericComponents/code/src/autocomplete';
import ButtonSwitch from '../Feature/GenericComponents/code/src/button-switch';
import ListingWithPagination from '../Feature/GenericComponents/code/src/listing-with-pagination';
import Timeline from '../Feature/Breeds/code/src/timeline';
import ViewAllArticles from '../Feature/Articles/code/src/view-all-articles';
import '../Feature/Tips/code/src/tips';
import '../Feature/Tips/code/src/tip-page';
import ReadMore from '../Feature/GenericComponents/code/src/read-more';
import SearchResults from '../Feature/GenericComponents/code/src/search-results';
import { Form, Recaptcha, ShowRule, HiddenIf, ServerValidation } from '../Feature/Forms/code/src';
import Map from '../Feature/GenericComponents/code/src/map';
import ProductRating from '../Feature/Products/code/src/product-rating';
import WriteReview from '../Feature/Products/code/src/write-review';
import ArticleTabbedNavigation from '../Feature/Articles/code/src/article-tabbed-nav';
import VideoPanel from '../Feature/GenericComponents/code/src/video-wrapper';
import CarouselEventTracker from '../Feature/GenericComponents/code/src/carousel-event-tracker';
import CtaEventTracker from '../Feature/GenericComponents/code/src/cta-event-tracker';
import SearchEventTracker from '../Feature/GenericComponents/code/src/search-event-tracker';
import NavigationEventTracker from '../Feature/GenericComponents/code/src/navigation-event-tracker';
import ContentTracker from '../Feature/GenericComponents/code/src/content-tracker';
import ContentAnimation from '../Feature/GenericComponents/code/src/content-animation';
import ProductTrack from '../Feature/Products/code/src/product-track';
import ProductListTrack from '../Feature/Products/code/src/product-list-track';
import AmpRedirectBuyWidget from '../Feature/Products/code/src/amp-redirect-buy-widget';
import LiveChat from '../Feature/GenericComponents/code/src/live-chat';
import LiveChatPopup from '../Feature/GenericComponents/code/src/live-chat-popup';
import TencentVideo from '../Feature/GenericComponents/code/src/tencent-video';
import BrowserRegion from '../Feature/RegionPicker/code/src/browser-region';
import ManualRegion from '../Feature/RegionPicker/code/src/manual-region';
import RegionLinks from '../Feature/RegionPicker/code/src/region-links';
import TimelineLink from '../Feature/GenericComponents/code/src/puppies-kittens/timeline';

// BaseCore

import 'Foundation/src/js/lazysizes';
import BaseCore from 'Foundation/src/js/BaseCore';

const baseCore = new BaseCore();

baseCore.componentClasses = [
    LanguagePicker,
    Filter,
    AutoComplete,
    ButtonSwitch,
    Timeline,
    ReadMore,
    ListingWithPagination,
    SearchResults,
    Form,
    Recaptcha,
    ShowRule,
    HiddenIf,
    ServerValidation,
    Map,
    ProductRating,
    WriteReview,
    ArticleTabbedNavigation,
    VideoPanel,
    CarouselEventTracker,
    CtaEventTracker,
    SearchEventTracker,
    NavigationEventTracker,
    ContentTracker,
    ContentAnimation,
    ViewAllArticles,
    ProductTrack,
    ProductListTrack,
    AmpRedirectBuyWidget,
    LiveChat,
    LiveChatPopup,
    TencentVideo,
    BrowserRegion,
    ManualRegion,
    RegionLinks,
    TimelineLink
];
baseCore.init();

// SVG Sprite

const files = require.context('../Foundation/src/svg', true, /^\.\/.*\.svg/);
files.keys().forEach(files);
