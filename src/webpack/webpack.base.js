import webpack from 'webpack';
import path from 'path';
import CleanWebpackPlugin from 'clean-webpack-plugin';
import SpriteLoaderPlugin from 'svg-sprite-loader/plugin';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';

import { BundleAnalyzerPlugin } from 'webpack-bundle-analyzer';
import ManifestPlugin from 'webpack-manifest-plugin';
import CopyWebpackPlugin from 'copy-webpack-plugin';

import PostCssPipelineWebpackPlugin from './plugins/postcss-pipeline-webpack-plugin';
import postcssCriticalSplit from 'postcss-critical-split';

export default (args) => {
    const isAnalize =
        !!args && !!args.anlz
            ? new BundleAnalyzerPlugin({
                analyzerPort: 8889
            })
            : [];

    return {
        devtool: 'inline-source-map',
        entry: {
            app: './src',
            productfinder: './Feature/ProductFinder/code/src',
            amp: './Foundation/src/amp',
            admin:  './admin'
        },
        output: {
            path: path.resolve(__dirname, '../dist'),
            filename: '[name].[chunkhash].js',
            chunkFilename: '[name].[chunkhash].js',
            publicPath: '/dist/',
            jsonpFunction: 'webpackJsonpDelete'
        },
        resolve: {
            extensions: ['.js', '.scss'],
            modules: [
                'node_modules',
                path.resolve(__dirname, '../')
            ],
            alias: {
                modernizr$: path.resolve(__dirname, '../.modernizrrc'),
                pathToGeneral: path.resolve(__dirname, '../Foundation/src'),
                styleSettings: path.resolve(__dirname, '../Foundation/src/scss/settings/index.scss')
            }
        },
        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    loader: 'babel-loader',
                    options: {
                        cacheDirectory: true
                    }
                },
                {
                    test: /\.hbs$/,
                    loader: 'handlebars-loader',
                    query: {
                        helperDirs: [path.resolve(__dirname, '../Foundation/src/js/hbsHelpers')]
                    }
                },
                {
                    test: /\.modernizrrc(\.json)?$/,
                    use: ['modernizr-loader', 'json-loader']
                },
                {
                    test: /\.(png|svg|jpe?g|gif)$/,
                    exclude: /svg[\/\\]/,
                    loader: 'file-loader',
                    options: {
                        name: 'images/[name].[ext]'
                    }
                },
                {
                    test: /\.(woff2?|eot|ttf)$/,
                    loader: 'file-loader',
                    options: {
                        name: 'fonts/[name].[ext]'
                    }
                },
                {
                    test: /\.svg$/,
                    include: /svg[\/\\]/,
                    loader: 'svg-sprite-loader',
                    options: {
                        symbolId: 'icon-[name]'
                    }
                },
                {
                    test: /\.css$/,
                    use: [MiniCssExtractPlugin.loader, 'style-loader', 'css-loader']
                }
            ]
        },
        optimization: {
            splitChunks: {
                chunks: 'all',
                automaticNameDelimiter: '.',
                cacheGroups: {
                    commons: {
                        chunks: 'initial',
                        name: 'commons',
                        minChunks: 2
                    }
                }
            },
            runtimeChunk: {
                name: 'runtime'
            }
        },
        plugins: [
            new CleanWebpackPlugin('dist', {
                root: path.resolve(__dirname, '..')
            }),
            new MiniCssExtractPlugin({
                filename: '[name].[contenthash].css'
            }),
            new SpriteLoaderPlugin(),
            new ManifestPlugin(),            
            new CopyWebpackPlugin([{ from: './Foundation/src/images', to: '../dist/images' }]),
            new PostCssPipelineWebpackPlugin({
                predicate: k => /(^(?!livechat).*)$/.test(k),
                prefix: 'critical.',
                pipeline: [
                    postcssCriticalSplit({
                        output: postcssCriticalSplit.output_types.CRITICAL_CSS
                    })
                ]
            }),
        ].concat(isAnalize),
        stats: {
            children: false
        }
    };
};
