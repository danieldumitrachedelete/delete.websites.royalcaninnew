import webpack from 'webpack';
import path from 'path';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import PostCssPipelineWebpackPlugin from './plugins/postcss-pipeline-webpack-plugin';
import cssnano from 'cssnano';

export default () => {

    const isJsonOutput = process.argv.indexOf('--json') >= 0;
    if(!isJsonOutput){
        console.log('\nPRODUCTION BUILD\n');
    }

    return {
        devtool: 'source-map',
        module: {
            rules: [
                {
                    test: /\.s?css$/,
                    use: [
                        MiniCssExtractPlugin.loader,
                        {
                            loader: 'css-loader',
                            options: {
                                sourceMap: true,
                                importLoaders: 2,
                            }
                        },
                        {
                            loader: 'postcss-loader',
                            options: {
                                sourceMap: true
                            }
                        },
                        {
                            loader: 'sass-loader',
                            options: {
                                sourceMap: true
                            }
                        }
                    ]
                }
            ],
        },
        plugins: [
            new PostCssPipelineWebpackPlugin({
                prefix: '',
                pipeline: [
                    cssnano({
                        autoprefixer: {
                            remove: false,
                            add: false
                        },
                        safe: true,
                        discardComments: {
                            removeAll: true
                        }
                    })
                ]
            })
        ]
    };
};
