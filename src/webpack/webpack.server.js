import webpack from 'webpack';
import path from 'path';

export default () => {
    console.log('\nDEV SERVER\n');
    return {
        devServer: {
            hot: true,
            contentBase: path.resolve(__dirname, '../dist'),
            publicPath: '/dist/',
            compress: true,
            proxy: {
                '*': 'http://royalcanin.deletedev.com'
            },
            overlay: {
                errors: true
            },
            historyApiFallback: true
        },
        output: {
            publicPath: '/dist/'
        },
        module: {
            rules: [{
                test: /\.s?css$/,
                use: [
                    'style-loader',
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: true,
                            importLoaders: 2
                        }
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            sourceMap: true
                        }
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: true
                        }
                    }
                ]
            }]
        },
        plugins: [new webpack.HotModuleReplacementPlugin()]
    };
};
