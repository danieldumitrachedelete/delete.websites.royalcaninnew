module.exports = {
    plugins: [
        require('autoprefixer'),
        require('postcss-flexbugs-fixes'),
        require('postcss-object-fit-images'),
        require('css-mqpacker'),
        require('postcss-inline-svg'),
        require('postcss-custom-media'),
        require('postcss-media-minmax')
    ]
};
