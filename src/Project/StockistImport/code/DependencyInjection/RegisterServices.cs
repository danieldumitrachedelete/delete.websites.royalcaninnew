using Delete.Feature.Stockists.Models;
using Delete.Foundation.DataTemplates.Models.Taxonomy;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.DeleteFoundationCore.Managers;
using Delete.Foundation.InitializeDependencyInjection.Extensions;
using Delete.Foundation.ItemImport.Models.Stockists;
using Microsoft.Extensions.DependencyInjection;
using RoyalCanin.Project.StockistImport.Importers;
using RoyalCanin.Project.StockistImport.Orchestrators;
using RoyalCanin.Project.StockistImport.Parsers;
using RoyalCanin.Project.StockistImport.Processors;
using Sitecore.DependencyInjection;

namespace RoyalCanin.Project.StockistImport.DependencyInjection
{
    public class RegisterServices : IServicesConfigurator
    {
        public void Configure(IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IStockistFileParser<StockistCsvModel>, StockistCsvParser>();
            serviceCollection.AddScoped<IStockistFileParser<StockistXlsxModel>, StockistXlsxParser>();
            serviceCollection.AddScoped<IFileImportedStockistConstructionOrchestrator, FileImportedStockistConstructionOrchestrator>();
            serviceCollection.AddScoped<StockistImporter<StockistCsvModel>, StockistImporter<StockistCsvModel>>();
            serviceCollection.AddScoped<StockistImporter<StockistXlsxModel>, StockistImporter<StockistXlsxModel>>();
            serviceCollection.AddScoped<IStockistFileItemProcessor, StockistFileItemProcessor>();
            serviceCollection.AddScoped<IContactTypeItemProcessor, ContactTypeItemProcessor>();
            serviceCollection.AddScoped<IProfileTypeItemProcessor, ProfileTypeItemProcessor>();

            serviceCollection.AddTransient<IBaseSearchManager<Stockist>, BaseSearchManager<Stockist>>();
            serviceCollection.AddTransient<IBaseSearchManager<ContactType>, BaseSearchManager<ContactType>>();
            serviceCollection.AddTransient<IBaseSearchManager<ProfileType>, BaseSearchManager<ProfileType>>();

            serviceCollection.AddMvcControllersInCurrentAssembly();
        }
    }
}
