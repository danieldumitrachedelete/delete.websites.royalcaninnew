﻿using Delete.Foundation.DeleteFoundationCore.Constants;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.DeleteFoundationCore.Managers;
using Delete.Foundation.ItemImport.Models;
using Delete.Foundation.MultiSite.Models;
using RoyalCanin.Project.StockistImport.Processors;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Globalization;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace RoyalCanin.Project.StockistImport.Orchestrators
{
    public interface IFileImportedStockistConstructionOrchestrator
    {
        IList<ImportLogEntry> CreateAndSaveNewStockist(StockistDto stockist, LocalMarketSettings marketSettings, IEnumerable<Language> languages, ICacheManager cacheManager = null);

        IList<ImportLogEntry> RemoveStockistsExcept(IList<Guid> importedStockists);

        IList<ImportLogEntry> UnpublishStockistsExcept(IList<Guid> importedStockists);
    }

    public class FileImportedStockistConstructionOrchestrator : IFileImportedStockistConstructionOrchestrator
    {
        protected readonly IStockistFileItemProcessor StockistFileItemProcessor;

        public FileImportedStockistConstructionOrchestrator(IStockistFileItemProcessor stockistFileItemProcessor)
        {
            this.StockistFileItemProcessor = stockistFileItemProcessor;
        }

        public virtual IList<ImportLogEntry> CreateAndSaveNewStockist(StockistDto stockist, LocalMarketSettings marketSettings, IEnumerable<Language> languages, ICacheManager cacheManager = null)
        {
            var log = new List<ImportLogEntry>();

            var sw = new Stopwatch();
            sw.Start();

            // 0. Validate the record that came from the file before creating anything, skip if broken
            if (!this.ValidateRecord(stockist, out var reasons))
            {
                sw.Stop();

                log.Add(new ImportLogEntry
                {
                    Action = ImportAction.Rejected,
                    Level = MessageLevel.Info,
                    Message = $"stockist with id=\"{stockist.CustomerReference}\" was skipped for {reasons}, took {sw.ElapsedMilliseconds} ms"
                });
                return log;
            }
            var entry = new List<ImportLogEntry>();
            var localStockist = this.StockistFileItemProcessor.ProcessItem(stockist, languages);

            sw.Stop();

            log.Add(new ImportLogEntry
            {
                Action = ImportAction.Imported,
                Level = MessageLevel.Info,
                Id = localStockist.Id,
                Message = $"stockist \"{localStockist.Name}\" ('{localStockist.Fullpath}', id={{{localStockist.Id}}}) successfully processed, took {sw.ElapsedMilliseconds} ms"
            });

            return log;
        }

        public IList<ImportLogEntry> UnpublishStockistsExcept(IList<Guid> importedStockists)
        {
            var exists = this.StockistFileItemProcessor
                .GetExistItems(false)
                .GroupBy(x => x.Id)
                .Select(x => x.FirstOrDefault())
                .Where(x => x != null)
                .ToList();
            var existItemIds = exists.Select(x => x.Id).Where(x => x != Guid.Empty).ToList();
            var notImportedStockistIds = existItemIds.Except(importedStockists).ToList();
            var notImportedStockists = exists.Where(x => notImportedStockistIds.Contains(x.Id)).ToList();
            this.StockistFileItemProcessor.UnpublishNotImportedStockists(notImportedStockists);

            return notImportedStockists.Select(x => new ImportLogEntry
                {
                    Action = ImportAction.Deleted,
                    Level = MessageLevel.Info,
                    Message = $"Unpublish stockist {x.Fullpath}"
                })
                .ToList();
        }

        public IList<ImportLogEntry> RemoveStockistsExcept(IList<Guid> importedStockists)
        {
            var exists = this.StockistFileItemProcessor
                .GetExistItems(false)
                .GroupBy(x => x.Id)
                .Select(x => x.FirstOrDefault())
                .Where(x => x != null)
                .ToList();
            var existItemIds = exists.Select(x => x.Id).Where(x => x != Guid.Empty).ToList();
            var notImportedStockistIds = existItemIds.Except(importedStockists).ToList();
            var notImportedStockists = exists.Where(x => notImportedStockistIds.Contains(x.Id)).ToList();
            this.StockistFileItemProcessor.RemoveNotImportedStockists(notImportedStockistIds);
            return notImportedStockists.Select(x => new ImportLogEntry
            {
                Action = ImportAction.Deleted,
                Level = MessageLevel.Info,
                Message = $"Remove item {x.Fullpath}"
            })
                .ToList();
        }

        protected virtual bool ValidateRecord(StockistDto stockist, out string reasonMsg)
        {
            // todo: implement file record validation

            reasonMsg = string.Empty;
            return true;
        }
    }
}