using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.ItemImport.Importers;
using Delete.Foundation.ItemImport.Models;
using Delete.Foundation.ItemImport.Models.Stockists;
using Delete.Foundation.MultiSite.Models;
using Glass.Mapper.Sc;
using RoyalCanin.Project.StockistImport.Orchestrators;
using RoyalCanin.Project.StockistImport.Parsers;
using Sitecore.Data.Managers;

namespace RoyalCanin.Project.StockistImport.Importers
{
    public interface IStockistImporter : IImporter
    {
        ImportLog Run(LocalMarketSettings localMarket, Stream fileStream, ICacheManager cacheManager = null);
    }

    public class StockistImporter<T> : BaseImporter, IStockistImporter
        where T : StockistImportFileModel
    {
        private readonly IFileImportedStockistConstructionOrchestrator fileImportedStockistConstructionorchestrator;

        private readonly IStockistFileParser<T> stockistFileParser;

        public StockistImporter(
            IStockistFileParser<T> stockistFileParser,
            IFileImportedStockistConstructionOrchestrator fileImportedStockistConstructionorchestrator,
            ISitecoreContext sitecoreContext) : base(sitecoreContext)
        {
            this.stockistFileParser = stockistFileParser;
            this.fileImportedStockistConstructionorchestrator = fileImportedStockistConstructionorchestrator;
        }

        public ImportLog Run(LocalMarketSettings localMarket, Stream fileStream, ICacheManager cacheManager = null)
        {
            var log = new ImportLog();
            var importedStockists = new List<Guid>();

            var languages = localMarket.StockistLanguageVersions.Select(x => LanguageManager.GetLanguage(x.Name))
                .ToList();

            var stockists = this.stockistFileParser.Process(fileStream);

            foreach (var stockist in stockists)
            {
                var newLogEntries = new List<ImportLogEntry>();

                try
                {
                    var importLogEntries = this.fileImportedStockistConstructionorchestrator.CreateAndSaveNewStockist(stockist, localMarket, languages, cacheManager);
                    WriteToLog(importLogEntries);
                    newLogEntries.AddRange(importLogEntries);
                    importedStockists.AddRange(newLogEntries.Select(x => x.Id).Distinct());
                }
                catch (SkippableImportLogException e)
                {
                    var importLogEntry = e.Entry;
                    WriteToLog(importLogEntry);
                    newLogEntries.Add(importLogEntry);
                }
                catch (Exception e)
                {
                    var importLogEntry = new ImportLogEntry
                    {
                        Level = MessageLevel.Error,
                        Message = $"{this.GetType()}: {e.GetAllMessages()} {e.StackTrace}"
                    };
                    WriteToLog(importLogEntry);
                    newLogEntries.Add(importLogEntry);
                }

                log.Entries.AddRange(newLogEntries);
            }

            if (stockists != null && stockists.Any() && importedStockists != null && importedStockists.Any())
            {
                log.Entries.AddRange(this.fileImportedStockistConstructionorchestrator.UnpublishStockistsExcept(importedStockists));
            }
            else
            {
                log.Entries.Add(new ImportLogEntry { Level = MessageLevel.Info, Message = "No stockists were received, unpublish skipped" });
            }

            return log;
        }

        private void WriteToLog(IList<ImportLogEntry> importLogEntries)
        {
            foreach (var importLogEntry in importLogEntries)
            {
                WriteToLog(importLogEntry);
            }
        }

        private void WriteToLog(ImportLogEntry importLogEntry)
        {
            switch (importLogEntry.Level)
            {
                case MessageLevel.Error:
                    Sitecore.Diagnostics.Log.Error(importLogEntry.Message, this);
                    break;
                case MessageLevel.Critical:
                    Sitecore.Diagnostics.Log.Fatal(importLogEntry.Message, this);
                    break;
                case MessageLevel.Info:
                default:
                    Sitecore.Diagnostics.Log.Info(importLogEntry.Message, this);
                    break;
            }
        }

        
    }
}