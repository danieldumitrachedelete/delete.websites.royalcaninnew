﻿using Delete.Foundation.ItemImport.Models.Stockists;

namespace RoyalCanin.Project.StockistImport.Comparers
{
    using System;
    using System.Collections.Generic;

    using Models;

    public class StockistCsvModelEqualityComparer : IEqualityComparer<StockistCsvModel>
    {
        public bool Equals(StockistCsvModel x, StockistCsvModel y)
        {
            if (x == null || y == null)
            {
                return false;
            }

            return x.UniqueCustomerId.Equals(y.UniqueCustomerId, StringComparison.InvariantCultureIgnoreCase);
        }

        public int GetHashCode(StockistCsvModel obj)
        {
            return StringComparer.InvariantCultureIgnoreCase.GetHashCode(obj.UniqueCustomerId);
        }
    }
}