﻿using System;
using System.Collections.Generic;
using Delete.Foundation.DataTemplates.Models.Taxonomy;
using Delete.Foundation.ItemImport.ImportProcessors;
using Delete.Foundation.ItemImport.LogManagerImport;
using Delete.Foundation.ItemImport.Models;
using Glass.Mapper;
using Glass.Mapper.Sc;
using Sitecore.Globalization;

namespace RoyalCanin.Project.StockistImport.Processors
{
    public interface IProfileTypeItemProcessor : IBaseImportItemProcessor<ProfileType, ProfileTypeDto>
    {
    }

    public class ProfileTypeItemProcessor : BaseImportItemProcessor<ProfileType, ProfileTypeDto>, IProfileTypeItemProcessor
    {
        public ProfileTypeItemProcessor(ISitecoreContext sitecoreContext, ILogManagerImport logManagerImport, string locationPathOverride = null) : base(sitecoreContext, logManagerImport, locationPathOverride)
        {
        }

        protected override Func<ProfileType, string> IdStringFromSitecoreItem => x => x.ImportMappingValue;

        protected override Func<ProfileTypeDto, string> IdStringFromImportObj => GetIdString;

        protected override Func<ProfileTypeDto, string> ItemNameFromImportObj => GetIdString;

        protected override string DefaultLocation =>
            Sitecore.Configuration.Settings.GetSetting("Stockists.DefaultProfileTypesLocation",
                "/sitecore/content/Global Configuration/Taxonomy/Profile Types");

        public override ProfileType ProcessItem(ProfileTypeDto importObj, IEnumerable<Language> languageVersions,
            string pathOverride = null)
        {
            var id = this.CalculateItemId(importObj);
            if (!id.HasValue())
            {
                return null;
            }

            ProfileType item;

            using (new VersionCountDisabler())
            {
                item = this.GetItem(id);
            }
            return item;
        }

        private static string GetIdString(ProfileTypeDto ProfileType)
        {
            return ProfileType?.Code ?? string.Empty;
        }
    }
}