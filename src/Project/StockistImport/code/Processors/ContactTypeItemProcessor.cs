﻿using System;
using System.Collections.Generic;
using Delete.Foundation.DataTemplates.Models.Taxonomy;
using Delete.Foundation.ItemImport.ImportProcessors;
using Delete.Foundation.ItemImport.LogManagerImport;
using Delete.Foundation.ItemImport.Models;
using Glass.Mapper;
using Glass.Mapper.Sc;
using Sitecore.Globalization;

namespace RoyalCanin.Project.StockistImport.Processors
{
    public interface IContactTypeItemProcessor : IBaseImportItemProcessor<ContactType, ContactTypeDto>
    {
    }

    public class ContactTypeItemProcessor : BaseImportItemProcessor<ContactType, ContactTypeDto>, IContactTypeItemProcessor
    {
        public ContactTypeItemProcessor(ISitecoreContext sitecoreContext, ILogManagerImport logManagerImport, string locationPathOverride = null) : base(sitecoreContext, logManagerImport, locationPathOverride)
        {
        }

        protected override Func<ContactType, string> IdStringFromSitecoreItem => x => x.ImportMappingValue;

        protected override Func<ContactTypeDto, string> IdStringFromImportObj => GetIdString;

        protected override Func<ContactTypeDto, string> ItemNameFromImportObj => GetIdString;

        protected override string DefaultLocation =>
            Sitecore.Configuration.Settings.GetSetting("Stockists.DefaultContactTypesLocation",
                "/sitecore/content/Global Configuration/Taxonomy/Contact Types");

        public override ContactType ProcessItem(ContactTypeDto importObj, IEnumerable<Language> languageVersions,
            string pathOverride = null)
        {
            var id = this.CalculateItemId(importObj);
            if (!id.HasValue())
            {
                return null;
            }

            ContactType item;

            using (new VersionCountDisabler())
            {
                item = this.GetItem(id, defaultItemSelector: x => x.IsDefault);
            }
            return item;
        }

        private static string GetIdString(ContactTypeDto ContactType)
        {
            return ContactType?.Code ?? string.Empty;
        }
    }
}