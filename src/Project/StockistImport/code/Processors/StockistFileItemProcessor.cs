﻿using System;
using System.Collections.Generic;
using System.Linq;
using Delete.Feature.Stockists.Models;
using Delete.Foundation.DeleteFoundationCore.Models;
using Delete.Foundation.ItemImport.ImportProcessors;
using Delete.Foundation.ItemImport.LogManagerImport;
using Delete.Foundation.ItemImport.Models;
using Delete.Foundation.MultiSite.Models;
using Glass.Mapper.Sc;
using Glass.Mapper.Sc.Fields;
using Sitecore.Data;
using Sitecore.Globalization;

namespace RoyalCanin.Project.StockistImport.Processors
{
    public interface IStockistFileItemProcessor : IBaseImportItemProcessor<Stockist, StockistDto>
    {
        IList<Stockist> GetExistItems(bool useCache = true, Language language = null);

        int RemoveNotImportedStockists(IList<Guid> stockistsToRemove);

        int UnpublishNotImportedStockists(IList<Stockist> stockistsToRemove);
    }

    public class StockistFileItemProcessor : BaseImportItemProcessor<Stockist, StockistDto>, IStockistFileItemProcessor
    {
        protected readonly ILocalMarketSettings LocalMarketSettings;

        protected readonly IContactTypeItemProcessor ContactTypeItemProcessor;

        protected readonly IProfileTypeItemProcessor ProfileTypeItemProcessor;

        private IList<Stockist> cachedItems;

        public StockistFileItemProcessor(
            ISitecoreContext sitecoreContext,
            ILocalMarketSettings localMarketSettings,
            IContactTypeItemProcessor contactTypeItemProcessor,
            IProfileTypeItemProcessor profileTypeItemProcessor,
            ILogManagerImport logManagerImport,
            string locationPathOverride = null)
            : base(sitecoreContext, logManagerImport, locationPathOverride)
        {
            this.LocalMarketSettings = localMarketSettings;
            this.ContactTypeItemProcessor = contactTypeItemProcessor;
            this.ProfileTypeItemProcessor = profileTypeItemProcessor;
        }

        protected override string DefaultLocation => string.Empty;

        protected override Func<Stockist, string> IdStringFromSitecoreItem => stockist => stockist.CustomerReference?.Trim();

        protected override Func<StockistDto, string> IdStringFromImportObj => stockistRecord => stockistRecord.CustomerReference?.Trim();

        protected override Func<StockistDto, string> ItemNameFromImportObj => stockistRecord => (this.LocalMarketSettings.FixNonLatinStockistNames ? IdStringFromImportObj(stockistRecord) : stockistRecord.CompanyName?.Trim());

        public override Stockist ProcessItem(StockistDto importObj, IEnumerable<Language> languageVersions,
             string pathOverride = null)
        {
            if (importObj == null || languageVersions == null || !languageVersions.Any())
            {
                return null;
            }

            var defaultLanguage = languageVersions.First();

            var newId = this.CalculateItemId(importObj);
            var itemLocationInTargetContext = pathOverride ?? this.CalculateItemLocation(importObj);

            var defaultItem = this.GetItem(newId, importObj, itemLocationInTargetContext, defaultLanguage)
                              ?? this.CreateItem(importObj, itemLocationInTargetContext, defaultLanguage);
            defaultItem = this.MapDefaultVersionFields(defaultItem, importObj);

            if (languageVersions.Any())
            {
                var entry = new List<ImportLogEntry>();
                var languageItem = this.MapLanguageVersionFields(defaultItem, importObj,  languageVersions);

                foreach (var language in languageVersions)
                {
                    languageItem.Language = language.Name;
                    this.SaveItem(languageItem);
                }
            }

            return defaultItem;
        }

        public override Stockist GetItem(
            string targetIdString,
            StockistDto importObj = default(StockistDto),
            string locationOverride = null,
            Language language = null,
            Func<Stockist, bool> defaultItemSelector = null)
        {
            var item = this.GetExistItems(language: language)
                .Where(x => x.CustomerReference.Equals(targetIdString, StringComparison.InvariantCultureIgnoreCase))
                .ToList();
            return item.FirstOrDefault();
        }

        public IList<Stockist> GetExistItems(bool useCache = true, Language language = null)
        {
            var itemLocation = this.CalculateItemLocation(null);

            if (useCache)
            {
                return this.cachedItems ?? (this.cachedItems = this.GenericItemRepository.GetByPath(itemLocation, language).ToList());
            }

            return this.GenericItemRepository.GetByPath(itemLocation, language).ToList();
        }

        public int UnpublishNotImportedStockists(IList<Stockist> stockistsToUnpublish)
        {
            var result = 0;
            foreach (var stockist in stockistsToUnpublish)
            {
                if (stockist == null) continue;

                if (stockist.NeverPublish) continue;
                stockist.NeverPublish = true;
                this.SaveItem(stockist);

                result++;
            }

            return result;
        }

        public int RemoveNotImportedStockists(IList<Guid> stockistsToRemove)
        {
            var result = 0;
            foreach (var guid in stockistsToRemove)
            {
                var item = this.Context.Database.GetItem(new ID(guid));
                if (item == null)
                {
                    continue;
                }

                item.Versions.RemoveAll(true);
                item.Delete();
                
                result++;
            }

            return result;
        }

        protected override Stockist MapDefaultVersionFields(Stockist item, StockistDto importObj)
        {
            item = base.MapDefaultVersionFields(item, importObj);

            item.CustomerReference = this.IdStringFromImportObj(importObj)?.Trim();
            var entry = new List<ImportLogEntry>();
            item.ContactTypes =
                importObj.ContactTypes.Select(x => this.ContactTypeItemProcessor.ProcessItem(x, new List<Language>()));

            item.ProfileType = this.ProfileTypeItemProcessor.ProcessItem(importObj.ProfileType, new List<Language>());

            item.AddressLine1 = importObj.AddressLine1?.Trim();
            item.AddressLine2 = importObj.AddressLine2?.Trim();
            item.AddressLine3 = importObj.AddressLine3?.Trim();
            item.City = importObj.City?.Trim();
            item.Postcode = importObj.Postcode?.Trim();
            item.Phone = importObj.Phone?.Trim();
            item.Fax = importObj.Fax?.Trim();
            item.Email = importObj.Email?.Trim();
            item.Country = importObj.Country?.Trim();
            item.CountyOrState = importObj.CountyOrState?.Trim();
            item.Latitude = importObj.Latitude;
            item.Longitude = importObj.Longitude;
            
            return item;
        }

        protected override Stockist MapLanguageVersionFields(Stockist item, StockistDto importObj,
             IEnumerable<Language> languages)
        {
            item = base.MapLanguageVersionFields(item, importObj,  languages);

            item.CompanyName = importObj.CompanyName?.Trim();
            item.WebsiteURL = new Link {Target = "_blank", Type = LinkType.External, Url = importObj.WebsiteUrl?.Trim() ?? String.Empty};
            item.Description = importObj.Description?.Trim();

            return item;
        }

        protected override string CalculateItemLocation(StockistDto importObj)
        {
            return this.GetStockistsRootItem(importObj).Fullpath;
        }

        private GlassBase GetStockistsRootItem(StockistDto importObj)
        {
            return this.LocalMarketSettings.StockistImportItemsPath;
        }
    }
}