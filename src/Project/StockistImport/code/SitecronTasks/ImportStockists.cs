﻿using System.Linq;
using Delete.Feature.Stockists.Models;
using Delete.Foundation.DeleteFoundationCore.Constants;
using Delete.Foundation.ItemImport.Models.Stockists;
using System;
using System.Collections.Generic;
using System.IO;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Managers;
using Delete.Foundation.ItemImport.Importers;
using Delete.Foundation.ItemImport.Loggers;
using Delete.Foundation.MultiSite.Helpers;
using Delete.Foundation.MultiSite.Models;
using Delete.Foundation.RoyalCaninCore.Extensions;
using Glass.Mapper.Sc;
using RoyalCanin.Project.StockistImport.Importers;
using log4net;
using Microsoft.Extensions.DependencyInjection;
using Quartz;
using Sitecore.ContentSearch;
using Sitecore.Data;
using Sitecore.Data.Events;
using Sitecore.DependencyInjection;
using Sitecore.Jobs;
using Sitecore.SecurityModel;
using Sitecore.Sites;
using Sitecron.SitecronSettings;
using Delete.Foundation.DeleteFoundationCore.Interfaces;

namespace RoyalCanin.Project.StockistImport.SitecronTasks
{
    public class ImportStockists : IJob
    {
        private readonly ILog logger = LogManager.GetLogger("RoyalCanin.Project.StockistImport");

        private readonly IImportJobLogger importJobLogger;

        private IRemoteFileManager remoteFileManager;

        private bool documentsLoaded;

        private IStockistImporter stockistImporter;

        private ICacheManager cacheManager;

        public ImportStockists()
        {
            this.importJobLogger = new ImportJobLogger(this.logger);
            this.cacheManager = new CacheManager();
        }

        public void Execute(IJobExecutionContext context)
        {
            try
            {
                ClearCache("start");
                var jobDataMap = context.JobDetail.JobDataMap;
                var items = jobDataMap.GetString(SitecronConstants.FieldNames.Items);
                var siteRoots = items.ToGuidList("|");

                if (siteRoots.Any())
                {
                    this.logger.Info($"Sitecron - Job {nameof(ImportStockists)} - start");

                    //var databaseNames = new List<string>();
                    //var importWasSuccessful = false;

                    try
                    {
                        foreach (var siteRoot in siteRoots)
                        {
                            try
                            {
                                var rootPath = Database.GetDatabase(Constants.Sitecore.Databases.Master)?.GetItem(new ID(siteRoot))?.Paths.FullPath;
                                var sites = SiteManagerHelper.GetWebsitesByRoot(rootPath);
                                foreach (var site in sites)
                                {
                                    IEnumerable<string> languagesToPublish = null;
                                    try
                                    {
                                        var siteContext = SiteContext.GetSite(site.Name);
                                        var itemsToPublish = new List<Guid>();

                                        ClearCache($"start [{siteContext.Language}]");

                                        this.logger.Info($"Sitecron - Job {nameof(ImportStockists)} - site in processing RootPath: {siteContext.RootPath}; Language: {siteContext.Language}; Database: {siteContext.Database}");

                                        using (new SecurityDisabler())
                                        using (new SiteContextSwitcher(siteContext))
                                        using (new Sitecore.Globalization.LanguageSwitcher(siteContext.Language))
                                        using (new EventDisabler())
                                        using (new BulkUpdateContext())
                                        using (var innerScope = ServiceLocator.ServiceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope())
                                        {
                                            var sitecoreContext = innerScope.ServiceProvider.GetService<ISitecoreContext>();
                                            var localMarketSettings = sitecoreContext.GetItem<LocalMarketSettings>(siteContext.RootPath);
                                            languagesToPublish = localMarketSettings.StockistLanguageVersions.Select(l => l.Name).ToList();

                                            if (this.ProcessItemsInCurrentSitecoreContext(sitecoreContext, siteContext, siteRoots, innerScope))
                                            {
                                                //databaseNames.Add(sitecoreContext.Database.Name);
                                                itemsToPublish.Add(this.GetStockistsItemBySiteRoot(sitecoreContext, siteContext, siteRoots));
                                                RefreshIndexes(itemsToPublish, sitecoreContext);
                                                //importWasSuccessful = true;
                                            }
                                        }
                                        using (new SecurityDisabler())
                                        using (new SiteContextSwitcher(siteContext))
                                        {
                                            foreach (var lang in languagesToPublish)
                                            { 
                                                using (new Sitecore.Globalization.LanguageSwitcher(lang))
                                                { 
                                                    PublishItems(itemsToPublish);
                                                }
                                            }
                                        }

                                        this.logger.Info($"Sitecron - Job {nameof(ImportStockists)} - import {siteContext.Language} stockists finished successfully");
                                    }
                                    catch (Exception e)
                                    {
                                        this.logger.Error($"Sitecron - Job {nameof(ImportStockists)} - error processing language", e);
                                    }
                                }
                            }
                            catch (Exception e)
                            {
                                this.logger.Error($"Sitecron - Job {nameof(ImportStockists)} - error processing market", e);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        this.logger.Error($"Sitecron - Job {nameof(ImportStockists)} - error", e);
                    }
                }
                else
                {
                    this.logger.Info($"Sitecron - Job {nameof(ImportStockists)} - skipped because no sites are selected");
                }
            }
            catch (Exception e)
            {
                this.logger.Error($"Sitecron - Job {nameof(ImportStockists)} - error", e);
            }

            this.logger.Info($"Sitecron - Job {nameof(ImportStockists)} - finished successfully");
            ClearCache("end");
        }

        private void ClearCache(string position)
        {
            try
            {
                if (this.cacheManager == null)
                {
                    throw new NullReferenceException();
                }
                this.cacheManager.Clear();
                logger.Info($"Sitecron - Job {nameof(ImportStockists)} - cache clearning succeed with {nameof(ImportStockists)} on {position}");
            }
            catch (Exception ex)
            {
                logger.Error($"Sitecron - Job {nameof(ImportStockists)} - cache clearning error with {nameof(ImportStockists)} on {position}. Exception: {ex.Message}");
            }
        }


        ///// <summary>
        ///// nameMethod - PublishItems for publish items or RebuildIndexes - for rebuild indexes
        ///// </summary>
        ///// <param name="roots"></param>
        ///// <param name="siteName"></param>
        ///// <param name="nameMethod"></param>
        //private void JobForPublish(object roots, string siteName, string nameMethod)
        //{
        //    var options = new JobOptions($"{nameMethod} ImportStockists for {siteName}",
        //        "SmartPublishMethod",
        //        siteName,
        //        this,
        //        nameMethod,
        //        new object[] { roots, $"PublishImportProducts for {siteName}" });
        //    JobManager.Start(options);
        //}

        private void RebuildIndexes(IEnumerable<string> databaseNames)
        {
            foreach (var databaseName in databaseNames.Distinct())
            {
                this.logger.Info($"Sitecron - Job {nameof(ImportStockists)} - rebuilding index start");
                var targetIndexName = SearchManagersConfiguration.GetIndexNameByType(typeof(Stockist), databaseName);
                this.logger.Info($"Sitecron - Job {nameof(ImportStockists)} - {targetIndexName} - target index name");
                var index = ContentSearchManager.GetIndex(targetIndexName);

                index.Rebuild(IndexingOptions.Default);
                this.logger.Info($"Sitecron - Job {nameof(ImportStockists)} - {targetIndexName} - commit transactions");
                index.CreateUpdateContext().Commit();
                this.logger.Info($"Sitecron - Job {nameof(ImportStockists)} - rebuilding index finished");
                
            }
            
        }

        private void RefreshIndexes(List<Guid> itemsToPublish, ISitecoreContext sitecoreContext)
        {
            this.logger.Info($"Sitecron - Job {nameof(ImportStockists)} - refreshing index start");
            var targetIndexName =
                SearchManagersConfiguration.GetIndexNameByType(typeof(Stockist), sitecoreContext.Database.Name);
            this.logger.Info($"Sitecron - Job {nameof(ImportStockists)} - {targetIndexName} - target index name");

            var index = ContentSearchManager.GetIndex(targetIndexName);
            foreach (var itemGuid in itemsToPublish)
            {
                var startingPointItem = sitecoreContext.Database.GetItem(new ID(itemGuid));
                var indexableStartingPoint = new SitecoreIndexableItem(startingPointItem);
                this.logger.Info($"Sitecron - Job {nameof(ImportStockists)} - running refresh for {startingPointItem.Paths.FullPath}");

                index.Refresh(indexableStartingPoint, IndexingOptions.Default);
                this.logger.Info($"Sitecron - Job {nameof(ImportStockists)} - refresh done for {startingPointItem.Paths.FullPath}");
            }

            this.logger.Info($"Sitecron - Job {nameof(ImportStockists)} - {targetIndexName} - commit transactions");
            index.CreateUpdateContext().Commit();
            this.logger.Info($"Sitecron - Job {nameof(ImportStockists)} - refreshing index finished");
            this.logger.Info("");
        }

        private void PublishItems(IList<Guid> roots)
        {
            this.logger.Info($"Sitecron - Job {nameof(ImportStockists)} - publishing start");
            try
            {
                using (new DatabaseSwitcher(Database.GetDatabase(Constants.Sitecore.Databases.Master)))
                using (var innerScope = ServiceLocator.ServiceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope())
                {
                    var importer = innerScope.ServiceProvider.GetService<IImporter>();

                    if (importer == null)
                    {
                        this.logger.Warn($"Sitecron - Job {nameof(ImportStockists)} - nullable importer - items hasn't been published");
                        return;
                    }

                    if (roots == null || !roots.Any())
                    {
                        this.logger.Warn($"Sitecron - Job {nameof(ImportStockists)} - empty roots - items hasn't been published");
                        return;
                    }

                    foreach (var root in roots.Where(x => x != Guid.Empty))
                    {
                        var rootLogs = importer.Publish(root);
                        this.importJobLogger.LogImportResults(rootLogs);
                        
                    }
                }
            }
            catch (Exception exception)
            {
                this.logger.Error($"Sitecron - Job {nameof(ImportStockists)} - publishing error", exception);
            }

            this.logger.Info($"Sitecron - Job {nameof(ImportStockists)} - publishing end");
        }

        private Guid GetStockistsItemBySiteRoot(ISitecoreContext sitecoreContext, SiteContext siteContext, IList<Guid> siteRoots)
        {
            var localMarketSettings = sitecoreContext.GetItem<LocalMarketSettings>(siteContext.RootPath);
            if (localMarketSettings == null || !siteRoots.Contains(localMarketSettings.Id))
            {
                this.logger.Warn($"Sitecron - Job {nameof(ImportStockists)} - Local Market Settings not found");
                return Guid.Empty;
            }

            return localMarketSettings.StockistImportItemsPath?.Id ?? Guid.Empty;
        }

        private bool ProcessItemsInCurrentSitecoreContext(ISitecoreContext sitecoreContext, SiteContext siteContext, IList<Guid> siteRoots, IServiceScope innerScope)
        {
            var globalConfiguration = sitecoreContext.GetItem<GlobalConfigurationFolder>(GlobalConfigurationFolderConstants.GlobalConfigurationFolderItemIdString);
            if (globalConfiguration == null)
            {
                this.logger.Warn($"Sitecron - Job {nameof(ImportStockists)} - Global Market Settings not found");
                return false;
            }

            this.logger.Warn($"Sitecron - Job {nameof(ImportStockists)} - Global Market Settings found");

            var localMarketSettings = sitecoreContext.GetItem<LocalMarketSettings>(siteContext.RootPath);
            if (localMarketSettings == null || !siteRoots.Contains(localMarketSettings.Id))
            {
                this.logger.Warn($"Sitecron - Job {nameof(ImportStockists)} - Local Market Settings not found");
                return false;
            }

            this.logger.Info($"Sitecron - Job {nameof(ImportStockists)} - Local Market Settings found");

            var pathToFiles = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, localMarketSettings.StockistImportPathToFiles);
            if (!Directory.Exists(pathToFiles))
            {
                Directory.CreateDirectory(pathToFiles);
            }

            if (!this.documentsLoaded)
            {
                this.remoteFileManager = new SftpFileManager(
                    globalConfiguration.StockistImportSFTPHostname,
                    globalConfiguration.StockistImportSFTPUsername,
                    globalConfiguration.StockistImportSFTPPassword);
                this.remoteFileManager.GetFiles(globalConfiguration.StockistImportSFTPPath, pathToFiles);
                this.documentsLoaded = true;
            }

            var files = this.FindFiles(pathToFiles, localMarketSettings.StockistImportFtpFilePattern);

            return this.ProcessFiles(files, sitecoreContext, siteContext, globalConfiguration, localMarketSettings, innerScope);
        }

        private List<FileInfo> FindFiles(string pathToFiles, string searchPattern)
        {
            var files = Directory.GetFiles(pathToFiles, $"{searchPattern}")
                .Select(x => new FileInfo(x))
                .ToList();
            if (!files.Any())
            {
                this.logger.Info($"Sitecron - Job {nameof(ImportStockists)} - Files not found {pathToFiles}\\{searchPattern}");
            }

            return files;
        }

        private IStockistImporter GetImporterToProcessFile(FileInfo fileToProcess, IServiceScope innerScope)
        {
            if (fileToProcess.IsCsv())
            {
                return innerScope.ServiceProvider.GetService<StockistImporter<StockistCsvModel>>();
            }
            else if (fileToProcess.IsXlsx())
            {
                return innerScope.ServiceProvider.GetService<StockistImporter<StockistXlsxModel>>();
            }

            return null;
        }

        private bool ProcessFiles(IList<FileInfo> files,
            ISitecoreContext sitecoreContext,
            SiteContext siteContext,
            GlobalConfigurationFolder globalConfiguration,
            LocalMarketSettings localMarketSettings,
            IServiceScope innerScope)
        {
            if (!files.Any())
            {
                return false;
            }

            var fileToProcess = files.First();
            this.stockistImporter = this.GetImporterToProcessFile(fileToProcess, innerScope);
            if (this.stockistImporter == null)
            {
                return false;
            }

            using (var fileStream = fileToProcess.OpenRead())
            {
                var logs = this.stockistImporter.Run(localMarketSettings, fileStream, cacheManager);

                this.importJobLogger.LogImportResults(logs.Entries);
            }

            var processedFileNames = files.Select(x => x.Name).ToList();

            foreach (var file in files)
            {
                file.Delete();
            }

            this.remoteFileManager?.ArchiveFiles(globalConfiguration.StockistImportSFTPPath, globalConfiguration.StockistImportSFTPSavePath, processedFileNames);

            return true;
        }
    }
}