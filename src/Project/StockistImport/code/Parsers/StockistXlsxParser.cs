﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using AutoMapper;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.ItemImport.Models;
using Delete.Foundation.ItemImport.Models.Stockists;
using ExcelDataReader;

namespace RoyalCanin.Project.StockistImport.Parsers
{
    public interface IStockistXlsxParser : IStockistFileParser<StockistXlsxModel>
    {
    }

    public class StockistXlsxParser : StockistFileParser<StockistDto>, IStockistXlsxParser
    {
        public StockistXlsxParser(IMapper mapper) : base(mapper)
        {
        }

        public override IList<StockistDto> Process(Stream fileStream)
        {
            var result = new List<StockistXlsxModel>();
            using (var streamReader = ExcelReaderFactory.CreateReader(fileStream))
            {
                var dataSet = streamReader.AsDataSet(new ExcelDataSetConfiguration
                {
                    ConfigureDataTable = tableReader => new ExcelDataTableConfiguration
                    {
                        UseHeaderRow = true
                    }
                });

                if (dataSet.Tables.Count == 0)
                {
                    this.Logger?.Info($"Importing file doesn't contain Seller sheet");
                    return Mapper.Map<IList<StockistDto>>(result);
                }

                DataTable table = null;
                if (dataSet.Tables.Count >= 2)
                {
                    foreach (DataTable dataTable in dataSet.Tables)
                    {
                        if (!dataTable.TableName.Equals("Seller", StringComparison.InvariantCultureIgnoreCase))
                        {
                            continue;
                        }

                        table = dataTable;
                    }
                }

                if (table == null)
                {
                    table = dataSet.Tables[0];
                }

                result.AddRange(this.ParseDataTable(table));
            }

            return Mapper.Map<IList<StockistDto>>(result);
        }

        private static StockistXlsxModel ParseDataRow(DataRow dataTableRow)
        {
            var model = new StockistXlsxModel();
            model.CompanyName = dataTableRow[0] is DBNull ? string.Empty : dataTableRow[0].ToString();
            model.UniqueCustomerId = dataTableRow[1] is DBNull ? string.Empty : dataTableRow[1].ToString();
            model.ContactType = dataTableRow[2] is DBNull ? string.Empty : dataTableRow[2].ToString();
            model.Address = dataTableRow[3] is DBNull ? string.Empty : dataTableRow[3].ToString();
            model.City = dataTableRow[4] is DBNull ? string.Empty : dataTableRow[4].ToString();
            model.State = dataTableRow[5] is DBNull ? string.Empty : dataTableRow[5].ToString();
            model.Postcode = dataTableRow[6] is DBNull ? string.Empty : dataTableRow[6].ToString();
            model.Phone = dataTableRow[7] is DBNull ? string.Empty : dataTableRow[7].ToString();

            if (!(dataTableRow[8] is DBNull))
            {
                model.Latitude = dataTableRow[8].ToString().ParseInvariantOrDefault(0);
            }

            if (!(dataTableRow[9] is DBNull))
            {
                model.Longitude = dataTableRow[9].ToString().ParseInvariantOrDefault(0);
            }

            return model;
        }

        private List<StockistXlsxModel> ParseDataTable(DataTable dataTable)
        {
            var result = new List<StockistXlsxModel>();
            foreach (DataRow dataTableRow in dataTable.Rows)
            {
                try
                {
                    result.Add(ParseDataRow(dataTableRow));
                }
                catch (Exception ex)
                {
                    this.Logger?.Info($"Bad data found. Row: {dataTableRow}. Exception: {ex.Message}");
                }
            }

            return result;
        }
    }
}