﻿using Delete.Foundation.ItemImport.Models;
using System.Collections.Generic;
using System.IO;
using AutoMapper;
using log4net;

namespace RoyalCanin.Project.StockistImport.Parsers
{
    public interface IStockistFileParser<T>
    {
        IList<StockistDto> Process(Stream fileStream);
        
        void SetLogger(ILog logger);
    }

    public abstract class StockistFileParser<T> : IStockistFileParser<T>
    {
        protected ILog Logger { get;set; }
        protected readonly IMapper Mapper;

        protected StockistFileParser(IMapper mapper)
        {
            Mapper = mapper;
        }

        public abstract IList<StockistDto> Process(Stream fileStream);

        public void SetLogger(ILog logger)
        {
            this.Logger = logger;
        }
    }
}