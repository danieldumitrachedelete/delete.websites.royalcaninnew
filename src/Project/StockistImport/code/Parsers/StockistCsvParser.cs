﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq.Expressions;
using AutoMapper;
using Delete.Foundation.ItemImport.Models;
using Delete.Foundation.ItemImport.Models.Stockists;
using Delete.Foundation.MultiSite.Models;
using Glass.Mapper.Sc;
using System.Globalization;
using System.IO;
using System.Linq;
using CsvHelper;
using CsvHelper.Configuration;
using CsvHelper.TypeConversion;
using RoyalCanin.Project.StockistImport.Comparers;
using RoyalCanin.Project.StockistImport.Models;

namespace RoyalCanin.Project.StockistImport.Parsers
{
    public interface IStockistCsvParser : IStockistFileParser<StockistCsvModel>
    {
    }

    public class StockistCsvParser : StockistFileParser<StockistCsvModel>, IStockistCsvParser
    {
        private readonly ILocalMarketSettings localMarketSettings;
        private readonly IStockistsImportConfiguration stockistsImportConfiguration;

        private static readonly StockistCsvModelEqualityComparer StockistCsvModelEqualityComparer = new StockistCsvModelEqualityComparer();

        private readonly Configuration csvConfiguration;

        public StockistCsvParser(ILocalMarketSettings localMarketSettings, IMapper mapper) : base(mapper)
        {
            this.localMarketSettings = localMarketSettings;
            this.stockistsImportConfiguration =
                new SitecoreContext().GetItem<StockistsImportConfiguration>(localMarketSettings
                    .StockistsImportConfiguration);

            this.csvConfiguration = new Configuration();
            this.csvConfiguration.Delimiter = stockistsImportConfiguration.CSVDelimeter;
            this.csvConfiguration.HeaderValidated = (b, strings, arg3, arg4) => { };
            this.csvConfiguration.RegisterClassMap(new Mapping(stockistsImportConfiguration?.CSVFieldsOrder));
            this.csvConfiguration.BadDataFound = x =>
                {
                    this.Logger?.Info($"Bad data found. Field:{x.Field}, Row: {x.Row}, RawValues: {x.RawRecord}");
                };
        }

        public override IList<StockistDto> Process(Stream fileStream)
        {
            using (var streamReader = new StreamReader(fileStream))
            {
                var csv = new CsvReader(streamReader, this.csvConfiguration);

                return Mapper.Map<IList<StockistDto>>(csv.GetRecords<StockistCsvModel>().Distinct(StockistCsvModelEqualityComparer));
            }
        }

        public class Mapping : ClassMap<StockistCsvModel>
        {
            private void MapWithCheck<T>(NameValueCollection config, string fieldName, Expression<Func<StockistCsvModel, T>> fieldSelector, ITypeConverter converter = null)
            {
                var value = config?.Get(fieldName);
                var index = int.TryParse(value, out var result) ? result : -1;

                if (index != -1)
                {
                    var map = this.Map(fieldSelector).Index(index);
                    if (converter != null) map.TypeConverter(converter);
                }
            }

            public Mapping(NameValueCollection config)
            {
                this.MapWithCheck(config, nameof(StockistCsvModel.UniqueCustomerId), x => x.UniqueCustomerId);
                this.MapWithCheck(config, nameof(StockistCsvModel.CompanyName), x => x.CompanyName);
                this.MapWithCheck(config, nameof(StockistCsvModel.ContactType), x => x.ContactType);
                this.MapWithCheck(config, nameof(StockistCsvModel.AddContactType), x => x.AddContactType);
                this.MapWithCheck(config, nameof(StockistCsvModel.ProfileType), x => x.ProfileType);
                this.MapWithCheck(config, nameof(StockistCsvModel.Address), x => x.Address);
                this.MapWithCheck(config, nameof(StockistCsvModel.AddressLine2), x => x.AddressLine2);
                this.MapWithCheck(config, nameof(StockistCsvModel.AddressLine3), x => x.AddressLine3);
                this.MapWithCheck(config, nameof(StockistCsvModel.City), x => x.City);
                this.MapWithCheck(config, nameof(StockistCsvModel.Postcode), x => x.Postcode);
                this.MapWithCheck(config, nameof(StockistCsvModel.Country), x => x.Country);
                this.MapWithCheck(config, nameof(StockistCsvModel.Phone), x => x.Phone);
                this.MapWithCheck(config, nameof(StockistCsvModel.Fax), x => x.Fax);
                this.MapWithCheck(config, nameof(StockistCsvModel.Email), x => x.Email);
                this.MapWithCheck(config, nameof(StockistCsvModel.WebsiteURL), x => x.WebsiteURL);
                this.MapWithCheck(config, nameof(StockistCsvModel.Description), x => x.Description);
                this.MapWithCheck(config, nameof(StockistCsvModel.Latitude), x => x.Latitude, new DoubleConverter());
                this.MapWithCheck(config, nameof(StockistCsvModel.Longitude), x => x.Longitude, new DoubleConverter());
                this.MapWithCheck(config, nameof(StockistCsvModel.State), x => x.State);
            }

            public class DoubleConverter : ITypeConverter
            {
                public string ConvertToString(object value, IWriterRow row, MemberMapData memberMapData)
                {
                    return value?.ToString() ?? string.Empty;
                }

                public object ConvertFromString(string text, IReaderRow row, MemberMapData memberMapData)
                {
                    if (double.TryParse(text.Trim(), NumberStyles.Any, new NumberFormatInfo(), out var result))
                    {
                        return result;
                    }

                    return null;
                }
            }
        }
    }
}