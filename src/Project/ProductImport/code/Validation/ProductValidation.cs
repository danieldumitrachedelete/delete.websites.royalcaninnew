﻿using AutoMapper;
using Delete.Feature.Products.Import.Processors;
using Delete.Foundation.DataTemplates.Models.Taxonomy;
using Delete.Foundation.ItemImport.ImportProcessors;
using Delete.Foundation.ItemImport.LogManagerImport;
using Delete.Foundation.ItemImport.Services;
using Delete.Foundation.MultiSite.Models;
using Glass.Mapper.Sc;
using RoyalCanin.Project.ProductImport.Processors;
using System;
using System.Collections.Generic;
using Delete.Foundation.ItemImport.Models;
using RoyalCanin.Project.RoyalCanin.Models;
using Sitecore.Globalization;

namespace RoyalCanin.Project.ProductImport
{
    public interface IGlobalProductValidation
    {
        bool CheckGlobalProductExist(ImportedProductDto productDto);
    }

    public class GlobalProductValidation : IGlobalProductValidation
    {
        protected readonly Func<Species, IGlobalProductItemProcessor> GlobalProductItemServiceAccessor;
        public GlobalProductValidation(Func<Species, IGlobalProductItemProcessor> globalProductItemServiceAccessor,
            ILocalProductItemProcessor localProductItemProcessor)
        {
            this.GlobalProductItemServiceAccessor = globalProductItemServiceAccessor;
        }

        public bool CheckGlobalProductExist(ImportedProductDto productDto)
        {
            var globalProduct = this.GlobalProductItemServiceAccessor(productDto.Species).ProcessItem(productDto, new List<Language>());
            return globalProduct != null;
        }
    }
}