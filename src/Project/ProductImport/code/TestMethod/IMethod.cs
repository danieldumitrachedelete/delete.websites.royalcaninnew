﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Delete.Foundation.ItemImport.LogContainer;
using Delete.Foundation.ItemImport.Models;

namespace RoyalCanin.Project.ProductImport.TestMethod
{
    public interface IMethod
    {
        void Test(ILogContainer logContainer);

        void ChangeValue();

        void AddEntryToLog(ImportLogEntry entry);
        ILogContainer GetValue();

    }
}