﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Delete.Foundation.ItemImport.LogContainer;
using Delete.Foundation.ItemImport.Models;

namespace RoyalCanin.Project.ProductImport.TestMethod
{
    public class Method:IMethod
    {
        private ILogContainer contLocal;
        private ImportLog LocalLog;
        public Method(ILogContainer count)
        {
            count.Log = new ImportLog();
            
            LocalLog = count.Log;
            LocalLog.Entries = new List<ImportLogEntry>();
            contLocal = count;
            //count.Value = 20;
        }

        public void Test(ILogContainer logContainerr)
        {
            throw new NotImplementedException();
        }

        public void ChangeValue()
        {
            
            LocalLog.Entries.Add(new ImportLogEntry()
            {
                Action = ImportAction.New,
                Message = "Test"
            });
            //contLocal.Value += 30;
        }

        public void AddEntryToLog(ImportLogEntry entry)
        {
            LocalLog.Entries.Add(entry);
        }

        public ILogContainer GetValue()
        {
            return contLocal;

        }


    }
}