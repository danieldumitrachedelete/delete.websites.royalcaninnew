﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Delete.Feature.Products.Import.Processors;
using Delete.Feature.Products.Models;
using Delete.Foundation.DataTemplates.Models.Taxonomy;
using Delete.Foundation.DeleteFoundationCore.Models;
using Delete.Foundation.ItemImport.ImportProcessors;
using Delete.Foundation.ItemImport.LogManagerImport;
using Delete.Foundation.ItemImport.Services;
using Delete.Foundation.MultiSite.Models;
using Delete.Foundation.MultiSite.Services;
using Glass.Mapper.Sc;
using Lucene.Net.Search;
using RoyalCanin.Project.RoyalCanin.Models;
using Sitecore.Data;
using Sitecore.Data.Managers;
using Sitecore.Globalization;

namespace RoyalCanin.Project.ProductImport.Processors
{
    using Delete.Feature.Products.Models;
    using Delete.Foundation.ItemImport.Models;

    public interface ILocalProductItemProcessor : IBaseImportItemProcessor<ProductPage, ImportedProductDto>
    {
        IList<ProductPage> GetImportedProducts(string rootPath);

        bool ValidateProductRootPath(ImportedProductDto importedProduct);
    }



    public class LocalProductItemProcessor :
        BaseImportItemProcessor<ProductPage, ImportedProductDto>,
        ILocalProductItemProcessor
    {
        protected readonly ILocalMarketSettings LocalMarketSettings;
        protected readonly IPillarIdItemProcessor PillarIdItemProcessor;
        protected readonly IProductTypeItemProcessor ProductTypeProcessor;
        protected readonly Func<Species, IGlobalProductItemProcessor> GlobalProductItemServiceAccessor;
        protected readonly IPackagingSizeItemProcessor PackagingSizeItemProcessor;
        protected readonly IMediaItemItemProcessor MediaItemItemProcessor;
        protected readonly IMediaItemService MediaItemService;
        protected readonly Func<Species, ISpecialNeedItemProcessor> SpecialNeedServiceAccessor;
        protected readonly Func<Species, IIsSterilisedItemProcessor> IsSterilisedServiceAccessor;
        protected readonly Func<Species, IIndicationItemProcessor> IndicationServiceAccessor;
        protected readonly Func<Species, ICounterIndicationItemProcessor> CounterIndicationServiceAccessor;

        private IList<ProductCataloguePage> cachedProductCatalogueRoots;
        protected readonly IMapper Mapper;

        

        public LocalProductItemProcessor(
            ISitecoreContext sitecoreContext,
            ILocalMarketSettings localMarketSettings,
            IPillarIdItemProcessor pillarIdItemProcessor,
            Func<Species, IGlobalProductItemProcessor> globalProductItemServiceAccessor,
            IPackagingSizeItemProcessor packagingSizeItemProcessor,
            IMediaItemItemProcessor mediaItemItemProcessor,
            IMediaItemService mediaItemService,
            Func<Species, ISpecialNeedItemProcessor> specialNeedServiceAccessor,
            Func<Species, IIsSterilisedItemProcessor> isSterilisedServiceAccessor,
            Func<Species, IIndicationItemProcessor> indicationServiceAccessor,
            Func<Species, ICounterIndicationItemProcessor> counterIndicationServiceAccessor,
            IProductTypeItemProcessor productTypeProcessor,
            ILogManagerImport logManagerImport,
            IMapper mapper,
            string locationPathOverride = null) : base(sitecoreContext, logManagerImport, locationPathOverride)
        {
            this.LocalMarketSettings = localMarketSettings;
            this.PillarIdItemProcessor = pillarIdItemProcessor;
            this.GlobalProductItemServiceAccessor = globalProductItemServiceAccessor;
            this.PackagingSizeItemProcessor = packagingSizeItemProcessor;
            this.MediaItemItemProcessor = mediaItemItemProcessor;
            this.MediaItemService = mediaItemService;
            this.SpecialNeedServiceAccessor = specialNeedServiceAccessor;
            this.IsSterilisedServiceAccessor = isSterilisedServiceAccessor;
            this.IndicationServiceAccessor = indicationServiceAccessor;
            this.CounterIndicationServiceAccessor = counterIndicationServiceAccessor;
            this.ProductTypeProcessor = productTypeProcessor;
            this.Mapper = mapper;
        }

        protected override Func<ProductPage, string> IdStringFromSitecoreItem => x => x.GlobalProduct?.MainItemID;

        protected override Func<ImportedProductDto, string> IdStringFromImportObj => x => x.MainItemId;

        protected override Func<ImportedProductDto, string> ItemNameFromImportObj => x => (this.LocalMarketSettings.FixNonLatinProductNames ? GetGlobalProduct(x)?.Name : BuildProductName(x));

        private string BuildProductName(ImportedProductDto importObj)
        {
            /*var languages = new List<Language> { LanguageManager.DefaultLanguage };*/
            var languages = new List<Language> { LanguageManager.GetLanguage(SiteContextResolutionService.CurrentContext.Language) };
            var technology = this.ProductTypeProcessor.ProcessItem(importObj.Technology, languages);
            if (technology != null)
            {
                var globalItem = GetGlobalProduct(importObj);
                return FormatProductNameString(importObj, technology);
            }
            else
            {
                return importObj.ProductName;
            }
        }

        private string FormatProductNameString(ImportedProductDto importObj, ProductType technology)
        {
            return $"{importObj.ProductName} {technology?.Text}".Trim();
        }

        // not used, paths are handled by overriding CalculateItemLocation
        protected override string DefaultLocation => string.Empty;

        protected override bool MapDatabaseFields => true;

        protected bool CheckUpdatedDate =>
            LocalMarketSettings.IsCheckingUpdatedDate;

        public bool ValidateProductRootPath(ImportedProductDto importObj)
        {
            return this.GetProductCatalogueRootItem(importObj) != null;
        }

        public IList<ProductPage> GetImportedProducts(string rootPath)
        {
            var products = this.GetItems(rootPath)
                .Where(x => x.Imported)
                .ToList();
            return products;
        }

        private ProductCataloguePage GetProductCatalogueRootItem(ImportedProductDto importObj)
        {
            if (this.cachedProductCatalogueRoots == null)
            {
                var localMarketSettings = this.Context.GetRootItem<LocalMarketSettings>();
                var rootItems = localMarketSettings.ProductRootItems;
                this.cachedProductCatalogueRoots = rootItems.Select(x => x as ProductCataloguePage).Where(x => x != null).ToList();
            }

            var entry = new List<ImportLogEntry>();
            var pillar = this.PillarIdItemProcessor.ProcessItem(importObj?.Pillar, new List<Language>());
            var speciesId = importObj?.Species == Species.Cats ? SpecieConstants.CatItemId : SpecieConstants.DogItemId;

            var allMatches = this.cachedProductCatalogueRoots
                .Where(x => x.PCPillars.Any(p => p.Id == pillar?.Id))
                .Where(x => x.PCSpecies.Any(species => species.Id == speciesId));

            return allMatches.FirstOrDefault();
        }

        public override ProductPage ProcessItem(ImportedProductDto importObj,
            IEnumerable<Language> languageVersions, string pathOverride = null)
        {
            if (importObj == null)
            {
                return null;
            }

            ImportAction action;

            var defaultLanguage = LanguageManager.DefaultLanguage;

            var newId = this.CalculateItemId(importObj);
            var itemLocationInTargetContext = pathOverride ?? this.CalculateItemLocation(importObj);

            var defaultItem = this.GetItem(newId, importObj, itemLocationInTargetContext, defaultLanguage);

            if (defaultItem == null)
            {
                defaultItem = this.CreateItem(importObj, itemLocationInTargetContext, defaultLanguage);
                action = ImportAction.New;
                LogManagerLocalImport.WriteToLog($"Not found existing product for {importObj.ProductName}, and creating new");
            }
            else
            {
                action = ImportAction.Updated;
                LogManagerLocalImport.WriteToLog($"Updating existing product - {importObj.ProductName}");
            }


            defaultItem = this.MapDefaultVersionFields(defaultItem, importObj);
            defaultItem.Language = defaultLanguage.Name;
            this.SaveItem(defaultItem);
            LogManagerLocalImport.WriteToLog($"Mapping and saving existing/new product for - {importObj.ProductName} successful");
            LogManagerLocalImport.AddEntryToLog(new ImportLogEntry
            {
                Action = action,
                Level = MessageLevel.Info,
                Id = defaultItem.Id,
                Message = $"{importObj.ProductName}"

            });

            var languageList = languageVersions ?? new List<Language>();

            if (languageList.Any())
            {
                foreach (var language in languageList)
                {
                    var languageItem = this.GetItem(newId, importObj, itemLocationInTargetContext, language)
                                       ?? defaultItem;

                    languageItem = this.MapLanguageVersionFields(languageItem, importObj, languageList);
                    languageItem.Language = language.Name;
                    this.SaveItem(languageItem);
                    LogManagerLocalImport.WriteToLog($"Mapping and saving language version existing/new product for - {importObj.ProductName} successful");
                }
            }

            return defaultItem;


        }

        public override ProductPage GetItem(
            string targetIdString,
            ImportedProductDto importObj,
            string locationOverride = null,
            Language language = null,
            Func<ProductPage, bool> defaultItemSelector = null)
        {
            if (string.IsNullOrEmpty(targetIdString)) return null;

            var itemLocation = locationOverride ?? this.CalculateItemLocation(importObj);

            var items = this.GetItems(itemLocation, language);

            var matchedItems = items
                .Where(x => targetIdString.Equals(this.CalculateItemId(x), StringComparison.OrdinalIgnoreCase))
                .ToList();

            if (!matchedItems.Any() && defaultItemSelector != null)
            {
                matchedItems = items.Where(defaultItemSelector).ToList();
            }

            if (matchedItems.Count > 1)
            {
                LogManagerLocalImport.AddEntryToLog(new ImportLogEntry
                {
                    Level = MessageLevel.Error,
                    Message = $"{this.GetType()}: Multiple existing matches found in \"{itemLocation}\" for Id=\"{targetIdString}\""
                });

            }

            return matchedItems.FirstOrDefault();
        }

        protected override string CalculateItemLocation(ImportedProductDto importObj)
        {
            return this.GetProductCatalogueRootItem(importObj).Fullpath;
        }

        protected override ProductPage MapLanguageVersionFields(ProductPage item, ImportedProductDto importObj, IEnumerable<Language> languages)
        {
            if (CheckUpdatedDate && languages != null && languages.Any())
            {
                var langItem = this.Context.GetItem<ProductPage>(item.Id, languages.First());

                if (langItem != null && langItem.Updated > importObj.UpdatedAt && !langItem.HideVersion && !langItem.NeverPublish)
                {
                    throw new SkippableImportLogException
                    {
                        Entry = new ImportLogEntry
                        {
                            Action = ImportAction.Skipped,
                            Level = MessageLevel.Info,
                            Message = $"{importObj.ProductName} ({importObj.MainItemId}) skipped: received product is not newer than existing, received: {importObj.UpdatedAt}, existing: {langItem.Updated}",
                            Id = langItem.Id
                        }
                    };
                }
            }

            item = base.MapLanguageVersionFields(item, importObj, languages);
            item = this.ResetOverrideFields(item);

            item.Imported = true;
            item.HideVersion = false;
            item.NeverPublish = false;

            item = this.MapPackaging(item, importObj, languages);

            var importImages = Sitecore.Configuration.Settings.GetBoolSetting("Products.ImportProductImages", true);
            if (importImages)
            {
                //item = this.MapImages(item, importObj, ref entries);
                item = this.MapImages(item, importObj);
            }

            item = this.MapSpecialNeeds(item, importObj, languages);
            item = this.MapIndications(item, importObj, languages);
            item = this.MapCounterIndications(item, importObj, languages);
            item = this.MapProductType(item, importObj, languages);
            item = this.MapTechnology(item, importObj, languages);

            this.MapFeedingPeriod(item, importObj);

            item.ProductName = importObj.ProductName;
            item.MainDescription = importObj.MainDescription;
            item.MetaData = importObj.Tags;
            item.MetaDataKeywords = importObj.Keywords;
            item.BenefitText = importObj.BenefitText;
            item.ProductDescriptionRaw = importObj.ProductDescription;
            item.MainBenefit = importObj.MainBenefit;
            item.SecondaryBenefit = importObj.SecondaryBenefit;
            item.Text = importObj.Text;
            item.Ingredients = importObj.Ingredients;
            item.SKU = importObj.SKU;
            item.EAN = importObj.EAN;
            item.BenefitText4 = importObj.BenefitText4;
            item.BenefitText5 = importObj.BenefitText5;
            item.BenefitText6 = importObj.BenefitText6;
            item.BenefitTitle1 = importObj.BenefitTitle1;
            item.BenefitTitle2 = importObj.BenefitTitle2;
            item.BenefitTitle3 = importObj.BenefitTitle3;
            item.BenefitTitle4 = importObj.BenefitTitle4;
            item.BenefitTitle5 = importObj.BenefitTitle5;
            item.BenefitTitle6 = importObj.BenefitTitle6;
            item.FeedingGuideRaw = importObj.FeedingGuide;
            item.AdditivesRaw = importObj.Additives;
            item.AConstituentsRaw = importObj.AConstituents;
            item.GuaranteedAnalysisRaw = importObj.GuaranteedAnalysis;

            return item;
        }

        protected override ProductPage MapDefaultVersionFields(ProductPage item, ImportedProductDto importObj)
        {
            item = base.MapDefaultVersionFields(item, importObj);
            var entry = new List<ImportLogEntry>();
            item.GlobalProduct = GetGlobalProduct(importObj);

            //if (item.GlobalProduct == null)
            //{
            //    if (this.ItemsCache.ContainsKey(this.CalculateItemLocation(importObj)))
            //    {
            //        this.ItemsCache[this.CalculateItemLocation(importObj)].Remove(item);
            //    }
            //    this.Context.Delete(item);

            //    throw new SkippableImportLogException
            //    {
            //        Entry = new ImportLogEntry
            //        {
            //            Action = ImportAction.Rejected,
            //            Level = MessageLevel.Info,
            //            Message = $"{importObj.ProductName} ({importObj.MainItemId}) skipped: Global product with Main Item ID ({importObj.MainItemId}) doesn't exist"
            //        }
            //    };
            //}

            return item;
        }

        protected GlobalProduct GetGlobalProduct(ImportedProductDto importObj)
        {
            return this.GlobalProductItemServiceAccessor(importObj.Species).ProcessItem(importObj, new List<Language>());
        }

        private ProductPage ResetOverrideFields(ProductPage item)
        {
            if (!item.AnimalSizeOverridePermanently) item.AnimalSizeOverrides = null;
            if (!item.BenefitTextOverridePermanently) item.BenefitTextOverride = null;
            if (!item.BreedOverridePermanently) item.BreedOverride = Guid.Empty;
            if (!item.CounterIndicationsOverridePermanently) item.CounterIndicationsOverrides = null;
            if (!item.FeedingPeriodFromOverridePermanently) item.FeedingPeriodFromOverride = 0;
            if (!item.FeedingPeriodToOverridePermanently) item.FeedingPeriodToOverride = 0;
            if (!item.Image1_BAGOverridePermanently) item.Image1_BAGOverride = null;
            if (!item.Image4_KIBBLEOverridePermanently) item.Image4_KIBBLEOverride = null;
            if (!item.Image5_EMBLEMATICOverridePermanently) item.Image5_EMBLEMATICOverride = null;
            if (!item.Image6OverridePermanently) item.Image6Override = null;
            if (!item.Image7OverridePermanently) item.Image7Override = null;
            if (!item.Image8OverridePermanently) item.Image8Override = null;
            if (!item.FeedingGuideImageOverridePermanently) item.FeedingGuideImageOverride = null;
            if (!item.IndicationsOverridePermanently) item.IndicationsOverrides = null;
            if (!item.IngredientsOverridePermanently) item.IngredientsOverrideRaw = null;
            if (!item.LifestagesOverridePermanently) item.LifestagesOverrides = null;
            if (!item.MainBenefitOverridePermanently) item.MainBenefitOverride = null;
            if (!item.MainDescriptionOverridePermanently) item.MainDescriptionOverride = null;
            if (!item.MetaDataOverridePermanently) item.MetaDataOverride = null;
            if (!item.MetaKeywordsOverridePermanently) item.MetaKeywordsOverride = null;
            if (!item.PackagingSizeOverridePermanently) item.PackagingSizeOverrides = null;
            if (!item.PillarIDCodeOverridePermanently) item.PillarIDCodeOverride = Guid.Empty;
            if (!item.ProductDescriptionOverridePermanently) item.ProductDescriptionOverrideRaw = null;
            if (!item.ProductNameOverridePermanently) item.ProductNameOverride = null;
            if (!item.ProductTypeOverridePermanently) item.ProductTypeOverride = Guid.Empty;
            if (!item.SecondaryBenefitOverridePermanently) item.SecondaryBenefitOverride = null;
            if (!item.SpecialNeedsOverridePermanently) item.SpecialNeedsOverrides = null;
            if (!item.IsSterilisedOverridePermanently) item.IsSterilisedOverride = null;
            if (!item.TextOverridePermanently) item.TextOverride = null;
            if (!item.TextSpare1OverridePermanently) item.TextSpare1Override = null;
            if (!item.TextSpare2OverridePermanently) item.TextSpare2Override = null;
            if (!item.AConstituentsOverridePermanently) item.AConstituentsOverrideRaw = null;
            if (!item.AdditivesOverridePermanently) item.AdditivesOverrideRaw = null;
            if (!item.FeedingGuideOverridePermanently) item.FeedingGuideOverrideRaw = null;

            return item;
        }

        private void MapFeedingPeriod(ProductPage item, ImportedProductDto importObj)
        {
            if (int.TryParse(importObj.FeedingPeriodFrom, out var feedingPeriodFrom))
            {
                item.FeedingPeriodFrom = feedingPeriodFrom;
            }

            if (int.TryParse(importObj.FeedingPeriodTo, out var feedingPeriodTo))
            {
                item.FeedingPeriodTo = feedingPeriodTo;
            }
        }

        private ProductPage MapPackaging(ProductPage item, ImportedProductDto importObj, IEnumerable<Language> languages)
        {
            // Packaging sizes, nested under the item
            if (importObj.Packaging == null || !importObj.Packaging.Any()) return item;
            var sizes = importObj.Packaging.Select(x =>
                this.PackagingSizeItemProcessor.ProcessItem(x, languages, item.Fullpath)).ToList();

            var listPackages = sizes.Select(x => x.Size).Aggregate("", (a, b) => $"{a}, {b}");
            LogManagerLocalImport.WriteToLog($"Incoming packages - {listPackages}");

            var PackagingSizesInItem = item.Children.Where(x => x.TemplateName == "Packaging Size").ToList();
            var itemsPackages = new List<PackagingSize>();
            listPackages = "";
            foreach (var existPackagingSize in PackagingSizesInItem)
            {
                var localPackgeSize = this.Mapper.Map<PackagingSize>(existPackagingSize);
                itemsPackages.Add(localPackgeSize);
                listPackages += localPackgeSize.Size + ",";
            }

            listPackages = listPackages.TrimEnd(',');
            LogManagerLocalImport.WriteToLog($"Existing packages - {listPackages}");

            item.PackagingSizes = sizes;

            foreach (var itemsPackage in itemsPackages)
            {
                var t = importObj.Packaging.Where(x => x.Size == itemsPackage.Size);
                if (!t.Any())
                {
                    this.PackagingSizeItemProcessor.DeleteItem(itemsPackage);
                    LogManagerLocalImport.WriteToLog($"Deleting package - {itemsPackage.Size}");
                }
            }

            return item;
        }


        private ProductPage MapImages(ProductPage item, ImportedProductDto importObj)
        {
            LogManagerLocalImport.WriteToLog($"Starting mapping and downloading/updating images for - {importObj.ProductName} ..... ");
            var websiteMediaFolder = this.MediaItemService.GetLocalMarketMediaFolder();
            var imagesFolder = this.MediaItemService.GetOrCreateMediaFolder(websiteMediaFolder, "Images");
            var productsFolder = this.MediaItemService.GetOrCreateMediaFolder(imagesFolder, "Products");
            var speciesFolder =
                this.MediaItemService.GetOrCreateMediaFolder(productsFolder, importObj.Species.ToString());
            var productFolder = this.MediaItemService.GetOrCreateMediaFolder(speciesFolder, item.Name);

            ////Image 1 - BAG
            item.Image1_BAG = this.CreateImageFromDto(importObj.Image1, productFolder.Fullpath);
            LogManagerLocalImport.WriteToLog($"Image 1 downloading/updating for - {importObj.ProductName}");
            ////Image 4 - KIBBLE
            item.Image4_KIBBLE = this.CreateImageFromDto(importObj.Image4, productFolder.Fullpath);
            LogManagerLocalImport.WriteToLog($"Image 4 downloading/updating for - {importObj.ProductName}");
            ////Image 5 - EMBLEMATIC
            item.Image5_EMBLEMATIC = this.CreateImageFromDto(importObj.Image5, productFolder.Fullpath);
            LogManagerLocalImport.WriteToLog($"Image 5 downloading/updating for - {importObj.ProductName}");
            item.Image6 = this.CreateImageFromDto(importObj.Image6, productFolder.Fullpath);
            LogManagerLocalImport.WriteToLog($"Image 6 downloading/updating for - {importObj.ProductName}");
            item.Image7 = this.CreateImageFromDto(importObj.Image7, productFolder.Fullpath);
            LogManagerLocalImport.WriteToLog($"Image 7 downloading/updating for - {importObj.ProductName}");
            item.Image8 = this.CreateImageFromDto(importObj.Image8, productFolder.Fullpath);
            LogManagerLocalImport.WriteToLog($"Image 8 downloading/updating for - {importObj.ProductName}");
            item.FeedingGuideImage = this.CreateImageFromDto(importObj.FeedingGuideImage, productFolder.Fullpath);
            LogManagerLocalImport.WriteToLog($"Image FeedingGuideImage downloading/updating for - {importObj.ProductName}");
            return item;
        }

        private ProductPage MapProductType(ProductPage item, ImportedProductDto importObj, IEnumerable<Language> languages)
        {
            if (importObj.ProductType == null) return item;
            item.ProductType = this.ProductTypeProcessor.ProcessItem(importObj.ProductType, languages);

            return item;
        }

        private ProductPage MapTechnology(ProductPage item, ImportedProductDto importObj, IEnumerable<Language> languages)
        {
            if (importObj.Technology == null) return item;

            item.Technology = this.ProductTypeProcessor.ProcessItem(importObj.Technology, languages);

            return item;
        }

        private ProductPage MapCounterIndications(ProductPage item, ImportedProductDto importObj, IEnumerable<Language> languages)
        {
            var counterindications = importObj.CounterIndications;
            if (counterindications == null || !counterindications.Any()) return item;
            item.CounterIndications = counterindications
                .Select(x => this.CounterIndicationServiceAccessor(importObj.Species).ProcessItem(x, languages));

            return item;
        }

        private ProductPage MapIndications(ProductPage item, ImportedProductDto importObj, IEnumerable<Language> languages)
        {
            var indications = importObj.Indications;
            if (indications == null || !indications.Any()) return item;
            item.Indications = indications.Select(x =>
                this.IndicationServiceAccessor(importObj.Species).ProcessItem(x, languages));

            return item;
        }

        private ProductPage MapSpecialNeeds(ProductPage item, ImportedProductDto importObj, IEnumerable<Language> languages)
        {
            // Special Needs
            var specialNeeds = importObj.SpecialNeeds;
            if (specialNeeds == null || !specialNeeds.Any()) return item;
            item.SpecialNeeds = specialNeeds.Select(x =>
                this.SpecialNeedServiceAccessor(importObj.Species).ProcessItem(x, languages));

            // Is Sterilised
            item.IsSterilised = this.IsSterilisedServiceAccessor(importObj.Species)
                .ProcessItem(importObj.IsSterilized, languages);

            return item;
        }

        private static ExtendedImage CreateExtendedImageFromExternalImage(ExternalImage externalImage)
        {
            return new ExtendedImage
            {
                Src = externalImage.Src,
                Width = externalImage.ImgWidth,
                Height = externalImage.ImgHeight,
                DateUpdated = externalImage.Date,
                Alt = externalImage.Description,
            };
        }

        private ExtendedImage CreateImageFromDto(ImageDto content, string path)
        {
            ExtendedImage result = null;
            if (content?.ExternalImage != null)
            {
                result = CreateExtendedImageFromExternalImage(content.ExternalImage);
            }
            else
            {
                var image = this.MediaItemItemProcessor.ProcessItem(content, new List<Language>(), path);
                if (image != null)
                {
                    result = new ExtendedImage { MediaId = image.Id };
                }
            }

            return result;
        }
    }
}