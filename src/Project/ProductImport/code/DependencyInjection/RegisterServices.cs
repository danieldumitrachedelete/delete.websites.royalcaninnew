using System.ComponentModel;
using Common.Logging;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.DeleteFoundationCore.Managers;
using Delete.Foundation.InitializeDependencyInjection.Extensions;
using Delete.Foundation.ItemImport.LogContainer;
using Delete.Foundation.ItemImport.LogManagerImport;
using Delete.Foundation.ItemImport.Models;
using Microsoft.Extensions.DependencyInjection;
using RoyalCanin.Project.ProductImport.Importers;
using RoyalCanin.Project.ProductImport.Orchestrators;
using RoyalCanin.Project.ProductImport.Processors;
using RoyalCanin.Project.ProductImport.TestMethod;
using RoyalCanin.Project.RoyalCanin.Models;
using Sitecore.DependencyInjection;

namespace RoyalCanin.Project.ProductImport.DependencyInjection
{
    public class RegisterServices : IServicesConfigurator
    {
        public void Configure(IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IImportedProductConstructionOrchestrator, ImportedProductConstructionOrchestrator>();

            serviceCollection.AddScoped<IProductImporter, ProductImporter>();

            serviceCollection.AddScoped<ILocalProductItemProcessor, LocalProductItemProcessor>();

            serviceCollection.AddScoped<IGlobalProductValidation, GlobalProductValidation>();

            serviceCollection.AddTransient<IBaseSearchManager<ProductPage>, BaseSearchManager<ProductPage>>();

            serviceCollection.AddScoped<ILogContainer, CounterValueSet>();

            serviceCollection.AddScoped<IMethod, Method>();

            serviceCollection.AddScoped<ILogManagerImport, LogManagerImport>();

            serviceCollection.AddMvcControllersInCurrentAssembly();
        }
    }
}
