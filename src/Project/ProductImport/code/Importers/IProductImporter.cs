﻿using System;
using Delete.Foundation.ItemImport.Importers;
using Delete.Foundation.ItemImport.Models;

namespace RoyalCanin.Project.ProductImport.Importers
{
    public interface IProductImporter : IImporter
    {
        void Run(Guid siteId, DateTime lastRunDate);
    }
}