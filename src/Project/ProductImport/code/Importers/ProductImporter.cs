﻿using AutoMapper;
using Delete.Foundation.DataTemplates.Models.Taxonomy;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.DeleteFoundationCore.Managers;
using Delete.Foundation.ItemImport.Controllers;
using Delete.Foundation.ItemImport.Importers;
using Delete.Foundation.ItemImport.LogManagerImport;
using Delete.Foundation.ItemImport.Models;
using Delete.Foundation.ItemImport.Models.LSHImportFeed;
using Delete.Foundation.ItemImport.Models.SalsifyImportFeed;
using Delete.Foundation.ItemImport.Models.WeShareImportFeed;
using Delete.Foundation.MultiSite.Models;
using Glass.Mapper;
using Glass.Mapper.Sc;
using RoyalCanin.Project.ProductImport.Orchestrators;
using RoyalCanin.Project.ProductImport.Processors;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;

namespace RoyalCanin.Project.ProductImport.Importers
{
    using Delete.Foundation.DeleteFoundationCore.Extensions;
    using System.Text.RegularExpressions;

    public class ProductImporter : BaseImporter, IProductImporter
    {
        protected readonly IImportedProductConstructionOrchestrator ImportedProductConstructionOrchestrator;

        protected readonly IMapper Mapper;

        protected ILogManagerImport LogManagerLocalImport;

        private ILocalProductItemProcessor LocalProductItemProcessor;

        public ProductImporter(
            ISitecoreContext sitecoreContext,
            IMapper mapper,
            ICacheManager cacheManager,
            ILogManagerImport LogManagerImport,
            IImportedProductConstructionOrchestrator importedProductConstructionOrchestrator,
            ILocalProductItemProcessor localProductItemProcessor) : base(sitecoreContext)
        {
            this.ImportedProductConstructionOrchestrator = importedProductConstructionOrchestrator;
            this.Mapper = mapper;
            this.LogManagerLocalImport = LogManagerImport;
            this.LocalProductItemProcessor = localProductItemProcessor;
        }

        public void Run(Guid siteId, DateTime lastRunDate)
        {
            //var log = new ImportLog();
            var localMarketSettings = this.SitecoreContext.GetItem<LocalMarketSettings>(siteId);

            var existingPublishedProductIds = new List<Guid>();

            var products = this.GetProductsFromRemoteSource(localMarketSettings);

            foreach (var currentProduct in products)
            {
                if (currentProduct.IsCatDog)
                {
                    currentProduct.IsDog = true;
                    currentProduct.Species = Species.Dogs;
                    currentProduct.Ages = currentProduct.Ages.Select(x =>
                        {
                            if (string.Equals(x.Name, "kitten", StringComparison.CurrentCultureIgnoreCase))
                            {
                                return new LifestageDto { Name = "puppy" };
                            }
                            else
                            {
                                return x;
                            }
                        }
                    ).ToArray();
                    Import(currentProduct, lastRunDate, existingPublishedProductIds);
                    currentProduct.IsDog = false;
                    currentProduct.Species = Species.Cats;
                    currentProduct.Ages = currentProduct.Ages.Select(x =>
                        {
                            if (string.Equals(x.Name, "puppy", StringComparison.CurrentCultureIgnoreCase))
                            {
                                return new LifestageDto { Name = "kitten" };
                            }
                            else
                            {
                                return x;
                            }
                        }
                    ).ToArray();
                    Import(currentProduct, lastRunDate, existingPublishedProductIds);

                }
                else
                {
                    Import(currentProduct, lastRunDate, existingPublishedProductIds);
                }


            }
            existingPublishedProductIds.AddRange(LogManagerLocalImport.GetValue().Log.Entries.Select(x => x.Id).Distinct());
            if (products != null && products.Any())
            {
                var unpublishedItemsLog = this.ImportedProductConstructionOrchestrator.UnpublishProductsExcept(existingPublishedProductIds).ToList();
                LogManagerLocalImport.AddItemsProcessed(unpublishedItemsLog.Count(x =>
                    x.Action == ImportAction.New || x.Action == ImportAction.Updated || x.Action == ImportAction.Deleted));
                LogManagerLocalImport.AddEntriesToLog(unpublishedItemsLog);
            }
            else
            {
                LogManagerLocalImport.AddEntryToLog(new ImportLogEntry { Level = MessageLevel.Info, Message = "No products were received, unpublish skipped" });
            }

            return;
        }

        private void Import(ImportedProductDto currentProduct, DateTime lastRunDate,
            List<Guid> existingPublishedProductIds)
        {
            var newLogEntries = new List<ImportLogEntry>();
            try
            {
                this.ImportedProductConstructionOrchestrator.CreateAndSaveNewProduct(currentProduct, lastRunDate);
                if (LogManagerLocalImport.GetValue().Log.Entries.Any(x =>
                    x.Action == ImportAction.New || x.Action == ImportAction.Updated ||
                    x.Action == ImportAction.Deleted))
                {
                    LogManagerLocalImport.AddItemsProcessed(1);
                }
            }
            catch (SkippableImportLogException e)
            {
                var importLogEntry = e.Entry;
                WriteToLog(importLogEntry);
                newLogEntries.Add(importLogEntry);
                if (e.Entry.Action == ImportAction.Skipped)
                {
                    existingPublishedProductIds.Add(e.Entry.Id);
                }
            }
            catch (Exception e)
            {
                var importLogEntry = new ImportLogEntry
                {
                    Level = MessageLevel.Error,
                    Message = $"{this.GetType()}: {e.GetAllMessages()} {e.StackTrace}"
                };
                LogManagerLocalImport.WriteToLog($"{currentProduct.ProductName} unhandled exception");
                WriteToLog(importLogEntry);
                newLogEntries.Add(importLogEntry);
            }

            LogManagerLocalImport.AddEntriesToLog(newLogEntries);
        }




        private void DeleteItems()
        {

        }


        private void WriteToLog(ImportLogEntry importLogEntry)
        {
            switch (importLogEntry.Level)
            {
                case MessageLevel.Error:
                    Sitecore.Diagnostics.Log.Error(importLogEntry.Message, this);
                    break;
                case MessageLevel.Critical:
                    Sitecore.Diagnostics.Log.Fatal(importLogEntry.Message, this);
                    break;
                case MessageLevel.Info:
                default:
                    Sitecore.Diagnostics.Log.Info(importLogEntry.Message, this);
                    break;
            }
        }

        private static country_products GetXmlFeedData(string url, string username, string password)
        {
            var xmlFeedDownloadController = new XmlFeedDownloadController<country_products>();
            return xmlFeedDownloadController.GetData(url, username, password);
        }

        protected static List<FileInfo> FindFiles(string pathToFiles, string searchPattern)
        {
            var files = Directory.GetFiles(pathToFiles, $"{searchPattern}")
                .Select(x => new FileInfo(x))
                .ToList();

            return files;
        }

        protected IEnumerable<Product> GetProductsFromSalsifyJsonFile(IList<FileInfo> files, LocalMarketSettings localMarketSettings)
        {
            if (!files.Any())
            {
                return new List<Product>();
            }

            var fileToProcess = files.First();
            LogManagerLocalImport.WriteToLog($"File {fileToProcess.Name} to be processed");
            var fileContent = File.ReadAllText(fileToProcess.FullName);

            if (!string.IsNullOrWhiteSpace(localMarketSettings?.ProductImportRegexModifier))
            {
                var regexList =
                    localMarketSettings.ProductImportRegexModifier.Split(new[] { "\r\n" },
                        StringSplitOptions.RemoveEmptyEntries);

                if (regexList.Length % 2 == 0)
                {
                    for (int i = 0; i < regexList.Length / 2; i++)
                    {
                        var regex = regexList[i];
                        var replacement = regexList[i + 1];

                        fileContent = Regex.Replace(fileContent, regex, replacement);
                    }
                }
            }

            var productsFromJson = SalsifyProductImport.FromJson(fileContent);
            if (productsFromJson == null)
            {
                return new List<Product>();
            }

            var mergedProductsFromJson = new List<Product>();
            foreach (var salsifyProductImport in productsFromJson)
            {
                if (salsifyProductImport?.Products == null)
                {
                    continue;
                }

                mergedProductsFromJson.AddRange(salsifyProductImport.Products);
            }

            return mergedProductsFromJson;
        }

        private IEnumerable<ImportedProductDto> GetProductsFromRemoteSource(LocalMarketSettings localMarketSettings)
        {
            if (localMarketSettings.IsWeShareImport)
            {
                var weShareProducts = this.GetProductsFromWeShareJson(localMarketSettings);
                return this.GetProductsDtoFromWeShareJson(weShareProducts, localMarketSettings) ?? new List<ImportedProductDto>();
            }
            else if (localMarketSettings.ProductImportFtpFilePattern.HasValue() && localMarketSettings.ProductImportFtpFilePattern.EndsWith("json"))
            {
                var productsJson = this.GetProductsFromJsonFileViaFtp(localMarketSettings);
                return this.GetProductsDtoFromJson(productsJson) ?? new List<ImportedProductDto>();
            }

            var productsXml = GetProductsFromLshRemoteXml(localMarketSettings);
            return this.GetProductsDtoFromXml(productsXml) ?? new List<ImportedProductDto>();
        }

        #region LSH Import


        private country_products GetProductsFromLshRemoteXml(LocalMarketSettings localMarketSettings)
        {
            if (localMarketSettings.ProductImportURL.HasValue())
            {
                LogManagerLocalImport.AddEntryToLog(new ImportLogEntry
                {
                    Level = MessageLevel.Info,
                    Message = $"Product import URL: {localMarketSettings.ProductImportURL}"
                });
                var products = GetXmlFeedData(localMarketSettings.ProductImportURL,
                    localMarketSettings.ProductImportBasicAuthUsername,
                    localMarketSettings.ProductImportBasicAuthPassword);
                LogManagerLocalImport.AddEntryToLog(new ImportLogEntry
                { Level = MessageLevel.Info, Message = $"{products.country_product.Length} products received" });

                return products;
            }

            LogManagerLocalImport.AddEntryToLog(new ImportLogEntry { Level = MessageLevel.Info, Message = "Product import URL is not specified for this site" });

            return new country_products();
        }

        private IEnumerable<ImportedProductDto> GetProductsDtoFromXml(
            country_products productsXml)
        {
            return this.Mapper.Map<IEnumerable<ImportedProductDto>>(productsXml.country_product);
        }

        #endregion

        #region Salsify import

        protected virtual IEnumerable<Product> GetProductsFromJsonFileViaFtp(LocalMarketSettings localMarketSettings)
        {
            //LogManagerLocalImport.AddEntryToLog(new ImportLogEntry { Level = MessageLevel.Info, Message = $"Product import Ftp File Pattern: {localMarketSettings.ProductImportFtpFilePattern}" });
            LogManagerLocalImport.WriteToLog($"Product import Ftp File Pattern: {localMarketSettings.ProductImportFtpFilePattern}");
            var globalConfiguration = this.SitecoreContext.GetItem<GlobalConfigurationFolder>(GlobalConfigurationFolderConstants.GlobalConfigurationFolderItemIdString);

            var remoteFileManager = new SftpFileManager(
                globalConfiguration.ProductImportSFTPHostname,
                globalConfiguration.ProductImportSFTPUsername,
                globalConfiguration.ProductImportSFTPPassword);

            var pathToFiles = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, localMarketSettings.ProductImportPathToFiles);
            if (!Directory.Exists(pathToFiles))
            {
                LogManagerLocalImport.AddEntryToLog(new ImportLogEntry { Level = MessageLevel.Info, Message = $"Local folder for Products has been created" });
                Directory.CreateDirectory(pathToFiles);
            }

            remoteFileManager.GetFiles(globalConfiguration.ProductImportSFTPPath, pathToFiles);
            var files = FindFiles(pathToFiles, localMarketSettings.ProductImportFtpFilePattern);

            files = files.OrderByDescending(x => x.CreationTime).ToList();

            var processedFileNames = files.Select(x => x.Name).ToList();
            var products = GetProductsFromSalsifyJsonFile(files, localMarketSettings);

            LogManagerLocalImport.WriteToLog($"Total received products - {products.Count()}");

            foreach (var file in files)
            {
                file.Delete();
            }

            remoteFileManager?.ArchiveFiles(globalConfiguration.ProductImportSFTPPath, globalConfiguration.ProductImportSFTPSavePath, processedFileNames);
            LogManagerLocalImport.AddEntryToLog(new ImportLogEntry { Level = MessageLevel.Info, Message = $"Archiving files on ftp has been finished" });

            return products;
        }

        private IEnumerable<ImportedProductDto> GetProductsDtoFromJson(IEnumerable<Product> productsJson)
        {
            var mergedProducts = new List<Product>();
            var groupedProducts = productsJson.GroupBy(p => p.SitecoreMainItemId);
            foreach (var groupedProduct in groupedProducts)
            {
                // Select the source of the images for product page (if there are multiple products with the same Main Item ID)
                var productDataSource = groupedProduct.FirstOrDefault(x => x.SitecoreImagesSource)
                                        ?? groupedProduct.FirstOrDefault();
                if (productDataSource == null) continue;

                foreach (var product in groupedProduct)
                {
                    if (!productDataSource.Packages.Any(x =>
                        x.SitecorePackSize == product.SitecorePackSize &&
                        x.SitecorePackUnit.SafeEquals(product.SitecorePackUnit)))
                    {
                        productDataSource.Packages.Add(product.CurrentPackaging);
                    }
                }

                mergedProducts.Add(productDataSource);
            }

            List<ImportedProductDto> result = new List<ImportedProductDto>();
            foreach (var product in mergedProducts)
            {
                try
                {
                    result.Add(this.Mapper.Map<ImportedProductDto>(product));
                }
                catch
                {
                }
            }
            return result;
        }

        #endregion

        #region WeShare Import
        private IEnumerable<ImportedProductDto> GetProductsDtoFromWeShareJson(IList<Hit> weShareProducts, LocalMarketSettings localMarketSettings)
        {
            List<ImportedProductDto> result = new List<ImportedProductDto>();
            foreach (var weShareProduct in weShareProducts)
            {
                try
                {
                    result.Add(this.Mapper.Map<ImportedProductDto>(weShareProduct, opt => opt.Items[LocalMarketSettingsConstants.TemplateName] = localMarketSettings));
                }
                catch
                {
                }
            }
            return result;
        }

        private IList<Hit> GetProductsFromWeShareJson(LocalMarketSettings localMarketSettings)
        {
            if (localMarketSettings.ProductImportURL.HasValue())
            {
                LogManagerLocalImport.AddEntryToLog(new ImportLogEntry
                {
                    Level = MessageLevel.Info,
                    Message = $"Product import URL: {localMarketSettings.ProductImportURL}"
                });

                WeShareResponse response = GetWeShareFeedData(localMarketSettings.ProductImportURL, localMarketSettings.ProductImportBasicAuthUsername,
                    localMarketSettings.ProductImportBasicAuthPassword,
                    "TheHub-Dev");

                LogManagerLocalImport.AddEntryToLog(new ImportLogEntry
                { Level = MessageLevel.Info, Message = $"{response.Products.Count} products received" });

                return response.Products;
            }

            LogManagerLocalImport.AddEntryToLog(new ImportLogEntry { Level = MessageLevel.Info, Message = "Product import URL is not specified for this site" });

            return new List<Hit>();
        }

        protected virtual WeShareResponse GetWeShareFeedData(string productImportUrl, string authUsername, string authPassword, string sourceType)
        {
            //todo: move to separate controller
            //var weShareFeedDownloadController = new WeShareFeedDownloadController<WeShareResponse>();
            //return weShareFeedDownloadController.GetData(productImportUrl);
            using (var httpClient = new HttpClient(new HttpClientHandler
            {
                AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
            }))
            using (var requestMessage = new HttpRequestMessage(HttpMethod.Get, productImportUrl))
            {
                httpClient.Timeout = new TimeSpan(0, 0, 2, 0);
                requestMessage.Headers.Add("client_id", authUsername);
                requestMessage.Headers.Add("client_secret", authPassword);
                requestMessage.Headers.Add("sourceType", sourceType);
                var response =
                    httpClient.SendAsync(requestMessage).GetAwaiter().GetResult();
                var json = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                return WeShareResponse.FromJson(json);
            }
        }

        #endregion
    }
}