﻿using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.ItemImport.Models;
using Delete.Foundation.ItemImport.Repositories;
using Delete.Foundation.MultiSite.Models;
using Delete.Foundation.MultiSite.Services;
using Glass.Mapper;
using Glass.Mapper.Sc;
using Microsoft.Extensions.DependencyInjection;
using RoyalCanin.Project.ProductImport.Processors;
using RoyalCanin.Project.RoyalCanin.Models;
using Sitecore.Data.Managers;
using Sitecore.DependencyInjection;
using Sitecore.Globalization;
using Sitecore.Mvc.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RoyalCanin.Project.ProductImport.Orchestrators
{
    public interface IImportedProductConstructionOrchestrator
    {
        IList<ImportLogEntry> CreateAndSaveNewProduct(ImportedProductDto product, DateTime lastImportRun);

        IList<ImportLogEntry> UnpublishProductsExcept(IList<Guid> importedProducts);
    }

    public class ImportedProductConstructionOrchestrator : IImportedProductConstructionOrchestrator
    {
        protected readonly ILocalProductItemProcessor LocalProductItemProcessor;
        protected readonly ILocalMarketSettings MarketSettings;
        protected readonly IGenericSitecoreItemRepository<ProductPage> GenericItemRepository;
        protected readonly IGlobalProductValidation GlobalProductValidation;

        public ImportedProductConstructionOrchestrator(
            ISitecoreContext sitecoreContext,
            ILocalProductItemProcessor localProductItemProcessor,
            ILocalMarketSettings marketSettings,
            IGlobalProductValidation globalProductValidation)
        {
            this.GlobalProductValidation = globalProductValidation;
            this.LocalProductItemProcessor = localProductItemProcessor;
            this.MarketSettings = marketSettings;
            var searchManager = ServiceLocator.ServiceProvider.GetService<IBaseSearchManager<ProductPage>>();
            this.GenericItemRepository = new GenericSitecoreItemRepository<ProductPage>(sitecoreContext, searchManager);
        }

        public IList<ImportLogEntry> CreateAndSaveNewProduct(ImportedProductDto product, DateTime lastImportRun)
        {
            var log = new List<ImportLogEntry>();

            var entry = ValidateProductStatus(product, lastImportRun);

            if (entry != null)
            {
                throw new SkippableImportLogException
                {
                    Entry = entry
                };
            }

            this.LocalProductItemProcessor.ProcessItem(product,
                new List<Language> { LanguageManager.GetLanguage(SiteContextResolutionService.CurrentContext.Language) });

            return log;
        }

        public IList<ImportLogEntry> UnpublishProductsExcept(IList<Guid> importedProducts)
        {
            var existsingProducts = this.LocalProductItemProcessor.GetImportedProducts(this.MarketSettings.Fullpath);
            var missingProducts = existsingProducts.Where(x => !importedProducts.Contains(x.Id)).ToList();
            var logs = new List<ImportLogEntry>();

            foreach (var missingProduct in missingProducts)
            {
                if (missingProduct.NeverPublish) continue;
                missingProduct.NeverPublish = true;
                this.GenericItemRepository.Save(missingProduct);

                logs.Add(new ImportLogEntry
                {
                    Action = ImportAction.Deleted,
                    Level = MessageLevel.Info,
                    Message = $"{missingProduct.ProductName} ({missingProduct.GlobalProduct?.MainItemID})"
                });
            }

            return logs;
        }

        protected ImportLogEntry ValidateProductStatus(ImportedProductDto importedProduct, DateTime lastImportRun)
        {

            switch (importedProduct.ProductStatus)
            {
                case 0:
                    return new ImportLogEntry
                    {
                        Action = ImportAction.Rejected,
                        Level = MessageLevel.Info,
                        Message = $"{importedProduct.ProductName} ({importedProduct.MainItemId}) rejected: product status is not ACTIVE"
                    };
                case 2:
                    return new ImportLogEntry
                    {
                        Action = ImportAction.Skipped,
                        Level = MessageLevel.Info,
                        Message = $"{importedProduct.ProductName} ({importedProduct.MainItemId}) rejected: product is in renovation"
                    };
            }

            ImportLogEntry Entry = null;

            var reasonMsg = string.Empty;

            if (!importedProduct.MainItemId.HasValue())
            {
                reasonMsg = "product has empty MainItemId";
            }

            if (importedProduct.ProductName.IsEmpty())
            {
                reasonMsg = "product name is empty";
            }

            if (importedProduct.UpdatedAt < lastImportRun)
            {
                reasonMsg = $"product update date is {importedProduct.UpdatedAt}";
            }

            if (!LocalProductItemProcessor.ValidateProductRootPath(importedProduct))
            {
                reasonMsg = $"no matching product catalogue root for Species=\"{importedProduct.Species}\", Pillar=\"{importedProduct.Pillar?.Code}\"";
            }

            if (!this.GlobalProductValidation.CheckGlobalProductExist(importedProduct))
            {
                reasonMsg =
                    $"{importedProduct.ProductName} ({importedProduct.MainItemId}) skipped: Global product with Main Item ID ({importedProduct.MainItemId}) doesn't exist";

            }

            if (!reasonMsg.IsEmpty())
            {
                Entry = new ImportLogEntry
                {
                    Action = ImportAction.Rejected,
                    Level = MessageLevel.Info,
                    Message = $"{importedProduct.ProductName} ({importedProduct.MainItemId}) rejected: {reasonMsg}"
                };
            }

            return Entry;
        }

    }
}