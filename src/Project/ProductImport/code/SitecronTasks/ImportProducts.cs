﻿using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.DeleteFoundationCore.Managers;
using Delete.Foundation.ItemImport.Importers;
using Delete.Foundation.ItemImport.LogManagerImport;
using Delete.Foundation.ItemImport.Models;
using Delete.Foundation.ItemImport.Services;
using Delete.Foundation.MultiSite.Helpers;
using Delete.Foundation.MultiSite.Models;
using Glass.Mapper.Sc;
using Microsoft.Extensions.DependencyInjection;
using Quartz;
using RoyalCanin.Project.ProductImport.Importers;
using RoyalCanin.Project.ProductImport.Models;
using RoyalCanin.Project.RoyalCanin.Models;
using Sitecore.ContentSearch;
using Sitecore.Data;
using Sitecore.Data.Events;
using Sitecore.DependencyInjection;
using Sitecore.Jobs;
using Sitecore.SecurityModel;
using Sitecore.Sites;
using Sitecron.SitecronSettings;
using System;
using System.Collections.Generic;
using System.Linq;
using ILog = log4net.ILog;
using LogManager = log4net.LogManager;
using Constants = Delete.Foundation.DeleteFoundationCore.Constants.Constants;
using System.Net.Mail;
using System.Text;
using Sitecore.Globalization;


namespace RoyalCanin.Project.ProductImport.SitecronTasks
{
    using Delete.Foundation.ItemImport.Loggers;

    public class ImportProducts : IJob
    {
        public const string ProductImportLoggerName = "RoyalCanin.Project.ProductImport";

        private readonly ILog logger = LogManager.GetLogger(ProductImportLoggerName);

        private readonly IImportJobLogger importJobLogger;

        private ICacheManager cacheManager;


        public ImportProducts()
        {
            this.importJobLogger = new ImportJobLogger(logger);
            this.cacheManager = new CacheManager();
        }

        public ImportProducts(IImportJobLogger logger, ICacheManager cacheManager)
        {
            this.importJobLogger = logger;
            this.cacheManager = cacheManager;
        }

        private void AddProducts(Database db, Sitecore.Install.Items.ExplicitItemSource source, string country, string spices, string pillar)
        {
            Sitecore.Data.Items.Item[] backupItems = db.Items.Database.SelectItems($"fast://sitecore/content/Royal Canin/{country}/Home/{spices}/Products/{pillar} Products//*");
            source.Entries.Add(new Sitecore.Install.Items.ItemReference(db.Items.Database.GetItem($"sitecore/content/Royal Canin/{country}/Home/{spices}/Products/{pillar} Products").Uri, false).ToString());
            foreach (Sitecore.Data.Items.Item item in backupItems)
            {
                source.Entries.Add(new Sitecore.Install.Items.ItemReference(item.Uri, false).ToString());
            }
        }

        protected virtual void Backup(Database db, string country)
        {
            Sitecore.Install.PackageProject document = new Sitecore.Install.PackageProject();

            var packageName = $"{country}Products";

            document.Metadata.PackageName = packageName;
            document.Metadata.Author = "Application Management Team";

            Sitecore.Install.Items.ExplicitItemSource source = new Sitecore.Install.Items.ExplicitItemSource(); //Create source – source should be based on BaseSource  
            source.Name = packageName;

            AddProducts(db, source, country, "Cats", "Retail");
            AddProducts(db, source, country, "Cats", "Vet");
            AddProducts(db, source, country, "Dogs", "Retail");
            AddProducts(db, source, country, "Dogs", "Vet");

            document.Sources.Add(source);
            document.SaveProject = true;

            string filePath = Sitecore.Configuration.Settings.DataFolder + "/packages/" + $"{country}_products_{DateTime.Today.ToString("yyyyMMdd")}" + ".zip";

            //path where the zip file package is saved  
            using (Sitecore.Install.Zip.PackageWriter writer = new Sitecore.Install.Zip.PackageWriter(filePath))
            {
                Sitecore.Context.SetActiveSite("shell");
                writer.Initialize(Sitecore.Install.Installer.CreateInstallationContext());
                Sitecore.Install.PackageGenerator.GeneratePackage(document, writer);
                Sitecore.Context.SetActiveSite("website");
            }
        }

        public void Execute(IJobExecutionContext context)
        {
            var jobDataMap = context.JobDetail.JobDataMap;
            var items = jobDataMap.GetString(SitecronConstants.FieldNames.Items);
            var siteRoots = items.ToGuidList("|");
            var lastRunDate = jobDataMap.GetDateTimeValue(SitecronConstants.FieldNames.LastRunUTC);

            var importSuccessful = false;


            if (siteRoots.Any())
            {
                this.logger.Info($"ImportProducts job started");

                ClearCache("start");
                foreach (var siteRoot in siteRoots)
                {
                    try
                    {
                        var rootPath = Database.GetDatabase(Constants.Sitecore.Databases.Master)?.GetItem(new ID(siteRoot))?.Paths.FullPath;
                        var sites = SiteManagerHelper.GetWebsitesByRoot(rootPath);

                        List<MailAddress> reportEmails = new List<MailAddress>();
                        ImportLog aggregateLog = new ImportLog();
                        var market = new ProductImportGeoData
                        {
                            MarketName = Database.GetDatabase(Constants.Sitecore.Databases.Master)
                                ?.GetItem(new ID(siteRoot))?.Name
                        };

                        foreach (var site in sites)
                        {
                            var itemsToPublish = new List<Guid>();
                            var siteContext = SiteContext.GetSite(site.Name);
                            var language = siteContext.Language;
                            market.Languages.Add(language);

                            ClearCache($"start [{language}]");

                            this.logger.Info($"Importing products for site RootPath: {siteContext.RootPath}; Language: {language}; Database: {siteContext.Database}");

                            using (new SecurityDisabler())
                            using (new SiteContextSwitcher(siteContext))
                            using (new Sitecore.Globalization.LanguageSwitcher(language))
                            using (new EventDisabler())
                            using (new BulkUpdateContext())
                            using (var innerScope = ServiceLocator.ServiceProvider
                                .GetRequiredService<IServiceScopeFactory>().CreateScope())
                            {
                                var sitecoreContext = innerScope.ServiceProvider.GetService<ISitecoreContext>();
                                var productImporter = innerScope.ServiceProvider.GetService<IProductImporter>();
                                var LogManagerImport = innerScope.ServiceProvider.GetService<ILogManagerImport>();
                                LogManagerImport.InitLogger(ProductImportLoggerName);

                                Backup(siteContext.Database, siteContext.ContentStartPath.Split('/').Last());

                                productImporter.Run(siteRoot, lastRunDate);
                                var logs = LogManagerImport.GetValue().Log;

                                itemsToPublish.AddRange(this.GetProductRootItemsBySiteRoot(sitecoreContext, siteContext, siteRoots));
                                var productMediaFolder = this.GetProductMediaLibraryItemsBySiteRoot(sitecoreContext, siteContext, siteRoots);
                                if (productMediaFolder.HasValue)
                                {
                                    itemsToPublish.Add(productMediaFolder.Value);
                                }

                                this.importJobLogger.LogImportResults(logs.Entries);
                                if (logs.ItemsProcessed > 0)
                                {
                                    //RebuildIndexes(new List<string> {sitecoreContext.Database.Name});
                                    RefreshIndexes(itemsToPublish, sitecoreContext);
                                    importSuccessful = true;
                                }

                                aggregateLog.Entries.AddRange(logs.Entries);
                                aggregateLog.ItemsProcessed += logs.ItemsProcessed;
                                var localMarketSettings =
                                    sitecoreContext.GetItem<LocalMarketSettings>(siteContext.RootPath);

                                if (!localMarketSettings.RecipientEmailAddress.IsNullOrEmpty())
                                {
                                    var recpEmail = new MailAddress(localMarketSettings.RecipientEmailAddress);
                                    reportEmails.Add(recpEmail);
                                }
                            }

                            using (new SecurityDisabler())
                            using (new SiteContextSwitcher(siteContext))
                            using (new Sitecore.Globalization.LanguageSwitcher(siteContext.Language))
                            {
                                PublishItems(itemsToPublish);
                            }
                        }

                        this.SendImportReportEmail(reportEmails, aggregateLog.Entries, market, importSuccessful);
                    }
                    catch (Exception e)
                    {
                        this.logger.Error(e.Message, e);
                    }
                }

                this.logger.Info($"ImportProducts job finished");
                this.logger.Info("");

            }
            else
            {
                this.logger.Info($"ImportProducts job skipped because no sites are selected");
            }
            ClearCache("end");
        }

        private void ClearCache(string position)
        {
            try
            {
                if (this.cacheManager == null)
                {
                    throw new NullReferenceException();
                }
                this.cacheManager.Clear();
                logger.Info($"Sitecron - Job {nameof(ImportProducts)} - cache clearning succeed with {nameof(ImportProducts)} on {position}");
            }
            catch (Exception ex)
            {
                logger.Error($"Sitecron - Job {nameof(ImportProducts)} - cache clearning error with {nameof(ImportProducts)} on {position}. Exception: {ex.Message}");
            }
        }

        private void SendImportReportEmail(List<MailAddress> reportEmails, IList<ImportLogEntry> importResults, ProductImportGeoData importMarket,
            bool importSuccessful)
        {
            var msgBody = new StringBuilder();
            msgBody.AppendLine($"Hello {importMarket.MarketName} market team,");
            msgBody.AppendLine("");
            msgBody.AppendLine($"Please note that a product import has been completed today {DateTime.Now:dd/MMM/yyyy} for {importMarket.MarketName}").AppendLine();

            if (!importSuccessful)
            {
                msgBody.AppendLine("Import failed.").AppendLine();
                ImportLogEntry error = null;
                try
                {
                    error = importResults.LastOrDefault(x =>
                        x.Level == MessageLevel.Error || x.Level == MessageLevel.Critical);
                }
                catch
                {
                    // ignored
                }

                if (error != null)
                {
                    msgBody.AppendLine("Please review the information about the last error below:").AppendLine();

                    msgBody.AppendLine($"Item id = {error.Id}");
                    msgBody.AppendLine($"Message = {error.Message}");
                }
            }
            else
            {
                var deleted = importResults.Where(x => x.Action == ImportAction.Deleted).ToList();
                var newItem = importResults.Where(x => x.Action == ImportAction.New).ToList();
                var updated = importResults.Where(x => x.Action == ImportAction.Updated).ToList();
                var newImages = importResults.Where(x => x.Action == ImportAction.NewImage).ToList();
                var updatesImages = importResults.Where(x => x.Action == ImportAction.UpdatedImage).ToList();




                msgBody.AppendLine($"New products added <{newItem.Count}>");
                msgBody.AppendLine($"Products unpublished <{deleted.Count}>");
                msgBody.AppendLine($"Products updated <{updated.Count}>");
                msgBody.AppendLine("");


                var deletedProducts = String.Join($"{System.Environment.NewLine}", importResults.Where(x => (x.Action == ImportAction.Deleted)).Select(x => x.Message));
                if (deletedProducts.Length > 0)
                {
                    msgBody.AppendLine("Deleted products:");
                    msgBody.AppendLine(deletedProducts);
                }

                msgBody.AppendLine("Rejected products:");
                msgBody.AppendLine(String.Join($"{System.Environment.NewLine}", importResults.Where(x => (x.Action == ImportAction.Rejected) && (x.Message.Contains("Pillar"))).Select(x => x.Message)));
                msgBody.AppendLine(String.Join($"{System.Environment.NewLine}", importResults.Where(x => (x.Action == ImportAction.Rejected) && (!x.Message.Contains("Pillar"))).Select(x => x.Message)));

                msgBody.AppendLine("");

                msgBody.AppendLine("Generic rejection scenarios and recommended actions:");
                msgBody.AppendLine("    Only VET and RETAIL pillars are currently supported in SiteCore, meaning that any Pillar PRO product will fail to import. The Account Managers/Market Activators should be able to clarify any question on the pillar PRO products and associated release timeline.");
                msgBody.AppendLine("    Any product set under an incorrect Global Product ID in WeShare will fail the import. For some items you can check the suggested IDs or seek WeShare's team support, them being responsible of the Global Product IDs.");
                msgBody.AppendLine("    Should new Global Product IDs be required, the WeShare team must sign them off first. Once you get the new Global Product IDs validated by the WeShare team, we can add them to SiteCore. Please keep the Account Managers/Market Activators in the loop.");
                msgBody.AppendLine("");
                msgBody.AppendLine("Should you need any other assistance, please contact us at hub@royalcanin.zendesk.com");


                msgBody.AppendLine();

                msgBody.AppendLine("Thank you,");
                msgBody.AppendLine("Application Management Team");
            }
            //NEVER USED
            //var environmentName = System.Web.Hosting.HostingEnvironment.ApplicationHost.GetSiteName();

            MailMessage message = new MailMessage()
            {
                Subject = $"The Hub Website {importMarket} // Product Import for {DateTime.Now:dd/MMM/yyyy}",
                From = new MailAddress(Sitecore.Configuration.Settings.GetSetting("Products.ProductImport.FromReportEmail")),
                Body = msgBody.ToString(),
                IsBodyHtml = false
            };

            foreach (var mailAddress in reportEmails)
            {
                message.To.Add(mailAddress);
            }

            this.logger.Info("Sending mail");
            this.logger.Info("----------------------------------------------------------------------------");
            this.logger.Info(msgBody.ToString());

            Sitecore.MainUtil.SendMail(message);

            return;
        }

        private IEnumerable<Guid> GetProductRootItemsBySiteRoot(ISitecoreContext sitecoreContext, SiteContext siteContext, IList<Guid> siteRoots)
        {
            var localMarketSettings = sitecoreContext.GetItem<LocalMarketSettings>(siteContext.RootPath);
            if (localMarketSettings == null || !siteRoots.Contains(localMarketSettings.Id))
            {
                this.logger.Warn($"Sitecron - Job {nameof(ImportProducts)} - Local Market Settings not found");
                return null;
            }

            return localMarketSettings.ProductRootItems.Select(x => x.Id);
        }

        private Guid? GetProductMediaLibraryItemsBySiteRoot(ISitecoreContext sitecoreContext, SiteContext siteContext, IList<Guid> siteRoots)
        {
            var localMarketSettings = sitecoreContext.GetItem<LocalMarketSettings>(siteContext.RootPath);
            if (localMarketSettings == null || !siteRoots.Contains(localMarketSettings.Id))
            {
                this.logger.Warn($"Sitecron - Job {nameof(ImportProducts)} - Local Market Settings not found");
                return null;
            }

            var websiteMediaFolder = sitecoreContext.GetItem<MediaFolder>(localMarketSettings.MediaFolder);
            if (websiteMediaFolder == null)
            {
                this.logger.Warn($"Sitecron - Job {nameof(ImportProducts)} - Media Folder not found");
                return null;
            }

            var mediaItemService = new MediaItemService(sitecoreContext, localMarketSettings, ServiceLocator.ServiceProvider.GetService<ILogManagerImport>());
            var imagesFolder = mediaItemService.GetOrCreateMediaFolder(websiteMediaFolder, "Images");
            var productsFolder = mediaItemService.GetOrCreateMediaFolder(imagesFolder, "Products");

            return productsFolder.Id;
        }

        /// <summary>
        /// nameMethod - PublishItems for publish items or RebuildIndexes - for rebuild indexes
        /// </summary>
        /// <param name="roots"></param>
        /// <param name="siteName"></param>
        /// <param name="nameMethod"></param>
        private void JobForPublish(object roots, string siteName, string nameMethod)
        {
            var options = new JobOptions($"{nameMethod} ImportProducts for {siteName}",
                "SmartPublishMethod",
                siteName,
                this,
                nameMethod,
                new object[] { roots, $"PublishImportProducts for {siteName}" });
            JobManager.Start(options);
        }

        private void RebuildIndexes(IEnumerable<string> databaseNames)
        {
            foreach (var databaseName in databaseNames.Distinct())
            {
                this.logger.Info($"Sitecron - Job ImportProducts - rebuilding index start");
                var targetIndexName = SearchManagersConfiguration.GetIndexNameByType(typeof(ProductPage), databaseName);
                this.logger.Info($"Sitecron - Job ImportProducts - {targetIndexName} - target index name");
                var index = ContentSearchManager.GetIndex(targetIndexName);

                index.Rebuild(IndexingOptions.Default);
                this.logger.Info($"Sitecron - Job ImportProducts - {targetIndexName} - commit transactions");
                index.CreateUpdateContext().Commit();
                this.logger.Info($"Sitecron - Job ImportProducts - rebuilding index finished");
                this.logger.Info("");
            }
        }

        //todo: move indexing-related code to a separate class, also from ImportStockists and GlobalBreedEventHandlerBase
        protected virtual void RefreshIndexes(List<Guid> itemsToPublish, ISitecoreContext sitecoreContext)
        {
            this.logger.Info($"Sitecron - Job ImportProducts - refreshing index start");
            var targetIndexName =
                SearchManagersConfiguration.GetIndexNameByType(typeof(ProductPage), sitecoreContext.Database.Name);
            this.logger.Info($"Sitecron - Job ImportProducts - {targetIndexName} - target index name");

            var index = ContentSearchManager.GetIndex(targetIndexName);
            foreach (var itemGuid in itemsToPublish)
            {
                var startingPointItem = sitecoreContext.Database.GetItem(new ID(itemGuid));
                var indexableStartingPoint = new SitecoreIndexableItem(startingPointItem);
                this.logger.Info($"Sitecron - Job ImportProducts - running refresh for {startingPointItem.Paths.FullPath}");

                index.Refresh(indexableStartingPoint, IndexingOptions.Default);
                this.logger.Info($"Sitecron - Job ImportProducts - refresh done for {startingPointItem.Paths.FullPath}");
            }

            this.logger.Info($"Sitecron - Job ImportProducts - {targetIndexName} - commit transactions");
            index.CreateUpdateContext().Commit();
            this.logger.Info($"Sitecron - Job ImportProducts - refreshing index finished");
            this.logger.Info("");

        }

        private void PublishItems(IList<Guid> roots)
        {
            this.logger.Info($"Sitecron - Job ImportProducts - publishing start");
            try
            {
                using (new DatabaseSwitcher(Database.GetDatabase(Constants.Sitecore.Databases.Master)))
                using (var innerScope = ServiceLocator.ServiceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope())
                {
                    var importer = innerScope.ServiceProvider.GetService<IImporter>();

                    if (importer == null)
                    {
                        this.logger.Warn($"Sitecron - Job ImportProducts - nullable importer - items hasn't been published");
                        return;
                    }

                    if (roots == null || !roots.Any())
                    {
                        this.logger.Warn($"Sitecron - Job ImportProducts - empty roots - items hasn't been published");
                        return;
                    }

                    foreach (var root in roots.Where(x => x != Guid.Empty))
                    {
                        var rootLogs = importer.Publish(root);
                        this.importJobLogger.LogImportResults(rootLogs);
                    }
                }
            }
            catch (Exception exception)
            {
                this.logger.Error($"Sitecron - Job ImportProducts - publishing error", exception);
            }

            this.logger.Info($"Sitecron - Job ImportProducts - publishing end");
        }
    }
}