﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Castle.Core.Internal;

namespace RoyalCanin.Project.ProductImport.Models
{
    public class ProductImportGeoData
    {
        public string MarketName { get; set; }
        public List<string> Languages { get; set; }

        public ProductImportGeoData()
        {
            Languages = new List<string>();
        }

        public override string ToString()
        {
            if (Languages.IsNullOrEmpty())
            {
                return MarketName;
            }
            var langList = String.Join(", ", Languages);
            return $"{MarketName} ({langList})";
        }
    }
}