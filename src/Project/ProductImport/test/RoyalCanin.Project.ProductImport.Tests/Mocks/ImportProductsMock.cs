﻿using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.ItemImport.Loggers;
using Glass.Mapper.Sc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sitecore.Data;

namespace RoyalCanin.Project.ProductImport.Tests
{
    class ImportProductsMock : SitecronTasks.ImportProducts
    {
        public ImportProductsMock(IImportJobLogger logger, ICacheManager cacheManager) : base(logger, cacheManager)
        {

        }

        protected override void RefreshIndexes(List<Guid> itemsToPublish, ISitecoreContext sitecoreContext)
        {

        }

        protected override void Backup(Database db, string country)
        {
        }
    }
}
