﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Delete.Feature.Products.Import.Processors;
using Delete.Foundation.DataTemplates.Models.Taxonomy;
using Delete.Foundation.ItemImport.ImportProcessors;
using Delete.Foundation.ItemImport.LogManagerImport;
using Delete.Foundation.ItemImport.Models;
using Delete.Foundation.ItemImport.Services;
using Delete.Foundation.MultiSite.Models;
using Glass.Mapper.Sc;
using RoyalCanin.Project.ProductImport.Processors;
using RoyalCanin.Project.RoyalCanin.Models;
using Sitecore.Globalization;

namespace RoyalCanin.Project.ProductImport.Tests
{
    class LocalProductItemProcessorUpdateMock : LocalProductItemProcessor
    {
        public LocalProductItemProcessorUpdateMock(
            ISitecoreContext sitecoreContext,
            ILocalMarketSettings localMarketSettings,
            IPillarIdItemProcessor pillarIdItemProcessor,
            Func<Species, IGlobalProductItemProcessor> globalProductItemServiceAccessor,
            IPackagingSizeItemProcessor packagingSizeItemProcessor,
            IMediaItemItemProcessor mediaItemItemProcessor,
            IMediaItemService mediaItemService,
            Func<Species, ISpecialNeedItemProcessor> specialNeedServiceAccessor,
            Func<Species, IIsSterilisedItemProcessor> isSterilisedServiceAccessor,
            Func<Species, IIndicationItemProcessor> indicationServiceAccessor,
            Func<Species, ICounterIndicationItemProcessor> counterIndicationServiceAccessor,
            IProductTypeItemProcessor productTypeProcessor,
            ILogManagerImport logManagerImport,
            IMapper mapper,
            string locationPathOverride = null) : base(sitecoreContext, localMarketSettings, pillarIdItemProcessor,
            globalProductItemServiceAccessor, packagingSizeItemProcessor, mediaItemItemProcessor,
            mediaItemService, specialNeedServiceAccessor, isSterilisedServiceAccessor, indicationServiceAccessor,
            counterIndicationServiceAccessor, productTypeProcessor, logManagerImport, mapper, locationPathOverride)
        {
        }

        public override ProductPage GetItem(
            string targetIdString,
            ImportedProductDto importObj,
            string locationOverride = null,
            Language language = null,
            Func<RoyalCanin.Models.ProductPage, bool> defaultItemSelector = null)
        {
            return null;
        }

        protected override ProductPage CreateItem(ImportedProductDto importObj, string itemLocationOverride = null,
            Language language = null)
        {
            var newItemName = this.ProposeSitecoreItemName(this.ItemNameFromImportObj(importObj));

            var productPage = new ProductPage();
            productPage.Name = newItemName;
            productPage.SetID(Guid.NewGuid());

            return productPage;
        }

        protected override ProductPage MapDefaultVersionFields(ProductPage item, ImportedProductDto importObj)
        {
            return item;
        }

        protected override ProductPage SaveItem(ProductPage updatedItem)
        {
            return updatedItem;
        }

        protected override ProductPage MapLanguageVersionFields(ProductPage item, ImportedProductDto importObj, IEnumerable<Language> languages)
        {
            throw new SkippableImportLogException
            {
                Entry = new ImportLogEntry
                {
                    Action = ImportAction.Skipped,
                    Level = MessageLevel.Info,
                    Message = $"{importObj.ProductName} ({importObj.MainItemId}) skipped: received product is not newer than existing, received: {importObj.UpdatedAt}, existing: {item.Updated}",
                    Id = item.Id
                }
            };
        }
    }
}
