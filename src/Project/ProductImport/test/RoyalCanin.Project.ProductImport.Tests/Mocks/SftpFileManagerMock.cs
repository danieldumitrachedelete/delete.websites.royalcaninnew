﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Delete.Foundation.DeleteFoundationCore.Managers;

namespace RoyalCanin.Project.ProductImport.Tests
{
    public class SftpFileManagerMock : SftpFileManager
    {
        public SftpFileManagerMock(string hostname, string username, string password) : base(hostname, username, password)
        {

        }

        public override void ArchiveFiles(string remotePathFrom, string remotePathTo, IList<string> filenames)
        {
        }
    }
}
