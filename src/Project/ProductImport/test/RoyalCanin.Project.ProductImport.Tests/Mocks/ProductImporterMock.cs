﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.ItemImport.LogManagerImport;
using Delete.Foundation.ItemImport.Models.WeShareImportFeed;
using Glass.Mapper.Sc;
using RoyalCanin.Project.ProductImport.Importers;
using RoyalCanin.Project.ProductImport.Orchestrators;
using RoyalCanin.Project.ProductImport.Processors;
using Delete.Foundation.DeleteFoundationCore.Managers;
using Delete.Foundation.ItemImport.Models;
using Delete.Foundation.ItemImport.Models.SalsifyImportFeed;
using Delete.Foundation.MultiSite.Models;

namespace RoyalCanin.Project.ProductImport.Tests
{
    public class ProductImporterMock : ProductImporter
    {
        public const string AustraliaSource = @"D:\Websites\RoyalCanin\Latest\src\Project\ProductImport\test\RoyalCanin.Project.ProductImport.Tests\Configs\AustraliaWeShare.json";
        public const string AustraliaErrorsSource = @"D:\Websites\RoyalCanin\Latest\src\Project\ProductImport\test\RoyalCanin.Project.ProductImport.Tests\Configs\AustraliaWeShareErrors.json";

        private string SourcePath;

        public ProductImporterMock(
            ISitecoreContext sitecoreContext,
            IMapper mapper,
            ICacheManager cacheManager,
            ILogManagerImport LogManagerImport,
            IImportedProductConstructionOrchestrator importedProductConstructionOrchestrator,
            ILocalProductItemProcessor localProductItemProcessor, string sourcePath) : base(sitecoreContext, mapper, cacheManager,
            LogManagerImport, importedProductConstructionOrchestrator, localProductItemProcessor)
        {
            SourcePath = sourcePath;
        }

        protected override WeShareResponse GetWeShareFeedData(string productImportUrl, string authUsername,
            string authPassword, string sourceType)
        {
            return WeShareResponse.FromJson(File.ReadAllText(SourcePath));
        }

        protected override IEnumerable<Product> GetProductsFromJsonFileViaFtp(LocalMarketSettings localMarketSettings)
        {
            //LogManagerLocalImport.AddEntryToLog(new ImportLogEntry { Level = MessageLevel.Info, Message = $"Product import Ftp File Pattern: {localMarketSettings.ProductImportFtpFilePattern}" });
            LogManagerLocalImport.WriteToLog($"Product import Ftp File Pattern: {localMarketSettings.ProductImportFtpFilePattern}");
            var globalConfiguration = this.SitecoreContext.GetItem<GlobalConfigurationFolder>(GlobalConfigurationFolderConstants.GlobalConfigurationFolderItemIdString);

            var remoteFileManager = new SftpFileManagerMock(
                globalConfiguration.ProductImportSFTPHostname,
                globalConfiguration.ProductImportSFTPUsername,
                globalConfiguration.ProductImportSFTPPassword);

            var pathToFiles = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, localMarketSettings.ProductImportPathToFiles);
            if (!Directory.Exists(pathToFiles))
            {
                LogManagerLocalImport.AddEntryToLog(new ImportLogEntry { Level = MessageLevel.Info, Message = $"Local folder for Products has been created" });
                Directory.CreateDirectory(pathToFiles);
            }

            remoteFileManager.GetFiles(globalConfiguration.ProductImportSFTPPath, pathToFiles);
            var files = FindFiles(pathToFiles, localMarketSettings.ProductImportFtpFilePattern);

            files = files.OrderByDescending(x => x.CreationTime).ToList();

            var processedFileNames = files.Select(x => x.Name).ToList();
             var products = GetProductsFromSalsifyJsonFile(files, localMarketSettings);

            LogManagerLocalImport.WriteToLog($"Total received products - {products.Count()}");

            foreach (var file in files)
            {
                file.Delete();
            }

            remoteFileManager?.ArchiveFiles(globalConfiguration.ProductImportSFTPPath, globalConfiguration.ProductImportSFTPSavePath, processedFileNames);
            LogManagerLocalImport.AddEntryToLog(new ImportLogEntry { Level = MessageLevel.Info, Message = $"Archiving files on ftp has been finished" });

            return products;
        }
    }
}
