﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Delete.Foundation.ItemImport.Models;
using Delete.Foundation.ItemImport.Models.WeShareImportFeed;
using Delete.Foundation.ItemImport.Profiles;

namespace RoyalCanin.Project.ProductImport.Tests
{
    class ItemImportProfileMock : ItemImportProfile
    {
        public ItemImportProfileMock() : base() {
        }

        protected override ImageDto GetImageFromWeShareRelation(IList<int> imageCode, int n = 0)
        {
            return null;
        }

        protected override string GetPropertyFromPacks(IList<Pack> packs, Func<Pack, string> func)
        {
            return string.Empty;
        }
    }
}
