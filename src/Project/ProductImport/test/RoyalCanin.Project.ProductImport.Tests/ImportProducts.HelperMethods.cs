﻿using System;
using System.Collections.Generic;
using AutoMapper;
using NUnit.Framework;
using Quartz;
using NSubstitute;
using Delete.Foundation.ItemImport.Loggers;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.ItemImport.LogContainer;
using Delete.Foundation.ItemImport.LogManagerImport;
using Delete.Foundation.MultiSite.Models;
using Glass.Mapper.Sc;
using Microsoft.Extensions.DependencyInjection;
using RoyalCanin.Project.ProductImport.Importers;
using RoyalCanin.Project.ProductImport.Orchestrators;
using RoyalCanin.Project.ProductImport.Processors;
using Sitecore.Data;
using Sitecore.DependencyInjection;
using Delete.Feature.Products.Import.Processors;
using Delete.Foundation.DataTemplates.Models.Taxonomy;
using Delete.Feature.Breeds.Profiles;
using Delete.Feature.Articles.Profiles;
using Delete.Feature.AzureActiveDirectory.Profiles;
using Delete.Feature.Events.Profiles;
using Delete.Feature.Footer.Profiles;
using Delete.Feature.GenericComponents.Profiles;
using Delete.Feature.HeaderBar.Profiles;
using Delete.Feature.LanguagePicker.Profiles;
using Delete.Feature.News.Profiles;
using Delete.Feature.NotFound.Profiles;
using Delete.Feature.PartialLayouts.Profiles;
using Delete.Feature.ProductFinder.Profiles;
using Delete.Feature.Products.Managers;
using Delete.Feature.Products.Profiles;
using Delete.Feature.Stockists.Profiles;
using Delete.Foundation.AssetsManagement.Profiles;
using Delete.Foundation.DataTemplates.Models.Dictionary;
using Delete.Foundation.DataTemplates.Models.Interfaces;
using Delete.Foundation.DataTemplates.Profiles;
using Delete.Foundation.DeleteFoundationCore.Models;
using Delete.Foundation.DesignLanguage.Profiles;
using Delete.Foundation.GenericComponentsSettings.Profiles;
using Delete.Foundation.MultiSite.Profiles;
using Delete.Foundation.RoyalCaninCore.Profiles;
using RoyalCanin.Project.CheckProduct.Profiles;
using RoyalCanin.Project.GooglePlacesImport.Profiles;
using RoyalCanin.Project.Metadata.Profiles;
using RoyalCanin.Project.ProductImport.Profiles;
using RoyalCanin.Project.RoyalCanin.Profiles;
using RoyalCanin.Project.StockistImport.Profiles;
using Delete.Foundation.DataTemplates.Models.Filter;
using Delete.Foundation.ItemImport.Models;
using NSubstitute.Core.Arguments;
using RoyalCanin.Project.RoyalCanin.Models;
using Sitecore.Globalization;

namespace RoyalCanin.Project.ProductImport.Tests
{
    [TestFixture]
    public partial class ImportProducts
    {
        private const string AustraliaSiteRoot = "{79C26763-D485-4D83-BF8B-152C59BF7BE9}";
        private const string CanadaSiteRoot = "{918cf94f-a97f-4d23-bcde-9e48e17a5449}";

        public IJob ProductImportSiteChrone;
        public IJobExecutionContext Context;

        [SetUp]
        public void Setup()
        {
            #region Mocking & object creation
            var logger = Substitute.For<IImportJobLogger>();
            var cacheManager = Substitute.For<ICacheManager>();

            var importProducts = new ImportProductsMock(logger, cacheManager);
            ProductImportSiteChrone = (IJob)importProducts;

            var jobDataMap = Substitute.For<JobDataMap>();
            jobDataMap.GetString("Items").Returns(AustraliaSiteRoot);

            var jobDetail = Substitute.For<IJobDetail>();
            jobDetail.JobDataMap.Returns(jobDataMap);

            Context = Substitute.For<IJobExecutionContext>();
            Context.JobDetail.Returns(jobDetail);
            #endregion
        }

        private IMapper CreateMapper()
        {
            var mapperConfiguration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new ArticlesProfile());
                cfg.AddProfile(new AzureActiveDirectoryProfile());
                cfg.AddProfile(new BreedsProfile());
                cfg.AddProfile(new EventsProfile());
                cfg.AddProfile(new FooterProfile());
                cfg.AddProfile(new GenericComponentsProfile());
                cfg.AddProfile(new HeaderBarProfile());
                cfg.AddProfile(new LanguagePickerProfile());
                cfg.AddProfile(new NewsProfile());
                cfg.AddProfile(new NotFoundProfile());
                cfg.AddProfile(new PartialLayoutsProfile());
                cfg.AddProfile(new ProductFinderProfile());
                cfg.AddProfile(new ProductsProfile());
                cfg.AddProfile(new StockistsProfile());
                cfg.AddProfile(new AssetsManagementProfile());
                cfg.AddProfile(new DataTemplatesProfile());
                cfg.AddProfile(new DesignLanguageProfile());
                cfg.AddProfile(new GenericComponentsSettingsProfile());
                cfg.AddProfile(new ItemImportProfileMock());
                cfg.AddProfile(new MultiSiteProfile());
                cfg.AddProfile(new RoyalCaninCoreProfile());
                cfg.AddProfile(new CheckProductProfile());
                cfg.AddProfile(new GooglePlacesImportProfile());
                cfg.AddProfile(new MetadataProfile());
                cfg.AddProfile(new ProductImportProfile());
                cfg.AddProfile(new RoyalCaninProfile());
                cfg.AddProfile(new StockistImportProfile());
            });

            return mapperConfiguration.CreateMapper();
        }

        private LocalMarketSettings CreateLocalMarketSettingsAustralia(bool fullImport)
        {
            var localMarketSettings = Substitute.For<LocalMarketSettings>();
            localMarketSettings.IsCheckingUpdatedDate.Returns(!fullImport);
            localMarketSettings.IsWeShareImport.Returns(true);
            localMarketSettings.Id.Returns(new Guid(AustraliaSiteRoot));
            localMarketSettings.ProductImportURL.Returns("test");
            localMarketSettings.RecipientEmailAddress.Returns("dumy@dumy.com");
            localMarketSettings.ProductRootItems.Returns(new List<I_ProductCatalogue>() { CreateProductCataloguePage() });
            return localMarketSettings;
        }

        private LocalMarketSettings CreateLocalMarketSettingsCanada(bool fullImport, string filePattern)
        {
            var localMarketSettings = Substitute.For<LocalMarketSettings>();
            localMarketSettings.IsCheckingUpdatedDate.Returns(!fullImport);
            localMarketSettings.IsWeShareImport.Returns(false);
            localMarketSettings.Id.Returns(new Guid(CanadaSiteRoot));
            localMarketSettings.ProductImportURL.Returns("test");
            localMarketSettings.RecipientEmailAddress.Returns("dumy@dumy.com");
            localMarketSettings.ProductRootItems.Returns(new List<I_ProductCatalogue>() { CreateProductCataloguePage() });
            localMarketSettings.ProductImportFtpFilePattern = filePattern;
            localMarketSettings.ProductImportPathToFiles = "Canada";
            return localMarketSettings;
        }

        private ISitecoreContext CreateSitecoreAustraliaContext(LocalMarketSettings localMarketSettings)
        {
            var sitecoreContext = Substitute.For<ISitecoreContext>();
            sitecoreContext.GetItem<LocalMarketSettings>(new Guid(AustraliaSiteRoot)).Returns(localMarketSettings);
            sitecoreContext.GetItem<LocalMarketSettings>("/sitecore/content/Australia").Returns(localMarketSettings);
            sitecoreContext.GetRootItem<LocalMarketSettings>().Returns(localMarketSettings);
            return sitecoreContext;
        }

        private ISitecoreContext CreateSitecoreCanadaContext(LocalMarketSettings localMarketSettings)
        {
            var globalConfigurationFolder = new GlobalConfigurationFolder();
            globalConfigurationFolder.ProductImportSFTPHostname = "localhost";
            globalConfigurationFolder.ProductImportSFTPUsername = "Mihai Rimovecz";
            globalConfigurationFolder.ProductImportSFTPPassword = "secret";
            globalConfigurationFolder.ProductImportSFTPPath = "/Websites/RoyalCanin/Latest/src/Project/ProductImport/test/RoyalCanin.Project.ProductImport.Tests/Configs";
            globalConfigurationFolder.ProductImportSFTPSavePath = "/Websites/RoyalCanin/Latest/src/Project/ProductImport/test/RoyalCanin.Project.ProductImport.Tests/Configs";

            var sitecoreContext = Substitute.For<ISitecoreContext>();
            sitecoreContext.GetItem<GlobalConfigurationFolder>(GlobalConfigurationFolderConstants.GlobalConfigurationFolderItemIdString).Returns(globalConfigurationFolder);
            sitecoreContext.GetItem<LocalMarketSettings>(new Guid(CanadaSiteRoot)).Returns(localMarketSettings);
            sitecoreContext.GetItem<LocalMarketSettings>("/sitecore/content/Canada").Returns(localMarketSettings);
            sitecoreContext.GetRootItem<LocalMarketSettings>().Returns(localMarketSettings);
            return sitecoreContext;
        }

        private ProductCataloguePage CreateProductCataloguePage()
        {
            var productCataloguePage = new ProductCataloguePage();
            var pilar1 = new FilterItem();
            pilar1.SetId(Guid.Parse("352e1675-3ad8-40eb-98dc-f9d6ad8c2b5a"));

            var pilar2 = new FilterItem();
            pilar2.SetId(Guid.Parse("41511285-8626-4e4d-94b8-efcc8ebc8e39"));

            var pilar3 = new FilterItem();
            pilar3.SetId(Guid.Parse("00000000-0000-0000-0000-000000000000"));

            var spices1 = new FilterItem();
            spices1.SetId(Guid.Parse("b60472da-918e-4f5e-8082-fd1d51c07762"));

            var spices2 = new FilterItem();
            spices2.SetId(Guid.Parse("42b60b8c-d080-477d-bb15-b5f8ddbffe3a"));

            productCataloguePage.PCPillars = new List<GlassBase>() { pilar1, pilar2, pilar3 };
            productCataloguePage.PCSpecies = new List<DictionaryEntry>() { spices1, spices2 };
            productCataloguePage.Fullpath = @"/sitecore/content/Royal Canin/Australia/Home/Dogs/Products/Vet Products";

            return productCataloguePage;
        }
    }
}
