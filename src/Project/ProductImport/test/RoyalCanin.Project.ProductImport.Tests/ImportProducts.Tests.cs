﻿using System;
using System.Collections.Generic;
using AutoMapper;
using NUnit.Framework;
using Quartz;
using NSubstitute;
using Delete.Foundation.ItemImport.Loggers;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.ItemImport.LogContainer;
using Delete.Foundation.ItemImport.LogManagerImport;
using Delete.Foundation.MultiSite.Models;
using Glass.Mapper.Sc;
using Microsoft.Extensions.DependencyInjection;
using RoyalCanin.Project.ProductImport.Importers;
using RoyalCanin.Project.ProductImport.Orchestrators;
using RoyalCanin.Project.ProductImport.Processors;
using Sitecore.Data;
using Sitecore.DependencyInjection;
using Delete.Feature.Products.Import.Processors;
using Delete.Foundation.DataTemplates.Models.Taxonomy;
using Delete.Feature.Breeds.Profiles;
using Delete.Feature.Articles.Profiles;
using Delete.Feature.AzureActiveDirectory.Profiles;
using Delete.Feature.Events.Profiles;
using Delete.Feature.Footer.Profiles;
using Delete.Feature.GenericComponents.Profiles;
using Delete.Feature.HeaderBar.Profiles;
using Delete.Feature.LanguagePicker.Profiles;
using Delete.Feature.News.Profiles;
using Delete.Feature.NotFound.Profiles;
using Delete.Feature.PartialLayouts.Profiles;
using Delete.Feature.ProductFinder.Profiles;
using Delete.Feature.Products.Managers;
using Delete.Feature.Products.Profiles;
using Delete.Feature.Stockists.Profiles;
using Delete.Foundation.AssetsManagement.Profiles;
using Delete.Foundation.DataTemplates.Models.Dictionary;
using Delete.Foundation.DataTemplates.Models.Interfaces;
using Delete.Foundation.DataTemplates.Profiles;
using Delete.Foundation.DeleteFoundationCore.Models;
using Delete.Foundation.DesignLanguage.Profiles;
using Delete.Foundation.GenericComponentsSettings.Profiles;
using Delete.Foundation.MultiSite.Profiles;
using Delete.Foundation.RoyalCaninCore.Profiles;
using RoyalCanin.Project.CheckProduct.Profiles;
using RoyalCanin.Project.GooglePlacesImport.Profiles;
using RoyalCanin.Project.Metadata.Profiles;
using RoyalCanin.Project.ProductImport.Profiles;
using RoyalCanin.Project.RoyalCanin.Profiles;
using RoyalCanin.Project.StockistImport.Profiles;
using Delete.Foundation.DataTemplates.Models.Filter;
using Delete.Foundation.ItemImport.Models;
using NSubstitute.Core.Arguments;
using RoyalCanin.Project.RoyalCanin.Models;
using Sitecore.Globalization;

namespace RoyalCanin.Project.ProductImport.Tests
{
    [TestFixture]
    public partial class ImportProducts
    {
        [Test]
        public void FullImportNewMarket()
        {
            using (var database = new Sitecore.FakeDb.Db())
            {
                #region Arrange
                var serviceCollection = new ServiceCollection();
                new DefaultSitecoreServicesConfigurator().Configure(serviceCollection);

                var logManager = new LogManagerImport(new CounterValueSet());

                serviceCollection.AddScoped<ILogManagerImport>(x => logManager);

                var localMarketSettings = CreateLocalMarketSettingsAustralia(true);
                var cacheManager = Substitute.For<ICacheManager>();
                var mapper = CreateMapper();
                var sitecoreContext = CreateSitecoreAustraliaContext(localMarketSettings);

                serviceCollection.AddScoped<IBaseSearchManager<IGlassBase>>(x => new ProductSearchManager() { });
                serviceCollection.AddScoped<IBaseSearchManager<FilterItem>>(x => Substitute.For<IBaseSearchManager<FilterItem>>() );
                serviceCollection.AddScoped<IBaseSearchManager<Pillar>>(x => Substitute.For<IBaseSearchManager<Pillar>>());
                serviceCollection.AddScoped<IBaseSearchManager<ProductPage>>(x => Substitute.For<IBaseSearchManager<ProductPage>>());
                serviceCollection.AddScoped<ISitecoreContext>(x => sitecoreContext);

                ServiceLocator.SetServiceProvider(serviceCollection.BuildServiceProvider());

                var pillarResult = new Pillar();

                var pillarIdItemProcessor = Substitute.For<IPillarIdItemProcessor>();
                pillarIdItemProcessor.ProcessItem(Arg.Any<PillarDto>(), Arg.Any<List<Language>>()).Returns(pillarResult);

                var itemProcessor = new LocalProductItemProcessorFullImportMock(
                    sitecoreContext,
                    localMarketSettings,
                    pillarIdItemProcessor, 
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    Substitute.For<IProductTypeItemProcessor>(),
                    logManager,
                    mapper,
                    null);

                var globalProductValidation = Substitute.For<IGlobalProductValidation>();
                globalProductValidation.CheckGlobalProductExist(Arg.Any<ImportedProductDto>()).Returns(true);

                var orchestrator = new ImportedProductConstructionOrchestrator(sitecoreContext, itemProcessor, localMarketSettings, globalProductValidation);

                var productImporter = new ProductImporterMock(sitecoreContext, mapper, cacheManager, logManager, orchestrator, itemProcessor, ProductImporterMock.AustraliaSource);
                serviceCollection.AddScoped<IProductImporter>(x => productImporter);
                IServiceProvider serviceProvider = serviceCollection.BuildServiceProvider();
                ServiceLocator.SetServiceProvider(serviceProvider);
                var item = new Sitecore.FakeDb.DbItem("Australia", new ID(AustraliaSiteRoot)) {TemplateID = ID.NewID};
                item.FullPath = "//sitecore//content//Royal Canin//Australia";
                database.Add(item);
                #endregion

                // Act
                ProductImportSiteChrone.Execute(Context);

                var importLog = logManager.GetValue().Log;

                // Assert
                Assert.AreEqual(216, importLog.ItemsProcessed);
            }
        }

        [Test]
        public void FullImportExistingMarket()
        {
            using (var database = new Sitecore.FakeDb.Db())
            {
                #region Arrange
                var serviceCollection = new ServiceCollection();
                new DefaultSitecoreServicesConfigurator().Configure(serviceCollection);

                var logManager = new LogManagerImport(new CounterValueSet());

                serviceCollection.AddScoped<ILogManagerImport>(x => logManager);

                var localMarketSettings = CreateLocalMarketSettingsAustralia(true);
                var cacheManager = Substitute.For<ICacheManager>();
                var mapper = CreateMapper();
                var sitecoreContext = CreateSitecoreAustraliaContext(localMarketSettings);

                serviceCollection.AddScoped<IBaseSearchManager<IGlassBase>>(x => new ProductSearchManager() { });
                serviceCollection.AddScoped<IBaseSearchManager<FilterItem>>(x => Substitute.For<IBaseSearchManager<FilterItem>>());
                serviceCollection.AddScoped<IBaseSearchManager<Pillar>>(x => Substitute.For<IBaseSearchManager<Pillar>>());
                serviceCollection.AddScoped<IBaseSearchManager<ProductPage>>(x => Substitute.For<IBaseSearchManager<ProductPage>>());
                serviceCollection.AddScoped<ISitecoreContext>(x => sitecoreContext);

                ServiceLocator.SetServiceProvider(serviceCollection.BuildServiceProvider());

                var pillarResult = new Pillar();

                var pillarIdItemProcessor = Substitute.For<IPillarIdItemProcessor>();
                pillarIdItemProcessor.ProcessItem(Arg.Any<PillarDto>(), Arg.Any<List<Language>>()).Returns(pillarResult);

                var itemProcessor = new LocalProductItemProcessorFullImportMock(
                    sitecoreContext,
                    localMarketSettings,
                    pillarIdItemProcessor,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    Substitute.For<IProductTypeItemProcessor>(),
                    logManager,
                    mapper,
                    null);

                var globalProductValidation = Substitute.For<IGlobalProductValidation>();
                globalProductValidation.CheckGlobalProductExist(Arg.Any<ImportedProductDto>()).Returns(true);

                var orchestrator = new ImportedProductConstructionOrchestrator(sitecoreContext, itemProcessor, localMarketSettings, globalProductValidation);

                var productImporter = new ProductImporterMock(sitecoreContext, mapper, cacheManager, logManager, orchestrator, itemProcessor, ProductImporterMock.AustraliaSource);
                serviceCollection.AddScoped<IProductImporter>(x => productImporter);
                IServiceProvider serviceProvider = serviceCollection.BuildServiceProvider();
                ServiceLocator.SetServiceProvider(serviceProvider);
                var item = new Sitecore.FakeDb.DbItem("Australia", new ID(AustraliaSiteRoot)) { TemplateID = ID.NewID };
                item.FullPath = "//sitecore//content//Royal Canin//Australia";
                database.Add(item);
                #endregion

                // Act
                ProductImportSiteChrone.Execute(Context);

                var importLog = logManager.GetValue().Log;

                Assert.AreEqual(216, importLog.ItemsProcessed);

                ProductImportSiteChrone.Execute(Context);

                Assert.AreEqual(216 * 2, importLog.ItemsProcessed);
            }
        }

        [Test]
        public void UpdateExistingMarket()
        {
            using (var database = new Sitecore.FakeDb.Db())
            {
                #region Arrange
                var serviceCollection = new ServiceCollection();
                new DefaultSitecoreServicesConfigurator().Configure(serviceCollection);

                var logManager = new LogManagerImport(new CounterValueSet());

                serviceCollection.AddScoped<ILogManagerImport>(x => logManager);

                var localMarketSettings = CreateLocalMarketSettingsAustralia(false);
                var cacheManager = Substitute.For<ICacheManager>();
                var mapper = CreateMapper();
                var sitecoreContext = CreateSitecoreAustraliaContext(localMarketSettings);

                serviceCollection.AddScoped<IBaseSearchManager<IGlassBase>>(x => new ProductSearchManager() { });
                serviceCollection.AddScoped<IBaseSearchManager<FilterItem>>(x => Substitute.For<IBaseSearchManager<FilterItem>>());
                serviceCollection.AddScoped<IBaseSearchManager<Pillar>>(x => Substitute.For<IBaseSearchManager<Pillar>>());
                serviceCollection.AddScoped<IBaseSearchManager<ProductPage>>(x => Substitute.For<IBaseSearchManager<ProductPage>>());
                serviceCollection.AddScoped<ISitecoreContext>(x => sitecoreContext);

                ServiceLocator.SetServiceProvider(serviceCollection.BuildServiceProvider());

                var pillarResult = new Pillar();

                var pillarIdItemProcessor = Substitute.For<IPillarIdItemProcessor>();
                pillarIdItemProcessor.ProcessItem(Arg.Any<PillarDto>(), Arg.Any<List<Language>>()).Returns(pillarResult);

                var itemProcessor = new LocalProductItemProcessorFullImportMock(
                    sitecoreContext,
                    localMarketSettings,
                    pillarIdItemProcessor,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    Substitute.For<IProductTypeItemProcessor>(),
                    logManager,
                    mapper,
                    null);

                var globalProductValidation = Substitute.For<IGlobalProductValidation>();
                globalProductValidation.CheckGlobalProductExist(Arg.Any<ImportedProductDto>()).Returns(true);

                var orchestrator = new ImportedProductConstructionOrchestrator(sitecoreContext, itemProcessor, localMarketSettings, globalProductValidation);

                var productImporter = new ProductImporterMock(sitecoreContext, mapper, cacheManager, logManager, orchestrator, itemProcessor, ProductImporterMock.AustraliaSource);
                serviceCollection.AddScoped<IProductImporter>(x => productImporter);
                IServiceProvider serviceProvider = serviceCollection.BuildServiceProvider();
                ServiceLocator.SetServiceProvider(serviceProvider);
                var item = new Sitecore.FakeDb.DbItem("Australia", new ID(AustraliaSiteRoot)) { TemplateID = ID.NewID };
                item.FullPath = "//sitecore//content//Royal Canin//Australia";
                database.Add(item);
                #endregion

                // Act
                ProductImportSiteChrone.Execute(Context);

                var importLog = logManager.GetValue().Log;

                Assert.AreEqual(216, importLog.ItemsProcessed);

                serviceCollection = new ServiceCollection();
                new DefaultSitecoreServicesConfigurator().Configure(serviceCollection);
                serviceCollection.AddScoped<ILogManagerImport>(x => logManager);
                serviceCollection.AddScoped<IBaseSearchManager<IGlassBase>>(x => new ProductSearchManager() { });
                serviceCollection.AddScoped<IBaseSearchManager<FilterItem>>(x => Substitute.For<IBaseSearchManager<FilterItem>>());
                serviceCollection.AddScoped<IBaseSearchManager<Pillar>>(x => Substitute.For<IBaseSearchManager<Pillar>>());
                serviceCollection.AddScoped<IBaseSearchManager<ProductPage>>(x => Substitute.For<IBaseSearchManager<ProductPage>>());
                serviceCollection.AddScoped<ISitecoreContext>(x => sitecoreContext);
                ServiceLocator.SetServiceProvider(serviceCollection.BuildServiceProvider());

                var itemUpdateProcessor = new LocalProductItemProcessorUpdateMock(
                    sitecoreContext,
                    localMarketSettings,
                    pillarIdItemProcessor,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    Substitute.For<IProductTypeItemProcessor>(),
                    logManager,
                    mapper,
                    null);

                orchestrator = new ImportedProductConstructionOrchestrator(sitecoreContext, itemUpdateProcessor, localMarketSettings, globalProductValidation);

                productImporter = new ProductImporterMock(sitecoreContext, mapper, cacheManager, logManager, orchestrator, itemUpdateProcessor, ProductImporterMock.AustraliaSource);
                serviceCollection.AddScoped<IProductImporter>(x => productImporter);
                serviceProvider = serviceCollection.BuildServiceProvider();
                ServiceLocator.SetServiceProvider(serviceProvider);


                ProductImportSiteChrone.Execute(Context);

                Assert.AreEqual(216, importLog.ItemsProcessed);
            }
        }

        [Test]
        public void UpdateCanada()
        {
            using (var database = new Sitecore.FakeDb.Db())
            {
                #region Arrange
                var jobDataMap = Substitute.For<JobDataMap>();
                jobDataMap.GetString("Items").Returns(CanadaSiteRoot);

                var jobDetail = Substitute.For<IJobDetail>();
                jobDetail.JobDataMap.Returns(jobDataMap);

                Context = Substitute.For<IJobExecutionContext>();
                Context.JobDetail.Returns(jobDetail);



                var serviceCollection = new ServiceCollection();
                new DefaultSitecoreServicesConfigurator().Configure(serviceCollection);

                var logManager = new LogManagerImport(new CounterValueSet());

                serviceCollection.AddScoped<ILogManagerImport>(x => logManager);

                var localMarketSettings = CreateLocalMarketSettingsCanada(false, "CanadaSalsify.json");
                var cacheManager = Substitute.For<ICacheManager>();
                var mapper = CreateMapper();
                var sitecoreContext = CreateSitecoreCanadaContext(localMarketSettings);

                serviceCollection.AddScoped<IBaseSearchManager<IGlassBase>>(x => new ProductSearchManager() { });
                serviceCollection.AddScoped<IBaseSearchManager<FilterItem>>(x => Substitute.For<IBaseSearchManager<FilterItem>>());
                serviceCollection.AddScoped<IBaseSearchManager<Pillar>>(x => Substitute.For<IBaseSearchManager<Pillar>>());
                serviceCollection.AddScoped<IBaseSearchManager<ProductPage>>(x => Substitute.For<IBaseSearchManager<ProductPage>>());
                serviceCollection.AddScoped<ISitecoreContext>(x => sitecoreContext);

                ServiceLocator.SetServiceProvider(serviceCollection.BuildServiceProvider());

                var pillarResult = new Pillar();

                var pillarIdItemProcessor = Substitute.For<IPillarIdItemProcessor>();
                pillarIdItemProcessor.ProcessItem(Arg.Any<PillarDto>(), Arg.Any<List<Language>>()).Returns(pillarResult);

                var itemProcessor = new LocalProductItemProcessorFullImportMock(
                    sitecoreContext,
                    localMarketSettings,
                    pillarIdItemProcessor,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    Substitute.For<IProductTypeItemProcessor>(),
                    logManager,
                    mapper,
                    null);

                var globalProductValidation = Substitute.For<IGlobalProductValidation>();
                globalProductValidation.CheckGlobalProductExist(Arg.Any<ImportedProductDto>()).Returns(true);

                var orchestrator = new ImportedProductConstructionOrchestrator(sitecoreContext, itemProcessor, localMarketSettings, globalProductValidation);

                var productImporter = new ProductImporterMock(sitecoreContext, mapper, cacheManager, logManager, orchestrator, itemProcessor, ProductImporterMock.AustraliaSource);
                serviceCollection.AddScoped<IProductImporter>(x => productImporter);
                IServiceProvider serviceProvider = serviceCollection.BuildServiceProvider();
                ServiceLocator.SetServiceProvider(serviceProvider);
                var item = new Sitecore.FakeDb.DbItem("Canada", new ID(CanadaSiteRoot)) { TemplateID = ID.NewID };
                item.FullPath = "//sitecore//content//Royal Canin//Canada";
                database.Add(item);
                #endregion

                // Act
                ProductImportSiteChrone.Execute(Context);

                var importLog = logManager.GetValue().Log;

                // Assert
                Assert.AreEqual(290, importLog.ItemsProcessed);

                serviceCollection = new ServiceCollection();
                new DefaultSitecoreServicesConfigurator().Configure(serviceCollection);
                serviceCollection.AddScoped<ILogManagerImport>(x => logManager);
                serviceCollection.AddScoped<IBaseSearchManager<IGlassBase>>(x => new ProductSearchManager() { });
                serviceCollection.AddScoped<IBaseSearchManager<FilterItem>>(x => Substitute.For<IBaseSearchManager<FilterItem>>());
                serviceCollection.AddScoped<IBaseSearchManager<Pillar>>(x => Substitute.For<IBaseSearchManager<Pillar>>());
                serviceCollection.AddScoped<IBaseSearchManager<ProductPage>>(x => Substitute.For<IBaseSearchManager<ProductPage>>());
                serviceCollection.AddScoped<ISitecoreContext>(x => sitecoreContext);
                ServiceLocator.SetServiceProvider(serviceCollection.BuildServiceProvider());

                var itemUpdateProcessor = new LocalProductItemProcessorUpdateMock(
                    sitecoreContext,
                    localMarketSettings,
                    pillarIdItemProcessor,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    Substitute.For<IProductTypeItemProcessor>(),
                    logManager,
                    mapper,
                    null);

                orchestrator = new ImportedProductConstructionOrchestrator(sitecoreContext, itemUpdateProcessor, localMarketSettings, globalProductValidation);

                productImporter = new ProductImporterMock(sitecoreContext, mapper, cacheManager, logManager, orchestrator, itemUpdateProcessor, ProductImporterMock.AustraliaSource);
                serviceCollection.AddScoped<IProductImporter>(x => productImporter);
                serviceProvider = serviceCollection.BuildServiceProvider();
                ServiceLocator.SetServiceProvider(serviceProvider);


                ProductImportSiteChrone.Execute(Context);

                Assert.AreEqual(290, importLog.ItemsProcessed);
            }
        }

        [Test]
        public void FullImportNewMarketErrorsInWeShare()
        {
            using (var database = new Sitecore.FakeDb.Db())
            {
                #region Arrange
                var serviceCollection = new ServiceCollection();
                new DefaultSitecoreServicesConfigurator().Configure(serviceCollection);

                var logManager = new LogManagerImport(new CounterValueSet());

                serviceCollection.AddScoped<ILogManagerImport>(x => logManager);

                var localMarketSettings = CreateLocalMarketSettingsAustralia(true);
                var cacheManager = Substitute.For<ICacheManager>();
                var mapper = CreateMapper();
                var sitecoreContext = CreateSitecoreAustraliaContext(localMarketSettings);

                serviceCollection.AddScoped<IBaseSearchManager<IGlassBase>>(x => new ProductSearchManager() { });
                serviceCollection.AddScoped<IBaseSearchManager<FilterItem>>(x => Substitute.For<IBaseSearchManager<FilterItem>>());
                serviceCollection.AddScoped<IBaseSearchManager<Pillar>>(x => Substitute.For<IBaseSearchManager<Pillar>>());
                serviceCollection.AddScoped<IBaseSearchManager<ProductPage>>(x => Substitute.For<IBaseSearchManager<ProductPage>>());
                serviceCollection.AddScoped<ISitecoreContext>(x => sitecoreContext);

                ServiceLocator.SetServiceProvider(serviceCollection.BuildServiceProvider());

                var pillarResult = new Pillar();

                var pillarIdItemProcessor = Substitute.For<IPillarIdItemProcessor>();
                pillarIdItemProcessor.ProcessItem(Arg.Any<PillarDto>(), Arg.Any<List<Language>>()).Returns(pillarResult);

                var itemProcessor = new LocalProductItemProcessorFullImportMock(
                    sitecoreContext,
                    localMarketSettings,
                    pillarIdItemProcessor,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    Substitute.For<IProductTypeItemProcessor>(),
                    logManager,
                    mapper,
                    null);

                var globalProductValidation = Substitute.For<IGlobalProductValidation>();
                globalProductValidation.CheckGlobalProductExist(Arg.Any<ImportedProductDto>()).Returns(true);

                var orchestrator = new ImportedProductConstructionOrchestrator(sitecoreContext, itemProcessor, localMarketSettings, globalProductValidation);

                var productImporter = new ProductImporterMock(sitecoreContext, mapper, cacheManager, logManager, orchestrator, itemProcessor, ProductImporterMock.AustraliaErrorsSource);
                serviceCollection.AddScoped<IProductImporter>(x => productImporter);
                IServiceProvider serviceProvider = serviceCollection.BuildServiceProvider();
                ServiceLocator.SetServiceProvider(serviceProvider);
                var item = new Sitecore.FakeDb.DbItem("Australia", new ID(AustraliaSiteRoot)) { TemplateID = ID.NewID };
                item.FullPath = "//sitecore//content//Royal Canin//Australia";
                database.Add(item);
                #endregion

                // Act
                ProductImportSiteChrone.Execute(Context);

                var importLog = logManager.GetValue().Log;

                // Assert
                Assert.AreEqual(215, importLog.ItemsProcessed);
            }
        }

        [Test]
        public void FullImportNewMarketErrorsInSalsify()
        {
            using (var database = new Sitecore.FakeDb.Db())
            {
                #region Arrange
                var jobDataMap = Substitute.For<JobDataMap>();
                jobDataMap.GetString("Items").Returns(CanadaSiteRoot);

                var jobDetail = Substitute.For<IJobDetail>();
                jobDetail.JobDataMap.Returns(jobDataMap);

                Context = Substitute.For<IJobExecutionContext>();
                Context.JobDetail.Returns(jobDetail);



                var serviceCollection = new ServiceCollection();
                new DefaultSitecoreServicesConfigurator().Configure(serviceCollection);

                var logManager = new LogManagerImport(new CounterValueSet());

                serviceCollection.AddScoped<ILogManagerImport>(x => logManager);

                var localMarketSettings = CreateLocalMarketSettingsCanada(false, "CanadaSalsifyErrors.json");
                var cacheManager = Substitute.For<ICacheManager>();
                var mapper = CreateMapper();
                var sitecoreContext = CreateSitecoreCanadaContext(localMarketSettings);

                serviceCollection.AddScoped<IBaseSearchManager<IGlassBase>>(x => new ProductSearchManager() { });
                serviceCollection.AddScoped<IBaseSearchManager<FilterItem>>(x => Substitute.For<IBaseSearchManager<FilterItem>>());
                serviceCollection.AddScoped<IBaseSearchManager<Pillar>>(x => Substitute.For<IBaseSearchManager<Pillar>>());
                serviceCollection.AddScoped<IBaseSearchManager<ProductPage>>(x => Substitute.For<IBaseSearchManager<ProductPage>>());
                serviceCollection.AddScoped<ISitecoreContext>(x => sitecoreContext);

                ServiceLocator.SetServiceProvider(serviceCollection.BuildServiceProvider());

                var pillarResult = new Pillar();

                var pillarIdItemProcessor = Substitute.For<IPillarIdItemProcessor>();
                pillarIdItemProcessor.ProcessItem(Arg.Any<PillarDto>(), Arg.Any<List<Language>>()).Returns(pillarResult);

                var itemProcessor = new LocalProductItemProcessorFullImportMock(
                    sitecoreContext,
                    localMarketSettings,
                    pillarIdItemProcessor,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    Substitute.For<IProductTypeItemProcessor>(),
                    logManager,
                    mapper,
                    null);

                var globalProductValidation = Substitute.For<IGlobalProductValidation>();
                globalProductValidation.CheckGlobalProductExist(Arg.Any<ImportedProductDto>()).Returns(true);

                var orchestrator = new ImportedProductConstructionOrchestrator(sitecoreContext, itemProcessor, localMarketSettings, globalProductValidation);

                var productImporter = new ProductImporterMock(sitecoreContext, mapper, cacheManager, logManager, orchestrator, itemProcessor, ProductImporterMock.AustraliaSource);
                serviceCollection.AddScoped<IProductImporter>(x => productImporter);
                IServiceProvider serviceProvider = serviceCollection.BuildServiceProvider();
                ServiceLocator.SetServiceProvider(serviceProvider);
                var item = new Sitecore.FakeDb.DbItem("Canada", new ID(CanadaSiteRoot)) { TemplateID = ID.NewID };
                item.FullPath = "//sitecore//content//Royal Canin//Canada";
                database.Add(item);
                #endregion

                // Act
                ProductImportSiteChrone.Execute(Context);

                var importLog = logManager.GetValue().Log;

                // Assert
                Assert.AreEqual(290, importLog.ItemsProcessed);
            }
        }
    }
}