﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace RoyalCanin.Project.Metadata.Models
{
    [DataContract]
    public class CustomDimensionsObject
    {
        [DataMember(Name = "user")]
        public Dictionary<string, string> UserData { get; set; }

        [DataMember(Name = "session")]
        public Dictionary<string, string> SessionData { get; set; }

        [DataMember(Name = "site")]
        public Dictionary<string, string> SiteData { get; set; }
        
        [DataMember(Name = "page")]
        public Dictionary<string, string> PageData { get; set; }
        
        [DataMember(Name = "pet")]
        public Dictionary<string, string> PetData { get; set; }
    }
}