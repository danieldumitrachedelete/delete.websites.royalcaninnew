﻿using System.Collections.Generic;

namespace RoyalCanin.Project.Metadata.Models
{
    public class MetadataLinks
    {
        public List<MetadataLink> Links { get; set; }
    }

    public class MetadataLink
    {
        public string Rel { get; set; }

        public string Href { get; set; }

        public string Hreflang { get; set; }
    }
}