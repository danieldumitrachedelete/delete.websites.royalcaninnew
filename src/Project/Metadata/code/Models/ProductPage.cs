﻿using System;
using System.Collections.Generic;
using System.Linq;
using Delete.Foundation.DataTemplates.Models.Taxonomy;

namespace RoyalCanin.Project.Metadata.Models
{

    public partial class ProductPage
    {
        public IEnumerable<Lifestage> ExtendedLifestages => LifestagesOverrides != null && LifestagesOverrides.Any() ? LifestagesOverrides : (GlobalProduct?.Lifestages ?? new List<Lifestage>());

        public Guid ExtendedBreed => BreedOverride != Guid.Empty ? BreedOverride : GlobalProduct?.Breed ?? Guid.Empty;
    }
}
