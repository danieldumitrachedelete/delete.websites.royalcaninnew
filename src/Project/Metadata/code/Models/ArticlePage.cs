﻿namespace RoyalCanin.Project.Metadata.Models
{
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using Glass.Mapper.Sc.Configuration.Attributes;
    using Sitecore.ContentSearch;
    using Delete.Feature.Articles.Models.Taxonomy;
    
    public static partial class ArticlePageConstants
    {
        public const string BreedsFieldId = "{beaa9dd6-5ed0-4689-9adc-8e86aedc5cee}";
        public const string ContentTagsFieldId = "{62e4ef91-adbf-439c-9758-53d73a102346}";
        public const string LifestagesFieldId = "{f3dc0387-7c59-4bea-a855-25f2e8a1577c}";
        public const string ArticleTopicsFieldId = "{58ccc978-54c8-493a-ad41-fbac5720c9a3}";

        public const string BreedsFieldName = "Breeds";
        public const string ContentTagsFieldName = "Content Tags";
        public const string LifestagesFieldName = "Lifestages";
        public const string ArticleTopicsFieldName = "Article Topics";

    }

    public partial class ArticlePage
    {
        public IEnumerable<Guid> RelatedEntities { get; }

        public string ArticleTopicString => ArticleTopics != null && ArticleTopics.Any() ? string.Join(", ", ArticleTopics.Select(x => x.Text)) : string.Empty;

        [SitecoreField(FieldId = ArticlePageConstants.LifestagesFieldId, FieldName = ArticlePageConstants.LifestagesFieldName)]
        [IndexField(ArticlePageConstants.LifestagesFieldName)]
        public virtual IEnumerable<Delete.Foundation.DataTemplates.Models.Dictionary.DictionaryEntry> Lifestages { get; set; }

        [SitecoreField(FieldId = ArticlePageConstants.ArticleTopicsFieldId, FieldName = ArticlePageConstants.ArticleTopicsFieldName)]
        [IndexField(ArticlePageConstants.ArticleTopicsFieldName)]
        public virtual IEnumerable<ArticleTopic> ArticleTopics { get; set; }

        [SitecoreField(FieldId = ArticlePageConstants.ContentTagsFieldId, FieldName = ArticlePageConstants.ContentTagsFieldName)]
        [IndexField(ArticlePageConstants.ContentTagsFieldName)]
        public virtual IEnumerable<Delete.Foundation.DataTemplates.Models.Dictionary.DictionaryEntry> ContentTags { get; set; }

        [SitecoreField(FieldId = ArticlePageConstants.BreedsFieldId, FieldName = ArticlePageConstants.BreedsFieldName)]
        [IndexField(ArticlePageConstants.BreedsFieldName)]
        public virtual IEnumerable<Delete.Foundation.DataTemplates.Models.Dictionary.DictionaryEntry> Breeds { get; set; }
    }
}