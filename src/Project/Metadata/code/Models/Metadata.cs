﻿namespace RoyalCanin.Project.Metadata.Models
{
    public interface IMetadata
    {
        string OriginalTitle { get;set; }

        string Title { get; set; }

        string Description { get; set; }

        string Keywords { get; set; }

        string Url { get; set; }

        string Image { get; set; }

        string ContentType { get; set; }

        bool NoIndex { get; set; }
    }

    public class Metadata : IMetadata
    {
        public string OriginalTitle { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Keywords { get; set; }

        public string Url { get; set; }

        public string Image { get; set; }

        public string ContentType { get; set; }

        public bool NoIndex { get; set; }
    }
}