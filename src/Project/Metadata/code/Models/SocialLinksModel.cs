﻿namespace RoyalCanin.Project.Metadata.Models
{
    public class SocialLinksModel
    {
        public string CurrentPageAbsoluteUrl { get; set; }

        public string CurrentPageTitle { get; set; }

        public string TwitterHashTags { get; set; }

        public string FacebookHashTags { get; set; }
    }
}