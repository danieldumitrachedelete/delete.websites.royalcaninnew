using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Delete.Foundation.DeleteFoundationCore.Controllers;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.DeleteFoundationCore.Models;
using Delete.Foundation.MultiSite.Helpers;
using Glass.Mapper.Sc;
using RoyalCanin.Project.Metadata.Managers;
using Sitecore.Mvc.Extensions;

namespace RoyalCanin.Project.Metadata.Controllers
{
    using System.Web.Mvc;

    using Delete.Foundation.DeleteFoundationCore.Extensions;
    using Delete.Foundation.MultiSite.Models;

    using MetadataStrategies;

    using Models;

    using Sitecore.Data;

    public class MetadataController : BaseController
    {
        private readonly IMetadataStrategyFactory metadataStrategyFactory;

        private readonly ILocalMarketSettings localMarketSettings;

        private readonly IPageSearchManager pageSearchManager;

        public MetadataController(ILocalMarketSettings localMarketSettings,
            IMetadataStrategyFactory metadataStrategyFactory,
            ISitecoreContext sitecoreContext,
            IMapper mapper,
            ICacheManager cacheManager,
            IPageSearchManager pageSearchManager) : base(sitecoreContext, mapper, cacheManager)
        {
            this.metadataStrategyFactory = metadataStrategyFactory;
            this.localMarketSettings = localMarketSettings;
            this.pageSearchManager = pageSearchManager;
        }

        public ActionResult SetupHomePageFlag()
        {
            this.GetHtmlHelper().SetHomePageFlag((Sitecore.Context.Item?.TemplateID ?? new ID()) == HomepageConstants.TemplateId);

            return new EmptyResult();
        }

        public ActionResult RenderMeta()
        {
            var model = GetContextItemMetadata();
            
            return this.PartialView("~/Views/Project/Metadata/Metadata.cshtml", model);
        }

        public ActionResult RenderMetaFacebook()
        {
            var model = GetContextItemMetadata();
            
            return this.PartialView("~/Views/Project/Metadata/MetadataFacebook.cshtml", model);
        }

        public ActionResult RenderMetaTwitter()
        {
            var model = GetContextItemMetadata();

            return this.PartialView("~/Views/Project/Metadata/MetadataTwitter.cshtml", model);
        }

        public ActionResult RenderLinks()
        {
            var model = GetContextItemLinks();
            return this.PartialView("~/Views/Project/Metadata/MetadataLinks.cshtml", model);
        }

        public ActionResult RegisterPageValues()
        {
            var model = GetContextItemMetadata();
            var page = GetContextItem();

            this.GetHtmlHelper().SetPageTitle(model?.Title ?? string.Empty);
            this.GetHtmlHelper().SetItemName(page?.Name ?? string.Empty);

            return new EmptyResult();
        }

        private MetadataLinks GetContextItemLinks()
        {
            var contextItemId = Sitecore.Context.Item?.ID?.ToString();
            var cacheKey = $"page_metadata_{contextItemId}";

            MetadataLinks result = null;

            if (contextItemId.NotEmpty())
            {
                result = this._cacheManager.CacheResults (() => 
                {
                    var page = this.GetContextItem<BasePage>(false, true);

                    var model = new MetadataLinks
                    {
                        Links = new List<MetadataLink>
                        {
                            new MetadataLink
                            {
                                Rel = Constants.LinkTypes.Canonical,
                                Href = page.Url
                            },
                            new MetadataLink
                            {
                                Rel = Constants.LinkTypes.Alternate,
                                Hreflang = page.Language,
                                Href = page.Url
                            }                            
                        }
                    };

                    var siteRoot = SitecoreContext.GetRootItemRelative<GlassBase>();
                    var pageRelativePath = page.Fullpath.Replace(siteRoot.Fullpath, String.Empty);
                    
                    var allAlternativePages = new List<GlassBase>();

                    if (siteRoot?.Parent?.Fullpath.NotEmpty() == true)
                    {
                        allAlternativePages = pageSearchManager
                            .FindAlternativePages(siteRoot.Parent.Fullpath, pageRelativePath)
                            .ToList();
                    }
                    
                    allAlternativePages = allAlternativePages.Where(x => SiteManagerHelper.GetAvailableWebsites().Any(y =>
                                x.Language.Equals(y.Properties["language"], StringComparison.OrdinalIgnoreCase) &&
                                x.Fullpath.StartsWith(string.Concat(y.Properties["rootPath"], y.Properties["startItem"]), StringComparison.OrdinalIgnoreCase))).ToList();

                    foreach (var alternativePage in allAlternativePages.Where(x =>
                        x.Id != page.Id || !x.Language.Equals(page.Language)))
                    {
                        model.Links.Add(new MetadataLink
                        {
                            Rel = Constants.LinkTypes.Alternate,
                            Hreflang = alternativePage.Language,
                            Href = alternativePage.AbsoluteUrl
                        });
                    }

                    var defaultPage = allAlternativePages.FirstOrDefault(x => x.Language.Equals("en-US", StringComparison.OrdinalIgnoreCase));
                    if (defaultPage != null)
                    {
                        model.Links.Add(new MetadataLink
                        {
                            Rel = Constants.LinkTypes.Alternate,
                            Hreflang = Constants.HrefLangTypes.XDefault,
                            Href = defaultPage.Url
                        });
                    }
                    
                    if (page.HasAMPLayout && !this.HttpContext.IsAmpRequest())
                    {
                        model.Links.Add(new MetadataLink
                        {
                            Rel = "amphtml",
                            Href = page.Url.EnsureEndsWith("/").Append("amp", '/')
                        });
                    }

                    return model;
                }, cacheKey);
            }

            return result;
        }

        public ActionResult SocialLinks()
        {
            var pageMetadata = GetContextItemMetadata();

            var model = new SocialLinksModel
            {
                CurrentPageAbsoluteUrl = this.Url.AbsoluteUrl(Sitecore.Context.RawUrl),
                CurrentPageTitle = pageMetadata?.OriginalTitle,
                TwitterHashTags = this.localMarketSettings?.TwitterHashtags ?? string.Empty,
                FacebookHashTags = this.localMarketSettings?.FacebookHashtags ?? string.Empty
            };
            return this.View("~/Views/Project/Metadata/SocialLinks.cshtml", model);
        }

        private IMetadata GetContextItemMetadata()
        {
            var contextItemId = Sitecore.Context.Item?.ID?.ToString();
            var cacheKey = $"page_metadata_{contextItemId}";

            IMetadata model = null;

            if (contextItemId.NotEmpty())
            {
                model = this._cacheManager.CacheResults(() =>
                {
                    var page = GetContextItem();
                    return this.metadataStrategyFactory.GetStrategy(page).GetMetadata(page);
                }, cacheKey);
            }

            return model;
        }

        private BasePage GetContextItem()
        {
            var contextItemId = Sitecore.Context.Item?.ID?.ToString();
            var cacheKey = $"context_page_{contextItemId}";

            return this._cacheManager.CacheResults(() => this.GetContextItem<BasePage>(false, true), cacheKey);
        }
    }
}
