﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using Delete.Foundation.DeleteFoundationCore.Controllers;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Glass.Mapper.Sc;
using RoyalCanin.Project.Metadata.Models;
using RoyalCanin.Project.Metadata.Strategies;

namespace RoyalCanin.Project.Metadata.Controllers
{
    public class GoogleAnalyticsController : BaseController
    {
        private readonly ICustomDimensionsStrategyFactory _dimensionsStrategyFactory;

        public GoogleAnalyticsController(ISitecoreContext sitecoreContext, IMapper mapper, ICacheManager cacheManager,
            ICustomDimensionsStrategyFactory dimensionsStrategyFactory) : base(sitecoreContext, mapper, cacheManager)
        {
            _dimensionsStrategyFactory = dimensionsStrategyFactory;
        }

        public ActionResult PushCustomDimensions()
        {
            if (Sitecore.Context.PageMode.IsExperienceEditor)
            {
                return new EmptyResult();
            }

            var contextItemId = Sitecore.Context.Item?.ID?.ToString();

            if (contextItemId.NotEmpty())
            {
                var cacheKey = $"global_dimensions_{contextItemId}";
                var model = _cacheManager.CacheResults(() =>
                {
                    var contextItem = GetContextItem<BasePage>(false, true);

                    return this._dimensionsStrategyFactory.GetStrategy(contextItem).GetCustomDimensions(contextItem);
                }, cacheKey);

                model.UserData = new Dictionary<string, string>
                {
                    { "sitecoreId", GetUserId() },
                    { "locale", GetContextLanguage()}
                };

                model.SessionData = new Dictionary<string, string>
                {
                    { "id", Session.SessionID }
                };

                return this.View("~/Views/Project/Metadata/_PushCustomDimensions.cshtml", model);
            }

            return new EmptyResult();
        }

        private string GetUserId()
        {
            var contactCookie = new Sitecore.Analytics.Web.ContactKeyCookie();
            return contactCookie.ContactId.ToString();
        }

        private string GetContextLanguage()
        {
            return Sitecore.Context.Language?.Name;
        }

    }
}
