﻿using Delete.Foundation.DeleteFoundationCore.Models;
using Microsoft.Extensions.DependencyInjection;
using RoyalCanin.Project.Metadata.Models;
using Sitecore.DependencyInjection;

namespace RoyalCanin.Project.Metadata.Strategies
{
    public interface ICustomDimensionsStrategyFactory
    {
        ICustomDimensionsStrategy<T> GetStrategy<T>(T item) where T : BasePage;
    }

    public class CustomDimensionsStrategyFactory : ICustomDimensionsStrategyFactory
    {
        public ICustomDimensionsStrategy<T> GetStrategy<T>(T item)
            where T : BasePage
        {
            var itemType = item.GetType();
            var metadataStrategyType = typeof(ICustomDimensionsStrategy<>).MakeGenericType(itemType);

            return ServiceLocator.ServiceProvider.GetService(metadataStrategyType) as ICustomDimensionsStrategy<T>
                   ?? ServiceLocator.ServiceProvider.GetService<ICustomDimensionsStrategy<BasePage>>() as ICustomDimensionsStrategy<T>;
        }
    }
}