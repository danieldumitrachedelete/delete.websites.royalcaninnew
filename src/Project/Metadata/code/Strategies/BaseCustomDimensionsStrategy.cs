﻿using System;
using System.Collections.Generic;
using Delete.Feature.Breeds.Models;
using Delete.Foundation.DataTemplates.Models.Taxonomy;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Glass.Mapper.Sc;
using RoyalCanin.Project.Metadata.Managers;
using RoyalCanin.Project.Metadata.Models;

namespace RoyalCanin.Project.Metadata.Strategies
{
    public interface ICustomDimensionsStrategy<T> where T: BasePage
    {
        CustomDimensionsObject GetCustomDimensions(T model);
    }

    public class BaseCustomDimensionsStrategy : ICustomDimensionsStrategy<BasePage>
    {
        protected readonly ICustomDimensionService _customDimensionService;
        protected readonly ISitecoreContext _sitecoreContext;

        private BasePage _model;

        public BaseCustomDimensionsStrategy(ICustomDimensionService customDimensionService, ISitecoreContext sitecoreContext)
        {
            _customDimensionService = customDimensionService;
            _sitecoreContext = sitecoreContext;
        }

        public virtual CustomDimensionsObject GetCustomDimensions(BasePage model)
        {
            this._model = model;

            var result = _customDimensionService.GetGlobalDimensions(model);

            AddSpecificDimensions(result);

            return result;
        }

        protected virtual void AddSpecificDimensions(CustomDimensionsObject result)
        {
            if (_model.IsDerivedFrom(TaxonomyConstants.TemplateId))
            {
                var taxonomyItem = _sitecoreContext.GetCurrentItem<Taxonomy>();
                var species = _customDimensionService.GetSpeciesDimension(taxonomyItem);
                var specieId = _customDimensionService.GetSpecieIdDimension(taxonomyItem);

                result.PageData.Add("topic", taxonomyItem.Name);
                if (species.NotEmpty())
                {
                    result.PageData.Add("theme", species);
                }

                if (specieId.NotEmpty())
                {
                    result.PetData = result.PetData ?? new Dictionary<string, string>();
                    result.PetData.Add("specieId", specieId);
                }
            }
        }

        protected string GetBreedDimension(Guid id)
        {
            var breed = _sitecoreContext.GetItem<GlobalBreed>(id, false, true);
            return breed?.LSHBreedID;
        }
    }
}