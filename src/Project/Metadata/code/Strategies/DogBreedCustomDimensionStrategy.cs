﻿using System;
using System.Collections.Generic;
using Delete.Foundation.DataTemplates.Models.Taxonomy;
using Glass.Mapper.Sc;
using RoyalCanin.Project.Metadata.Managers;
using RoyalCanin.Project.Metadata.Models;
using Sitecore.Mvc.Extensions;

namespace RoyalCanin.Project.Metadata.Strategies
{
    public class DogBreedCustomDimensionStrategy : BaseCustomDimensionsStrategy, ICustomDimensionsStrategy<DogBreedPage>
    {
        private DogBreedPage _model;
        public DogBreedCustomDimensionStrategy(ICustomDimensionService customDimensionService, ISitecoreContext sitecoreContext) : base(customDimensionService, sitecoreContext)
        {
        }

        public CustomDimensionsObject GetCustomDimensions(DogBreedPage model)
        {
            return this.GetCustomDimensions((BasePage)this._model);
        }

        public override CustomDimensionsObject GetCustomDimensions(BasePage model)
        {
            this._model = model as DogBreedPage;
            return base.GetCustomDimensions(model);
        }

        protected override void AddSpecificDimensions(CustomDimensionsObject result)
        {
            base.AddSpecificDimensions(result);

            var pageCustoms = new Dictionary<string, string>
            {
                {"theme", _customDimensionService.GetSpeciesDimension(Species.Dogs)},
                {"topic", _model.Name}
            };
            var petCustoms = new Dictionary<string, string>
            {
                {"breedId",  _model.GlobalBreed?.LSHBreedID},
                {"specieId", _customDimensionService.GetSpecieIdDimension(Species.Dogs)},
            };

            result.PageData.AddRange(pageCustoms);

            result.PetData = result.PetData ?? new Dictionary<string, string>();
            result.PetData.AddRange(petCustoms);
        }
    }
}