﻿using System;
using System.Collections.Generic;
using System.Linq;
using Glass.Mapper.Sc;
using RoyalCanin.Project.Metadata.Managers;
using RoyalCanin.Project.Metadata.Models;
using Sitecore.Mvc.Extensions;

namespace RoyalCanin.Project.Metadata.Strategies
{
    public class ArticleCustomDimensionStrategy : BaseCustomDimensionsStrategy, ICustomDimensionsStrategy<ArticlePage>
    {
        private ArticlePage _model;
        public ArticleCustomDimensionStrategy(ICustomDimensionService customDimensionService, ISitecoreContext sitecoreContext) : base(customDimensionService, sitecoreContext)
        {
        }

        public CustomDimensionsObject GetCustomDimensions(ArticlePage model)
        {
            return this.GetCustomDimensions((BasePage)this._model);
        }

        public override CustomDimensionsObject GetCustomDimensions(BasePage model)
        {
            this._model = model as ArticlePage;
            return base.GetCustomDimensions(model);
        }

        protected override void AddSpecificDimensions(CustomDimensionsObject result)
        {
            base.AddSpecificDimensions(result);

            var breedId = _model.Breeds.FirstOrDefault()?.Id;
            var breedDimension = breedId.HasValue ? GetBreedDimension(breedId.Value) : string.Empty;
            var lifeStageDimension = String.Join(",", _model.Lifestages.Select(x => x.Value));

            var pageCustoms = new Dictionary<string, string>
            {
                {"lifeStage", lifeStageDimension },
                {"contentTags",  String.Join(",", _model.ContentTags.Select(x => x.Value))}
            };

            var petCustoms = new Dictionary<string, string>
            {
                {"lifeStage",  lifeStageDimension},
                {"breedId", breedDimension },
            };

            result.PageData.AddRange(pageCustoms);
            result.PetData.AddRange(petCustoms);
        }
    }
}