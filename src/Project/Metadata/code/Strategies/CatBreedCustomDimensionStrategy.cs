﻿using System.Collections.Generic;
using Delete.Foundation.DataTemplates.Models.Taxonomy;
using Glass.Mapper.Sc;
using RoyalCanin.Project.Metadata.Managers;
using RoyalCanin.Project.Metadata.Models;
using Sitecore.Mvc.Extensions;

namespace RoyalCanin.Project.Metadata.Strategies
{
    public class CatBreedCustomDimensionStrategy : BaseCustomDimensionsStrategy, ICustomDimensionsStrategy<CatBreedPage>
    {
        private CatBreedPage _model;
        public CatBreedCustomDimensionStrategy(ICustomDimensionService customDimensionService, ISitecoreContext sitecoreContext) : base(customDimensionService, sitecoreContext)
        {
        }

        public CustomDimensionsObject GetCustomDimensions(CatBreedPage model)
        {
            return this.GetCustomDimensions((BasePage)this._model);
        }

        public override CustomDimensionsObject GetCustomDimensions(BasePage model)
        {
            this._model = model as CatBreedPage;
            return base.GetCustomDimensions(model);
        }

        protected override void AddSpecificDimensions(CustomDimensionsObject result)
        {
            base.AddSpecificDimensions(result);

            var pageCustoms = new Dictionary<string, string>
            {
                {"theme", _customDimensionService.GetSpeciesDimension(Species.Cats)},
                {"topic", _model.Name}
            };

            var petCustoms = new Dictionary<string, string>
            {
                {"breedId",  _model.GlobalBreed?.LSHBreedID},
                {"specieId", _customDimensionService.GetSpecieIdDimension(Species.Cats)},
            };

            result.PageData.AddRange(pageCustoms);

            result.PetData = result.PetData ?? new Dictionary<string, string>();
            result.PetData.AddRange(petCustoms);
        }
    }
}