﻿using System;
using System.Collections.Generic;
using System.Linq;
using Glass.Mapper.Sc;
using RoyalCanin.Project.Metadata.Managers;
using RoyalCanin.Project.Metadata.Models;
using Sitecore.Mvc.Extensions;

namespace RoyalCanin.Project.Metadata.Strategies
{
    public class ProductCustomDimensionStrategy : BaseCustomDimensionsStrategy, ICustomDimensionsStrategy<ProductPage>
    {
        private ProductPage _model;
        public ProductCustomDimensionStrategy(ICustomDimensionService customDimensionService, ISitecoreContext sitecoreContext) : base(customDimensionService, sitecoreContext)
        {
        }

        public CustomDimensionsObject GetCustomDimensions(ProductPage model)
        {
            return this.GetCustomDimensions((BasePage)this._model);
        }

        public override CustomDimensionsObject GetCustomDimensions(BasePage model)
        {
            this._model = model as ProductPage;
            return base.GetCustomDimensions(model);
        }

        protected override void AddSpecificDimensions(CustomDimensionsObject result)
        {
            base.AddSpecificDimensions(result);

            var lifeStageDimension = String.Join(",", _model.ExtendedLifestages.Select(x => x.Value));
            var breedDimension = GetBreedDimension(_model.ExtendedBreed);
            var species = _customDimensionService.GetSpeciesDimension(_model.GlobalProduct?.Specie?.Id);
            var speciesId = _customDimensionService.GetSpecieIdDimension(_model.GlobalProduct?.Specie?.Id);

            var pageCustoms = new Dictionary<string, string>
            {
                {"productId",  _model.GlobalProduct?.MainItemID},
                {"lifeStage", lifeStageDimension},
                {"theme", species},
                {"topic", _model.Name}
            };

            var petCustoms = new Dictionary<string, string>
            {
                {"lifeStage",  lifeStageDimension},
                {"breedId", breedDimension},
                {"specieId", speciesId},
            };

            result.PageData.AddRange(pageCustoms);

            result.PetData = result.PetData ?? new Dictionary<string, string>();
            result.PetData.AddRange(petCustoms);
        }
    }
}