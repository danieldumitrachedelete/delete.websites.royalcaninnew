﻿namespace RoyalCanin.Project.Metadata
{
    public class Constants
    {
        public class LinkTypes
        {
            public static string Canonical = "canonical";
            public static string Alternate = "alternate";
        }

        public class HrefLangTypes
        {
            public static string XDefault = "x-default";
        }
    }
}