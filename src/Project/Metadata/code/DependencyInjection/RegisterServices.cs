using Delete.Foundation.InitializeDependencyInjection.Extensions;
using Microsoft.Extensions.DependencyInjection;
using RoyalCanin.Project.Metadata.Managers;
using RoyalCanin.Project.Metadata.Strategies;
using Sitecore.DependencyInjection;

namespace RoyalCanin.Project.Metadata.DependencyInjection
{
    using MetadataStrategies;

    using Models;

    public class RegisterServices : IServicesConfigurator
    {
        public void Configure(IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IMetadataStrategy<BasePage>, BaseMetadataStrategy>();

            serviceCollection.AddScoped<IMetadataStrategy<ContentPage>, ContentPageMetadataStrategy>();

            serviceCollection.AddScoped<IMetadataStrategy<Homepage>, HomepageMetadataStrategy>();

            serviceCollection.AddScoped<IMetadataStrategy<CatBreedPage>, CatBreedPageMetadataStrategy>();

            serviceCollection.AddScoped<IMetadataStrategy<DogBreedPage>, DogBreedPageMetadataStrategy>();

            serviceCollection.AddScoped<IMetadataStrategy<ProductPage>, ProductPageMetadataStrategy>();

            serviceCollection.AddScoped<IMetadataStrategy<ArticlePage>, ArticlePageMetadataStrategy>();
            serviceCollection.AddScoped<IMetadataStrategy<NewsItemPage>, NewsItemPageMetadataStrategy>();
            serviceCollection.AddScoped<IMetadataStrategy<EventPage>, EventPageMetadataStrategy>();

            serviceCollection.AddScoped<IMetadataStrategyFactory, MetadataStrategyFactory>();

            serviceCollection.AddScoped<IPageSearchManager, PageSearchManager>();
            
            serviceCollection.AddScoped<ICustomDimensionService, CustomDimensionService>();

            serviceCollection.AddScoped<ICustomDimensionsStrategy<BasePage>, BaseCustomDimensionsStrategy>();
            serviceCollection.AddScoped<ICustomDimensionsStrategy<ProductPage>, ProductCustomDimensionStrategy>();
            serviceCollection.AddScoped<ICustomDimensionsStrategy<DogBreedPage>, DogBreedCustomDimensionStrategy>();
            serviceCollection.AddScoped<ICustomDimensionsStrategy<CatBreedPage>, CatBreedCustomDimensionStrategy>();
            serviceCollection.AddScoped<ICustomDimensionsStrategy<ArticlePage>, ArticleCustomDimensionStrategy>();

            serviceCollection.AddScoped<ICustomDimensionsStrategyFactory, CustomDimensionsStrategyFactory>();


            serviceCollection.AddMvcControllersInCurrentAssembly();
        }
    }
}
