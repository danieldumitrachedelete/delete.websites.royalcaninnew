﻿namespace RoyalCanin.Project.Metadata.MetadataStrategies
{
    using Models;

    public class HomepageMetadataStrategy : BaseMetadataStrategy, IMetadataStrategy<Homepage>
    {
        public IMetadata GetMetadata(Homepage model)
        {
            return base.GetMetadata(model);
        }
    }
}