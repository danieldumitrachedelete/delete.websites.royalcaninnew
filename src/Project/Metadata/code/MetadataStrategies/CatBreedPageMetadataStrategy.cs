﻿namespace RoyalCanin.Project.Metadata.MetadataStrategies
{
    using Delete.Foundation.DeleteFoundationCore.Extensions;

    using Models;

    public class CatBreedPageMetadataStrategy : BaseMetadataStrategy, IMetadataStrategy<CatBreedPage>
    {
        private CatBreedPage model;

        public IMetadata GetMetadata(CatBreedPage model)
        {
            return this.GetMetadata((BasePage)this.model);
        }

        public override IMetadata GetMetadata(BasePage model)
        {
            this.model = model as CatBreedPage;
            return base.GetMetadata(model);
        }

        protected override string GetDescription()
        {
            return this.model.GlobalBreed?.BreedDescription.OrDefault(base.GetDescription());
        }

        protected override string GetImageUrl()
        {
            return this.model?.GlobalBreed?.DesktopBreedHeroImage?.Src.OrDefault(base.GetImageUrl());
        }
    }
}