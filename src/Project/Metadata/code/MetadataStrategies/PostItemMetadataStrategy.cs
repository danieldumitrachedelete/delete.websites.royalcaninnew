﻿using Delete.Foundation.DataTemplates.Models.Interfaces;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Glass.Mapper.Sc;
using RoyalCanin.Project.Metadata.Models;

namespace RoyalCanin.Project.Metadata.MetadataStrategies
{
    public abstract class PostItemMetadataStrategy<TPage> : BaseMetadataStrategy, IMetadataStrategy<TPage>
        where TPage : BasePage
    {
        protected readonly ISitecoreContext sitecoreContext;

        protected TPage model;

        protected I_PostItem postItem;

        protected PostItemMetadataStrategy(ISitecoreContext sitecoreContext)
        {
            this.sitecoreContext = sitecoreContext;
        }
        
        public override IMetadata GetMetadata(BasePage model)
        {
            this.model = model as TPage;

            // TODO: do not use additional call to database, just add mapping between classes
            if (this.model != null)
            {
                this.postItem = this.sitecoreContext.GetItem<I_PostItem>(this.model.Id);
            }

            return base.GetMetadata(model);
        }

        protected override string GetTitle()
        {
            if (Model.MetaTitle.NotEmpty()) return AddTitleSuffix(this.Model.MetaTitle);
            return (this.postItem?.Heading).NotEmpty() ? AddTitleSuffix(this.postItem?.Heading) : base.GetTitle();
        }

        protected override string GetDescription()
        {
            if (Model.MetaDescription.NotEmpty()) return Model.MetaDescription;
            return this.postItem?.Summary?.OrDefault(base.GetDescription());
        }

        protected override string GetImageUrl()
        {
            return this.postItem?.ThumbnailImage?.Src.OrDefault(base.GetImageUrl());
        }

        public IMetadata GetMetadata(TPage m)
        {
            return this.GetMetadata((BasePage)m);
        }
    }
}