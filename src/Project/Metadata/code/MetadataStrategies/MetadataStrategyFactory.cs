﻿namespace RoyalCanin.Project.Metadata.MetadataStrategies
{
    using Microsoft.Extensions.DependencyInjection;

    using Models;

    using Sitecore.DependencyInjection;

    public interface IMetadataStrategyFactory
    {
        IMetadataStrategy<T> GetStrategy<T>(T item) where T : BasePage;
    }

    public class MetadataStrategyFactory : IMetadataStrategyFactory
    {
        public IMetadataStrategy<T> GetStrategy<T>(T item)
            where T : BasePage
        {
            var itemType = item.GetType();
            var metadataStrategyType = typeof(IMetadataStrategy<>).MakeGenericType(itemType);

            return ServiceLocator.ServiceProvider.GetService(metadataStrategyType) as IMetadataStrategy<T>
                           ?? ServiceLocator.ServiceProvider.GetService<IMetadataStrategy<BasePage>>() as IMetadataStrategy<T>;
        }
    }
}