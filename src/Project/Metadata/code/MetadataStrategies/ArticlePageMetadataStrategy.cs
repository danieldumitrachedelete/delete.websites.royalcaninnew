﻿using Glass.Mapper.Sc;
using RoyalCanin.Project.Metadata.Models;

namespace RoyalCanin.Project.Metadata.MetadataStrategies
{
    public class ArticlePageMetadataStrategy : PostItemMetadataStrategy<ArticlePage>, IMetadataStrategy<ArticlePage>
    {
        public ArticlePageMetadataStrategy(ISitecoreContext sitecoreContext) : base(sitecoreContext)
        {
            
        }
        
    }
}