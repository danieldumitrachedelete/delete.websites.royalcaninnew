﻿namespace RoyalCanin.Project.Metadata.MetadataStrategies
{
    using Delete.Foundation.DeleteFoundationCore.Extensions;
    using Delete.Foundation.DeleteFoundationCore.Helpers;
    using Models;

    public interface IMetadataStrategy<T>
        where T : BasePage
    { 
        IMetadata GetMetadata(T model);
    }

    public class BaseMetadataStrategy : IMetadataStrategy<BasePage>
    {
        private readonly string titleSuffix = TranslateHelper.TextByDomainCached("Translations", "translations.project.metadata.titlesuffix");

        protected BasePage Model { get; set; }
        
        public virtual IMetadata GetMetadata(BasePage model)
        {
            this.Model = model;
            return new Metadata
            {
                OriginalTitle = this.GetDefaultTitle(),
                Title = this.GetTitle(),
                Description = this.GetDescription(),
                Keywords = this.GetKeywords(),
                Url = this.GetUrl(),
                Image = this.GetImageUrl(),
                ContentType = this.GetContentType(),
                NoIndex = this.GetNoIndex()
            };
        }

        protected virtual string GetTitle()
        {
            return this.AddTitleSuffix(this.Model.MetaTitle.OrDefault(this.GetDefaultTitle()));
        }

        protected virtual string GetDefaultTitle()
        {
            return this.Model.DisplayName.OrDefault(this.Model.Name).Trim();
        }

        protected virtual string GetDescription()
        {
            return this.Model.MetaDescription;
        }

        protected virtual string GetKeywords()
        {
            return this.Model.MetaKeywords;
        }

        protected virtual string GetUrl()
        {
            return this.Model.AbsoluteUrl;
        }

        protected virtual string GetImageUrl()
        {
            return string.Empty;
        }

        protected virtual string GetContentType()
        {
            return "website";
        }

        protected virtual bool GetNoIndex()
        {
            return this.Model.NoIndex;
        }

        protected virtual string AddTitleSuffix(string title)
        {
            return title + (this.titleSuffix.IsNullOrEmpty() ? string.Empty : $" - {this.titleSuffix}");
        }
    }
}