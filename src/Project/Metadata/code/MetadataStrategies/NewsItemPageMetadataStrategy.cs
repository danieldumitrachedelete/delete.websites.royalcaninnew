﻿using Glass.Mapper.Sc;
using RoyalCanin.Project.Metadata.Models;

namespace RoyalCanin.Project.Metadata.MetadataStrategies
{
    public class NewsItemPageMetadataStrategy : PostItemMetadataStrategy<NewsItemPage>, IMetadataStrategy<NewsItemPage>
    {
        public NewsItemPageMetadataStrategy(ISitecoreContext sitecoreContext) : base(sitecoreContext)
        {
        }
    }
}