﻿namespace RoyalCanin.Project.Metadata.MetadataStrategies
{
    using Delete.Foundation.DeleteFoundationCore.Extensions;

    using Models;

    public class DogBreedPageMetadataStrategy : BaseMetadataStrategy, IMetadataStrategy<DogBreedPage>
    {
        private DogBreedPage model;

        public IMetadata GetMetadata(DogBreedPage model)
        {
            return this.GetMetadata((BasePage)this.model);
        }

        public override IMetadata GetMetadata(BasePage model)
        {
            this.model = model as DogBreedPage;
            return base.GetMetadata(model);
        }

        protected override string GetDescription()
        {
            return this.model.GlobalBreed?.BreedDescription.OrDefault(base.GetDescription());
        }

        protected override string GetImageUrl()
        {
            return this.model?.GlobalBreed?.DesktopBreedHeroImage?.Src.OrDefault(base.GetImageUrl());
        }
    }
}