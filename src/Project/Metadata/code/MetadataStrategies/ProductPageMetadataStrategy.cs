﻿namespace RoyalCanin.Project.Metadata.MetadataStrategies
{
    using Delete.Feature.Products.Models;
    using Delete.Foundation.DeleteFoundationCore.Extensions;

    using Glass.Mapper.Sc;

    using Models;

    public class ProductPageMetadataStrategy : BaseMetadataStrategy, IMetadataStrategy<ProductPage>
    {
        private readonly ISitecoreContext sitecoreContext;

        private ProductPage model;

        private LocalProduct product;

        public ProductPageMetadataStrategy(ISitecoreContext sitecoreContext)
        {
            this.sitecoreContext = sitecoreContext;
        }

        public IMetadata GetMetadata(ProductPage model)
        {
            return this.GetMetadata((BasePage)model);
        }

        public override IMetadata GetMetadata(BasePage model)
        {
            this.model = model as ProductPage;

            // TODO: do not use additional call to database, just add mapping between classes
            if (this.model != null)
            {
                this.product = this.sitecoreContext.GetItem<LocalProduct>(this.model.Id);
            }
            
            return base.GetMetadata(model);
        }

        protected override string GetDescription()
        {
            return this.product.ExtendedMainDescription.OrDefault(base.GetDescription());
        }

        protected override string GetImageUrl()
        {
            return this.product.ExtendedImage1_BAG?.Src.OrDefault(base.GetImageUrl());
        }
    }
}