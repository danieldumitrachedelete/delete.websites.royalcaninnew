﻿namespace RoyalCanin.Project.Metadata.MetadataStrategies
{
    using Models;

    public class ContentPageMetadataStrategy : BaseMetadataStrategy, IMetadataStrategy<ContentPage>
    {
        public IMetadata GetMetadata(ContentPage model)
        {
            return base.GetMetadata(model);
        }
    }
}