﻿using Glass.Mapper.Sc;
using RoyalCanin.Project.Metadata.Models;

namespace RoyalCanin.Project.Metadata.MetadataStrategies
{
    public class EventPageMetadataStrategy : PostItemMetadataStrategy<EventPage>, IMetadataStrategy<EventPage>
    {
        public EventPageMetadataStrategy(ISitecoreContext sitecoreContext) : base(sitecoreContext)
        {
         
        }
    }
}