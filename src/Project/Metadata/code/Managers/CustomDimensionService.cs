﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Delete.Foundation.DataTemplates.Models.Taxonomy;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.DeleteFoundationCore.Managers;
using Delete.Foundation.DeleteFoundationCore.Models;
using Delete.Foundation.MultiSite.Models;
using Glass.Mapper.Sc;
using RoyalCanin.Project.Metadata.Models;

namespace RoyalCanin.Project.Metadata.Managers
{
    public interface ICustomDimensionService
    {
        CustomDimensionsObject GetGlobalDimensions(GlassBase contextItem);
        string GetSpeciesDimension(Taxonomy contextItem);
        string GetSpeciesDimension(Guid? specieId);
        string GetSpeciesDimension(Species? species);
        string GetSpecieIdDimension(Taxonomy contextItem);
        string GetSpecieIdDimension(Species specie);
        string GetSpecieIdDimension(Guid? specieGuid);
    }

    public class CustomDimensionService : BaseSearchManager<GlassBase>, ICustomDimensionService
    {
        protected readonly string BaseSpeciesDimensionCacheKey = $"royalcanin|custom|{nameof(CustomDimensionService)}|{{0}}|{{1}}";
        protected readonly string BaseSpecieIdDimensionCacheKey = $"royalcanin|custom|{nameof(CustomDimensionService)}|{{0}}|{{1}}|specieId";
        protected readonly string BaseSpeciesCacheKey = $"royalcanin|custom|{nameof(CustomDimensionService)}|{{0}}|{{1}}";

        private readonly ICacheManager _cacheManager;
        private readonly ISitecoreContext _sitecoreContext;

        public CustomDimensionService(ICacheManager cacheManager, ISitecoreContext sitecoreContext)
        {
            _cacheManager = cacheManager;
            _sitecoreContext = sitecoreContext;
        }

        public CustomDimensionsObject GetGlobalDimensions(GlassBase contextItem)
        {
            var country = GetCountry();
            var websiteId = GetWebsiteId();
            var contentType = contextItem?.TemplateName;
            var environment = GetEnvironment();

            var result = new CustomDimensionsObject
            {
                SiteData = new Dictionary<string, string>
                {
                    {"country", country}, {"id", websiteId}, {"environment", environment},
                },
                PageData = new Dictionary<string, string>
                {
                    {"type", contentType},
                    {"hitTimestamp", DateTime.Now.ToString("O")}
                }
            };
            
            return result;
        }

        private string GetEnvironment()
        {
            return Sitecore.Configuration.Settings.GetSetting("RC.Environment");
        }

        private string GetWebsiteId()
        {
            var model = _sitecoreContext.GetRootItemRelative<LocalMarketSettings>();
            return model?.WebsiteId;
        }

        public string GetSpeciesDimension(Taxonomy contextItem)
        {
            var fullCacheKey = string.Format(BaseSpeciesDimensionCacheKey, Sitecore.Context.Site.Name, contextItem?.Id);
            return _cacheManager.CacheResults(() => GetSpecies(contextItem), fullCacheKey);
        }

        public string GetSpeciesDimension(Guid? specieId)
        {
            var species = SpecieConstants.SpeciesByGuid(specieId ?? Guid.Empty);
            return GetSpeciesDimension(species);
        }

        public string GetSpeciesDimension(Species? species)
        {
            return SpecieConstants.GetSeoString(species);
        }

        public string GetSpecieIdDimension(Taxonomy contextItem)
        {
            var fullCacheKey = string.Format(BaseSpecieIdDimensionCacheKey, Sitecore.Context.Site.Name, contextItem?.Id);
            return _cacheManager.CacheResults(() => GetSpecieId(contextItem), fullCacheKey);
        }

        public string GetSpecieIdDimension(Species specie)
        {
            var fullCacheKey = string.Format(BaseSpecieIdDimensionCacheKey, Sitecore.Context.Site.Name, specie);
            return _cacheManager.CacheResults(() => GetSpecieId(specie), fullCacheKey);
        }

        public string GetSpecieIdDimension(Guid? specieGuid)
        {
            var fullCacheKey = string.Format(BaseSpecieIdDimensionCacheKey, Sitecore.Context.Site.Name, specieGuid);
            return _cacheManager.CacheResults(() => GetSpecieId(specieGuid), fullCacheKey);
        }

        private string GetSpecieId(Taxonomy taxonomyItem)
        {
            if (taxonomyItem?.Species?.Count() == 1)
            {
                return GetSpecieId(SpecieConstants.SpeciesByGuid(taxonomyItem.Species.First()?.Id ?? Guid.Empty));
            }

            string result = null;

            var homeItem = _sitecoreContext.GetHomeItem<GlassBase>();
            var parent = taxonomyItem.GetAscendant(homeItem);

            if (parent != null && !parent.IsBucket)
            {
                var fullCacheKey = string.Format(BaseSpecieIdDimensionCacheKey, Sitecore.Context.Site.Name, taxonomyItem?.Id);
                result = _cacheManager.CacheResults(() => GetSpecieId(GetSpecies(parent)), fullCacheKey);
            }

            return result;
        }

        private string GetSpecieId(Species? specie)
        {
            if (specie.HasValue)
            {
                return ((int) specie.Value).ToString();
            }

            return string.Empty;
        }

        private string GetSpecieId(Guid? specieGuid)
        {
            if (specieGuid.HasValue)
            {
                return GetSpecieId(SpecieConstants.SpeciesByGuid(specieGuid.Value));
            }

            return null;
        }

        private string GetCountry()
        {
            var lcid = Sitecore.Context.Language?.CultureInfo?.LCID;

            if (lcid.HasValue && !Sitecore.Context.Language.CultureInfo.IsNeutralCulture)
            {
                return new RegionInfo(lcid.Value).TwoLetterISORegionName;
            }

            return Sitecore.Context.Language?.Name;
        }

        private string GetSpecies(Taxonomy taxonomyItem)
        {
            if (taxonomyItem?.Species?.Count() == 1)
            {
                var species = SpecieConstants.SpeciesByGuid(taxonomyItem.Species.First()?.Id ?? Guid.Empty);
                if (species.HasValue)
                {
                    return GetSpeciesDimension(species);
                }
            }

            string result = null;

            var homeItem = _sitecoreContext.GetHomeItem<GlassBase>();
            var parent = taxonomyItem.GetAscendant(homeItem);

            if (parent != null && !parent.IsBucket)
            {
                var fullCacheKey = string.Format(BaseSpeciesCacheKey, Sitecore.Context.Site.Name, taxonomyItem?.Id);
                result = _cacheManager.CacheResults(() => GetSpeciesDimension(GetSpecies(parent)), fullCacheKey);
            }

            return result;
        }

        private Species? GetSpecies(GlassBase root)
        {
            Species? result = null;

            var dogBreedItem = GetBreedItem(DogBreedPageConstants.TemplateId, root);
            if (dogBreedItem != null)
            {
                result = Species.Dogs;
            }
            else
            {
                var catBreedItem = GetBreedItem(CatBreedPageConstants.TemplateId, root);
                if (catBreedItem != null)
                {
                    result = Species.Cats;
                }
            }

            return result;
        }

        private GlassBase GetBreedItem(Sitecore.Data.ID template, GlassBase rootItem) 
        {
            var query = this.GetQueryable();
            query = this.FilterLocal(query, rootItem);
            query = query.Where(x => x.TemplateId == template);
            
            return query.FirstOrDefault();
        }

    }
}