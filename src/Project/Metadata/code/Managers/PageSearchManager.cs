﻿using System.Collections.Generic;
using System.Linq;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.DeleteFoundationCore.Managers;
using Delete.Foundation.DeleteFoundationCore.Models;
using RoyalCanin.Project.Metadata.Models;
using Sitecore.ContentSearch.Linq;
using Sitecore.ContentSearch.Linq.Utilities;

namespace RoyalCanin.Project.Metadata.Managers
{
    public interface IPageSearchManager : IBaseSearchManager<GlassBase>
    {
        IList<GlassBase> FindAlternativePages(string tenantPath, string relativePath);
    }

    public class PageSearchManager : BaseSearchManager<GlassBase>, IPageSearchManager
    {
        public IList<GlassBase> FindAlternativePages(string tenantPath, string relativePath)
        {
            var relativePathWords = relativePath.Split(' ');
            var lastWordInPath = relativePathWords.Last();

            var relativePathPredicate = PredicateBuilder.True<GlassBase>();
            foreach (var currentWord in relativePathWords)
            {
                relativePathPredicate = relativePathPredicate.And(x => x.Fullpath.Contains(currentWord));
            }

            return GetQueryable()
                .Where(x => x.IsLatestVersion)
                .Where(x => x.Fullpath.EndsWith(lastWordInPath))
                .Where(x => x.Fullpath.StartsWith(tenantPath))
                .Where(relativePathPredicate)
                .MapResults(SitecoreContext)
                .ToList();
        }

        protected override IQueryable<GlassBase> GetQueryable()
        {
            return SearchContext
                .GetQueryable<GlassBase>()
                .Where(i => i.Name != Delete.Foundation.DeleteFoundationCore.Constants.Constants.Sitecore.StandardValuesItemName && i.Name != Delete.Foundation.DeleteFoundationCore.Constants.Constants.Sitecore.BranchItemName);
        }
    }
}