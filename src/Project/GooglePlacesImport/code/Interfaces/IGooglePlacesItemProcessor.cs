﻿using System.Collections.Generic;
using Delete.Feature.Stockists.Models;
using Delete.Foundation.ItemImport.ImportProcessors;
using Delete.Foundation.ItemImport.Models;
using Sitecore.Globalization;

namespace RoyalCanin.Project.GooglePlacesImport.Interfaces
{
    public interface IGooglePlacesItemProcessor : IBaseImportItemProcessor<Stockist, StockistDto>
    {
        IEnumerable<Stockist> GetExistItems(Language language = null);
    }
}