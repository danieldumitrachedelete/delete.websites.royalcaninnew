﻿using Delete.Foundation.ItemImport.Importers;
using Delete.Foundation.ItemImport.Models;

namespace RoyalCanin.Project.GooglePlacesImport.Interfaces
{
    public interface IGooglePlacesImporter : IImporter
    {
        ImportLog Run();
    }
}