﻿using System.Collections.Generic;
using Delete.Foundation.ItemImport.Models;

namespace RoyalCanin.Project.GooglePlacesImport.Interfaces
{
    public interface IGooglePlacesService
    {
        IEnumerable<StockistDto> PopulateGoogleCoordinate(IEnumerable<StockistDto> existingStockists, ref List<ImportLogEntry> logEntries);
        IEnumerable<StockistDto> PopulateGooglePlacesIds(IEnumerable<StockistDto> existingStockists, bool reSearchPlaceId, ref List<ImportLogEntry> logEntries);
        IEnumerable<StockistDto> PopulateGooglePlacesData(IEnumerable<StockistDto> existingStockists, ref List<ImportLogEntry> logEntries);
    }
}