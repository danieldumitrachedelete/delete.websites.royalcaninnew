using System;
using AutoMapper;
using Delete.Feature.Stockists.Models;
using Delete.Foundation.DataTemplates.Models.Taxonomy;
using Delete.Foundation.ItemImport.Models;

namespace RoyalCanin.Project.GooglePlacesImport.Profiles
{
    public class GooglePlacesImportProfile : Profile
    {
        public GooglePlacesImportProfile()
        {
            this.CreateMap<ContactType, ContactTypeDto>()
                .ForMember(x => x.Code, opt => opt.MapFrom(z => z.ImportMappingValue))
                .ForAllOtherMembers(opt => opt.Ignore());

            this.CreateMap<ProfileType, ProfileTypeDto>()
                .ForMember(x => x.Code, opt => opt.MapFrom(z => z.ImportMappingValue))
                .ForAllOtherMembers(opt => opt.Ignore());

            this.CreateMap<Stockist, StockistDto>()
                .ForMember(x => x.CustomerReference, opt => opt.MapFrom(z => z.CustomerReference))
                .ForMember(x => x.CompanyName, opt => opt.MapFrom(z => z.CompanyName))
                .ForMember(x => x.ContactTypes, opt => opt.MapFrom(z => z.ContactTypes))
                .ForMember(x => x.Phone, opt => opt.MapFrom(z => z.Phone))
                .ForMember(x => x.Fax, opt => opt.MapFrom(z => z.Fax))
                .ForMember(x => x.Email, opt => opt.MapFrom(z => z.Email))
                .ForMember(x => x.WebsiteUrl,
                    opt => opt.MapFrom(z => z.WebsiteURL != null
                        ? z.WebsiteURL.Url
                        : String.Empty))
                .ForMember(x => x.Country, opt => opt.MapFrom(z => z.Country))
                .ForMember(x => x.CountyOrState, opt => opt.MapFrom(z => z.CountyOrState))
                .ForMember(x => x.Postcode, opt => opt.MapFrom(z => z.Postcode))
                .ForMember(x => x.AddressLine1, opt => opt.MapFrom(z => z.AddressLine1))
                .ForMember(x => x.AddressLine2, opt => opt.MapFrom(z => z.AddressLine2))
                .ForMember(x => x.AddressLine3, opt => opt.MapFrom(z => z.AddressLine3))
                .ForMember(x => x.City, opt => opt.MapFrom(z => z.City))
                .ForMember(x => x.Description, opt => opt.MapFrom(z => z.Description))
                .ForMember(x => x.ProfileType, opt => opt.MapFrom(z => z.ProfileType))
                .ForMember(x => x.Latitude, opt => opt.MapFrom(z => z.Latitude))
                .ForMember(x => x.Longitude, opt => opt.MapFrom(z => z.Longitude))
                .ForMember(x => x.GooglePlaceData,
                    opt => opt.MapFrom(z => new GooglePlaceDto
                    {
                        PlaceId = z.PlaceId,
                        BasicFormattedAddress = z.BasicFormattedAddress,
                        BasicName = z.BasicName,
                        BasicUrl = z.BasicUrl,
                        BasicDataImported = z.BasicDataImported,
                        ContactFormattedPhoneNumber = z.ContactFormattedPhoneNumber,
                        ContactOpeningHours = z.ContactOpeningHours,
                        ContactDataImported = z.ContactDataImported,
                        AtmosphereRating = z.AtmosphereRating,
                        AtmosphereDataImported = z.AtmosphereDataImported
                    }))
                .ForAllOtherMembers(opt => opt.Ignore());
        }
    }
}
