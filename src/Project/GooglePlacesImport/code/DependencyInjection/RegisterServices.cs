using Microsoft.Extensions.DependencyInjection;
using RoyalCanin.Project.GooglePlacesImport.Importers;
using RoyalCanin.Project.GooglePlacesImport.Interfaces;
using RoyalCanin.Project.GooglePlacesImport.Processors;
using RoyalCanin.Project.GooglePlacesImport.Services;
using Sitecore.DependencyInjection;

namespace RoyalCanin.Project.GooglePlacesImport.DependencyInjection
{
    public class RegisterServices : IServicesConfigurator
    {
        public void Configure(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IGooglePlacesImporter, GooglePlacesImporter>();
            serviceCollection.AddTransient<IGooglePlacesItemProcessor, GooglePlacesItemProcessor>();
            serviceCollection.AddTransient<IGooglePlacesService, GooglePlacesService>();
        }
    }
}
