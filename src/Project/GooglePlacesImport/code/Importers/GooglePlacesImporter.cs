﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using AutoMapper;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.ItemImport.Importers;
using Delete.Foundation.ItemImport.Models;
using Delete.Foundation.MultiSite.Models;
using Glass.Mapper.Sc;
using log4net;
using RoyalCanin.Project.GooglePlacesImport.Interfaces;
using Sitecore.Data.Managers;

namespace RoyalCanin.Project.GooglePlacesImport.Importers
{
    public class GooglePlacesImporter : BaseImporter, IGooglePlacesImporter
    {
        protected readonly ILog Logger;

        private readonly ISitecoreContext _sitecoreContext;
        private readonly ILocalMarketSettings _localMarketSettings;
        private readonly IGooglePlacesItemProcessor _googlePlacesItemProcessor;
        private readonly IMapper _mapper;
        private readonly IGooglePlacesService _googlePlacesService;
        
        public GooglePlacesImporter(
            ISitecoreContext sitecoreContext,
            ILocalMarketSettings localMarketSettings,
            IGooglePlacesItemProcessor googlePlacesItemProcessor,
            IMapper mapper,
            IGooglePlacesService googlePlacesService) : base(sitecoreContext)
        {
            Logger = LogManager.GetLogger("GooglePlacesImport");

            this._sitecoreContext = sitecoreContext;
            this._localMarketSettings = localMarketSettings;
            this._googlePlacesItemProcessor = googlePlacesItemProcessor;
            this._mapper = mapper;
            this._googlePlacesService = googlePlacesService;
        }

        public ImportLog Run()
        {
            var log = new ImportLog();

            if (!_localMarketSettings.UseGooglePlacesForStockists)
            {
                log.Entries.Add(new ImportLogEntry
                {
                    Level = MessageLevel.Error,
                    Message = "Google Places are not enabled for the market"
                });
                return log;
            }

            var languages = _localMarketSettings.StockistLanguageVersions
                .Select(x => LanguageManager.GetLanguage(x.Name))
                .ToList();

            if (!languages.Any())
            {
                log.Entries.Add(new ImportLogEntry
                {
                    Level = MessageLevel.Error,
                    Message = "No language versions specified for stockist import"
                });
                return log;
            }

            var existingStockists = _googlePlacesItemProcessor.GetExistItems(languages.First());
            var existingStockistsDtos = _mapper.Map<IEnumerable<StockistDto>>(existingStockists);

            var reSearchPlaceId = Sitecore.Configuration.Settings.GetBoolSetting("GooglePlaces.ReSearchPlaceId", false);

            var placesSearchLog = new List<ImportLogEntry>();

            var stockistWithCoordinate = _googlePlacesService.PopulateGoogleCoordinate(existingStockistsDtos, ref placesSearchLog);

            var stockistsWithPlaceId = _googlePlacesService.PopulateGooglePlacesIds(stockistWithCoordinate, reSearchPlaceId, ref placesSearchLog);

            var stockistsWithPlaceData = _googlePlacesService.PopulateGooglePlacesData(stockistsWithPlaceId, ref placesSearchLog);

            var mergedPlaceDataWithCoordinate = new List<StockistDto>(stockistsWithPlaceData);
            mergedPlaceDataWithCoordinate.AddRange(stockistWithCoordinate.Where(p2 =>
                stockistsWithPlaceData.All(p1 => p1.CustomerReference != p2.CustomerReference)));

            log.Entries.AddRange(placesSearchLog);

            foreach (var stockistDto in mergedPlaceDataWithCoordinate)
            {
                var sw = new Stopwatch();
                sw.Start();

                try
                {
                    var entry = new List<ImportLogEntry>();
                    var stockist = _googlePlacesItemProcessor.ProcessItem(stockistDto, languages);
                    sw.Stop();

                    var importLogEntry = new ImportLogEntry
                    {
                        Level = MessageLevel.Info,
                        Action = ImportAction.Imported,
                        Id = stockist.Id,
                        Message = $"{stockistDto.CompanyName} - Google Place data imported successfully, took {sw.ElapsedMilliseconds} ms"
                    };
                    WriteToLog(importLogEntry);
                    log.Entries.Add(importLogEntry);
                }
                catch (SkippableImportLogException e)
                {
                    var importLogEntry = e.Entry;
                    WriteToLog(importLogEntry);
                    log.Entries.Add(importLogEntry);
                }
                catch (Exception e)
                {
                    var importLogEntry = new ImportLogEntry
                    {
                        Level = MessageLevel.Error,
                        Message = $"{this.GetType()}: {e.GetAllMessages()} {e.StackTrace}"
                    };
                    WriteToLog(importLogEntry);
                    log.Entries.Add(importLogEntry);
                }
            }

            return log;
        }

        private void WriteToLog(IList<ImportLogEntry> importLogEntries)
        {
            foreach (var importLogEntry in importLogEntries)
            {
                WriteToLog(importLogEntry);
            }
        }

        private void WriteToLog(ImportLogEntry importLogEntry)
        {
            switch (importLogEntry.Level)
            {
                case MessageLevel.Error:
                    Logger.Error(importLogEntry.Message);
                    break;
                case MessageLevel.Critical:
                    Logger.Fatal(importLogEntry.Message);
                    break;
                case MessageLevel.Info:
                default:
                    Logger.Info(importLogEntry.Message);
                    break;
            }
        }
    }
}