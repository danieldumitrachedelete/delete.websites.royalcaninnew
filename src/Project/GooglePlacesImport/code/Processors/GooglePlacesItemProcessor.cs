﻿using System;
using System.Collections.Generic;
using System.Linq;
using Delete.Feature.Stockists.Models;
using Delete.Foundation.ItemImport.ImportProcessors;
using Delete.Foundation.ItemImport.LogManagerImport;
using Delete.Foundation.ItemImport.Models;
using Delete.Foundation.MultiSite.Models;
using Glass.Mapper.Sc;
using RoyalCanin.Project.GooglePlacesImport.Interfaces;
using Sitecore.Globalization;

namespace RoyalCanin.Project.GooglePlacesImport.Processors
{
    public class GooglePlacesItemProcessor : BaseImportItemProcessor<Stockist, StockistDto>, IGooglePlacesItemProcessor
    {
        protected readonly ILocalMarketSettings LocalMarketSettings;

        public GooglePlacesItemProcessor(
            ISitecoreContext sitecoreContext,
            ILocalMarketSettings localMarketSettings,
            ILogManagerImport logManagerImport,
            string locationPathOverride = null) : base(sitecoreContext, logManagerImport,  locationPathOverride)
        {
            this.LocalMarketSettings = localMarketSettings;
        }

        protected override string DefaultLocation => string.Empty;

        protected override Func<Stockist, string> IdStringFromSitecoreItem => stockist => stockist.CustomerReference?.Trim();

        protected override Func<StockistDto, string> IdStringFromImportObj => stockistRecord => stockistRecord.CustomerReference?.Trim();

        protected override Func<StockistDto, string> ItemNameFromImportObj => stockistRecord => stockistRecord.CompanyName?.Trim();

        protected override bool MapDatabaseFields => true;

        public override Stockist ProcessItem(StockistDto importObj, IEnumerable<Language> languageVersions,
            
            string pathOverride = null)
        {
            if (importObj == null || languageVersions == null || !languageVersions.Any())
            {
                throw new SkippableImportLogException
                {
                    Entry = new ImportLogEntry
                    {
                        Level = MessageLevel.Error,
                        Message = "No language versions specified for stockist import"
                    }
                };
            }

            var defaultLanguage = languageVersions.First();

            var newId = this.CalculateItemId(importObj);
            var itemLocationInTargetContext = pathOverride ?? this.CalculateItemLocation(importObj);

            var defaultItem = this.GetItem(newId, importObj, itemLocationInTargetContext, defaultLanguage);

            if (defaultItem == null)
            {
                throw new SkippableImportLogException
                {
                    Entry = new ImportLogEntry
                    {
                        Level = MessageLevel.Error,
                        Message = $"Stockist {importObj.CompanyName} ({newId}) cannot be found in Stockists index"
                    }
                };
            }

            defaultItem = this.MapDefaultVersionFields(defaultItem, importObj);

            foreach (var language in languageVersions)
            {
                defaultItem.Language = language.Name;
                this.SaveItem(defaultItem);
            }

            return defaultItem;
        }

        public IEnumerable<Stockist> GetExistItems(Language language = null)
        {
            return this.GetItems(this.CalculateItemLocation(null), language);
        }

        protected override Stockist MapDefaultVersionFields(Stockist item, StockistDto importObj)
        {
            item = base.MapDefaultVersionFields(item, importObj);

            item.Latitude = importObj.Latitude;
            item.Longitude = importObj.Longitude;
            item.PlaceId = importObj.GooglePlaceData?.PlaceId;

            item.BasicFormattedAddress = importObj.GooglePlaceData?.BasicFormattedAddress;
            item.BasicName = importObj.GooglePlaceData?.BasicName;
            item.BasicUrl = importObj.GooglePlaceData?.BasicUrl;
            item.BasicDataImported = importObj.GooglePlaceData?.BasicDataImported ?? new DateTime();

            item.ContactFormattedPhoneNumber = importObj.GooglePlaceData?.ContactFormattedPhoneNumber;
            item.ContactOpeningHours = importObj.GooglePlaceData?.ContactOpeningHours;
            item.ContactDataImported = importObj.GooglePlaceData?.ContactDataImported ?? new DateTime();

            item.AtmosphereRating = importObj.GooglePlaceData?.AtmosphereRating ?? Decimal.Zero;
            item.AtmosphereDataImported = importObj.GooglePlaceData?.AtmosphereDataImported ?? new DateTime();

            return item;
        }

        protected override string CalculateItemLocation(StockistDto importObj)
        {
            return this.LocalMarketSettings.StockistImportItemsPath.Fullpath;
        }
    }
}