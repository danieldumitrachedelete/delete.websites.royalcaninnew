﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using Delete.Foundation.ItemImport.Models;
using Delete.Foundation.ItemImport.Repositories;
using Delete.Foundation.MultiSite.Models;
using Glass.Mapper.Sc;
using log4net;
using Newtonsoft.Json;
using RoyalCanin.Project.GooglePlacesImport.Interfaces;
using RoyalCanin.Project.GooglePlacesImport.Models;
using RoyalCanin.Project.GooglePlacesImport.ModelsForSearchCoordinates;

namespace RoyalCanin.Project.GooglePlacesImport.Services
{
    public class GooglePlacesService : IGooglePlacesService
    {
        protected readonly ILog Logger;

        protected readonly ILocalMarketSettings LocalMarketSettings;

        protected readonly int MaxNumberOfThreads;

        public GooglePlacesService(
            ILocalMarketSettings localMarketSettings)
        {
            Logger = LogManager.GetLogger("GooglePlacesImport");
            this.LocalMarketSettings = localMarketSettings;
            this.MaxNumberOfThreads = Sitecore.Configuration.Settings.GetIntSetting("GooglePlaces.MaxNumberOfThreads", 100);
        }

        public IEnumerable<StockistDto> PopulateGoogleCoordinate(IEnumerable<StockistDto> existingStockists, ref List<ImportLogEntry> logEntries)
        {
            var baseUrl = LocalMarketSettings.GoogleCoordinatesSearchRequest;
            var key = LocalMarketSettings.GoogleApiKey;
            var logs = new ConcurrentBag<ImportLogEntry>();
            IEnumerable<StockistDto> stockistsToSearchCoordinate;
            
            stockistsToSearchCoordinate = existingStockists;

            try
            {
                foreach (var stockist in stockistsToSearchCoordinate)
                {
                    if (!stockist.Latitude.Equals(0)) continue;
                    var searchString = HttpUtility.UrlEncode(
                        $"{stockist.CompanyName} {stockist.AddressLine1}");
                    var requestUrl = string.Format(baseUrl, searchString, key);

                    var request = WebRequest.Create(requestUrl);
                    try
                    {
                        var responseStream = request.GetResponse().GetResponseStream();

                        string responseText;
                        using (var sr = new StreamReader(responseStream))
                        {
                            responseText = sr.ReadToEnd();
                        }

                        var searchResults = JsonConvert.DeserializeObject<GooglePlacesSearchCoordinatesResponse>(responseText);

                        if (searchResults.Status == "OK")
                        {
                            if (searchResults.Candidates.Count > 1)
                            {
                                var logEntry = new ImportLogEntry
                                {
                                    Message = $"{stockist.CompanyName} - Multiple Google Place Coordinates found",
                                    Action = ImportAction.Rejected,
                                    Level = MessageLevel.Info
                                };
                                this.Logger.Info(logEntry.Message);
                                logs.Add(logEntry);
                            }
                            else
                            {
                                var result = searchResults.Candidates.FirstOrDefault();
                                stockist.Latitude = Convert.ToDecimal(result.Geometry.Location.Lat);
                                stockist.Longitude = Convert.ToDecimal(result.Geometry.Location.Lng);
                                var logEntry = new ImportLogEntry
                                {
                                    Message = $"{stockist.CompanyName} - Insert coordinates",
                                    Action = ImportAction.Rejected,
                                    Level = MessageLevel.Info
                                };
                                this.Logger.Info(logEntry.Message);
                                logs.Add(logEntry);

                            }

                        }
                        else
                        {
                            if (searchResults.Status == "ZERO_RESULTS")
                            {
                                var logEntry = new ImportLogEntry
                                {
                                    Message = $"{stockist.CompanyName} - Not found coordinates",
                                    Action = ImportAction.Rejected,
                                    Level = MessageLevel.Info
                                };
                                this.Logger.Info(logEntry.Message);
                                logs.Add(logEntry);
                            }
                        }
                    }
                    catch (Exception exception)
                    {
                        var logEntry = new ImportLogEntry
                        {
                            Message = $"{stockist.CompanyName} - error searching Google coordinates\nRequest: {requestUrl}",
                            Action = ImportAction.Rejected,
                            Level = MessageLevel.Info
                        };
                        this.Logger.Error(logEntry.Message, exception);
                        logs.Add(logEntry);
                    }
                }
            }
            catch (Exception exception)
            {
                var logEntry = new ImportLogEntry
                {
                    Message = $"Error searching Google Place IDs",
                    Action = ImportAction.Undefined,
                    Level = MessageLevel.Error
                };
                this.Logger.Error(logEntry.Message, exception);
                logs.Add(logEntry);
            }

            logEntries.AddRange(logs);

            return stockistsToSearchCoordinate;

        }

        public IEnumerable<StockistDto> PopulateGooglePlacesIds(IEnumerable<StockistDto> existingStockists, bool reSearchPlaceId, ref List<ImportLogEntry> logEntries)
        {
            var baseUrl = LocalMarketSettings.GooglePlaceSearchRequest;
            var key = LocalMarketSettings.GoogleApiKey;
            var logs = new ConcurrentBag<ImportLogEntry>();
            ConcurrentBag<StockistDto> stockists;

            IEnumerable<StockistDto> stockistsToSearchPlaceId;
            if (reSearchPlaceId)
            {
                stockistsToSearchPlaceId = existingStockists;
                stockists = new ConcurrentBag<StockistDto>();
            }
            else
            {
                stockistsToSearchPlaceId = existingStockists.Where(x =>
                    x.GooglePlaceData == null || string.IsNullOrWhiteSpace(x.GooglePlaceData.PlaceId));
                stockists = new ConcurrentBag<StockistDto>(existingStockists.Where(x =>
                    x.GooglePlaceData != null && !string.IsNullOrWhiteSpace(x.GooglePlaceData.PlaceId)));
            }

            try
            {
                Parallel.ForEach(stockistsToSearchPlaceId, new ParallelOptions { MaxDegreeOfParallelism = this.MaxNumberOfThreads }, stockist =>
                {
                    if (stockist.GooglePlaceData == null)
                    {
                        stockist.GooglePlaceData = new GooglePlaceDto();
                    }

                    var searchString = HttpUtility.UrlEncode(
                        $"{stockist.CompanyName} {stockist.AddressLine1} {stockist.City} {stockist.CountyOrState} {stockist.Postcode}");
                    var requestUrl = string.Format(baseUrl, key, searchString,
                        stockist.Latitude.ToString(CultureInfo.InvariantCulture),
                        stockist.Longitude.ToString(CultureInfo.InvariantCulture));

                    var request = WebRequest.Create(requestUrl);
                    try
                    {
                        var responseStream = request.GetResponse().GetResponseStream();

                        string responseText;
                        using (var sr = new StreamReader(responseStream))
                        {
                            responseText = sr.ReadToEnd();
                        }

                        var searchResults = JsonConvert.DeserializeObject<GooglePlacesSearchResponse>(responseText);

                        if (searchResults.Candidates != null && searchResults.Candidates.Count(x => !x.PermanentlyClosed) == 1)
                        {
                            stockist.GooglePlaceData.PlaceId = searchResults.Candidates.First(x => !x.PermanentlyClosed).PlaceId;
                            stockists.Add(stockist);
                        }
                        else
                        {
                            if (searchResults.Candidates != null &&
                                searchResults.Candidates.Count(x => !x.PermanentlyClosed) > 1)
                            {
                                var logEntry = new ImportLogEntry
                                {
                                    Message = $"{stockist.CompanyName} - Multiple Google Place IDs found: {string.Join(", ", searchResults.Candidates.Select(x => x.PlaceId))}\nRequest: {requestUrl}\nResponse: {responseText}",
                                    Action = ImportAction.Rejected,
                                    Level = MessageLevel.Info
                                };
                                this.Logger.Info(logEntry.Message);
                                logs.Add(logEntry);
                            }
                            else if (searchResults.Candidates != null &&
                                     searchResults.Candidates.Any(x => x.PermanentlyClosed))
                            {
                                var logEntry = new ImportLogEntry
                                {
                                    Message = $"{stockist.CompanyName} - Google Place is permanently closed\nRequest: {requestUrl}\nResponse: {responseText}",
                                    Action = ImportAction.Rejected,
                                    Level = MessageLevel.Info
                                };
                                this.Logger.Info(logEntry.Message);
                                logs.Add(logEntry);
                            }
                            else
                            {
                                var logEntry = new ImportLogEntry
                                {
                                    Message = $"{stockist.CompanyName} - No Google Place IDs found\nRequest: {requestUrl}\nResponse: {responseText}",
                                    Action = ImportAction.Rejected,
                                    Level = MessageLevel.Info
                                };
                                this.Logger.Info(logEntry.Message);
                                logs.Add(logEntry);
                            }
                        }
                    }
                    catch (Exception exception)
                    {
                        var logEntry = new ImportLogEntry
                        {
                            Message = $"{stockist.CompanyName} - error searching Google Place ID\nRequest: {requestUrl}",
                            Action = ImportAction.Rejected,
                            Level = MessageLevel.Info
                        };
                        this.Logger.Error(logEntry.Message, exception);
                        logs.Add(logEntry);
                    }
                });
            }
            catch (Exception exception)
            {
                var logEntry = new ImportLogEntry
                {
                    Message = $"Error searching Google Place IDs",
                    Action = ImportAction.Undefined,
                    Level = MessageLevel.Error
                };
                this.Logger.Error(logEntry.Message, exception);
                logs.Add(logEntry);
            }

            logEntries.AddRange(logs);

            return stockists;
        }

        public IEnumerable<StockistDto> PopulateGooglePlacesData(IEnumerable<StockistDto> existingStockists, ref List<ImportLogEntry> logEntries)
        {
            var baseUrl = LocalMarketSettings.GooglePlaceDetailsByIdRequest;
            var key = LocalMarketSettings.GoogleApiKey;
            var basicFields = LocalMarketSettings.GooglePlaceBasicDataFields;
            var basicDataCacheMinutes = LocalMarketSettings.GooglePlaceBasicDataCacheMinutes;
            var contactFields = LocalMarketSettings.GooglePlaceContactDataFields;
            var contactDataCacheMinutes = LocalMarketSettings.GooglePlaceContactDataCacheMinutes;
            var atmosphereFields = LocalMarketSettings.GooglePlaceAtmosphereDataFields;
            var atmosphereDataCacheMinutes = LocalMarketSettings.GooglePlaceAtmosphereDataCacheMinutes;

            if (basicDataCacheMinutes <= 0 || contactDataCacheMinutes <= 0 || atmosphereDataCacheMinutes <= 0 ||
                string.IsNullOrWhiteSpace(basicFields) || string.IsNullOrWhiteSpace(contactFields) ||
                string.IsNullOrWhiteSpace(atmosphereFields))
            {
                var logEntry = new ImportLogEntry
                {
                    Message = $"Google Places Data import settings are incomplete or empty",
                    Action = ImportAction.Undefined,
                    Level = MessageLevel.Error
                };
                this.Logger.Error(logEntry.Message);
                logEntries.Add(logEntry);
                return new List<StockistDto>();
            }

            var logs = new ConcurrentBag<ImportLogEntry>();
            var stockists = new ConcurrentBag<StockistDto>();

            try
            {
                Parallel.ForEach(existingStockists, new ParallelOptions { MaxDegreeOfParallelism = this.MaxNumberOfThreads }, stockist =>
                {
                    if (stockist.GooglePlaceData == null || string.IsNullOrWhiteSpace(stockist.GooglePlaceData.PlaceId))
                    {
                        var logEntry = new ImportLogEntry
                        {
                            Message = $"{stockist.CompanyName} - No Google Place IDs specified",
                            Action = ImportAction.Rejected,
                            Level = MessageLevel.Info
                        };
                        this.Logger.Info(logEntry.Message);
                        logs.Add(logEntry);
                        return;
                    }

                    var fields = new List<string>();

                    if (stockist.GooglePlaceData.BasicDataImported.AddMinutes(basicDataCacheMinutes) < DateTime.Now) fields.Add(basicFields);
                    if (stockist.GooglePlaceData.ContactDataImported.AddMinutes(contactDataCacheMinutes) < DateTime.Now) fields.Add(contactFields);
                    if (stockist.GooglePlaceData.AtmosphereDataImported.AddMinutes(atmosphereDataCacheMinutes) < DateTime.Now) fields.Add(atmosphereFields);

                    if (fields.Any())
                    {
                        var allFields = string.Join(",", fields);
                        var requestUrl = string.Format(baseUrl, key, stockist.GooglePlaceData.PlaceId, allFields);
                        var request = WebRequest.Create(requestUrl);

                        try
                        {
                            var responseStream = request.GetResponse().GetResponseStream();

                            string responseText;
                            using (var sr = new StreamReader(responseStream))
                            {
                                responseText = sr.ReadToEnd();
                            }

                            var placeData = JsonConvert.DeserializeObject<GooglePlacesGetByIdResponse>(responseText);

                            if (fields.Contains(basicFields, StringComparer.OrdinalIgnoreCase))
                            {
                                stockist.GooglePlaceData.BasicFormattedAddress = placeData?.Result?.FormattedAddress;
                                stockist.GooglePlaceData.BasicName = placeData?.Result?.Name;
                                stockist.GooglePlaceData.BasicUrl = placeData?.Result?.Url;
                                stockist.GooglePlaceData.BasicDataImported = DateTime.Now;
                            }

                            if (fields.Contains(contactFields, StringComparer.OrdinalIgnoreCase))
                            {
                                stockist.GooglePlaceData.ContactFormattedPhoneNumber = placeData?.Result?.FormattedPhoneNumber;
                                stockist.GooglePlaceData.ContactOpeningHours = placeData?.Result?.OpeningHours?.WeekdayText != null
                                    ? string.Join("\r\n", placeData.Result.OpeningHours.WeekdayText)
                                    : null;
                                stockist.GooglePlaceData.ContactDataImported = DateTime.Now;
                            }

                            if (fields.Contains(atmosphereFields, StringComparer.OrdinalIgnoreCase))
                            {
                                stockist.GooglePlaceData.AtmosphereRating = placeData?.Result?.Rating ?? Decimal.Zero;
                                stockist.GooglePlaceData.AtmosphereDataImported = DateTime.Now;
                            }

                            stockists.Add(stockist);
                        }
                        catch (Exception exception)
                        {
                            var logEntry = new ImportLogEntry
                            {
                                Message = $"{stockist.CompanyName} - error getting Google Place Data\nRequest: {requestUrl}",
                                Action = ImportAction.Rejected,
                                Level = MessageLevel.Info
                            };
                            this.Logger.Error(logEntry.Message, exception);
                            logs.Add(logEntry);
                        }
                    }
                });
            }
            catch (Exception exception)
            {
                var logEntry = new ImportLogEntry
                {
                    Message = $"Error getting Google Places Data",
                    Action = ImportAction.Undefined,
                    Level = MessageLevel.Error
                };
                this.Logger.Error(logEntry.Message, exception);
                logs.Add(logEntry);
            }

            logEntries.AddRange(logs);

            return stockists;
        }
    }
}