﻿using System;
using System.Collections.Generic;
using System.Linq;
using Delete.Feature.Stockists.Models;
using Delete.Foundation.DeleteFoundationCore.Constants;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Managers;
using Delete.Foundation.DeleteFoundationCore.Models;
using Delete.Foundation.ItemImport.Importers;
using Delete.Foundation.ItemImport.Loggers;
using Delete.Foundation.ItemImport.Models;
using Delete.Foundation.MultiSite.Helpers;
using Delete.Foundation.MultiSite.Models;
using Glass.Mapper.Sc;
using log4net;
using Microsoft.Extensions.DependencyInjection;
using Quartz;
using RoyalCanin.Project.GooglePlacesImport.Interfaces;
using Sitecore.ContentSearch;
using Sitecore.Data;
using Sitecore.Data.Events;
using Sitecore.DependencyInjection;
using Sitecore.SecurityModel;
using Sitecore.Sites;
using Sitecron.SitecronSettings;
using Delete.Foundation.DeleteFoundationCore.Interfaces;

namespace RoyalCanin.Project.GooglePlacesImport.SitecronTasks
{
    public class ImportGooglePlaces : IJob
    {
        private readonly ILog _logger;

        private readonly IImportJobLogger _importJobLogger;

        private ICacheManager cacheManager;

        public ImportGooglePlaces()
        {
            _logger = LogManager.GetLogger("GooglePlacesImport");
            _importJobLogger = new ImportJobLogger(_logger);
            this.cacheManager = new CacheManager();
        }

        public void Execute(IJobExecutionContext context)
        {
            try
            {
                ClearCache("start");
                var jobDataMap = context.JobDetail.JobDataMap;
                var items = jobDataMap.GetString(SitecronConstants.FieldNames.Items);
                var siteRoots = items.ToGuidList("|");

                if (siteRoots.Any())
                {
                    this._logger.Info($"Sitecron - Job {nameof(ImportGooglePlaces)} - start");

                    try
                    {
                        foreach (var siteRoot in siteRoots)
                        {
                            try
                            {
                                var rootPath = Database.GetDatabase(Constants.Sitecore.Databases.Master)?.GetItem(new ID(siteRoot))?.Paths.FullPath;
                                var sites = SiteManagerHelper.GetWebsitesByRoot(rootPath);
                                foreach (var site in sites)
                                {
                                    IEnumerable<string> languagesToPublish = null;
                                    try
                                    {
                                        var itemsToPublish = new List<Guid>();
                                        var siteContext = SiteContext.GetSite(site.Name);
                                        ClearCache($"start [{siteContext.Language}]");

                                        this._logger.Info($"Sitecron - Job {nameof(ImportGooglePlaces)} - site in processing RootPath: {siteContext.RootPath}; Language: {siteContext.Language}; Database: {siteContext.Database}");

                                        using (new SecurityDisabler())
                                        using (new SiteContextSwitcher(siteContext))
                                        using (new Sitecore.Globalization.LanguageSwitcher(siteContext.Language))
                                        using (new EventDisabler())
                                        using (new BulkUpdateContext())
                                        using (var innerScope = ServiceLocator.ServiceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope())
                                        {
                                            var logs = innerScope.ServiceProvider.GetService<IGooglePlacesImporter>().Run();
                                            var sitecoreContext = innerScope.ServiceProvider.GetService<ISitecoreContext>();
                                            var settings = sitecoreContext.GetItem<LocalMarketSettings>(siteContext.RootPath);
                                            languagesToPublish = settings.StockistLanguageVersions.Select(l => l.Name).ToList();

                                            _importJobLogger.LogImportResults(logs.Entries);

                                            if (settings.StockistImportItemsPath != null &&
                                                settings.StockistImportItemsPath.Id != Guid.Empty)
                                            {
                                                itemsToPublish.Add(settings.StockistImportItemsPath.Id);
                                                RefreshIndexes(itemsToPublish, sitecoreContext);
                                            }
                                        }
                                        using (new SecurityDisabler())
                                        using (new SiteContextSwitcher(siteContext))
                                        {
                                            foreach (var lang in languagesToPublish)
                                            { 
                                                using (new Sitecore.Globalization.LanguageSwitcher(lang))
                                                { 
                                                    PublishItems(itemsToPublish);
                                                }
                                            }
                                        }

                                        this._logger.Info($"Sitecron - Job {nameof(ImportGooglePlaces)} - import {siteContext.Language} stockists finished successfully");
                                    }
                                    catch (Exception e)
                                    {
                                        this._logger.Error($"Sitecron - Job {nameof(ImportGooglePlaces)} - error processing language", e);
                                    }
                                }
                            }
                            catch (Exception e)
                            {
                                this._logger.Error($"Sitecron - Job {nameof(ImportGooglePlaces)} - error processing market", e);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        this._logger.Error($"Sitecron - Job {nameof(ImportGooglePlaces)} - error", e);
                    }
                }
                else
                {
                    this._logger.Info($"Sitecron - Job {nameof(ImportGooglePlaces)} - skipped because no sites are selected");
                }
            }
            catch (Exception e)
            {
                this._logger.Error($"Sitecron - Job {nameof(ImportGooglePlaces)} - error", e);
            }

            this._logger.Info($"Sitecron - Job {nameof(ImportGooglePlaces)} - finished successfully");
            ClearCache("end");
        }

        private void ClearCache(string position)
        {
            try
            {
                if (this.cacheManager == null)
                {
                    throw new NullReferenceException();
                }
                this.cacheManager.Clear();
                _logger.Info($"Sitecron - Job {nameof(ImportGooglePlaces)} - cache clearning succeed with {nameof(ImportGooglePlaces)} on {position}");
            }
            catch (Exception ex)
            {
                _logger.Error($"Sitecron - Job {nameof(ImportGooglePlaces)} - cache clearning error with {nameof(ImportGooglePlaces)} on {position}. Exception: {ex.Message}");
            }
        }

        private void RebuildIndexes()
        {
            this._logger.Info($"Sitecron - Job {nameof(ImportGooglePlaces)} - rebuilding index start");
            var targetIndexName = SearchManagersConfiguration.GetIndexNameByType(typeof(Stockist), Constants.Sitecore.Databases.Master);
            this._logger.Info($"Sitecron - Job {nameof(ImportGooglePlaces)} - {targetIndexName} - target index name");
            var index = ContentSearchManager.GetIndex(targetIndexName);

            index.Rebuild(IndexingOptions.Default);
            this._logger.Info($"Sitecron - Job {nameof(ImportGooglePlaces)} - {targetIndexName} - commit transactions");
            index.CreateUpdateContext().Commit();
            this._logger.Info($"Sitecron - Job {nameof(ImportGooglePlaces)} - rebuilding index finished");
        }

        private void RefreshIndexes(List<Guid> itemsToPublish, ISitecoreContext sitecoreContext)
        {
            this._logger.Info($"Sitecron - Job {nameof(ImportGooglePlaces)} - refreshing index start");
            var targetIndexName =
                SearchManagersConfiguration.GetIndexNameByType(typeof(Stockist), sitecoreContext.Database.Name);
            this._logger.Info($"Sitecron - Job {nameof(ImportGooglePlaces)} - {targetIndexName} - target index name");

            var index = ContentSearchManager.GetIndex(targetIndexName);
            foreach (var itemGuid in itemsToPublish)
            {
                var startingPointItem = sitecoreContext.Database.GetItem(new ID(itemGuid));
                var indexableStartingPoint = new SitecoreIndexableItem(startingPointItem);
                this._logger.Info($"Sitecron - Job {nameof(ImportGooglePlaces)} - running refresh for {startingPointItem.Paths.FullPath}");

                index.Refresh(indexableStartingPoint, IndexingOptions.Default);
                this._logger.Info($"Sitecron - Job {nameof(ImportGooglePlaces)} - refresh done for {startingPointItem.Paths.FullPath}");
            }

            this._logger.Info($"Sitecron - Job {nameof(ImportGooglePlaces)} - {targetIndexName} - commit transactions");
            index.CreateUpdateContext().Commit();
            this._logger.Info($"Sitecron - Job {nameof(ImportGooglePlaces)} - refreshing index finished");
            this._logger.Info("");
        }

        private void PublishItems(IList<Guid> roots)
        {
            this._logger.Info($"Sitecron - Job {nameof(ImportGooglePlaces)} - publishing start");

            try
            {
                using (new DatabaseSwitcher(Database.GetDatabase(Constants.Sitecore.Databases.Master)))
                using (var innerScope = ServiceLocator.ServiceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope())
                {
                    var importer = innerScope.ServiceProvider.GetService<IImporter>();

                    if (importer == null)
                    {
                        this._logger.Warn($"Sitecron - Job {nameof(ImportGooglePlaces)} - nullable importer - items hasn't been published");
                        return;
                    }

                    if (roots == null || !roots.Any())
                    {
                        this._logger.Warn($"Sitecron - Job {nameof(ImportGooglePlaces)} - empty roots - items hasn't been published");
                        return;
                    }

                    foreach (var root in roots.Where(x => x != Guid.Empty))
                    {
                        var rootLogs = importer.Publish(root);
                        this._importJobLogger.LogImportResults(rootLogs);
                    }
                }
            }
            catch (Exception exception)
            {
                this._logger.Error($"Sitecron - Job {nameof(ImportGooglePlaces)} - publishing error", exception);
            }

            this._logger.Info($"Sitecron - Job {nameof(ImportGooglePlaces)} - publishing end");
        }
    }
}