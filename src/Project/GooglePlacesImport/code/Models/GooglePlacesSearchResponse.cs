﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace RoyalCanin.Project.GooglePlacesImport.Models
{
    public partial class GooglePlacesSearchResponse
    {
        [JsonProperty("candidates", NullValueHandling = NullValueHandling.Ignore)]
        public List<Candidate> Candidates { get; set; }

        [JsonProperty("debug_log", NullValueHandling = NullValueHandling.Ignore)]
        public DebugLog DebugLog { get; set; }

        [JsonProperty("status", NullValueHandling = NullValueHandling.Ignore)]
        public string Status { get; set; }
    }

    public partial class Candidate
    {
        [JsonProperty("place_id", NullValueHandling = NullValueHandling.Ignore)]
        public string PlaceId { get; set; }

        [JsonProperty("permanently_close")]
        public bool PermanentlyClosed { get; set; }
    }

    public partial class DebugLog
    {
        [JsonProperty("line", NullValueHandling = NullValueHandling.Ignore)]
        public List<object> Line { get; set; }
    }
}