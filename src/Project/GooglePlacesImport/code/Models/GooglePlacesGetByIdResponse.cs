﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace RoyalCanin.Project.GooglePlacesImport.Models
{
    public partial class GooglePlacesGetByIdResponse
    {
        [JsonProperty("html_attributions", NullValueHandling = NullValueHandling.Ignore)]
        public List<object> HtmlAttributions { get; set; }

        [JsonProperty("result", NullValueHandling = NullValueHandling.Ignore)]
        public Result Result { get; set; }

        [JsonProperty("status", NullValueHandling = NullValueHandling.Ignore)]
        public string Status { get; set; }
    }

    public partial class Result
    {
        [JsonProperty("formatted_address", NullValueHandling = NullValueHandling.Ignore)]
        public string FormattedAddress { get; set; }

        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        [JsonProperty("url", NullValueHandling = NullValueHandling.Ignore)]
        public string Url { get; set; }

        [JsonProperty("formatted_phone_number", NullValueHandling = NullValueHandling.Ignore)]
        public string FormattedPhoneNumber { get; set; }

        [JsonProperty("opening_hours", NullValueHandling = NullValueHandling.Ignore)]
        public OpeningHours OpeningHours { get; set; }

        [JsonProperty("rating", NullValueHandling = NullValueHandling.Ignore)]
        public decimal Rating { get; set; }
    }

    public partial class OpeningHours
    {
        [JsonProperty("open_now", NullValueHandling = NullValueHandling.Ignore)]
        public bool? OpenNow { get; set; }

        [JsonProperty("periods", NullValueHandling = NullValueHandling.Ignore)]
        public List<Period> Periods { get; set; }

        [JsonProperty("weekday_text", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> WeekdayText { get; set; }
    }

    public partial class Period
    {
        [JsonProperty("close", NullValueHandling = NullValueHandling.Ignore)]
        public Close Close { get; set; }

        [JsonProperty("open", NullValueHandling = NullValueHandling.Ignore)]
        public Close Open { get; set; }
    }

    public partial class Close
    {
        [JsonProperty("day", NullValueHandling = NullValueHandling.Ignore)]
        public long? Day { get; set; }

        [JsonProperty("time", NullValueHandling = NullValueHandling.Ignore)]
        public string Time { get; set; }
    }

}