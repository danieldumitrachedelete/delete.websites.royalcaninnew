﻿using Castle.Core.Internal;

namespace RoyalCanin.Project.RoyalCanin.Writers
{
    using System;
    using System.IO;
    using System.Text;
    using System.Xml;
    using System.Xml.Linq;

    public abstract class SitemapWriter : IDisposable
    {
        protected readonly XNamespace SitemapNamespace = "http://www.sitemaps.org/schemas/sitemap/0.9";

        private readonly bool minimize;

        private XDocument xDocument;

        protected SitemapWriter(string filename, bool minimize = false)
        {
            this.Filename = filename;
            this.minimize = minimize;

            this.WriteHeader();
        }

        protected XElement Root => this.XDocument.Root;

        protected XDocument XDocument => this.xDocument ?? (this.xDocument = new XDocument(new XDeclaration("1.0", Encoding.UTF8.BodyName, "yes")));

        private string Filename { get; }

        public abstract void WriteHeader();

        public void Dispose()
        {
            CreateDirectoryIfMissing(this.Filename);
            DeleteFileIfExists(this.Filename);
            this.WriteDocument();
        }

        private void CreateDirectoryIfMissing(string filename)
        {
            var dirPath = Path.GetDirectoryName(Filename);
            if (!Directory.Exists(dirPath) && !string.IsNullOrEmpty(dirPath))
            {
                Directory.CreateDirectory(dirPath);
            }
        }

        private static void DeleteFileIfExists(string filename)
        {
            if (File.Exists(filename))
            {
                File.Delete(filename);
            }
        }

        private void WriteDocument()
        {

            using (var writer = new StreamWriter(File.Open(this.Filename, FileMode.CreateNew, FileAccess.Write, FileShare.Read), Encoding.UTF8))
            {
                using (var xmlWriter = new XmlTextWriter(writer))
                {
                    if (!this.minimize)
                    {
                        xmlWriter.Formatting = Formatting.Indented;
                    }

                    this.XDocument.WriteTo(xmlWriter);
                }
            }
        }
    }
}