﻿namespace RoyalCanin.Project.RoyalCanin.Writers
{
    using System;
    using System.Xml.Linq;

    using Delete.Foundation.DeleteFoundationCore.Extensions;

    using Sitecore;

    public class SitemapContentWriter : SitemapWriter
    {
        public SitemapContentWriter(string filename, bool minimize = false) : base(filename, minimize)
        {
        }

        public override void WriteHeader()
        {
            this.XDocument.Add(new XElement("urlset", new XAttribute("xmlns", this.SitemapNamespace)));
        }

        public void WriteNode(string link)
        {
            this.WriteNode(link, DateTime.Now, null);
        }

        public void WriteNode(string link, decimal priority)
        {
            this.WriteNode(link, DateTime.Now, priority);
        }

        public void WriteNode(string link, DateTime date)
        {
            this.WriteNode(link, date, null);
        }

        public void WriteNode(string link, DateTime date, decimal? priority)
        {
            var node = new XElement(
                "url", 
                new XElement("loc", link), 
                new XElement("lastmod", date.ToGoogleDate()));

            if (priority.HasValue && priority > 0 && priority <= 1)
            {
                node.Add(new XElement("priority", priority.ToString()));
            }

            this.Root.Add(node);
        }

        public void WriteFileNode(string url)
        {
            var fileNode = new XElement("url", new XElement("loc", $"{url}"));
            this.Root.Add(fileNode);
        }
    }
}