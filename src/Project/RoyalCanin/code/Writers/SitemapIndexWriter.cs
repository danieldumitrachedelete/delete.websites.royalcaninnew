﻿namespace RoyalCanin.Project.RoyalCanin.Writers
{
    using System;
    using System.Xml.Linq;

    using Delete.Foundation.DeleteFoundationCore.Extensions;

    using Sitecore;

    public class SitemapIndexWriter : SitemapWriter
    {
        public SitemapIndexWriter(string filename, bool minimize = false)
            : base(filename, minimize)
        {
        }

        public void WriteIndexNode(string content)
        {
            this.WriteIndexNode(content, DateTime.Now.ToGoogleDate());
        }

        public override void WriteHeader()
        {
            this.XDocument.Add(new XElement("sitemapindex", new XAttribute("xmlns", this.SitemapNamespace)));
        }

        private void WriteIndexNode(string indexItem, string date)
        {
            var indexNode = new XElement(
                "sitemap", 
                new XElement("loc", $"{indexItem}"), 
                new XElement("lastmod", date));
            this.Root.Add(indexNode);
        }
    }
}