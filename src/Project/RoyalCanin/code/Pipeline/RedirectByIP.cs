﻿using System;
using System.Linq;
using Glass.Mapper.Sc;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.Data.Items;
using Sitecore.DependencyInjection;
using Sitecore.Pipelines.HttpRequest;
using Delete.Feature.LanguagePicker.Extensions;
using Delete.Feature.LanguagePicker.Managers;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Country = Delete.Feature.LanguagePicker.Models.Country;
using Language = Delete.Foundation.RoyalCaninCore.Models.Language;

namespace RoyalCanin.Project.RoyalCanin.Pipeline
{
    public class RedirectByIP : HttpRequestProcessor
    {
        private readonly string LanguageRegionsCountriesFolderPath = "/sitecore/content/global configuration/system settings/feature/languagepicker";

        public ICountryService CountryService => (ServiceLocator.ServiceProvider.GetService<ICountryService>());
        public ISitecoreContext Context => (ServiceLocator.ServiceProvider.GetService<ISitecoreContext>());


        public override void Process(HttpRequestArgs args)
        {
            if (Sitecore.Context.Site?.SiteInfo?.Name != "website" || !args.LocalPath.Equals("/"))
            {
                return;
            }

            var cookie = args.HttpContext.Request.Cookies["selected_country"]?.Value;
            if (cookie.NotEmpty())
            {
                args.HttpContext.Response.Redirect(GetRedirectUrl(cookie, args.RequestUrl.Query));
                return;
            }

            var country = GetCountryCodeByIP(args);

            if (country.NotEmpty() && country != "N/A")
            {
                string chosenLanguage = null;

                var languages = args.HttpContext.Request.UserLanguages;
                if (languages != null)
                {
                    chosenLanguage = languages[0];
                }

                var countryLink = GetCountryByCodeAndLanguage(country, chosenLanguage);
                if (countryLink != null)
                {
                    var urlCode = countryLink.GetURLCode();
                    if (urlCode.NotEmpty())
                    {
                        args.HttpContext.Response.Redirect(GetRedirectUrl(urlCode, args.RequestUrl.Query));
                        return;
                    }
                }
            }
            
            var defaultSite = GetCountryByCodeAndLanguage("us", "en-us");
            if (defaultSite != null)
            {
                args.HttpContext.Response.Redirect(GetRedirectUrl(defaultSite.GetURLCode(), args.RequestUrl.Query));
            }
            
        }

        private static string GetRedirectUrl(string url, string queryString)
        {
            if (queryString.Empty())
            {
                return url;
            }

            var baseUri = new Uri(url, UriKind.RelativeOrAbsolute);

            if (baseUri.IsAbsoluteUri && baseUri.Query.NotEmpty())
            {
                return $"{url}&{queryString.TrimStart('?')}";
            }

            return $"{url}{queryString}";
        }

        private Country GetCountryByCodeAndLanguage(string country, string language = null)
        {
            var countries = CountryService
                .GetIndexedResults(GetEntitiesFolder())
                .Where(x => x.CountryCodeISO2.SafeEquals(country))
                .MapResults(Context, true)
                .ToList();

            return 
                countries.FirstOrDefault(x => language.IsNullOrEmpty() || AreEqualLanguages(language, x.RelatedLanguage))
                ?? countries.FirstOrDefault();
        }

        private static bool AreEqualLanguages(string languageCode, Language langauge)
        {
            return  languageCode.SafeEquals(langauge?.Iso) 
                    || languageCode.SafeEquals(langauge?.RegionalIsoCode) 
                    || languageCode.SafeEquals(langauge?.Name);
        }

        private string GetCountryCodeByIP(HttpRequestArgs args)
        {
            var ipString = GetIp(args);

            if (ipString.NotEmpty())
            {
                try
                {
                    return Sitecore.Analytics.Lookups.LookupManager.GetInformationByIp(ipString)?.Country;
                }
                catch (Exception e)
                {
                    Sitecore.Diagnostics.Log.Error("Redirect By Ip failed at GetInformationByIp", e, typeof(RedirectByIP));
                }
            }

            return null;
        }

        private Item GetEntitiesFolder()
        {
            return Context.Database.GetItem(LanguageRegionsCountriesFolderPath);
        }

        private string GetIp(HttpRequestArgs args)
        {
            //todo: this method to foundation section
            var request = args.HttpContext.Request;

            var ip = string.Empty;
            //todo: get name header from config-files
            var xForwardedFor = request.Headers["X-Forwarded-For"];
            var remoteAddress = request.ServerVariables["REMOTE_ADDR"];
            if (string.IsNullOrEmpty(xForwardedFor))
            {
                this.LogDebug($"New request: IP from Remote_Address {remoteAddress}; url: {request.Url}");

                ip = remoteAddress.SafelySplit(":").FirstOrDefault();
            }
            else
            {
                ip = xForwardedFor.SafelySplit(",").FirstOrDefault()
                    .SafelySplit(":").FirstOrDefault();

                this.LogDebug($"New request: IP {ip} from HTTP_X_FORWARDED_FOR {xForwardedFor}; url: {request.Url}");
            }

            return ip;
        }
    }
}