﻿using System.Web;
using Sitecore.Diagnostics;
using Sitecore.Mvc.Pipelines.Response.RenderRendering;

namespace RoyalCanin.Project.RoyalCanin.Pipeline
{
    public class ApplyVaryByHomepage : RenderRenderingProcessor
    {
        public override void Process(RenderRenderingArgs args)
        {
            Assert.ArgumentNotNull(args, "args");

            if (args.Rendered
                || HttpContext.Current == null
                || !args.Cacheable
                || string.IsNullOrWhiteSpace(args.CacheKey)
                || args.Rendering.RenderingItem == null)
            {
                return;
            }

            var rendering = args.PageContext.Database.GetItem(args.Rendering.RenderingItem.ID);

            if (rendering == null || rendering["VaryByHomepage"] != "1")
            {
                return;
            }

            var homepage = Sitecore.Context.Database.GetItem(Sitecore.Context.Site.StartPath);
            var isHomepage = homepage.ID.Equals(Sitecore.Context.Item.ID);

            args.CacheKey += $"_#homepage:{isHomepage}";
        }
    }
}