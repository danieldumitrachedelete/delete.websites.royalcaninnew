using AutoMapper;
using Delete.Feature.Tips.Models;
using Delete.Foundation.DeleteFoundationCore.Controllers;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.DeleteFoundationCore.Models;
using Glass.Mapper.Sc;
using Sitecore.Data.Items;

namespace RoyalCanin.Project.RoyalCanin.Controllers
{
    using Delete.Feature.Articles.Models;
    using Delete.Feature.Breeds.Models;
    using Delete.Feature.Events.Models;
    using Delete.Feature.News.Models;
    using Delete.Feature.Products.Models;
    using Delete.Foundation.DataTemplates.Models;
    using Delete.Foundation.DataTemplates.Models.Interfaces;
    using Delete.Foundation.DeleteFoundationCore;
    using Delete.Foundation.DeleteFoundationCore.Extensions;
    using Delete.Foundation.DeleteFoundationCore.Helpers;
    using Delete.Foundation.MultiSite.Models;
    using Glass.Mapper;
    using Managers;
    using Models;
    using Sitecore.Globalization;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using SearchViewModel = Models.SearchViewModel;

    public class RoyalCaninController : BaseController
    {
        private const int InitialPageIndex = 1;

        private const int PredictiveItemsCount = 4;

        protected readonly IContentSearchManager contentSearchManager;
        protected readonly ILocalMarketSettings localMarketSettings;

        public RoyalCaninController(
            IContentSearchManager contentSearchManager,
            ISitecoreContext sitecoreContext,
            IMapper mapper,
            ICacheManager cacheManager,
            ILocalMarketSettings localMarketSettings) : base(sitecoreContext, mapper, cacheManager)
        {
            this.contentSearchManager = contentSearchManager;
            this.localMarketSettings = localMarketSettings;
        }

        public ActionResult Search(ContentFilterViewModel filterConfiguration)
        {
            Guard.ArgumentNotNull(filterConfiguration, nameof(filterConfiguration));

            ContentSearchConfiguration searchConfiguration;
            if (filterConfiguration.Filter == Guid.Empty)
            {
                searchConfiguration = this.GetDataSourceItem<ContentSearchConfiguration>();
            }
            else
            {
                searchConfiguration = this.SitecoreContext.GetItem<ContentSearchConfiguration>(filterConfiguration.Filter, false, true);
            }

            if (searchConfiguration == null)
            {
                return this.Content(Sitecore.Context.PageMode.IsExperienceEditorEditing ? "<p>Please select search configuration</p>" : string.Empty);
            }

            var searchResults = this.contentSearchManager.GetSearchResultsWithHighlights(
                filterConfiguration.SearchQuery,
                searchConfiguration.NumberOfSurroundingCharactersForHighlights).ToList();

            var products = GetProductResults(searchResults);

            var articleItemResults = GetArticleResults(searchResults);

            var tipsItemResults = GetTipsResults(searchResults);

            var breeds = GetBreedResults(searchResults);

            var otherTabsContentIds = new List<Guid>();
            otherTabsContentIds.AddRange(products.Select(x => x.Id));
            otherTabsContentIds.AddRange(articleItemResults.Select(x => x.Id));
            otherTabsContentIds.AddRange(tipsItemResults.Select(x => x.Id));
            otherTabsContentIds.AddRange(breeds.Select(x => x.Id));

            var allResults = searchResults.Where(x => !otherTabsContentIds.Contains(x.Id)).ToList();

            var results = new SearchViewModel
            {
                Filter = searchConfiguration.Id,
                SearchQuery = filterConfiguration.SearchQuery,
                AllResults = CreateSearchAllResultsViewModel(
                    allResults,
                    products,
                    searchConfiguration.Id,
                    InitialPageIndex,
                    searchConfiguration.AllResultsPageSize,
                    searchConfiguration.AllResultsProductsPageSize,
                    searchConfiguration.AllResultsProductsMobilePageSize),
                Products = CreateProductsResultViewModel(
                    products,
                    searchConfiguration.Id,
                    InitialPageIndex,
                    searchConfiguration.ProductsPageSize),
                ContentItems = CreateArticleSearchResultsViewModel(
                    this._mapper.Map<IEnumerable<ContentItemViewModel>>(articleItemResults).ToList(),
                    searchConfiguration.Id,
                    InitialPageIndex,
                    searchConfiguration.ArticlesPageSize),
                Breeds = CreateBreedsSearchResultsViewModel(breeds, searchConfiguration.Id, InitialPageIndex, searchConfiguration.BreedsPageSize),
                Tips = CreateTipsSearchResultsViewModel(
                    this._mapper.Map<IEnumerable<ContentItemViewModel>>(tipsItemResults).ToList(),
                    searchConfiguration.Id,
                    InitialPageIndex,
                    searchConfiguration.TipsPageSize)
        };

            return this.View("~/Views/Project/RoyalCanin/Search.cshtml", results);
        }

        public ActionResult Find(ContentFilterViewModel filterConfiguration)
        {
            Guard.ArgumentNotNull(filterConfiguration, nameof(filterConfiguration));

            var searchConfiguration = this.SitecoreContext.GetItem<ContentSearchConfiguration>(filterConfiguration.Filter, false, true);
            var searchResults = this.contentSearchManager.GetSearchResultsWithHighlights(
                filterConfiguration.SearchQuery,
                searchConfiguration.NumberOfSurroundingCharactersForHighlights).ToList();

            var products = GetProductResults(searchResults);
            var articleItemResults = GetArticleResults(searchResults);
            var breeds = GetBreedResults(searchResults);
            var tips = GetTipsResults(searchResults);

            if (filterConfiguration.Category.SafeEquals("products"))
            {
                var productResults = CreateProductsResultViewModel(products, searchConfiguration.Id, filterConfiguration.Page, searchConfiguration.ProductsPageSize);

                return this.View("~/Views/Project/RoyalCanin/_ProductsSearchResults.cshtml", productResults);
            }
            else if (filterConfiguration.Category.SafeEquals("content"))
            {
                var articleResults = CreateArticleSearchResultsViewModel(
                    this._mapper.Map<IEnumerable<ContentItemViewModel>>(articleItemResults).ToList(),
                    searchConfiguration.Id,
                    filterConfiguration.Page,
                    searchConfiguration.ArticlesPageSize);
                return this.View("~/Views/Project/RoyalCanin/_ContentItemsResults.cshtml", articleResults);
            }
            else if (filterConfiguration.Category.SafeEquals("breed"))
            {
                var breedResults = CreateBreedsSearchResultsViewModel(
                    breeds,
                    searchConfiguration.Id,
                    filterConfiguration.Page,
                    searchConfiguration.BreedsPageSize);
                return this.View("~/Views/Project/RoyalCanin/_BreedsResults.cshtml", breedResults);
            } else if (filterConfiguration.Category.SafeEquals("tips"))
            {
                var tipsResults = CreateTipsSearchResultsViewModel(
                    this._mapper.Map<IEnumerable<ContentItemViewModel>>(tips).ToList(),
                    searchConfiguration.Id,
                    filterConfiguration.Page,
                    searchConfiguration.TipsPageSize);
                return this.View("~/Views/Project/RoyalCanin/_ContentItemsResults.cshtml", tipsResults);
            }

            var otherTabsContentIds = new List<Guid>();
            otherTabsContentIds.AddRange(products.Select(x => x.Id));
            otherTabsContentIds.AddRange(articleItemResults.Select(x => x.Id));
            otherTabsContentIds.AddRange(breeds.Select(x => x.Id));
            otherTabsContentIds.AddRange(tips.Select(x => x.Id));

            var allResults = searchResults.Where(x => !otherTabsContentIds.Contains(x.Id)).ToList();

            var results = CreateSearchAllResultsViewModel(
                allResults,
                filterConfiguration.Page == 1 ? products : new List<LocalProduct>(),
                searchConfiguration.Id,
                filterConfiguration.Page,
                searchConfiguration.AllResultsPageSize,
                searchConfiguration.AllResultsProductsPageSize,
                searchConfiguration.AllResultsProductsMobilePageSize);
            return this.View("~/Views/Project/RoyalCanin/_AllSearchResults.cshtml", results);
        }

        public ActionResult Predictive(string keyword)
        {
            var results = this.contentSearchManager.GetAllLocalSearchResults(keyword, PredictiveItemsCount);
            var predictiveResults = this._mapper.Map<IEnumerable<PredictiveSearchItemJsonModel>>(results.Results);
            var json = new PredictiveSearchJsonModel
            {
                Items = predictiveResults
            };

            var localSearchPageUrl = localMarketSettings?.SearchResultsPage?.Url;
            if (results.TotalResults <= PredictiveItemsCount || StringExtensions.IsNullOrEmpty(localSearchPageUrl))
            {
                return this.Json(json, JsonRequestBehavior.AllowGet);
            }


            keyword = keyword.Replace(' ', '+');
            var allResultsPageUrl = new UrlHelper().UrlWithParameters(
                localSearchPageUrl,
                new Dictionary<string, string> { { nameof(ContentFilterViewModel.SearchQuery), keyword } }, false);
            json.FeaturedItems = new List<PredictiveSearchItemJsonModel>
             {
                 new PredictiveSearchItemJsonModel
                 {
                     Url = allResultsPageUrl,
                     Title = TranslateHelper.TextByDomainCached("Translations", "translations.project.royalcanin.viewallresults", results.TotalResults)
                 }
             };

            return this.Json(json, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ClientSideConfiguration()
        {
            var model = new
            {
                googleMapApiKey = localMarketSettings?.GoogleApiKey
            };

            return View("~/Views/Project/RoyalCanin/_ClientSideConfiguration.cshtml", model);
        }

        public ActionResult PriceSpiderConfiguration()
        {
            var contextItem = this.GetContextItem<GlassBase>();
            if (contextItem.IsDerivedFrom(ProductPageConstants.TemplateId) && !HttpContext.IsAmpRequest())
            {
                return View("~/Views/Project/RoyalCanin/_PriceSpiderConfiguration.cshtml", this.localMarketSettings);
            }

            return new EmptyResult();
        }

        public ActionResult ForeseeConfiguration()
        {
            return View("~/Views/Project/RoyalCanin/_Foresee.cshtml", this.localMarketSettings);
        }

        public ActionResult Widgets()
        {
            return View("~/Views/Project/RoyalCanin/Widgets.cshtml", this.localMarketSettings);
        }

        private static SearchAllResultsViewModel CreateSearchAllResultsViewModel(
            IList<_SearchableContent> searchResults,
            IList<LocalProduct> products,
            Guid filter,
            int page,
            int pageSize,
            int productsPageSize,
            int? productsMobilePageSize = null)
        {
            var results = new SearchAllResultsViewModel()
            {
                Filter = filter,
                Page = page,
                PageSize = pageSize,
                TotalResults = searchResults.Count(),
                ProductsMobilePageSize = productsMobilePageSize ?? pageSize
            };
            results.Results = searchResults.Skip(results.GetSkippedItemsCountToCurrentPage()).Take(results.PageSize);
            results.ProductResults = CreateProductsResultViewModel(
                products,
                filter,
                InitialPageIndex,
                productsPageSize);

            return results;
        }

        private static BreedsSearchResultsViewModel CreateBreedsSearchResultsViewModel(
            IList<LocalBreed> breeds,
            Guid filter,
            int page,
            int pageSize)
        {
            var results = new BreedsSearchResultsViewModel()
            {
                Filter = filter,
                Page = page,
                PageSize = pageSize,
                TotalResults = breeds.Count()
            };
            results.Results = breeds.Skip(results.GetSkippedItemsCountToCurrentPage()).Take(results.PageSize);
            return results;
        }

        private static Delete.Feature.Products.Models.SearchResultsViewModel CreateProductsResultViewModel(
            IList<LocalProduct> products,
            Guid filter,
            int page,
            int pageSize)
        {
            var results = new Delete.Feature.Products.Models.SearchResultsViewModel
            {
                Filter = filter,
                Page = page,
                PageSize = pageSize,
                TotalResults = products.Count
            };

            results.SearchResults = new FacetedResults<LocalProduct>
            {
                Results = products
                    .Skip(results.GetSkippedItemsCountToCurrentPage())
                    .Take(results.PageSize)
                    .ToList()
            };
            return results;
        }

        private static ContentSearchResultsViewModel CreateArticleSearchResultsViewModel(
            IList<ContentItemViewModel> contentItems,
            Guid filter,
            int page,
            int pageSize)
        {
            var results = new ContentSearchResultsViewModel()
            {
                Filter = filter,
                Page = page,
                PageSize = pageSize,
                TotalResults = contentItems.Count()
            };

            results.Results = contentItems
                .Skip(results.GetSkippedItemsCountToCurrentPage())
                .Take(results.PageSize);
            return results;
        }
        private static ContentSearchResultsViewModel CreateTipsSearchResultsViewModel(
            IList<ContentItemViewModel> contentItems,
            Guid filter,
            int page,
            int pageSize)
        {
            var results = new ContentSearchResultsViewModel()
            {
                Filter = filter,
                Page = page,
                PageSize = pageSize,
                TotalResults = contentItems.Count()
            };

            results.Results = contentItems
                .Skip(results.GetSkippedItemsCountToCurrentPage())
                .Take(results.PageSize);
            return results;
        }

        private static IList<LocalProduct> GetProductResults(IEnumerable<_SearchableContent> searchResults)
        {
            return searchResults
                .Where(x => x.IsDerivedFrom(ProductPageConstants.TemplateId))
                .Cast<LocalProduct>()
                .ToList();
        }

        private static IList<LocalBreed> GetBreedResults(IEnumerable<_SearchableContent> searchResults)
        {
            return searchResults
                .Where(x => x.IsDerivedFrom(BreedPageConstants.TemplateId))
                .Cast<LocalBreed>()
                .ToList();
        }

        private static IList<Article> GetArticleResults(IEnumerable<_SearchableContent> searchResults)
        {
            return searchResults
                .Where(x => x.IsDerivedFrom(ArticlePageConstants.TemplateId))
                .Cast<Article>()
                .ToList();
        }

        private static IList<Tip> GetTipsResults(IEnumerable<_SearchableContent> searchResults)
        {
            return searchResults
                .Where(x => x.IsDerivedFrom(TipsPageConstants.TemplateId))
                .Cast<Tip>()
                .ToList();
        }

        private IList<ContentItemViewModel> GetContentItems(IEnumerable<_SearchableContent> searchResults)
        {
            var contentResults = new List<ContentItemViewModel>();

            foreach (var searchResult in searchResults)
            {
                if (searchResult.IsDerivedFrom(NewsItemPageConstants.TemplateId))
                {
                    contentResults.Add(this._mapper.Map<ContentItemViewModel>(searchResult.CastTo<NewsItem>()));
                }
                else if (searchResult.IsDerivedFrom(EventPageConstants.TemplateId))
                {
                    contentResults.Add(this._mapper.Map<ContentItemViewModel>(searchResult.CastTo<Event>()));
                }
                else if (searchResult.IsDerivedFrom(ProductCataloguePageConstants.TemplateId)
                    && searchResult is ContentPage)
                {
                    contentResults.Add(this._mapper.Map<ContentItemViewModel>(searchResult.CastTo<ContentPage>()));
                }
                // do not check via IsDerivedFrom
                else if (searchResult.TemplateId == ContentPageConstants.TemplateId)
                {
                    contentResults.Add(this._mapper.Map<ContentItemViewModel>(searchResult.CastTo<ContentPage>()));
                }
            }

            return contentResults;
        }

        public ActionResult PageCustomTrackingCode()
        {
            var contextItem = this.GetContextItem<GlassBase>();
            if (contextItem.IsDerivedFrom(BasePageConstants.TemplateId) && !HttpContext.IsAmpRequest())
            {
                var basePageItem = this.GetContextItem<BasePage>();
                if (!basePageItem.TrackingScript.IsNullOrWhiteSpace())
                {
                    return Content(basePageItem.TrackingScript);
                }
                else
                {
                    return new EmptyResult();
                }
            }

            return new EmptyResult();
        }

        public ActionResult LocalMarketCustomTrackingCode()
        {
            if (!(this.localMarketSettings?.TrackingScript).IsNullOrWhiteSpace())
            {
                return Content(this.localMarketSettings?.TrackingScript);
            }
            else
            {
                return new EmptyResult();
            }
        }

        public ActionResult CookieServices()
        {
            return PartialView("~/Views/Project/RoyalCanin/_CookieServices.cshtml", this.localMarketSettings);
        }
    }
}
