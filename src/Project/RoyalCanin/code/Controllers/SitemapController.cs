using System.Globalization;
using Delete.Feature.Breeds.Models;
using RoyalCanin.Project.RoyalCanin.Managers;

namespace RoyalCanin.Project.RoyalCanin.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;

    using Delete.Foundation.DeleteFoundationCore.Extensions;
    using Delete.Foundation.DeleteFoundationCore.Models;
    using Delete.Foundation.MultiSite.Extensions;
    using Delete.Foundation.MultiSite.Helpers;
    using Delete.Foundation.RoyalCaninCore.Extensions;
    using Delete.Foundation.RoyalCaninCore.Helpers;

    using Glass.Mapper.Sc;

    using Microsoft.Extensions.DependencyInjection;

    using Models;
    using Models.Sitemap;

    using Sitecore;
    using Sitecore.Data;
    using Sitecore.Data.Events;
    using Sitecore.DependencyInjection;
    using Sitecore.Globalization;
    using Sitecore.IO;
    using Sitecore.Sites;

    using Writers;



    public class SitemapController : Controller
    {
        public string Database { get; set; }

        public void BuildSitemap(object sender, EventArgs args)
        {
            var parameters = (Sitecore.Publishing.Publisher)((Sitecore.Events.SitecoreEventArgs)args).Parameters[0];
            var publishedLanguages = parameters.GetPrivateFieldValue<Language[]>("languages");
            var publishedItem = parameters?.Options?.RootItem;
            var publishedItemPath = publishedItem != null ? publishedItem.Paths.FullPath : string.Empty;

            this.BuildSitemap(publishedLanguages.Select(x => x.Name), publishedItemPath);
        }

        public void BuildSitemapRemotely(object sender, EventArgs args)
        {
            var eventArgs = args as PublishEndRemoteEventArgs;
            var publishedLanguage = eventArgs.LanguageName;
            var publishedItem = Sitecore.Configuration.Factory.GetDatabase(eventArgs.TargetDatabaseName)?.GetItem(new ID(eventArgs.RootItemId));
            var publishedItemPath = publishedItem != null ? publishedItem.Paths.FullPath : string.Empty;

            this.BuildSitemap(new[] { publishedLanguage }, publishedItemPath);
        }

        [NonAction]
        public void BuildSitemap(IEnumerable<string> languages, string publishedItemPath)
        {
            var siteContexts = SiteManagerHelper.GetAvailableWebsites()
                .Select(site => SiteContext.GetSite(site.Name))
                .Where(x => languages.Contains(x.Language) || languages.Any(l => l.IsDefaultLanguage()))
                .ToList();

            var defaultWebsiteContext = SiteContext.GetSite(SiteContextExtensions.DefaultWebsiteName);
            var defaultWebsiteUrl = GetAbsoluteUrl(defaultWebsiteContext);

            foreach (var siteContext in siteContexts)
            {
                if (!publishedItemPath.StartsWith(siteContext.RootPath, StringComparison.InvariantCultureIgnoreCase)
                    && !publishedItemPath.Equals(defaultWebsiteContext.RootPath, StringComparison.InvariantCultureIgnoreCase))
                {
                    continue;
                }

                using (new SiteContextSwitcher(siteContext))
                {
                    // get glassmapper context after switching to correct sitecore contexts.
                    var sitecoreContext = ServiceLocator.ServiceProvider.GetService<ISitecoreContext>();

                    BuildIndexSitemapFile(sitecoreContext, siteContext.Language, defaultWebsiteUrl);
                }
            }
        }

        private void BuildIndexSitemapFile(ISitecoreContext sitecoreContext, string language, string absoluteUrl)
        {
            var rootItem = sitecoreContext.GetRootItem<GlassBase>();

            var url =
                !string.IsNullOrEmpty(Context.Site.SiteInfo.Scheme) &&
                !string.IsNullOrEmpty(Context.Site.SiteInfo.TargetHostName)
                    ? $"{Context.Site.SiteInfo.Scheme}://{Context.Site.SiteInfo.TargetHostName}"
                    : absoluteUrl ;

            var sitemapSearchManager = ServiceLocator.ServiceProvider.GetService<ISitemapSearchManager>();

            var sitemapConfiguration = sitemapSearchManager.GetAllLocal(rootItem).FirstOrDefault();

            if (sitemapConfiguration == null)
            {
                return;
            }

            var sitemapNames = GenerateSitemapFiles(sitecoreContext, sitemapConfiguration, language);

            GenerateCompressedSitemapFilesWithIndexIfNecessary(url, sitemapConfiguration, sitemapNames, language);

            GenerateIndexSitemap(url, sitemapConfiguration, sitemapNames, language);
        }

        private static string GenerateIndexSitemap(string urlPathToFile, SitemapConfiguration sitemapConfiguration, IEnumerable<string> sitemapNames, string language)
        {
            var languageSpecificFileName = FileHelper.AddLanguageToFilepath(sitemapConfiguration.Filename, language);
            using (var sitemapWriter = new SitemapIndexWriter(
                FileUtil.MakePath(System.Web.HttpRuntime.AppDomainAppPath, languageSpecificFileName),
                true))
            {
                foreach (var sitemap in sitemapNames)
                {
                    var indexFile = new Uri(urlPathToFile).Append(StringUtil.EnsurePrefix('/', sitemap)).AbsoluteUri;
                    sitemapWriter.WriteIndexNode(indexFile);
                }
            }

            return languageSpecificFileName;
        }

        private static IList<string> GenerateSitemapFiles(ISitecoreContext sitecoreContext, SitemapConfiguration sitemapConfiguration, string language)
        {
            return sitemapConfiguration.Sitemaps
                .Select(sitemap => GenerateSitemap(sitecoreContext, sitemap, language))
                .ToList();
        }

        private static void GenerateCompressedSitemapFilesWithIndexIfNecessary(
            string urlPathToFile,
            SitemapConfiguration sitemapConfiguration,
            IList<string> sitemapNames,
            string language)
        {
            if (!sitemapConfiguration.CompressSitemaps)
            {
                return;
            }

            var gzipSitemapNames = sitemapNames
                .Select(sitemapName => FileHelper.GZipCompress(FileUtil.MakePath(System.Web.HttpRuntime.AppDomainAppPath, sitemapName)))
                .ToList();
            var sitemapIndexFileName = GenerateIndexSitemap(urlPathToFile, sitemapConfiguration, gzipSitemapNames, language);
            FileHelper.GZipCompress(FileUtil.MakePath(System.Web.HttpRuntime.AppDomainAppPath, sitemapIndexFileName));
        }

        private static string GenerateSitemap(ISitecoreContext sitecoreContext, Sitemap sitemap, string language)
        {
            var pages = new List<BasePage>();
            foreach (var root in sitemap.Roots)
            {
                var rootPage = sitecoreContext.GetItem<BasePage>(root);
                if (rootPage != null)
                {
                    pages.AddRange(GetPages(sitecoreContext, rootPage, sitemap));
                }
            }

            var languageSpecificFileName = FileHelper.AddLanguageToFilepath(sitemap.Filename, language);
            WriteContentSitemap(pages, languageSpecificFileName);

            return languageSpecificFileName;
        }

        private static void WriteContentSitemap(IList<BasePage> pages, string filename)
        {
            using (var sitemapWriter = new SitemapContentWriter(FileUtil.MakePath(System.Web.HttpRuntime.AppDomainAppPath, filename), true))
            {
                foreach (var page in pages)
                {
                    var priority = GetPagePriority(page, pages);
                    sitemapWriter.WriteNode(page.AbsoluteUrl, page.Updated, priority);
                }
            }
        }

        private static decimal GetPagePriority(BasePage page, IList<BasePage> pages)
        {
            decimal parsedPriority, result;
            var hasValidPriority =
                decimal.TryParse(page.SitemapPriority, NumberStyles.Any, CultureInfo.InvariantCulture, out parsedPriority);

            if (hasValidPriority)
            {
                result = parsedPriority;
            }
            else
            {
                var parentItem = pages.FirstOrDefault(x => x.Id == page.Parent.Id);
                if (parentItem != null && parentItem.TemplateId != HomepageConstants.TemplateId)
                {
                    result = GetPagePriority(parentItem, pages);
                }
                else
                {
                    result = 0.5m;
                }
            }

            return result;
        }

        private static IEnumerable<BasePage> GetPages(ISitecoreContext sitecoreContext, BasePage root, Sitemap sitemap)
        {
            var result = new List<BasePage>();

            if (sitemap.ExcludeItems.Any(x => x == root.Id))
            {
                return result;
            }

            if (sitemap.IncludeTemplates.Any(x => x == root.TemplateId.Guid) && IsItemVisible(sitecoreContext, root))
            {
                result.Add(root);
            }

            if (root.ChildrenBasePages != null && root.ChildrenBasePages.Any())
            {
                result.AddRange(root.ChildrenBasePages.SelectMany(x => GetPages(sitecoreContext, x, sitemap)));
            }

            return result;
        }

        private static bool IsItemVisible(ISitecoreContext sitecoreContext, BasePage item)
        {
            if (item.IsDerivedFrom(LocalBreedConstants.TemplateId))
            {
                var local = sitecoreContext.QuerySingle<LocalBreed>(item.Fullpath, false, true);
                if (local != null && (local.HideBreedLocally || local.GlobalBreed?.HideBreedGlobally == true))
                {
                    return false;
                }
            }

            return true;
        }

        private string GetAbsoluteUrl(SiteContext websiteContext)
        {
            using (new SiteContextSwitcher(websiteContext))
            {
                var sitecoreContext = ServiceLocator.ServiceProvider.GetService<ISitecoreContext>();
                var rootItem = sitecoreContext.GetRootItem<GlassBase>();
                return rootItem.AbsoluteUrl;
            }
        }
    }
}
