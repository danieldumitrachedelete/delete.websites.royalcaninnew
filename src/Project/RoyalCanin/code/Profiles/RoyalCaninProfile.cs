using AutoMapper;
using Delete.Feature.Tips.Models;

namespace RoyalCanin.Project.RoyalCanin.Profiles
{
    using Delete.Feature.Articles.Models;
    using Delete.Feature.Events.Models;
    using Delete.Feature.News.Models;

    using Models;

    using Sitecore.StringExtensions;

    public class RoyalCaninProfile : Profile
    {
        public RoyalCaninProfile()
        {
            this.CreateMap<Article, ContentItemViewModel>()
                .ForMember(x => x.Title, opt => opt.MapFrom(z => z.Heading))
                .ForMember(x => x.Url, opt => opt.MapFrom(z => z.Url))
                .ForMember(x => x.Image, opt => opt.MapFrom(z => z.ThumbnailImage))
                .ForAllOtherMembers(x => x.Ignore())
                ;

            this.CreateMap<Tip, ContentItemViewModel>()
                .ForMember(x => x.Title, opt => opt.MapFrom(z => z.Heading))
                .ForMember(x => x.Url, opt => opt.MapFrom(z => z.Url))
                .ForMember(x => x.Image, opt => opt.MapFrom(z => z.ThumbnailImage))
                .ForAllOtherMembers(x => x.Ignore())
                ;

            this.CreateMap<NewsItem, ContentItemViewModel>()
                .ForMember(x => x.Title, opt => opt.MapFrom(z => z.Heading))
                .ForMember(x => x.Url, opt => opt.MapFrom(z => z.Url))
                .ForMember(x => x.Image, opt => opt.MapFrom(z => z.ThumbnailImage))
                .ForAllOtherMembers(x => x.Ignore())
                ;

            this.CreateMap<Event, ContentItemViewModel>()
                .ForMember(x => x.Title, opt => opt.MapFrom(z => z.Heading))
                .ForMember(x => x.Url, opt => opt.MapFrom(z => z.Url))
                .ForMember(x => x.Image, opt => opt.MapFrom(z => z.ThumbnailImage))
                .ForAllOtherMembers(x => x.Ignore())
                ;

            this.CreateMap<ContentPage, ContentItemViewModel>()
                .ForMember(x => x.Title, opt => opt.MapFrom(z => z.DisplayName.IsNullOrEmpty() ? z.Name : z.DisplayName))
                .ForMember(x => x.Url, opt => opt.MapFrom(z => z.Url))
                .ForAllOtherMembers(x => x.Ignore())
                ;
        }
    }
}
