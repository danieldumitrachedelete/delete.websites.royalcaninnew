﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Delete.Foundation.DataTemplates.Models.Interfaces;

namespace RoyalCanin.Project.RoyalCanin.Extensions
{
    using Delete.Foundation.DeleteFoundationCore.Comparers;
    using Delete.Foundation.DeleteFoundationCore.Extensions;

    using Models;

    using Sitecore.ContentSearch;
    using Sitecore.Data.Fields;
    using Sitecore.Data.Items;

    public static class SitecoreItemExtensions
    {
        private static StringEqualityComparer stringEqualityComparer = new StringEqualityComparer(StringComparison.InvariantCultureIgnoreCase);

        public static bool IsSearchableContentItem(this Item item)
        {
            if (item == null)
            {
                return false;
            }

            if (item.Fields[_SearchableContentConstants.ExcludeFromSearchFieldName]?.Value == "1")
            {
                return false;
            }

            return item.IsDerived(ProductPageConstants.TemplateId)
                || item.IsDerived(ProductCataloguePageConstants.TemplateId)
                || item.IsDerived(ArticlePageConstants.TemplateId)
                || item.IsDerived(NewsItemPageConstants.TemplateId)
                || item.IsDerived(EventPageConstants.TemplateId)
                || item.IsDerived(BreedPageConstants.TemplateId)
                || item.IsDerived(ContentPageConstants.TemplateId);
        }

        public static IEnumerable<string> GetItemContent(this Item item)
        {
            var contentStrings = new List<string>();
            foreach (Field field in item.Fields)
            {
                // this check is what Sitecore uses to determine if a field belongs in _content (see LuceneDocumentBuilder.AddField())
                if (!IndexOperationsHelper.IsTextField(new SitecoreItemDataField(field)))
                {
                    continue;
                }

                var fieldValue = (field.Value ?? string.Empty).StripHtml();
                if (!string.IsNullOrWhiteSpace(fieldValue))
                {
                    contentStrings.Add(fieldValue);
                }
            }

            return contentStrings.Distinct(stringEqualityComparer);
        }
    }
}