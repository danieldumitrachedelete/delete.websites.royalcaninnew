﻿// <autogenerated>
//   This file was generated by T4 code generator ScriptRunner.tt.
//   Any changes made to this file manually will be lost next time the file is regenerated.
// </autogenerated>

using System;
using Sitecore.Data;
using Sitecore.Common;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Sitecore.Globalization;
using System.ComponentModel;
using Delete.Foundation.DeleteFoundationCore.Models;
using Sitecore.ContentSearch;
using RoyalCanin.Project.RoyalCanin;

namespace RoyalCanin.Project.RoyalCanin.Models
{
	public partial interface ITipsBabyHub : IGlassBase, Delete.Feature.Tips.Models.IBabyHub
	{
	}

	public static partial class TipsBabyHubConstants {
		public const string TemplateIdString = "{f77073a5-0a29-4482-a981-4e2f268da9ac}";
		public static readonly ID TemplateId = new ID(TemplateIdString);
		public const string TemplateName = "Tips Baby Hub";

	
	
	}

	
	/// <summary>
	/// TipsBabyHub
	/// <para>Path: /sitecore/templates/Project/RoyalCanin/Tips Baby Hub</para>	
	/// <para>ID: {f77073a5-0a29-4482-a981-4e2f268da9ac}</para>	
	/// </summary>
	[SitecoreType(TemplateId=TipsBabyHubConstants.TemplateIdString)] //, Cachable = true
	public partial class TipsBabyHub  : Delete.Feature.Tips.Models.BabyHub, ITipsBabyHub
	{
	
	}
}
