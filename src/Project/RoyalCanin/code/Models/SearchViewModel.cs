﻿namespace RoyalCanin.Project.RoyalCanin.Models
{
    using System;

    public class SearchViewModel
    {
        public Guid Filter { get; set; }

        public string SearchQuery { get;set; }

        public SearchAllResultsViewModel AllResults { get; set; }

        public Delete.Feature.Products.Models.SearchResultsViewModel Products { get; set; }

        public int ProductsMobilePageSize { get; set; }

        public ContentSearchResultsViewModel ContentItems { get; set; }

        public BreedsSearchResultsViewModel Breeds { get; set; }

        public ContentSearchResultsViewModel Tips { get; set; }

        public int TotalCount
        {
            get
            {
                return
                    AllResults.TotalResults +
                    Products.TotalResults +
                    ContentItems.TotalResults +
                    Breeds.TotalResults +
                    Tips.TotalResults;
            }
        }
    }
}