﻿// <autogenerated>
//   This file was generated by T4 code generator ScriptRunner.tt.
//   Any changes made to this file manually will be lost next time the file is regenerated.
// </autogenerated>

using System;
using Sitecore.Data;
using Sitecore.Common;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Sitecore.Globalization;
using System.ComponentModel;
using Delete.Foundation.DeleteFoundationCore.Models;
using Sitecore.ContentSearch;
using RoyalCanin.Project.RoyalCanin;

namespace RoyalCanin.Project.RoyalCanin.Models
{
	public partial interface IProductFinderPage : IGlassBase, global::RoyalCanin.Project.RoyalCanin.Models.IContentPage
	{
	}

	public static partial class ProductFinderPageConstants {
		public const string TemplateIdString = "{27e95a50-5ecc-486b-99c5-9ffc3397c469}";
		public static readonly ID TemplateId = new ID(TemplateIdString);
		public const string TemplateName = "Product Finder Page";

	
	
	}

	
	/// <summary>
	/// ProductFinderPage
	/// <para>Path: /sitecore/templates/Project/RoyalCanin/Product Finder Page</para>	
	/// <para>ID: {27e95a50-5ecc-486b-99c5-9ffc3397c469}</para>	
	/// </summary>
	[SitecoreType(TemplateId=ProductFinderPageConstants.TemplateIdString)] //, Cachable = true
	public partial class ProductFinderPage  : global::RoyalCanin.Project.RoyalCanin.Models.ContentPage, IProductFinderPage
	{
	
	}
}
