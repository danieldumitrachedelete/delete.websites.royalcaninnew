﻿using System;

namespace RoyalCanin.Project.RoyalCanin.Models
{
    public class ContentFilterViewModel
    {
        public Guid Filter { get; set; }

        public int Page { get; set; }

        public string Category { get; set; }

        public string SearchQuery { get; set; }
    }
}