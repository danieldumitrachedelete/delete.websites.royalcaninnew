﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RoyalCanin.Project.RoyalCanin.Models
{
    using Delete.Foundation.RoyalCaninCore.Models;

    public class ContentSearchResultsViewModel : PaginatedResults
    {
        public Guid Filter { get; set; }

        public IEnumerable<ContentItemViewModel> Results { get; set; }
    }
}