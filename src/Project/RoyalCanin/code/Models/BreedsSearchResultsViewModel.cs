﻿namespace RoyalCanin.Project.RoyalCanin.Models
{
    using System;
    using System.Collections.Generic;

    using Delete.Feature.Breeds.Models;
    using Delete.Foundation.RoyalCaninCore.Models;

    public class BreedsSearchResultsViewModel : PaginatedResults 
    {
        public Guid Filter { get; set; }

        public IEnumerable<LocalBreed> Results { get; set; }
    }
}