﻿using System;
using Delete.Foundation.DeleteFoundationCore.Models;

namespace RoyalCanin.Project.RoyalCanin.Models
{
    public class ContentItemViewModel
    {
        public string Title { get; set; }

        public string Url { get; set; }

        public ExtendedImage Image { get; set; }
    }
}
