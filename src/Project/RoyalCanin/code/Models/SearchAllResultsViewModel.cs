﻿namespace RoyalCanin.Project.RoyalCanin.Models
{
    using System;
    using System.Collections.Generic;

    using Delete.Foundation.DataTemplates.Models.Interfaces;
    using Delete.Foundation.RoyalCaninCore.Models;

    public class SearchAllResultsViewModel : PaginatedResults
    {
        public Guid Filter { get; set; }

        public IEnumerable<_SearchableContent> Results {get; set; }

        public Delete.Feature.Products.Models.SearchResultsViewModel ProductResults { get; set; }

        public int ProductsMobilePageSize { get; set; }
    }
}