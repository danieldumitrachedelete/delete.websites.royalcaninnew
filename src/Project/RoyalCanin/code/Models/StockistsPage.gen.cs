﻿// <autogenerated>
//   This file was generated by T4 code generator ScriptRunner.tt.
//   Any changes made to this file manually will be lost next time the file is regenerated.
// </autogenerated>

using System;
using Sitecore.Data;
using Sitecore.Common;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Sitecore.Globalization;
using System.ComponentModel;
using Delete.Foundation.DeleteFoundationCore.Models;
using Sitecore.ContentSearch;
using RoyalCanin.Project.RoyalCanin;

namespace RoyalCanin.Project.RoyalCanin.Models
{
	public partial interface IStockistsPage : IGlassBase, global::RoyalCanin.Project.RoyalCanin.Models.IContentPage
	{
	}

	public static partial class StockistsPageConstants {
		public const string TemplateIdString = "{65a6a6ce-2d27-4abc-82f3-f7a65354ff5a}";
		public static readonly ID TemplateId = new ID(TemplateIdString);
		public const string TemplateName = "Stockists Page";

	
	
	}

	
	/// <summary>
	/// StockistsPage
	/// <para>Path: /sitecore/templates/Project/RoyalCanin/Stockists Page</para>	
	/// <para>ID: {65a6a6ce-2d27-4abc-82f3-f7a65354ff5a}</para>	
	/// </summary>
	[SitecoreType(TemplateId=StockistsPageConstants.TemplateIdString)] //, Cachable = true
	public partial class StockistsPage  : global::RoyalCanin.Project.RoyalCanin.Models.ContentPage, IStockistsPage
	{
	
	}
}
