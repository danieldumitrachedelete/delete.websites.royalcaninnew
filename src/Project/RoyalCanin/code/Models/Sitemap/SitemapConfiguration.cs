﻿namespace RoyalCanin.Project.RoyalCanin.Models.Sitemap
{
    using System.Collections.Generic;

    using Glass.Mapper.Sc.Configuration.Attributes;

    public partial class SitemapConfiguration
    {
        [SitecoreChildren]
        public IEnumerable<Sitemap> Sitemaps { get; set; }
    }
}