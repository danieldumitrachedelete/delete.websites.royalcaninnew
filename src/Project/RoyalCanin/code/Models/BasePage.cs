﻿using System.Collections.Generic;

using Glass.Mapper.Sc.Configuration.Attributes;

namespace RoyalCanin.Project.RoyalCanin.Models
{
    public partial class BasePage
    {
        [SitecoreChildren(InferType = true, IsLazy = true)]
        public virtual IEnumerable<BasePage> ChildrenBasePages { get; set; }

    }
}
