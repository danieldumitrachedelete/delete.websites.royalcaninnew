﻿namespace RoyalCanin.Project.RoyalCanin.ComputedFields
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Delete.Foundation.DataTemplates.Models.Interfaces;
    using Delete.Foundation.DeleteFoundationCore;
    using Delete.Foundation.DeleteFoundationCore.Comparers;
    using Delete.Foundation.DeleteFoundationCore.ComputedFields;
    using Delete.Foundation.DeleteFoundationCore.Extensions;

    using Extensions;

    using Sitecore;
    using Sitecore.ContentSearch;
    using Sitecore.ContentSearch.ComputedFields;
    using Sitecore.Data;
    using Sitecore.Data.Fields;
    using Sitecore.Data.Items;

    [JetBrains.Annotations.UsedImplicitly]
    public class SearchableContentComputedField : BaseComputedField, IComputedIndexField
    {
        private static SitecoreItemEqualityComparer sitecoreItemEqualityComparer = new SitecoreItemEqualityComparer();

        private static StringEqualityComparer stringEqualityComparer = new StringEqualityComparer(StringComparison.InvariantCultureIgnoreCase);

        public string FieldName { get; set; }

        public string ReturnType { get; set; }

        public object ComputeFieldValue(IIndexable indexable)
        {
            Guard.ArgumentNotNull(indexable, nameof(indexable));

            var indexedItem = this.GetItemHavingBaseTemplate(indexable, _SearchableContentConstants.TemplateId);

            // var indexedItem = (Item)(indexable as SitecoreIndexableItem);
            if (indexedItem == null || !indexedItem.IsSearchableContentItem())
            {
                return null;
            }

            var excludedRenderings = indexedItem[_SearchableContentConstants.ExcludedRenderingsFieldName]
                .ToGuidList("|")
                .Select(x => new ID(x))
                .ToList();

            var itemDatasources = ExtractRenderingDataSourceItems(
                indexedItem,
                indexedItem.Fields[FieldIDs.FinalLayoutField],
                excludedRenderings).ToList();

            itemDatasources.AddRange(ExtractRenderingDataSourceItems(
                indexedItem,
                indexedItem.Fields[FieldIDs.LayoutField],
                excludedRenderings));

            itemDatasources.Add(indexedItem);

            // extract text from data sources
            var contentToAdd = itemDatasources.Distinct(sitecoreItemEqualityComparer).SelectMany(SitecoreItemExtensions.GetItemContent).ToList();
            return contentToAdd.Count == 0 ? null : string.Join(" ", contentToAdd.Distinct(stringEqualityComparer).ToList());
        }

        /// <summary>
        /// Finds all renderings on an item's layout details with valid custom data sources set and returns the data source items.
        /// </summary>
        private static IEnumerable<Item> ExtractRenderingDataSourceItems(Item baseItem, Field layoutField, IEnumerable<ID> excludedRenderings)
        {
            var currentLayoutXml = LayoutField.GetFieldValue(layoutField);
            if (string.IsNullOrEmpty(currentLayoutXml))
            {
                return Enumerable.Empty<Item>();
            }

            var datasourceItems = new List<Item>();
            foreach (var device in baseItem.Database.Resources.Devices.GetAll())
            {
                datasourceItems.AddRange(ExtractDeviceRenderingDataSourceItems(baseItem, device, excludedRenderings));
            }

            return datasourceItems;
        }

        private static IEnumerable<Item> ExtractDeviceRenderingDataSourceItems(
            Item baseItem,
            DeviceItem device,
            IEnumerable<ID> excludedRenderings)
        {
            var result = new List<Item>();
            foreach (var rendering in baseItem.Visualization.GetRenderings(device, false))
            {
                if (excludedRenderings.Contains(rendering.RenderingID))
                {
                    continue;
                }

                if (rendering.Database != baseItem.Database)
                {
                    continue;
                }

                var datasourceId = rendering.Settings.DataSource;
                if (!ID.IsID(datasourceId))
                {
                    continue;
                }

                var datasourceItem = baseItem.Database.GetItem(new ID(datasourceId), baseItem.Language);
                if (datasourceItem == null)
                {
                    continue;
                }

                if (datasourceItem.HasChildren)
                {
                    foreach (Item datasourceItemChild in datasourceItem.Children)
                    {
                        result.Add(datasourceItemChild);
                    }
                }

                result.Add(datasourceItem);
            }

            return result;
        }
    }
}