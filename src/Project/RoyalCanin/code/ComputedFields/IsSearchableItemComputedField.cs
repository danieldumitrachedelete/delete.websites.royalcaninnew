﻿namespace RoyalCanin.Project.RoyalCanin.ComputedFields
{
    using Delete.Foundation.DataTemplates.Models.Interfaces;
    using Delete.Foundation.DeleteFoundationCore;
    using Delete.Foundation.DeleteFoundationCore.ComputedFields;

    using Extensions;

    using Sitecore.ContentSearch;
    using Sitecore.ContentSearch.ComputedFields;

    [JetBrains.Annotations.UsedImplicitly]
    public class IsSearchableItemComputedField : BaseComputedField, IComputedIndexField
    {
        public string FieldName { get; set; }

        public string ReturnType { get; set; }

        public object ComputeFieldValue(IIndexable indexable)
        {
            Guard.ArgumentNotNull(indexable, nameof(indexable));

            var indexedItem = this.GetItemHavingBaseTemplate(indexable, _SearchableContentConstants.TemplateId);
            if (indexedItem == null || !indexedItem.IsSearchableContentItem())
            {
                return null;
            }

            return 1;
        }
    }
}