﻿namespace RoyalCanin.Project.RoyalCanin.ComputedFields
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Delete.Foundation.DataTemplates.Models.Dictionary;
    using Delete.Foundation.DataTemplates.Models.Interfaces;
    using Delete.Foundation.DeleteFoundationCore;
    using Delete.Foundation.DeleteFoundationCore.Comparers;
    using Delete.Foundation.DeleteFoundationCore.ComputedFields;
    using Delete.Foundation.DeleteFoundationCore.Extensions;
    using Delete.Feature.Products.Models;

    using Extensions;

    using Sitecore;
    using Sitecore.ContentSearch;
    using Sitecore.ContentSearch.ComputedFields;
    using Sitecore.Data;
    using Sitecore.Data.Fields;
    using Sitecore.Data.Items;

    [JetBrains.Annotations.UsedImplicitly]
    public class SearchableTaxonomyComputedField : BaseComputedField, IComputedIndexField
    {
        private static SitecoreItemEqualityComparer sitecoreItemEqualityComparer = new SitecoreItemEqualityComparer();

        private static StringEqualityComparer stringEqualityComparer = new StringEqualityComparer(StringComparison.InvariantCultureIgnoreCase);

        public string FieldName { get; set; }

        public string ReturnType { get; set; }

        public object ComputeFieldValue(IIndexable indexable)
        {
            Guard.ArgumentNotNull(indexable, nameof(indexable));

            var indexedItem = this.GetItemHavingBaseTemplate(indexable, _SearchableContentConstants.TemplateId);

            // var indexedItem = (Item)(indexable as SitecoreIndexableItem);
            if (indexedItem == null || !indexedItem.IsSearchableContentItem())
            {
                return null;
            }

            var itemDatasources = ExtractTaxonomyItems(indexedItem).ToList();

            // extract text from data sources
            var contentToAdd = itemDatasources.Distinct(sitecoreItemEqualityComparer).SelectMany(SitecoreItemExtensions.GetItemContent).ToList();
            return contentToAdd.Count == 0 ? null : string.Join(" ", contentToAdd.Distinct(stringEqualityComparer).ToList());
        }

        private static IEnumerable<Item> ExtractTaxonomyItems(Item baseItem)
        {
            var result = new List<Item>();

            if (baseItem != null)
            {
                foreach (Field baseItemField in baseItem.Fields)
                {
                    var ids = baseItem.GetSitecoreIdsFromFieldValues(baseItemField.Name);
                    foreach (var guid in ids)
                    {
                        var relatedItem = baseItem.Database.GetItem(new ID(guid), baseItem.Language);
                        if (relatedItem == null)
                        {
                            continue;
                        }

                        if (!relatedItem.IsDerived(DictionaryEntryConstants.TemplateId))
                        {
                            continue;
                        }

                        result.Add(relatedItem);
                    }
                }

                result.AddRange(ExtractGlobalBreedItems(baseItem));
                result.AddRange(ExtractGlobalProductItems(baseItem));
            }

            return result;
        }
        
        private static IEnumerable<Item> ExtractGlobalBreedItems(Item baseItem)
        {
            var globalBreedLinkField = baseItem.Fields.FirstOrDefault(x => x.ID == new ID(_LocalBreedConstants.GlobalBreedInterfaceFieldId));
            if (globalBreedLinkField != null)
            {
                var globalBreed = baseItem.Database.GetItem(new ID(globalBreedLinkField.Value), baseItem.Language);
                return ExtractTaxonomyItems(globalBreed);
            }

            return Enumerable.Empty<Item>();
        }
        
        private static IEnumerable<Item> ExtractGlobalProductItems(Item baseItem)
        {
            var globalProductLinkField = baseItem.Fields.FirstOrDefault(x => x.ID == new ID(LocalProductConstants.GlobalProductFieldId));
            if (globalProductLinkField != null)
            {
                var globalProduct = baseItem.Database.GetItem(new ID(globalProductLinkField.Value), baseItem.Language);
                return ExtractTaxonomyItems(globalProduct);
            }

            return Enumerable.Empty<Item>();
        }
    }
}