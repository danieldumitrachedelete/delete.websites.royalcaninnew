﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper.QueryableExtensions;
using JetBrains.Annotations;
using RoyalCanin.Project.RoyalCanin.Models.Sitemap;
using Sitecore.Data;

namespace RoyalCanin.Project.RoyalCanin.Managers
{
    using System.Collections.Generic;
    using System.Linq;

    using Delete.Foundation.DataTemplates.Extensions;
    using Delete.Foundation.DataTemplates.Models.Interfaces;
    using Delete.Foundation.DeleteFoundationCore.Extensions;
    using Delete.Foundation.DeleteFoundationCore.Interfaces;
    using Delete.Foundation.DeleteFoundationCore.Managers;
    using Delete.Foundation.DeleteFoundationCore.Models;

    using Sitecore.ContentSearch.Linq;

    public interface ISitemapSearchManager : IBaseSearchManager<SitemapConfiguration>
    {
        IEnumerable<SitemapConfiguration> GetAllLocal(GlassBase rootItem);
    }
    public class SitemapSearchManager : BaseSearchManager<SitemapConfiguration>, ISitemapSearchManager
    {
        public IEnumerable<SitemapConfiguration> GetAllLocal(GlassBase rootItem)
        {
            var query = GetQueryable();
            query = FilterByRootItem(query, rootItem.Id);
            return query.MapResults(this.SitecoreContext, true).ToList();
        }


        private IQueryable<SitemapConfiguration> FilterByRootItem(IQueryable<SitemapConfiguration> query, Guid? rootItemId)
        {
            if (!rootItemId.HasValue)
            {
                return this.FilterLocal(query);
            }
            var rootItem = new ID(rootItemId.Value);
            return query.Where(x => x.Paths.Contains(rootItem)); 
        }

        protected override IQueryable<SitemapConfiguration> GetQueryable()
        {
            return base.GetQueryable()
                .Where(x => x.TemplateId == SitemapConfigurationConstants.TemplateId 
                            || x.BaseTemplates.Contains(SitemapConfigurationConstants.TemplateId.Guid)); 
        }
    }
}