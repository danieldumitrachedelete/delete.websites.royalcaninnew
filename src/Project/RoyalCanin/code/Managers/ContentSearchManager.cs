﻿namespace RoyalCanin.Project.RoyalCanin.Managers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Delete.Foundation.DataTemplates.Extensions;
    using Delete.Foundation.DataTemplates.Models.Interfaces;
    using Delete.Foundation.DeleteFoundationCore.Extensions;
    using Delete.Foundation.DeleteFoundationCore.Interfaces;
    using Delete.Foundation.DeleteFoundationCore.Managers;
    using Delete.Foundation.DeleteFoundationCore.Models;

    using Sitecore.ContentSearch.Linq;
    using Sitecore.ContentSearch.Linq.Utilities;

    using RCConstants = Delete.Foundation.RoyalCaninCore.Constants;

    public interface IContentSearchManager : IBaseSearchManager<_SearchableContent>
    {
        IEnumerable<_SearchableContent> GetAllLocal(string keyword, int? count = null);

        SearchResultItems<_SearchableContent> GetAllLocalSearchResults(string keyword, int? count = null);

        IEnumerable<_SearchableContent> GetSearchResultsWithHighlights(string keyword, int? numberOfSurroudingCharacters = null);
    }

    public class ContentSearchManager : BaseSearchManager<_SearchableContent>, IContentSearchManager
    {
        private readonly int numberOfSurroundingCharacters = 100;

        public IEnumerable<_SearchableContent> GetAllLocal(string keyword, int? count = null)
        {
            var query = this.GetQueryable();
            query = this.FilterLocal(query);
            query = FilterByKeyword(query, keyword);
            query = FilterByCount(query, count);
            return query.MapResults(this.SitecoreContext, true).ToList();
        }

        public SearchResultItems<_SearchableContent> GetAllLocalSearchResults(string keyword, int? count = null)
        {
            var query = this.GetQueryable();
            query = this.FilterLocal(query);
            query = FilterByKeyword(query, keyword);
            query = FilterByCount(query, count);

            var searchResults = query.GetResults(GetResultsOptions.GetScores);
            var documentsOrderedByScore = searchResults.Hits.OrderByDescending(x => x.Score).Select(x => x.Document).ToList();
            var result = new SearchResultItems<_SearchableContent>
            {
                TotalResults = searchResults.TotalSearchResults,
                Results = documentsOrderedByScore.MapResults(this.SitecoreContext, true).ToList()
            };
            return result;
        }

        public IEnumerable<_SearchableContent> GetSearchResultsWithHighlights(string keyword, int? surroundingCharactersCount = null)
        {
            var query = this.GetQueryable();
            query = this.FilterLocal(query);
            query = FilterByKeyword(query, keyword);

            var charactersCount = surroundingCharactersCount ?? this.numberOfSurroundingCharacters;
            var searchResults = query.GetResultsWithHighlights(new[] { _SearchableContentConstants.SearchableContentFieldName }, charactersCount);

            var itemsWithHighlights = new SearchResultItemsWithHighlights<_SearchableContent>();
            itemsWithHighlights.Results = searchResults.OrderByDescending(x => x.Score).Select(x => x).MapResults(this.SitecoreContext, true).ToList();
            itemsWithHighlights.TotalResults = searchResults.NumFound;
            itemsWithHighlights.Highlights = searchResults.Highlights;

            this.PopulateParents(itemsWithHighlights.Results);

            return itemsWithHighlights.ToSearchableContent();
        }

        protected override IQueryable<_SearchableContent> GetQueryable()
        {
            return base.GetQueryable().Where(x => x.IsSearchableItem && x.IsVisibleItem);
        }
        
        private static IQueryable<_SearchableContent> FilterByKeyword(IQueryable<_SearchableContent> query, string keyword)
        {
            if (keyword.IsNullOrEmpty())
            {
                return query;
            }

            var predicate = PredicateBuilder.False<_SearchableContent>();

            predicate = predicate.Or(x => x.DisplayName.StartsWith(keyword)
                                        || x.Name.StartsWith(keyword)
                                        || x.SearchableContent.Contains(keyword).Boost(RCConstants.Search.BoostLevels.Secondary)
                                        || x.SearchableTaxonomy.Contains(keyword).Boost(RCConstants.Search.BoostLevels.Secondary));


            // if keyword is a phrase, e.g. 'Jack Russell' or 'Dry Dog Food' 
            // than we search by each word separately, not by the whole phrase
            // because SOLR doesn't support searches like 'Jack Russ'

            var terms = keyword.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            if (terms.Length > 1)
            {
                var firstWord = terms.FirstOrDefault();

                var predicateDisplayName = PredicateBuilder.True<_SearchableContent>();
                predicateDisplayName = predicateDisplayName.And(x => x.DisplayName.StartsWith(firstWord));
                predicateDisplayName = terms.Skip(1).Aggregate(predicateDisplayName, (current, term) => current.And(x => x.DisplayName.Contains(term)));

                var predicateName = PredicateBuilder.True<_SearchableContent>();
                predicateName = predicateName.And(x => x.Name.StartsWith(firstWord));
                predicateName = terms.Skip(1).Aggregate(predicateName, (current, term) => current.And(x => x.Name.Contains(term)));

                var predicateContent = PredicateBuilder.True<_SearchableContent>();
                predicateContent = terms.Aggregate(predicateContent, 
                    (current, term) => current.And(item => item.SearchableContent.Contains(term).Boost(RCConstants.Search.BoostLevels.Secondary)));

                var predicateTaxonomy = PredicateBuilder.True<_SearchableContent>();
                predicateTaxonomy = terms.Aggregate(predicateTaxonomy,
                    (current, term) => current.And(item => item.SearchableTaxonomy.Contains(term).Boost(RCConstants.Search.BoostLevels.Secondary)));

                predicate = predicate.Or(predicateDisplayName);
                predicate = predicate.Or(predicateName);
                predicate = predicate.Or(predicateContent);
                predicate = predicate.Or(predicateTaxonomy);
            }


            return query.Where(predicate);
        }

        private static IQueryable<_SearchableContent> FilterByCount(IQueryable<_SearchableContent> query, int? count)
        {
            if (!count.HasValue || count < 0)
            {
                return query;
            }

            return query.Take(count.Value);
        }

        private void PopulateParents(IEnumerable<_SearchableContent> results)
        {
            var home = this.SitecoreContext.GetHomeItem<GlassBase>();
            if (home == null)
            {
                return;
            }

            foreach (var searchableContent in results)
            {
                searchableContent.Parents = new List<GlassBase>();

                if (searchableContent.Id == home.Id)
                {
                    continue;
                }

                var parent = searchableContent.Parent;
                while (parent?.Parent != null && parent.Id != home.Id)
                {
                    searchableContent.Parents.Add(parent);
                    parent = parent.Parent;
                }
            }
        }
    }
}