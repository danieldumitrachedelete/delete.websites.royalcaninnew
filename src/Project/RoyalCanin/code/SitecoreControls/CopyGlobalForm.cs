﻿using System;
using System.Collections.Generic;
using System.EnterpriseServices;
using System.Linq;
using System.Web.Helpers;
using System.Web.UI.WebControls;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.IO;
using Sitecore.Shell.Framework;
using Sitecore.Web;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Web.UI.Pages;
using Sitecore.Web.UI.Sheer;
using Sitecore.Web.UI.WebControls;
using Button = Sitecore.Web.UI.HtmlControls.Button;

namespace RoyalCanin.Project.RoyalCanin.SitecoreControls
{
    public class CopyGlobalForm : DialogForm
    {
        

        private string[] Tasks = new[] {
            "Checklist/Multilist with Search/Treelist/TreelistEx/Droplink/Droptree/Grouped Droplink field changes",
            "Name Lookup Value List/Name Value List field changes",
            "Renaming Sitecore item",
            "Dropdown & Multilist field changes",
            "Text/Rich text/Multi-Line/Droplist/Grouped Droplist Text field change",
            "Image field change including alt text",
            "Link field change",
            "Checkbox field change",
            "Removing component from page",
            "Adding component to page",
            "Adding new page"
        };


        protected Checklist WithSubItems;
        protected Checklist TasksList;
        protected Checklist MarketList;
        protected Edit Filename;
        protected TreeviewEx Treeview;
        protected DataContext DataContext;
        protected Button SelectAllTask;
        protected Button UnSelectAllTask;
        protected Checklist TargetLanguages;


        protected override void OnLoad(EventArgs e)
        {
            Assert.ArgumentNotNull(e, nameof(e));
            base.OnLoad(e);
            if (Context.ClientPage.IsEvent)
            {
                return;
            }

            BuildTaskList();
            BuildMarketList();
            BuildWorkingMode();
            BuildTargetLanguageList();

            SelectAllTask.Header = "Select all tasks";
            UnSelectAllTask.Header = "Unselect all tasks";

            this.DataContext.GetFromQueryString();
            Context.ClientPage.ServerProperties["id"] = WebUtil.GetQueryString("fo");

            var item = Context.ContentDatabase.GetItem("/sitecore/content/Global Configuration/Local Market Cloning/Local Market Template");
            this.DataContext.SetFolder(new ItemUri(item));
            var folder = this.DataContext.GetFolder();

            Assert.IsNotNull(folder, "Item not found");

            this.Filename.Value = this.ShortenPath(folder.Paths.Path);
        }

        private IEnumerable<KeyValuePair<string, string>> GetMarketList()
        {
            var sourcePath = "/sitecore/content/Royal Canin";
            return
                Context.ContentDatabase.GetItem(sourcePath)
                    .Children
                    .Where(x => x.TemplateName == "Local Market Settings")
                    .Select(x => new KeyValuePair<string, string>(x.ID.ToString(), x.Name));
        }

        private IEnumerable<KeyValuePair<string, string>> GetTargetLanguageList()
        {
            var sourcePath = "/sitecore/system/Languages";
            return
                Context.ContentDatabase.GetItem(sourcePath)
                    .Children
                    .Where(x => x.TemplateName == "Language")
                    .Select(x => new KeyValuePair<string, string>(x.ID.ToString(), x.Name));
        }

        protected override void OnOK(object sender, EventArgs args)
        {

            Assert.ArgumentNotNull(sender, nameof(sender));
            Assert.ArgumentNotNull(args, nameof(args));

            var targetPath = this.Filename.Value;
            if (targetPath.Length == 0)
            {
                SheerResponse.Alert("Select an item.", Array.Empty<string>());
            }

            var targetItem = this.DataContext.GetItem(targetPath);
            if (targetItem == null)
            {
                SheerResponse.Alert("The target item could not be found.", Array.Empty<string>());
            }
            else
            {
                var tasks = this.TasksList.Items.Where(x => x.Checked).Select(x => x.Value);
                var markets = this.MarketList.Items.Where(x => x.Checked).Select(x => x.Value);
                var workingMode = this.WithSubItems.Items.Where(x => x.Checked).Select(x => x.Value);
                var targetLanguages = this.TargetLanguages.Items.Where(x => x.Checked).Select(x => x.Value);


                var targetId = targetItem.ID.ToString();
                var filename = this.Filename.Value;
                

                var settings = new CopyGlobalSettings()
                {
                    Tasks = tasks,
                    Markets = markets,
                    TargetId = targetId,
                    Filename = filename,
                    WorkingMode = workingMode,
                    TargetLanguages = targetLanguages

                };

                var result = Json.Encode(settings);
                SheerResponse.SetDialogValue(result);
                base.OnOK(sender, args);
            }

            
        }

        private string ShortenPath(string path)
        {
            Assert.ArgumentNotNull(path, nameof(path));

            var root = this.DataContext.GetRoot();
            if (root != null && root.ID != root.Database.GetRootItem().ID)
            {
                var path1 = root.Paths.Path;
                if (path.StartsWith(path1, StringComparison.InvariantCulture))
                {
                    path = StringUtil.Mid(path, path1.Length);
                }
            }

            return path;
        }

        private void BuildTaskList()
        {
            foreach (var task in Tasks)
            {
                var checklistItem = new ChecklistItem();
                this.TasksList.Controls.Add(checklistItem);
                checklistItem.ID = Control.GetUniqueID("ChecklistItem");
                checklistItem.Header = task;
                checklistItem.Value = task;
            }
        }

        private void BuildWorkingMode()
        {
            var checklistItem = new ChecklistItem();
            this.WithSubItems.Controls.Add(checklistItem);
            checklistItem.ID = Control.GetUniqueID("ChecklistItem");
            checklistItem.Header = "With subitems";
            checklistItem.Value = "With subitems";
            
        }
        private void BuildMarketList()
        {
            foreach (var tierLevel in GetMarketList())
            {
                var checklistItem = new ChecklistItem();
                this.MarketList.Controls.Add(checklistItem);
                checklistItem.ID = Control.GetUniqueID("ChecklistItem");
                checklistItem.Header = tierLevel.Value;
                checklistItem.Value = tierLevel.Key;
            }
        }

        private void BuildTargetLanguageList()
        {
            foreach (var tierLevel in GetTargetLanguageList())
            {
                var checklistItem = new ChecklistItem();
                this.TargetLanguages.Controls.Add(checklistItem);
                checklistItem.ID = Control.GetUniqueID("ChecklistItem");
                checklistItem.Header = tierLevel.Value;
                checklistItem.Value = tierLevel.Key;
            }
        }

        protected void SelectTreeNode()
        {
            var selectionItem = this.Treeview.GetSelectionItem();
            if (selectionItem == null)
            {
                return;
            }

            this.Filename.Value = this.ShortenPath(selectionItem.Paths.Path);
        }

        protected void SelectAllTaskClick()
        {
            foreach (var task in TasksList.Items)
            {
                task.Checked = true;
            }
            
        }

        protected void UnSelectAllTaskClick()
        {
            foreach (var task in TasksList.Items)
            {
                task.Checked = false;
            }

        }
    }

    public class CopyGlobalSettings
    {
        public IEnumerable<string> Tasks { get; set; }
        public IEnumerable<string> Markets { get; set; }
        public IEnumerable<string> WorkingMode { get; set; }
        public IEnumerable<string> TargetLanguages { get; set; }
        public string TargetId { get; set; }
        public string Filename { get; set; }
    }
}