﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Helpers;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.IO;
using Sitecore.Shell.Framework;
using Sitecore.Web;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Web.UI.Pages;
using Sitecore.Web.UI.Sheer;
using Sitecore.Web.UI.WebControls;

namespace RoyalCanin.Project.RoyalCanin.SitecoreControls
{
    public class CopyMarketForm : DialogForm
    {
        private static readonly string RoyalCaninNodeId = Sitecore.Configuration.Settings.GetSetting("Project.RoyalCanin.MainNodeId");

        protected Checklist TierLevels;
        protected DataContext DataContext;
        protected Edit Filename;
        protected Edit TargetName;
        protected TreeviewEx Treeview;

        public override void HandleMessage(Message message)
        {
            Assert.ArgumentNotNull(message, nameof(message));
            Dispatcher.Dispatch(message, this.GetCurrentItem(message));
            base.HandleMessage(message);
        }

        protected override void OnLoad(EventArgs e)
        {
            Assert.ArgumentNotNull(e, nameof(e));
            base.OnLoad(e);
            if (Context.ClientPage.IsEvent)
            {
                return;
            }
            
            this.BuildTierLevels();
            this.DataContext.GetFromQueryString();
            Context.ClientPage.ServerProperties["id"] = WebUtil.GetQueryString("fo");

            var item = Context.ContentDatabase.GetItem(new ID(RoyalCaninNodeId));
            this.DataContext.SetFolder(new ItemUri(item));
            var folder = this.DataContext.GetFolder();

            Assert.IsNotNull(folder, "Item not found");

            this.Filename.Value = this.ShortenPath(folder.Paths.Path);
        }

        private void BuildTierLevels()
        {
            foreach (var tierLevel in GetTierLevels())
            {
                var checklistItem = new ChecklistItem();
                this.TierLevels.Controls.Add(checklistItem);
                checklistItem.ID = Control.GetUniqueID("ChecklistItem");
                checklistItem.Header = tierLevel.Value;
                checklistItem.Value = tierLevel.Key;
            }
        }

        private IEnumerable<KeyValuePair<string, string>> GetTierLevels()
        {
            var sourcePath = "/sitecore/content/Global Configuration/Local Market Cloning/Tier Levels";
            return 
                Context.ContentDatabase.GetItem(sourcePath)
                .Children
                .Select(x => new KeyValuePair<string, string>(x.ID.ToString(), x.Name));
        }

        protected override void OnOK(object sender, EventArgs args)
        {
            Assert.ArgumentNotNull(sender, nameof(sender));
            Assert.ArgumentNotNull(args, nameof(args));

            var targetPath = this.Filename.Value;
            if (targetPath.Length == 0)
            {
                SheerResponse.Alert("Select an item.", Array.Empty<string>());
            }

            var root = this.DataContext.GetRoot();
            if (root != null && root.ID != root.Database.GetRootItem().ID)
            {
                targetPath = FileUtil.MakePath(root.Paths.Path, targetPath, '/');
            }

            var targetItem = this.DataContext.GetItem(targetPath);
            if (targetItem == null)
            {
                SheerResponse.Alert("The target item could not be found.", Array.Empty<string>());
            }
            else
            {
                var sourceItemId = Context.ClientPage.ServerProperties["id"] as string;
                if (sourceItemId.NotEmpty())
                {
                    var sourceItem = Context.ContentDatabase.Items[sourceItemId];
                    if (sourceItem == null)
                    {
                        SheerResponse.Alert("The source item could not be found.", Array.Empty<string>());
                        return;
                    }
                    if (targetItem.ID.ToString() == sourceItem.ID.ToString())
                    {
                        SheerResponse.Alert("Select a different item.", Array.Empty<string>());
                        return;
                    }
                    if (targetItem.Paths.LongID.StartsWith(sourceItem.Paths.LongID, StringComparison.InvariantCulture))
                    {
                        SheerResponse.Alert("An item cannot be copied below itself.", Array.Empty<string>());
                        return;
                    }
                }

                if (!targetItem.Access.CanCreate())
                {
                    SheerResponse.Alert("The item cannot be copied to this location because\nyou do not have Create permission.", Array.Empty<string>());
                }
                else
                {
                    var tiers = this.TierLevels.Items.Where(x => x.Checked).Select(x => x.Value);
                    var targetId = targetItem.ID.ToString();
                    var targetName = this.TargetName.Value;

                    var settings = new CopyMarketSettings { Tiers = tiers, TargetId = targetId, TargetName = targetName };

                    var result = Json.Encode(settings);
                    SheerResponse.SetDialogValue(result);
                    base.OnOK(sender, args);
                }
            }
        }

        protected void SelectTreeNode()
        {
            var selectionItem = this.Treeview.GetSelectionItem();
            if (selectionItem == null)
            {
                return;
            }

            this.Filename.Value = this.ShortenPath(selectionItem.Paths.Path);
        }

        private Item GetCurrentItem(Message message)
        {
            Assert.ArgumentNotNull(message, nameof(message));
            var index = message["id"];
            var folder = this.DataContext.GetFolder();
            var language = Context.Language;
            if (folder != null)
            {
                language = folder.Language;
            }

            if (index.NotEmpty())
            {
                return Context.ContentDatabase.Items[index, language];
            }

            return folder;
        }

        private string ShortenPath(string path)
        {
            Assert.ArgumentNotNull(path, nameof(path));

            var root = this.DataContext.GetRoot();
            if (root != null && root.ID != root.Database.GetRootItem().ID)
            {
                var path1 = root.Paths.Path;
                if (path.StartsWith(path1, StringComparison.InvariantCulture))
                {
                    path = StringUtil.Mid(path, path1.Length);
                }
            }

            return path;
        }
    }

    public class CopyMarketSettings
    {
        public IEnumerable<string> Tiers { get; set; }
        public string TargetId { get; set; }
        public string TargetName { get; set; }
    }
}