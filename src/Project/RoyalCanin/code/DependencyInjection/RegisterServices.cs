using Delete.Feature.Products;
using Delete.Feature.Stockists;
using Delete.Foundation.DeleteFoundationCore.Managers;
using Delete.Foundation.InitializeDependencyInjection.Extensions;
using Microsoft.Extensions.DependencyInjection;
using RoyalCanin.Project.RoyalCanin.Models;
using Sitecore.DependencyInjection;

namespace RoyalCanin.Project.RoyalCanin.DependencyInjection
{
    using Managers;

    public class RegisterServices : IServicesConfigurator
    {
        public void Configure(IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IContentSearchManager, ContentSearchManager>();
            serviceCollection.AddScoped<ISitemapSearchManager, SitemapSearchManager>();

            RegisterIndexes();

            serviceCollection.AddMvcControllersInCurrentAssembly();
        }

        private static void RegisterIndexes()
        {
            SearchManagersConfiguration.RegisterIndexConfiguration(typeof(ProductPage), ProductsConstants.CustomIndexAlias);
            SearchManagersConfiguration.RegisterIndexConfiguration(typeof(StockistsPage), StockistsConstants.CustomIndexAlias);
            SearchManagersConfiguration.RegisterIndexConfiguration(typeof(ProductCataloguePage), ProductsConstants.CustomIndexAlias);
        }
    }
}
