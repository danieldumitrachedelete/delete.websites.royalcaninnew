﻿using Sitecore.Data;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using Quartz;
using System.IO;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.ItemImport.Loggers;
using Delete.Foundation.ItemImport.Models;
using Delete.Foundation.ItemImport.Services;
using log4net;
using System.Linq;
using Delete.Foundation.DeleteFoundationCore.Constants;
using Delete.Foundation.MultiSite.Helpers;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.Data.Events;
using Sitecore.DependencyInjection;
using Sitecore.SecurityModel;
using Sitecore.Sites;
using Sitecron.SitecronSettings;


namespace RoyalCanin.Project.CheckProduct.SitecronTasks
{
    public class CheckProducts : IJob
    {
        public const string ProductImportLoggerName = "RoyalCanin.Project.CheckProduct";

        private readonly ILog logger = LogManager.GetLogger(ProductImportLoggerName);

        private readonly IImportJobLogger importJobLogger;

        public CheckProducts()
        {
            this.importJobLogger = new ImportJobLogger(logger);
        }

        public void Execute(IJobExecutionContext context)
        {
            var jobDataMap = context.JobDetail.JobDataMap;
            var items = jobDataMap.GetString(SitecronConstants.FieldNames.Items);
            var siteRoots = items.ToGuidList("|");
            
            
            if (siteRoots.Any())
            {
                try
                {
                    foreach (var siteRoot in siteRoots)
                    {
                        var rootPath = Database.GetDatabase(Constants.Sitecore.Databases.Master)
                            ?.GetItem(new ID(siteRoot))?.Paths.FullPath;
                        var sites = SiteManagerHelper.GetWebsitesByRoot(rootPath);
                        foreach (var site in sites)
                        {
                            var siteContext = SiteContext.GetSite(site.Name);

                            this.logger.Info(
                                $"Importing products for site RootPath: {siteContext.RootPath}; Language: {siteContext.Language}; Database: {siteContext.Database}");

                            using (new SecurityDisabler())
                            using (new SiteContextSwitcher(siteContext))
                            using (new EventDisabler())
                            using (new BulkUpdateContext())
                            using (var innerScope = ServiceLocator.ServiceProvider
                                .GetRequiredService<IServiceScopeFactory>().CreateScope())
                            {

                                this.logger.Info("CheckProduct job started");

                                var mediaItemService = innerScope.ServiceProvider.GetService<IMediaItemService>();

                                var websiteMediaFolder = mediaItemService.GetLocalMarketMediaFolder();
                                var imagesFolder = mediaItemService.GetOrCreateMediaFolder(websiteMediaFolder, "Images");
                                var productsFolder = mediaItemService.GetOrCreateMediaFolder(imagesFolder, "Products");

                                var folders = new List<MediaFolder>();
                                folders.Add(mediaItemService.GetOrCreateMediaFolder(productsFolder, "Dogs"));
                                folders.Add(mediaItemService.GetOrCreateMediaFolder(productsFolder, "Cats"));

                                Database db = Sitecore.Configuration.Factory.GetDatabase("master");

                                foreach (var folder in folders)
                                {
                                    var products = folder.Children.ToList();

                                    foreach (var product in products)
                                    {
                                        var images = product.Children.ToList();

                                        foreach (var image in images)
                                        {
                                            var item = db.GetItem(new ID(image.Id));
                                            ChangeItem(item);
                                            CheckFields(item);
                                        }

                                    }

                                }

                                this.logger.Info("CheckProduct job end");

                            }
                        }
                    }
  
                }
                catch (Exception ex)
                {
                    this.logger.Error($"Error: {ex}");
                }

            }
        }
        

        private void CheckFields(Item JpgItem)
        {
            
            try
            {
                MediaItem mediaItem = new MediaItem(JpgItem);
                using (new SecurityDisabler())
                {
                    if (mediaItem.InnerItem.Fields["Height"].Value == "")
                    {
                        this.logger.Info($"Item: {JpgItem.ID} - have null value of Height");
                    }
                    else
                    {
                        this.logger.Info($"Item: {JpgItem.ID} - right value of Height");
                    }
                    if (mediaItem.InnerItem.Fields["Width"].Value == "")
                    {
                        this.logger.Info($"Item: {JpgItem.ID} - have null value of Width");
                    }
                    else
                    {
                        this.logger.Info($"Item: {JpgItem.ID} - right value of Width");
                        
                    }
                }

            }
            catch (Exception ex)
            {
                this.logger.Error($" Error Item: {JpgItem.ID} - {ex}");
            }

        }

        private void ChangeItem(Item JpgItem)
        {
            try
            {                
                MediaItem mediaItem = new MediaItem(JpgItem);
                var image = new System.Drawing.Bitmap(mediaItem.GetMediaStream());

                mediaItem.GetMediaStream();
                
                using (new SecurityDisabler())
                {
                    mediaItem.InnerItem.Editing.BeginEdit();

                    if(mediaItem.InnerItem.Fields["Height"].Value != image.Height.ToString())
                    {
                        mediaItem.InnerItem.Fields["Height"].Value = image.Height.ToString();
                        this.logger.Info($"Item: {JpgItem.ID} - edited Height");
                    }
                    else
                    {
                        this.logger.Info($"Item: {JpgItem.ID} - Not needed change the value a Height");
                    }

                    if (mediaItem.InnerItem.Fields["Width"].Value != image.Width.ToString())
                    {
                        mediaItem.InnerItem.Fields["Width"].Value = image.Width.ToString();
                        this.logger.Info($"Item: {JpgItem.ID} - edited Width");
                    }
                    else
                    {
                        this.logger.Info($"Item: {JpgItem.ID} - Not needed change the value a Width");
                    }
                    
                    mediaItem.InnerItem.Editing.EndEdit();
                }

                
            }
            catch (Exception ex)
            {
                this.logger.Error($" Error with Item: {JpgItem.ID} - {ex}");
            }
        }

       
    }

}