using AutoMapper;
using Delete.Foundation.DeleteFoundationCore.Controllers;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Glass.Mapper.Sc;

namespace RoyalCanin.Project.CheckProduct.Controllers
{
    using System;
    using System.Web.Mvc;
    using Sitecore.Data;
    using Repositories;

    public class CheckProductController : BaseController
    {
        private readonly ICheckProductRepository _CheckProductRepository;
        private string CheckProductCacheKey = "CheckProduct_Component_CacheParametersShouldBeAddedHere";

        public CheckProductController(ICheckProductRepository CheckProductRepository, ISitecoreContext sitecoreContext, IMapper mapper, ICacheManager cacheManager) : base(sitecoreContext, mapper, cacheManager)
        {
            _CheckProductRepository = CheckProductRepository;
        }

        public ActionResult CheckProduct()
        {
            var cacheKey = string.Format(CheckProductCacheKey);
            /* 
              TODO: Use the repository to retrieve model data 
              which can be passed into the view.
            */

            //var datasourceItem = GetDataSourceItem<Models.CheckProduct>(false, true);
            //var model = _cacheManager.CacheResults(() => _mapper.Map<CheckProductViewModel>(datasourceItem), string.Format(CheckProductCacheKey));
            //return View("~/Views/Project/CheckProduct/CheckProduct.cshtml", model);

            throw new NotImplementedException();
        }
    }
}
