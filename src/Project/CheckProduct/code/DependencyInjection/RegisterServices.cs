using Delete.Foundation.InitializeDependencyInjection.Extensions;
using Delete.Foundation.ItemImport.Services;
using Microsoft.Extensions.DependencyInjection;
using RoyalCanin.Project.CheckProduct.Repositories;
using Sitecore.DependencyInjection;

namespace RoyalCanin.Project.CheckProduct.DependencyInjection
{
    public class RegisterServices : IServicesConfigurator
    {
        public void Configure(IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<ICheckProductRepository, CheckProductRepository>();

            serviceCollection.AddScoped<IMediaItemService, MediaItemService>();

            serviceCollection.AddMvcControllersInCurrentAssembly();
        }
    }
}
