﻿namespace Delete.Feature.Articles
{
    public static class ArticlesConstants
    {
        public const int FeaturedContentSize = 3;
        public const int RelatedContentSize = 3;

        public const string CustomIndexAlias = "articles";
    }
}
