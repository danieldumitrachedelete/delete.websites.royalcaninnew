using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Delete.Feature.Articles.Comparers;
using Delete.Feature.Articles.Managers;
using Delete.Feature.Articles.Models;
using Delete.Foundation.DataTemplates.Managers;
using Delete.Foundation.DataTemplates.Models;
using Delete.Foundation.DeleteFoundationCore.Controllers;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.DeleteFoundationCore.Models;
using Glass.Mapper.Sc;

namespace Delete.Feature.Articles.Controllers
{
    public class ArticlesController : BaseController
    {
        private const string ArticlesCacheKey = "articles_{0}_{1}_{2}_{3}";
        private const string RelatedArticlesCacheKey = "relatedarticles_{0}";
        private const string FeaturedContentCacheKey = "featuredcontent_{0}";
        private const string ArticlesByTaxonomyCacheKey = "articlesbytaxonomy_{0}_{1}";
        private const string ArticlesFilterGroupsCacheKey = "articlesfiltergroups_{0}_{1}_{2}_{3}";

        private readonly IArticleSearchManager articleSearchManager;
        private readonly IFilterGroupFolderSearchManager filterGroupFolderSearchManager;

        public ArticlesController(
            ISitecoreContext sitecoreContext,
            IMapper mapper,
            ICacheManager cacheManager,
            IArticleSearchManager articleSearchManager,
            IFilterGroupFolderSearchManager filterGroupFolderSearchManager) : base(sitecoreContext, mapper, cacheManager)
        {
            this.articleSearchManager = articleSearchManager;
            this.filterGroupFolderSearchManager = filterGroupFolderSearchManager;
        }

        [HttpGet]
        public ActionResult Search()
        {
            var articleFilterConfiguration = this.GetDataSourceItem<ArticleFilterConfiguration>();
            if (articleFilterConfiguration == null)
            {
                return this.Content(Sitecore.Context.PageMode.IsExperienceEditorEditing ? "<p>Please select article filter configuration</p>" : string.Empty);
            }

            var species = articleFilterConfiguration.Species;
            var selectedFilterGroups = articleFilterConfiguration.Filters;

            var contextItem = this.GetContextItem<GlassBase>();

            var listingRootId = articleFilterConfiguration.ListingRoot?.Id ?? contextItem.Id;

            var preFiltering = articleFilterConfiguration
                .PreFilterings
                .GroupBy(x => x.Parent.Id);
                
            var filterIds = preFiltering
                .ToDictionary(x => x.First().Parent.Name, x => x.Select(y => y.Id));

            var cacheKey = string.Format(ArticlesCacheKey, listingRootId, species?.Id, null, string.Join("|", filterIds.SelectMany(x => x.Value)));
            var articles = this._cacheManager.CacheResults(() => this.articleSearchManager.GetAll(listingRootId, species?.Id, null, filterIds).ToList(), cacheKey);

            var resultArticles = articles;

            var groupCacheKey = string.Format(ArticlesFilterGroupsCacheKey, listingRootId, species?.Id, null, string.Join("|", filterIds.SelectMany(x => x.Value)));
            var allFilterGroups = this._cacheManager.CacheResults(() => this.filterGroupFolderSearchManager.GetAll(), groupCacheKey);
            var sortedFilterGroups = selectedFilterGroups
                .Select(x => x.Id)
                .Select(filterGroupId => allFilterGroups.FirstOrDefault(x => x.Id == filterGroupId))
                .Where(g => g != null)
                .ToList();

            var articleTopicComparer = new ArticleTopicComparer();
            var allTopics = resultArticles.SelectMany(x => x.ArticleTopics).Distinct(articleTopicComparer).ToList();

            if (filterIds.Any(x => x.Value.Intersect(allTopics.Select(y => y.Id)).Any()))
            {
                allTopics = allTopics.Where(x => filterIds.Any(y => y.Value.Contains(x.Id))).ToList();
            }

            var result = new SearchViewModel
            {
                FilterModel = new SearchFilterViewModel
                {
                    FilterGroups = sortedFilterGroups,
                    PreFiltering = preFiltering.ToDictionary(x => x.Key, y => y.Select(x => x.Id)),
                    FilterRelatedSearchResults = articles,
                    Specie = species,
                    FilterId = articleFilterConfiguration.Id
                },
                SearchResultsModel = GetSearchResultsModel(articleFilterConfiguration, resultArticles, allTopics)
            };

            return this.View("~/Views/Feature/Articles/Search.cshtml", result);
        }

        public ActionResult FeaturedContent()
        {
            var contextItem = this.GetContextItem<GlassBase>();

            if (contextItem == null)
            {
                return new EmptyResult();
            }

            var cacheKey = string.Format(FeaturedContentCacheKey, contextItem.Id);

            var articles = this._cacheManager.CacheResults(() =>
                    this.articleSearchManager
                        .GetLatest(contextItem.Id, ArticlesConstants.FeaturedContentSize)
                        .ToList(),
                cacheKey);

            var result = new FeaturedContentModel
            {
                LeftItem = articles.FirstOrDefault(),
                RightItems = articles.Skip(1).ToList(),
            };

            return this.View("~/Views/Feature/Articles/FeaturedContent.cshtml", result);
        }

        
        public ActionResult RelatedArticles()
        {
            var contextItem = this.GetContextItem<Article>();

            if (contextItem == null || !contextItem.IsDerivedFrom(ArticleConstants.TemplateId))
            {
                return new EmptyResult();
            }

            var cacheKey = string.Format(RelatedArticlesCacheKey, contextItem.Id);
            var articles = this._cacheManager.CacheResults(()=> this.articleSearchManager.FindRelated(contextItem).ToList(), cacheKey);

            return this.View("~/Views/Feature/Articles/RelatedArticles.cshtml", articles);
        }

        public ActionResult CardPanel()
        {
            var sourceItem = this.GetDataSourceItem<ArticleCardPanel>();

            if (sourceItem == null)
            {
                return new EmptyResult();
            }

            var articles = new List<Article>();

            if (sourceItem.SpecifiedArticles.Any())
            {
                articles.AddRange(sourceItem.SpecifiedArticles);
            }
            else
            {
                //var cacheKey = string.Format(ArticlesByTaxonomyCacheKey, string.Join("|", sourceItem.TaxonomyIds), sourceItem.Count);
                //var filtered = this._cacheManager.CacheResults(() => articleSearchManager.FindArticles(sourceItem, sourceItem.Count).ToList(), cacheKey);
                var filtered = articleSearchManager.FindArticles(sourceItem, sourceItem.Count).ToList();
                articles.AddRange(filtered);
            }

            var model = new ArticleCardPanelViewModel
            {
                Header = sourceItem.Header,
                Articles = articles,
                Spacers = sourceItem.Spacers,
                Width = sourceItem.Width
            };

            return this.View("~/Views/Feature/Articles/ArticleCardPanel.cshtml", model);
        }

        public ActionResult Carousel()
        {
            var sourceItem = this.GetDataSourceItem<ArticleCarousel>();
            ArticleCarouselViewModel model;
            if (sourceItem.SelectArticles.Any())
            {
                model = new ArticleCarouselViewModel
                {
                    FilterConfiguration = sourceItem.Id,
                    Header = sourceItem.Header,
                    Articles = sourceItem.SelectArticles.ToList(),
                    GridView = false,
                };
            }
            else
            {
                model = GetCarouselViewModel(sourceItem, false);
            }

            if (model == null)
            {
                return new EmptyResult();
            }

            return this.View("~/Views/Feature/Articles/ArticleCarousel.cshtml", model);
        }

        public ActionResult ExpandCarousel(Guid filter)
        {
            var sourceItem = this.SitecoreContext.GetItem<ArticleCarousel>(filter);
            ArticleCarouselViewModel model;
            if (sourceItem.SelectArticles.Any())
            {
                model = new ArticleCarouselViewModel
                {
                    FilterConfiguration = sourceItem.Id,
                    Header = sourceItem.Header,
                    Articles = sourceItem.SelectArticles.ToList(),
                    GridView = true,
                };
            }
            else
            {
                model = GetCarouselViewModel(sourceItem, true);
            }

            if (model == null)
            {
                return new EmptyResult();
            }

            return this.View("~/Views/Feature/Articles/ArticleCarousel.cshtml", model);
        }

        private ArticleCarouselViewModel GetCarouselViewModel(ArticleCarousel sourceItem, bool gridView)
        {
            if (sourceItem == null)
            {
                return null;
            }

            var cacheKey = string.Format(ArticlesByTaxonomyCacheKey, string.Join("|", sourceItem.TaxonomyIds), sourceItem.Count);
            var articles = this._cacheManager.CacheResults(() => articleSearchManager.FindArticles(sourceItem).ToList(), cacheKey);

            var sourceIds = sourceItem.RelatedEntities;

            articles = articles
                .OrderByDescending(x =>
                {
                    var ids = x.RelatedEntities.ToList();

                    return (double)ids.Intersect(sourceIds).Count() / ids.Count();
                })
                .Take(sourceItem.Count)
                .ToList();

            var model = new ArticleCarouselViewModel
            {
                FilterConfiguration = sourceItem.Id,
                Header = sourceItem.Header,
                Articles = articles,
                GridView = gridView,
            };

            return model;
        }

        [HttpGet]
        public ActionResult SearchArticles(ArticlesFilterViewModel filterConfiguration)
        {
            var articleFilterConfiguration = this.SitecoreContext.GetItem<ArticleFilterConfiguration>(filterConfiguration.Filter, false, true);
            var species = articleFilterConfiguration.Species;
            var listingRootId = articleFilterConfiguration.ListingRoot?.Id;

            var preFiltering = articleFilterConfiguration.PreFilterings.Select(x => x.Id).ToList();

            var filterModel = new ArticlesFilterModel(filterConfiguration)
            {
                Species = articleFilterConfiguration.Species.Id,
                ListingRootId = articleFilterConfiguration.ListingRoot?.Id,
            };

            var filterIds = this.GetRequestFilterIdsFromCheckboxes(filterConfiguration);

            var cacheKey = string.Format(ArticlesCacheKey, listingRootId, species?.Id, filterModel.SearchQuery, string.Join("|", filterIds.SelectMany(x => x.Value)));
            var allArticles = this._cacheManager.CacheResults(() => this.articleSearchManager.GetAll(listingRootId, species?.Id, filterModel.SearchQuery, filterIds).ToList(), cacheKey);

            var selectedTopicIds = filterConfiguration.ArticleTopics.ToGuidList();
            if (!selectedTopicIds.Any())
            {
                selectedTopicIds = preFiltering;
            }
            var selectedTopics = allArticles
                .SelectMany(x => x.ArticleTopics)
                .Where(x => !selectedTopicIds.Any() || selectedTopicIds.Contains(x.Id))
                .Distinct(new ArticleTopicComparer())
                .ToList();

            var searchResultModel = GetSearchResultsModel(articleFilterConfiguration, allArticles, selectedTopics);

            if (filterConfiguration.ViewAll)
            {
                return this.View("~/Views/Feature/Articles/_AllArticlesSearchResults.cshtml", searchResultModel);
            }

            return this.View("~/Views/Feature/Articles/_ArticlesSearchResults.cshtml", searchResultModel);
        }

        private IDictionary<string, IEnumerable<Guid>> GetRequestFilterIdsFromCheckboxes(ArticlesFilterViewModel filterConfiguration)
        {
            var filterValues = new Dictionary<string, IEnumerable<Guid>>();

            var articleTopics = filterConfiguration.ArticleTopics.ToGuidList();

            var lifestages = filterConfiguration.Lifestages.ToGuidList();

            var sizes = filterConfiguration.Sizes.ToGuidList();

            if (articleTopics.Any())
            {
                filterValues.Add(nameof(filterConfiguration.ArticleTopics), articleTopics);
            }

            if (lifestages.Any())
            {
                filterValues.Add(nameof(filterConfiguration.Lifestages), lifestages);
            }

            if (sizes.Any())
            {
                filterValues.Add(nameof(filterConfiguration.Sizes), sizes);
            }

            return filterValues;
        }

        private static SearchResultsViewModel GetSearchResultsModel(ArticleFilterConfiguration articleFilterConfiguration, List<Article> resultArticles, List<Models.Taxonomy.ArticleTopic> allTopics)
        {
            var species = articleFilterConfiguration.Species;
            var pageSize = articleFilterConfiguration.PageSize;
            var maxPageCount = articleFilterConfiguration.MaxPageCount;
            var articleTopicComparer = new ArticleTopicComparer();

            return new SearchResultsViewModel
            {
                PageSize = pageSize,
                MainFilterGroup = articleFilterConfiguration.MainFilter,
                Specie = species,
                SearchResults = allTopics
                    .Select(x =>
                    {
                        var relevantArticles = resultArticles.Where(y => y.ArticleTopics.Contains(x, articleTopicComparer)).ToList();

                        return new SearchResultsGroupViewModel
                        {
                            Key = x,
                            Items = relevantArticles.Take(pageSize * maxPageCount).ToList(),
                            ItemsMaxCount = relevantArticles.Count(),
                        };
                    })
                    .OrderBy(x => x.Key?.Text, StringComparer.CurrentCultureIgnoreCase)
            };
        }

        [HttpGet]
        public ActionResult FindByKeyword(Guid? filter, string keyword = "")
        {
            var model = new PredictiveSearchJsonModel();

            if (filter.HasValue)
            {
                var articleFilterConfiguration = this.SitecoreContext.GetItem<ArticleFilterConfiguration>(filter.Value, false, true);
                var species = articleFilterConfiguration.Species;
                var listingRootId = articleFilterConfiguration.ListingRoot?.Id;

                var preFiltering = articleFilterConfiguration.PreFilterings.Select(x => x.Id).ToList();
                var filterIds = new Dictionary<string, IEnumerable<Guid>> { { string.Empty, preFiltering } };

                var cacheKey = string.Format(ArticlesCacheKey, listingRootId, species?.Id, keyword, string.Join("|", preFiltering));
                var articles = this._cacheManager.CacheResults(() => this.articleSearchManager.GetAll(listingRootId, species?.Id, keyword, filterIds).ToList(), cacheKey);

                var predictiveItems = this._mapper.Map<IEnumerable<PredictiveSearchItemJsonModel>>(articles);
                model.Items = predictiveItems;
            }

            return this.Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}
