using AutoMapper;
using Delete.Feature.Articles.Models;
using Delete.Foundation.DataTemplates.Models;
using Delete.Foundation.DeleteFoundationCore.Extensions;

namespace Delete.Feature.Articles.Profiles
{
    public class ArticlesProfile : Profile
    {
        public ArticlesProfile()
        {
            CreateMap<Article, PredictiveSearchItemJsonModel>()
                .ForMember(x => x.Title, opt => opt.MapFrom(z => z.DisplayName.OrDefault(z.Name)))
                .ForMember(x => x.Description, opt => opt.Ignore())
                .ForMember(x => x.Url, opt => opt.MapFrom(z => z.Url))
                ;
        }
    }
}
