﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Delete.Feature.Articles.Models;
using Delete.Feature.Articles.Models.Taxonomy;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.DeleteFoundationCore.Managers;
using Delete.Foundation.DeleteFoundationCore.Models;
using Delete.Foundation.MultiSite.Models;
using Sitecore.ContentSearch.Linq;
using Sitecore.ContentSearch.Linq.Utilities;
using Sitecore.Data;

using BoostLevels = Delete.Foundation.RoyalCaninCore.Constants.Search.BoostLevels;

namespace Delete.Feature.Articles.Managers
{
    public interface IArticleSearchManager : IBaseSearchManager<Article>
    {
        IEnumerable<Article> GetAll(Guid? rootItemId = null,
            Guid? specieId = null,
            string keyword = null,
            IDictionary<string, IEnumerable<Guid>> filterIds = null);
        IEnumerable<Article> GetLatest(Guid? rootItemId = null, int count = 0);
        IEnumerable<Article> FindRelated(Article item);
        IEnumerable<Article> FindArticles(IArticleTaxonomy item, int maxCount = 0);
    }

    public class ArticleSearchManager : BaseSearchManager<Article>, IArticleSearchManager
    {
        public IEnumerable<Article> GetAll(Guid? rootItemId = null,
            Guid? specieId = null,
            string keyword = null,
            IDictionary<string, IEnumerable<Guid>> filterIds = null)
        {
            var filtered = this.GetQueryable();
            filtered = this.FilterByRootItem(filtered, rootItemId);
            filtered = this.FilterBySpecie(filtered, specieId);
            filtered = this.FilterByKeyword(filtered, keyword);
            filtered = this.FilterByRelatedEntities(filtered, filterIds);

            return filtered
                .MapResults(SitecoreContext, true)
                .ToList();
        }

        public IEnumerable<Article> GetLatest(Guid? rootItemId = null, int count = 0)
        {
            var filtered = this.GetQueryable();
            filtered = this.FilterByRootItem(filtered, rootItemId);
            filtered = filtered.OrderByDescending(x => x.Date);

            if (count > 0)
            {
                filtered = filtered.Take(count);
            }

            return filtered
                .MapResults(SitecoreContext, true)
                .ToList();
        }

        private IQueryable<Article> FilterByRelatedEntities(
            IQueryable<Article> query,
            IDictionary<string, IEnumerable<Guid>> filterIds)
        {
            if (filterIds == null || !filterIds.Any())
            {
                return query;
            }

            return query.Where(BuildFilterGroupExpression(filterIds));
        }

        private IQueryable<Article> FilterByRootItem(IQueryable<Article> query, Guid? rootItemId)
        {
            if (!rootItemId.HasValue)
            {
                return this.FilterLocal(query);
            }

            var rootItem = new ID(rootItemId.Value);
            return query.Where(x => x.Paths.Contains(rootItem));
        }

        private IQueryable<Article> FilterBySpecie(IQueryable<Article> query, Guid? specieId)
        {
            if (!specieId.HasValue)
            {
                return query;
            }

            return query.Where(x => x.SpecieIds.Contains(specieId.Value));
        }

        private IQueryable<Article> FilterByKeyword(IQueryable<Article> query, string keyword)
        {
            if (!keyword.NotEmpty())
            {
                return query;
            }

            var predicate = this.FilterByKeyword(keyword);
            return query.Where(predicate);
        }

        public IEnumerable<Article> FindRelated(Article item)
        {
            var rootItemId = item.GetListingRoot()?.Id;

            return Find(item, rootItemId, ArticlesConstants.RelatedContentSize, item.Parent?.Id);
        }

        public IEnumerable<Article> FindArticles(IArticleTaxonomy item, int maxCount = 0)
        {
            var gbItem = item as GlassBase;
            var rootItemId = gbItem.GetAscendantOfTemplate(LocalMarketSettingsConstants.TemplateId)?.Id ?? SitecoreContext.GetHomeItem<GlassBase>()?.Id;
            var contextItemId = SitecoreContext.GetCurrentItem<GlassBase>()?.Id;

            return Find(item, rootItemId, maxCount, contextItemId);
        }

        private IEnumerable<Article> Find(IArticleTaxonomy item, Guid? rootItemId, int maxCount, Guid? parentId)
        {
            var filtered = GetQueryable().Where(x => x.Id != item.Id);

            if (rootItemId != null)
            {
                var rootItem = new ID(rootItemId.Value);
                var predicate = PredicateBuilder.Create<Article>(x => x.Paths.Contains(rootItem));

                if (parentId.HasValue)
                {
                    var id = new ID(parentId.Value);
                    
                    predicate = predicate.Or(x => (x.ParentId == id).Boost(BoostLevels.Secondary) || (x.Paths.Contains(id)).Boost(BoostLevels.Tertiary));
                }

                filtered = filtered.Where(predicate);
            }
            else
            {
                filtered = this.FilterLocal(filtered);
            }

            filtered = FilterRelated(filtered, item);

            var ordered = filtered
                    .GetResults()
                    .Hits
                    .OrderByDescending(x => x.Score)
                    .Select(x => x.Document)
                    .AsQueryable()
                ;

            if (maxCount > 0)
            {
                ordered = ordered.Take(maxCount);
            }

            return ordered
                .MapResults(SitecoreContext, true);
        }

        protected override IQueryable<Article> GetQueryable()
        {
            return base.GetQueryable()
                .Where(x => x.TemplateId == ArticleConstants.TemplateId
                            || x.BaseTemplates.Contains(ArticleConstants.TemplateId.Guid));
        }

        protected Expression<Func<Article, bool>> FilterByKeyword(string keyword)
        {
            var predicate = PredicateBuilder.False<Article>();

            predicate = predicate.Or(x => x.Heading.StartsWith(keyword)
                                          || x.SearchableArticleTopicsText.StartsWith(keyword)
                                          || x.SearchableArticleLifestageText.StartsWith(keyword)
                                          || x.SearchableArticleAnimalSizeText.StartsWith(keyword));
        
            var terms = keyword.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            if(terms.Length > 1)
            {
                var firstWord = terms.FirstOrDefault();

                var predicateHeading = PredicateBuilder.True<Article>();
                predicateHeading = predicateHeading.And(x => x.Heading.StartsWith(firstWord));
                predicateHeading = terms.Skip(1).Aggregate(predicateHeading, (current, term) => current.And(x => x.Heading.Contains(term)));

                var predicateTopics = PredicateBuilder.True<Article>();
                predicateTopics = predicateTopics.And(x => x.SearchableArticleTopicsText.StartsWith(firstWord));
                predicateTopics = terms.Skip(1).Aggregate(predicateTopics, (current, term) => current.And(x => x.SearchableArticleTopicsText.Contains(term)));

                var predicateLifestages = PredicateBuilder.True<Article>();
                predicateLifestages = predicateLifestages.And(x => x.SearchableArticleLifestageText.StartsWith(firstWord));
                predicateLifestages = terms.Skip(1).Aggregate(predicateLifestages, (current, term) => current.And(x => x.SearchableArticleLifestageText.Contains(term)));

                var predicateSize = PredicateBuilder.True<Article>();
                predicateSize = predicateSize.And(x => x.SearchableArticleAnimalSizeText.StartsWith(firstWord));
                predicateSize = terms.Skip(1).Aggregate(predicateSize, (current, term) => current.And(x => x.SearchableArticleAnimalSizeText.Contains(term)));

                predicate = predicate.Or(predicateHeading);
                predicate = predicate.Or(predicateTopics);
                predicate = predicate.Or(predicateLifestages);
                predicate = predicate.Or(predicateSize);
            }

            return predicate;
        }

        protected IQueryable<Article> FilterRelated(IQueryable<Article> query, IArticleTaxonomy item)
        {
            var predicate = PredicateBuilder.True<Article>();

            if (item.ArticleTopics != null && item.ArticleTopics.Any())
            {
                var p1 = PredicateBuilder.True<Article>();
                p1 = item.ArticleTopics.Aggregate(p1, (current, entry) => current.Or(x => x.ArticleTopicIds.Contains(entry.Id).Boost(BoostLevels.Primary)));

                predicate = predicate.And(p1);
            }

            if (item.Lifestages != null && item.Lifestages.Any())
            {
                var p1 = PredicateBuilder.True<Article>();
                p1 = item.Lifestages.Aggregate(p1, (current, entry) => current.Or(x => x.LifestageIds.Contains(entry.Id).Boost(BoostLevels.Secondary)));

                predicate = predicate.And(p1);
            }
            
            if (item.Sizes != null && item.Sizes.Any())
            {
                var p1 = PredicateBuilder.True<Article>();
                p1 = item.Sizes.Aggregate(p1, (current, entry) => current.Or(x => x.SizeIds.Contains(entry.Id).Boost(BoostLevels.Secondary)));

                predicate = predicate.And(p1);
            }

            if (item.Breeds != null && item.Breeds.Any())
            {
                var p1 = PredicateBuilder.True<Article>();
                p1 = item.Breeds.Aggregate(p1, (current, entry) => current.Or(x => x.BreedIds.Contains(entry.Id).Boost(BoostLevels.Secondary)));

                predicate = predicate.And(p1);
            }

            if (item.Species != null && item.Species.Any())
            {
                var p1 = PredicateBuilder.True<Article>();
                p1 = item.Species.Aggregate(p1, (current, entry) => current.Or(x => x.SpecieIds.Contains(entry.Id).Boost(BoostLevels.Secondary)));

                predicate = predicate.And(p1);
            }

            query = query.Where(predicate);

            return query;
        }

        private static Expression<Func<Article, bool>> BuildFilterGroupExpression(
            IDictionary<string, IEnumerable<Guid>> filterGroups)
        {
            var expression = PredicateBuilder.True<Article>();
            foreach (var filterGroup in filterGroups)
            {
                var currentExpression = PredicateBuilder.True<Article>();

                foreach (var filter in filterGroup.Value)
                {
                    var filterValue = filter;

                    currentExpression = currentExpression.Or(x => x.SearchableRelatedEntitiesIds.Contains(filterValue));
                }

                expression = expression.And(currentExpression);
            }

            return expression;
        }
    }
}
