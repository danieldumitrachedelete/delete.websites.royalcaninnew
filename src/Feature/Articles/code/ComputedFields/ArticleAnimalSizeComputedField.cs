﻿using Delete.Foundation.DataTemplates.Models.Taxonomy;

namespace Delete.Feature.Articles.ComputedFields
{
    public class ArticleAnimalSizeComputedField : BaseArticleComputedField
    {
        public ArticleAnimalSizeComputedField() : base(TaxonomyConstants.SizesFieldName)
        {
        }
    }
}