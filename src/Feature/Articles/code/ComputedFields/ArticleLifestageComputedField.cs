﻿using Delete.Foundation.DataTemplates.Models.Taxonomy;

namespace Delete.Feature.Articles.ComputedFields
{
    public class ArticleLifestageComputedField : BaseArticleComputedField
    {
        public ArticleLifestageComputedField() : base(TaxonomyConstants.LifestagesFieldName)
        {
        }
    }
}