﻿using Delete.Feature.Articles.Models.Taxonomy;

namespace Delete.Feature.Articles.ComputedFields
{
    public class ArticleTopicsComputedField : BaseArticleComputedField
    {
        public ArticleTopicsComputedField() : base(ArticleTaxonomyConstants.ArticleTopicsFieldName)
        {
        }
    }
}