﻿using System;
using Delete.Feature.Articles.Models;
using Delete.Foundation.DataTemplates.ComputedFields;
using Delete.Foundation.DeleteFoundationCore;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;
using Sitecore.ContentSearch.Diagnostics;

namespace Delete.Feature.Articles.ComputedFields
{
    public class BaseArticleComputedField : BaseDictionaryTextComputedField, IComputedIndexField
    {
        private readonly string _coupledFieldName;

        public BaseArticleComputedField(string coupledFieldName)
        {
            _coupledFieldName = coupledFieldName;
        }

        public object ComputeFieldValue(IIndexable indexable)
        {
            Guard.ArgumentNotNull(indexable, nameof(indexable));

            var indexedItem = GetItemHavingBaseTemplate(indexable, ArticleConstants.TemplateId);
            if (indexedItem == null)
            {
                return null;
            }

            try
            {
                return GetDictionaryText(indexedItem, _coupledFieldName);
            }
            catch (Exception e)
            {
                CrawlingLog.Log.Warn($"Unexpected error when computing {GetType()} index field value for item [ID:{indexedItem.ID}]. Error message: {e.Message}");

                return null;
            }
        }

        public string FieldName { get; set; }
        public string ReturnType { get; set; }
    }
}