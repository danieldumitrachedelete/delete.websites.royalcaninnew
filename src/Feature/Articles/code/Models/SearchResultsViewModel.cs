﻿using System.Collections.Generic;
using Delete.Foundation.DataTemplates.Models.Folder;
using Delete.Foundation.DataTemplates.Models.Taxonomy;

namespace Delete.Feature.Articles.Models
{
    public class SearchResultsViewModel
    {
        public int PageSize { get; set; }

        public FilterGroupFolder MainFilterGroup { get; set; }

        public Specie Specie { get; set; }

        public IEnumerable<SearchResultsGroupViewModel> SearchResults { get; set; }
    }
}