﻿using System;
using System.Collections.Generic;
using System.Linq;
using Delete.Foundation.DataTemplates.Models.Dictionary;
using Glass.Mapper.Sc.Configuration.Attributes;
using Sitecore.ContentSearch;

namespace Delete.Feature.Articles.Models.Taxonomy
{
    public partial class ArticleTaxonomy
    {
        protected IEnumerable<Guid> relatedEntities;

        public IEnumerable<Guid> RelatedEntities
        {
            get
            {
                if (this.relatedEntities != null)
                {
                    return this.relatedEntities;
                }

                var result = new List<Guid>();

                if (this.ArticleTopics != null && this.ArticleTopics.Any())
                {
                    result.AddRange(this.ArticleTopics.Select(x => x.Id));
                }

                if (this.Lifestages != null && this.Lifestages.Any())
                {
                    result.AddRange(this.Lifestages.Select(x => x.Id));
                }

                if (this.Sizes != null && this.Sizes.Any())
                {
                    result.AddRange(this.Sizes.Select(x => x.Id));
                }

                if (this.Breeds != null && this.Breeds.Any())
                {
                    result.AddRange(this.Breeds.Select(x => x.Id));
                }
                if (this.Species != null && this.Species.Any())
                {
                    result.AddRange(this.Species.Select(x => x.Id));
                }

                return this.relatedEntities = result;
            }
        }

        public IEnumerable<Guid> RelatedEntitiesFromIndex
        {
            get
            {
                var result = new List<Guid>();

                result.AddRange(this.ArticleTopicIds ?? Enumerable.Empty<Guid>());
                result.AddRange(this.LifestageIds ?? Enumerable.Empty<Guid>());
                result.AddRange(this.SizeIds ?? Enumerable.Empty<Guid>());

                return result;
            }
        }

        public string ArticleTopicString => ArticleTopics != null && ArticleTopics.Any() ? string.Join(", ", ArticleTopics.Select(x => x.Text)) : string.Empty;

        public IEnumerable<Guid> TaxonomyIds
        {
            get
            {
                return
                    (ArticleTopics ?? Enumerable.Empty<ArticleTopic>()).Select(x => x.Id)
                        .Union((Lifestages ?? Enumerable.Empty<DictionaryEntry>()).Select(x => x.Id))
                        .Union((Sizes ?? Enumerable.Empty<DictionaryEntry>()).Select(x => x.Id))
                        .Union((Breeds ?? Enumerable.Empty<DictionaryEntry>()).Select(x => x.Id))
                        .Union((Species ?? Enumerable.Empty<DictionaryEntry>()).Select(x => x.Id))
                        .Union((ContentTags ?? Enumerable.Empty<DictionaryEntry>()).Select(x => x.Id))
                        .Where(x => x != Guid.Empty )
                    ;
            }
        }

        [SitecoreField(FieldId = ArticleTaxonomyConstants.ArticleTopicsFieldId, FieldName = ArticleTaxonomyConstants.ArticleTopicsFieldName)]
        [IndexField(ArticleTaxonomyConstants.ArticleTopicsFieldName)]
        public virtual List<Guid> ArticleTopicIds { get; set; }
    }
}
