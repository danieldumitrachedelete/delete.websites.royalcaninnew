﻿using System;

namespace Delete.Feature.Articles.Models
{
    public class ArticlesFilterViewModel
    {
        public string SearchQuery { get; set; }

        public Guid Filter { get; set; }

        public string Sizes { get; set; }

        public string ArticleTopics { get; set; }

        public string Lifestages { get; set; }

        public bool ViewAll { get; set; }
    }
}