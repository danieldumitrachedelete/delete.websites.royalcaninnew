﻿using System;
using System.Collections.Generic;

namespace Delete.Feature.Articles.Models
{
    public class ArticleCarouselViewModel
    {
        public List<Article> Articles { get; set; }

        public Guid FilterConfiguration { get; set; }

        public string Header { get; set; }

        public bool GridView { get; set; }
    }
}
