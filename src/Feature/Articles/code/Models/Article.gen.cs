﻿// <autogenerated>
//   This file was generated by T4 code generator ScriptRunner.tt.
//   Any changes made to this file manually will be lost next time the file is regenerated.
// </autogenerated>

using System;
using Sitecore.Data;
using Sitecore.Common;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Sitecore.Globalization;
using System.ComponentModel;
using Delete.Foundation.DeleteFoundationCore.Models;
using Sitecore.ContentSearch;
using Delete.Feature.Articles;

namespace Delete.Feature.Articles.Models
{
	public partial interface IArticle : IGlassBase, Delete.Feature.Articles.Models.Taxonomy.IArticleTaxonomy, Delete.Foundation.DataTemplates.Models.Interfaces.I_PostItem
	{
	}

	public static partial class ArticleConstants {
		public const string TemplateIdString = "{c15806ea-dba5-46ad-8126-ac13363ee772}";
		public static readonly ID TemplateId = new ID(TemplateIdString);
		public const string TemplateName = "Article";

	
	
	}

	
	/// <summary>
	/// Article
	/// <para>Path: /sitecore/templates/Feature/Articles/Article</para>	
	/// <para>ID: {c15806ea-dba5-46ad-8126-ac13363ee772}</para>	
	/// </summary>
	[SitecoreType(TemplateId=ArticleConstants.TemplateIdString)] //, Cachable = true
	public partial class Article  : Delete.Feature.Articles.Models.Taxonomy.ArticleTaxonomy, IArticle
	{
	
	}
}
