﻿using System.Collections.Generic;
using Delete.Foundation.DataTemplates.Models.Filter;

namespace Delete.Feature.Articles.Models
{
    public class SearchResultsGroupViewModel
    {
        public FilterItem Key { get; set; }

        public IEnumerable<Article> Items { get; set; }

        public int ItemsMaxCount { get; set; }
    }
}