﻿using System;

namespace Delete.Feature.Articles.Models
{
    public class ArticlesFilterModel
    {
        public string SearchQuery { get; set; }

        public Guid Species { get; set; }

        public Guid? ListingRootId { get; set; }

        public string Sizes { get; set; }

        public string ArticleTopics { get; set; }

        public string Lifestages { get; set; }

        public ArticlesFilterModel(ArticlesFilterViewModel viewModel)
        {
            if (viewModel == null) return;

            SearchQuery = viewModel.SearchQuery;
            Sizes = viewModel.Sizes;
            ArticleTopics = viewModel.ArticleTopics;
            Lifestages = viewModel.Lifestages;
        }
    }
}