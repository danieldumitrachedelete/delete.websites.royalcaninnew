﻿namespace Delete.Feature.Articles.Models
{
    public class SearchViewModel
    {
        public SearchFilterViewModel FilterModel { get; set; }

        public SearchResultsViewModel SearchResultsModel { get;set; }
    }
}