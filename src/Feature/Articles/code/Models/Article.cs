﻿using System;
using System.Collections.Generic;
using System.Linq;
using Delete.Feature.Articles.Models.Taxonomy;
using Delete.Foundation.DataTemplates.Interfaces;
using Delete.Foundation.DataTemplates.Models.Taxonomy;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Models;
using Delete.Foundation.DeleteFoundationCore.SchemaOrg;
using Delete.Foundation.DeleteFoundationCore.SchemaOrg.Extensions;
using Delete.Foundation.DeleteFoundationCore.SchemaOrg.Interfaces;
using Glass.Mapper.Sc.Configuration.Attributes;
using Sitecore.ContentSearch;
using DictionaryEntry = Delete.Foundation.DataTemplates.Models.Dictionary.DictionaryEntry;

namespace Delete.Feature.Articles.Models
{
    using System.ComponentModel;
    using Sitecore.ContentSearch.Converters;

    public partial interface IArticle : IFilterRelatedEntities
    {
    }


    /// <summary>
    /// As there are no possibilities in Code Generation to inherit one base template and implement fields from another base template
    /// and because we have to inherit taxonomy class for searching
    /// we do this dirty hack in hope to fix Code Generator in some future
    /// </summary>
    public static partial class ArticleConstants
    {
        public const string DateFieldId = "{ee1c4d9f-5117-45f7-b0bd-fb6b12d19c95}";
        public const string HeadingFieldId = "{3e0d3222-fc81-4105-8496-f340ba2219b8}";
        public const string SummaryFieldId = "{dcbe0623-bbd4-4d7b-91c8-9339fe0400e6}";
        public const string ThumbnailImageFieldId = "{6fd15459-5a23-42fe-a352-6894c12fe253}";
        public const string ContentFieldId = "{4bfa1c61-4105-4f2b-ae99-7b0e91879c17}";
        public const string HeroImageFieldId = "{fe7aa554-e2a8-47be-8206-1fa76ea9503e}";

        public const string DateFieldName = "Date";
        public const string HeadingFieldName = "Heading";
        public const string SummaryFieldName = "Summary";
        public const string ThumbnailImageFieldName = "Thumbnail Image";
        public const string ContentFieldName = "Content";
        public const string HeroImageFieldName = "Hero Image";

    }

    public partial class Article : IMicrodata
    {
        [SitecoreField(FieldId = ArticleConstants.DateFieldId, FieldName = ArticleConstants.DateFieldName)]
        [IndexField(ArticleConstants.DateFieldName)]
        public virtual DateTime Date { get; set; }

        [SitecoreField(FieldId = ArticleConstants.HeadingFieldId, FieldName = ArticleConstants.HeadingFieldName)]
        [IndexField(ArticleConstants.HeadingFieldName)]
        public virtual string Heading { get; set; }

        [SitecoreField(FieldId = ArticleConstants.SummaryFieldId, FieldName = ArticleConstants.SummaryFieldName)]
        [IndexField(ArticleConstants.SummaryFieldName)]
        public virtual string Summary { get; set; }

        [SitecoreField(FieldId = ArticleConstants.ThumbnailImageFieldId, FieldName = ArticleConstants.ThumbnailImageFieldName)]
        [IndexField(ArticleConstants.ThumbnailImageFieldName)]
        public virtual ExtendedImage ThumbnailImage { get; set; }

        [SitecoreField(FieldId = ArticleConstants.ContentFieldId, FieldName = ArticleConstants.ContentFieldName, ReadOnly = true)]
        [IndexField(ArticleConstants.ContentFieldName)]
        public virtual string Content { get; set; }

        [SitecoreField(FieldId = ArticleConstants.HeroImageFieldId, FieldName = ArticleConstants.HeroImageFieldName)]
        [IndexField(ArticleConstants.HeroImageFieldName)]
        public virtual Delete.Foundation.DeleteFoundationCore.Models.ExtendedImage HeroImage { get; set; }

        [IndexField("articleTopics")]
        public virtual string SearchableArticleTopicsText { get; set; }

        [IndexField("articleLifestage")]
        public virtual string SearchableArticleLifestageText { get; set; }

        [IndexField("articleAnimalSize")]
        public virtual string SearchableArticleAnimalSizeText { get; set; }

        [IndexField("articleRelatedEntities")]
        public virtual List<Guid> SearchableRelatedEntitiesIds { get; set; }

        public IEnumerable<DictionaryEntry> TaxonomyTags => ArticleTopics.Union(Species).Union(Sizes).Where(x => x?.Text.NotEmpty() == true);
        
        public GlassBase GetListingRoot()
        {
            var parent = Parent;

            while (parent != null && parent.TemplateId == ArticleConstants.TemplateId)
            {
                parent = parent.Parent;
            }

            return parent?.TemplateId != ArticleConstants.TemplateId ? parent : Parent;
        }

        public Thing GetMicrodata(IAuthorResolver authorResolver)
        {
            return new NewsArticle
            {
                Headline = Heading ?? String.Empty,
                Name = Heading ?? String.Empty,
                Image = ThumbnailImage.GetMicrodata(authorResolver),
                DatePublished = Created.ToDateTimeOffset(),
                DateModified = Updated.ToDateTimeOffset(),
                Description = ArticleTopics == null ? string.Empty : string.Join(", ", ArticleTopics.Select(x => x.Text).Where(x => x.NotEmpty())),
                Url = new Uri(Url ?? String.Empty, UriKind.RelativeOrAbsolute),
                Author = authorResolver.GetAuthor(),
                Publisher = authorResolver.GetAuthor(),
                MainEntityOfPage = new WebPage
                {
                    Id = new Uri(Url ?? String.Empty, UriKind.RelativeOrAbsolute)
                }
            };
        }
    }
}
