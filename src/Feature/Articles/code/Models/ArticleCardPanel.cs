﻿using System.Collections.Generic;
using System.Linq;
using Delete.Foundation.DeleteFoundationCore.Extensions;

namespace Delete.Feature.Articles.Models
{
	public partial class ArticleCardPanel  
	{
	    public IEnumerable<Article> SpecifiedArticles
	    {
	        get
	        {
	            return
	                Articles
	                    .Where(x => x.IsDerivedFrom(ArticleConstants.TemplateId))
	                ;
	        }
	    }
	}
}
