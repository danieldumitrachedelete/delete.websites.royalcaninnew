﻿using System.Collections.Generic;
using System.Linq;
using Delete.Foundation.DeleteFoundationCore.Extensions;

namespace Delete.Feature.Articles.Models
{
	public partial class ArticleCarousel
    {
	    public IEnumerable<string> TaxonomyNames
	    {
	        get
	        {
	            return
	                ArticleTopics.Select(x => x.Text)
	                    .Union(Lifestages.Select( x => x.Text))
	                    .Union(Sizes.Select(x => x.Text))
	                    .Union(Breeds.Select(x => x.Text))
	                    .Union(Species.Select(x => x.Text))
	                    .Union(ContentTags.Select(x => x.Text))
	                    .Where(x => x.NotEmpty())
	                ;
	        }
	    }
	}
}
