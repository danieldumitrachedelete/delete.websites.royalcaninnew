﻿using System;
using System.Collections.Generic;

namespace Delete.Feature.Articles.Models
{
    public class FeaturedContentModel
    {
        public Article LeftItem { get; set; }
        public List<Article> RightItems { get; set; }

        public bool HasContent => LeftItem != null && RightItems.Count == 2;
    }
}