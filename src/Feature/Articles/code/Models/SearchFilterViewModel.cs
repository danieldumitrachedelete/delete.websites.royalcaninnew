﻿using System;
using System.Collections.Generic;
using Delete.Foundation.DataTemplates.Interfaces;
using Delete.Foundation.DataTemplates.Models.Folder;
using Delete.Foundation.DataTemplates.Models.Taxonomy;

namespace Delete.Feature.Articles.Models
{
    public class SearchFilterViewModel
    {
        public Specie Specie { get; set; }

        public Guid FilterId { get; set; }

        public IEnumerable<FilterGroupFolder> FilterGroups { get; set; }

        public IEnumerable<IFilterRelatedEntities> FilterRelatedSearchResults { get; set; }

        public Dictionary<Guid, IEnumerable<Guid>> PreFiltering { get; set; }
    }
}
