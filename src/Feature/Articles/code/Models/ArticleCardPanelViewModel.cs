﻿using System.Collections.Generic;

namespace Delete.Feature.Articles.Models
{
    public class ArticleCardPanelViewModel
    {
        public List<Article> Articles { get; set; }

        public string Header { get; set; }


        public Delete.Foundation.DataTemplates.Models.Dictionary.DictionaryEntry Spacers { get; set; }

        public Delete.Foundation.DataTemplates.Models.Dictionary.DictionaryEntry Width { get; set; }
    }
}
