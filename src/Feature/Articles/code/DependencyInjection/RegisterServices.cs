using Delete.Feature.Articles.Managers;
using Delete.Feature.Articles.Models;
using Delete.Foundation.InitializeDependencyInjection.Extensions;
using Microsoft.Extensions.DependencyInjection;
using Delete.Foundation.DeleteFoundationCore.Managers;
using Sitecore.DependencyInjection;

namespace Delete.Feature.Articles.DependencyInjection
{
    public class RegisterServices : IServicesConfigurator
    {
        public void Configure(IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IArticleSearchManager, ArticleSearchManager>();

            serviceCollection.AddMvcControllersInCurrentAssembly();

            RegisterIndexes();
        }

        private static void RegisterIndexes()
        {
            SearchManagersConfiguration.RegisterIndexConfiguration(typeof(Article), ArticlesConstants.CustomIndexAlias);
        }
    }
}
