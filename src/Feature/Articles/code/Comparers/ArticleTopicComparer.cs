﻿using System.Collections.Generic;
using Delete.Feature.Articles.Models.Taxonomy;

namespace Delete.Feature.Articles.Comparers
{
    public class ArticleTopicComparer : IEqualityComparer<ArticleTopic>
    {
        public bool Equals(ArticleTopic x, ArticleTopic y)
        {
            if (x == null || y == null) return false;

            return x.Id == y.Id;
        }

        public int GetHashCode(ArticleTopic obj)
        {
            return obj.Id.GetHashCode();
        }
    }
}