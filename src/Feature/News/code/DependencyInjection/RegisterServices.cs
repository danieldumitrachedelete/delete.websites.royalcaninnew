using Delete.Feature.News.Managers;
using Delete.Foundation.InitializeDependencyInjection.Extensions;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.DependencyInjection;

namespace Delete.Feature.News.DependencyInjection
{
    public class RegisterServices : IServicesConfigurator
    {
        public void Configure(IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<INewsSearchManager, NewsSearchManager>();
            serviceCollection.AddMvcControllersInCurrentAssembly();
        }
    }
}
