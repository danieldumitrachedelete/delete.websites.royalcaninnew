using System.Linq;
using AutoMapper;

namespace Delete.Feature.News.Profiles
{
    using Foundation.DataTemplates.Models;

    using JetBrains.Annotations;

    using Models;

    [UsedImplicitly]
    public class NewsProfile : Profile
    {
        public NewsProfile()
        {
            this.CreateMap<NewsItem, NewsItemViewModel>()
                .ForMember(x => x.Id, opt => opt.MapFrom(z => z.Id))
                .ForMember(x => x.Date, opt => opt.MapFrom(z => z.Date))
                .ForMember(x => x.Heading, opt => opt.MapFrom(z => z.Heading))
                .ForMember(x => x.Url, opt => opt.MapFrom(z => z.Url))
                .ForMember(x => x.Type, opt => opt.MapFrom(z => string.Join(", ", z.NewsTypes.Select(x => x.Text))));

            this.CreateMap<NewsItem, PredictiveSearchItemJsonModel>()
                .ForMember(x => x.Title, opt => opt.MapFrom(z => z.Heading))
                .ForMember(x => x.Url, opt => opt.MapFrom(z => z.Url))
                .ForAllOtherMembers(x => x.Ignore())
                ;
        }
    }
}
