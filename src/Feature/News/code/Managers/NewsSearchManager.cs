﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Delete.Feature.News.Models;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.DeleteFoundationCore.Managers;
using Sitecore.ContentSearch.Linq.Utilities;
using Sitecore.Data;

namespace Delete.Feature.News.Managers
{
    using Foundation.DeleteFoundationCore.ExpressionExtensions;

    public interface INewsSearchManager : IBaseSearchManager<NewsItem>
    {
        IEnumerable<NewsItem> GetAll(
            Guid? rootItemId = null,
            Guid? specieId = null, 
            string keyword = null, 
            IDictionary<string, IEnumerable<Guid>> filterIds = null);
    }

    public class NewsSearchManager : BaseSearchManager<NewsItem>, INewsSearchManager
    {
        public IEnumerable<NewsItem> GetAll(
            Guid? rootItemId = null, 
            Guid? specieId = null, 
            string keyword = null, 
            IDictionary<string, IEnumerable<Guid>> filterIds = null)
        {
            var filtered = this.GetQueryable();
            filtered = this.FilterByRootItem(filtered, rootItemId);
            filtered = this.FilterBySpecie(filtered, specieId);
            filtered = this.FilterByKeyword(filtered, keyword);
            filtered = this.FilterByRelatedEntities(filtered, filterIds);

            return filtered
                .MapResults(this.SitecoreContext, true)
                .OrderByDescending(x => x.Date)
                .ToList();
        }

        private IQueryable<NewsItem> FilterByRelatedEntities(
            IQueryable<NewsItem> query,
            IDictionary<string, IEnumerable<Guid>> filterIds)
        {
            if (filterIds == null || !filterIds.Any())
            {
                return query;
            }

            return query.Where(BuildFilterGroupExpression(filterIds));
        }

        private IQueryable<NewsItem> FilterByRootItem(IQueryable<NewsItem> query, Guid? rootItemId)
        {
            if (!rootItemId.HasValue)
            {
                return this.FilterLocal(query);
            }

            var rootItem = new ID(rootItemId.Value);
            return query.Where(x => x.Paths.Contains(rootItem));
        }

        private IQueryable<NewsItem> FilterBySpecie(IQueryable<NewsItem> query, Guid? specieId)
        {
            if (!specieId.HasValue)
            {
                return query;
            }

            return query.Where(x => x.SpecieIds.Contains(specieId.Value));
        }

        private IQueryable<NewsItem> FilterByKeyword(IQueryable<NewsItem> query, string keyword)
        {
            if (!keyword.NotEmpty())
            {
                return query;
            }

            var predicate = this.FilterByKeyword(keyword);
            return query.Where(predicate);
        }

        protected override IQueryable<NewsItem> GetQueryable()
        {
            return base.GetQueryable()
                .Where(x => x.TemplateId == NewsItemConstants.TemplateId
                    || x.BaseTemplates.Contains(NewsItemConstants.TemplateId.Guid));
        }

        private Expression<Func<NewsItem, bool>> FilterByKeyword(string keyword)
        {
            var predicate = PredicateBuilder.False<NewsItem>();

            predicate = PredicateBuilder.Or(
                    predicate,
                    x => x.Heading.StartsWith(keyword)
                         || x.SearchableNewsTypesText.StartsWith(keyword)
                         || x.SearchableNewsCategoryText.StartsWith(keyword)
                         || x.SearchableNewsLifestagesText.StartsWith(keyword)
                         || x.SearchableNewsAnimalSizeText.StartsWith(keyword));


            var terms = keyword.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            if(terms.Length > 1)
            {
                var firstWord = terms.FirstOrDefault();

                var predicateHeading = PredicateBuilder.True<NewsItem>();
                predicateHeading = predicateHeading.And(x => x.Heading.StartsWith(firstWord));
                predicateHeading = terms.Skip(1).Aggregate(predicateHeading, (current, term) => current.And(x => x.Heading.Contains(term)));

                var predicateType = PredicateBuilder.True<NewsItem>();
                predicateType = predicateType.And(x => x.SearchableNewsTypesText.StartsWith(firstWord));
                predicateType = terms.Skip(1).Aggregate(predicateType, (current, term) => current.And(x => x.SearchableNewsTypesText.Contains(term)));

                var predicateCategory = PredicateBuilder.True<NewsItem>();
                predicateCategory = predicateCategory.And(x => x.SearchableNewsCategoryText.StartsWith(firstWord));
                predicateCategory = terms.Skip(1).Aggregate(predicateCategory, (current, term) => current.And(x => x.SearchableNewsCategoryText.Contains(term)));
                
                var predicateLifestages = PredicateBuilder.True<NewsItem>();
                predicateLifestages = predicateLifestages.And(x => x.SearchableNewsLifestagesText.StartsWith(firstWord));
                predicateLifestages = terms.Skip(1).Aggregate(predicateLifestages, (current, term) => current.And(x => x.SearchableNewsLifestagesText.Contains(term)));

                var predicateSize = PredicateBuilder.True<NewsItem>();
                predicateSize = predicateSize.And(x => x.SearchableNewsAnimalSizeText.StartsWith(firstWord));
                predicateSize = terms.Skip(1).Aggregate(predicateSize, (current, term) => current.And(x => x.SearchableNewsAnimalSizeText.Contains(term)));


                predicate = predicate.Or(predicateHeading);
                predicate = predicate.Or(predicateType);
                predicate = predicate.Or(predicateCategory);
                predicate = predicate.Or(predicateLifestages);
                predicate = predicate.Or(predicateSize);
            }

            return predicate;
        }
        
        /// <summary>
        /// Search by grouped items. Do not use generic method because it throws exception in building solr query
        /// </summary>
        /// <param name="filterGroups"></param>
        /// <returns></returns>
        private static Expression<Func<NewsItem, bool>> BuildFilterGroupExpression(
            IDictionary<string, IEnumerable<Guid>> filterGroups)
        {
            var expression = PredicateBuilder.True<NewsItem>();
            foreach (var filterGroup in filterGroups)
            {
                var currentExpression = PredicateBuilder.True<NewsItem>();

                foreach (var filter in filterGroup.Value)
                {
                    var filterValue = filter;
                    currentExpression = currentExpression.Or(x => x.SearchableRelatedEntitiesIds.Contains(filterValue));
                }

                expression = expression.And(currentExpression);
            }

            return expression;
        }
    }
}