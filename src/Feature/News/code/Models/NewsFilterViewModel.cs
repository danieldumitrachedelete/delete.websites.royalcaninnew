﻿namespace Delete.Feature.News.Models
{
    using System;

    public class NewsFilterViewModel
    {
        public string SearchQuery {get; set; }

        public Guid Filter { get; set; }

        public string NewsCategories { get; set; }

        public string NewsTypes { get; set; }

        public int Page { get; set; }
    }
}