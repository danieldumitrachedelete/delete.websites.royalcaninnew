﻿using System;

namespace Delete.Feature.News.Models
{
    public class SearchViewModel
    {
        public Guid FilterConfiguration { get; set; }

        public SearchFilterViewModel FilterModel { get; set; }

        public SearchResultsViewModel SearchResultsModel { get;set; }
    }
}