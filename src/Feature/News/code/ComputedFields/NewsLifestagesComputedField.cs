﻿namespace Delete.Feature.News.ComputedFields
{
    using Foundation.DataTemplates.Models.Taxonomy;

    using JetBrains.Annotations;

    [UsedImplicitly]
    public class NewsLifestagesComputedField : BaseNewsComputedField
    {
        public NewsLifestagesComputedField() : base(TaxonomyConstants.LifestagesFieldName)
        {
        }
    }
}