﻿namespace Delete.Feature.News.ComputedFields
{
    using JetBrains.Annotations;

    using Models.Taxonomy;

    [UsedImplicitly]
    public class NewsCategoryComputedField : BaseNewsComputedField
    {
        public NewsCategoryComputedField() : base(NewsTaxonomyConstants.NewsCategoryFieldName)
        {
        }
    }
}