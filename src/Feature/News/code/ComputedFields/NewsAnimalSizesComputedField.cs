﻿namespace Delete.Feature.News.ComputedFields
{
    using Foundation.DataTemplates.Models.Taxonomy;

    using JetBrains.Annotations;

    [UsedImplicitly]
    public class NewsAnimalSizesComputedField : BaseNewsComputedField
    {
        public NewsAnimalSizesComputedField() : base(TaxonomyConstants.SizesFieldName)
        {
        }
    }
}