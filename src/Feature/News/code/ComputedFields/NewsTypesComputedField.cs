﻿namespace Delete.Feature.News.ComputedFields
{
    using JetBrains.Annotations;

    using Models.Taxonomy;

    [UsedImplicitly]
    public class NewsTypesComputedField : BaseNewsComputedField
    {
        public NewsTypesComputedField() : base(NewsTaxonomyConstants.NewsTypesFieldName)
        {
        }
    }
}