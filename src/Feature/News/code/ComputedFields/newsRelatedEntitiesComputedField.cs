﻿using System;
using System.Collections.Generic;
using Delete.Foundation.DeleteFoundationCore;
using Delete.Foundation.DeleteFoundationCore.ComputedFields;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using JetBrains.Annotations;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;
using Sitecore.ContentSearch.Diagnostics;

namespace Delete.Feature.News.ComputedFields
{
    using Foundation.DataTemplates.Models.Taxonomy;

    using Models;
    using Models.Taxonomy;

    [UsedImplicitly]
    public class NewsRelatedEntitiesComputedField : BaseComputedField, IComputedIndexField
    {
        public string FieldName { get; set; }

        public string ReturnType { get; set; }

        public object ComputeFieldValue(IIndexable indexable)
        {
            Guard.ArgumentNotNull(indexable, nameof(indexable));

            var indexedItem = this.GetItemHavingBaseTemplate(indexable, NewsItemConstants.TemplateId);
            if (indexedItem == null)
            {
                return null;
            }

            try
            {
                var result = new List<string>();
                result.AddRange(indexedItem.GetFieldValues(TaxonomyConstants.BreedsFieldName));
                result.AddRange(indexedItem.GetFieldValues(TaxonomyConstants.ContentTagsFieldName));
                result.AddRange(indexedItem.GetFieldValues(TaxonomyConstants.LifestagesFieldName));
                result.AddRange(indexedItem.GetFieldValues(TaxonomyConstants.SizesFieldName));
                result.AddRange(indexedItem.GetFieldValues(TaxonomyConstants.SpeciesFieldName));
                result.AddRange(indexedItem.GetFieldValues(NewsTaxonomyConstants.NewsCategoryFieldName));
                result.AddRange(indexedItem.GetFieldValues(NewsTaxonomyConstants.NewsTypesFieldName));
                return result;
            }
            catch (Exception e)
            {
                CrawlingLog.Log.Warn($"Unexpected error when computing {GetType()} index field value for item [ID:{indexedItem.ID}]. Error message: {e.Message}");

                return null;
            }
        }
    }
}