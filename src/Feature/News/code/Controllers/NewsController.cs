using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Delete.Feature.News.Managers;
using Delete.Feature.News.Models;
using Delete.Foundation.DataTemplates.Managers;
using Delete.Foundation.DeleteFoundationCore.Controllers;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.DeleteFoundationCore.Models;
using Delete.Foundation.MultiSite.Extensions;
using Glass.Mapper.Sc;

namespace Delete.Feature.News.Controllers
{
    using System;
    using System.Collections.Generic;

    using Foundation.DataTemplates.Models;
    using Foundation.DeleteFoundationCore.Extensions;

    public class NewsController : BaseController
    {
        private const int InitialPageIndex = 1;

        private const string NewsCacheKey = "News_Component_CacheParametersShouldBeAddedHere";
        private const string NewsFilterGroupsCacheKey = "newsfiltergroups";
        private const string AllNewsCacheKey = "allnews_{0}_{1}";
        private const string FilteredNewsCacheKey = "filterednews_{0}_{1}_{2}_{3}";
        private const string NewsByKeywordCacheKey = "newsbykeyword_{0}_{1}_{2}";
        private readonly INewsSearchManager newsSearchManager;

        private readonly IFilterGroupFolderSearchManager filterGroupFolderSearchManager;

        public NewsController(
            INewsSearchManager newsSearchManager,
            IFilterGroupFolderSearchManager filterGroupFolderSearchManager,
            ISitecoreContext sitecoreContext,
            IMapper mapper,
            ICacheManager cacheManager) : base(sitecoreContext, mapper, cacheManager)
        {
            this.newsSearchManager = newsSearchManager;
            this.filterGroupFolderSearchManager = filterGroupFolderSearchManager;
        }

        public ActionResult Search()
        {
            var filterConfiguration = this.GetDataSourceItem<NewsFilterConfiguration>();
            if (filterConfiguration == null)
            {
                return this.Content(Sitecore.Context.PageMode.IsExperienceEditorEditing ? "<p>Please select news filter configuration</p>" : string.Empty);
            }

            var species = filterConfiguration.Species;
            var selectedFilterGroups = filterConfiguration.Filters;
            var pageSize = filterConfiguration.PageSize;
            var contextItem = this.GetContextItem<GlassBase>();

            var allFilterGroups = this._cacheManager.CacheResults(() => this.filterGroupFolderSearchManager.GetAll(), NewsFilterGroupsCacheKey);
            var sortedFilterGroups = selectedFilterGroups
                .Select(x => x.Id)
                .Select(filterGroupId => allFilterGroups.FirstOrDefault(x => x.Id == filterGroupId))
                .Where(g => g != null)
                .ToList();

            var cacheKey = string.Format(AllNewsCacheKey, contextItem?.Id, species?.Id);
            var allNews =
                this._cacheManager.CacheResults(
                    () => this.newsSearchManager.GetAll(contextItem?.Id, species?.Id).ToList(), cacheKey);
            var resultNews = allNews.Take(pageSize);

            var marketSettings = this.SitecoreContext.GetLocalMarketSettings();

            var result = new SearchViewModel
            {
                FilterConfiguration = filterConfiguration.Id,
                FilterModel = new SearchFilterViewModel
                {
                    FilterGroups = sortedFilterGroups,
                    Specie = species
                },
                SearchResultsModel = new SearchResultsViewModel
                {
                    FilterConfiguration = filterConfiguration.Id,
                    Specie = species,
                    Results = this._mapper.Map<IEnumerable<NewsItemViewModel>>(resultNews),
                    Page = InitialPageIndex,
                    PageSize = pageSize,
                    TotalResults = allNews.Count,
                    DateFormat = marketSettings.NewsDateFormat
                }
            };

            return this.View("~/Views/Feature/News/Search.cshtml", result);
        }

        [HttpGet]
        public ActionResult Filter(NewsFilterViewModel filterModel)
        {
            var filterConfiguration = this.SitecoreContext.GetItem<NewsFilterConfiguration>(filterModel.Filter, false, true);
            var species = filterConfiguration.Species;
            var pageSize = filterConfiguration.PageSize;
            var contextItem = this.GetContextItem<GlassBase>();
            var currentPage = filterModel.Page > InitialPageIndex ? filterModel.Page : InitialPageIndex;

            var filterIds = this.GetRequestFilterIdsFromCheckboxes(filterModel);

            var cacheKey = string.Format(FilteredNewsCacheKey, contextItem?.Id, species?.Id, filterModel.SearchQuery, string.Join("|", filterIds.SelectMany(x => x.Value)));
            var allNews = this._cacheManager.CacheResults(
                () => this.newsSearchManager.GetAll(contextItem?.Id, species?.Id, filterModel.SearchQuery, filterIds)
                    .ToList(), cacheKey);
            var resultNews = allNews.Skip((currentPage - InitialPageIndex) * pageSize).Take(pageSize);

            var marketSettings = this.SitecoreContext.GetLocalMarketSettings();

            var result = new SearchResultsViewModel
            {
                FilterConfiguration = filterConfiguration.Id,
                Specie = species,
                Results = this._mapper.Map<IEnumerable<NewsItemViewModel>>(resultNews),
                Page = currentPage,
                PageSize = pageSize,
                TotalResults = allNews.Count,
				DateFormat = marketSettings.NewsDateFormat
		};

            return this.View("~/Views/Feature/News/_NewsSearchResults.cshtml", result);
        }

        public ActionResult Predictive(string keyword, Guid? species)
        {
            var contextItem = this.GetContextItem<GlassBase>();
            var cacheKey = string.Format(NewsByKeywordCacheKey, contextItem?.Id, species, keyword);
            var results =
                this._cacheManager.CacheResults(() => this.newsSearchManager.GetAll(contextItem?.Id, species, keyword),
                    cacheKey);
            var predictiveResults = this._mapper.Map<IEnumerable<PredictiveSearchItemJsonModel>>(results);
            var json = new PredictiveSearchJsonModel
            {
                Items = predictiveResults
            };

            return this.Json(json, JsonRequestBehavior.AllowGet);
        }

        private IDictionary<string, IEnumerable<Guid>> GetRequestFilterIdsFromCheckboxes(NewsFilterViewModel filterConfiguration)
        {
            var filterValues = new Dictionary<string, IEnumerable<Guid>>();

            var newsCategories = filterConfiguration.NewsCategories.ToGuidList();

            var newsTypes = filterConfiguration.NewsTypes.ToGuidList();

            if (newsCategories.Any())
            {
                filterValues.Add(nameof(filterConfiguration.NewsCategories), newsCategories);
            }

            if (newsTypes.Any())
            {
                filterValues.Add(nameof(filterConfiguration.NewsTypes), newsTypes);
            }

            return filterValues;
        }
    }
}
