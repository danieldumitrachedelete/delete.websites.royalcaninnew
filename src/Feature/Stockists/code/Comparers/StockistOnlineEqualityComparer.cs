﻿namespace Delete.Feature.Stockists.Comparers
{
    using System;
    using System.Collections.Generic;

    using Models;

    public class StockistOnlineEqualityComparer : IEqualityComparer<Stockist>
    {
        public bool Equals(Stockist x, Stockist y)
        {
            if (x == null || y == null)
            {
                return false;
            }

            return x.CustomerReference.Equals(y.CustomerReference, StringComparison.InvariantCultureIgnoreCase);
        }

        public int GetHashCode(Stockist obj)
        {
            return StringComparer.InvariantCultureIgnoreCase.GetHashCode(obj.CustomerReference);
        }
    }
}