﻿namespace Delete.Feature.Stockists.Models
{
    using System;

    public class StockistListingFilterViewModel
    {
        public Guid Filter { get; set; }

        public int Page { get; set; }
    }
}