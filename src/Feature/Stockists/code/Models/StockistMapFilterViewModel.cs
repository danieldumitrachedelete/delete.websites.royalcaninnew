﻿namespace Delete.Feature.Stockists.Models
{
    using System;
    using System.Collections.Generic;

    using Enums;

    public class StockistMapFilterViewModel : BoundingBoxCoordinates
    {
        private int? page;

        private IDictionary<string, string> filtersDictionary;

        public bool IsLocal { get; set; }

        public Guid Filter { get; set; }

        public string ContactTypes { get; set; }

        public string ProfileTypes { get; set; }

        public int Page
        {
            get => this.page ?? 1;
            set => this.page = value;
        }

        public string SearchQuery { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public double CurrentLatitude { get; set; }

        public double CurrentLongitude { get; set; }

        public StockistSorting SortResultsBy { get; set; }

        public IDictionary<string, string> FiltersDictionary
        {
            get
            {
                if (this.filtersDictionary != null)
                {
                    return this.filtersDictionary;
                }

                return this.filtersDictionary = new Dictionary<string, string>
                {
                    {nameof(this.SearchQuery), this.SearchQuery},
                    {nameof(this.Filter), this.Filter != Guid.Empty ? this.Filter.ToString("N") : string.Empty},
                    {nameof(this.ContactTypes), this.ContactTypes},
                    {nameof(this.ProfileTypes), this.ProfileTypes},
                    {nameof(this.Page), this.Page > 1 ? this.Page.ToString() : string.Empty},
                };
            }
        }
    }
}