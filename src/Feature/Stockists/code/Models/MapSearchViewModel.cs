﻿using System;

namespace Delete.Feature.Stockists.Models
{
    public class MapSearchViewModel
    {
        public Guid FilterConfiguration { get; set; }

        public MapFilterViewModel FilterModel { get; set; }

        public CoordinateViewModel UserLocationCoordinate { get; set; }

        public StockistsSearchResultViewModel SearchResultModel { get; set; }

        public string CurrentCountryCode { get; set; }
        
        public CoordinateViewModel MapDefaultCoordinate { get; set; }

        public decimal MapDefaultZoom { get; set; }
    }
}