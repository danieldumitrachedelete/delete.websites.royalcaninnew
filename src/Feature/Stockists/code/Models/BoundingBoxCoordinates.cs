﻿namespace Delete.Feature.Stockists.Models
{
    public class BoundingBoxCoordinates
    {
        public double? NeLatitude { get; set; }

        public double? NeLongitude { get; set; }

        public double? SwLatitude { get; set; }

        public double? SwLongitude { get; set; }

        public bool IsBoundBoxParametersValid => this.NeLatitude.HasValue
                                                 && this.NeLongitude.HasValue
                                                 && this.SwLatitude.HasValue
                                                 && this.SwLongitude.HasValue;
    }
}