﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Delete.Feature.Stockists.Models
{
    public class StockistSearchResults
    {
        public int TotalResults { get; set; }

        public IEnumerable<Stockist> CurrentResults { get; set; }
    }
}