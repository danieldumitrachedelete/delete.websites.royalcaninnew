﻿using System;
using System.Collections.Generic;
using System.Linq;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Models;
using Delete.Foundation.DeleteFoundationCore.SchemaOrg;
using Delete.Foundation.DeleteFoundationCore.SchemaOrg.Extensions;
using Delete.Foundation.DeleteFoundationCore.SchemaOrg.Interfaces;
using Delete.Foundation.MultiSite.Models;

namespace Delete.Feature.Stockists.Models
{
    public class StockistViewModel : IMicrodata
    {
        public string Id { get; set; }

        public string CompanyName { get; set; }

        public string Description { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string AddressLine3 { get; set; }

        public string FormattedAddress { get; set; }

        public string Address
        {
            get
            {
                return string.IsNullOrWhiteSpace(FormattedAddress)
                    ? string.Join(", ",
                        new[]
                        {
                            this.AddressLine1,
                            this.AddressLine2,
                            this.AddressLine3,
                            this.Postcode,
                            this.City
                        }.Where(x => x.NotEmpty()))
                    : FormattedAddress;
            }
        }

        public string StreetAddress
        {
            get
            {
                return string.Join(
                    ", ",
                    new[]
                    {
                        this.AddressLine1,
                        this.AddressLine2,
                        this.AddressLine3,
                        this.City
                    }.Where(x => x.NotEmpty()));
            }
        }

        public string Postcode { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        public string Phone { get; set; }

        public string WebsiteUrl { get; set; }

        public ExtendedImage Logo { get; set; }

        public string Distance { get; set; }

        public CoordinateViewModel Location { get; set; }

        public IList<string> Tags { get; set; }

        public IList<string> ContactTypes { get; set; }

        public string ProfileType { get; set; }

        public string OpeningHours { get; set; }

        public decimal Rating { get; set; }

        public Thing GetMicrodata(IAuthorResolver authorResolver = null)
        {
            return new PetStore
            {
                Name = this.CompanyName ?? string.Empty,
                Address = new PostalAddress
                {
                    PostalCode = this.Postcode ?? string.Empty,
                    StreetAddress = this.StreetAddress ?? string.Empty
                },
                Image = this.Logo.GetMicrodata(authorResolver),
                Telephone = this.Phone ?? string.Empty
            };
        }

        public string DataTrackCustomAction { get; set; }
        public string DataTrackCustomCategory { get; set; }
        public Boolean DataTrackCustomEnable { get; set; }
        public string DataTrackCustomEvent { get; set; }
        public string DataTrackCustomLabel { get; set; }
    }
}