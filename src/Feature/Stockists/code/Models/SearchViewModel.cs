﻿namespace Delete.Feature.Stockists.Models
{
    public class SearchViewModel
    {
        public string Title { get; set; }

        public MapSearchViewModel MapSearchModel { get; set; }

        public StockistsSearchResultViewModel ListingSearchModel { get; set; }
    }
}