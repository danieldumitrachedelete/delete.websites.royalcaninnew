﻿using System;
using Sitecore.Data;
using Sitecore.Common;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Sitecore.Globalization;
using System.ComponentModel;
using Delete.Foundation.DeleteFoundationCore.Models;
using Sitecore.ContentSearch;
using Delete.Feature.Stockists;

namespace Delete.Feature.Stockists.Models
{
	public partial interface IStockistResultConfiguration : IGlassBase
	{
    }

	public static partial class StockistResultConfigurationConstants
    {

        public const string ActionFieldId = "{5bfa5485-d06f-4db3-b07a-caa8d7c272a0}";
        public const string CategoryFieldId = "{c0cb3234-0b69-4906-bb8c-c8225773aafa}";
        public const string EnableFieldId = "{d81f0afd-44bd-4ee6-8f8f-ede6fb236cae}";
        public const string EventFieldId = "{ba46a5ba-8ae4-4a08-8fee-1926eb1103b3}";
        public const string LabelFieldId = "{da5a6645-1756-46eb-9dd6-235ef518ebc0}";

        public const string ActionFieldName = "Action";
        public const string CategoryFieldName = "Category";
        public const string EnableFieldName = "Enable";
        public const string EventFieldName = "Event";
        public const string LabelFieldName = "Label";
    }

    
	public partial class StockistResultConfiguration
	{
        /// <summary>
		/// The Action field.
		/// <para>Field Type: Single-Line Text</para>		
		/// <para>Field ID: {5bfa5485-d06f-4db3-b07a-caa8d7c272a0}</para>
		/// </summary>
		[SitecoreField(FieldId = StockistResultConfigurationConstants.ActionFieldId, FieldName = StockistResultConfigurationConstants.ActionFieldName)]
        [IndexField(StockistResultConfigurationConstants.ActionFieldName)]
        public virtual string Action { get; set; }

        /// <summary>
        /// The Category field.
        /// <para>Field Type: Single-Line Text</para>		
        /// <para>Field ID: {c0cb3234-0b69-4906-bb8c-c8225773aafa}</para>
        /// </summary>
        [SitecoreField(FieldId = StockistResultConfigurationConstants.CategoryFieldId, FieldName = StockistResultConfigurationConstants.CategoryFieldName)]
        [IndexField(StockistResultConfigurationConstants.CategoryFieldName)]
        public virtual string Category { get; set; }

        /// <summary>
        /// The Enable field.
        /// <para>Field Type: Checkbox</para>		
        /// <para>Field ID: {d81f0afd-44bd-4ee6-8f8f-ede6fb236cae}</para>
        /// </summary>
        [SitecoreField(FieldId = StockistResultConfigurationConstants.EnableFieldId, FieldName = StockistResultConfigurationConstants.EnableFieldName)]
        [IndexField(StockistResultConfigurationConstants.EnableFieldName)]
        public virtual Boolean Enable { get; set; }

        /// <summary>
        /// The Event field.
        /// <para>Field Type: Single-Line Text</para>		
        /// <para>Field ID: {ba46a5ba-8ae4-4a08-8fee-1926eb1103b3}</para>
        /// </summary>
        [SitecoreField(FieldId = StockistResultConfigurationConstants.EventFieldId, FieldName = StockistResultConfigurationConstants.EventFieldName)]
        [IndexField(StockistResultConfigurationConstants.EventFieldName)]
        public virtual string Event { get; set; }

        /// <summary>
        /// The Label field.
        /// <para>Field Type: Single-Line Text</para>		
        /// <para>Field ID: {da5a6645-1756-46eb-9dd6-235ef518ebc0}</para>
        /// </summary>
        [SitecoreField(FieldId = StockistResultConfigurationConstants.LabelFieldId, FieldName = StockistResultConfigurationConstants.LabelFieldName)]
        [IndexField(StockistResultConfigurationConstants.LabelFieldName)]
        public virtual string Label { get; set; }
    }
}
