﻿using System.Collections.Generic;
using Delete.Foundation.MultiSite.Models;
using Delete.Foundation.RoyalCaninCore.Models;

namespace Delete.Feature.Stockists.Models
{
    using System;

    public class StockistsSearchResultViewModel : PaginatedResults
    {
        public Guid Filter { get; set; }

        public IEnumerable<StockistViewModel> SearchResults { get;set; }
    }
}