﻿namespace Delete.Feature.Stockists.Models
{
    public class CoordinateViewModel
    {
        public double Lat { get; set; }

        public double Lng {get; set; }

        public override string ToString()
        {
            return $"{this.Lat.ToString(System.Globalization.CultureInfo.InvariantCulture)},{this.Lng.ToString(System.Globalization.CultureInfo.InvariantCulture)}";
        }
    }
}