﻿using System.Collections.Generic;
using Delete.Foundation.DataTemplates.Models.Folder;

namespace Delete.Feature.Stockists.Models
{
    using System;

    public class MapFilterViewModel
    {
        public IEnumerable<FilterGroupFolder> FilterGroups { get;set; }

        public IEnumerable<Guid> OnlineRetailerIds { get; set; }

        public StockistMapFilterViewModel FilterValue { get; set; }
    }
}