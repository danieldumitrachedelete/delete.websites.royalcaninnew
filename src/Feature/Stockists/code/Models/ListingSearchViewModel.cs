﻿using System;
using System.Collections.Generic;
using Delete.Foundation.RoyalCaninCore.Models;

namespace Delete.Feature.Stockists.Models
{
    public class ListingSearchViewModel : PaginatedResults
    {
        public Guid FilterConfiguration { get; set; }

        public IEnumerable<StockistViewModel> Results { get; set; }
    }
}