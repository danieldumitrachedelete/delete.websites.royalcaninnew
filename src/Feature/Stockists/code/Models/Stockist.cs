﻿using System;
using System.Collections;
using System.Collections.Generic;
using Delete.Foundation.MultiSite.Models;
using Sitecore.ContentSearch;

namespace Delete.Feature.Stockists.Models
{
    using Glass.Mapper.Sc.Configuration.Attributes;

    public partial class Stockist
    {
        public string Distance { get; set; }

        [IndexField("stockistquality")]
        public override int Quality { get; set; }
    }
}