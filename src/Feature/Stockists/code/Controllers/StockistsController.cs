using AutoMapper;
using Delete.Feature.Stockists.Enums;
using Delete.Feature.Stockists.Managers;
using Delete.Feature.Stockists.Models;
using Delete.Foundation.DataTemplates.Managers;
using Delete.Foundation.DataTemplates.Models.Folder;
using Delete.Foundation.DeleteFoundationCore;
using Delete.Foundation.DeleteFoundationCore.Controllers;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.MultiSite.Extensions;
using Delete.Foundation.MultiSite.Models;
using Glass.Mapper.Sc;
using Newtonsoft.Json;
using Sitecore.Analytics;
using Sitecore.ContentSearch.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Delete.Feature.Stockists.Controllers
{
    public partial class StockistsController : BaseController
    {
        private const int InitialPageIndex = 1;

        private readonly IContactTypeSearchManager contactTypeSearchManager;
        private readonly IStockistSearchManager stockistSearchManager;
        private readonly IFilterGroupFolderSearchManager filterGroupFolderSearchManager;

        public StockistsController(
            IContactTypeSearchManager contactTypeSearchManager,
            IStockistSearchManager stockistSearchManager,
            IFilterGroupFolderSearchManager filterGroupFolderSearchManager,
            ISitecoreContext sitecoreContext,
            IMapper mapper,
            ICacheManager cacheManager)
            : base(sitecoreContext, mapper, cacheManager)
        {
            this.contactTypeSearchManager = contactTypeSearchManager;
            this.stockistSearchManager = stockistSearchManager;
            this.filterGroupFolderSearchManager = filterGroupFolderSearchManager;
        }

        public ActionResult Search(StockistMapFilterViewModel mapFilterConfiguration)
        {
            var filterConfigurationItem = this.GetDataSourceItem<StockistResultConfiguration>();
            if (filterConfigurationItem == null)
            {
                return this.Content(Sitecore.Context.PageMode.IsExperienceEditorEditing ? "<p>Please select stockist filter configuration</p>" : null);
            }

            var rootItem = this.SitecoreContext.GetRootItemRelative<LocalMarketSettings>();
            var hideOnlineRetailers = rootItem != null && rootItem.StockistHideOnlineRetailers;
            var selectedFilterGroups = filterConfigurationItem.MapFilters;

            var allFilterGroups = this.filterGroupFolderSearchManager.GetAll();
            var sortedFilterGroups = selectedFilterGroups
                .Select(x => x.Id)
                .Select(filterGroupId => allFilterGroups.FirstOrDefault(x => x.Id == filterGroupId))
                .Where(g => g != null)
                .ToList();

            var onlineRetailersContactTypes = filterConfigurationItem.OnlineContactTypes.Select(x => x.Id).ToList();

            var notOnlineContactTypes = this.contactTypeSearchManager.GetAll()
                .Select(x => x.Id)
                .Where(x => !onlineRetailersContactTypes.Contains(x))
                .ToList();

            var filterIds = this.GetRequestFilterIdsFromCheckboxes(mapFilterConfiguration)
                .SelectMany(x => x.Value)
                .ToList();

            var listingTitle = string.Empty;

            if (!filterIds.Any())
            {
                filterIds = notOnlineContactTypes;
            }
            else
            {
                listingTitle = GetCustomisedPageTitle(mapFilterConfiguration);
            }

            var mapStockistResults = this.stockistSearchManager.GetAllLocal(filterIds , 1, filterConfigurationItem.MapPageSize, checkZeroCoordinates: true);

            var mapStockists = mapStockistResults.CurrentResults.OrderBy(x => x.CompanyName).ToList();

            var listingStockists = new List<Stockist>();
            var onlineRetailersCount = 0;
            if (!hideOnlineRetailers)
            {
                var listingStockistResults = this.stockistSearchManager.GetAllLocal(onlineRetailersContactTypes, 1, filterConfigurationItem.ListingPageSize, StockistSorting.Quality);
                onlineRetailersCount = listingStockistResults.TotalResults;
                listingStockists = listingStockistResults.CurrentResults.ToList();
            }

            var results = new SearchViewModel
            {
                Title = listingTitle,
                MapSearchModel = this.GetMapSearchViewModel(
                    sortedFilterGroups,
                    onlineRetailersContactTypes,
                    mapStockists,
                    filterConfigurationItem.Id,
                    InitialPageIndex,
                    filterConfigurationItem.MapPageSize,
                    mapStockistResults.TotalResults,
                    GetGoogleSpecificCountryCode(),
                    mapFilterConfiguration),
                ListingSearchModel = this.GetStockistsSearchResultViewModel(
                    listingStockists,
                    filterConfigurationItem.Id,
                    InitialPageIndex,
                    filterConfigurationItem.ListingPageSize,
                    onlineRetailersCount)
            };

            return this.View("~/Views/Feature/Stockists/Search.cshtml", results);
        }

        private string GetCustomisedPageTitle(StockistMapFilterViewModel mapFilterConfiguration)
        {
            var contactTypes = mapFilterConfiguration.ContactTypes.ToGuidList();
            if (contactTypes.Count == 1)
            {
                var selectedContact = this.contactTypeSearchManager.FindItem(contactTypes[0]);
                if (selectedContact != null)
                {
                    return selectedContact.OptionalListingTitle;
                }
            }

            return string.Empty;
        }

        [HttpGet]
        public ActionResult FindOnline(StockistListingFilterViewModel listingFilterConfiguration)
        {
            var filterConfigurationItem = this.SitecoreContext.GetItem<StockistResultConfiguration>(listingFilterConfiguration.Filter, false, true);

            var onlineRetailersContactTypes = filterConfigurationItem.OnlineContactTypes.Select(x => x.Id);
            var onlineStockistResults = this.stockistSearchManager.GetAllLocal(
                onlineRetailersContactTypes,
                listingFilterConfiguration.Page,
                filterConfigurationItem.ListingPageSize);

            var result = this.GetStockistsSearchResultViewModel(
                onlineStockistResults.CurrentResults.ToList(),
                filterConfigurationItem.Id,
                listingFilterConfiguration.Page,
                filterConfigurationItem.ListingPageSize,
                onlineStockistResults.TotalResults);

            return this.View("~/Views/Feature/Stockists/_Listing.cshtml", result);
        }

        [HttpGet]
        public ActionResult Filter(StockistMapFilterViewModel mapFilterConfiguration)
        {
            Guard.ArgumentNotNull(mapFilterConfiguration, nameof(mapFilterConfiguration));

            var filterConfigurationItem = this.SitecoreContext.GetItem<StockistResultConfiguration>(mapFilterConfiguration.Filter, false, true);
            var selectedFilterGroups = filterConfigurationItem.MapFilters;

            var allFilterGroups = this.filterGroupFolderSearchManager.GetAll();
            var sortedFilterGroups = selectedFilterGroups
                .Select(x => x.Id)
                .Select(filterGroupId => allFilterGroups.FirstOrDefault(x => x.Id == filterGroupId))
                .Where(g => g != null)
                .ToList();

            var filterIds = this.GetRequestFilterIdsFromCheckboxes(mapFilterConfiguration);
            var requestLocation = GetRequestLocation(mapFilterConfiguration);
            var currentLocation = GetCurrentLocation(mapFilterConfiguration);

            StockistSearchResults allStockists;
            if (mapFilterConfiguration.IsLocal)
            {
                allStockists = this.FindStockistsNearMe(mapFilterConfiguration, filterIds, filterConfigurationItem.MapPageSize);
            }
            else
            {
                allStockists = this.FindStockists(mapFilterConfiguration, filterIds, filterConfigurationItem.MapPageSize, currentLocation);
            }

            var onlineRetailersContactTypes = filterConfigurationItem.OnlineContactTypes.Select(x => x.Id).ToList();

            var currentResults = allStockists.CurrentResults.ToList();
            currentResults = ApplyDistanceToCurrentLocation(currentResults, mapFilterConfiguration).ToList();

            var results = this.GetMapSearchViewModel(
                sortedFilterGroups,
                onlineRetailersContactTypes,
                currentResults,
                filterConfigurationItem.Id,
                mapFilterConfiguration.Page,
                filterConfigurationItem.MapPageSize,
                allStockists.TotalResults,
                GetGoogleSpecificCountryCode(),
                mapFilterConfiguration,
                mapFilterConfiguration.Latitude,
                mapFilterConfiguration.Longitude);

            return this.View("~/Views/Feature/Stockists/_Map.cshtml", results);
        }

        private static Coordinate GetRequestLocation(StockistMapFilterViewModel mapFilterConfiguration)
        {
            return !mapFilterConfiguration.Latitude.Equals(0) && !mapFilterConfiguration.Longitude.Equals(0)
                       ? new Coordinate(mapFilterConfiguration.Latitude, mapFilterConfiguration.Longitude)
                       : null;
        }
        private static Coordinate GetCurrentLocation(StockistMapFilterViewModel mapFilterConfiguration)
        {
            return !mapFilterConfiguration.CurrentLatitude.Equals(0) && !mapFilterConfiguration.CurrentLongitude.Equals(0)
                ? new Coordinate(mapFilterConfiguration.CurrentLatitude, mapFilterConfiguration.CurrentLongitude)
                : GetContactCurrentLocation();
        }


        private IEnumerable<Stockist> ApplyDistanceToCurrentLocation(
            IList<Stockist> stockists,
            StockistMapFilterViewModel mapFilterConfiguration)
        {
            var currentLocation = GetCurrentLocation(mapFilterConfiguration);

            if (currentLocation == null)
            {
                return stockists;
            }

            var marketSettingsUnit = this.SitecoreContext.GetRootItemRelative<LocalMarketSettings>().DistanceUnitForMap;
            var globalConfigUnit = this.SitecoreContext.GetItem<GlobalConfigurationFolder>(GlobalConfigurationFolderConstants.GlobalConfigurationFolderItemId.Guid, true, true).DefaultDistanceMeasurementUnit;


            foreach (var stockist in stockists)
            {
                var stockistDistance = stockist.ItemLocation.DistanceTo(currentLocation);
                var stockistDistanceString = ((decimal)stockistDistance).ConvertToUnits(globalConfigUnit, marketSettingsUnit, showZeros: false);
                stockist.Distance = stockistDistanceString;
            }

            return stockists;
        }

        private static string GetGoogleSpecificLanguageCode()
        {
            var parts = Sitecore.Context.Language.Name.SafelySplit("-");
            return parts.FirstOrDefault()?.ToLower() ?? string.Empty;
        }

        private string GetGoogleSpecificCountryCode()
        {
            var cultures = this.SitecoreContext.GetRootItemRelative<LocalMarketSettings>().GooglePlacesCountryCode;
            if (!cultures.IsNullOrEmpty())
            {
                var splitedCultures = cultures.SafelySplit("|");
                var result = JsonConvert.SerializeObject(splitedCultures);

                return result;
            }

            var parts = Sitecore.Context.Language.Name.SafelySplit("-");
            var lang = parts.LastOrDefault()?.ToUpper() ?? string.Empty;
            return lang.IsNullOrEmpty() ? lang : JsonConvert.SerializeObject(new string[] {lang});
        }

        private static Coordinate GetContactCurrentLocation()
        {
            if (!Tracker.IsActive)
            {
                Tracker.StartTracking();
            }

            var tracker = Tracker.Current;
            var interactionData = tracker?.Interaction;

            if (interactionData != null && interactionData.HasGeoIpData
                && interactionData.GeoData?.Latitude != null
                && interactionData.GeoData?.Longitude != null)
            {
                return new Coordinate(interactionData.GeoData.Latitude.Value, interactionData.GeoData.Longitude.Value);
            }

            return null;
        }

        private MapSearchViewModel GetMapSearchViewModel(
            IList<FilterGroupFolder> filterGroups,
            IList<Guid> onlineRetailerIds,
            IList<Stockist> stockists,
            Guid filter,
            int page,
            int pageSize,
            int totalResults,
            string countryCode,
            StockistMapFilterViewModel filterValue,
            double? latitude = null,
            double? longitude = null)
        {
            var marketSettings = this.SitecoreContext.GetRootItemRelative<LocalMarketSettings>();

            return new MapSearchViewModel
            {
                FilterConfiguration = filter,
                FilterModel = new MapFilterViewModel
                {
                    FilterGroups = filterGroups,
                    OnlineRetailerIds = onlineRetailerIds,
                    FilterValue = filterValue,
                },
                UserLocationCoordinate = latitude.HasValue && longitude.HasValue
                    ? new CoordinateViewModel { Lat = latitude.Value, Lng = longitude.Value }
                    : null,
                SearchResultModel = this.GetStockistsSearchResultViewModel(stockists, filter, page, pageSize, totalResults),
                CurrentCountryCode = countryCode,
                MapDefaultCoordinate = (marketSettings.DefaultMapLatitude != 0 || marketSettings.DefaultMapLongitude != 0)
                    ? new CoordinateViewModel { Lat = (double)marketSettings.DefaultMapLatitude, Lng = (double)marketSettings.DefaultMapLongitude }
                    : null,
                MapDefaultZoom = marketSettings.DefaultMapZoom
            };
        }

        private StockistsSearchResultViewModel GetStockistsSearchResultViewModel(
            IList<Stockist> stockists,
            Guid filter,
            int page,
            int pageSize,
            int totalResults)
        {
            var result = new StockistsSearchResultViewModel()
            {
                Filter = filter,
                Page = page,
                PageSize = pageSize,
                TotalResults = totalResults
            };
            result.SearchResults = this._mapper.Map<IEnumerable<StockistViewModel>>(stockists);
            return result;
        }

        private StockistSearchResults FindStockistsNearMe(StockistMapFilterViewModel mapFilterConfiguration, IDictionary<string, IEnumerable<Guid>> filterIds, int pageSize)
        {
            Guard.ArgumentNotNull(mapFilterConfiguration, nameof(mapFilterConfiguration));

            if (!mapFilterConfiguration.IsLocal)
            {
                return new StockistSearchResults() { CurrentResults = new List<Stockist>() };
            }

            var foundAddress = new Coordinate(mapFilterConfiguration.CurrentLatitude, mapFilterConfiguration.CurrentLongitude);
            return this.stockistSearchManager.GetAllLocal(filterIds, foundAddress, mapFilterConfiguration.Page, pageSize, mapFilterConfiguration.SortResultsBy);
        }

        private StockistSearchResults FindStockists(
            StockistMapFilterViewModel mapFilterConfiguration,
            IDictionary<string, IEnumerable<Guid>> filterIds,
            int pageSize,
            Coordinate userCurrentLocation)
        {
            var requestLocation =
                (!mapFilterConfiguration.Latitude.Equals(0) &&
                 !mapFilterConfiguration.Longitude.Equals(0))
                    ? new Coordinate(mapFilterConfiguration.Latitude,
                        mapFilterConfiguration.Longitude)
                    : null;
            Coordinate coordinateNorthEast = null;
            Coordinate coordinateSouthWest = null;

            if (mapFilterConfiguration.NeLatitude.HasValue && mapFilterConfiguration.NeLongitude.HasValue)
            {
                coordinateNorthEast = new Coordinate(mapFilterConfiguration.NeLatitude.Value, mapFilterConfiguration.NeLongitude.Value);
            }

            if (mapFilterConfiguration.SwLatitude.HasValue && mapFilterConfiguration.SwLongitude.HasValue)
            {
                coordinateSouthWest = new Coordinate(mapFilterConfiguration.SwLatitude.Value, mapFilterConfiguration.SwLongitude.Value);
            }

            return this.stockistSearchManager.GetAllLocal(requestLocation, filterIds, coordinateNorthEast, coordinateSouthWest, mapFilterConfiguration.Page, pageSize, mapFilterConfiguration.SortResultsBy, userCurrentLocation);
        }

        private IDictionary<string, IEnumerable<Guid>> GetRequestFilterIdsFromCheckboxes(StockistMapFilterViewModel mapFilterConfiguration)
        {
            var filterValues = new Dictionary<string, IEnumerable<Guid>>();

            var contactTypes = mapFilterConfiguration.ContactTypes.ToGuidList();

            var profileTypes = mapFilterConfiguration.ProfileTypes.ToGuidList();

            filterValues.Add(nameof(mapFilterConfiguration.ContactTypes), contactTypes);
            filterValues.Add(nameof(mapFilterConfiguration.ProfileTypes), profileTypes);
            return filterValues;
        }
    }
}