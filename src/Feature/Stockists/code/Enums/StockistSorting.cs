﻿namespace Delete.Feature.Stockists.Enums
{
    public enum StockistSorting
    {
        Name,
        Distance,
        Rating,
        Quality
    }
}