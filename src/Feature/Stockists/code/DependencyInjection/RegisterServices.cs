using Delete.Feature.Stockists.Managers;
using Delete.Feature.Stockists.Models;
using Delete.Foundation.DeleteFoundationCore.Managers;
using Delete.Foundation.InitializeDependencyInjection.Extensions;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.DependencyInjection;

namespace Delete.Feature.Stockists.DependencyInjection
{
    public class RegisterServices : IServicesConfigurator
    {
        public void Configure(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IStockistSearchManager, StockistSearchManager>();

            serviceCollection.AddMvcControllersInCurrentAssembly();

            RegisterIndexes();
        }

        private static void RegisterIndexes()
        {
            SearchManagersConfiguration.RegisterIndexConfiguration(typeof(Stockist), StockistsConstants.CustomIndexAlias);
        }
    }
}
