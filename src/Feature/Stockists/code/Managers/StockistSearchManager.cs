﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Delete.Feature.Stockists.Models;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.DeleteFoundationCore.Managers;
using Sitecore.ContentSearch.Data;
using Sitecore.ContentSearch.Linq;

namespace Delete.Feature.Stockists.Managers
{
    using System.Linq.Expressions;

    using Enums;

    using Sitecore.ContentSearch.Linq.Utilities;

    public interface IStockistSearchManager : IBaseSearchManager<Stockist>
    {
        StockistSearchResults GetAllLocal(
            int page,
            int pageSize,
            StockistSorting sorting = StockistSorting.Name,
            Coordinate location = null,
            bool checkZeroCoordinates = false);

        StockistSearchResults GetAllLocal(
            IEnumerable<Guid> filterGroups,
            int page,
            int pageSize,
            StockistSorting sorting = StockistSorting.Name,
            Coordinate location = null,
            bool checkZeroCoordinates = false);

        StockistSearchResults GetAllLocal(
            Coordinate foundAddress,
            IDictionary<string, IEnumerable<Guid>> filterGroups,
            Coordinate northEastCoordinate,
            Coordinate southWestCoordinate,
            int page,
            int pageSize,
            StockistSorting sorting = StockistSorting.Name,
            Coordinate location = null,
            bool checkZeroCoordinates = false);

        StockistSearchResults GetAllLocal(
            IDictionary<string, IEnumerable<Guid>> filterGroups,
            Coordinate location,
            int page,
            int pageSize,
            StockistSorting sorting = StockistSorting.Name,
            bool checkZeroCoordinates = false);

        IEnumerable<Stockist> GetStockistsByPath(string path);
    }

    public class StockistSearchManager : BaseSearchManager<Stockist>, IStockistSearchManager
    {
        public StockistSearchResults GetAllLocal(
            int page,
            int pageSize,
            StockistSorting sorting,
            Coordinate location = null,
            bool checkZeroCoordinates = false)
        {
            var results = this.GetLocalQueryable(page, pageSize, sorting, location, checkZeroCoordinates).GetResults();
            return new StockistSearchResults
            {
                TotalResults = results.TotalSearchResults,
                CurrentResults = results.Hits.Select(x => x.Document)
                               .MapResults(this.SitecoreContext, true)
            };
        }

        public StockistSearchResults GetAllLocal(
            IEnumerable<Guid> filterGroups,
            int page,
            int pageSize,
            StockistSorting sorting,
            Coordinate location = null,
            bool checkZeroCoordinates = false)
        {
            var filters = new Dictionary<string, IEnumerable<Guid>> { { nameof(filterGroups), filterGroups } };
            var query = this.GetLocalQueryable(page, pageSize, sorting, location, checkZeroCoordinates);
            query = query.Where(StockistRelationExists(filters));

            var results = query.GetResults();

            var currentResult = results.Select(x => x.Document).OrderByDescending(x => x.Quality).ThenBy(x => x.NameString.ToUpper());

            return new StockistSearchResults
            {
                TotalResults = results.TotalSearchResults,
                CurrentResults = currentResult.MapResults(this.SitecoreContext, true)
            };
        }

        public StockistSearchResults GetAllLocal(
            Coordinate foundAddress,
            IDictionary<string, IEnumerable<Guid>> filterGroups,
            Coordinate northEastCoordinate,
            Coordinate southWestCoordinate,
            int page,
            int pageSize,
            StockistSorting sorting = StockistSorting.Name,
            Coordinate location = null,
            bool checkZeroCoordinates = false)
        {
            var queryResult = this.GetLocalQueryable(page, pageSize, sorting, location ?? foundAddress, checkZeroCoordinates);

            if (foundAddress != null)
            {
                queryResult = queryResult.WithinRadius(x => x.ItemLocation, foundAddress, 10);
            }

            if (northEastCoordinate != null && southWestCoordinate != null)
            {
                queryResult = queryResult
                    .Where(x => x.ItemLocation.Between(ToSolrString(southWestCoordinate), ToSolrString(northEastCoordinate), Inclusion.Both));
            }

            queryResult = queryResult.Where(x => x.Longitude != 0).Where(x => x.Latitude != 0);

            var results = queryResult.Where(StockistRelationExists(filterGroups))
                .GetResults();

            return new StockistSearchResults
            {
                TotalResults = results.TotalSearchResults,
                CurrentResults = results.Hits.Select(x => x.Document)
                               .MapResults(this.SitecoreContext, true)
            };
        }

        public StockistSearchResults GetAllLocal(
            IDictionary<string, IEnumerable<Guid>> filterGroups,
            Coordinate location,
            int page,
            int pageSize,
            StockistSorting sorting = StockistSorting.Name,
            bool checkZeroCoordinates = false)
        {
            var queryResult = this.GetLocalQueryable(page, pageSize, sorting, location, checkZeroCoordinates);

            if (location != null)
            {
                queryResult = queryResult.WithinRadius(x => x.ItemLocation, location, 10);
            }

            var results = queryResult.Where(StockistRelationExists(filterGroups))
                .GetResults();

            return new StockistSearchResults
            {
                TotalResults = results.TotalSearchResults,
                CurrentResults = results.Hits.Select(x => x.Document)
                               .MapResults(this.SitecoreContext, true)
            };
        }

        public IEnumerable<Stockist> GetStockistsByPath(string path)
        {
            return this.GetQueryable().Where(x => x.Fullpath.StartsWith(path)).ToList();
        }

        protected override IQueryable<Stockist> GetQueryable()
        {
            return base.GetQueryable().Where(x => x.TemplateId == StockistConstants.TemplateId);
        }

        private static Expression<Func<Stockist, bool>> StockistRelationExists(IDictionary<string, IEnumerable<Guid>> filterGroups)
        {
            return BuildFilterGroupExpression(filterGroups);
        }

        private static string ToSolrString(Coordinate coordinate)
        {
            return $"{coordinate.Latitude.ToString(CultureInfo.InvariantCulture)},{coordinate.Longitude.ToString(CultureInfo.InvariantCulture)}";
        }

        private static Expression<Func<Stockist, bool>> BuildFilterGroupExpression(
            IDictionary<string, IEnumerable<Guid>> filterGroups)
        {
            var expression = PredicateBuilder.True<Stockist>();
            foreach (var filterGroup in filterGroups)
            {
                var currentExpression = PredicateBuilder.True<Stockist>();

                foreach (var filter in filterGroup.Value)
                {
                    var filterValue = filter;
                    currentExpression = currentExpression.Or(x => x.SearchableRelatedEntities.Contains(filterValue));
                }

                expression = expression.And(currentExpression);
            }

            return expression;
        }

        private IQueryable<Stockist> GetLocalQueryable(int page, int pageSize, StockistSorting sorting = StockistSorting.Name, Coordinate center = null, bool checkZeroCoordinates = false)
        {
            var baseQuery = this.FilterLocal(this.GetQueryable());
            baseQuery = baseQuery.Where(x => x.CompanyName != string.Empty);
            if (checkZeroCoordinates)
            {
                baseQuery = baseQuery.Where(x => x.Latitude != default(decimal) && x.Longitude != default(decimal));
            }
            baseQuery = baseQuery.Page(page - 1, pageSize);

            switch (sorting)
            {
                case StockistSorting.Name:
                    baseQuery = baseQuery.OrderBy(x => x.NameString);
                    break;
                case StockistSorting.Distance:
                    if (center != null)
                    {
                        baseQuery = baseQuery.OrderByDistance(x => x.ItemLocation, center).ThenBy(x => x.NameString);
                    }

                    break;
                case StockistSorting.Quality:
                    baseQuery = baseQuery.OrderByDescending(x => x.Quality).ThenBy(x => x.NameString);
                    break;
                case StockistSorting.Rating:
                    baseQuery = baseQuery.OrderByDescending(x => x.AtmosphereRating).ThenBy(x => x.NameString);
                    break;
            }

            return baseQuery;
        }
    }
}