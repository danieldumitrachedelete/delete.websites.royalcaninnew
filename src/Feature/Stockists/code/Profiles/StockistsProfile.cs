using System.Linq;
using AutoMapper;
using Delete.Feature.Stockists.Models;
using Delete.Foundation.MultiSite.Models;
using Glass.Mapper.Sc;
using JetBrains.Annotations;
using Sitecore.ContentSearch.Data;
using Sitecore.Mvc.Extensions;

namespace Delete.Feature.Stockists.Profiles
{
    using System.Globalization;

    using Delete.Foundation.DeleteFoundationCore.Extensions;

    using Foundation.DataTemplates.Models;

    using Sitecore.Collections;

    [UsedImplicitly]
    public class StockistsProfile : Profile
    {
        public StockistsProfile()
        {
            this.CreateMap<Coordinate, CoordinateViewModel>()
                .ForMember(x => x.Lat, opt => opt.MapFrom(z => z.Latitude))
                .ForMember(x => x.Lng, opt => opt.MapFrom(z => z.Longitude))
                .ForAllOtherMembers(x => x.Ignore());

            this.CreateMap<Stockist, StockistViewModel>()
                .ForMember(x => x.Id, opt => opt.MapFrom(z => z.Id))
                .ForMember(x => x.CompanyName, opt => opt.MapFrom(z => z.CompanyName))
                .ForMember(x => x.Description, opt => opt.MapFrom(z => z.Description))
                .ForMember(x => x.Phone, opt => opt.MapFrom(z => z.ContactFormattedPhoneNumber.OrIfEmpty(z.Phone)))
                .ForMember(x => x.City, opt => opt.MapFrom(z => z.City))
                .ForMember(x => x.Country, opt => opt.MapFrom(z => z.Country))
                .ForMember(x => x.AddressLine1, opt => opt.MapFrom(z => z.AddressLine1))
                .ForMember(x => x.AddressLine2, opt => opt.MapFrom(z => z.AddressLine2))
                .ForMember(x => x.AddressLine3, opt => opt.MapFrom(z => z.AddressLine3))
                .ForMember(x => x.FormattedAddress, opt => opt.MapFrom(z => z.BasicFormattedAddress))
                .ForMember(x => x.OpeningHours, opt => opt.MapFrom(z => z.ContactOpeningHours))
                .ForMember(x => x.Rating, opt => opt.MapFrom(z => z.AtmosphereRating))
                .ForMember(x => x.Postcode, opt => opt.MapFrom(z => z.Postcode))
                .ForMember(x => x.Logo, opt => opt.MapFrom(z => z.Logo))
                .ForMember(x => x.WebsiteUrl, opt => opt.MapFrom(z => z.WebsiteURL.BuildUrl(new SafeDictionary<string>())))
                .ForMember(x => x.Tags, opt => opt.MapFrom(z => z.ContactTypes.Select(x => x.Text)))
                .ForMember(x => x.ContactTypes, opt => opt.MapFrom(z => z.ContactTypes.Select(x => x.CustomerType.OrDefault(x.FilterLabel))))
                .AfterMap(
                      (src, dest) =>
                      {
                          if (src.ProfileType != null)
                          {
                              dest.Tags.Add(src.ProfileType.Text);
                              dest.ProfileType = src.ProfileType.Text;
                          }
                      })
                .ForMember(x => x.Location, opt => opt.MapFrom(z => z.ItemLocation))
                .ForMember(x => x.Distance, opt => opt.MapFrom(z => z.Distance))
                .ForAllOtherMembers(x => x.Ignore())
                ;
        }
    }
}