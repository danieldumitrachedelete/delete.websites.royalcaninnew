﻿using System;
using System.Collections.Generic;
using Delete.Feature.Stockists.Models;
using Delete.Foundation.DataTemplates.Models.Interfaces;
using Delete.Foundation.DeleteFoundationCore;
using Delete.Foundation.DeleteFoundationCore.ComputedFields;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using JetBrains.Annotations;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;
using Sitecore.ContentSearch.Diagnostics;

namespace Delete.Feature.Stockists.ComputedFields
{
    using System.Linq;

    [UsedImplicitly]
    public class StockistQualityComputedField : BaseComputedField, IComputedIndexField
    {
        public string FieldName { get; set; }

        public string ReturnType { get; set; }

        public object ComputeFieldValue(IIndexable indexable)
        {
            Guard.ArgumentNotNull(indexable, nameof(indexable));

            var indexedItem = this.GetItem(indexable, StockistConstants.TemplateId);
            if (indexedItem == null)
            {
                return null;
            }

            try
            {
                var profileTypes = indexedItem.GetFieldValues(_StockistConstants.ProfileTypeFieldName);
                return profileTypes.Any() ? 1 : 0;
            }
            catch (Exception e)
            {
                CrawlingLog.Log.Warn($"Unexpected error when computing {GetType()} index field value for item [ID:{indexedItem.ID}]. Error message: {e.Message}");

                return null;
            }
        }
    }
}