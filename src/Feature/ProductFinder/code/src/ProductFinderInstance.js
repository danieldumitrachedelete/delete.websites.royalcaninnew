import './styles.scss';
import React from 'react';
import ReactDOM from 'react-dom';
import BaseComponent from 'Foundation/src/js/BaseComponent';
import ProductFinderComponent from './ProductFinder';

export default class ProductFinder extends BaseComponent {
    constructor(el) {
        super(el);
        this.el = el;
        this.init();
        this.isInited = true;
    }

    static get tagName() {
        return 'product-finder';
    }

    setVariables() {}

    addListeners() {}

    init() {
        ReactDOM.render(<ProductFinderComponent />, this.el);
    }
}
