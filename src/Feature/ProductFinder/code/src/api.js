import axios from 'axios';

const instance = axios.create({
    timeout: 30000
});

instance.interceptors.response.use(response => response, error => Promise.reject(error));

export default instance;
