import React from 'react';

class Preloader extends React.Component {
    constructor(props) {
        super(props);
        this.enablePreloader = this.enablePreloader;

        this.state = {
            displayPreloader: false
        };

        this.timer = setTimeout(this.enablePreloader, 1000);
    }

    componentWillUnmount() {
        clearTimeout(this.timer);
    }

    enablePreloader = () => {
        this.setState({ displayPreloader: true });
    };

    render() {
        const { displayPreloader } = this.state;

        if (!displayPreloader) {
            return null;
        }

        return (
            <div className="rc-fixed-center rc-shade--white is-active">
                <div className="rc-loader-infinite" data-js-progress>
                    <object
                        className="rc-loader__logo logo-svg logo--crown"
                        width="150"
                        height="100"
                        data="https://d3moonnr9fkxfg.cloudfront.net/logo--crown.svg?v=8-6-2"
                        type="image/svg+xml"
                    >
                        <img
                            src="https://d3moonnr9fkxfg.cloudfront.net/1x1.gif?v=8-6-2"
                            style={{
                                backgroundImage:
                                    'url(https://d3moonnr9fkxfg.cloudfront.net/logo--crown.png?v=8-6-2)'
                            }}
                            width="150"
                            height="100"
                            alt="Royal Caninn logo"
                        />
                    </object>
                    <div className="rc-loader__spinner" />
                    <div className="rc-loader__background" />
                </div>
            </div>
        );
    }
}

export default Preloader;
