import React from 'react';
import 'formdata-polyfill';
import TooltipControl from './controls/TooltipControl';
import RadioButtonControl from './controls/RadioButtonControl';
import ProgressBar from './ProgressBar';
import Recommendation from './Recommendation';
import * as R from 'ramda';
import { PosedParent, SlideIn, FadeIn } from './animators';
import TranslationsContext from './TranslationsContext';
import Loadable from 'react-loadable';
import PropTypes from 'prop-types';

const LoadableRangeControl = Loadable({
    loader: () => import('./controls/RangeControl'),
    loading: () => null
});

const LoadableSelectBreed = Loadable({
    loader: () => import('./SelectBreed'),
    loading: () => null
});

export default class Step extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            initialFormData: null,
            formData: null,
            isChanged: false
        };
    }

    componentDidMount() {
        this.form = React.createRef();
        this.setState({
            initialFormData: new FormData(this.form.current)
        });
    }

    getParams = (param) => {
        const { stepData } = this.props;
        const params = Object.keys(stepData.nextStepParams[stepData.value].params)[0];
        return {
            [params]: param
        };
    };

    handleChange = (event) => {
        const changedFormData = new FormData(event.target.form);

        let obj1 = {};
        const obj2 = {};

        if (!R.isEmpty(this.state.initialFormData)) {
            this.state.initialFormData.forEach((value, key) => {
                obj1[key] = value;
            });
        } else {
            obj1 = {};
        }

        changedFormData.forEach((value, key) => {
            obj2[key] = value;
        });

        this.setState({
            isChanged: !R.equals(obj1, obj2)
        });
    };

    handleSubmit = (event) => {
        const { onNextStepClick, stepData } = this.props;
        const formData = new FormData(this.form.current);
        formData.forEach((val, key) => {
            if (key === stepData.value && val !== '') {
                onNextStepClick(stepData.nextStepParams[stepData.value].href, this.getParams(val));
            }
        });
        event.preventDefault();
    };

    componentDidUpdate(prevProps) {
        if (prevProps.step !== this.props.step) {
            this.setState({
                initialFormData: new FormData(this.form.current),
                formData: new FormData(this.form.current),
                isChanged: false
            });
        }
    }

    renderControls(stepData, culture, specie, handleImageChange) {
        if (stepData.reasonsList) {
            return Object.keys(stepData.reasonsList).map((item, i) => (
                <RadioButtonControl
                    key={`${item}_${i}`}
                    name={stepData.value}
                    label={stepData.reasonsList[item]}
                    value={item}
                />
            ));
        } else if (stepData.sizeList) {
            return stepData.sizeList.map((item, i) => (
                <RadioButtonControl
                    key={`${item}_${i}`}
                    name={stepData.value}
                    label={item.label}
                    onChange={handleImageChange}
                    value={item.value}
                />
            ));
        } else if (stepData.sensitivitiesList) {
            return Object.keys(stepData.sensitivitiesList).map((item, i) => (
                <RadioButtonControl
                    key={`${item}_${i}`}
                    name={stepData.value}
                    label={stepData.sensitivitiesList[item]}
                    value={item}
                />
            ));
        } else if (stepData.value === 'SELECT_AGE') {
            return <LoadableRangeControl name={stepData.value} options={stepData.ageList} />;
        } else if (stepData.value === 'SELECT_BREED') {
            return (
                <LoadableSelectBreed
                    culture={culture}
                    specie={specie}
                    stepData={stepData}
                    handleChange={this.handleChange}
                />
            );
        } else if (stepData.lifestylesList) {
            return Object.keys(stepData.lifestylesList).map((item, i) => (
                <RadioButtonControl
                    key={`${item}_${i}`}
                    name={stepData.value}
                    label={stepData.lifestylesList[item]}
                    value={item}
                />
            ));
        } else if (stepData.value === 'SELECT_STERELIZED') {
            return (
                <React.Fragment>
                    <RadioButtonControl name={stepData.value} label={'Yes'} value={'YES'} />
                    <RadioButtonControl name={stepData.value} label={'No'} value={'NO'} />
                </React.Fragment>
            );
        }
        return null;
    }

    render() {
        const {
            stepData, handleImageChange, progress, culture, specie, playIn
        } = this.props;

        return (
            <React.Fragment>
                {stepData && stepData.question_type !== 'RECOMMENDATION' ? (
                    <React.Fragment>
                        <div className="rc-layout-container rc-single-column js-step-contents">
                            <div className="rc-column">
                                <FadeIn pose={playIn ? 'enter' : 'exit'}>
                                    <h1 className="rc-alpha rc-text--center">
                                        {stepData.labels && stepData.labels[stepData.value]}
                                    </h1>
                                </FadeIn>
                                <div className="rc-content-block">
                                    <div className="rc-content-block__wrapper">
                                        <div className="rc-layout-container rc-two-column">
                                            <div className="rc-column rc-text--right">
                                                <FadeIn pose={playIn ? 'enter' : 'exit'}>
                                                    {stepData.image && (
                                                        <img
                                                            width={stepData.image.width}
                                                            height={stepData.image.height}
                                                            src={stepData.image.src}
                                                        />
                                                    )}
                                                </FadeIn>
                                            </div>

                                            <div className="rc-column">
                                                <form
                                                    ref={this.form}
                                                    onChange={this.handleChange}
                                                    onSubmit={this.handleSubmit}
                                                >
                                                    <div
                                                        style={{
                                                            minWidth: '300px'
                                                        }}
                                                    >
                                                        <PosedParent
                                                            pose={playIn ? 'enter' : 'exit'}
                                                        >
                                                            <SlideIn>
                                                                {this.renderControls(
                                                                    stepData,
                                                                    culture,
                                                                    specie,
                                                                    handleImageChange
                                                                )}
                                                            </SlideIn>
                                                            <SlideIn key={'button_1'}>
                                                                <TranslationsContext.Consumer>
                                                                    {translations => (
                                                                        <p>
                                                                            <button
                                                                                className="rc-btn rc-btn--one"
                                                                                type="submit"
                                                                                disabled={
                                                                                    !this.state
                                                                                        .isChanged &&
                                                                                    stepData.value !==
                                                                                        'SELECT_AGE'
                                                                                }
                                                                            >
                                                                                {
                                                                                    translations.nextButtonText
                                                                                }
                                                                            </button>
                                                                        </p>
                                                                    )}
                                                                </TranslationsContext.Consumer>
                                                            </SlideIn>
                                                            {stepData.labels &&
                                                                stepData.labels.TOOLTIP && (
                                                                <SlideIn key={'tooltip_1'}>
                                                                    <TooltipControl
                                                                        title={
                                                                            stepData.labels
                                                                                .TOOLTIP_TITLE
                                                                        }
                                                                        content={
                                                                            stepData.labels
                                                                                .TOOLTIP
                                                                        }
                                                                    />
                                                                </SlideIn>
                                                            )}
                                                        </PosedParent>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <FadeIn pose={playIn ? 'enter' : 'exit'}>
                                    <ProgressBar progress={progress} />
                                </FadeIn>
                            </div>
                        </div>
                    </React.Fragment>
                ) : (
                    <React.Fragment>
                        {stepData.products && <Recommendation playIn={playIn} />}
                    </React.Fragment>
                )}
            </React.Fragment>
        );
    }
}

Step.propTypes = {
    stepData: PropTypes.object.isRequired,
    onNextStepClick: PropTypes.func.isRequired,
    progress: PropTypes.number.isRequired,
    culture: PropTypes.object.isRequired,
    specie: PropTypes.string.isRequired,
    handleImageChange: PropTypes.func.isRequired,
    step: PropTypes.number,
    playIn: PropTypes.bool
};
