import React from 'react';
import { PosedParent, SlideIn, FadeIn } from './animators';
import PropTypes from 'prop-types';

export default class SplashScreen extends React.Component {
    render() {
        const { onNextStepClick, stepData, playIn } = this.props;
        return (
            <React.Fragment>
                {stepData ? (
                    <React.Fragment>
                        <PosedParent pose={playIn ? 'enter' : 'exit'}>
                            <div className="rc-content-block rc-bg-colour--brand3">
                                <div className="rc-content-block__wrapper">
                                    <div className="rc-layout-container rc-two-column rc-stacked">
                                        <div className="rc-bg-video__wrapper">
                                            <FadeIn pose={playIn ? 'enter' : 'exit'}>
                                                <video
                                                    className="rc-center-align"
                                                    poster=""
                                                    playsInline
                                                    autoPlay
                                                    muted
                                                >
                                                    <source
                                                        src="/-/media/default-website/homepage-video.mp4"
                                                        type="video/mp4"
                                                    />
                                                </video>
                                            </FadeIn>
                                        </div>
                                        <div className="rc-column">
                                            <div className="rc-text--center">
                                                <SlideIn>
                                                    <h1 className="rc-alpha">
                                                        {stepData.labels.HELP}
                                                    </h1>
                                                </SlideIn>
                                                <SlideIn>
                                                    <h3 className="rc-delta">
                                                        {stepData.labels.FIND}
                                                    </h3>
                                                </SlideIn>
                                                <SlideIn>
                                                    <p>{stepData.labels.SELECT_PET}</p>
                                                </SlideIn>
                                                <SlideIn>
                                                    <div className="rc-btn-group">
                                                        <button
                                                            className="rc-btn rc-btn--one"
                                                            type="button"
                                                            onClick={() =>
                                                                onNextStepClick(
                                                                    stepData.nextStepParams.CAT
                                                                        .href,
                                                                    {},
                                                                    'Cat'
                                                                )
                                                            }
                                                        >
                                                            {stepData.labels.CAT}
                                                        </button>
                                                        <button
                                                            className="rc-btn rc-btn--one"
                                                            type="button"
                                                            onClick={() =>
                                                                onNextStepClick(
                                                                    stepData.nextStepParams.DOG
                                                                        .href,
                                                                    {},
                                                                    'Dog'
                                                                )
                                                            }
                                                        >
                                                            {stepData.labels.DOG}
                                                        </button>
                                                    </div>
                                                </SlideIn>
                                            </div>
                                        </div>
                                        <div className="rc-column rc-active rc-bg-placeholder-16-9" />
                                    </div>
                                </div>
                            </div>
                        </PosedParent>
                    </React.Fragment>
                ) : null}
            </React.Fragment>
        );
    }
}

SplashScreen.propTypes = {
    onNextStepClick: PropTypes.func.isRequired,
    stepData: PropTypes.object,
    playIn: PropTypes.bool
};
