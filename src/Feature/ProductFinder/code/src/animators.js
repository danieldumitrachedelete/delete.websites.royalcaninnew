import posed from 'react-pose';
import { easing, tween } from 'popmotion';

const PosedParent = posed.div({
    enter: {
        staggerChildren: 300
    },
    exit: {
        staggerChildren: 250,
        staggerDirection: -1
    }
});

const Reveal = posed.div({
    enter: {
        opacity: 1,
        y: 0,
        transition: props =>
            tween({
                ...props,
                duration: 400,
                ease: easing.easeOut
            })
    },
    exit: {
        opacity: 0,
        y: 100,
        transition: props =>
            tween({
                ...props,
                duration: 200,
                ease: easing.easeOut
            })
    }
});

const SlideIn = posed.div({
    enter: {
        opacity: 1,
        y: 0,
        transition: props =>
            tween({
                ...props,
                duration: 200,
                ease: easing.easeOut
            })
    },
    exit: {
        opacity: 0,
        y: 80,
        transition: props =>
            tween({
                ...props,
                duration: 200,
                ease: easing.easeOut
            })
    }
});

const FadeIn = posed.div({
    enter: { opacity: 1, delay: 500 },
    exit: { opacity: 0 }
});

module.exports = {
    PosedParent,
    SlideIn,
    FadeIn,
    Reveal
};
