// Features

import ProductFinder from './ProductFinderInstance';

// BaseCore
import BaseCore from 'Foundation/src/js/BaseCore';

const baseCore = new BaseCore();

baseCore.componentClasses = [ProductFinder];
baseCore.init();
