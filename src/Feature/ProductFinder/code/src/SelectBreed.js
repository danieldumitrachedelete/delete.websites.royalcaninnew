import React from 'react';
import InputControl from './controls/InputControl';
import CheckboxControl from './controls/CheckboxControl';
import * as R from 'ramda';
import Downshift from 'downshift';
import Highlighter from 'react-highlight-words';
import ProductFinderService from './ProductFinderService';
import PropTypes from 'prop-types';

export default class SelectBreed extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            breeds: []
        };
    }
    componentDidMount() {
        this.fetchBreeds(
            '/api/v1/product-finder/get-breeds',
            Object.assign({}, this.props.culture, { specie: this.props.specie.toUpperCase() })
        );
    }
    fetchBreeds(url, data, method) {
        ProductFinderService.fetchData(url, data, method)
            .then((result) => {
                this.setState({
                    breeds: result.data
                });
            })
            .catch(() => {});
    }
    resetSiblings = (event) => {
        const { handleChange } = this.props;
        const el = event.target;
        const form = el.form;
        const excluded = n => n === el || (n.type === 'hidden' && el.type === 'text');

        const elements = R.reject(excluded, [...form.elements]);

        elements.forEach((element, i) => {
            const fieldType = elements[i].type.toLowerCase();

            switch (fieldType) {
                case 'text':
                case 'password':
                case 'textarea':
                case 'hidden':
                    this.downshift.clearSelection();
                    elements[i].value = '';
                    break;

                case 'radio':
                case 'checkbox':
                    if (elements[i].checked) {
                        elements[i].checked = false;
                    }
                    break;

                case 'select-one':
                case 'select-multi':
                    elements[i].selectedIndex = -1;
                    break;

                default:
                    break;
            }
        });
        handleChange(event);
    };
    itemToString = item => (item ? item.name : '');
    render() {
        const { stepData } = this.props;
        const { breeds } = this.state;
        return (
            <React.Fragment>
                <Downshift
                    itemToString={this.itemToString}
                    ref={(downshift) => {
                        this.downshift = downshift;
                    }}
                    render={({
                        getInputProps,
                        getItemProps,
                        isOpen,
                        inputValue,
                        highlightedIndex,
                        selectedItem
                    }) => (
                        <div className="rc-select rc-select--search rc-select-processed">
                            <div className="choices">
                                <input
                                    type="hidden"
                                    name={stepData.value}
                                    value={(selectedItem && selectedItem.code) || ''}
                                />
                                <InputControl
                                    name={stepData.value}
                                    getInputProps={getInputProps}
                                    inputValue={inputValue}
                                    onFocus={this.resetSiblings}
                                    value={'Choose your breed'}
                                />
                                {isOpen ? (
                                    <div className="choices__list choices__list--dropdown is-active">
                                        <div className="choices__list" dir="ltr" role="listbox">
                                            {breeds &&
                                                breeds
                                                    .filter(i =>
                                                        !inputValue ||
                                                            i.name
                                                                .toLowerCase()
                                                                .includes(inputValue.toLowerCase()))
                                                    .map((item, index) => (
                                                        <div
                                                            key={`${item}_${index}`}
                                                            className={`choices__item choices__item--choice choices__item--selectable ${
                                                                highlightedIndex === index
                                                                    ? 'is-highlighted'
                                                                    : ''
                                                            }`}
                                                            {...getItemProps({
                                                                key: item.code,
                                                                index,
                                                                item
                                                            })}
                                                        >
                                                            <Highlighter
                                                                highlightTag="span"
                                                                highlightClassName="highlighted"
                                                                searchWords={[inputValue]}
                                                                autoEscape={true}
                                                                textToHighlight={item.name || ''}
                                                            />
                                                        </div>
                                                    ))}
                                        </div>
                                    </div>
                                ) : null}
                            </div>
                        </div>
                    )}
                />
                <CheckboxControl
                    name={stepData.value}
                    label={stepData.labels.UNKNOWN_MIXED}
                    onChange={this.resetSiblings}
                    value={'UNKNOWN'}
                />
            </React.Fragment>
        );
    }
}

SelectBreed.propTypes = {
    stepData: PropTypes.object.isRequired,
    specie: PropTypes.string.isRequired,
    culture: PropTypes.object.isRequired,
    handleChange: PropTypes.func.isRequired
};
