import React from 'react';
import PropTypes from 'prop-types';

export default function ProgressBar(props) {
    const { progress } = props;
    return (
        <div
            className="rc-progress rc-progress--a"
            data-js-progress=""
            data-js-min="0"
            data-js-max="100"
            data-js-value={progress}
            data-js-step="1"
        >
            <input type="text" id="slider-result" />
        </div>
    );
}

ProgressBar.propTypes = {
    progress: PropTypes.number.isRequired
};
