import api from './api';

const PRODUCT_FINDER_API_DOMAIN = 'https://prd-eus2-rc-productfinder-webservice.mars.com';

class ProductFinderService {
    fetchData(path, data, local = false, method = 'POST') {
        if (local) {
            return api({ method, url: path, data });
        }
        return api({ method, url: `${PRODUCT_FINDER_API_DOMAIN}${path}`, data });
    }
}

const instance = new ProductFinderService();
export default instance;
