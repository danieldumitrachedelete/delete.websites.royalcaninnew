import React from 'react';
import Product from './Product';
import { capitalize, notEmpty } from './utils';
import * as R from 'ramda';
import { PosedParent, SlideIn, FadeIn } from './animators';
import StepContext from './StepContext';
import TranslationsContext from './TranslationsContext';
import PropTypes from 'prop-types';

export default function Recommendation(props) {
    const { playIn } = props;

    return (
        <TranslationsContext.Consumer>
            {translations => (
                <StepContext.Consumer>
                    {({ labels, products }) => (
                        <div className="rc-layout-container rc-single-column js-step-contents">
                            <div className="rc-column">
                                <PosedParent pose={playIn ? 'enter' : 'exit'}>
                                    <SlideIn>
                                        <h1 className="rc-alpha rc-text--center">
                                            {labels && labels.PRODUCTS}
                                        </h1>
                                    </SlideIn>

                                    {products &&
                                    (!R.isEmpty(R.filter(notEmpty, products.DRY)) ||
                                        !R.isEmpty(R.filter(notEmpty, products.WET))) ? (
                                            <React.Fragment>
                                                <SlideIn>
                                                    <div className="rc-fade--x" data-toggle-group>
                                                        {!R.isEmpty(products.DRY) &&
                                                    !R.isEmpty(products.WET) ? (
                                                                <ul
                                                                    className="rc-scroll--x rc-list rc-list--inline rc-list--align rc-list--blank rc-tab--centered"
                                                                    role="tablist"
                                                                >
                                                                    {Object.keys(products).map((item, i) => (
                                                                        <li key={`tab_${item}_${i}`}>
                                                                            <button
                                                                                className="rc-tab rc-btn"
                                                                                data-toggle={`tab__panel-${i}`}
                                                                                role="tab"
                                                                            >
                                                                                {capitalize(item)}
                                                                            </button>
                                                                        </li>
                                                                    ))}
                                                                </ul>
                                                            ) : null}
                                                    </div>
                                                    {Object.keys(products).map((item, i) => (
                                                        <React.Fragment key={`tabpanel_${item}_${i}`}>
                                                            {!R.isEmpty(products[item]) ? (
                                                                <div id={`tab__panel-${i}`}>
                                                                    <div className="rc-layout-container rc-one-column">
                                                                        {products[item]
                                                                            .slice(0, 1)
                                                                            .map(product => (
                                                                                <div
                                                                                    className="rc-column"
                                                                                    key={
                                                                                        product.MainItemId
                                                                                    }
                                                                                >
                                                                                    <Product
                                                                                        product={
                                                                                            product
                                                                                        }
                                                                                    />
                                                                                </div>
                                                                            ))}
                                                                    </div>
                                                                </div>
                                                            ) : null}
                                                        </React.Fragment>
                                                    ))}
                                                </SlideIn>

                                                {(R.length(products.DRY) > 1 ||
                                                R.length(products.WET) > 1) && (
                                                    <React.Fragment>
                                                        <SlideIn className="rc-padding-y--sm">
                                                            <h3 className="rc-beta rc-text--center">
                                                                {translations.weRecommend}
                                                            </h3>
                                                            <div className="rc-layout-container rc-one-column">
                                                                {Object.keys(products).map((item, i) => (
                                                                    <React.Fragment key={i}>
                                                                        {products[item]
                                                                            .slice(1)
                                                                            .slice(0, 1)
                                                                            .map(product => (
                                                                                <div
                                                                                    className="rc-column"
                                                                                    key={
                                                                                        product.MainItemId
                                                                                    }
                                                                                >
                                                                                    <Product
                                                                                        product={
                                                                                            product
                                                                                        }
                                                                                    />
                                                                                </div>
                                                                            ))}
                                                                    </React.Fragment>
                                                                ))}
                                                            </div>
                                                            <div className="rc-layout-container rc-three-column">
                                                                {Object.keys(products).map((item, i) => (
                                                                    <React.Fragment key={i}>
                                                                        {products[item]
                                                                            .slice(2)
                                                                            .map(product => (
                                                                                <div
                                                                                    className="rc-column rc-column--two"
                                                                                    key={
                                                                                        product.MainItemId
                                                                                    }
                                                                                >
                                                                                    <Product
                                                                                        product={
                                                                                            product
                                                                                        }
                                                                                    />
                                                                                </div>
                                                                            ))}
                                                                    </React.Fragment>
                                                                ))}
                                                            </div>
                                                        </SlideIn>
                                                    </React.Fragment>
                                                )}
                                            </React.Fragment>
                                        ) : (
                                            <SlideIn>
                                                <p className="rc-text--center">
                                                    {translations.noResults}
                                                </p>
                                            </SlideIn>
                                        )}

                                    <SlideIn>
                                        <div className="rc-content-block">
                                            <div className="rc-content-block__wrapper">
                                                <div className="rc-layout-container rc-two-column">
                                                    <div className="rc-column">
                                                        <FadeIn>
                                                            <img
                                                                src="http://developer.royalcanin.com/assets/images/img-b-w.png"
                                                                width="500"
                                                                height="385"
                                                            />
                                                        </FadeIn>
                                                    </div>
                                                    <div className="rc-column">
                                                        <div className="rc-text--center">
                                                            <h1 className="rc-alpha">
                                                                {translations.anotherRecommendation}
                                                            </h1>
                                                            <div className="rc-btn-group rc-padding-y--xs">
                                                                <a
                                                                    className="rc-btn rc-btn--two"
                                                                    href={window.location.href}
                                                                >
                                                                    {translations.startAgain}
                                                                </a>
                                                                <a
                                                                    className="rc-btn rc-btn--one"
                                                                    href="#"
                                                                >
                                                                    {translations.contactUs}
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </SlideIn>
                                </PosedParent>
                            </div>
                        </div>
                    )}
                </StepContext.Consumer>
            )}
        </TranslationsContext.Consumer>
    );
}

Recommendation.propTypes = {
    playIn: PropTypes.bool
};
