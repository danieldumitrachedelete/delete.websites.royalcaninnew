import React from 'react';
import PropTypes from 'prop-types';

export default function TooltipControl(props) {
    const { title, content } = props;

    return (
        <React.Fragment>
            <a
                className="rc-styled-link"
                data-tooltip="top-tooltip"
                data-tooltip-placement="top"
                title="Top"
            >
                {title}
            </a>
            <div id="top-tooltip" className="rc-tooltip">
                {content}
            </div>
        </React.Fragment>
    );
}

TooltipControl.propTypes = {
    title: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired
};
