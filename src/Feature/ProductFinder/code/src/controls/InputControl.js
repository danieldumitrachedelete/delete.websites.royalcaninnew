import React from 'react';
import PropTypes from 'prop-types';

export default function InputControl(props) {
    const {
        name, value, onFocus, getInputProps
    } = props;
    return (
        <span className="rc-input">
            <input
                {...getInputProps()}
                onFocus={onFocus}
                className="rc-input__control"
                name={`${name}_input`}
                id={name}
                type="text"
            />
            <label className="rc-input__label" htmlFor={name}>
                <span className="rc-input__label-text">{value || ''}</span>
            </label>
        </span>
    );
}

InputControl.propTypes = {
    name: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    onFocus: PropTypes.func.isRequired,
    getInputProps: PropTypes.func.isRequired
};
