import React from 'react';
import { capitalize } from '../utils';
import PropTypes from 'prop-types';

export default function RadioButtonControl(props) {
    const {
        name, label, value, onChange
    } = props;
    return (
        <div className="rc-input rc-input--stacked">
            <input
                className="rc-input__radio"
                id={`${name}_${value}`}
                type="radio"
                onChange={onChange || null}
                name={name}
                value={value}
            />
            <label className="rc-input__label--inline" htmlFor={`${name}_${value}`}>
                {capitalize(label)}
            </label>
        </div>
    );
}

RadioButtonControl.propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func
};
