import React from 'react';
import PropTypes from 'prop-types';

export default function CheckboxControl(props) {
    const {
        name, value, onChange, label
    } = props;
    return (
        <div className="rc-input rc-input--stacked">
            <input
                className="rc-input__checkbox"
                onChange={onChange}
                id={`${name}_1`}
                name={name}
                type="checkbox"
                value={value || ''}
            />
            <label className="rc-input__label--inline" htmlFor={`${name}_1`}>
                {label}
            </label>
        </div>
    );
}

CheckboxControl.propTypes = {
    name: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired
};
