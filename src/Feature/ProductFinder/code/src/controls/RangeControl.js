import React from 'react';
import Nouislider from 'nouislider-react';
import * as R from 'ramda';
import PropTypes from 'prop-types';

export default class RangeControl extends React.Component {
    constructor(props) {
        super(props);
        this.rangeInput = React.createRef();
    }
    onChangeSlide = (value) => {
        this.rangeInput.current.value = value;
    };

    shouldComponentUpdate() {
        return false;
    }

    render() {
        const { name, options } = this.props;

        const step = 100 / (options.length - 1);

        const values = {};

        options.forEach((option, index) => {
            if (index + 1 === options.length) return;
            values[step * index] = option.value;
        });

        return (
            <React.Fragment>
                <input ref={this.rangeInput} name={name} type="hidden" value="1.00" />
                <Nouislider
                    className="rc-slider"
                    range={Object.assign({}, { min: options[0].value }, values, {
                        max: options.slice(-1)[0].value
                    })}
                    start={[1]}
                    snap={true}
                    connect={[true, false]}
                    tooltips={[
                        {
                            to: value => R.filter(R.propEq('value', value))(options)[0].label
                        }
                    ]}
                    onSlide={this.onChangeSlide}
                />
            </React.Fragment>
        );
    }
}

RangeControl.propTypes = {
    name: PropTypes.string.isRequired,
    options: PropTypes.array.isRequired
};
