import * as R from 'ramda';

const capitalize = R.compose(R.replace(/^./, R.toUpper), R.toLower);
const notEmpty = R.compose(R.not, R.isEmpty);
const renameKeys = R.curry((keysMap, obj) =>
    R.reduce((acc, key) => R.assoc(keysMap[key] || key, obj[key], acc), {}, R.keys(obj)));

module.exports = {
    capitalize,
    notEmpty,
    renameKeys
};
