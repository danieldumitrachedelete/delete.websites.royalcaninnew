import React from 'react';
import * as R from 'ramda';
import SplashScreen from './SplashScreen';
import Step from './Step';
import { Reveal } from './animators';
import { renameKeys } from './utils';
import ReactResizeDetector from 'react-resize-detector';
import StepContext from './StepContext';
import GlobalContext from './GlobalContext';
import TranslationsContext from './TranslationsContext';
import ProductFinderService from './ProductFinderService';
import Preloader from './Preloader';

export default class ProductFinder extends React.Component {
    constructor(props) {
        super(props);

        this.initialConfig = window.productFinderSettings;
        this.container = React.createRef();
        this.state = {
            currentStepData: null,
            currentStep: -1,
            isLoading: false,
            translations: this.initialConfig.translations,
            culture: {
                country: this.initialConfig.countryCode,
                language: this.initialConfig.languageCode
            },
            productsEndPoint: this.initialConfig.productDetailsUrl,
            defaultProductImage: this.initialConfig.defaultProductImage,
            species: this.initialConfig.species,
            progress: 0,
            playIn: false,
            height: 0
        };
    }

    componentDidMount() {
        this.getNextStep('/api/v1/product-finder', this.state.culture);
    }

    getNextStepParams(data) {
        if (!this.state.currentStep) return data;
        return R.omit(['self', 'previous'], data);
    }

    mixStepData(data, specie) {
        const currentStepKey = Object.keys(data)[0];
        const availableSteps = R.filter(R.propEq('name', specie))(this.initialConfig.categories)[0]
            .steps;
        const stepKey = R.filter(R.propEq('value', currentStepKey))(availableSteps)[0];

        this.setState({
            availableStepsCount: availableSteps.length,
            specie
        });

        return stepKey;
    }

    getPrevStep = () => {
        this.setState(({ currentStep }) => ({
            currentStep: currentStep - 1
        }));
    };

    getNextStep = (url, data, specie = this.state.specie) => {
        if (this.state.isLoading) return;

        this.setState({
            isLoading: true,
            playIn: false
        });

        if ('requestIdleCallback' in window) {
            requestIdleCallback(() => this.fetchData(url, data, specie), { timeout: 1500 });
        } else {
            this.fetchData(url, data, specie);
        }
    };

    fetchData(url, data, specie) {
        ProductFinderService.fetchData(url, data)
            .then((result) => {
                const paramsObj = this.getNextStepParams(result.data._links);
                let mixedStepData = {};

                if (paramsObj.vetProducts) {
                    window.location = paramsObj.vetProducts.href;
                    return;
                }

                if (specie) {
                    mixedStepData = this.mixStepData(paramsObj, specie);
                }

                const payload = (currentStep, availableStepsCount) => ({
                    currentStepData: Object.assign({}, result.data, mixedStepData, {
                        nextStepParams: paramsObj
                    }),
                    currentStep: currentStep + 1,
                    progress: Math.round((currentStep + 1) / availableStepsCount * 100),
                    isLoading: false
                });

                if (result.data.products) {
                    const products = result.data.products;
                    const species = this.state.species;
                    const ids = {};

                    Object.keys(products).forEach((type) => {
                        products[type].forEach((product, index) => {
                            products[type][index] = renameKeys({
                                main_item_id: 'MainItemId',
                                commercial_label: 'ProductName'
                            })(product);
                        });
                    });

                    Object.keys(products).forEach((type) => {
                        ids[type] = R.pluck('MainItemId')(products[type]);
                    });

                    const productsData = { species, ids };
                    ProductFinderService.fetchData(
                        this.state.productsEndPoint,
                        productsData,
                        true,
                        'GET'
                    ).then((results) => {
                        const cmsProducts = results.data;
                        const combinedProducts = R.mergeWith(R.concat, cmsProducts, products);

                        Object.keys(combinedProducts).forEach((type) => {
                            combinedProducts[type] = R.uniqWith(R.eqBy(R.prop('MainItemId')))(combinedProducts[type]);
                        });

                        this.setState(({ currentStep, availableStepsCount }) =>
                            R.mergeDeepRight(payload(currentStep, availableStepsCount), {
                                currentStepData: {
                                    products: combinedProducts
                                }
                            }));
                    });
                } else {
                    this.setState(({ currentStep, availableStepsCount }) =>
                        payload(currentStep, availableStepsCount));
                }
            })
            .catch((error) => {
                throw new Error(error);
            });
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.currentStep !== this.state.currentStep) {
            setTimeout(() => {
                this.setState({
                    playIn: true
                });
            }, 250);
        }
    }

    onResize = (width, height) => {
        if ('requestIdleCallback' in window) {
            requestIdleCallback(() => this.applyHeight(height), { timeout: 1500 });
        } else {
            this.applyHeight(height);
        }
    };

    applyHeight(height) {
        this.setState({
            height
        });
    }

    handleImageChange = (e) => {
        const { currentStepData } = this.state;

        if (!R.isEmpty(currentStepData.stepValues)) {
            const newImage = R.filter(R.propEq('value', e.target.value))(currentStepData.stepValues)[0].image;
            this.setState((state) => {
                const imageLens = R.lensPath(['currentStepData', 'image']);
                return R.set(imageLens, newImage, state);
            });
        }
    };

    render() {
        const {
            currentStepData,
            progress,
            specie,
            currentStep,
            culture,
            playIn,
            translations,
            defaultProductImage
        } = this.state;

        return (
            <React.Fragment>
                <Reveal pose={playIn ? 'enter' : 'exit'}>
                    <div
                        className="product-finder__container"
                        ref={this.container}
                        style={{ height: `${this.state.height}px` }}
                    >
                        <GlobalContext.Provider value={{ defaultProductImage }}>
                            <StepContext.Provider value={currentStepData}>
                                <TranslationsContext.Provider value={translations}>
                                    <div>
                                        <ReactResizeDetector
                                            handleHeight
                                            onResize={this.onResize}
                                        />
                                        {currentStep >= 1 ? (
                                            <React.Fragment>
                                                {currentStepData ? (
                                                    <Step
                                                        handleImageChange={this.handleImageChange}
                                                        stepData={currentStepData}
                                                        progress={progress}
                                                        playIn={playIn}
                                                        step={currentStep}
                                                        specie={specie}
                                                        culture={culture}
                                                        onNextStepClick={this.getNextStep}
                                                    />
                                                ) : null}
                                            </React.Fragment>
                                        ) : (
                                            <SplashScreen
                                                playIn={playIn}
                                                stepData={currentStepData}
                                                onNextStepClick={this.getNextStep}
                                            />
                                        )}
                                    </div>
                                </TranslationsContext.Provider>
                            </StepContext.Provider>
                        </GlobalContext.Provider>
                    </div>
                </Reveal>
                {this.state.isLoading && <Preloader />}
            </React.Fragment>
        );
    }
}
