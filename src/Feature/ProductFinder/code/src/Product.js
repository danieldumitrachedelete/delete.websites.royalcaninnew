import React from 'react';
import GlobalContext from './GlobalContext';
import TranslationsContext from './TranslationsContext';
import PropTypes from 'prop-types';

export default function Product(props) {
    const { product } = props;
    return (
        <GlobalContext.Consumer>
            {({ defaultProductImage }) => (
                <TranslationsContext.Consumer>
                    {translations => (
                        <article className="rc-card rc-card--product rc-padding--sm rc-full-width">
                            <picture className="rc-card__image rc-padding-bottom--sm">
                                <img
                                    src={product.Image || defaultProductImage}
                                    alt={product.ProductName}
                                />
                            </picture>
                            <div className="rc-text--center">
                                <header>
                                    <p className="rc-card__meta">{`id: ${product.MainItemId}`}</p>
                                    <h1 className="rc-card__title">{product.ProductName}</h1>
                                </header>
                                {product.Url && (
                                    <a
                                        className="rc-btn rc-btn--two rc-btn--sm"
                                        href={product.Url}
                                        aria-label="Button"
                                    >
                                        {translations.viewProduct}
                                    </a>
                                )}
                                <p>
                                    {product.WhereToBuyUrl && (
                                        <a href={product.WhereToBuyUrl} className="rc-styled-link">
                                            {translations.whereToBuy}
                                        </a>
                                    )}
                                </p>
                            </div>
                        </article>
                    )}
                </TranslationsContext.Consumer>
            )}
        </GlobalContext.Consumer>
    );
}

Product.propTypes = {
    product: PropTypes.object.isRequired
};
