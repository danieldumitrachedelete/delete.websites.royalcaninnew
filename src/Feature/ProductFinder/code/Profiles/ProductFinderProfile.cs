using AutoMapper;
using Delete.Feature.ProductFinder.Models;

namespace Delete.Feature.ProductFinder.Profiles
{
    public class ProductFinderProfile : Profile
    {
        public ProductFinderProfile()
        {
            CreateMap<ProductFinderStepValue, Models.ViewModels.ProductFinderStepValue>()
                .ForMember(x => x.Name, opt => opt.MapFrom(z => z.Name))
                .ForMember(x => x.Value, opt => opt.MapFrom(z => z.Value))
                .ForMember(x => x.Image, opt => opt.MapFrom(z => z.Image))
                .ForMember(x => x.Description, opt => opt.MapFrom(z => z.Description));

            CreateMap<ProductFinderStep, Models.ViewModels.ProductFinderStep>()
                .ForMember(x => x.Name, opt => opt.MapFrom(z => z.Name))
                .ForMember(x => x.Value, opt => opt.MapFrom(z => z.Value))
                .ForMember(x => x.Image, opt => opt.MapFrom(z => z.Image))
                .ForMember(x => x.StepValues, opt => opt.MapFrom(z => z.StepValues));

            CreateMap<ProductFinderCategory, Models.ViewModels.ProductFinderCategory>()
                .ForMember(x => x.Name, opt => opt.MapFrom(z => z.Name))
                .ForMember(x => x.Value, opt => opt.MapFrom(z => z.Value))
                .ForMember(x => x.Steps, opt => opt.MapFrom(z => z.Steps));
        }
    }
}
