using Delete.Feature.ProductFinder.Managers;
using Delete.Foundation.InitializeDependencyInjection.Extensions;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.DependencyInjection;

namespace Delete.Feature.ProductFinder.DependencyInjection
{
    public class RegisterServices : IServicesConfigurator
    {
        public void Configure(IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IProductFinderCategoryService, ProductFinderCategoryService>();

            serviceCollection.AddMvcControllersInCurrentAssembly();
        }
    }
}
