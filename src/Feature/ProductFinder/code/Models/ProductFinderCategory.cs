﻿using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration.Attributes;

namespace Delete.Feature.ProductFinder.Models
{
	public partial class ProductFinderCategory
	{
        [SitecoreChildren(InferType = true)]
		public virtual IEnumerable<ProductFinderStep> Steps { get; set; }
	}
}
