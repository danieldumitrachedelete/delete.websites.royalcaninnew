﻿using System;
using Sitecore.Data;
using Sitecore.Common;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Sitecore.Globalization;
using System.ComponentModel;
using Delete.Foundation.DeleteFoundationCore.Models;
using Sitecore.ContentSearch;
using Delete.Feature.ProductFinder;

namespace Delete.Feature.ProductFinder.Models.ViewModels
{
	public class ProductFinderStepValue
	{
        public string Name { get; set; }
        public string Value { get; set; }
        public Image Image { get; set; }
        public string Description { get; set; }

        /*
	    public ProductFinderStepValue(Models.ProductFinderStepValue stepValue)
	    {
	        if (stepValue == null) return;

	        Name = stepValue.DisplayName;
	        Value = stepValue.Value;
	        Image = stepValue.Image;
	    }*/
	}
}
