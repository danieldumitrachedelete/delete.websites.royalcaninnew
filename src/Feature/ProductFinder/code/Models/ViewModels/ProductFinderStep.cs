﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Delete.Foundation.DeleteFoundationCore;
using Glass.Mapper.Sc.Fields;
using Sitecore.Data.Query;
using Sitecore.Pipelines.Search;
using Sitecore.Shell.Framework.Commands.Masters;

namespace Delete.Feature.ProductFinder.Models.ViewModels
{
    public class ProductFinderStep
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public Image Image { get; set; }
        public IEnumerable<ProductFinderStepValue> StepValues { get; set; }

        public ProductFinderStep() {}

        /*
        public ProductFinderStep(Models.ProductFinderStep step)
        {
            if (step == null) return;

            Name = step.DisplayName;
            Value = step.Value;
            Image = step.Image;
            StepValues = step?.StepValues?.Select(x => new ProductFinderStepValue(x)).ToList() ??
                         new List<ProductFinderStepValue>();
        }
        */
    }
}