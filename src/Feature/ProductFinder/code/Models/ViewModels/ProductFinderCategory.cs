﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Delete.Feature.ProductFinder.Models.ViewModels
{
    public class ProductFinderCategory
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public IEnumerable<ProductFinderStep> Steps { get; set; }

        public ProductFinderCategory(){}

        /*public ProductFinderCategory(Models.ProductFinderCategory category)
        {
            if (category == null) return;

            Name = category.DisplayName;
            Value = category.Value;
            Steps = category?.Steps?.Select(x => new ProductFinderStep(x)).ToList() ?? new List<ProductFinderStep>();
        }*/
    }
}