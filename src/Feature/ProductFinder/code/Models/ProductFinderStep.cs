﻿using System.Collections.Generic;
using Delete.Feature.ProductFinder.Models.ViewModels;
using Glass.Mapper.Sc.Configuration.Attributes;

namespace Delete.Feature.ProductFinder.Models
{
	public partial class ProductFinderStep
	{
	    [SitecoreChildren(InferType = true)]
	    public virtual IEnumerable<ProductFinderStepValue> StepValues { get; set; }
	}
}
