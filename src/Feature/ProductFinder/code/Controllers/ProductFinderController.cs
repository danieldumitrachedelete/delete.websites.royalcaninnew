using System.Collections.Generic;
using System.Web.Mvc;

using AutoMapper;

using Delete.Feature.ProductFinder.Managers;
using Delete.Feature.ProductFinder.Models;
using Delete.Foundation.DeleteFoundationCore.Controllers;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Interfaces;

using Glass.Mapper.Sc;

using Sitecore.Globalization;

namespace Delete.Feature.ProductFinder.Controllers
{
    using Delete.Foundation.DeleteFoundationCore.Helpers;
    using Delete.Foundation.MultiSite.Models;

    using Models.ViewModels;

    public class ProductFinderController : BaseController
    {
        private readonly IProductFinderCategoryService productFinderCategoryService;

        public ProductFinderController(IProductFinderCategoryService productFinderCategoryService, ISitecoreContext sitecoreContext, IMapper mapper, ICacheManager cacheManager) : base(sitecoreContext, mapper, cacheManager)
        {
            this.productFinderCategoryService = productFinderCategoryService;
        }

        public ActionResult ProductFinder()
        {
            var rootItem = this.SitecoreContext.GetRootItemRelative<LocalMarketSettings>();
            var configuration = this.GetDataSourceItem<ProductFinderConfiguration>();
            var categories = this._mapper.Map<IEnumerable<ProductFinderCategory>>(this.productFinderCategoryService.GetAll(configuration));
            var model = new
            {
                rootItem.CountryCode,
                rootItem.LanguageCode,
                species = rootItem.Species?.Id,
                defaultProductImage = rootItem.DefaultProductImage?.Src,
                categories,
                translations =
                new
                {
                    nextButtonText =
                            TranslateHelper.TextByDomainCached(
                                "Translations",
                                "translations.feature.productfinder.nextbuttontext"),
                    contactUs =
                            TranslateHelper.TextByDomainCached(
                                "Translations",
                                "translations.feature.productfinder.contactus"),
                    noResults =
                            TranslateHelper.TextByDomainCached(
                                "Translations",
                                "translations.feature.productfinder.noresults"),
                    startAgain =
                            TranslateHelper.TextByDomainCached(
                                "Translations",
                                "translations.feature.productfinder.startagain"),
                    viewProduct =
                            TranslateHelper.TextByDomainCached(
                                "Translations",
                                "translations.feature.productfinder.viewproduct"),
                    weRecommend =
                            TranslateHelper.TextByDomainCached(
                                "Translations",
                                "translations.feature.productfinder.werecommend"),
                    whereToBuy =
                            TranslateHelper.TextByDomainCached(
                                "Translations",
                                "translations.feature.productfinder.wheretobuy"),
                    progress = TranslateHelper.TextByDomainCached(
                            "Translations",
                            "translations.feature.productfinder.progress"),
                    anotherRecommendation = TranslateHelper.TextByDomainCached(
                            "Translations",
                            "translations.feature.productfinder.anotherrecommendation"),
                },
                productDetailsUrl = this.Url.ApiAction("FindProducts", "Products")
            };

            return this.View("~/Views/Feature/ProductFinder/ProductFinder.cshtml", model);
        }
    }
}
