﻿using Delete.Feature.ProductFinder.Models;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.DeleteFoundationCore.Managers;
using System.Collections.Generic;
using System.Linq;
using Delete.Foundation.DeleteFoundationCore.Extensions;

namespace Delete.Feature.ProductFinder.Managers
{
    using Delete.Foundation.DeleteFoundationCore.Models;

    public interface IProductFinderCategoryService : IBaseSearchManager<ProductFinderCategory>
    {
        IEnumerable<ProductFinderCategory> GetAll(GlassBase productFinderPage);
    }

    public class ProductFinderCategoryService : BaseSearchManager<ProductFinderCategory>, IProductFinderCategoryService
    {
        public IEnumerable<ProductFinderCategory> GetAll(GlassBase productFinderPage)
        {
            var filteredResults = GetQueryable().Where(x => x.Fullpath.StartsWith(productFinderPage.Fullpath));

            return filteredResults.MapResults(SitecoreContext, true).ToList();
        }

        protected override IQueryable<ProductFinderCategory> GetQueryable()
        {
            return base.GetQueryable().Where(x => x.TemplateId == ProductFinderCategoryConstants.TemplateId);
        }
    }
}