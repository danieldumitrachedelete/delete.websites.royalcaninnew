﻿using Sitecore.Layouts;
using Sitecore.Mvc.Presentation;
using Sitecore.Pipelines.GetChromeData;

namespace Delete.Feature.PartialLayouts.Pipeline
{
    public class DisableWebEditingOfInsertedRenderings : GetChromeDataProcessor
    {
        public override void Process(GetChromeDataArgs args)
        {
            if (args.CustomData["renderingReference"] is RenderingReference renderingReference)
            {
                if (new RenderingParameters(renderingReference.Settings.Parameters)["inserted"] == "true")
                {
                    args.ChromeData.Custom["editable"] = false.ToString().ToLowerInvariant();
                }
            }
        }
    }
}