﻿using System;
using System.Web;
using Sitecore.Pipelines.GetChromeData;

namespace Delete.Feature.PartialLayouts.Pipeline
{
    public class DisablePlaceholdersWithInsertedRenderings : GetChromeDataProcessor
    {
        public override void Process(GetChromeDataArgs args)
        {
            if ("placeholder".Equals(args.ChromeType, StringComparison.OrdinalIgnoreCase))
            {
                if (HttpContext.Current.Items["disable-ph-" + args.CustomData["placeHolderKey"]] != null)
                {
                    args.ChromeData.Custom["editable"] = false.ToString().ToLowerInvariant();
                }
            }
        }
    }
}