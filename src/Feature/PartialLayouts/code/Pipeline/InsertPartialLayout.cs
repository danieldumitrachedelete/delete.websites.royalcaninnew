﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using Sitecore;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Mvc.Extensions;
using Sitecore.Mvc.Pipelines.Response.GetXmlBasedLayoutDefinition;
using Sitecore.Mvc.Presentation;

namespace Delete.Feature.PartialLayouts.Pipeline
{
    public class InsertPartialLayout : GetXmlBasedLayoutDefinitionProcessor
    {
        private readonly List<string> partialLayoutFields = new List<string>();

        public void AddField(string fieldId)
        {
            this.partialLayoutFields.Add(fieldId);
        }

        public override void Process(GetXmlBasedLayoutDefinitionArgs args)
        {
            if (args.Result == null)
            {
                return;
            }

            var item = PageContext.Current.Item;
            if (item == null)
            {
                return;
            }

            var partialLayouts = GetPartialLayouts(item);

            if (partialLayouts != null && partialLayouts.Any())
            {
                foreach (var partialLayout in partialLayouts)
                {
                    var field = partialLayout.Fields[FieldIDs.FinalLayoutField];
                    if (field == null)
                    {
                        return;
                    }

                    var fieldValue = LayoutField.GetFieldValue(field);

                    if (string.IsNullOrWhiteSpace(fieldValue))
                    {
                        return;
                    }

                    var layoutElements = XDocument.Parse(fieldValue).Root;

                    if (layoutElements != null)
                    {
                        var devices = args.Result.Elements(XName.Get("d"));
                        foreach (var device in devices)
                        {
                            var partialDevice = layoutElements.Elements(XName.Get("d")).FirstOrDefault(
                                d => d.GetAttributeValueOrEmpty("id") == device.GetAttributeValueOrEmpty("id"));

                            if (device != null && partialDevice != null)
                            {
                                InsertPartialRenderings(partialDevice, device);
                            }
                        }
                    }
                }
            }
        }

        public IList<Item> GetPartialLayouts(Item item)
        {
            var items = new List<Item>();
            if (item != null)
            {
                foreach (var fieldId in this.partialLayoutFields)
                {
                    if (!string.IsNullOrEmpty(item[fieldId]))
                    {
                        MultilistField field = item.Fields[fieldId];
                        var targetItems = field?.GetItems()?.Where(x => item.ID != x.ID).ToList();

                        if (targetItems != null)
                        {
                            items.AddRange(targetItems);
                            foreach (var targetItem in targetItems)
                            {
                                items.AddRange(this.GetPartialLayouts(targetItem));
                            }
                        }
                    }
                }
            }

            return items;
        }

        private void InsertPartialRenderings(XElement partialDevice, XElement device)
        {
            var partialRenderings = partialDevice.Elements(XName.Get("r")).ToList();

            if (partialRenderings.Count > 0)
            {
                var partialRenderingsDict = partialRenderings.ToDictionary(CreateKey, e => e);
                var newRenderings = new List<XElement>();

                var insertedRenderings = ReconfigureExistingRenderings(device, partialRenderingsDict, partialRenderings, newRenderings);

                if (partialRenderings.Count > 0)
                {
                    insertedRenderings = true;

                    foreach (var rendering in partialRenderings)
                    {
                        MarkRenderingAsInserted(rendering);
                        MarkPlaceholderAsDisabled(rendering);
                        newRenderings.Add(rendering);
                    }
                }

                if (insertedRenderings)
                {
                    device.RemoveNodes();
                    device.Add(newRenderings.Cast<object>().ToArray());
                }
            }
        }

        private void MarkPlaceholderAsDisabled(XElement rendering)
        {
            HttpContext.Current.Items["disable-ph-" + rendering.GetAttributeValueOrEmpty("ph")] = true;
        }

        private bool ReconfigureExistingRenderings(XElement device, Dictionary<string, XElement> insertedRenderingsDict, List<XElement> insertedRenderings, List<XElement> newRenderings)
        {
            var anyInserted = false;
            var renderings = device.Elements(XName.Get("r")).ToArray();

            XElement insertedRendering;
            foreach (var rendering in renderings)
            {
                var inserted = false;

                var key = CreateKey(rendering);

                if (insertedRenderingsDict.TryGetValue(key, out insertedRendering))
                {
                    insertedRenderings.Remove(insertedRendering);
                    insertedRenderingsDict.Remove(key);


                    anyInserted = true;
                    inserted = true;

                    insertedRendering.SetAttributeValue(XName.Get("uid"), rendering.GetAttributeValueOrEmpty("uid"));
                    MarkRenderingAsInserted(insertedRendering);
                    newRenderings.Add(insertedRendering);

                }

                if (!inserted)
                {
                    newRenderings.Add(rendering);
                }
            }
            return anyInserted;
        }

        private void MarkRenderingAsInserted(XElement rendering)
        {
            var paramString = rendering.GetAttributeValueOrEmpty("par");

            if (string.IsNullOrEmpty(paramString))
            {
                rendering.SetAttributeValue(XName.Get("par"), paramString + "inserted=true");
            }
            else
            {
                rendering.SetAttributeValue(XName.Get("par"), paramString + "&inserted=true");
            }
        }

        private string CreateKey(XElement xElement)
        {
            return $"{xElement.GetAttributeValueOrEmpty("ph")}::{xElement.GetAttributeValueOrEmpty("uid")}";
        }
    }
}