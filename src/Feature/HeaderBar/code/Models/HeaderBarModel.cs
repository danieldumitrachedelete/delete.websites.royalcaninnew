﻿using Delete.Foundation.DataTemplates.Models.Menu;

namespace Delete.Feature.HeaderBar.Models
{
    public class HeaderBarModel
    {
        public Foundation.MultiSite.Models.ILocalMarketSettings LocalMarketSettings { get; set; }

        public MenuFolder Navigation { get; set; }
    }
}