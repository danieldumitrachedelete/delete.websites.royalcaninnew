using System.Linq;
using System.Web;
using AutoMapper;
using Delete.Foundation.DeleteFoundationCore.Controllers;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.MultiSite.Extensions;
using Glass.Mapper.Sc;

namespace Delete.Feature.HeaderBar.Controllers
{
    using System;
    using System.Web.Mvc;
    using Sitecore.Data;
    using Repositories;
    using Delete.Foundation.DeleteFoundationCore.Helpers;

    public class HeaderBarController : BaseController
    {
        private readonly IHeaderBarRepository _headerBarRepository;
        private string HeaderBarCacheKey = "HeaderBar_Component_CacheParametersShouldBeAddedHere";

        public HeaderBarController(
            IHeaderBarRepository headerBarRepository,
            ISitecoreContext sitecoreContext, 
            IMapper mapper, 
            ICacheManager cacheManager
        ) : base(sitecoreContext, mapper, cacheManager)
        {
            _headerBarRepository = headerBarRepository;
        }

        public ActionResult HeaderBar()
        {
            var model = _cacheManager.CacheResults(() => _headerBarRepository.GetModel(), HeaderBarCacheKey);

            if (model.Navigation != null)
            { 
                if (model.LocalMarketSettings.EnableYandexTracking)
                {
                    model.Navigation.RegionName = TranslateHelper.TextByDomainCached("Translations", "translations.feature.regionpicker.defaultregion");
                        
                    model.Navigation.Primary.RegionName = model.Navigation.RegionName;
                    model.Navigation.Secondary.RegionName = model.Navigation.RegionName;
                    model.Navigation.Modal.RegionName = model.Navigation.RegionName;
                    model.Navigation.MenuItems.Select(x =>
                    {
                        return x.RegionName == null ? model.Navigation.RegionName : null;
                    });
                    model.Navigation.Primary.MenuItems.Select(x =>
                    {
                        return x.RegionName == null ? model.Navigation.RegionName : null;
                    });
                    model.Navigation.Secondary.MenuItems.Select(x =>
                    {
                        return x.RegionName == null ? model.Navigation.RegionName : null;
                    });
                    model.Navigation.Modal.MenuItems.Select(x =>
                    {
                        return x.RegionName == null ? model.Navigation.RegionName : null;
                    });
                }
                else
                {
                    model.Navigation.RegionName = null;
                }
            }

            return View("~/Views/Feature/HeaderBar/HeaderBar.cshtml", model);
        }
    }
}
