using Delete.Feature.HeaderBar.Repositories;
using Delete.Foundation.InitializeDependencyInjection.Extensions;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.DependencyInjection;

namespace Delete.Feature.HeaderBar.DependencyInjection
{
    public class RegisterServices : IServicesConfigurator
    {
        public void Configure(IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IHeaderBarRepository, HeaderBarRepository>();

            serviceCollection.AddMvcControllersInCurrentAssembly();
        }
    }
}
