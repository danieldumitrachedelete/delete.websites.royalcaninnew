using System;
using Delete.Feature.HeaderBar.Models;
using Delete.Foundation.DataTemplates.Managers;
using Delete.Foundation.MultiSite.Extensions;
using Delete.Foundation.MultiSite.Models;
using Glass.Mapper.Sc;

namespace Delete.Feature.HeaderBar.Repositories
{
    public class HeaderBarRepository : IHeaderBarRepository
    {
        private readonly ISitecoreContext _sitecoreContext;
        private readonly IMenuSearchManager _menuSearchManager;

        public HeaderBarRepository(ISitecoreContext sitecoreContext, IMenuSearchManager  menuSearchManager)
        {
            _sitecoreContext = sitecoreContext;
            _menuSearchManager = menuSearchManager;
        }

        public HeaderBarModel GetModel()
        {
            var localMarketSettings = _sitecoreContext.GetLocalMarketSettings(); // GetRootItem<ILocalMarketSettings>();
            var navigation = _menuSearchManager.GetHeaderFolderForLocal();

            return new HeaderBarModel
            {
                Navigation = navigation,
                LocalMarketSettings = localMarketSettings
            };
        }
    }
}