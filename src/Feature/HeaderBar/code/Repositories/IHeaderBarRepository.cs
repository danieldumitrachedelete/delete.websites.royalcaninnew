using Delete.Feature.HeaderBar.Models;

namespace Delete.Feature.HeaderBar.Repositories
{
    public interface IHeaderBarRepository
  {
      HeaderBarModel GetModel();
  }
}
