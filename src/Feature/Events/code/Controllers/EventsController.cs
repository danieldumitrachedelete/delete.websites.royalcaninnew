using AutoMapper;
using Delete.Foundation.DeleteFoundationCore.Controllers;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Glass.Mapper.Sc;
using Delete.Foundation.MultiSite.Extensions;

namespace Delete.Feature.Events.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;

    using Foundation.DataTemplates.Managers;
    using Foundation.DataTemplates.Models;
    using Foundation.DeleteFoundationCore.Extensions;
    using Foundation.DeleteFoundationCore.Models;

    using Managers;

    using Models;

    public class EventsController : BaseController
    {
        private const int InitialPageIndex = 1;
        private const string FilterGroupsCacheKey = "eventfiltergroups";
        private const string AllEventsCacheKey = "allevents_{0}_{1}";
        private const string FilteredEventsCacheKey = "filteredevents_{0}_{1}_{2}_{3}";
        private const string EventsByKeywordCacheKey = "eventsbykeyword_{0}_{1}_{2}";
        private readonly IEventsSearchManager eventsSearchManager;

        private readonly IFilterGroupFolderSearchManager filterGroupFolderSearchManager;

        public EventsController(
            IEventsSearchManager eventsSearchManager,
            IFilterGroupFolderSearchManager filterGroupFolderSearchManager,
            ISitecoreContext sitecoreContext,
            IMapper mapper,
            ICacheManager cacheManager) : base(sitecoreContext, mapper, cacheManager)
        {
            this.eventsSearchManager = eventsSearchManager;
            this.filterGroupFolderSearchManager = filterGroupFolderSearchManager;
        }

        public ActionResult Search()
        {
            var filterConfiguration = this.GetDataSourceItem<EventsFilterConfiguration>();
            if (filterConfiguration == null)
            {
                return this.Content(Sitecore.Context.PageMode.IsExperienceEditorEditing ? "<p>Please select event filter configuration</p>" : string.Empty);
            }

            var species = filterConfiguration.Species;
            var selectedFilterGroups = filterConfiguration.Filters;
            var pageSize = filterConfiguration.PageSize;
            var contextItem = this.GetContextItem<GlassBase>();

            var allFilterGroups = this._cacheManager.CacheResults(() => this.filterGroupFolderSearchManager.GetAll(), FilterGroupsCacheKey);
            var sortedFilterGroups = selectedFilterGroups
                .Select(x => x.Id)
                .Select(filterGroupId => allFilterGroups.FirstOrDefault(x => x.Id == filterGroupId))
                .Where(g => g != null)
                .ToList();

            var cacheKey = string.Format(AllEventsCacheKey, contextItem?.Id, species?.Id);
            var allEvents = this._cacheManager.CacheResults(() => this.eventsSearchManager.GetAll(contextItem?.Id, species?.Id).ToList(), cacheKey);
            var resultEvents = allEvents.Take(pageSize);

            var result = new SearchViewModel
            {
                FilterConfiguration = filterConfiguration.Id,
                FilterModel = new SearchFilterViewModel
                {
                    FilterGroups = sortedFilterGroups,
                    Specie = species
                },
                SearchResultsModel = new SearchResultsViewModel
                {
                    FilterConfiguration = filterConfiguration.Id,
                    Specie = species,
                    Results = this._mapper.Map<IEnumerable<EventViewModel>>(resultEvents),
                    Page = InitialPageIndex,
                    PageSize = pageSize,
                    TotalResults = allEvents.Count,
                }
            };

            return this.View("~/Views/Feature/Events/Search.cshtml", result);
        }

        [HttpGet]
        public ActionResult Filter(EventsFilterViewModel filterModel)
        {
            var filterConfiguration = this.SitecoreContext.GetItem<EventsFilterConfiguration>(filterModel.Filter, false, true);
            var species = filterConfiguration.Species;
            var pageSize = filterConfiguration.PageSize;
            var contextItem = this.GetContextItem<GlassBase>();
            var currentPage = filterModel.Page > InitialPageIndex ? filterModel.Page : InitialPageIndex;

            var filterIds = this.GetRequestFilterIdsFromCheckboxes(filterModel);

            var cacheKey = string.Format(FilteredEventsCacheKey, contextItem?.Id, species?.Id, filterModel.SearchQuery, string.Join("|", filterIds.SelectMany(x => x.Value)));
            var allEvents = this._cacheManager.CacheResults(
                () => this.eventsSearchManager.GetAll(contextItem?.Id, species?.Id, filterModel.SearchQuery, filterIds)
                    .ToList(), cacheKey);
            var resultEvents = allEvents.Skip((currentPage - InitialPageIndex) * pageSize).Take(pageSize);
            var marketSettings = this.SitecoreContext.GetLocalMarketSettings();

            var result = new SearchResultsViewModel
            {
                FilterConfiguration = filterConfiguration.Id,
                Specie = species,
                Results = this._mapper.Map<IEnumerable<EventViewModel>>(resultEvents),
                Page = currentPage,
                PageSize = pageSize,
                TotalResults = allEvents.Count,
                DateFormat = marketSettings.NewsDateFormat
            };

            return this.View("~/Views/Feature/Events/_EventsSearchResults.cshtml", result);
        }

        public ActionResult Predictive(string keyword, Guid? species)
        {
            var contextItem = this.GetContextItem<GlassBase>();
            var cacheKey = string.Format(EventsByKeywordCacheKey, contextItem?.Id, species, keyword);
            var results =
                this._cacheManager.CacheResults(
                    () => this.eventsSearchManager.GetAll(contextItem?.Id, species, keyword), cacheKey);
            var predictiveResults = this._mapper.Map<IEnumerable<PredictiveSearchItemJsonModel>>(results);
            var json = new PredictiveSearchJsonModel
            {
                Items = predictiveResults
            };

            return this.Json(json, JsonRequestBehavior.AllowGet);
        }

        private IDictionary<string, IEnumerable<Guid>> GetRequestFilterIdsFromCheckboxes(EventsFilterViewModel filterConfiguration)
        {
            var filterValues = new Dictionary<string, IEnumerable<Guid>>();

            var categories = filterConfiguration.EventCategories.ToGuidList();

            var types = filterConfiguration.EventTypes.ToGuidList();

            if (categories.Any())
            {
                filterValues.Add(nameof(filterConfiguration.EventCategories), categories);
            }

            if (types.Any())
            {
                filterValues.Add(nameof(filterConfiguration.EventTypes), types);
            }

            return filterValues;
        }
    }
}