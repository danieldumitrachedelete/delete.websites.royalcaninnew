﻿namespace Delete.Feature.Events.Models
{
    using System;

    public class EventsFilterViewModel
    {
        public string SearchQuery {get; set; }

        public Guid Filter { get; set; }

        public string EventCategories { get; set; }

        public string EventTypes { get; set; }

        public int Page { get; set; }
    }
}