﻿namespace Delete.Feature.Events.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Sitecore.ContentSearch;
    using Glass.Mapper.Sc.Configuration.Attributes;

    using Delete.Foundation.DeleteFoundationCore.Models;

    /// <summary>
    /// As there are no possibilities in Code Generation to inherit one base template and implement fields from another base template
    /// and because we have to inherit taxonomy class for searching
    /// we do this dirty hack in hope to fix Code Generator in some future
    /// </summary>
    public static partial class EventConstants
    {
        public const string DateFieldId = "{ee1c4d9f-5117-45f7-b0bd-fb6b12d19c95}";
        public const string HeadingFieldId = "{3e0d3222-fc81-4105-8496-f340ba2219b8}";
        public const string SummaryFieldId = "{dcbe0623-bbd4-4d7b-91c8-9339fe0400e6}";
        public const string ThumbnailImageFieldId = "{6fd15459-5a23-42fe-a352-6894c12fe253}";
        public const string ContentFieldId = "{4bfa1c61-4105-4f2b-ae99-7b0e91879c17}";
        public const string HeroImageFieldId = "{fe7aa554-e2a8-47be-8206-1fa76ea9503e}";

        public const string DateFieldName = "Date";
        public const string HeadingFieldName = "Heading";
        public const string SummaryFieldName = "Summary";
        public const string ThumbnailImageFieldName = "Thumbnail Image";
        public const string ContentFieldName = "Content";
        public const string HeroImageFieldName = "Hero Image";

    }

    public partial class Event
    {
        [SitecoreField(FieldId = EventConstants.DateFieldId, FieldName = EventConstants.DateFieldName)]
        [IndexField(EventConstants.DateFieldName)]
        public virtual DateTime Date { get; set; }

        [SitecoreField(FieldId = EventConstants.HeadingFieldId, FieldName = EventConstants.HeadingFieldName)]
        [IndexField(EventConstants.HeadingFieldName)]
        public virtual string Heading { get; set; }

        [SitecoreField(FieldId = EventConstants.SummaryFieldId, FieldName = EventConstants.SummaryFieldName)]
        [IndexField(EventConstants.SummaryFieldName)]
        public virtual string Summary { get; set; }

        [SitecoreField(FieldId = EventConstants.ThumbnailImageFieldId, FieldName = EventConstants.ThumbnailImageFieldName)]
        [IndexField(EventConstants.ThumbnailImageFieldName)]
        public virtual ExtendedImage ThumbnailImage { get; set; }

        [SitecoreField(FieldId = EventConstants.ContentFieldId, FieldName = EventConstants.ContentFieldName, ReadOnly = true)]
        [IndexField(EventConstants.ContentFieldName)]
        public virtual string Content { get; set; }

        [SitecoreField(FieldId = EventConstants.HeroImageFieldId, FieldName = EventConstants.HeroImageFieldName)]
        [IndexField(EventConstants.HeroImageFieldName)]
        public virtual Delete.Foundation.DeleteFoundationCore.Models.ExtendedImage HeroImage { get; set; }

        private IEnumerable<Guid> relatedEntities;

        public IEnumerable<Guid> RelatedEntities
        {
            get
            {
                if (this.relatedEntities != null)
                {
                    return this.relatedEntities;
                }

                var result = new List<Guid>();
                if (this.EventCategory != null)
                {
                    result.Add(this.EventCategory.Id);
                }

                result.AddRange(this.EventTypes.Select(x => x.Id));
                result.AddRange(this.Lifestages.Select(x => x.Id));
                result.AddRange(this.Sizes.Select(x => x.Id));
                result.AddRange(this.BreedIds);
                result.AddRange(this.ContentTagIds);
                result.AddRange(this.LifestageIds);
                result.AddRange(this.SizeIds);
                result.AddRange(this.SpecieIds);

                return this.relatedEntities = result;
            }
        }

        [IndexField("eventRelatedEntities")]
        public virtual List<Guid> SearchableRelatedEntitiesIds { get; set; }

        [IndexField("eventTypes")]
        public virtual string SearchableEventTypesText { get; set; }

        [IndexField("eventCategory")]
        public virtual string SearchableEventCategoryText { get; set; }

        [IndexField("eventLifestages")]
        public virtual string SearchableEventLifestagesText { get; set; }

        [IndexField("eventAnimalSizes")]
        public virtual string SearchableEventAnimalSizesText { get; set; }
    }
}