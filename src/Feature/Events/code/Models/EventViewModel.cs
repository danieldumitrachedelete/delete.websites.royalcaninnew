﻿using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Models;
using Delete.Foundation.DeleteFoundationCore.SchemaOrg;
using Delete.Foundation.DeleteFoundationCore.SchemaOrg.Extensions;
using Delete.Foundation.DeleteFoundationCore.SchemaOrg.Interfaces;

namespace Delete.Feature.Events.Models
{
    using System;

    public class EventViewModel : IMicrodata
    {
        public Guid Id { get; set; }

        public DateTime Date { get; set; }

        public string Heading { get; set; }

        public string Type { get; set; }

        public string Url { get; set; }

        public ExtendedImage ThumbnailImage { get; set; }

        public DateTime Updated { get; set; }

        public DateTime Created { get; set; }

        public Thing GetMicrodata(IAuthorResolver authorResolver)
        {
            return new NewsArticle
            {
                Headline = Heading ?? String.Empty,
                Image = ThumbnailImage.GetMicrodata(authorResolver),
                DatePublished = Created.ToDateTimeOffset(),
                DateModified = Updated.ToDateTimeOffset(),
                Url = new Uri(Url ?? String.Empty, UriKind.RelativeOrAbsolute),
                Author = authorResolver.GetAuthor(),
                Publisher = authorResolver.GetAuthor(),
                MainEntityOfPage = new WebPage
                {
                    Id = new Uri(Url ?? String.Empty, UriKind.RelativeOrAbsolute)
                }
            };
        }
    }
}