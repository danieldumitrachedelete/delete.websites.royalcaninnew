﻿using System.Collections.Generic;
using Delete.Foundation.DataTemplates.Models.Folder;
using Delete.Foundation.DataTemplates.Models.Taxonomy;

namespace Delete.Feature.Events.Models
{
    public class SearchFilterViewModel
    {
        public Specie Specie { get; set; }

        public IEnumerable<FilterGroupFolder> FilterGroups { get; set; }
    }
}