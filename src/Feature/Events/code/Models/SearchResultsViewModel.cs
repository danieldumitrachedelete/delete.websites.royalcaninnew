﻿using System;
using System.Collections.Generic;
using Delete.Foundation.DataTemplates.Models.Taxonomy;
using Delete.Foundation.RoyalCaninCore.Models;

namespace Delete.Feature.Events.Models
{
    public class SearchResultsViewModel : PaginatedResults
    {
        public Guid FilterConfiguration { get; set; }

        public Specie Specie { get; set; }

        public IEnumerable<EventViewModel> Results { get; set; }

        public string DateFormat { get; set; }
    }
}