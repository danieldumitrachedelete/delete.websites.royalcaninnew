﻿// <autogenerated>
//   This file was generated by T4 code generator ScriptRunner.tt.
//   Any changes made to this file manually will be lost next time the file is regenerated.
// </autogenerated>

using System;
using Sitecore.Data;
using Sitecore.Common;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Sitecore.Globalization;
using System.ComponentModel;
using Delete.Foundation.DeleteFoundationCore.Models;
using Sitecore.ContentSearch;
using Delete.Feature.Events;

namespace Delete.Feature.Events.Models
{
	public partial interface IEvent : IGlassBase, Delete.Feature.Events.Models.Taxonomy.IEventTaxonomy, Delete.Foundation.DataTemplates.Models.Interfaces.I_PostItem
	{
	}

	public static partial class EventConstants {
		public const string TemplateIdString = "{0695aa84-c497-48da-a886-6ac2e2228ba4}";
		public static readonly ID TemplateId = new ID(TemplateIdString);
		public const string TemplateName = "Event";

	
	
	}

	
	/// <summary>
	/// Event
	/// <para>Path: /sitecore/templates/Feature/Events/Event</para>	
	/// <para>ID: {0695aa84-c497-48da-a886-6ac2e2228ba4}</para>	
	/// </summary>
	[SitecoreType(TemplateId=EventConstants.TemplateIdString)] //, Cachable = true
	public partial class Event  : Delete.Feature.Events.Models.Taxonomy.EventTaxonomy, IEvent
	{
	
	}
}
