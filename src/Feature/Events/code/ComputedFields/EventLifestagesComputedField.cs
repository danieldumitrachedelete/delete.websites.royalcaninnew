﻿namespace Delete.Feature.Events.ComputedFields
{
    using Foundation.DataTemplates.Models.Taxonomy;

    using JetBrains.Annotations;

    [UsedImplicitly]
    public class EventLifestagesComputedField : BaseEventComputedField
    {
        public EventLifestagesComputedField() : base(TaxonomyConstants.LifestagesFieldName)
        {
        }
    }
}