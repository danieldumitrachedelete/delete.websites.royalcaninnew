﻿namespace Delete.Feature.Events.ComputedFields
{
    using Foundation.DataTemplates.Models.Taxonomy;

    using JetBrains.Annotations;

    [UsedImplicitly]
    public class EventAnimalSizesComputedField : BaseEventComputedField
    {
        public EventAnimalSizesComputedField() : base(TaxonomyConstants.SizesFieldName)
        {
        }
    }
}