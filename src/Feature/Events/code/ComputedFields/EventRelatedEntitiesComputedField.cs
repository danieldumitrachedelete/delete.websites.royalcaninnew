﻿namespace Delete.Feature.Events.ComputedFields
{
    using System;
    using System.Collections.Generic;

    using Foundation.DataTemplates.Models.Taxonomy;
    using Foundation.DeleteFoundationCore;
    using Foundation.DeleteFoundationCore.ComputedFields;
    using Foundation.DeleteFoundationCore.Extensions;

    using JetBrains.Annotations;

    using Models;
    using Models.Taxonomy;

    using Sitecore.ContentSearch;
    using Sitecore.ContentSearch.ComputedFields;
    using Sitecore.ContentSearch.Diagnostics;

    [UsedImplicitly]
    public class EventRelatedEntitiesComputedField : BaseComputedField, IComputedIndexField
    {
        public string FieldName { get; set; }

        public string ReturnType { get; set; }

        public object ComputeFieldValue(IIndexable indexable)
        {
            Guard.ArgumentNotNull(indexable, nameof(indexable));

            var indexedItem = this.GetItemHavingBaseTemplate(indexable, EventConstants.TemplateId);
            if (indexedItem == null)
            {
                return null;
            }

            try
            {
                var result = new List<string>();
                result.AddRange(indexedItem.GetFieldValues(TaxonomyConstants.BreedsFieldName));
                result.AddRange(indexedItem.GetFieldValues(TaxonomyConstants.ContentTagsFieldName));
                result.AddRange(indexedItem.GetFieldValues(TaxonomyConstants.LifestagesFieldName));
                result.AddRange(indexedItem.GetFieldValues(TaxonomyConstants.SizesFieldName));
                result.AddRange(indexedItem.GetFieldValues(TaxonomyConstants.SpeciesFieldName));
                result.AddRange(indexedItem.GetFieldValues(EventTaxonomyConstants.EventCategoryFieldName));
                result.AddRange(indexedItem.GetFieldValues(EventTaxonomyConstants.EventTypesFieldName));
                return result;
            }
            catch (Exception e)
            {
                CrawlingLog.Log.Warn($"Unexpected error when computing {GetType()} index field value for item [ID:{indexedItem.ID}]. Error message: {e.Message}");

                return null;
            }
        }
    }
}