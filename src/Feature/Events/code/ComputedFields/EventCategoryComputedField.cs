﻿namespace Delete.Feature.Events.ComputedFields
{
    using JetBrains.Annotations;

    using Models.Taxonomy;

    [UsedImplicitly]
    public class EventCategoryComputedField : BaseEventComputedField
    {
        public EventCategoryComputedField() : base(EventTaxonomyConstants.EventCategoryFieldName)
        {
        }
    }
}