﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Delete.Feature.Events.ComputedFields
{
    using Foundation.DataTemplates.ComputedFields;
    using Foundation.DeleteFoundationCore;

    using Models;

    using Sitecore.ContentSearch;
    using Sitecore.ContentSearch.ComputedFields;
    using Sitecore.ContentSearch.Diagnostics;

    public class BaseEventComputedField : BaseDictionaryTextComputedField, IComputedIndexField
    {
        private readonly string coupledFieldName;

        public BaseEventComputedField(string coupledFieldName)
        {
            this.coupledFieldName = coupledFieldName;
        }

        public string FieldName { get; set; }

        public string ReturnType { get; set; }

        public object ComputeFieldValue(IIndexable indexable)
        {
            Guard.ArgumentNotNull(indexable, nameof(indexable));

            var indexedItem = this.GetItemHavingBaseTemplate(indexable, EventConstants.TemplateId);
            if (indexedItem == null)
            {
                return null;
            }

            try
            {
                return this.GetDictionaryText(indexedItem, this.coupledFieldName);
            }
            catch (Exception e)
            {
                CrawlingLog.Log.Warn($"Unexpected error when computing {this.GetType()} index field value for item [ID:{indexedItem.ID}]. Error message: {e.Message}");

                return null;
            }
        }
    }
}