﻿namespace Delete.Feature.Events.ComputedFields
{
    using JetBrains.Annotations;

    using Models.Taxonomy;

    [UsedImplicitly]
    public class EventTypesComputedField : BaseEventComputedField
    {
        public EventTypesComputedField() : base(EventTaxonomyConstants.EventTypesFieldName)
        {
        }
    }
}