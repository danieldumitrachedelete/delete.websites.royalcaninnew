using System.Linq;
using AutoMapper;

namespace Delete.Feature.Events.Profiles
{
    using Foundation.DataTemplates.Models;
    using JetBrains.Annotations;
    using Models;

    [UsedImplicitly]
    public class EventsProfile : Profile
    {
        public EventsProfile()
        {
            this.CreateMap<Event, EventViewModel>()
                .ForMember(x => x.Id, opt => opt.MapFrom(z => z.Id))
                .ForMember(x => x.Date, opt => opt.MapFrom(z => z.Date))
                .ForMember(x => x.Heading, opt => opt.MapFrom(z => z.Heading))
                .ForMember(x => x.Url, opt => opt.MapFrom(z => z.Url))
                .ForMember(x => x.ThumbnailImage, opt => opt.MapFrom(z => z.ThumbnailImage))
                .ForMember(x => x.Type, opt => opt.MapFrom(z => string.Join(", ",  z.EventTypes.Select(x => x.Text))));

            this.CreateMap<Event, PredictiveSearchItemJsonModel>()
                .ForMember(x => x.Title, opt => opt.MapFrom(z => z.Heading))
                .ForMember(x => x.Url, opt => opt.MapFrom(z => z.Url))
                .ForAllOtherMembers(x => x.Ignore())
                ;
        }
    }
}
