﻿namespace Delete.Feature.Events.Managers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;

    using Foundation.DeleteFoundationCore.Extensions;
    using Foundation.DeleteFoundationCore.Interfaces;
    using Foundation.DeleteFoundationCore.Managers;

    using Models;

    using Sitecore.ContentSearch.Linq.Utilities;
    using Sitecore.Data;

    public interface IEventsSearchManager : IBaseSearchManager<Event>
    {
        IEnumerable<Event> GetAll(
            Guid? rootItemId = null, 
            Guid? specieId = null, 
            string keyword = null, 
            IDictionary<string, IEnumerable<Guid>> filterIds = null);
    }

    public class EventsSearchManager: BaseSearchManager<Event>, IEventsSearchManager
    {
        private const int DaysSinceStartToShowEvents = 5;

        public IEnumerable<Event> GetAll(
            Guid? rootItemId = null, 
            Guid? specieId = null, 
            string keyword = null, 
            IDictionary<string, IEnumerable<Guid>> filterIds = null)
        {
            var filtered = this.GetQueryable();
            filtered = this.FilterLocal(filtered);
            filtered = this.FilterByRootItem(filtered, rootItemId);
            filtered = this.FilterBySpecie(filtered, specieId);
            filtered = this.FilterByKeyword(filtered, keyword);
            filtered = this.FilterByRelatedEntities(filtered, filterIds);

            return filtered
                .MapResults(this.SitecoreContext, true)
                .OrderBy(x => x.Date)
                .ToList();
        }

        private IQueryable<Event> FilterByDate(IQueryable<Event> query, DateTime dateToCompare)
        {
            return query.Where(x => x.Date > dateToCompare);
        }

        private IQueryable<Event> FilterByRelatedEntities(
            IQueryable<Event> query,
            IDictionary<string, IEnumerable<Guid>> filterIds)
        {
            if (filterIds == null || !filterIds.Any())
            {
                return query;
            }

            return query.Where(BuildFilterGroupExpression(filterIds));
        }

        private IQueryable<Event> FilterByRootItem(IQueryable<Event> query, Guid? rootItemId)
        {
            if (!rootItemId.HasValue)
            {
                return query;
            }

            var rootItem = new ID(rootItemId.Value);
            return query.Where(x => x.Paths.Contains(rootItem));
        }

        private IQueryable<Event> FilterBySpecie(IQueryable<Event> query, Guid? specieId)
        {
            if (!specieId.HasValue)
            {
                return query;
            }

            return query.Where(x => x.SpecieIds.Contains(specieId.Value));
        }

        private IQueryable<Event> FilterByKeyword(IQueryable<Event> query, string keyword)
        {
            if (!keyword.NotEmpty())
            {
                return query;
            }

            var predicate = this.FilterByKeyword(keyword);
            return query.Where(predicate);
        }

        protected override IQueryable<Event> GetQueryable()
        {
            return base.GetQueryable()
                .Where(x => x.TemplateId == EventConstants.TemplateId
                    || x.BaseTemplates.Contains(EventConstants.TemplateId.Guid));
        }

        private Expression<Func<Event, bool>> FilterByKeyword(string keyword)
        {
            var predicate = PredicateBuilder.False<Event>();
            predicate = PredicateBuilder.Or(
                predicate,
                x => x.Heading.StartsWith(keyword)
              || x.SearchableEventTypesText.StartsWith(keyword)
              || x.SearchableEventCategoryText.StartsWith(keyword)
              || x.SearchableEventLifestagesText.StartsWith(keyword)
              || x.SearchableEventAnimalSizesText.StartsWith(keyword));
            return predicate;
        }

        /// <summary>
        /// Search by grouped items. Do not use generic method because it throws exception in building solr query
        /// </summary>
        /// <param name="filterGroups"></param>
        /// <returns></returns>
        private static Expression<Func<Event, bool>> BuildFilterGroupExpression(
            IDictionary<string, IEnumerable<Guid>> filterGroups)
        {
            var expression = PredicateBuilder.True<Event>();
            foreach (var filterGroup in filterGroups)
            {
                var currentExpression = PredicateBuilder.True<Event>();

                foreach (var filter in filterGroup.Value)
                {
                    var filterValue = filter;
                    currentExpression = currentExpression.Or(x => x.SearchableRelatedEntitiesIds.Contains(filterValue));
                }

                expression = expression.And(currentExpression);
            }

            return expression;
        }
    }
}