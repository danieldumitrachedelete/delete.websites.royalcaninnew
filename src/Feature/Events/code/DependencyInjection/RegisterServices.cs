using AutoMapper;
using Delete.Foundation.InitializeDependencyInjection.Extensions;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.DependencyInjection;

namespace Delete.Feature.Events.DependencyInjection
{
    using Managers;

    public class RegisterServices : IServicesConfigurator
    {
        public void Configure(IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IEventsSearchManager, EventsSearchManager>();
            serviceCollection.AddMvcControllersInCurrentAssembly();
        }
    }
}
