export default class MapBridge {
    constructor(el, options) {
        this.el = el;
        this.options = options;
        this.mapClasses = {
            Google: null,
            Baidu: null,
            Yandex: null
        };
        this.defineImplementations();
        this.chooseImplementation();
    }

    defineImplementations() {
        throw new Error('Please define the method in inherited class');
    }

    chooseImplementation() {
        /** *
         * TODO Implement logic of choosing implementation
         *
         *  if (window.state.cultureCode === 'zh-CN') {
                this._implementation = new this.mapClasses.Baidu(this.el, this.options);
            }
         */
        this._implementation = new this.mapClasses.Google(this.el, this.options);
    }

    initApi() {
        return this._implementation.initApi();
    }

    triggerResize() {
        this._implementation.triggerResize();
    }

    setZoom(zoom) {
        this._implementation.setZoom(zoom);
    }
}
