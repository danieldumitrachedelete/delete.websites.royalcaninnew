import BaseComponent from 'Foundation/src/js/BaseComponent';
import Utils from 'Foundation/src/js/utils';
import { config } from '../../index';
import MediatorEvents from '../enums/mediatorEvents';
import MediatorEventModel from '../models/mediatorEventModel';
import EventEmitter from 'Foundation/src/js/EventEmitter';

export default class FilterPaginationControl extends BaseComponent {
    constructor(el) {
        super(el);
        this.init();
    }

    static get tagName() {
        return 'filter-pagination';
    }

    static get constructorName() {
        return 'filterPaginationControl';
    }

    init() {
        this.setVariables();
        this.addListeners();
    }

    setVariables() {
        this.isInited = true;
        this.mediator = null;
        this.paginationButtons = [
            ...this.el.querySelectorAll('[data-ref="filter-pagination-btn"]')
        ];
        this.paginationInputs = [
            ...this.el.querySelectorAll('[data-ref="filter-pagination-input"]')
        ];
    }

    setMediator(mediator) {
        this.mediator = mediator;
    }

    addListeners() {
        this.paginationButtons.forEach((button) => {
            this.addListener(button, 'click', e => this.handlePaginationBtnClick(e));
        });

        this.paginationInputs.forEach((input) => {
            this.addListener(input, 'keypress', e => this.handleInputKeyPress(e));
        });
    }

    handlePaginationBtnClick(e) {
        e.preventDefault();

        const mediatorEvent = new MediatorEventModel();
        mediatorEvent.eventType = MediatorEvents.paginationButtonClicked;

        const searchQuery = config.filterPanelControl.getSearchQuery();
        Object.assign(searchQuery, ...config.requestParameters);
        searchQuery.searchQuery = config.filterSearchControl.getInputValue();
        const bounds = config.state.map.bounds;
        Object.assign(searchQuery, ...bounds);
        searchQuery.page = e.target.getAttribute('data-page');
        searchQuery.latitude = config.state.map.latitude;
        searchQuery.longitude = config.state.map.longitude;
        searchQuery.currentLatitude = config.state.map.currentLatitude;
        searchQuery.currentLongitude = config.state.map.currentLongitude;
        mediatorEvent.searchQuery = searchQuery;
        const queryString = Utils.convertToQuery(searchQuery);

        mediatorEvent.eventUrl = `${config.endPointUrl}${queryString}`;

        if (config.mediator) {
            config.mediator.stateChanged(mediatorEvent);
            EventEmitter.emit('pagination:change');
        }
    }

    handleInputKeyPress(e) {
        const regExp = /^\d+$/;
        const char = String.fromCharCode(e.which);

        if (!regExp.test(char)) {
            e.preventDefault();
        }

        const inputValue = e.target.value;

        if (e.keyCode === 13) {
            const mediatorEvent = new MediatorEventModel();
            mediatorEvent.eventType = MediatorEvents.paginationButtonClicked;

            const searchQuery = config.filterPanelControl.getSearchQuery();
            Object.assign(searchQuery, ...config.requestParameters);
            searchQuery.searchQuery = config.filterSearchControl.getInputValue();
            const bounds = config.state.map.bounds;
            Object.assign(searchQuery, ...bounds);
            searchQuery.page = inputValue;

            searchQuery.latitude = config.state.map.latitude;
            searchQuery.longitude = config.state.map.longitude;
            searchQuery.currentLatitude = config.state.map.currentLatitude;
            searchQuery.currentLongitude = config.state.map.currentLongitude;

            mediatorEvent.searchQuery = searchQuery;
            const queryString = Utils.convertToQuery(searchQuery);

            mediatorEvent.eventUrl = `${config.endPointUrl}${queryString}`;

            if (config.mediator) {
                config.mediator.stateChanged(mediatorEvent);
                EventEmitter.emit('pagination:change');
            }
        }
    }

    onDestroy() {
        this.paginationButtons = null;
        this.paginationInputs = null;
        this.el = null;
    }
}
