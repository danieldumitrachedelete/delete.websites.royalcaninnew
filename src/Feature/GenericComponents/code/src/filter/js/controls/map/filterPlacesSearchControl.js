import Utils from 'Foundation/src/js/utils';
import { config } from '../../../index';
import MediatorEvents from '../../enums/mediatorEvents';
import MediatorEventModel from '../../models/mediatorEventModel';
import MapScriptObserver from '../mapScriptObserver';
import BaseComponent from 'Foundation/src/js/BaseComponent';

export default class FilterPlacesSearchControl extends BaseComponent {
    constructor(el) {
        super(el);

        MapScriptObserver.onLoad(() => {
            this.init();
        });
    }

    static get tagName() {
        return 'filter-places-search';
    }

    static get constructorName() {
        // Use the same name as filterSearchControl
        return 'filterSearchControl';
    }

    init() {
        this.setVariables();
        this.addListeners();
        this.initializeAutocomplete();
    }

    setVariables() {
        this.isInited = true;
        this.currentPositionBtn = this.el.querySelector('[data-ref="locate-to-me-btn"]');
        this.searchFieldNode = this.el.querySelector('[data-ref="filter-input"]');
        this.searchButtonNode = this.el.querySelector('[data-ref="filter-search-btn"]');
        this.currentCulture = this.el.getAttribute('data-search-current-culture');

        this.hasDownBeenPressed = false;
        this.trackCategory = this.searchFieldNode.getAttribute('data-track-category') || '';
        this.trackLabel = this.searchFieldNode.getAttribute('data-track-label') || '';
        this.trackEvent = this.searchFieldNode.getAttribute('data-track-event') || '';
        this.trackAction = this.searchFieldNode.getAttribute('data-track-action') || '';
        this.trackBreedId = this.searchFieldNode.getAttribute('data-track-breed-id') || '';
        this.trackSpecieId = this.searchFieldNode.getAttribute('data-track-specie-id') || '';
        this.trackAge = this.searchFieldNode.getAttribute('data-track-age') || '';
    }

    setMediator(mediator) {
        this.mediator = mediator;
    }

    addListeners() {
        this.addListener(this.currentPositionBtn, 'click', e => this.handleGetCurrentPosition(e));
        this.addListener(this.searchButtonNode, 'click', e => this.handlePlacesSearch(e));
        this.addListener(this.searchFieldNode, 'keydown', e => this.handleKeyDown(e));
        this.addListener(this.searchFieldNode, 'focus', () => this.handleSearchFieldFocus());

        window.google.maps.event.addDomListener(this.searchFieldNode, 'keydown', (e) => {
            e.cancelBubble = true;
            if (e.keyCode === 13 || e.keyCode === 9) {
                if (!this.hasDownBeenPressed) {
                    const event = document.createEvent('Event');
                    event.keyCode = 40;
                    event.which = 40;
                    event.key = 40;
                    event.initEvent('keydown', true, true, false, 40);
                    this.searchFieldNode.dispatchEvent(event);
                    this.hasDownBeenPressed = true;
                }
            }
        });
    }

    handleSearchFieldFocus() {
        this.hasDownBeenPressed = false;
    }

    handleKeyDown(e) {
        if (e.keyCode === 40) {
            this.hasDownBeenPressed = true;
        }
    }

    initializeAutocomplete() {
        const cultures = JSON.parse(this.currentCulture);
        const options = {
            componentRestrictions: { country: cultures }
        };

        this.autocomplete = new window.google.maps.places.Autocomplete(this.searchFieldNode, {
            options
        });

        this.autocomplete.setFields(['address_components', 'geometry', 'name']);
        window.google.maps.event.addListener(
            this.autocomplete,
            'place_changed',
            this.handlePlaceChanged.bind(this)
        );
    }

    handlePlacesSearch() {
        if (!this.hasDownBeenPressed) {
            const event = document.createEvent('Event');
            event.keyCode = 13;
            event.which = 13;
            event.key = 13;
            event.initEvent('keydown', true, true, false, 13);
            this.searchFieldNode.dispatchEvent(event);
        }
    }

    handlePlaceChanged() {
        const place = this.autocomplete.getPlace();

        if (typeof place.address_components !== 'undefined') {
            this.hasDownBeenPressed = false;
        }

        const placeName = place.name;
        const location = place.geometry ? place.geometry.location : null;

        this.handleSelectPlace(placeName, location);
    }

    handleSelectPlace(placeName, location) {
        const mediatorEvent = new MediatorEventModel();
        mediatorEvent.eventType = MediatorEvents.search;

        const searchQuery = config.filterPanelControl.getSearchQuery();
        Object.assign(searchQuery, ...config.requestParameters);
        config.state.map.bounds = {};
        searchQuery.searchQuery = placeName;

        if (location) {
            searchQuery.latitude = location.lat();
            config.state.map.latitude = location.lat();
            searchQuery.longitude = location.lng();
            config.state.map.longitude = location.lng();
        }

        searchQuery.currentLatitude = config.state.map.currentLatitude;
        searchQuery.currentLongitude = config.state.map.currentLongitude;

        mediatorEvent.searchQuery = searchQuery;
        const queryString = Utils.convertToQuery(searchQuery);
        mediatorEvent.eventUrl = `${config.endPointUrl}${queryString}`;

        mediatorEvent.trackDetails = this.getTrackDetails();

        if (config.mediator) {
            config.mediator.stateChanged(mediatorEvent);
        }
    }

    handleGetCurrentPosition() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                position => this.handleSuccessPosition(position),
                error => this.handleErrorPosition(error)
            );
        } else {
            throw new Error('Geolocation is not supported by this browser');
        }
    }

    handleSuccessPosition(position) {
        const { latitude, longitude } = position.coords;

        const mediatorEvent = new MediatorEventModel();
        this.resetInputValue();
        mediatorEvent.eventType = MediatorEvents.getCurrentPosition;

        const searchQuery = config.filterPanelControl.getSearchQuery();
        Object.assign(searchQuery, ...config.requestParameters);
        const bounds = config.state.map.bounds;
        Object.assign(searchQuery, ...bounds);

        searchQuery.currentLatitude = latitude;
        searchQuery.latitude = undefined;
        searchQuery.currentLongitude = longitude;
        searchQuery.longitude = undefined;
        config.state.map.currentLatitude = latitude;
        config.state.map.latitude = undefined;
        config.state.map.currentLongitude = longitude;
        config.state.map.longitude = undefined;
        searchQuery.isLocal = true;

        mediatorEvent.searchQuery = searchQuery;
        const queryString = Utils.convertToQuery(searchQuery);
        mediatorEvent.eventUrl = `${config.endPointUrl}${queryString}`;
        mediatorEvent.position = position;

        if (config.mediator) {
            config.mediator.stateChanged(mediatorEvent);
        }
    }

    handleErrorPosition(error) {
        throw new Error(`ERROR(${error.code}): ${error.message}`);
    }

    getInputValue() {
        return this.searchFieldNode.value;
    }

    resetInputValue() {
        this.searchFieldNode.value = '';
    }

    getTrackDetails() {
        const trackDetails = {
            category: this.trackCategory,
            label: this.trackLabel,
            event: this.trackEvent,
            breedId: this.trackBreedId,
            specieId: this.trackSpecieId,
            age: this.trackAge
        };
        return trackDetails;
    }
}
