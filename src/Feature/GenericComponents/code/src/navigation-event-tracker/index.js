import BaseComponent from 'Foundation/src/js/BaseComponent';
import Utils from 'Foundation/src/js/utils';
import GTM from 'Foundation/src/js/gtm/gtm';

export default class NavigationEventTracker extends BaseComponent {
    constructor(el) {
        super(el);
        this.trackLabel = null;
        this.navName = this.el.getAttribute('data-nav-name');
        this.pageTitle = document.title;
        this.items = [...this.el.querySelectorAll('[data-ref="nav-link"]')];
        this.init();
    }

    static get tagName() {
        return 'nav-track';
    }

    init() {
        this.items.forEach((link) => {
            link.addEventListener('click', this.onClick.bind(this));
        });
    }

    onClick(e) {
        this.standardTracking(e);
        this.customTracking(e);
    }

    standardTracking(e) {
        const parentNode = Utils.getClosestNodes(e.target, '[data-ref="nav-link"]');
        const trackCategory = `${this.navName} / ${this.pageTitle} ${this.navName}`;
        const label = parentNode[0].getAttribute('href');
        const trackEvent = trackCategory + ' ' + label;
        GTM.push(trackCategory, 'Click', label, trackEvent);
    }

    customTracking(e) {
        const parentNode = Utils.getClosestNodes(e.target, '[data-ref="nav-link"]');

        console.log('parentNode ', parentNode);

        const customTracking = parentNode[0].getAttribute('data-track-custom');
        
        if (!customTracking) return;
        
        const trackCategory = parentNode[0].getAttribute('data-track-custom-category');
        const trackAction = parentNode[0].getAttribute('data-track-custom-action');
        const trackLabel = parentNode[0].getAttribute('data-track-custom-label').replace(/(^\w+:|^)\/\//, '');
        const trackEvent = parentNode[0].getAttribute('data-track-custom-event');

        GTM.push(trackCategory, trackAction, trackLabel, trackEvent);
    }
}
