import BaseComponent from 'Foundation/src/js/BaseComponent';
import './styles.scss';
import intersectionHelper from 'Foundation/src/js/intersection-helper';

export default class ContentAnimation extends BaseComponent {
    constructor(el) {
        super(el);
        this.init();
    }

    static get tagName() {
        return 'content-animation';
    }

    setVariables() {
        this.defaultOptions = {
            baseAnimationCSSClass: 'rc-animation-001--base',
            activateAnimationCSSClass: 'rc-animation-001--active',
            mobile: true,
            animationDisabled: false
        };

        this.settings = {
            ...this.defaultOptions,
            ...this.el.dataset
        };

        this.animate = this.animate.bind(this);
    }

    addListeners() {}

    init() {
        this.isInited = true;

        this.setVariables();
        this.addListeners();

        if (this.settings.animationDisabled) return;

        setTimeout(() => {
            const isElementIntoView = this.isElementIntoView();
            if (!isElementIntoView) {
                this.applyBaseAnimationStyles();
                intersectionHelper.enterViewportOnce(this.el, this.animate.bind(this), {
                    threshold: 0
                });
            }
        }, 300);
    }

    isElementIntoView() {
        const windowBottomEdge = window.scrollY + window.innerHeight;
        const elementTopEdge = this.el.offsetTop;
        return elementTopEdge <= windowBottomEdge;
    }

    applyBaseAnimationStyles() {
        this.el.classList.add(this.settings.baseAnimationCSSClass);
    }

    animate() {
        this.el.classList.add(this.settings.activateAnimationCSSClass);
    }
}
