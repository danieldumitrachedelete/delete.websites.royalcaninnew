﻿using System.Collections.Generic;
using Delete.Feature.GenericComponents.Models;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Models;

namespace Delete.Feature.GenericComponents.Services
{
    public static class BreadcrumbService
    {
        public static BreadcrumbLink GetOneLevelBreadcrumb(GlassBase currentItem)
        {
            var item = currentItem.Parent;

            if (item == null)
            {
                return null;
            }

            return new BreadcrumbLink
            {
                Title = item.DisplayName.OrDefault(item.Name),
                Url = item.Url,
                IsCurrent = item == currentItem,
            };
        }

        public static List<BreadcrumbLink> GetMultiLevelBreadcrumbs(GlassBase currentItem, GlassBase homeItem)
        {
            var breadcrumbs = new List<BreadcrumbLink>();

            var homeItemId = homeItem.Id;

            var item = currentItem;
            while (item != null)
            {
                breadcrumbs.Add(new BreadcrumbLink
                {
                    Title = item.DisplayName.OrDefault(item.Name),
                    Url = item.Url,
                    IsCurrent = item == currentItem,
                });

                if (item.Id == homeItemId)
                {
                    break;
                }

                item = item.Parent;
            }

            breadcrumbs.Reverse();

            return breadcrumbs;
        }
    }
}