﻿using System;
using System.Linq;
using System.Web.Mvc;
using Delete.Foundation.DataTemplates.Models.Dictionary;
using Delete.Foundation.DeleteFoundationCore.Extensions;

namespace Delete.Feature.GenericComponents.Extensions
{
    using Foundation.DeleteFoundationCore.Utils;

    using Sitecore.Mvc;

    public static class HtmlHelperExtensions
    {
        public static IDisposable WrapToContentBlockFixedWidth(this HtmlHelper htmlHelper, bool wrap)
        {
            if (htmlHelper == null || !wrap)
            {
                return new DisposableHtmlWrapper(() => { }, () => { });
            }

            return new DisposableHtmlWrapper(
                () => htmlHelper.ViewContext.Writer.Write($"<div class='content-block'><div class='content-block__wrapper'>"),
                () => htmlHelper.ViewContext.Writer.Write("</div></div>"));
        }

        public static IDisposable WrapToContainerWithSpecificWidth(this HtmlHelper htmlHelper, IDictionaryEntry widthSetting, params IDictionaryEntry[] additionalStyles)
        {
            if (widthSetting == null || string.IsNullOrEmpty(widthSetting?.Value)
                                     || htmlHelper?.ViewContext?.Writer == null)
            {
                return new DisposableHtmlWrapper(() => { }, () => { });
            }

            var additionalClasses = string.Join(" ", additionalStyles.Select(x => x?.Value).Where(x => x.NotEmpty()));

            return new DisposableHtmlWrapper(
                () => htmlHelper.ViewContext.Writer.Write($"<div class='{widthSetting.Value} {additionalClasses}'>"),
                () => htmlHelper.ViewContext.Writer.Write("</div>"));
        }

        public static IDisposable WrapToRoyalCaninColumn(this HtmlHelper htmlHelper, IDictionaryEntry columndWidth, IDictionaryEntry columnPadding, bool noPadding = false)
        {
            if (columndWidth == null || string.IsNullOrEmpty(columndWidth?.Value) || htmlHelper?.ViewContext?.Writer == null)
            {
                return new DisposableHtmlWrapper(() => { }, () => { });
            }

            return new DisposableHtmlWrapper(
                () => htmlHelper.ViewContext.Writer.Write($"<div class='rc-column {(noPadding ? "rc-padding-y--none " : "")}{columndWidth?.Value} {columnPadding?.Value}'>"),
                () => htmlHelper.ViewContext.Writer.Write("</div>"));
        }

        public static IDisposable WrapToDivWithClasses(this HtmlHelper htmlHelper, params string[] classes)
        {
            return htmlHelper.WrapToTagWithClasses("div", classes);
        }


        public static IDisposable WrapToTagWithClasses(this HtmlHelper htmlHelper, string tagName, params string[] classes)
        {
            if (tagName.Empty() || classes == null)
            {
                return new DisposableHtmlWrapper(() => { }, () => { });
            }

            var joinedClasses = string.Join(" ", classes.Where(x => x.NotEmpty()));

            return new DisposableHtmlWrapper(
                () => htmlHelper.ViewContext.Writer.Write($"<{tagName} class='{joinedClasses}'>"),
                () => htmlHelper.ViewContext.Writer.Write($"</{tagName}>"));
        }

        public static bool IsInLayoutContainer(this HtmlHelper htmlHelper)
        {
            return htmlHelper.Sitecore().CurrentRendering?.Placeholder?.Contains("rc-layout-container") ?? false;
        }

    }
}