﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Delete.Feature.GenericComponents.Extensions
{
    public static class IEnumerableExtentions
    {
        public static IEnumerable<T> DistinctBy<T, TKey>(this IEnumerable<T> items, Func<T, TKey> property)
        {
            return items.GroupBy(property).Select(x => x.First<T>());
        }
    }
}