﻿using System;
using System.Collections.Generic;
using System.Linq;
using Delete.Feature.GenericComponents.Models;
using Delete.Foundation.DeleteFoundationCore.SchemaOrg;

namespace Delete.Feature.GenericComponents.Extensions
{
    public static class BreadcrumbLinkEnumerableExtensions
    {
        public static Thing GetMicrodata(this IEnumerable<BreadcrumbLink> breadcrumbLinks)
        {
            var links = new List<ListItem>();
            var position = 1;

            foreach (var link in breadcrumbLinks)
            {
                links.Add(new ListItem
                {
                    Position = position,
                    Item = new Thing
                    {
                        Id = new Uri(link.Url ?? String.Empty, UriKind.RelativeOrAbsolute),
                        Name = link.Title ?? String.Empty
                    }
                });

                position++;
            }
            
            return new BreadcrumbList
            {
                ItemListElement = links
            };
        }
    }
}