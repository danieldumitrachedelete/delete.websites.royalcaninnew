﻿using System;
using System.Collections.Generic;
using System.Linq;
using Delete.Feature.GenericComponents.Models;
using Glass.Mapper.Sc;

namespace Delete.Feature.GenericComponents.Extensions
{
    public static class SitecoreContextExtensions
    {
        public static bool DoesPlaceholderContainNoPaddingFlags(this ISitecoreContext context, string dynamicPlaceholderName, int seed)
        {
            var dynamicKey =
                $"{dynamicPlaceholderName}-{Sitecore.Mvc.Presentation.RenderingContext.Current.Rendering.UniqueId.ToString("B").ToUpperInvariant()}-{seed}";
            return context.DoesPlaceholderContainNoPaddingFlags(dynamicKey);
        }

        private static bool DoesPlaceholderContainNoPaddingFlags(this ISitecoreContext context, string placeholderKey)
        {
            var pageContext = Sitecore.Mvc.Presentation.PageContext.Current;
            var pageDefinition = pageContext.PageDefinition;

            var contentBlockRenderingNames = new List<string> { "Content Block", "Hero Panel" };
            var contentBlockVariationRenderingNames = new List<string>
            {
                "Content Block Variation 1",
                "Content Block Variation 2",
                "Content Block Variation 3",
                "Content Block Variation 4",
                "Content Block Variation 5",
                "Content Block Variation 6",
                "Content Block Variation 7"
            };

            var nestedRenderings = pageDefinition.Renderings.Where(x =>
                x.Placeholder.IndexOf(placeholderKey, StringComparison.InvariantCulture) >= 0);
            var contentBlockRenderings = nestedRenderings.Where(x => contentBlockRenderingNames.Contains(x.RenderingItem.Name));
            var contentBlockVariationRenderings = nestedRenderings.Where(x => contentBlockVariationRenderingNames.Contains(x.RenderingItem.Name));

            var result = false;

            result = contentBlockRenderings.Any(x =>
            {
                var datasource = context.GetItem<ContentBlockColumns>(x.DataSource);
                return datasource.NoPaddingUnderColumn1 || datasource.NoPaddingUnderColumn2;
            });

            result = result || contentBlockVariationRenderings.Any(x =>
            {
                var datasource = context.GetItem<ContentBlockWithText>(x.DataSource);
                return datasource.NoPaddingUnderImage;
            });

            return result;
        }
    }
}