using System.Web.Mvc;
using AutoMapper;
using Delete.Feature.GenericComponents.Services;
using Delete.Foundation.DeleteFoundationCore.Controllers;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.DeleteFoundationCore.Models;
using Delete.Foundation.MultiSite.Models;
using Glass.Mapper.Sc;

namespace Delete.Feature.GenericComponents.Controllers
{
    using Delete.Feature.GenericComponents.Models;
    using Delete.Foundation.DataTemplates.Managers;
    using Delete.Foundation.DataTemplates.Models.Interfaces;

    using Sitecore.Data;

    public class GenericComponentsController : BaseController
    {
        private readonly ILocalMarketSettings localMarketSettings;
        private readonly ITabbedNavigationSearchManager tabbedNavigationSearchManager;

        public GenericComponentsController(
            ISitecoreContext sitecoreContext,
            IMapper mapper,
            ICacheManager cacheManager,
            ILocalMarketSettings localMarketSettings,
            ITabbedNavigationSearchManager tabbedNavigationSearchManager)
            : base(sitecoreContext, mapper, cacheManager)
        {
            this.localMarketSettings = localMarketSettings;
            this.tabbedNavigationSearchManager = tabbedNavigationSearchManager;
        }

        public ActionResult MultiLevelBreadcrumbs()
        {
            var currentItem = this.GetContextItem<GlassBase>();
            var homeItem = this._context.GetHomeItem<GlassBase>();

            var model = BreadcrumbService.GetMultiLevelBreadcrumbs(currentItem, homeItem);
            return this.View("~/Views/Feature/GenericComponents/MultiLevelBreadcrumbs.cshtml", model);
        }

        [CacheControl(SharedMaxAge = 0, MaxAge = 0)]
        public EmptyResult CacheControl()
        {
            return new EmptyResult();
        }

        public ActionResult OneLevelBreadcrumb()
        {
            var currentItem = this.GetContextItem<GlassBase>();
            var model = BreadcrumbService.GetOneLevelBreadcrumb(currentItem);
            return this.View("~/Views/Feature/GenericComponents/OneLevelBreadcrumb.cshtml", model);
        }

        public ActionResult ImageTabbedNavigation()
        {
            var currentItem = this.SitecoreContext.GetCurrentItem<_TabbedNavigationItem>();
            if (currentItem == null)
            {
                return new EmptyResult();
            }

            var rootItem = currentItem.ChildPage
                               ? new ID(currentItem.Parent.Id)
                               : new ID(currentItem.Id);

            var model = new TabbedLifestagesNavigationModel
                            {
                                ListingItemSettings = currentItem,
                                Items = this.tabbedNavigationSearchManager.FindChildren(rootItem)
                            };
            return this.View("~/Views/Feature/GenericComponents/_ImageTabbedNavigation.cshtml", model);
        }

        public ActionResult OrganizationSchema()
        {
            return this.View("~/Views/Feature/GenericComponents/_OrganizationSchema.cshtml", this.localMarketSettings);
        }

        public ActionResult LocalBusinessSchema()
        {
            return this.View("~/Views/Feature/GenericComponents/_LocalBusinessSchema.cshtml", this.localMarketSettings);
        }

        public ActionResult BrandSchema()
        {
            return this.View("~/Views/Feature/GenericComponents/_BrandSchema.cshtml", this.localMarketSettings);
        }

        public ActionResult WebsiteSchema()
        {
            return this.View("~/Views/Feature/GenericComponents/_WebsiteSchema.cshtml", this.localMarketSettings);
        }

        public ActionResult ContactsCard()
        {
            return this.View("~/Views/Feature/GenericComponents/ContactsCard.cshtml", this.localMarketSettings);
        }

        public ActionResult LiveChat()
        {
            return this.View("~/Views/Feature/GenericComponents/LiveChatPage.cshtml", this.localMarketSettings);
        }
    }
}
