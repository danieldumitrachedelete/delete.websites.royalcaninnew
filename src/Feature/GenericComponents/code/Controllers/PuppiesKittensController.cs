﻿using AutoMapper;
using Delete.Feature.GenericComponents.Extensions;
using Delete.Feature.GenericComponents.Models.PuppiesKittens;
using Delete.Foundation.DeleteFoundationCore.Controllers;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.DeleteFoundationCore.Models;
using Glass.Mapper.Sc;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Delete.Foundation.DeleteFoundationCore.Extensions;

namespace Delete.Feature.GenericComponents.Controllers
{
    public class PuppiesKittensController : BaseController
    {
        public PuppiesKittensController(
            ISitecoreContext sitecoreContext,
            IMapper mapper,
            ICacheManager cacheManager) : base(sitecoreContext, mapper, cacheManager)
        {
        }

        public ActionResult FeatureContentBlock()
        {
            var model = GetDataSourceItem<FeatureContentBlock>();
            model.AdditionalButtons = model.Children.Where(x => x is ContentBlockButton);

            return View("~/Views/Feature/GenericComponents/PuppiesKittens/FeatureContentBlock.cshtml", model);
        }

        public ActionResult QuickTip()
        {
            var model = GetDataSourceItem<QuickTip>();
            return View("~/Views/Feature/GenericComponents/PuppiesKittens/QuickTip.cshtml", model);
        }

        public ActionResult QuickTipsContainer()
        {
            var model = GetDataSourceItem<QuickTipsContainer>();
            if (model?.ContainerType == null)
                return View("~/Views/Feature/GenericComponents/PuppiesKittens/QuickTipsContainer.cshtml", model);

            model.TipsGrouper = new QuickTipsGrouper(model.ContainerType.ColumnCount);

            foreach (IGlassBase child in model.Children)
                if (child is QuickTip tip)
                    model.TipsGrouper.AddTip(tip);

            return View("~/Views/Feature/GenericComponents/PuppiesKittens/QuickTipsContainer.cshtml", model);
        }

        public ActionResult DownloadableContent()
        {
            var model = GetDataSourceItem<DownloadableContent>(inferType: true);
            return View("~/Views/Feature/GenericComponents/PuppiesKittens/DownloadableContent.cshtml", model);
        }

        public ActionResult FAQs()
        {
            var dataSource = GetDataSourceItem<FAQDatasource>(inferType: true);
            var model = new List<FAQ>();
            if (dataSource.FAQs.Any())
            {
                model = dataSource.FAQs.ToList();
            }
            else
            {
                var AllFaqs = dataSource.FolderWithFAQs.FAQItems.ToList();

                model.AddRange(from faq in AllFaqs from categoryFaq in faq.Categories from category in dataSource.Categories where category.Id == categoryFaq.Id select faq);

                model.AddRange(from faq in AllFaqs from categoryFaq in faq.Categories from categoryType in dataSource.Categories where categoryType.Id == categoryFaq.Parent.Id select faq);

                model = model.DistinctBy(x => x.Id).ToList();



            }
            return View("~/Views/Feature/GenericComponents/PuppiesKittens/FAQs.cshtml", model.Take(10));
        }

        public ActionResult InteractiveChecklist()
        {
            var model = GetDataSourceItem<InteractiveChecklist>();
            return View("~/Views/Feature/GenericComponents/PuppiesKittens/InteractiveChecklist.cshtml", model);
        }

        public ActionResult InteractiveImage()
        {
            var dataSource = GetDataSourceItem<InteractiveImageDatasource>(inferType: true);
            InteractiveImage model = null;
            if (dataSource != null)
            {
                model = dataSource.Local ? dataSource.InteractiveImages?.FirstOrDefault() : dataSource.InteractiveImage;
            }
            return View("~/Views/Feature/GenericComponents/PuppiesKittens/InteractiveImage.cshtml", model);
        }

        public ActionResult ContentBlockList()
        {
            var model = GetDataSourceItem<ContentBlockList>();
            return View("~/Views/Feature/GenericComponents/PuppiesKittens/ContentBlockList.cshtml", model);
        }

        public ActionResult ContentBlockButton()
        {
            var model = GetDataSourceItem<ContentBlockButton>();
            return View("~/Views/Feature/GenericComponents/PuppiesKittens/ContentBlockButton.cshtml", model);
        }

        public ActionResult ContentBlockVideo()
        {
            var model = GetDataSourceItem<ContentBlockVideo>();
            return View("~/Views/Feature/GenericComponents/PuppiesKittens/ContentBlockVideo.cshtml", model);
        }

        public ActionResult ContentBlockDetails()
        {
            var model = GetDataSourceItem<ContentBlockDetails>();
            return View("~/Views/Feature/GenericComponents/PuppiesKittens/ContentBlockDetails.cshtml", model);
        }

        public ActionResult QuizQuestion()
        {
            var model = GetDataSourceItem<QuizSelector>();
            if (model?.QuizFolder != null) model.QuizFolder.Questions = GetQuestions(model);

            return View("~/Views/Feature/GenericComponents/PuppiesKittens/QuizQuestion.cshtml", model);
        }

        public ActionResult QuizQuestionTwo()
        {
            var model = GetDataSourceItem<QuizSelector>();
            if (model?.QuizFolder != null) model.QuizFolder.Questions = GetQuestions(model);

            return View("~/Views/Feature/GenericComponents/PuppiesKittens/QuizQuestionTwo.cshtml", model);
        }

        private List<QuizQuestion> GetQuestions(QuizSelector model)
        {
            var questions = new List<QuizQuestion>();

            var modelChildren = model.QuizFolder.Children?.ToList();
            if (modelChildren == null || !modelChildren.Any()) return questions;

            foreach (IGlassBase child in modelChildren)
                if (child is QuizQuestion question) questions.Add(question);

            return questions;
        }

        public ActionResult Timeline()
        {
            var dataSource = GetDataSourceItem<TimelineDatasource>();
            if (dataSource == null)
                return View("~/Views/Feature/GenericComponents/PuppiesKittens/Timeline.cshtml", null);

            List<TimelineGroup> groups = new List<TimelineGroup>();
            var cards = dataSource.Cards.ToList();

            foreach (TimelineCard card in cards)
            {
                var group = card.GroupFolder;
                if (group == null) continue;

                int parentIndex = groups.Select(x => x.Id).ToList().IndexOf(group.Id);
                if (parentIndex == -1)
                {
                    groups.Add(group);
                    parentIndex = groups.Select(x => x.Id).ToList().IndexOf(group.Id);
                }

                groups[parentIndex].Cards.Add(card);
            }

            groups = groups.OrderBy(x => x.OrderNumber).ToList();

            var model = new TimelineViewModel
            {
                DataSourceId = dataSource.Id,
                Groups = groups
            };

            return View("~/Views/Feature/GenericComponents/PuppiesKittens/Timeline.cshtml", model);
        }
    }
}