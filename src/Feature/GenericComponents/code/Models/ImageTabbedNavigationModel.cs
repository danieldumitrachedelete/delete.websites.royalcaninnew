﻿namespace Delete.Feature.GenericComponents.Models
{
    using System.Collections.Generic;

    using Delete.Foundation.DataTemplates.Models.Interfaces;

    public class TabbedLifestagesNavigationModel
    {
        public _TabbedNavigationItem ListingItemSettings { get; set; }

        public IList<_TabbedNavigationItem> Items { get; set; }
    }
}