﻿using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration.Attributes;

namespace Delete.Feature.GenericComponents.Models
{
    public partial class Accordion
    {
        [SitecoreChildren]
        public virtual IEnumerable<AccordionItem> AccordionItems { get; set; }
    }
}