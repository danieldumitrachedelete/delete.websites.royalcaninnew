﻿// <autogenerated>
//   This file was generated by T4 code generator ScriptRunner.tt.
//   Any changes made to this file manually will be lost next time the file is regenerated.
// </autogenerated>

using System;
using Sitecore.Data;
using Sitecore.Common;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Sitecore.Globalization;
using System.ComponentModel;
using Delete.Foundation.DeleteFoundationCore.Models;
using Sitecore.ContentSearch;
using Delete.Feature.GenericComponents;

namespace Delete.Feature.GenericComponents.Models
{
	public partial interface IContentBlockWithTextAndImage : IGlassBase, IContentBlockWithText, IInlineImage
	{
	}

	public static partial class ContentBlockWithTextAndImageConstants {
		public const string TemplateIdString = "{350b4d4f-ae45-467f-99ad-fd16faf1e351}";
		public static readonly ID TemplateId = new ID(TemplateIdString);
		public const string TemplateName = "Content Block With Text And Image";

		public const string ImageFieldId = "{b61d1c9f-7dd9-468e-8ec2-b43f1c4fcaa1}";
	
		public const string ImageFieldName = "Image";
	
	}

	
	/// <summary>
	/// ContentBlockWithTextAndImage
	/// <para>Path: /sitecore/templates/Feature/GenericComponents/Content Block With Text And Image</para>	
	/// <para>ID: {350b4d4f-ae45-467f-99ad-fd16faf1e351}</para>	
	/// </summary>
	[SitecoreType(TemplateId=ContentBlockWithTextAndImageConstants.TemplateIdString)] //, Cachable = true
	public partial class ContentBlockWithTextAndImage  : ContentBlockWithText, IContentBlockWithTextAndImage
	{
		/// <summary>
		/// The Image field.
		/// <para>Field Type: Image</para>		
		/// <para>Field ID: {b61d1c9f-7dd9-468e-8ec2-b43f1c4fcaa1}</para>
		/// </summary>
		[SitecoreField(FieldId = ContentBlockWithTextAndImageConstants.ImageFieldId, FieldName = ContentBlockWithTextAndImageConstants.ImageFieldName)]
		[IndexField(ContentBlockWithTextAndImageConstants.ImageFieldName)]
		public virtual Delete.Foundation.DeleteFoundationCore.Models.ExtendedImage Image  {get; set;}

	
	}
}
