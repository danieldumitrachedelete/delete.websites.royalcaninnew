﻿namespace Delete.Feature.GenericComponents.Models
{
    using System.Collections.Generic;

    using Glass.Mapper.Sc.Configuration.Attributes;

    public partial class TextTabs
    {
        [SitecoreChildren]
        public virtual IEnumerable<TextTab> Tabs { get; set; }
    }
}