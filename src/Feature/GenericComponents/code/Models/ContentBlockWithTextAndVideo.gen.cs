﻿// <autogenerated>
//   This file was generated by T4 code generator ScriptRunner.tt.
//   Any changes made to this file manually will be lost next time the file is regenerated.
// </autogenerated>

using System;
using Sitecore.Data;
using Sitecore.Common;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Sitecore.Globalization;
using System.ComponentModel;
using Delete.Foundation.DeleteFoundationCore.Models;
using Sitecore.ContentSearch;
using Delete.Feature.GenericComponents;

namespace Delete.Feature.GenericComponents.Models
{
	public partial interface IContentBlockWithTextAndVideo : IGlassBase, IContentBlockWithText, IYouTubeVideo
	{
	}

	public static partial class ContentBlockWithTextAndVideoConstants {
		public const string TemplateIdString = "{75224e2a-a046-4142-8515-5c81e8069c07}";
		public static readonly ID TemplateId = new ID(TemplateIdString);
		public const string TemplateName = "Content Block With Text And Video";

		public const string VideoIDFieldId = "{946f77f3-bb2d-4da6-b1e1-3f4ee2aee92b}";
		public const string AutoplayFieldId = "{dde10296-9fba-4ea4-99c4-16ee4c504c4f}";
		public const string FixedWidthFieldId = "{20a32c9c-e3cc-4dc7-a3a5-128751b79658}";
		public const string SEOVideoDescriptionFieldId = "{86167379-5151-41da-a226-a54c1e5d1401}";
		public const string SEOVideoNameFieldId = "{2d07da54-8371-4fee-842e-7d0f50b55396}";
		public const string SplashScreenImageFieldId = "{ff71a692-cd3e-4667-ab22-b00893064289}";
	
		public const string VideoIDFieldName = "Video ID";
		public const string AutoplayFieldName = "Autoplay";
		public const string FixedWidthFieldName = "Fixed Width";
		public const string SEOVideoDescriptionFieldName = "SEO Video Description";
		public const string SEOVideoNameFieldName = "SEO Video Name";
		public const string SplashScreenImageFieldName = "Splash Screen Image";
	
	}

	
	/// <summary>
	/// ContentBlockWithTextAndVideo
	/// <para>Path: /sitecore/templates/Feature/GenericComponents/Content Block With Text And Video</para>	
	/// <para>ID: {75224e2a-a046-4142-8515-5c81e8069c07}</para>	
	/// </summary>
	[SitecoreType(TemplateId=ContentBlockWithTextAndVideoConstants.TemplateIdString)] //, Cachable = true
	public partial class ContentBlockWithTextAndVideo  : ContentBlockWithText, IContentBlockWithTextAndVideo
	{
		/// <summary>
		/// The Video ID field.
		/// <para>Field Type: Single-Line Text</para>		
		/// <para>Field ID: {946f77f3-bb2d-4da6-b1e1-3f4ee2aee92b}</para>
		/// </summary>
		[SitecoreField(FieldId = ContentBlockWithTextAndVideoConstants.VideoIDFieldId, FieldName = ContentBlockWithTextAndVideoConstants.VideoIDFieldName)]
		[IndexField(ContentBlockWithTextAndVideoConstants.VideoIDFieldName)]
		public virtual string VideoID  {get; set;}

		/// <summary>
		/// The Autoplay field.
		/// <para>Field Type: Checkbox</para>		
		/// <para>Field ID: {dde10296-9fba-4ea4-99c4-16ee4c504c4f}</para>
		/// </summary>
		[SitecoreField(FieldId = ContentBlockWithTextAndVideoConstants.AutoplayFieldId, FieldName = ContentBlockWithTextAndVideoConstants.AutoplayFieldName)]
		[IndexField(ContentBlockWithTextAndVideoConstants.AutoplayFieldName)]
		public virtual Boolean Autoplay  {get; set;}

		/// <summary>
		/// The Fixed Width field.
		/// <para>Field Type: Checkbox</para>		
		/// <para>Field ID: {20a32c9c-e3cc-4dc7-a3a5-128751b79658}</para>
		/// </summary>
		[SitecoreField(FieldId = ContentBlockWithTextAndVideoConstants.FixedWidthFieldId, FieldName = ContentBlockWithTextAndVideoConstants.FixedWidthFieldName)]
		[IndexField(ContentBlockWithTextAndVideoConstants.FixedWidthFieldName)]
		public virtual Boolean FixedWidth  {get; set;}

		/// <summary>
		/// The SEO Video Description field.
		/// <para>Field Type: Single-Line Text</para>		
		/// <para>Field ID: {86167379-5151-41da-a226-a54c1e5d1401}</para>
		/// </summary>
		[SitecoreField(FieldId = ContentBlockWithTextAndVideoConstants.SEOVideoDescriptionFieldId, FieldName = ContentBlockWithTextAndVideoConstants.SEOVideoDescriptionFieldName)]
		[IndexField(ContentBlockWithTextAndVideoConstants.SEOVideoDescriptionFieldName)]
		public virtual string SEOVideoDescription  {get; set;}

		/// <summary>
		/// The SEO Video Name field.
		/// <para>Field Type: Single-Line Text</para>		
		/// <para>Field ID: {2d07da54-8371-4fee-842e-7d0f50b55396}</para>
		/// </summary>
		[SitecoreField(FieldId = ContentBlockWithTextAndVideoConstants.SEOVideoNameFieldId, FieldName = ContentBlockWithTextAndVideoConstants.SEOVideoNameFieldName)]
		[IndexField(ContentBlockWithTextAndVideoConstants.SEOVideoNameFieldName)]
		public virtual string SEOVideoName  {get; set;}

		/// <summary>
		/// The Splash Screen Image field.
		/// <para>Field Type: Image</para>		
		/// <para>Field ID: {ff71a692-cd3e-4667-ab22-b00893064289}</para>
		/// </summary>
		[SitecoreField(FieldId = ContentBlockWithTextAndVideoConstants.SplashScreenImageFieldId, FieldName = ContentBlockWithTextAndVideoConstants.SplashScreenImageFieldName)]
		[IndexField(ContentBlockWithTextAndVideoConstants.SplashScreenImageFieldName)]
		public virtual Delete.Foundation.DeleteFoundationCore.Models.ExtendedImage SplashScreenImage  {get; set;}

	
	}
}
