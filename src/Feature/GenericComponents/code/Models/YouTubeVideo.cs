﻿using System;
using Delete.Foundation.DeleteFoundationCore.SchemaOrg;
using Delete.Foundation.DeleteFoundationCore.SchemaOrg.Extensions;
using Delete.Foundation.DeleteFoundationCore.SchemaOrg.Interfaces;

namespace Delete.Feature.GenericComponents.Models
{
    public partial class YouTubeVideo : IMicrodata
    {
        public Thing GetMicrodata(IAuthorResolver authorResolver)
        {
            var splashScreenImage = this.SplashScreenImage.GetMicrodata(authorResolver);

            return new VideoObject
            {
                Id = new Uri(@"https://www.youtube.com/watch?v=" + (this.VideoID ?? string.Empty), UriKind.RelativeOrAbsolute),
                Thumbnail = splashScreenImage,
                ThumbnailUrl = new Uri(this.SplashScreenImage?.Src ?? string.Empty, UriKind.RelativeOrAbsolute),
                Author = authorResolver?.GetAuthor() ?? new Organization(),
                UploadDate = splashScreenImage.UploadDate,
                Name = this.SEOVideoName,
                Description = this.SEOVideoDescription
            };
        }
    }
}