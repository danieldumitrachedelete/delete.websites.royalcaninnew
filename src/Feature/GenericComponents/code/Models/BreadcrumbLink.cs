﻿using System;
using System.Collections.Generic;
using Delete.Foundation.DeleteFoundationCore.SchemaOrg;
using Delete.Foundation.DeleteFoundationCore.SchemaOrg.Interfaces;

namespace Delete.Feature.GenericComponents.Models
{
    public class BreadcrumbLink : IMicrodata
    {
        public string Title { get; set; }

        public string Url { get; set; }

        public bool IsCurrent { get; set; }

        public Thing GetMicrodata(IAuthorResolver authorResolver = null)
        {
            return new BreadcrumbList
            {
                ItemListElement = new List<ListItem>
                {
                    new ListItem
                    {
                        Position = 1,
                        Item = new Thing
                        {
                            Id = new Uri(this.Url ?? string.Empty, UriKind.RelativeOrAbsolute),
                            Name = this.Title ?? string.Empty
                        }
                    }
                }
            };
        }
    }
}
