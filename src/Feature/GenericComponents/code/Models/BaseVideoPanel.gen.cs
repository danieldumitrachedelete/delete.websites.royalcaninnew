﻿// <autogenerated>
//   This file was generated by T4 code generator ScriptRunner.tt.
//   Any changes made to this file manually will be lost next time the file is regenerated.
// </autogenerated>

using System;
using Sitecore.Data;
using Sitecore.Common;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Sitecore.Globalization;
using System.ComponentModel;
using Delete.Foundation.DeleteFoundationCore.Models;
using Sitecore.ContentSearch;
using Delete.Feature.GenericComponents;

namespace Delete.Feature.GenericComponents.Models
{
	public partial interface IBaseVideoPanel : IGlassBase
	{
		Boolean Autoplay  {get; set;}
		Boolean FixedWidth  {get; set;}
		string SEOVideoDescription  {get; set;}
		string SEOVideoName  {get; set;}
		Delete.Foundation.DeleteFoundationCore.Models.ExtendedImage SplashScreenImage  {get; set;}
	}

	public static partial class BaseVideoPanelConstants {
		public const string TemplateIdString = "{868a5125-0581-4db4-8f53-c922c5adacb5}";
		public static readonly ID TemplateId = new ID(TemplateIdString);
		public const string TemplateName = "Base Video Panel";

		public const string AutoplayFieldId = "{dde10296-9fba-4ea4-99c4-16ee4c504c4f}";
		public const string FixedWidthFieldId = "{20a32c9c-e3cc-4dc7-a3a5-128751b79658}";
		public const string SEOVideoDescriptionFieldId = "{86167379-5151-41da-a226-a54c1e5d1401}";
		public const string SEOVideoNameFieldId = "{2d07da54-8371-4fee-842e-7d0f50b55396}";
		public const string SplashScreenImageFieldId = "{ff71a692-cd3e-4667-ab22-b00893064289}";
	
		public const string AutoplayFieldName = "Autoplay";
		public const string FixedWidthFieldName = "Fixed Width";
		public const string SEOVideoDescriptionFieldName = "SEO Video Description";
		public const string SEOVideoNameFieldName = "SEO Video Name";
		public const string SplashScreenImageFieldName = "Splash Screen Image";
	
	}

	
	/// <summary>
	/// BaseVideoPanel
	/// <para>Path: /sitecore/templates/Feature/GenericComponents/Base Video Panel</para>	
	/// <para>ID: {868a5125-0581-4db4-8f53-c922c5adacb5}</para>	
	/// </summary>
	[SitecoreType(TemplateId=BaseVideoPanelConstants.TemplateIdString)] //, Cachable = true
	public partial class BaseVideoPanel  : GlassBase, IBaseVideoPanel
	{
		/// <summary>
		/// The Autoplay field.
		/// <para>Field Type: Checkbox</para>		
		/// <para>Field ID: {dde10296-9fba-4ea4-99c4-16ee4c504c4f}</para>
		/// </summary>
		[SitecoreField(FieldId = BaseVideoPanelConstants.AutoplayFieldId, FieldName = BaseVideoPanelConstants.AutoplayFieldName)]
		[IndexField(BaseVideoPanelConstants.AutoplayFieldName)]
		public virtual Boolean Autoplay  {get; set;}

		/// <summary>
		/// The Fixed Width field.
		/// <para>Field Type: Checkbox</para>		
		/// <para>Field ID: {20a32c9c-e3cc-4dc7-a3a5-128751b79658}</para>
		/// </summary>
		[SitecoreField(FieldId = BaseVideoPanelConstants.FixedWidthFieldId, FieldName = BaseVideoPanelConstants.FixedWidthFieldName)]
		[IndexField(BaseVideoPanelConstants.FixedWidthFieldName)]
		public virtual Boolean FixedWidth  {get; set;}

		/// <summary>
		/// The SEO Video Description field.
		/// <para>Field Type: Single-Line Text</para>		
		/// <para>Field ID: {86167379-5151-41da-a226-a54c1e5d1401}</para>
		/// </summary>
		[SitecoreField(FieldId = BaseVideoPanelConstants.SEOVideoDescriptionFieldId, FieldName = BaseVideoPanelConstants.SEOVideoDescriptionFieldName)]
		[IndexField(BaseVideoPanelConstants.SEOVideoDescriptionFieldName)]
		public virtual string SEOVideoDescription  {get; set;}

		/// <summary>
		/// The SEO Video Name field.
		/// <para>Field Type: Single-Line Text</para>		
		/// <para>Field ID: {2d07da54-8371-4fee-842e-7d0f50b55396}</para>
		/// </summary>
		[SitecoreField(FieldId = BaseVideoPanelConstants.SEOVideoNameFieldId, FieldName = BaseVideoPanelConstants.SEOVideoNameFieldName)]
		[IndexField(BaseVideoPanelConstants.SEOVideoNameFieldName)]
		public virtual string SEOVideoName  {get; set;}

		/// <summary>
		/// The Splash Screen Image field.
		/// <para>Field Type: Image</para>		
		/// <para>Field ID: {ff71a692-cd3e-4667-ab22-b00893064289}</para>
		/// </summary>
		[SitecoreField(FieldId = BaseVideoPanelConstants.SplashScreenImageFieldId, FieldName = BaseVideoPanelConstants.SplashScreenImageFieldName)]
		[IndexField(BaseVideoPanelConstants.SplashScreenImageFieldName)]
		public virtual Delete.Foundation.DeleteFoundationCore.Models.ExtendedImage SplashScreenImage  {get; set;}

	
	}
}
