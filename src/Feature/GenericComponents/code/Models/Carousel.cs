﻿using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration.Attributes;

namespace Delete.Feature.GenericComponents.Models
{
    public partial class Carousel
    {
        [SitecoreChildren]
        public virtual IEnumerable<CarouselSlide> Slides { get; set; }
    }
}