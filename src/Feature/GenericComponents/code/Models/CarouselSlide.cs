﻿using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Models;
using Glass.Mapper.Sc.Fields;

namespace Delete.Feature.GenericComponents.Models
{
    using Sitecore.Collections;

    public partial class CarouselSlide
    {
        public string ExtendedSubtitle => this.Subtitle.OrDefault(this.Article == null ? string.Empty : this.Article.Date.ToString("dd MMM yy"));

        public string ExtendedTitle => this.Title.OrDefault(this.Article?.Heading);

        public string Link => this.Article == null ? this.CTALink?.BuildUrl(new SafeDictionary<string>()) : this.Article.Url;

        public string LinkText => this.CTAText.NotEmpty() ? this.CTAText : this.CTALink?.Text;

        public bool IsVideo => (this.Video?.Src ?? this.YoutubeVideoId).NotEmpty();

        public ExtendedImage ExtendedImage => this.Article?.ThumbnailImage ?? this.Image;
    }
}