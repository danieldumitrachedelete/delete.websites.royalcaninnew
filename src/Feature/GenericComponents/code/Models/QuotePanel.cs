﻿using System;
using Delete.Foundation.DeleteFoundationCore.SchemaOrg;
using Delete.Foundation.DeleteFoundationCore.SchemaOrg.Interfaces;

namespace Delete.Feature.GenericComponents.Models
{
    public partial class QuotePanel : IMicrodata
    {
        public Thing GetMicrodata(IAuthorResolver authorResolver = null)
        {
            return new Quotation
            {
                Text = this.Quote ?? string.Empty,
                Creator = new Person
                {
                    Name = this.Author ?? string.Empty
                }
            };
        }
    }
}