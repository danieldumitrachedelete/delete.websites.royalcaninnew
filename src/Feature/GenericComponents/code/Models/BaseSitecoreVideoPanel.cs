﻿using System;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.SchemaOrg;
using Delete.Foundation.DeleteFoundationCore.SchemaOrg.Extensions;
using Delete.Foundation.DeleteFoundationCore.SchemaOrg.Interfaces;

namespace Delete.Feature.GenericComponents.Models
{
    public partial class BaseSitecoreVideoPanel : IMicrodata
    {
        public Thing GetMicrodata(IAuthorResolver authorResolver)
        {
            return new VideoObject
            {
                Id = new Uri(Video?.Src ?? String.Empty, UriKind.RelativeOrAbsolute),
                Thumbnail = SplashScreenImage.GetMicrodata(authorResolver),
                ThumbnailUrl = new Uri(SplashScreenImage?.Src ?? String.Empty, UriKind.RelativeOrAbsolute),
                Author = authorResolver?.GetAuthor() ?? new Organization(),
                UploadDate = (Video?.DateUpdated ?? new DateTime()).ToDateTimeOffset(),
                Name = SEOVideoName,
                Description = SEOVideoDescription
            };
        }
    }
}