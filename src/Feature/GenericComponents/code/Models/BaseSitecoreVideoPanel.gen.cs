﻿// <autogenerated>
//   This file was generated by T4 code generator ScriptRunner.tt.
//   Any changes made to this file manually will be lost next time the file is regenerated.
// </autogenerated>

using System;
using Sitecore.Data;
using Sitecore.Common;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Sitecore.Globalization;
using System.ComponentModel;
using Delete.Foundation.DeleteFoundationCore.Models;
using Sitecore.ContentSearch;
using Delete.Feature.GenericComponents;

namespace Delete.Feature.GenericComponents.Models
{
	public partial interface IBaseSitecoreVideoPanel : IGlassBase, global::Delete.Feature.GenericComponents.Models.IBaseVideoPanel
	{
		Boolean Muted  {get; set;}
		Delete.Foundation.DeleteFoundationCore.Models.ExtendedFile Video  {get; set;}
	}

	public static partial class BaseSitecoreVideoPanelConstants {
		public const string TemplateIdString = "{d80bfb62-2ead-49aa-8a3d-f1516a20a09a}";
		public static readonly ID TemplateId = new ID(TemplateIdString);
		public const string TemplateName = "Base Sitecore Video Panel";

		public const string MutedFieldId = "{ceccef66-9515-493a-9855-2fce383fc13f}";
		public const string VideoFieldId = "{ba6e7b4b-3b1b-4d80-ae16-7c21ba8a41d4}";
	
		public const string MutedFieldName = "Muted";
		public const string VideoFieldName = "Video";
	
	}

	
	/// <summary>
	/// BaseSitecoreVideoPanel
	/// <para>Path: /sitecore/templates/Feature/GenericComponents/Base Sitecore Video Panel</para>	
	/// <para>ID: {d80bfb62-2ead-49aa-8a3d-f1516a20a09a}</para>	
	/// </summary>
	[SitecoreType(TemplateId=BaseSitecoreVideoPanelConstants.TemplateIdString)] //, Cachable = true
	public partial class BaseSitecoreVideoPanel  : global::Delete.Feature.GenericComponents.Models.BaseVideoPanel, IBaseSitecoreVideoPanel
	{
		/// <summary>
		/// The Muted field.
		/// <para>Field Type: Checkbox</para>		
		/// <para>Field ID: {ceccef66-9515-493a-9855-2fce383fc13f}</para>
		/// </summary>
		[SitecoreField(FieldId = BaseSitecoreVideoPanelConstants.MutedFieldId, FieldName = BaseSitecoreVideoPanelConstants.MutedFieldName)]
		[IndexField(BaseSitecoreVideoPanelConstants.MutedFieldName)]
		public virtual Boolean Muted  {get; set;}

		/// <summary>
		/// The Video field.
		/// <para>Field Type: File</para>		
		/// <para>Field ID: {ba6e7b4b-3b1b-4d80-ae16-7c21ba8a41d4}</para>
		/// </summary>
		[SitecoreField(FieldId = BaseSitecoreVideoPanelConstants.VideoFieldId, FieldName = BaseSitecoreVideoPanelConstants.VideoFieldName)]
		[IndexField(BaseSitecoreVideoPanelConstants.VideoFieldName)]
		public virtual Delete.Foundation.DeleteFoundationCore.Models.ExtendedFile Video  {get; set;}

	
	}
}
