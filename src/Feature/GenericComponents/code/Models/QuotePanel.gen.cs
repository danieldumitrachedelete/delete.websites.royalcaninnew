﻿// <autogenerated>
//   This file was generated by T4 code generator ScriptRunner.tt.
//   Any changes made to this file manually will be lost next time the file is regenerated.
// </autogenerated>

using System;
using Sitecore.Data;
using Sitecore.Common;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Sitecore.Globalization;
using System.ComponentModel;
using Delete.Foundation.DeleteFoundationCore.Models;
using Sitecore.ContentSearch;
using Delete.Feature.GenericComponents;

namespace Delete.Feature.GenericComponents.Models
{
	public partial interface IQuotePanel : IGlassBase
	{
		string Author  {get; set;}
		Delete.Foundation.DeleteFoundationCore.Models.ExtendedImage AuthorImage  {get; set;}
		string Quote  {get; set;}
		Glass.Mapper.Sc.Fields.Link QuoteUrl  {get; set;}
	}

	public static partial class QuotePanelConstants {
		public const string TemplateIdString = "{98f5cd0e-6499-4b20-8960-5be016682a07}";
		public static readonly ID TemplateId = new ID(TemplateIdString);
		public const string TemplateName = "Quote Panel";

		public const string AuthorFieldId = "{9783002e-5768-458c-9f95-84640610ecab}";
		public const string AuthorImageFieldId = "{ee588587-ff76-432c-a2a0-f038f44968a7}";
		public const string QuoteFieldId = "{fe1af1f6-d76e-41d6-8571-699d79f94023}";
		public const string QuoteUrlFieldId = "{9aee3a30-306a-47e2-a4fb-2fd88543a95e}";
	
		public const string AuthorFieldName = "Author";
		public const string AuthorImageFieldName = "Author Image";
		public const string QuoteFieldName = "Quote";
		public const string QuoteUrlFieldName = "Quote Url";
	
	}

	
	/// <summary>
	/// QuotePanel
	/// <para>Path: /sitecore/templates/Feature/GenericComponents/Quote Panel</para>	
	/// <para>ID: {98f5cd0e-6499-4b20-8960-5be016682a07}</para>	
	/// </summary>
	[SitecoreType(TemplateId=QuotePanelConstants.TemplateIdString)] //, Cachable = true
	public partial class QuotePanel  : GlassBase, IQuotePanel
	{
		/// <summary>
		/// The Author field.
		/// <para>Field Type: Single-Line Text</para>		
		/// <para>Field ID: {9783002e-5768-458c-9f95-84640610ecab}</para>
		/// </summary>
		[SitecoreField(FieldId = QuotePanelConstants.AuthorFieldId, FieldName = QuotePanelConstants.AuthorFieldName)]
		[IndexField(QuotePanelConstants.AuthorFieldName)]
		public virtual string Author  {get; set;}

		/// <summary>
		/// The Author Image field.
		/// <para>Field Type: Image</para>		
		/// <para>Field ID: {ee588587-ff76-432c-a2a0-f038f44968a7}</para>
		/// </summary>
		[SitecoreField(FieldId = QuotePanelConstants.AuthorImageFieldId, FieldName = QuotePanelConstants.AuthorImageFieldName)]
		[IndexField(QuotePanelConstants.AuthorImageFieldName)]
		public virtual Delete.Foundation.DeleteFoundationCore.Models.ExtendedImage AuthorImage  {get; set;}

		/// <summary>
		/// The Quote field.
		/// <para>Field Type: Single-Line Text</para>		
		/// <para>Field ID: {fe1af1f6-d76e-41d6-8571-699d79f94023}</para>
		/// </summary>
		[SitecoreField(FieldId = QuotePanelConstants.QuoteFieldId, FieldName = QuotePanelConstants.QuoteFieldName)]
		[IndexField(QuotePanelConstants.QuoteFieldName)]
		public virtual string Quote  {get; set;}

		/// <summary>
		/// The Quote Url field.
		/// <para>Field Type: General Link</para>		
		/// <para>Field ID: {9aee3a30-306a-47e2-a4fb-2fd88543a95e}</para>
		/// </summary>
		[SitecoreField(FieldId = QuotePanelConstants.QuoteUrlFieldId, FieldName = QuotePanelConstants.QuoteUrlFieldName)]
		[IndexField(QuotePanelConstants.QuoteUrlFieldName)]
		public virtual Glass.Mapper.Sc.Fields.Link QuoteUrl  {get; set;}

	
	}
}
