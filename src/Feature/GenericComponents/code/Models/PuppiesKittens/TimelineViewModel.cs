﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Delete.Feature.Articles.Models;

namespace Delete.Feature.GenericComponents.Models.PuppiesKittens
{
    public class TimelineViewModel
    {
        public Guid DataSourceId { get; set; }
        public List<TimelineGroup> Groups { get; set; }
        public List<Article> Articles { get; set; }
    }
}