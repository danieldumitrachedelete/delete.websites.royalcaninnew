﻿using System;
using System.Collections.Generic;

namespace Delete.Feature.GenericComponents.Models.PuppiesKittens
{
    public class QuickTipsGrouper
    {
        private int _index;

        private QuickTip[] _group;
        private QuickTipsGrouper _next;

        public QuickTipsGrouper(int columnCount)
        {
            if (columnCount < 1)
                throw new ArgumentOutOfRangeException(nameof(columnCount), "Value cannot be less then 1");

            _group = new QuickTip[columnCount];
            _index = 0;
        }

        public void AddTip(QuickTip tip)
        {
            if (_index == _group.Length)
            {
                if (_next == null)
                    _next = new QuickTipsGrouper(_group.Length);

                _next.AddTip(tip);
            }
            else
            {
                _group[_index++] = tip;
            }
        }

        public List<QuickTip> Tips
        {
            get
            {
                List<QuickTip> result = new List<QuickTip>();
                for (int i = 0; i < _index; i++)
                    result.Add(_group[i]);

                return result;
            }
        }

        public IEnumerable<QuickTipsGrouper> Groups
        {
            get
            {
                QuickTipsGrouper current = this;
                while (current != null)
                {
                    yield return current;
                    current = current._next;
                }
            }
        }
    }
}