﻿using System;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration.Attributes;

namespace Delete.Feature.GenericComponents.Models.PuppiesKittens
{
    public partial class InteractiveImage
    {
        [SitecoreChildren]
        public virtual IEnumerable<InteractiveImageNote> InteractiveImageNotes { get; set; }
    }
}