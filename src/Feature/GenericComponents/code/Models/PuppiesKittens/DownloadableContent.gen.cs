﻿// <autogenerated>
//   This file was generated by T4 code generator ScriptRunner.tt.
//   Any changes made to this file manually will be lost next time the file is regenerated.
// </autogenerated>

using System;
using Sitecore.Data;
using Sitecore.Common;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Sitecore.Globalization;
using System.ComponentModel;
using Delete.Foundation.DeleteFoundationCore.Models;
using Sitecore.ContentSearch;
using Delete.Feature.GenericComponents;

namespace Delete.Feature.GenericComponents.Models.PuppiesKittens
{
	public partial interface IDownloadableContent : IGlassBase
	{
		Glass.Mapper.Sc.Fields.Link FileToDownload  {get; set;}
		string Header  {get; set;}
		Delete.Foundation.DeleteFoundationCore.Models.ExtendedImage Image  {get; set;}
		string TextPreview  {get; set;}
	}

	public static partial class DownloadableContentConstants {
		public const string TemplateIdString = "{d5fd289a-2e93-4f09-a42a-c7ca7448490c}";
		public static readonly ID TemplateId = new ID(TemplateIdString);
		public const string TemplateName = "Downloadable content";

		public const string FileToDownloadFieldId = "{24562cbc-4085-48cc-aa41-9f6654fa6836}";
		public const string HeaderFieldId = "{18530f2e-12c7-49bb-9db4-57243bb79527}";
		public const string ImageFieldId = "{ead3e8d4-dab7-4b4e-8b93-883f7926a3fe}";
		public const string TextPreviewFieldId = "{9939282b-b339-4a2d-9d8d-4b7ac04f9805}";
	
		public const string FileToDownloadFieldName = "File to download";
		public const string HeaderFieldName = "Header";
		public const string ImageFieldName = "Image";
		public const string TextPreviewFieldName = "Text preview";
	
	}

	
	/// <summary>
	/// DownloadableContent
	/// <para>Path: /sitecore/templates/Feature/GenericComponents/PuppiesKittens/Downloadable content</para>	
	/// <para>ID: {d5fd289a-2e93-4f09-a42a-c7ca7448490c}</para>	
	/// </summary>
	[SitecoreType(TemplateId=DownloadableContentConstants.TemplateIdString)] //, Cachable = true
	public partial class DownloadableContent  : GlassBase, IDownloadableContent
	{
		/// <summary>
		/// The File to download field.
		/// <para>Field Type: General Link</para>		
		/// <para>Field ID: {24562cbc-4085-48cc-aa41-9f6654fa6836}</para>
		/// </summary>
		[SitecoreField(FieldId = DownloadableContentConstants.FileToDownloadFieldId, FieldName = DownloadableContentConstants.FileToDownloadFieldName)]
		[IndexField(DownloadableContentConstants.FileToDownloadFieldName)]
		public virtual Glass.Mapper.Sc.Fields.Link FileToDownload  {get; set;}

		/// <summary>
		/// The Header field.
		/// <para>Field Type: Single-Line Text</para>		
		/// <para>Field ID: {18530f2e-12c7-49bb-9db4-57243bb79527}</para>
		/// </summary>
		[SitecoreField(FieldId = DownloadableContentConstants.HeaderFieldId, FieldName = DownloadableContentConstants.HeaderFieldName)]
		[IndexField(DownloadableContentConstants.HeaderFieldName)]
		public virtual string Header  {get; set;}

		/// <summary>
		/// The Image field.
		/// <para>Field Type: Image</para>		
		/// <para>Field ID: {ead3e8d4-dab7-4b4e-8b93-883f7926a3fe}</para>
		/// </summary>
		[SitecoreField(FieldId = DownloadableContentConstants.ImageFieldId, FieldName = DownloadableContentConstants.ImageFieldName)]
		[IndexField(DownloadableContentConstants.ImageFieldName)]
		public virtual Delete.Foundation.DeleteFoundationCore.Models.ExtendedImage Image  {get; set;}

		/// <summary>
		/// The Text preview field.
		/// <para>Field Type: Multi-Line Text</para>		
		/// <para>Field ID: {9939282b-b339-4a2d-9d8d-4b7ac04f9805}</para>
		/// </summary>
		[SitecoreField(FieldId = DownloadableContentConstants.TextPreviewFieldId, FieldName = DownloadableContentConstants.TextPreviewFieldName)]
		[IndexField(DownloadableContentConstants.TextPreviewFieldName)]
		public virtual string TextPreview  {get; set;}

	
	}
}
