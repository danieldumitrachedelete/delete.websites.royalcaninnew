﻿using System.Collections.Generic;
using Delete.Foundation.DeleteFoundationCore.Models;

namespace Delete.Feature.GenericComponents.Models.PuppiesKittens
{
    public partial class FeatureContentBlock
    {
        public IEnumerable<IGlassBase> AdditionalButtons { get; set; }
    }
}