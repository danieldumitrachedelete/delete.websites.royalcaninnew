﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Delete.Feature.GenericComponents.Models.PuppiesKittens
{
    public partial class QuizFolder
    {
        public List<QuizQuestion> Questions { get; set; }
    }
}