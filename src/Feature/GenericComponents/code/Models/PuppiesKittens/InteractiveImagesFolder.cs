﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Delete.Foundation.DataTemplates.Models;
using Glass.Mapper.Sc.Configuration.Attributes;

namespace Delete.Feature.GenericComponents.Models.PuppiesKittens
{
    public partial class InteractiveImagesFolder
    {
        [SitecoreChildren]
        public virtual IEnumerable<InteractiveImage> InteractiveImages { get; set; }
    }
}