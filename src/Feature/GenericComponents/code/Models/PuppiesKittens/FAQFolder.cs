﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Glass.Mapper.Sc.Configuration.Attributes;

namespace Delete.Feature.GenericComponents.Models.PuppiesKittens
{
    public partial class FAQFolder
    {
        [SitecoreChildren]
        public virtual IEnumerable<FAQ> FAQItems { get; set; }
    }
}