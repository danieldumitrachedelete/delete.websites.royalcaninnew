﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Delete.Feature.GenericComponents.Models.PuppiesKittens
{
    public partial class TimelineGroup
    {
        public List<TimelineCard> Cards { get; set; } = new List<TimelineCard>();
    }
}