﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Glass.Mapper.Sc.Configuration.Attributes;

namespace Delete.Feature.GenericComponents.Models.PuppiesKittens
{
    public partial class InteractiveImageDatasource
    {
        [SitecoreChildren]
        public virtual IEnumerable<InteractiveImage> InteractiveImages { get; set; }
    }
}