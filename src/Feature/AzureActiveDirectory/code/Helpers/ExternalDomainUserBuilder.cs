﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Sitecore.Diagnostics;
using Sitecore.Owin.Authentication.Identity;
using Sitecore.Owin.Authentication.Services;
using System;
using System.Linq;

namespace Delete.Feature.AzureActiveDirectory.Helpers
{
	public class ExternalDomainUserBuilder : DefaultExternalUserBuilder
	{
		public ExternalDomainUserBuilder(string isPersistentUser) : base(bool.Parse(isPersistentUser)) { }


		//protected virtual string CreateUniqueUserName(Microsoft.AspNet.Identity.UserManager<ApplicationUser> userManager, Microsoft.AspNet.Identity.Owin.ExternalLoginInfo externalLoginInfo);
		protected override string CreateUniqueUserName(UserManager<ApplicationUser> userManager, ExternalLoginInfo externalLoginInfo)
		{
			Assert.ArgumentNotNull((object)userManager, nameof(userManager));
			Assert.ArgumentNotNull((object)externalLoginInfo, nameof(externalLoginInfo));
			var identityProvider = this.FederatedAuthenticationConfiguration.GetIdentityProvider(externalLoginInfo.ExternalIdentity);
			if (identityProvider == null)
				throw new InvalidOperationException("Unable to retrieve identity provider for given identity");
			var domain = identityProvider.Domain;

			var name = externalLoginInfo.ExternalIdentity.Claims.Where(c => c.Type == "email")
				.Select(c => c.Value).SingleOrDefault();
			if (string.IsNullOrEmpty(name))
			{
				return domain + "\\" + externalLoginInfo.DefaultUserName;
			}
			else
			{
				name = name.Replace(",", "");
			}
			return domain + "\\" + name;
		}
	}
}