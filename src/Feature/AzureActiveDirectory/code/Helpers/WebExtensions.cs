﻿using System;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Web;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.Abstractions;
using Sitecore.Data;
using Sitecore.DependencyInjection;
using Sitecore.Pipelines.GetSignInUrlInfo;

namespace Delete.Feature.AzureActiveDirectory.Helpers
{
    public static class WebExtensions
    {
        public static void RedirectToAzureSSO()
        {
            var args = new GetSignInUrlInfoArgs(Sitecore.Context.Site.Name, HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host);
            var corePipelineManager = ServiceLocator.ServiceProvider.GetRequiredService<BaseCorePipelineManager>();
            GetSignInUrlInfoPipeline.Run(corePipelineManager, args);
            var signInUrlInfos = args.Result.Select(PrepareSignInInfo);
            var postParams = new NameValueCollection();

     
            var postBackUrl = signInUrlInfos.First().Href;

            RedirectWithData(postParams, postBackUrl);
        }

        public static void RedirectWithData(NameValueCollection data, string url)
        {
            var response = HttpContext.Current.Response;
            response.Clear();

            var s = new StringBuilder();
            s.Append("<html>");
            s.Append("<head>");
            s.Append("<script type = \"text/javascript\">//<![CDATA[" + "\n" +
                       "var theForm = document.forms['form1'];" +
                        "if (!theForm)" +
                        "{ theForm = document.form1; }" +
                        "function __doPostBack(eventTarget, eventArgument)" +
                        "{if (!theForm.onsubmit || (theForm.onsubmit() != false))" +
                        "{theForm.__EVENTTARGET.value = eventTarget;" +
                        "theForm.__EVENTARGUMENT.value = eventArgument;" +
                        "theForm.submit();}} " +
                        "//]]> </script>");

            s.Append("<script src =\"/WebResource.axd?d=pynGkmcFUV13He1Qd6_TZJIv0wC1bpTb_1ZVqaY-iyuALcXGMKWKNgAPHdxGsZQd3WeLwWZkvNU5zdVSQ5qxFw2&amp;t=636686402738678653\" type = \"text/javascript\" ></script>");
            s.Append("<script type=\"text/javascript\" src=\"/Content/MarketingPortal/scripts/WebForm_Script.js\"></script>");
            s.Append("<script>function SubmitForm(){ alert('1');document.getElementById('signOn').click();}</script></head>");
            s.AppendFormat("<body onload='SubmitForm()'>");
            s.AppendFormat("<form name='form'>");
            foreach (string key in data)
            {
                s.AppendFormat("<input type='hidden' name='{0}' value='{1}' />", key, data[key]);
                
            }

            s.AppendFormat("<a id='signOn' class=\"btn btn-default btn-block RC.sc.azureAD\" href='javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(\"ExternalSignIn$ctl00$ctl00\", \"\", false, \"\", \"{0}\", false, true))'>Sign-in</a>", url);
            s.Append("</form></body></html>");
            response.Write(s.ToString());
            response.End();
        }

        internal static SignInUrlInfo PrepareSignInInfo(SignInUrlInfo info)
        {
            var index = info.Href.IndexOf("?", StringComparison.Ordinal);
            var href = info.Href;
            if (index > -1)
            {
                var values = HttpUtility.ParseQueryString(info.Href.Substring(index + 1));
                var values2 = HttpUtility.ParseQueryString(string.Empty);
                foreach (string str2 in values.Keys)
                {
                    values2[str2] = HttpUtility.UrlEncode(values[str2]);
                }
                href = info.Href.Substring(0, index + 1) + values2;
            }
            return new SignInUrlInfo(info.IdentityProvider, href, info.Caption, info.Icon);
        }
    }
}