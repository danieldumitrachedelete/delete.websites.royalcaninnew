using System;
using System.Globalization;
using System.IdentityModel.Claims;
using System.Threading.Tasks;
using System.Web;
using Delete.Feature.AzureActiveDirectory.Helpers;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Notifications;
using Microsoft.Owin.Security.OpenIdConnect;
using Owin;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Diagnostics;
using Sitecore.Owin.Authentication.Configuration;
using Sitecore.Owin.Authentication.Pipelines.IdentityProviders;
using Sitecore.Owin.Authentication.Services;

namespace Delete.Feature.AzureActiveDirectory.Pipeline
{
    public class CustomAzureADIdentityProvider : IdentityProvidersProcessor
    {

        public CustomAzureADIdentityProvider(FederatedAuthenticationConfiguration federatedAuthenticationConfiguration) : base(federatedAuthenticationConfiguration)
        {
        }

        protected override string IdentityProviderName => "RC.sc.azureAD";

        protected override void ProcessCore([NotNull] IdentityProvidersArgs args)
        {
            Assert.ArgumentNotNull(args, nameof(args));

            var identityProvider = this.GetIdentityProvider();
            var authenticationType = this.GetAuthenticationType();

            var aadInstance = Settings.GetSetting("AADInstance");
            var tenant = Settings.GetSetting("Tenant");
            var clientId = Settings.GetSetting("ClientId");
            var postLogoutRedirectUri = Settings.GetSetting("PostLogoutRedirectURI");
            var redirectUri = Settings.GetSetting("RedirectURI");

            var authority = string.Format(CultureInfo.InvariantCulture, aadInstance, tenant);

            args.App.UseOpenIdConnectAuthentication(new OpenIdConnectAuthenticationOptions
            {
                Caption = identityProvider.Caption,
                AuthenticationType = authenticationType,
                AuthenticationMode = AuthenticationMode.Passive,
                ClientId = clientId,
                Authority = authority,
                PostLogoutRedirectUri = postLogoutRedirectUri,
                RedirectUri = redirectUri,

                Notifications = new OpenIdConnectAuthenticationNotifications
                {
                    SecurityTokenValidated = notification =>
                    {
                        var identity = notification.AuthenticationTicket.Identity;

                        foreach (var claimTransformationService in identityProvider.Transformations)
                        {
                            claimTransformationService.Transform(identity, new TransformationContext(FederatedAuthenticationConfiguration, identityProvider));
                        }

                        notification.AuthenticationTicket = new AuthenticationTicket(identity, notification.AuthenticationTicket.Properties);

                        return Task.CompletedTask;
                    },
                    AuthorizationCodeReceived = OnAuthorization,
                    AuthenticationFailed = OnAuthenticationFailed

                }
            });
        }

        private async Task OnAuthorization(AuthorizationCodeReceivedNotification context)
        {
            var code = context.Code;

            try
            {
                var signedInUserId = context.AuthenticationTicket.Identity.FindFirst(ClaimTypes.NameIdentifier).Value;
                Log.Info("Authorization Code received -->" + code, this);
                Log.Info("Authorization Code received for user -->" + signedInUserId, this);
            }
            catch(Exception ex)
            {
                Log.Error("An error occured --> " + ex.Message, this);
            }

            try
            {
                var aadInstance = Settings.GetSetting("AADInstance");
                var tenant = Settings.GetSetting("Tenant");
                var clientId = Settings.GetSetting("ClientId");
                var clientSecret = Settings.GetSetting("ClientSecret");

                var authority = string.Format(CultureInfo.InvariantCulture, aadInstance, tenant);

                var credential = new ClientCredential(clientId, clientSecret);
                var userObjectId = context.AuthenticationTicket.Identity.FindFirst("http://schemas.microsoft.com/identity/claims/objectidentifier").Value;

                var tokenCache = new NaiveSessionCache(userObjectId, context.OwinContext.Environment["System.Web.HttpContextBase"] as HttpContextBase);

                var authContext = new AuthenticationContext(authority, tokenCache);

                // If you create the redirectUri this way, it will contain a trailing slash.  
                // Make sure you've registered the same exact Uri in the Azure Portal (including the slash).
                var uri = new Uri(HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Path));

                var result = await authContext.AcquireTokenByAuthorizationCodeAsync(code, uri, credential);
            }
            catch(Exception ex)
            {
                Log.Error("Exception in getting refresh token -->" + ex.Message, this);
            }
        }

        private Task OnAuthenticationFailed(AuthenticationFailedNotification<Microsoft.IdentityModel.Protocols.OpenIdConnectMessage, OpenIdConnectAuthenticationOptions> notification)
        {
            // If there is a code in the OpenID Connect response, redeem it for an access token and refresh token, and store those away.
            Log.Error("OnAuthentication Failed error", this);
            if (notification?.Exception?.Message != null)
                Log.Error("Authentication failed --> " + notification.Exception.Message, this);

            notification.HandleResponse();
            //notification.Response.Redirect("/FedAuthLogin.aspx");

            return Task.FromResult(0);
        }
    }
}