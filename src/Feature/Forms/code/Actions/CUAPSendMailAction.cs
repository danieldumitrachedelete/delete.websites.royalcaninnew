﻿using Delete.Feature.Forms.Exceptions;
using Delete.Feature.Forms.Extensions;
using Delete.Feature.Forms.Models;
using Delete.Feature.Forms.Models.CUAP;
using Delete.Foundation.DeleteFoundationCore.Helpers;
using FormsSendMail.Forms.Models;
using FormsSendMail.Forms.Models.Templates;
using Glass.Mapper;
using Glass.Mapper.Sc;
using Sitecore.ExperienceForms.Models;
using Sitecore.ExperienceForms.Processing;
using Sitecore.ExperienceForms.Processing.Actions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Delete.Feature.Forms.Actions
{
    public class CUAPSendMailAction : SubmitActionBase<SendMailActionData[]>
    {
        private const string DefaultTwoDigits = "00";

        private SendMailAction defaultSendAction;
        private ISitecoreContext sitecoreContext;

        public CUAPSendMailAction(ISubmitActionData submitActionData) : base(submitActionData)
        {
            this.defaultSendAction = new SendMailAction(submitActionData);
            this.sitecoreContext = new SitecoreContext(Sitecore.Context.Database);
        }

        protected override bool Execute(SendMailActionData[] data, FormSubmitContext formSubmitContext)
        {
            try
            {
                var currectData = this.GetCorrectActionData(data, formSubmitContext);

                var requiredFieldsErrors = CheckValidation(currectData, formSubmitContext);
                if (requiredFieldsErrors.Any())
                {
                    this.SubmitActionData.ErrorMessage = TranslateHelper.TextByDomainCached("Translations", "translations.feature.forms.pleasefillalltherequiredfields");
                    formSubmitContext.Abort();
                    return false;
                }

                EmailTemplate emailTemplate = defaultSendAction.GetEmailTemplate(currectData);
                CustomEmailTemplate customEmailTemplate = UpdateEmailTemplate(emailTemplate, formSubmitContext);

                return defaultSendAction.CompleteAndSendEmail(customEmailTemplate, formSubmitContext, currectData.ReferenceId);
            }
            catch (NullProfessionException ex)
            {
                this.SubmitActionData.ErrorMessage = TranslateHelper.TextByDomainCached("Translations", "translations.feature.forms.pleasefillalltherequiredfields");
                ((IExtendedFormFieldViewModel)(ex.Field)).CodeError = TranslateHelper.TextByDomainCached("Translations", "translations.feature.forms.isrequired");
                formSubmitContext.Abort();
                return false;
            }
        }

        private CustomEmailTemplate UpdateEmailTemplate(EmailTemplate emailTemplate, FormSubmitContext formSubmitContext)
        {
            if (!Guid.TryParse(emailTemplate.To, out Guid itemId)) return new CustomEmailTemplate(emailTemplate);

            var emailList = sitecoreContext.GetItem<EmailCollection>(itemId);
            string twoPostalDigits = GetFirstTwoPostalDigits(formSubmitContext);

            var localEmail = emailList.EmailList.Get(twoPostalDigits);
            if (localEmail.IsNullOrWhiteSpace())
            {
                localEmail = emailList.EmailList[DefaultTwoDigits];
            }

            var customEmailTemplate = new CustomEmailTemplate(emailTemplate);
            customEmailTemplate.To = localEmail;

            return customEmailTemplate;
        }

        private string GetFirstTwoPostalDigits(FormSubmitContext formSubmitContext)
        {
            var field = formSubmitContext.Fields.FirstOrDefault(x => x.Name == "PostalCode");
            var postal = field.GetStringValue();

            if (postal.IsNullOrEmpty() || postal.Length < 2) return DefaultTwoDigits;

            var twoPostalSymbols = postal.Substring(0, 2);

            return char.IsDigit(twoPostalSymbols[0]) && char.IsDigit(twoPostalSymbols[1])
                ? twoPostalSymbols
                : DefaultTwoDigits;
        }

        private void ValidateFields(List<string> fieldNames, FormSubmitContext formSubmitContext)
        {
            foreach (var fieldName in fieldNames)
            {
                if (formSubmitContext.Fields.FirstOrDefault(x => x.Name == fieldName) is IExtendedFormFieldViewModel field)
                {
                    field.CodeError = TranslateHelper.TextByDomainCached("Translations", "translations.feature.forms.isrequired");
                }
            }
        }

        private List<string> CheckValidation(SendMailActionData data, FormSubmitContext formSubmitContext)
        {
            var professionField = formSubmitContext.Fields.FirstOrDefault(x => x.Name == "Profession");

            if (!(professionField is ListBoxViewModel))
                throw new NullProfessionException(professionField);

            var field = professionField as ListBoxViewModel;

            var attribute = field.WrapperCustomAttributes.Split(' ').FirstOrDefault(x => x.StartsWith("validation-rule-id="));
            if (attribute == null) throw new Exception("validation-rule-id attribute of Profession field is not available");

            var idString = attribute.Split('"')[1];
            var validationSettings = this.sitecoreContext.GetItem<ValidationSettings>(new Guid(idString));
            var requiredFieldsNames = GetRequiredFieldNames(validationSettings, field);

            var result = CheckEveryFieldOf(formSubmitContext, requiredFieldsNames);


            ValidateFields(result, formSubmitContext);

            return result;
        }

        private List<string> CheckEveryFieldOf(FormSubmitContext formSubmitContext, List<string> fieldNames)
        {
            var errorsList = new List<string>();

            foreach (var fieldName in fieldNames)
            {
                var field = formSubmitContext.Fields.FirstOrDefault(x => x.Name == fieldName);
                if (field == null) throw new Exception($"Not found field with similar name {fieldName}");

                var value = field.GetType().GetProperty("Value")?.GetValue(field);

                if (value is string result)
                {
                    if (result.IsNullOrWhiteSpace()) errorsList.Add(fieldName);
                }
                else if (value is List<string> results)
                {
                    if (!results.Any() || results.FirstOrDefault() == null) errorsList.Add(fieldName);
                }
                else if (value == null)
                {
                    errorsList.Add(fieldName);
                }
                else
                {
                    throw new Exception($"Unknown type of field named {fieldName}");
                }
            }

            return errorsList;
        }

        private List<string> GetRequiredFieldNames(ValidationSettings settings, ListBoxViewModel professionField)
        {
            var value = int.Parse(professionField.Value.First());
            switch (value)
            {
                case 1: return settings.StoreFields.Split('|').ToList();
                case 2: return settings.ProfessionalFields.Split('|').ToList();
                case 3: return settings.VeterinaryFields.Split('|').ToList();
                case 4: return settings.GroomerFields.Split('|').ToList();
                case 5: return settings.JournalistFields.Split('|').ToList();
                default: throw new NullProfessionException(professionField);
            }
        }

        private SendMailActionData GetCorrectActionData(SendMailActionData[] data, FormSubmitContext formSubmitContext)
        {
            int index = 1;
            var professionField = formSubmitContext.Fields.FirstOrDefault(x => x.Name == "Profession");
            if (professionField is ListBoxViewModel field)
            {
                index = int.Parse(field.Value.FirstOrDefault() ?? "0");
            }

            if (index >= 1 && index <= data.Length + 1)
            {
                return data[index - 1];
            }

            throw new NullProfessionException(professionField);
        }
    }
}