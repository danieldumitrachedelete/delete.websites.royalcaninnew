﻿using FormsSendMail.Forms.Models;
using FormsSendMail.Forms.Models.Templates;
using Sitecore;
using Sitecore.Data;
using Sitecore.Diagnostics;
using Sitecore.ExperienceForms.Models;
using Sitecore.ExperienceForms.Processing;
using Sitecore.Globalization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;
using Delete.Feature.Forms.Models.CUAP;

namespace Delete.Feature.Forms.Actions
{
    public class SendMailAction : FormsSendMail.Forms.Actions.SendMailAction
    {
        private readonly string _keywordPrefix = Translate.TextByLanguage("Forms.Actions.SendMail.KeywordPrefix", Context.Language, "{");
        private readonly string _keywordSuffix = Translate.TextByLanguage("Forms.Actions.SendMail.KeywordSuffix", Context.Language, "}");

        public SendMailAction(ISubmitActionData submitActionData)
          : base(submitActionData)
        {
        }

        public bool ExecuteOutside(SendMailActionData data, FormSubmitContext formSubmitContext)
        {
            return this.Execute(data, formSubmitContext);
        }

        protected override bool Execute(SendMailActionData data, FormSubmitContext formSubmitContext)
        {
            Assert.ArgumentNotNull(formSubmitContext, nameof(formSubmitContext));

            EmailTemplate emailTemplate = GetEmailTemplate(data);
            if (emailTemplate == null)
                return false;

            return CompleteAndSendEmail(new CustomEmailTemplate(emailTemplate), formSubmitContext, data.ReferenceId);

        }

        public EmailTemplate GetEmailTemplate(SendMailActionData data)
        {
            if (data == null || data.ReferenceId == Guid.Empty)
            {
                return null;
            }

            var obj = Context.Database.GetItem(new ID(data.ReferenceId));
            if (obj == null)
            {
                return null;
            }

            return new EmailTemplate(obj);
        }

        public bool CompleteAndSendEmail(CustomEmailTemplate emailTemplate, FormSubmitContext formSubmitContext, Guid referenceId)
        {
            try
            {
                var message = new MailMessage
                {
                    Subject = this.ExtendedReplaceKeywords(emailTemplate.Subject, formSubmitContext),
                    From = new MailAddress(this.ExtendedReplaceKeywords(emailTemplate.From, formSubmitContext))
                };

                this.FillMailAddressCollection(this.SplitEmails(this.ExtendedReplaceKeywords(emailTemplate.To, formSubmitContext)), message.To);
                this.FillMailAddressCollection(this.SplitEmails(this.ExtendedReplaceKeywords(emailTemplate.Cc, formSubmitContext)), message.CC);
                this.FillMailAddressCollection(this.SplitEmails(this.ExtendedReplaceKeywords(emailTemplate.Bcc, formSubmitContext)), message.Bcc);

                if (!string.IsNullOrEmpty(emailTemplate.MessageRichText))
                {
                    message.Body = this.ExtendedReplaceKeywords(emailTemplate.MessageRichText, formSubmitContext);
                    message.IsBodyHtml = true;
                }
                else
                {
                    message.Body = this.ExtendedReplaceKeywords(emailTemplate.MessageText, formSubmitContext);
                    message.IsBodyHtml = false;
                }

                MainUtil.SendMail(message);
                return true;
            }
            catch (Exception ex)
            {
                Log.Error($"[SendMail Action] Error sending e-mail based on template {referenceId}", ex, this);
                formSubmitContext.Abort();
                return false;
            }
        }

        private string ExtendedReplaceKeywords(string original, FormSubmitContext formSubmitContext)
        {
            var afterReplacingFromFields = this.ReplaceOriginalKeywords(original, formSubmitContext);
            var afterReplacingFromQueryString = this.ReplaceKeywordsFromQueryString(afterReplacingFromFields);

            return afterReplacingFromQueryString;
        }

        private string ReplaceKeywordsFromQueryString(string original)
        {
            var str = original;
            var queryString = HttpContext.Current.Request.QueryString;

            foreach (string key in queryString.Keys)
            {
                string newValue = queryString[key];
                str = str.Replace($"{this._keywordPrefix}{key}{this._keywordSuffix}", newValue);
            }

            return str;
        }

        private String ReplaceOriginalKeywords(string original, FormSubmitContext formSubmitContext)
        {
            var str = original;
            var regex = new Regex(@"\{([^}]+)\}");
            var matches = regex.Matches(original);

            foreach (Match match in matches)
            {
                var matchValue = match.Groups[1].Value;

                var formFieldViewModel = formSubmitContext.Fields.FirstOrDefault(x => x.Name.Equals(matchValue));
                if (formFieldViewModel == null)
                {
                    str = str.Replace(match.Value, string.Empty);
                    continue;
                };

                var formFieldObjectValue = formFieldViewModel.GetType().GetProperty("Value").GetValue(formFieldViewModel);
                if (formFieldObjectValue != null && !string.IsNullOrEmpty(formFieldViewModel.Name))
                {
                    if (formFieldObjectValue is String)
                    {
                        str = str.Replace(match.Value, formFieldObjectValue as String);
                    }
                    else if (formFieldObjectValue is List<String>)
                    {
                        var fieldValueList = formFieldObjectValue as List<String>;

                        var fieldValue = fieldValueList.Count > 0 ? fieldValueList[0] : string.Empty;
                        str = str.Replace(match.Value, fieldValue);
                    }
                }
                else
                {
                    str = str.Replace(match.Value, string.Empty);
                }
            }

            return str;
        }
    }
}
