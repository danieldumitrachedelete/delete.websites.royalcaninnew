﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Delete.Feature.Forms.Extensions;
using Delete.Feature.Forms.ExternalServices.Vital;
using Delete.Feature.Forms.Models;
using Delete.Feature.Forms.Validation;
using Delete.Feature.Forms.Vital.Context;
using Delete.Feature.Forms.Vital.Factories;
using Delete.Feature.Forms.Vital.Interfaces;
using Delete.Feature.Forms.Vital.Profiles;
using Delete.Foundation.DeleteFoundationCore.Helpers;
using log4net;
using Sitecore.ExperienceForms.Models;
using Sitecore.ExperienceForms.Processing;
using Sitecore.ExperienceForms.Processing.Actions;
using Sitecore.Mvc.Extensions;
using Sitecore.StringExtensions;

namespace Delete.Feature.Forms.Actions
{
    public class SendApiRequest : SubmitActionBase<object>
    {
        private IVitalService _vitalService;
        private IMapper mapper;
        private readonly ILog logger = LogManager.GetLogger("RoyalCanin.Feature.Forms");


        public SendApiRequest(ISubmitActionData submitActionData) : base(submitActionData)
        {
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(new VitalApiProfile()));
            this.mapper = configuration.CreateMapper();
            this._vitalService = VitalServiceFactory.GetVitalService();
        }

        protected override bool TryParse(string value, out object target)
        {
            target = string.Empty;
            return true;
        }

        protected override bool Execute(object data, FormSubmitContext formSubmitContext)
        {
            if (!CheckRequiredFields(formSubmitContext) || IsBadDate(formSubmitContext))
            {
                formSubmitContext.Abort();
                return false;
            }

            var formContext = new VitalFormContext(formSubmitContext.Fields);
            var apiContext = mapper.Map<VitalApiContext>(formContext);

            VitalResponsestring result = this._vitalService.SendRequest(apiContext);

            if (result.ThrewException)
                return ProcessBadResult(formSubmitContext, result);

            logger.Info($"VITAL-FORM api worked well");
            logger.Info($"Result: {result.Data}");

            return true;
        }

        private bool CheckRequiredFields(FormSubmitContext formSubmitContext)
        {
            bool result = true;
            Director d = new Director();

            foreach (var field in formSubmitContext.Fields)
            {
                if (field is IExtendedFormFieldViewModel el &&
                    el.WrapperCustomAttributes.ContainsText("server-validation-required"))
                {
                    d.Validate<EmptyFieldValidation>(el);
                    result = result && el.CodeError.IsNullOrEmpty();
                }
            }

            return result;
        }

        private bool ProcessBadResult(FormSubmitContext formSubmitContext, VitalResponsestring result)
        {
            if (result.Fault.ErrorType == ErrorType.InvalidField)
                ProcessInvalidField(formSubmitContext, result);

            logger.Error($"VTAL-FORM API MESSAGE: {result.Fault.Field}");
            logger.Error($"VTAL-FORM API MESSAGE: {result.Fault.Message}");
            logger.Error($"VTAL-FORM API MESSAGE: {result.Fault.ErrorType}");
            logger.Error($"VTAL-FORM API MESSAGE: {result.Fault.Type}");
            logger.Error($"VTAL-FORM API MESSAGE: {result.Fault.Method}");

            formSubmitContext.Abort();
            return false;
        }

        private void ProcessInvalidField(FormSubmitContext formSubmitContext, VitalResponsestring result)
        {
            string fieldName = result.Fault.Field;
            var field = formSubmitContext.Fields.FirstOrDefault(x => x.Name == fieldName) as IExtendedFormFieldViewModel;
            if (field == null)
                return;

            field.CodeError =
                TranslateHelper.TextByDomainCached("Translations", "translations.feature.forms.invalidfield");
        }


        private bool IsBadDate(FormSubmitContext formSubmitContext)
        {
            var dayField = formSubmitContext.Fields.FirstOrDefault(x => x.Name == "Day") as IExtendedFormFieldViewModel;
            var monthField = formSubmitContext.Fields.FirstOrDefault(x => x.Name == "Month");
            var yearField = formSubmitContext.Fields.FirstOrDefault(x => x.Name == "Year");

            var dir = new Director();
            dir.Validate<DateValidation>(dayField, monthField, yearField);

            return dayField == null || !dayField.CodeError.IsNullOrEmpty();
        }
    }
}