﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Delete.Foundation.DeleteFoundationCore.Helpers;
using Glass.Mapper;
using Sitecore.ExperienceForms.Models;

namespace Delete.Feature.Forms.Validation
{
    public class EmptyFieldValidation : Validation
    {
        protected override string Validate(string fieldData, params object[] extraFields)
        {
            if (fieldData.IsNullOrEmpty())
            {
                return TranslateHelper.TextByDomainCached("Translations", "translations.feature.forms.validation.isrequired");
            }

            return string.Empty;
        }
    }
}