﻿using System;
using System.Collections.Generic;
using Delete.Feature.Forms.Extensions;
using Delete.Foundation.DeleteFoundationCore.Helpers;
using Sitecore.ExperienceForms.Models;

namespace Delete.Feature.Forms.Validation
{
    public class DateValidation : Validation
    {
        protected override string Validate(string fieldData, params object[] extraFields)
        {
            var monthViewModel = (IViewModel)extraFields[0];
            var yearViewModel = (IViewModel)extraFields[1];

            try
            {
                int day = int.Parse(fieldData);
                int month = int.Parse(monthViewModel.GetStringValue());
                int year = int.Parse(yearViewModel.GetStringValue());

                new DateTime(year, month, day);
            }
            catch
            {
                return TranslateHelper.TextByDomainCached("Translations", "translations.feature.forms.validation.invaliddate");
            }

            return string.Empty;
        }
    }
}