﻿using Delete.Feature.Forms.Models;
using System;

namespace Delete.Feature.Forms.Validation
{
    public class Director
    {
        public void Validate<T>(IExtendedFormFieldViewModel field, params object[] extraFields) where T : Validation
        {
            var val = Activator.CreateInstance<T>();
            val.Execute(field, extraFields);
        }
    }
}