﻿using System;
using Delete.Feature.Forms.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Delete.Foundation.DeleteFoundationCore.Helpers;
using Sitecore.ExperienceForms.Models;

namespace Delete.Feature.Forms.Validation
{
    public abstract class Validation
    {
        public void Execute(IExtendedFormFieldViewModel field, params object[] extraFields)
        {
            string result = String.Empty;

            var data = field.GetType().GetProperty("Value").GetValue(field);
            if (data != null)
            {
                if (data is string str) result = Validate(str, extraFields);
                if (data is List<string> list) result = Validate(list.FirstOrDefault(), extraFields);
            }
            else
            {
                result = Validate(string.Empty, extraFields);
            }

            if (field is IExtendedFormFieldViewModel f)
                f.CodeError = result;
        }

        protected abstract string Validate(string fieldData, params object[] extraFields);
    }
}