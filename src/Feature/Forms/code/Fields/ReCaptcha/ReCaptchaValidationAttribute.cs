﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Delete.Feature.Forms.Services;
using Delete.Foundation.DeleteFoundationCore.Helpers;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.DependencyInjection;
using Sitecore.Globalization;

namespace Delete.Feature.Forms.Fields.ReCaptcha
{
    [AttributeUsage(AttributeTargets.Property)]
    public class ReCaptchaValidationAttribute : ValidationAttribute, IClientValidatable
    {
        private readonly IReCaptchaService reCaptchaService = ServiceLocator.ServiceProvider.GetService<IReCaptchaService>();

        public override bool IsValid(object value)
        {
            return reCaptchaService.VerifySync((string)value);
        }

        public override string FormatErrorMessage(string name)
        {
            return TranslateHelper.TextCached(this.ErrorMessageString);
        }

        IEnumerable<ModelClientValidationRule> IClientValidatable.GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule
                       {
                           ErrorMessage = FormatErrorMessage(metadata.GetDisplayName()),
                           ValidationType = "required"
                       };
            yield return rule;
        }

    }
}