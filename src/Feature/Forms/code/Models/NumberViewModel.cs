﻿using System;
using Sitecore.Data.Items;
using Delete.Feature.Forms.Extensions;

namespace Delete.Feature.Forms.Models
{
    [Serializable]
    public class NumberViewModel : Sitecore.ExperienceForms.Mvc.Models.Fields.NumberViewModel, IExtendedFormFieldViewModel
    {
        protected override void InitItemProperties(Item item)
        {
            base.InitItemProperties(item);

            this.InitCustomFields(item);
        }

        protected override void UpdateItemFields(Item item)
        {
            base.UpdateItemFields(item);

            this.UpdateItemCustomFields(item);
        }

        public string WrapperCustomAttributes { get; set; }
        public string InputCustomAttributes { get; set; }
        public string CodeError { get; set; }
    }
}