﻿using System;

namespace Delete.Feature.Forms.Models
{
    public interface IExtendedFormFieldViewModel
    {
        string WrapperCustomAttributes { get; set; }

        string InputCustomAttributes { get; set; }

        string CodeError { get; set; }
    }
}