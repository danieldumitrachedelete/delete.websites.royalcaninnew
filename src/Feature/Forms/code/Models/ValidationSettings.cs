﻿using Delete.Foundation.DeleteFoundationCore.Models;

namespace Delete.Feature.Forms.Models
{
    public class ValidationSettings : GlassBase
    {
        public string StoreFields { get; set; }
        public string ProfessionalFields { get; set; }
        public string VeterinaryFields { get; set; }
        public string GroomerFields { get; set; }
        public string JournalistFields { get; set; }
    }
}