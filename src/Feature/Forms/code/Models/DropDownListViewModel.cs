﻿using System;
using Delete.Feature.Forms.Extensions;
using Sitecore.Data.Items;

namespace Delete.Feature.Forms.Models
{
    [Serializable]
    public class DropDownListViewModel : Sitecore.ExperienceForms.Mvc.Models.Fields.DropDownListViewModel, IExtendedFormFieldViewModel
    {
        protected override void InitItemProperties(Item item)
        {
            base.InitItemProperties(item);

            this.InitCustomFields(item);
        }

        protected override void UpdateItemFields(Item item)
        {
            base.UpdateItemFields(item);

            this.UpdateItemCustomFields(item);
        }

        public string WrapperCustomAttributes { get; set; }
        public string InputCustomAttributes { get; set; }
        public string CodeError { get; set; }
    }
}