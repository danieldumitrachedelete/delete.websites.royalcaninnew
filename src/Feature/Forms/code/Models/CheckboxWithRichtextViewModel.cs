﻿using System;
using Delete.Feature.Forms.Extensions;
using Sitecore.Data.Items;

namespace Delete.Feature.Forms.Models
{
    [Serializable]
    public class CheckboxWithRichtextViewModel : Sitecore.ExperienceForms.Mvc.Models.Fields.CheckBoxViewModel, IExtendedFormFieldViewModel
    {
        private const string RichtextFieldName = "Text";

        protected override void InitItemProperties(Item item)
        {
            base.InitItemProperties(item);

            this.InitCustomFields(item);
            this.Text = item.Fields[RichtextFieldName]?.Value;
        }

        protected override void UpdateItemFields(Item item)
        {
            base.UpdateItemFields(item);

            this.UpdateItemCustomFields(item);
            item.Fields[RichtextFieldName]?.SetValue(this.Text, true);
        }

        public string WrapperCustomAttributes { get; set; }
        public string InputCustomAttributes { get; set; }
        public string CodeError { get; set; }
        public string Text { get; set; }
    }
}