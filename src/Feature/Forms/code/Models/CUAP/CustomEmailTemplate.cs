﻿using FormsSendMail.Forms.Models.Templates;

namespace Delete.Feature.Forms.Models.CUAP
{
    public class CustomEmailTemplate
    {
        public string Subject { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string Cc { get; set; }
        public string Bcc { get; set; }
        public string MessageRichText { get; set; }
        public string MessageText { get; set; }

        public CustomEmailTemplate(EmailTemplate emailTemplate)
        {
            Subject = emailTemplate.Subject;
            From = emailTemplate.From;
            To = emailTemplate.To;
            Cc = emailTemplate.Cc;
            Bcc = emailTemplate.Bcc;
            MessageRichText = emailTemplate.MessageRichText;
            MessageText = emailTemplate.MessageText;
        }
    }
}