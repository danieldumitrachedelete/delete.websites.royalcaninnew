﻿using System.Threading.Tasks;

namespace Delete.Feature.Forms.Services
{
    public interface IReCaptchaService
    {
        Task<bool> Verify(string response);
        bool VerifySync(string response);
    }
}