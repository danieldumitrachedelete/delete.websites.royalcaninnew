﻿using AutoMapper;
using Delete.Feature.Forms.Vital.Context;
using System;
using System.Globalization;
using System.Linq;

namespace Delete.Feature.Forms.Vital.Profiles
{
    public class VitalApiProfile : Profile
    {
        public VitalApiProfile()
        {
            CreateMap<VitalFormContext, VitalApiContext>()
                .ForMember("CustomerAddress", x => x.MapFrom(context => context.Address.Value))
                .ForMember("CustomerCity", x => x.MapFrom(context => context.City.Value))
                .ForMember("CustomerEmail1", x => x.MapFrom(context => context.Email.Value))
                .ForMember("CustomerFirstName", x => x.MapFrom(context => context.FirstName.Value))
                .ForMember("CustomerLastName", x => x.MapFrom(context => context.Surname.Value))
                .ForMember("CustomerMainPhoneNumber", x => x.MapFrom(context => context.PhoneNumber.Value))
                .ForMember("CustomerMobilePhoneNumber", x => x.MapFrom(context => context.MobileNumber.Value))
                .ForMember("CustomerZipCode", x => x.MapFrom(context => context.PostalCode.Value))
                .ForMember("PetGenderCode", x => x.MapFrom(context => context.Gender.Value.FirstOrDefault()))
                .ForMember("PetName", x => x.MapFrom(context => context.NameOfYourPet.Value))
                .ForMember("EnquiryLocalComments", x => x.MapFrom(context => context.YourMessage.Value))
                .ForMember("CustomerTitleCode", x => x.MapFrom(context => GetTitleCode(context)))
                .ForMember("CustomerCountryCode", x => x.MapFrom(context => short.Parse(context.Country.Value.FirstOrDefault() ?? "0")))
                .ForMember("EnquiryMainReasonCode", x => x.MapFrom(context => long.Parse(context.SubjectOfYourRequest.Value.FirstOrDefault() ?? "0")))
                .ForMember("EnquiryNameProductCode", x => x.MapFrom(context => GetProductCode(context)))
                .ForMember("PetBreedCode", x => x.MapFrom(context => GetBreedCode(context)))
                .ForMember("PetDateOfBirth", x => x.MapFrom(context => GetDateOfBirth(context)));
        }

        private DateTime GetDateOfBirth(VitalFormContext context)
        {
            var day = int.Parse(context.Day.Value.FirstOrDefault() ?? "1");
            var month = int.Parse(context.Month.Value.FirstOrDefault() ?? "1");
            var year = int.Parse(context.Year.Value.FirstOrDefault() ?? "2000");

            return new DateTime(year, month, day);
        }

        private byte? GetTitleCode(VitalFormContext context)
        {
            var result = byte.Parse(context.Title.Value.FirstOrDefault() ?? "0");
            if (result == 0) return null;

            return result;
        }

        private short? GetBreedCode(VitalFormContext context)
        {
            var specie = context.YouHaveA.Value.FirstOrDefault();
            var isBreed = context.YouKnowBreed.Value.FirstOrDefault();

            if (isBreed == null) return null;
            if (specie == null) return null;

            short? result = null;

            if (isBreed.ToUpper(CultureInfo.InvariantCulture) == "YES")
            {
                if (specie.ToUpper(CultureInfo.InvariantCulture) == "CAT")
                {
                    result = short.Parse(context.CatBreed.Value.FirstOrDefault() ?? "0");
                }
                else
                {
                    result = short.Parse(context.DogBreed.Value.FirstOrDefault() ?? "0");
                }
            }
            else
            {
                if (specie.ToUpper(CultureInfo.InvariantCulture) == "CAT")
                {
                    // todo: implement logic for cats length of coat
                }
                else
                {
                    result = short.Parse(context.DogSize.Value.FirstOrDefault() ?? "0");
                }
            }

            return result == null || result.Value == 0 ? null : result;
        }

        public short? GetProductCode(VitalFormContext context)
        {
            var specie = context.YouHaveA.Value.FirstOrDefault();
            if (specie == null) return null;

            short? result = null;

            if (specie.ToUpper(CultureInfo.InvariantCulture) == "CAT")
            {
                result = short.Parse(context.CatProduct.Value.FirstOrDefault() ?? "0");
            }
            else
            {
                result = short.Parse(context.DogProduct.Value.FirstOrDefault() ?? "0");
            }

            return result.Value == 0 ? null : result;
        }
    }
}