﻿using Delete.Feature.Forms.ExternalServices.Vital;
using Delete.Feature.Forms.Vital.Context;
using Delete.Feature.Forms.Vital.Interfaces;
using log4net;
using System;
using System.Collections.Generic;

namespace Delete.Feature.Forms.Vital
{
    public class VitalService : IVitalService
    {
        private VITALWCFServices _vitalServices;

        internal VitalService(VITALWCFServices vitalwcfServices)
        {
            this._vitalServices = vitalwcfServices;
        }

        public virtual IEnumerable<Country> GetCountries(string cultureCode = "en")
        {
            var result = this._vitalServices.GetCountries(cultureCode);
            if (result.ThrewException) return Array.Empty<Country>();

            var countries = result.Data;
            return countries;
        }

        public virtual IEnumerable<PetBreed> GetAllBreeds(string cultureCode = "en")
        {
            List<PetBreed> list = new List<PetBreed>();

            list.AddRange(GetCatBreeds(cultureCode));
            list.AddRange(GetDogBreeds(cultureCode));

            return list;
        }

        public virtual IEnumerable<PetBreed> GetCatBreeds(string cultureCode = "en")
        {
            return GetBreeds(cultureCode, "CAT");
        }

        public virtual IEnumerable<PetBreed> GetDogBreeds(string cultureCode = "en")
        {
            return GetBreeds(cultureCode, "DOG");
        }

        protected virtual IEnumerable<PetBreed> GetBreeds(string cultureCode, string petBreed)
        {
            var result = this._vitalServices.GetPetBreeds(cultureCode, petBreed);
            if (result.ThrewException) return Array.Empty<PetBreed>();

            var breeds = result.Data;
            return breeds;
        }

        public virtual IEnumerable<PetGenus> GetPetGenus(string cultureCode = "en")
        {
            var result = this._vitalServices.GetPetGenus(cultureCode);
            if (result.ThrewException) return Array.Empty<PetGenus>();

            var genuses = result.Data;
            return genuses;
        }

        public virtual VitalResponsestring SendRequest(VitalApiContext context)
        {
            VitalResponsestring result = this._vitalServices.InsertEnquiryCustomerPet(
                customerTitleCode: context.CustomerTitleCode,
                customerCountryCode: context.CustomerCountryCode,
                customerProfileCode: context.CustomerProfileCode,
                customerInternalCode: null,
                customerProvenance: context.CustomerProvenance,
                customerCompany: null,
                customerLastName: context.CustomerLastName,
                customerFirstName: context.CustomerFirstName,
                customerAddress: context.CustomerAddress,
                customerAddress2: null,
                customerZipCode: context.CustomerZipCode,
                customerCity: context.CustomerCity,
                customerEmail1: context.CustomerEmail1,
                customerEmail2: null,
                customerEmail3: null,
                customerStateProvince: null,
                customerMainPhoneNumber: context.CustomerMainPhoneNumber,
                customerMobilePhoneNumber: context.CustomerMobilePhoneNumber,
                customerFaxNumber: null,
                customerIsNotResidentState: null,
                customerIsAgreeForEmail: null,
                customerIsAgreeForLetter: null,
                customerExDirectory: null,
                customerWebSiteUrl: null,
                customerDistributorName: null,
                customerSalesRepresentative: null,
                enquiryNameProductCode: context.EnquiryNameProductCode,
                enquiryMainReasonCode: context.EnquiryMainReasonCode,
                enquiryLocalComments: context.EnquiryLocalComments,
                petBreedCode: context.PetBreedCode,
                petGenderCode: context.PetGenderCode,
                petName: context.PetName,
                petDateOfBirth: context.PetDateOfBirth,
                userLogin: context.UserLogin);

            return result;
        }
    }
}