﻿using Delete.Feature.Breeds.Managers;
using Delete.Feature.Breeds.Models;
using Delete.Feature.Forms.Vital.Interfaces;
using Delete.Feature.Forms.Vital.Models;
using Delete.Feature.Products.Managers;
using Delete.Feature.Products.Models;
using Delete.Foundation.MultiSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Sitecore.StringExtensions;

namespace Delete.Feature.Forms.Vital
{
    public class LocalService : ILocalService
    {
        private IProductSearchManager _searchManager;

        private Guid _specie;
        private Guid _pilar;
        private ILocalMarketSettings _localMarketSettings;
        private ILocalCatBreedSearchManager _catBreedSearchManager;
        private ILocalDogBreedSearchManager _dogBreedSearchManager;

        internal LocalService(IProductSearchManager searchManager, ILocalCatBreedSearchManager catBreedSearchManager, ILocalDogBreedSearchManager dogBreedSearchManager)
        {
            this._searchManager = searchManager;
            this._catBreedSearchManager = catBreedSearchManager;
            this._dogBreedSearchManager = dogBreedSearchManager;
        }

        public virtual ILocalService SetPillar(Guid pillar)
        {
            this._pilar = pillar;
            return this;
        }

        public virtual ILocalService SetSpecie(Guid specie)
        {
            this._specie = specie;
            return this;
        }

        public virtual ILocalService SetMarket(ILocalMarketSettings localMarketSettings)
        {
            this._localMarketSettings = localMarketSettings;
            return this;
        }

        public virtual IEnumerable<VitalProductsViewModel> GetProducts()
        {
            IEnumerable<LocalProduct> localProducts = this._searchManager.GetAll(this._specie);
            //IEnumerable<LocalProduct> localProducts = this._searchManager.GetAllLocal(this._localMarketSettings, this._specie, this._pilar).Results;
            localProducts = localProducts.Where(x => x.GlobalProduct != null);

            List<VitalProductsViewModel> result = new List<VitalProductsViewModel>();
            foreach (var product in localProducts)
            {
                var resultElement = new VitalProductsViewModel();
                resultElement.MainItemId = product.GlobalProduct.MainItemID;
                resultElement.ProductName = product.ProductName;
                result.Add(resultElement);
            }

            return result;
        }

        public virtual IEnumerable<LocalCatBreed> GetCatBreeds()
        {
            var breeds = this._catBreedSearchManager.GetAll();
            breeds = breeds.Where(x => x.VitalBreedCode != default(decimal));
            breeds = breeds.Where(x => x.GlobalCatBreed != null);

            return breeds;
        }

        public virtual IEnumerable<LocalDogBreed> GetDogBreeds()
        {
            var breeds = this._dogBreedSearchManager.GetAll();
            breeds = breeds.Where(x => x.VitalBreedCode != default(decimal));
            breeds = breeds.Where(x => x.GlobalDogBreed != null);

            return breeds;
        }
    }
}