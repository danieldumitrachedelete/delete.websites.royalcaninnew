﻿using Delete.Feature.Forms.ExternalServices.Vital;
using Delete.Feature.Forms.Vital.Context;
using System.Collections.Generic;

namespace Delete.Feature.Forms.Vital.Interfaces
{
    public interface IVitalService
    {
        VitalResponsestring SendRequest(VitalApiContext context);

        IEnumerable<Country> GetCountries(string cultureCode = "en");
        IEnumerable<PetGenus> GetPetGenus(string cultureCode = "en");
        IEnumerable<PetBreed> GetDogBreeds(string cultureCode = "en");
        IEnumerable<PetBreed> GetCatBreeds(string cultureCode = "en");
        IEnumerable<PetBreed> GetAllBreeds(string cultureCode = "en");
    }
}
