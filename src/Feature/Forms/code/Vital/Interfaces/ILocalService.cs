﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Delete.Feature.Breeds.Models;
using Delete.Feature.Forms.ExternalServices.Vital;
using Delete.Feature.Forms.Vital.Models;
using Delete.Feature.Products.Models;
using Delete.Foundation.MultiSite.Models;

namespace Delete.Feature.Forms.Vital.Interfaces
{
    public interface ILocalService
    {
        IEnumerable<VitalProductsViewModel> GetProducts();
        IEnumerable<LocalDogBreed> GetDogBreeds();
        IEnumerable<LocalCatBreed> GetCatBreeds();
        ILocalService SetPillar(Guid pillar);
        ILocalService SetSpecie(Guid specie);
        ILocalService SetMarket(ILocalMarketSettings localMarketSettings);
    }
}
