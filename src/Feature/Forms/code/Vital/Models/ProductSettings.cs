﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Delete.Feature.Forms.Vital.Models
{
    public class ProductSettings
    {
        public Guid Specie { get; set; }
        public Guid Pillar { get; set; }
    }
}