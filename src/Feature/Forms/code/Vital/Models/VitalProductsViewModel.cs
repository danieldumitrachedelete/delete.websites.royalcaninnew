﻿namespace Delete.Feature.Forms.Vital.Models
{
    public class VitalProductsViewModel
    {
        public string ProductName { get; set; }
        public string MainItemId { get; set; }

        public VitalProductsViewModel()
        {
        }

        public VitalProductsViewModel(string productName, string mainItemId)
        {
            this.ProductName = productName;
            this.MainItemId = mainItemId;
        }
    }
}