﻿using System.ServiceModel;
using System.ServiceModel.Channels;

namespace Delete.Feature.Forms.Vital.Settings
{
    public static class VitalHttpBinding
    {
        public static EndpointAddress GetVitalEndpointAddress()
        {
            var endpoint = new EndpointAddress("http://vitalwebservice.extranet.royalcanin.org/Service.svc");

            return endpoint;
        }

        public static Binding GetVitalBinding()
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.Name = "BasicHttpBinding_VITALWCFServices";
            binding.MaxReceivedMessageSize = int.MaxValue;

            return binding;
        }
    }
}