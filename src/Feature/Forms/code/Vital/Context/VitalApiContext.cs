﻿
using System;

namespace Delete.Feature.Forms.Vital.Context
{
    public class VitalApiContext
    {
        public string UserLogin { get; } = "svc_FormulaireWebFR";
        public string CustomerProvenance { get; } = "8";
        public long CustomerProfileCode { get; } = 5;

        public DateTime PetDateOfBirth { get; set; }
        public long EnquiryMainReasonCode { get; set; }
        public short? EnquiryNameProductCode { get; set; }
        public short? PetBreedCode { get; set; }
        public short CustomerCountryCode { get; set; }
        public byte? CustomerTitleCode { get; set; }
        public string EnquiryLocalComments { get; set; }
        public string CustomerEmail1 { get; set; }
        public string CustomerFirstName { get; set; }
        public string CustomerLastName { get; set; }
        public string CustomerAddress { get; set; }
        public string CustomerZipCode { get; set; }
        public string CustomerCity { get; set; }
        public string CustomerMainPhoneNumber { get; set; }
        public string CustomerMobilePhoneNumber { get; set; }
        public string PetGenderCode { get; set; }
        public string PetName { get; set; }

        public VitalApiContext()
        {
        }

        //public VitalApiContext(VitalFormContext context)
        //{
        //    this.CustomerAddress = context.Address.Value;
        //    this.CustomerCity = context.City.Value;
        //    this.CustomerEmail1 = context.Email.Value;
        //    this.CustomerFirstName = context.FirstName.Value;
        //    this.CustomerLastName = context.Surname.Value;
        //    this.CustomerMainPhoneNumber = context.PhoneNumber.Value;
        //    this.CustomerMobilePhoneNumber = context.MobileNumber.Value;
        //    this.CustomerZipCode = context.PostalCode.Value;
        //    this.PetGenderCode = context.Gender.Value.FirstOrDefault();
        //    this.PetName = context.NameOfYourPet.Value;
        //    this.EnquiryLocalComments = context.YourMessage.Value;

        //    this.CustomerTitleCode = byte.Parse(context.Title.Value.FirstOrDefault() ?? "0");
        //    this.CustomerCountryCode = short.Parse(context.Country.Value.FirstOrDefault() ?? "0");
        //    this.EnquiryMainReasonCode = long.Parse(context.SubjectOfYourRequest.Value.FirstOrDefault() ?? "0");

        //    this.PetDateOfBirth = GetDateOfBirth(context);
        //    this.EnquiryNameProductCode = GetProductCode(context);
        //    this.PetBreedCode = GetBreedCode(context);
        //}

        //private DateTime GetDateOfBirth(VitalFormContext context)
        //{
        //    var day = int.Parse(context.Day.Value.FirstOrDefault() ?? "1");
        //    var month = int.Parse(context.Month.Value.FirstOrDefault() ?? "1");
        //    var year = int.Parse(context.Year.Value.FirstOrDefault() ?? "2000");

        //    return new DateTime(year, month, day);
        //}

        //private short? GetBreedCode(VitalFormContext context)
        //{
        //    var specie = context.YouHaveA.Value.FirstOrDefault();
        //    if (specie == null) return null;
        //    short? result = null;

        //    if (specie.ToUpper(CultureInfo.InvariantCulture) == "CAT")
        //    {
        //        result = short.Parse(context.CatBreed.Value.FirstOrDefault() ?? "0");
        //    }
        //    else
        //    {
        //        result = short.Parse(context.DogBreed.Value.FirstOrDefault() ?? "0");
        //    }

        //    return result.Value == 0 ? null : result;
        //}

        //public short? GetProductCode(VitalFormContext context)
        //{

        //    var pilar = context.Pillar.Value.FirstOrDefault();
        //    var specie = context.Specie.Value.FirstOrDefault();

        //    if (pilar == null || specie == null) return null;

        //    short? result = null;

        //    if (specie.ToUpper(CultureInfo.InvariantCulture) == "CAT")
        //    {
        //        if (pilar.ToUpper(CultureInfo.InvariantCulture) == "POS")
        //        {
        //            result = short.Parse(context.CatPosProduct.Value.FirstOrDefault() ?? "0");
        //        }
        //        else
        //        {
        //            result = short.Parse(context.CatVetProduct.Value.FirstOrDefault() ?? "0");
        //        }
        //    }
        //    else
        //    {
        //        if (pilar.ToUpper(CultureInfo.InvariantCulture) == "POS")
        //        {
        //            result = short.Parse(context.DogPosProduct.Value.FirstOrDefault() ?? "0");
        //        }
        //        else
        //        {
        //            result = short.Parse(context.DogVetProduct.Value.FirstOrDefault() ?? "0");
        //        }
        //    }

        //    return result.Value == 0 ? null : result;
        //}
    }
}