﻿using Delete.Feature.Forms.Models;
using Newtonsoft.Json;
using Sitecore.ExperienceForms.Models;
using System.Collections.Generic;
using System.Linq;

namespace Delete.Feature.Forms.Vital.Context
{
    public class VitalFormContext
    {
        public ListBoxViewModel SubjectOfYourRequest { get; set; }
        public DropDownListViewModel Year { get; set; }
        public DropDownListViewModel Month { get; set; }
        public DropDownListViewModel Day { get; set; }
        public DropDownListViewModel CatProduct { get; set; }
        public DropDownListViewModel DogProduct { get; set; }
        public MultipleLineTextViewModel YourMessage { get; set; }
        public DropDownListViewModel Title { get; set; }
        public StringInputViewModel Email { get; set; }
        public StringInputViewModel FirstName { get; set; }
        public StringInputViewModel Surname { get; set; }
        public StringInputViewModel Address { get; set; }
        public DropDownListViewModel Country { get; set; }
        public StringInputViewModel PostalCode { get; set; }
        public StringInputViewModel City { get; set; }
        public StringInputViewModel PhoneNumber { get; set; }
        public StringInputViewModel MobileNumber { get; set; }
        public DropDownListViewModel CatBreed { get; set; }
        public DropDownListViewModel DogBreed { get; set; }
        public DateViewModel DateOfBirth { get; set; }
        public ListViewModel Gender { get; set; }
        public StringInputViewModel NameOfYourPet { get; set; }
        public ListViewModel YouHaveA { get; set; }
        public ListViewModel YouKnowBreed { get; set; }
        public DropDownListViewModel CoatLength { get; set; }
        public DropDownListViewModel DogSize { get; set; }

        public VitalFormContext()
        {
        }

        public VitalFormContext(IList<IViewModel> fields)
        {
            // product section
            this.DogProduct = fields.FirstOrDefault(x => x.Name == "DogProduct") as DropDownListViewModel;
            this.CatProduct = fields.FirstOrDefault(x => x.Name == "CatProduct") as DropDownListViewModel;

            // breed section
            this.YouHaveA = fields.FirstOrDefault(x => x.Name == "You have a") as ListViewModel;
            this.YouKnowBreed = fields.FirstOrDefault(x => x.Name == "You know breed") as ListViewModel;
            this.CatBreed = fields.FirstOrDefault(x => x.Name == "CatBreed") as DropDownListViewModel;
            this.DogBreed = fields.FirstOrDefault(x => x.Name == "DogBreed") as DropDownListViewModel;
            this.CoatLength = fields.FirstOrDefault(x => x.Name == "Coat Length") as DropDownListViewModel;
            this.DogSize = fields.FirstOrDefault(x => x.Name == "Dog Size") as DropDownListViewModel;

            // request type section
            this.SubjectOfYourRequest = fields.FirstOrDefault(x => x.Name == "Subject of your request") as ListBoxViewModel;
            this.YourMessage = fields.FirstOrDefault(x => x.Name == "Your message") as MultipleLineTextViewModel;

            // contact information section
            this.Title = fields.FirstOrDefault(x => x.Name == "Title") as DropDownListViewModel;
            this.Email = fields.FirstOrDefault(x => x.Name == "Email") as StringInputViewModel;
            this.FirstName = fields.FirstOrDefault(x => x.Name == "First name") as StringInputViewModel;
            this.Surname = fields.FirstOrDefault(x => x.Name == "Surname") as StringInputViewModel;
            this.Address = fields.FirstOrDefault(x => x.Name == "Address") as StringInputViewModel;
            this.Country = fields.FirstOrDefault(x => x.Name == "Country") as DropDownListViewModel;
            this.PostalCode = fields.FirstOrDefault(x => x.Name == "Postal code") as StringInputViewModel;
            this.City = fields.FirstOrDefault(x => x.Name == "City") as StringInputViewModel;
            this.PhoneNumber = fields.FirstOrDefault(x => x.Name == "Phone number") as StringInputViewModel;
            this.MobileNumber = fields.FirstOrDefault(x => x.Name == "Mobile number") as StringInputViewModel;

            // pet information section
            this.DateOfBirth = fields.FirstOrDefault(x => x.Name == "Date of birth") as DateViewModel;
            this.Gender = fields.FirstOrDefault(x => x.Name == "Gender") as ListViewModel;
            this.NameOfYourPet = fields.FirstOrDefault(x => x.Name == "Name of your pet") as StringInputViewModel;

            // pet date birth section
            this.Day = fields.FirstOrDefault(x => x.Name == "Day") as DropDownListViewModel;
            this.Month = fields.FirstOrDefault(x => x.Name == "Month") as DropDownListViewModel;
            this.Year = fields.FirstOrDefault(x => x.Name == "Year") as DropDownListViewModel;
        }
    }
}