﻿using Delete.Feature.Forms.ExternalServices.Vital;
using Delete.Feature.Forms.Vital.Interfaces;
using Delete.Feature.Forms.Vital.Settings;

namespace Delete.Feature.Forms.Vital.Factories
{
    public static class VitalServiceFactory
    {
        public static IVitalService GetVitalService()
        {
            VITALWCFServices vitalwcfServices = GetVitalWFCServices();

            IVitalService vitalService = new VitalService(vitalwcfServices);
            return vitalService;
        }

        public static VITALWCFServices GetVitalWFCServices()
        {
            var binding = VitalHttpBinding.GetVitalBinding();
            var endpoint = VitalHttpBinding.GetVitalEndpointAddress();

            VITALWCFServices vitalwcfServices = new VITALWCFServicesClient(binding, endpoint);
            return vitalwcfServices;
        }
    }
}