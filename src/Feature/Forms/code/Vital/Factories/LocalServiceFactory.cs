﻿using Delete.Feature.Breeds.Managers;
using Delete.Feature.Forms.Vital.Interfaces;
using Delete.Feature.Products.Managers;
using Delete.Foundation.MultiSite.Extensions;
using Delete.Foundation.MultiSite.Models;
using Glass.Mapper.Sc;

namespace Delete.Feature.Forms.Vital.Factories
{
    public static class LocalServiceFactory
    {
        public static ILocalService GetLocalService()
        {
            IProductSearchManager productSearchManager = new ProductSearchManager();
            ILocalCatBreedSearchManager localCatBreedSearch = new LocalCatBreedSearchManager();
            ILocalDogBreedSearchManager localDogBreedSearch = new LocalDogBreedSearchManager();


            ILocalService localService = new LocalService(productSearchManager, localCatBreedSearch, localDogBreedSearch);
            return localService;
        }
    }
}