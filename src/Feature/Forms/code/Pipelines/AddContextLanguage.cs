﻿using System;
using Sitecore.Diagnostics;
using Sitecore.ExperienceForms.Mvc;
using Sitecore.ExperienceForms.Mvc.Pipelines.RenderForm;
using Sitecore.Mvc.Pipelines;

namespace Delete.Feature.Forms.Pipelines
{
    public class AddContextLanguage : MvcPipelineProcessor<RenderFormEventArgs>
    {
        public override void Process(RenderFormEventArgs args)
        {
            args.QueryString.Add("scLanguage", Sitecore.Context.Site.VirtualFolder.Trim('/'));
        }
    }
}
