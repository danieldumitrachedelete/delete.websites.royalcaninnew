﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using Sitecore.Pipelines;

namespace Delete.Feature.Forms.Pipelines
{
    public class InitializeRoutes : Sitecore.Mvc.Pipelines.Loader.InitializeRoutes
    {
        protected override void RegisterRoutes(RouteCollection routes, PipelineArgs args)
        {
            routes.MapRoute("FormBuilder", "{scLanguage}/formbuilder/{action}/{id}", (object) new
            {
                controller = "FormBuilder",
                action = "Index",
                id = UrlParameter.Optional
            });
            
            routes.MapRoute("FieldTracking", "fieldtracking/{action}", (object)new
            {
                controller = "FieldTracking",
                action = "Register"
            });
        }
    }
}