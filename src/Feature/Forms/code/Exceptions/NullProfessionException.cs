﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.ExperienceForms.Models;

namespace Delete.Feature.Forms.Exceptions
{
    public class NullProfessionException : Exception
    {
        public IViewModel Field { get; }

        public NullProfessionException(IViewModel field) : base()
        {
            this.Field = field;
        }
    }
}