﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Diagnostics;
using Sitecore.Sites;

namespace Delete.Feature.Forms.Extensions
{
    public static class SitecoreContextExtensions
    {
        /// <summary>
        /// Use this method to get Captcha language set on each market site definition.
        /// </summary>
        /// <param name="site">market site definition</param>
        /// <returns></returns>
        public static string CaptchaLanguage(this SiteContext site)
        {
            try
            {
                Assert.IsNotNull(site, "Site cannot be null");

                var captchaLanguage = site.Properties["captchaLanguage"];
                return !string.IsNullOrEmpty(captchaLanguage) ? captchaLanguage : Sitecore.Context.Language.CultureInfo.TwoLetterISOLanguageName;
            }
            catch (Exception)
            {
                return "en";
            }
        }
    }
}