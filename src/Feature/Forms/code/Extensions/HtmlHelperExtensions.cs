﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Sitecore.ExperienceForms.Mvc.Constants;

namespace Delete.Feature.Forms.Extensions
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;

    using Delete.Foundation.DeleteFoundationCore.Extensions;

    using Newtonsoft.Json;

    using Sitecore.Diagnostics;
    using Sitecore.ExperienceForms.Models;
    using Sitecore.ExperienceForms.Mvc.Models.Fields;
    using Sitecore.ExperienceForms.Mvc.Models.Validation;

    public static class HtmlHelperExtensions
    {
        private static string parsleyPrefix = "data-parsley-";
        public static string CaptchaScriptKey => "google-recaptcha";

        public static string ValidateScriptKey => "formextensions-validate";

        public static void AddScript(this HtmlHelper htmlHelper, string key, IHtmlString value)
        {
            if (htmlHelper.ViewContext.HttpContext.Items[IncludeFileKeys.ScriptsFilesKey] is Dictionary<string, IHtmlString> scripts && !scripts.ContainsKey(key))
            {
                scripts.Add(key, value);
            }
        }

        public static HtmlString GenerateParsleyServerErrors<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> propertyExpression)
        {
            var fullHtmlFieldName = htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(ExpressionHelper.GetExpressionText(propertyExpression));
            var modelState = htmlHelper.ViewData.ModelState[fullHtmlFieldName];
            var errors = modelState?.Errors?.Where(x => x.ErrorMessage.NotEmpty()).Select(x => x.ErrorMessage);
            if (errors != null)
            {
                var serializedErrors = JsonConvert.SerializeObject(errors);
                return new HtmlString($"data-server-errors=\"{serializedErrors}\"");
            }
            return new HtmlString(string.Empty);
        }

        public static HtmlString GenerateParsleyValidationAttributes<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> propertyExpression, IEnumerable<ModelClientValidationRule> rules) where TModel : InputViewModel<TProperty>
        {
            Assert.ArgumentNotNull(htmlHelper, nameof(htmlHelper));

            var attributes = new Dictionary<string, string>();
            
            foreach (var rule in rules)
            {
                attributes.AddParsleyRequiredAttributes(htmlHelper);
                attributes.AddParsleyAttributesForValidationRule<TModel, TProperty>(rule, htmlHelper.ViewData.Model);
            }
           
            return new HtmlString(string.Join(" ", attributes.Select(attr => attr.Key.ToString() + "=\"" + HttpUtility.HtmlEncode(attr.Value) + "\"")));
        }

        private static void AddParsleyRequiredAttributes<TModel>(this IDictionary<string, string> attributes, HtmlHelper<TModel> htmlHelper) where TModel : IValueField
        {
            if (htmlHelper.ViewData.Model.Required)
            {
                if (!attributes.ContainsKey("required"))
                {
                    attributes.Add("required", string.Empty);
                }

                var property = htmlHelper.ViewData.Model.GetType().GetProperty(nameof(InputViewModel<string>.Value));
                var requiredAttribute = (DynamicRequiredAttribute)property?.GetCustomAttributes(typeof(DynamicRequiredAttribute), false).FirstOrDefault();
                if (requiredAttribute != null && !attributes.ContainsKey("data-parsley-required-message"))
                {
                    attributes.Add("data-parsley-required-message", requiredAttribute.FormatErrorMessage((htmlHelper.ViewData.Model as TitleFieldViewModel)?.Title));
                }
            }
        }

        private static void AddParsleyAttributesForValidationRule<TModel, T>(this IDictionary<string, string> attributes, ModelClientValidationRule rule, TModel model) where TModel : InputViewModel<T>
        {
            switch (rule.ValidationType)
            {
                case "daterange":
                    attributes.AddDateValidationAttributes<TModel, T>(rule, model);
                    break;
                case "length":
                    attributes.AddLengthValidationAttributes<TModel, T>(rule, model);
                    break;
                case "number":
                    attributes.AddNumberValidationAttributes<TModel, T>(rule, model);
                    break;
                case "regex":
                    attributes.AddRegexValidationAttributes(rule);
                    break;
                default:
                    var defaultMessage = $"{parsleyPrefix}{rule.ValidationType}-message";
                    attributes.Add(rule.ValidationType, string.Empty);
                    attributes.Add(defaultMessage, rule.ErrorMessage);
                    attributes.AddValidationParameterAttributes(rule);
                    break;
            }
        }


        private static void AddLengthValidationAttributes<TModel, T>(this IDictionary<string, string> attributes, ModelClientValidationRule rule, TModel model) where TModel : InputViewModel<T>
        {
            var stringModel = model as StringInputViewModel;
            if (stringModel != null)
            {
                if (stringModel.MinLength > 0)
                {
                    attributes.Add($"{parsleyPrefix}minlength", stringModel.MinLength.ToString());
                    attributes.Add($"{parsleyPrefix}minlength-message", rule.ErrorMessage);
                }

                if (stringModel.MaxLength > 0)
                {
                    attributes.Add($"{parsleyPrefix}maxlength", stringModel.MaxLength.ToString());
                    attributes.Add($"{parsleyPrefix}maxlength-message", rule.ErrorMessage);
                }
            }
        }

        private static void AddDateValidationAttributes<TModel, T>(this IDictionary<string, string> attributes, ModelClientValidationRule rule, TModel model) where TModel : InputViewModel<T>
        {
            var dateModel = model as DateViewModel;
            if (dateModel != null)
            {
                if (dateModel.Min.HasValue)
                {
                    attributes.Add("min", dateModel.Min.Value.ToString(dateModel.DateFormat));
                    attributes.Add($"{parsleyPrefix}min-message", rule.ErrorMessage);
                }

                if (dateModel.Max.HasValue)
                {
                    attributes.Add("max", dateModel.Max.Value.ToString(dateModel.DateFormat));
                    attributes.Add($"{parsleyPrefix}max-message", rule.ErrorMessage);
                }
            }
        }

        private static void AddNumberValidationAttributes<TModel, T>(this IDictionary<string, string> attributes, ModelClientValidationRule rule, TModel model) where TModel : InputViewModel<T>
        {
            var numberModel = model as NumberViewModel;
            if (numberModel != null)
            {
                if (numberModel.Min > 0)
                {
                    attributes.Add($"{parsleyPrefix}min-message", rule.ErrorMessage);
                }

                if (numberModel.Max > 0)
                {
                    attributes.Add($"{parsleyPrefix}max-message", rule.ErrorMessage);
                }

                attributes.AddValidationParameterAttributes(rule);
            }
        }

        private static void AddRegexValidationAttributes(this IDictionary<string, string> attributes, ModelClientValidationRule rule)
        {
            attributes.Add($"{parsleyPrefix}pattern-message", rule.ErrorMessage);
            attributes.AddValidationParameterAttributes(rule);
        }

        private static void AddValidationParameterAttributes(this IDictionary<string, string> attributes, ModelClientValidationRule rule)
        {
            foreach (var validationParameter in rule.ValidationParameters)
            {
                attributes.Add($"{validationParameter.Key}", validationParameter.Value?.ToString());
            }
        }
    }
}