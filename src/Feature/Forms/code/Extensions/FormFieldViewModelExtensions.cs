﻿using Delete.Feature.Forms.Models;
using Sitecore.Data.Items;
using Sitecore.ExperienceForms.Models;
using System;
using System.Collections.Generic;

namespace Delete.Feature.Forms.Extensions
{
    public static class FormFieldViewModelExtensions
    {
        private const string WrapperCustomAttributesFieldName = "Wrapper Custom Attributes";
        private const string InputCustomAttributesFieldName = "Input Custom Attributes";

        public static void InitCustomFields(this IExtendedFormFieldViewModel model, Item item)
        {
            model.WrapperCustomAttributes = item.Fields[WrapperCustomAttributesFieldName]?.Value;
            model.InputCustomAttributes = item.Fields[InputCustomAttributesFieldName]?.Value;
        }

        public static void UpdateItemCustomFields(this IExtendedFormFieldViewModel model, Item item)
        {
            item.Fields[WrapperCustomAttributesFieldName]?.SetValue(model.WrapperCustomAttributes, true);
            item.Fields[InputCustomAttributesFieldName]?.SetValue(model.InputCustomAttributes, true);
        }

        public static string GetStringValue(this IViewModel field)
        {
            var formFieldObjectValue = field?.GetType()?.GetProperty("Value")?.GetValue(field);

            if (formFieldObjectValue != null && !string.IsNullOrEmpty(field.Name))
            {
                if (formFieldObjectValue is string)
                {
                    return formFieldObjectValue as string;
                }
                if (formFieldObjectValue is List<string>)
                {
                    var fieldValueList = formFieldObjectValue as List<string>;
                    return fieldValueList.Count > 0 ? fieldValueList[0] : string.Empty;
                }
            }

            return string.Empty;
        }
    }
}