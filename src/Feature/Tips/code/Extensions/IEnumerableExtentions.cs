﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Delete.Foundation.DeleteFoundationCore.Models;
using Sitecore.ContentSearch.Linq.Extensions;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;

namespace Delete.Feature.Tips.Extensions
{
    public static class IEnumerableExtentions
    {
        public static IEnumerable<T> MakeItemsInOrderOf<T>(this IEnumerable<T> items, IEnumerable<Guid> idsOrder) 
            where T : IGlassBase
        {
            List<T> list = new List<T>();

            foreach (var id in idsOrder)
            {
                list.Add(items.FirstOrDefault(x => x.Id == id));
            }

            return list;
        }
    }
}