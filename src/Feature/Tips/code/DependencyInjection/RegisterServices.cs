using Delete.Feature.Tips.Managers;
using Delete.Feature.Tips.Models;
using Delete.Foundation.InitializeDependencyInjection.Extensions;
using Microsoft.Extensions.DependencyInjection;
using Delete.Foundation.DeleteFoundationCore.Managers;
using Sitecore.DependencyInjection;

namespace Delete.Feature.Tips.DependencyInjection
{
    public class RegisterServices : IServicesConfigurator
    {
        public void Configure(IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<ITipsSearchManager, TipsSearchManager>();

            serviceCollection.AddMvcControllersInCurrentAssembly();

            RegisterIndexes();
        }

        private static void RegisterIndexes()
        {
            SearchManagersConfiguration.RegisterIndexConfiguration(typeof(Tip), TipsConstants.CustomIndexAlias);
        }
    }
}
