﻿namespace Delete.Feature.Tips
{
    public static class TipsConstants
	{
        public const int FeaturedContentSize = 3;
        public const int RelatedContentSize = 3;

        public const string CustomIndexAlias = "articles";
    }
}
