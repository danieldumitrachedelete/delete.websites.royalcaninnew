﻿using Castle.Core.Internal;
using Delete.Feature.Tips.Models;
using Delete.Feature.Tips.Models.Taxonomy;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.DeleteFoundationCore.Managers;
using Delete.Foundation.DeleteFoundationCore.Models;
using Delete.Foundation.MultiSite.Models;
using Sitecore.ContentSearch.Linq;
using Sitecore.ContentSearch.Linq.Utilities;
using Sitecore.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using BoostLevels = Delete.Foundation.RoyalCaninCore.Constants.Search.BoostLevels;

namespace Delete.Feature.Tips.Managers
{
    public interface ITipsSearchManager : IBaseSearchManager<Tip>
    {
        IEnumerable<Tip> GetAll(Guid? rootItemId = null,
            Guid? specieId = null,
            string keyword = null,
            IDictionary<string, IEnumerable<Guid>> filterIds = null);
        IEnumerable<Tip> GetLatest(Guid? rootItemId = null, int count = 0);
        IEnumerable<Tip> FindRelated(Tip item);
        IEnumerable<Tip> FindTips(ITipsTaxonomy item, int maxCount = 0);
        IEnumerable<Tip> GetAllWithIds(IEnumerable<Guid> ids);
    }

    public class TipsSearchManager : BaseSearchManager<Tip>, ITipsSearchManager
    {
        public IEnumerable<Tip> GetAllWithIds(IEnumerable<Guid> ids)
        {
            var filtered = this.GetQueryable();
            filtered = this.FilterByIds(filtered, ids);

            return filtered
                .MapResults(SitecoreContext, true)
                .ToList();
        }

        private IQueryable<Tip> FilterByIds(IQueryable<Tip> query, IEnumerable<Guid> ids)
        {
            if (ids.IsNullOrEmpty()) return query;

            return query.Where(x => ids.Contains(x.Id));
        }

        public IEnumerable<Tip> GetAll(Guid? rootItemId = null,
            Guid? specieId = null,
            string keyword = null,
            IDictionary<string, IEnumerable<Guid>> filterIds = null)
        {
            var filtered = this.GetQueryable();
            filtered = this.FilterByRootItem(filtered, rootItemId);
            filtered = this.FilterBySpecie(filtered, specieId);
            filtered = this.FilterByKeyword(filtered, keyword);
            filtered = this.FilterByRelatedEntities(filtered, filterIds);

            return filtered
                .MapResults(SitecoreContext, true)
                .ToList();
        }

        public IEnumerable<Tip> GetLatest(Guid? rootItemId = null, int count = 0)
        {
            var filtered = this.GetQueryable();
            filtered = this.FilterByRootItem(filtered, rootItemId);
            filtered = filtered.OrderByDescending(x => x.Date);

            if (count > 0)
            {
                filtered = filtered.Take(count);
            }

            return filtered
                .MapResults(SitecoreContext, true)
                .ToList();
        }

        private IQueryable<Tip> FilterByRelatedEntities(
            IQueryable<Tip> query,
            IDictionary<string, IEnumerable<Guid>> filterIds)
        {
            if (filterIds == null || !filterIds.Any())
            {
                return query;
            }

            return query.Where(BuildFilterGroupExpression(filterIds));
        }

        private IQueryable<Tip> FilterByRootItem(IQueryable<Tip> query, Guid? rootItemId)
        {
            if (!rootItemId.HasValue)
            {
                return this.FilterLocal(query);
            }

            var rootItem = new ID(rootItemId.Value);
            return query.Where(x => x.Paths.Contains(rootItem));
        }

        private IQueryable<Tip> FilterBySpecie(IQueryable<Tip> query, Guid? specieId)
        {
            if (!specieId.HasValue)
            {
                return query;
            }

            return query.Where(x => x.SpecieIds.Contains(specieId.Value));
        }

        private IQueryable<Tip> FilterByKeyword(IQueryable<Tip> query, string keyword)
        {
            if (!keyword.NotEmpty())
            {
                return query;
            }

            var predicate = this.FilterByKeyword(keyword);
            return query.Where(predicate);
        }

        public IEnumerable<Tip> FindRelated(Tip item)
        {
            var rootItemId = item.GetListingRoot()?.Id;

            return Find(item, rootItemId, TipsConstants.RelatedContentSize, item.Parent?.Id);
        }

        public IEnumerable<Tip> FindTips(ITipsTaxonomy item, int maxCount = 0)
        {
            var gbItem = item as GlassBase;
            var rootItemId = gbItem.GetAscendantOfTemplate(LocalMarketSettingsConstants.TemplateId)?.Id ?? SitecoreContext.GetHomeItem<GlassBase>()?.Id;
            var contextItemId = SitecoreContext.GetCurrentItem<GlassBase>()?.Id;

            return Find(item, rootItemId, maxCount, contextItemId);
        }

        private IEnumerable<Tip> Find(ITipsTaxonomy item, Guid? rootItemId, int maxCount, Guid? parentId)
        {
            var filtered = GetQueryable().Where(x => x.Id != item.Id);

            if (rootItemId != null)
            {
                var rootItem = new ID(rootItemId.Value);
                var predicate = PredicateBuilder.Create<Tip>(x => x.Paths.Contains(rootItem));

                if (parentId.HasValue)
                {
                    var id = new ID(parentId.Value);

                    predicate = predicate.Or(x => (x.ParentId == id).Boost(BoostLevels.Secondary) || (x.Paths.Contains(id)).Boost(BoostLevels.Tertiary));
                }

                filtered = filtered.Where(predicate);
            }
            else
            {
                filtered = this.FilterLocal(filtered);
            }

            filtered = FilterRelated(filtered, item);

            var ordered = filtered
                    .GetResults()
                    .Hits
                    .OrderByDescending(x => x.Score)
                    .Select(x => x.Document)
                    .AsQueryable()
                ;

            if (maxCount > 0)
            {
                ordered = ordered.Take(maxCount);
            }

            return ordered
                .MapResults(SitecoreContext, true);
        }

        protected override IQueryable<Tip> GetQueryable()
        {
            return base.GetQueryable()
                .Where(x => x.TemplateId == TipConstants.TemplateId
                            || x.BaseTemplates.Contains(TipConstants.TemplateId.Guid));
        }

        protected Expression<Func<Tip, bool>> FilterByKeyword(string keyword)
        {
            var predicate = PredicateBuilder.False<Tip>();

            predicate = predicate.Or(x => x.Heading.StartsWith(keyword)
                                          || x.SearchableTipsTopicsText.StartsWith(keyword)
                                          || x.SearchableTipsLifestageText.StartsWith(keyword)
                                          || x.SearchableTipsAnimalSizeText.StartsWith(keyword));

            var terms = keyword.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            if (terms.Length > 1)
            {
                var firstWord = terms.FirstOrDefault();

                var predicateHeading = PredicateBuilder.True<Tip>();
                predicateHeading = predicateHeading.And(x => x.Heading.StartsWith(firstWord));
                predicateHeading = terms.Skip(1).Aggregate(predicateHeading, (current, term) => current.And(x => x.Heading.Contains(term)));

                var predicateTopics = PredicateBuilder.True<Tip>();
                predicateTopics = predicateTopics.And(x => x.SearchableTipsTopicsText.StartsWith(firstWord));
                predicateTopics = terms.Skip(1).Aggregate(predicateTopics, (current, term) => current.And(x => x.SearchableTipsTopicsText.Contains(term)));

                var predicateLifestages = PredicateBuilder.True<Tip>();
                predicateLifestages = predicateLifestages.And(x => x.SearchableTipsLifestageText.StartsWith(firstWord));
                predicateLifestages = terms.Skip(1).Aggregate(predicateLifestages, (current, term) => current.And(x => x.SearchableTipsLifestageText.Contains(term)));

                var predicateSize = PredicateBuilder.True<Tip>();
                predicateSize = predicateSize.And(x => x.SearchableTipsAnimalSizeText.StartsWith(firstWord));
                predicateSize = terms.Skip(1).Aggregate(predicateSize, (current, term) => current.And(x => x.SearchableTipsAnimalSizeText.Contains(term)));

                predicate = predicate.Or(predicateHeading);
                predicate = predicate.Or(predicateTopics);
                predicate = predicate.Or(predicateLifestages);
                predicate = predicate.Or(predicateSize);
            }

            return predicate;
        }

        protected IQueryable<Tip> FilterRelated(IQueryable<Tip> query, ITipsTaxonomy item)
        {
            var predicate = PredicateBuilder.True<Tip>();

            if (item.TipsTopics != null && item.TipsTopics.Any())
            {
                var p1 = PredicateBuilder.True<Tip>();
                p1 = item.TipsTopics.Aggregate(p1, (current, entry) => current.Or(x => x.TipsTopicIds.Contains(entry.Id).Boost(BoostLevels.Primary)));

                predicate = predicate.And(p1);
            }

            if (item.Lifestages != null && item.Lifestages.Any())
            {
                var p1 = PredicateBuilder.True<Tip>();
                p1 = item.Lifestages.Aggregate(p1, (current, entry) => current.Or(x => x.LifestageIds.Contains(entry.Id).Boost(BoostLevels.Secondary)));

                predicate = predicate.And(p1);
            }

            if (item.Sizes != null && item.Sizes.Any())
            {
                var p1 = PredicateBuilder.True<Tip>();
                p1 = item.Sizes.Aggregate(p1, (current, entry) => current.Or(x => x.SizeIds.Contains(entry.Id).Boost(BoostLevels.Secondary)));

                predicate = predicate.And(p1);
            }

            if (item.Breeds != null && item.Breeds.Any())
            {
                var p1 = PredicateBuilder.True<Tip>();
                p1 = item.Breeds.Aggregate(p1, (current, entry) => current.Or(x => x.BreedIds.Contains(entry.Id).Boost(BoostLevels.Secondary)));

                predicate = predicate.And(p1);
            }

            if (item.Species != null && item.Species.Any())
            {
                var p1 = PredicateBuilder.True<Tip>();
                p1 = item.Species.Aggregate(p1, (current, entry) => current.Or(x => x.SpecieIds.Contains(entry.Id).Boost(BoostLevels.Secondary)));

                predicate = predicate.And(p1);
            }

            query = query.Where(predicate);

            return query;
        }

        private static Expression<Func<Tip, bool>> BuildFilterGroupExpression(
            IDictionary<string, IEnumerable<Guid>> filterGroups)
        {
            var expression = PredicateBuilder.True<Tip>();
            foreach (var filterGroup in filterGroups)
            {
                var currentExpression = PredicateBuilder.True<Tip>();

                foreach (var filter in filterGroup.Value)
                {
                    var filterValue = filter;

                    currentExpression = currentExpression.Or(x => x.SearchableRelatedEntitiesIds.Contains(filterValue));
                }

                expression = expression.And(currentExpression);
            }

            return expression;
        }
    }
}
