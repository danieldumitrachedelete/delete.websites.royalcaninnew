﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Delete.Feature.Tips.Models;
using Delete.Feature.Tips.Models.Taxonomy;
using Delete.Foundation.DataTemplates.Models.Dictionary;
using Glass.Mapper.Sc;

namespace Delete.Feature.Tips.Managers
{
    public static class TipsCategoriesManager
    {
        public static IEnumerable<TipCategory> GetTipsCategoriesFromIds(IEnumerable<Guid> ids, TipsLanding context, ISitecoreContext sitecoreContext)
        {
            List<TipCategory> result = new List<TipCategory>();

            foreach (var id in ids)
            {
                var topic = sitecoreContext.GetItem<TipsTopic>(id);
                if (topic == null) continue;

                var category = new TipCategory();
                category.Id = topic.Id;
                category.Description = topic.Description ?? string.Empty;
                category.Title = topic.Text ?? string.Empty;
                category.IconClass = sitecoreContext.GetItem<DictionaryEntry>(topic.Icon)?.Value ?? string.Empty;

                var tipsUrl = context.Children.First(x => x.Name == "Tips").AbsoluteUrl;
                var uriBuilder = new UriBuilder(tipsUrl);
                var query = HttpUtility.ParseQueryString(uriBuilder.Query);
                query["topic"] = category.Id.ToString();
                uriBuilder.Query = query.ToString();
                var link = uriBuilder.ToString();

                category.Url = link;

                result.Add(category);
            }

            return result;
        }
    }
}