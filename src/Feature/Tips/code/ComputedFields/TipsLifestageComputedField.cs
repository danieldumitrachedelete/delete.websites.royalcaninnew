﻿using Delete.Feature.Tips.ComputedFields;
using Delete.Foundation.DataTemplates.Models.Taxonomy;

namespace Delete.Feature.Tips.ComputedFields
{
    public class TipsLifestageComputedField : BaseTipsComputedField
	{
        public TipsLifestageComputedField() : base(TaxonomyConstants.LifestagesFieldName)
        {
        }
    }
}