﻿using Delete.Foundation.DataTemplates.Models.Taxonomy;

namespace Delete.Feature.Tips.ComputedFields
{
    public class TipsAnimalSizeComputedField : BaseTipsComputedField
	{
        public TipsAnimalSizeComputedField() : base(TaxonomyConstants.SizesFieldName)
        {
        }
    }
}