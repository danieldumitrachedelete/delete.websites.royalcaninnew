﻿using Delete.Feature.Tips.Models.Taxonomy;

namespace Delete.Feature.Tips.ComputedFields
{
    public class TipsTopicsComputedField : BaseTipsComputedField
	{
        public TipsTopicsComputedField() : base(TipsTaxonomyConstants.TipsTopicsFieldName)
        {
        }
    }
}