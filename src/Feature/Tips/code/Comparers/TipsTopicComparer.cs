﻿using System.Collections.Generic;
using Delete.Feature.Tips.Models.Taxonomy;

namespace Delete.Feature.Tips.Comparers
{
    public class TipsTopicComparer : IEqualityComparer<TipsTopic>
    {
        public bool Equals(TipsTopic x, TipsTopic y)
        {
            if (x == null || y == null) return false;

            return x.Id == y.Id;
        }

        public int GetHashCode(TipsTopic obj)
        {
            return obj.Id.GetHashCode();
        }
    }
}