using AutoMapper;
using Delete.Feature.Tips.Models;
using Delete.Foundation.DataTemplates.Models;
using Delete.Foundation.DeleteFoundationCore.Extensions;

namespace Delete.Feature.Tips.Profiles
{
    public class TipsProfile : Profile
    {
        public TipsProfile()
        {
            CreateMap<Tip, PredictiveSearchItemJsonModel>()
                .ForMember(x => x.Title, opt => opt.MapFrom(z => z.DisplayName.OrDefault(z.Name)))
                .ForMember(x => x.Description, opt => opt.Ignore())
                .ForMember(x => x.Url, opt => opt.MapFrom(z => z.Url))
                ;
        }
    }
}
