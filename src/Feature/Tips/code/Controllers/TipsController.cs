using AutoMapper;
using Castle.Core.Internal;
using Delete.Feature.GenericComponents.Models;
using Delete.Feature.Tips.Comparers;
using Delete.Feature.Tips.Extensions;
using Delete.Feature.Tips.Managers;
using Delete.Feature.Tips.Models;
using Delete.Foundation.DataTemplates.Managers;
using Delete.Foundation.DataTemplates.Models;
using Delete.Foundation.DeleteFoundationCore.Controllers;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.DeleteFoundationCore.Models;
using Delete.Foundation.MultiSite.Extensions;
using Delete.Foundation.MultiSite.SchemaOrg.Resolvers;
using Delete.Foundation.RoyalCaninCore.Models;
using Glass.Mapper.Sc;
using RoyalCanin.Project.Metadata.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Delete.Foundation.MultiSite.Models;

namespace Delete.Feature.Tips.Controllers
{
    public class TipsController : BaseController
    {
        private const string TipsCacheKey = "tips_{0}_{1}_{2}";
        private const string TipsCategoriesCacheKey = "tips_{0}_{1}_{2}_{3}";
        private const string RelatedTipsCacheKey = "relatedarticles_{0}";
        private const string TopQuestionsTipsCacheKey = "topquestionstips_{0}";
        private const string FeaturedContentCacheKey = "featuredcontent_{0}";
        private const string TipsByTaxonomyCacheKey = "tipsbytaxonomy_{0}_{1}";
        private const string TipsFilterGroupsCacheKey = "tipsfiltergroups_{0}_{1}_{2}_{3}";
        private const int SelectedTipsByTaxonomyMaxcount = 10;

        private readonly ITipsSearchManager tipsSearchManager;
        private readonly IFilterGroupFolderSearchManager filterGroupFolderSearchManager;

        public TipsController(
            ISitecoreContext sitecoreContext,
            IMapper mapper,
            ICacheManager cacheManager,
            ITipsSearchManager tipsSearchManager,
            IFilterGroupFolderSearchManager filterGroupFolderSearchManager) : base(sitecoreContext, mapper, cacheManager)
        {
            this.tipsSearchManager = tipsSearchManager;
            this.filterGroupFolderSearchManager = filterGroupFolderSearchManager;
        }

        [HttpGet]
        public ActionResult TipDataComponent()
        {
            var context = this.GetContextItem<Tip>();
            if (context == null)
            {
                return new EmptyResult();
            }

            return this.View("~/Views/Feature/Tips/TipDataComponent.cshtml", context);
        }

        [HttpGet]
        public ActionResult ParentLink(string topic = null)
        {
            var context = this.GetContextItem<Tip>();
            if (context == null)
            {
                return new EmptyResult();
            }

            var parent = context.Parent;
            string link = null;
            string title = null;

            if (!topic.IsNullOrEmpty())
            {
                var uriBuilder = new UriBuilder(parent.AbsoluteUrl);
                var query = HttpUtility.ParseQueryString(uriBuilder.Query);
                query["topic"] = topic;
                uriBuilder.Query = query.ToString();
                link = uriBuilder.ToString();

                if (topic != "any")
                {
                    Guid topicId = new Guid(topic);
                    title = this.SitecoreContext.GetCurrentItem<Tip>().TipsTopics?.FirstOrDefault(x => x.Id == topicId)?.Text;
                }
            }

            var model = new ParentLink
            {
                Title = title ?? context.Parent.DisplayName,
                Url = link ?? parent.AbsoluteUrl
            };

            return this.View("~/Views/Feature/Tips/ParentLink.cshtml", model);
        }

        [HttpGet]
        public ActionResult TipsCategoryList()
        {
            var contextItem = GetContextItem<TipsLanding>();
            var dataSource = GetDataSourceItem<TipFilterConfiguration>();

            var topicsIds = dataSource.PreFilterings.Select(x => x.Id);

            string cacheKey = string.Format(TipsCategoriesCacheKey, contextItem.CategoriesTitle, contextItem.Text, contextItem.SearchTitle, contextItem.Language);
            var categories = this._cacheManager.CacheResults(() => TipsCategoriesManager.GetTipsCategoriesFromIds(topicsIds, contextItem, SitecoreContext), cacheKey);

            var model = new TipsCategoryListViewModel
            {
                PageData = contextItem,
                Categories = categories
            };

            return this.View("~/Views/Feature/Tips/TipsCategoryList.cshtml", model);
        }

        [HttpGet]
        public ActionResult TipsLandingSearch()
        {
            var context = GetContextItem<TipsLanding>();
            if (context == null)
            {
                return new EmptyResult();
            }

            var model = new TipsLandingSearchViewModel()
            {
                PageData = context,
                SearchResultsPage = this.SitecoreContext.GetLocalMarketSettings().SearchResultsPage
            };

            return this.View("~/Views/Feature/Tips/TipsLandingSearch.cshtml", model);
        }

        public ActionResult TopQuestions()
        {
            var dataSource = GetDataSourceItem<SelectedTips>();
            IEnumerable<Tip> tips = new List<Tip>();

            if (dataSource != null)
            {
                if(dataSource.SelectedTipsList != null && dataSource.SelectedTipsList.Any())
                { 
                    var ids = dataSource.SelectedTipsList.Select(x => x.Id);
                    var cacheKey = string.Format(TopQuestionsTipsCacheKey, string.Join("-", ids.Select(x => x.ToString())));
                    tips = this._cacheManager.CacheResults(() => this.tipsSearchManager.GetAllWithIds(ids).MakeItemsInOrderOf(ids), cacheKey);
                }
                else
                {
                    int maxcount = dataSource.MaxCount != 0 ? dataSource.MaxCount : SelectedTipsByTaxonomyMaxcount;
                    var cacheKey = string.Format(TipsByTaxonomyCacheKey, string.Join("|", dataSource.TaxonomyIds), maxcount);
                    tips = this._cacheManager.CacheResults(() => tipsSearchManager.FindTips(dataSource, maxcount).ToList(), cacheKey);
                }
            }

            var model = new TopQuestionsViewModel()
            {
                PageData = dataSource,
                Tips = tips,
            };

            return this.View("~/Views/Feature/Tips/TipsTopQuestions.cshtml", model);
        }

        [HttpGet]
        public ActionResult TipsList(string topic = null)
        {
            var tipFilterConfiguration = this.GetDataSourceItem<TipFilterConfiguration>();
            if (tipFilterConfiguration == null)
            {
                return this.Content(Sitecore.Context.PageMode.IsExperienceEditorEditing ? "<p>Please select tip filter configuration</p>" : string.Empty);
            }

            var species = tipFilterConfiguration.Species;

            var contextItem = this.GetContextItem<GlassBase>();
            var listingRootId = tipFilterConfiguration.ListingRoot?.Id ?? contextItem.Id;

            var cacheKey = string.Format(TipsCacheKey, listingRootId, species.Id, string.Join("|", tipFilterConfiguration.PreFilterings));
            var tips = this._cacheManager.CacheResults(() => this.tipsSearchManager.GetAll(listingRootId, species?.Id, string.Empty, null).ToList(), cacheKey);

            var tipsTopicComparer = new TipsTopicComparer();
            var allTopics = tips.SelectMany(x => x.TipsTopics).Distinct(tipsTopicComparer).ToList();

            var result = new SearchViewModel
            {
                ParentItem = contextItem.Parent,
                FilterModel = GetSearchFilterViewModel(tipFilterConfiguration, tips, topic ?? "all"),
                SearchResultsModel = GetSearchResultsModel(tipFilterConfiguration, tips, allTopics, chosenTopic: topic)
            };

            return this.View("~/Views/Feature/Tips/TipsList.cshtml", result);
        }

        [HttpGet]
        public ActionResult Tips(TipsFilterViewModel filterConfiguration)
        {
            var tipsFilterConfiguration = this.SitecoreContext.GetItem<TipFilterConfiguration>(filterConfiguration.Filter, false, true);
            var species = tipsFilterConfiguration.Species;
            var listingRootId = tipsFilterConfiguration.ListingRoot?.Id;

            var preFiltering = tipsFilterConfiguration.PreFilterings.Select(x => x.Id).ToList();

            var filterModel = new TipsFilterModel(filterConfiguration)
            {
                Species = tipsFilterConfiguration.Species.Id,
                ListingRootId = tipsFilterConfiguration.ListingRoot?.Id,
            };

            var filterIds = this.GetRequestFilterIdsFromCheckboxes(filterConfiguration);

            var cacheKey = string.Format(TipsCacheKey, listingRootId, species?.Id, filterModel.SearchQuery, string.Join("|", filterIds.SelectMany(x => x.Value)));
            var allTips = this._cacheManager.CacheResults(() => this.tipsSearchManager.GetAll(listingRootId, species?.Id, filterModel.SearchQuery, filterIds).ToList(), cacheKey);

            var selectedTopicIds = filterConfiguration.TipsTopics.ToGuidList();
            if (!selectedTopicIds.Any())
            {
                selectedTopicIds = preFiltering;
            }
            var selectedTopics = allTips
                .SelectMany(x => x.TipsTopics)
                .Where(x => !selectedTopicIds.Any() || selectedTopicIds.Contains(x.Id))
                .Distinct(new TipsTopicComparer())
                .ToList();

            var searchResultModel = GetSearchResultsModel(tipsFilterConfiguration, allTips, selectedTopics);

            if (filterConfiguration.ViewAll)
            {
                return this.View("~/Views/Feature/Tips/_AllTipsListResults.cshtml", searchResultModel);
            }

            return this.View("~/Views/Feature/Tips/_TipsListResults.cshtml", searchResultModel);
        }

        [HttpGet]
        public ActionResult Search()
        {
            var tipFilterConfiguration = this.GetDataSourceItem<TipFilterConfiguration>();
            if (tipFilterConfiguration == null)
            {
                return this.Content(Sitecore.Context.PageMode.IsExperienceEditorEditing ? "<p>Please select tip filter configuration</p>" : string.Empty);
            }

            var species = tipFilterConfiguration.Species;
            var selectedFilterGroups = tipFilterConfiguration.Filters;

            var contextItem = this.GetContextItem<GlassBase>();

            var listingRootId = tipFilterConfiguration.ListingRoot?.Id ?? contextItem.Id;

            var preFiltering = tipFilterConfiguration
                .PreFilterings
                .GroupBy(x => x.Parent.Id);

            var filterIds = preFiltering
                .ToDictionary(x => x.First().Parent.Name, x => x.Select(y => y.Id));

            var cacheKey = string.Format(TipsCacheKey, listingRootId, species?.Id, null, string.Join("|", filterIds.SelectMany(x => x.Value)));
            var tips = this._cacheManager.CacheResults(() => this.tipsSearchManager.GetAll(listingRootId, species?.Id, null, filterIds).ToList(), cacheKey);
            //var tips = this.tipsSearchManager.GetAll(listingRootId, species?.Id, null, filterIds).ToList();

            var resultTips = tips;

            var groupCacheKey = string.Format(TipsFilterGroupsCacheKey, listingRootId, species?.Id, null, string.Join("|", filterIds.SelectMany(x => x.Value)));
            var allFilterGroups = this._cacheManager.CacheResults(() => this.filterGroupFolderSearchManager.GetAll(), groupCacheKey);
            var sortedFilterGroups = selectedFilterGroups
                .Select(x => x.Id)
                .Select(filterGroupId => allFilterGroups.FirstOrDefault(x => x.Id == filterGroupId))
                .Where(g => g != null)
                .ToList();

            var tipsTopicComparer = new TipsTopicComparer();
            var allTopics = resultTips.SelectMany(x => x.TipsTopics).Distinct(tipsTopicComparer).ToList();

            if (filterIds.Any(x => x.Value.Intersect(allTopics.Select(y => y.Id)).Any()))
            {
                allTopics = allTopics.Where(x => filterIds.Any(y => y.Value.Contains(x.Id))).ToList();
            }

            var result = new SearchViewModel
            {
                FilterModel = new SearchFilterViewModel
                {
                    FilterGroups = sortedFilterGroups,
                    PreFiltering = preFiltering.ToDictionary(x => x.Key, y => y.Select(x => x.Id)),
                    FilterRelatedSearchResults = tips,
                    Specie = species,
                    FilterId = tipFilterConfiguration.Id
                },
                SearchResultsModel = GetSearchResultsModel(tipFilterConfiguration, resultTips, allTopics)
            };

            return this.View("~/Views/Feature/Tips/Search.cshtml", result);
        }

        public ActionResult FeaturedContent()
        {
            var contextItem = this.GetContextItem<GlassBase>();

            if (contextItem == null)
            {
                return new EmptyResult();
            }

            var cacheKey = string.Format(FeaturedContentCacheKey, contextItem.Id);

            var tips = this._cacheManager.CacheResults(() =>
                    this.tipsSearchManager
                        .GetLatest(contextItem.Id, TipsConstants.FeaturedContentSize)
                        .ToList(),
                cacheKey);

            var result = new FeaturedContentModel
            {
                LeftItem = tips.FirstOrDefault(),
                RightItems = tips.Skip(1).ToList(),
            };

            return this.View("~/Views/Feature/Tips/FeaturedContent.cshtml", result);
        }


        public ActionResult RelatedTips()
        {
            var contextItem = this.GetContextItem<Tip>();

            if (contextItem == null || !contextItem.IsDerivedFrom(TipConstants.TemplateId))
            {
                return new EmptyResult();
            }

            var cacheKey = string.Format(RelatedTipsCacheKey, contextItem.Id);
            var tips = this._cacheManager.CacheResults(() => this.tipsSearchManager.FindRelated(contextItem).ToList(), cacheKey);

            return this.View("~/Views/Feature/Tips/RelatedTips.cshtml", tips);
        }

        public ActionResult CardPanel()
        {
            var sourceItem = this.GetDataSourceItem<TipsCardPanel>();

            if (sourceItem == null)
            {
                return new EmptyResult();
            }

            var tips = new List<Tip>();

            if (sourceItem.SpecifiedTips.Any())
            {
                tips.AddRange(sourceItem.SpecifiedTips);
            }
            else
            {
                //var cacheKey = string.Format(TipsByTaxonomyCacheKey, string.Join("|", sourceItem.TaxonomyIds), sourceItem.Count);
                //var filtered = this._cacheManager.CacheResults(() => tipsSearchManager.FindTips(sourceItem, sourceItem.Count).ToList(), cacheKey);
                var filtered = tipsSearchManager.FindTips(sourceItem, sourceItem.Count).ToList();
                tips.AddRange(filtered);
            }

            var model = new TipsCardPanelViewModel
            {
                Header = sourceItem.Header,
                Tips = tips,
                Spacers = sourceItem.Spacers,
                Width = sourceItem.Width
            };

            return this.View("~/Views/Feature/Tips/TipsCardPanel.cshtml", model);
        }

        public ActionResult Carousel()
        {
            var sourceItem = this.GetDataSourceItem<TipsCarousel>();
            var model = GetCarouselViewModel(sourceItem, false);

            if (model == null)
            {
                return new EmptyResult();
            }

            return this.View("~/Views/Feature/Tips/TipsCarousel.cshtml", model);
        }

        public ActionResult ExpandCarousel(Guid filter)
        {
            var sourceItem = this.SitecoreContext.GetItem<TipsCarousel>(filter);
            var model = GetCarouselViewModel(sourceItem, true);

            if (model == null)
            {
                return new EmptyResult();
            }

            return this.View("~/Views/Feature/Tips/TipsCarousel.cshtml", model);
        }

        private TipsCarouselViewModel GetCarouselViewModel(TipsCarousel sourceItem, bool gridView)
        {
            if (sourceItem == null)
            {
                return null;
            }

            var cacheKey = string.Format(TipsByTaxonomyCacheKey, string.Join("|", sourceItem.TaxonomyIds), sourceItem.Count);
            var tips = this._cacheManager.CacheResults(() => tipsSearchManager.FindTips(sourceItem).ToList(), cacheKey);

            var sourceIds = sourceItem.RelatedEntities;

            tips = tips
                .OrderByDescending(x =>
                {
                    var ids = x.RelatedEntities.ToList();

                    return (double)ids.Intersect(sourceIds).Count() / ids.Count();
                })
                .Take(sourceItem.Count)
                .ToList();

            var model = new TipsCarouselViewModel
            {
                FilterConfiguration = sourceItem.Id,
                Header = sourceItem.Header,
                Tips = tips,
                GridView = gridView,
            };

            return model;
        }

        [HttpGet]
        public ActionResult SearchTips(TipsFilterViewModel filterConfiguration)
        {
            var tipsFilterConfiguration = this.SitecoreContext.GetItem<TipFilterConfiguration>(filterConfiguration.Filter, false, true);
            var species = tipsFilterConfiguration.Species;
            var listingRootId = tipsFilterConfiguration.ListingRoot?.Id;

            var preFiltering = tipsFilterConfiguration.PreFilterings;

            var filterModel = new TipsFilterModel(filterConfiguration)
            {
                Species = tipsFilterConfiguration.Species.Id,
                ListingRootId = tipsFilterConfiguration.ListingRoot?.Id,
            };

            var cacheKey = string.Format(TipsCacheKey, listingRootId, species?.Id, filterModel.SearchQuery, filterConfiguration.SortBy);
            //var allTips = this._cacheManager.CacheResults(() => this.tipsSearchManager.GetAll(listingRootId, species?.Id, filterModel.SearchQuery, null).ToList(), cacheKey);
            var allTips = this.tipsSearchManager.GetAll(listingRootId, species?.Id, string.Empty, null).ToList();

            var selectedTopicIds = filterConfiguration.SortBy.IsNullOrEmpty() || filterConfiguration.SortBy.Equals("all")
                ? preFiltering.Select(x => x.Id).ToList()
                : new List<Guid>(new[] { new Guid(filterConfiguration.SortBy) });

            var selectedTopics = allTips
                .SelectMany(x => x.TipsTopics)
                .Where(x => !selectedTopicIds.Any() || selectedTopicIds.Contains(x.Id))
                .Distinct(new TipsTopicComparer())
                .ToList();

            var result = new SearchViewModel
            {
                FilterModel = GetSearchFilterViewModel(tipsFilterConfiguration, allTips, filterConfiguration.SortBy),
                SearchResultsModel = GetSearchResultsModel(tipsFilterConfiguration, allTips, selectedTopics, filterConfiguration.Page, filterConfiguration.SortBy)
            };

            return this.View("~/Views/Feature/Tips/_UpdatedFilter.cshtml", result);
        }

        private SearchFilterViewModel GetSearchFilterViewModel(TipFilterConfiguration tipFilterConfiguration, List<Tip> tips, string chosenOption = null)
        {
            var preFiltering = tipFilterConfiguration
                .PreFilterings
                .GroupBy(x => x.Parent.Id)
                .ToDictionary(x => x.Key, y => y.Select(x => x.Id));

            var allFilterGroups = this.filterGroupFolderSearchManager.GetAll();
            var selectedFilterGroups = tipFilterConfiguration.Filters;

            var sortedFilterGroups = selectedFilterGroups
                .Select(x => x.Id)
                .Select(filterGroupId => allFilterGroups.FirstOrDefault(x => x.Id == filterGroupId))
                .Where(g => g != null)
                .ToList();

            return new SearchFilterViewModel
            {
                ChosenOption = chosenOption ?? "all",
                FilterGroups = sortedFilterGroups,
                PreFiltering = preFiltering,
                FilterRelatedSearchResults = tips,
                Specie = tipFilterConfiguration.Species,
                FilterId = tipFilterConfiguration.Id
            };
        }

        private IDictionary<string, IEnumerable<Guid>> GetRequestFilterIdsFromCheckboxes(TipsFilterViewModel filterConfiguration)
        {
            var filterValues = new Dictionary<string, IEnumerable<Guid>>();

            var tipsTopics = filterConfiguration.TipsTopics.ToGuidList();

            var lifestages = filterConfiguration.Lifestages.ToGuidList();

            var sizes = filterConfiguration.Sizes.ToGuidList();

            if (tipsTopics.Any())
            {
                filterValues.Add(nameof(filterConfiguration.TipsTopics), tipsTopics);
            }

            if (lifestages.Any())
            {
                filterValues.Add(nameof(filterConfiguration.Lifestages), lifestages);
            }

            if (sizes.Any())
            {
                filterValues.Add(nameof(filterConfiguration.Sizes), sizes);
            }

            return filterValues;
        }

        private static SearchResultsViewModel GetSearchResultsModel(
            TipFilterConfiguration tipsFilterConfiguration,
            List<Tip> resultTips,
            List<Models.Taxonomy.TipsTopic> allTopics,
            int page = 1,
            string chosenTopic = null)
        {
            IEnumerable<Tip> tips = resultTips;
            tips = tips.Where(x => x.TipsTopics.Select(y => y.Id).Intersect(allTopics.Select(y => y.Id)).Any());
            tips = tips.OrderBy(x => x.Name);

            if (!chosenTopic.IsNullOrEmpty() && !chosenTopic.Equals("all"))
            {
                var topicId = new Guid(chosenTopic);
                tips = tips.Where(x => x.TipsTopics.Select(y => y.Id).Contains(topicId));
            }

            var totalCount = tips.ToArray().Length;

            tips = tips.Skip((page - 1) * tipsFilterConfiguration.PageSize);
            tips = tips.Take(tipsFilterConfiguration.PageSize);

            return new SearchResultsViewModel
            {
                MainFilterGroup = tipsFilterConfiguration.MainFilter,
                Specie = tipsFilterConfiguration.Species,
                FullListTitle = tipsFilterConfiguration.FullListTitle,
                Pagination = new PaginatedResults
                {
                    PageSize = tipsFilterConfiguration.PageSize,
                    TotalResults = totalCount,
                    Page = page
                },
                ChosenOption = chosenTopic ?? "all",
                Tips = tips.ToList()
            };
        }

        [HttpGet]
        public ActionResult FindByKeyword(Guid? filter, string keyword = "")
        {
            var model = new PredictiveSearchJsonModel();

            if (filter.HasValue)
            {
                var tipsFilterConfiguration = this.SitecoreContext.GetItem<TipFilterConfiguration>(filter.Value, false, true);
                var species = tipsFilterConfiguration.Species;
                var listingRootId = tipsFilterConfiguration.ListingRoot?.Id;

                var preFiltering = tipsFilterConfiguration.PreFilterings.Select(x => x.Id).ToList();
                var filterIds = new Dictionary<string, IEnumerable<Guid>> { { string.Empty, preFiltering } };

                var cacheKey = string.Format(TipsCacheKey, listingRootId, species?.Id, keyword, string.Join("|", preFiltering));
                var tips = this._cacheManager.CacheResults(() => this.tipsSearchManager.GetAll(listingRootId, species?.Id, keyword, filterIds).ToList(), cacheKey);

                var predictiveItems = this._mapper.Map<IEnumerable<PredictiveSearchItemJsonModel>>(tips);
                model.Items = predictiveItems;
            }

            return this.Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult TipsSocialLinks()
        {
            var currentPage = this.SitecoreContext.GetCurrentItem<Tip>();
            var localMarketSettings = this.SitecoreContext.GetLocalMarketSettings();

            var model = new SocialLinksModel
            {
                CurrentPageAbsoluteUrl = currentPage.AbsoluteUrl,
                CurrentPageTitle = currentPage.Heading,
                TwitterHashTags = localMarketSettings?.TwitterHashtags ?? string.Empty,
                FacebookHashTags = localMarketSettings?.FacebookHashtags ?? string.Empty
            };
            return this.View("~/Views/Feature/Tips/SocialLinks.cshtml", model);
        }
    }
}
