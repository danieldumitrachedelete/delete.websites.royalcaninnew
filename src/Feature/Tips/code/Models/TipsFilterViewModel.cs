﻿using System;

namespace Delete.Feature.Tips.Models
{
    public class TipsFilterViewModel
	{
        public string SortBy { get; set; }

        public string SearchQuery { get; set; }

        public Guid Filter { get; set; }

        public string Sizes { get; set; }

        public string TipsTopics { get; set; }

        public string Lifestages { get; set; }

        public bool ViewAll { get; set; }

        public int Page { get; set; }
    }
}