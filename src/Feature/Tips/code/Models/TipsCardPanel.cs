﻿using System.Collections.Generic;
using System.Linq;
using Delete.Foundation.DeleteFoundationCore.Extensions;

namespace Delete.Feature.Tips.Models
{
	public partial class TipsCardPanel
	{
	    public IEnumerable<Tip> SpecifiedTips
		{
	        get
	        {
	            return
					Tips
						.Where(x => x.IsDerivedFrom(TipConstants.TemplateId))
	                ;
	        }
	    }
	}
}
