﻿using System;
using System.Collections.Generic;

namespace Delete.Feature.Tips.Models
{
    public class FeaturedContentModel
    {
        public Tip LeftItem { get; set; }
        public List<Tip> RightItems { get; set; }

        public bool HasContent => LeftItem != null && RightItems.Count == 2;
    }
}