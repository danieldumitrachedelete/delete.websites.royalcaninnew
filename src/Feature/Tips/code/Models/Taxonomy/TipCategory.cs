﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Delete.Feature.Tips.Models.Taxonomy
{
    public class TipCategory
    {
        public string IconClass { get; set; }
        public string Description { get; set; }
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
    }
}