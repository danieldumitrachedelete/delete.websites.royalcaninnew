﻿using System.Collections.Generic;
using Delete.Foundation.DataTemplates.Models.Folder;
using Delete.Foundation.DataTemplates.Models.Taxonomy;
using Delete.Foundation.DeleteFoundationCore.SchemaOrg;
using Delete.Foundation.DeleteFoundationCore.SchemaOrg.Interfaces;
using Delete.Foundation.RoyalCaninCore.Models;

namespace Delete.Feature.Tips.Models
{
    public class SearchResultsViewModel : IMicrodata
    {
        public FilterGroupFolder MainFilterGroup { get; set; }

        public Specie Specie { get; set; }

        public string FullListTitle { get; set; }

        public PaginatedResults Pagination { get; set; }

        public List<Tip> Tips { get; set; }

        public string ChosenOption { get; set; }

        public Thing GetMicrodata(IAuthorResolver authorResolver)
        {
            var questions = new List<Question>();
            foreach (var tip in this.Tips)
            {
                questions.Add(new Question
                {
                    Name = $"<a href=\"{tip.Url}\">{tip.Heading}</a>",
                    AcceptedAnswer = new Answer
                    {
                        Name = $"<a href=\"{tip.Url}\">{tip.Heading}</a>",
                        Text = tip.Summary
                    }
                });
            }

            var result = new FAQPage
            {
                MainEntity = questions.ToArray()
            };

            return result;
        }
    }
}