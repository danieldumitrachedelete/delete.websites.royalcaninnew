﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Delete.Feature.Tips.Models.Taxonomy;

namespace Delete.Feature.Tips.Models
{
    public class TipsCategoryListViewModel
    {
        public TipsLanding PageData { get; set; }
        public IEnumerable<TipCategory> Categories { get; set; }
    }
}