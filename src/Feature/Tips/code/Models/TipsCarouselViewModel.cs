﻿using System;
using System.Collections.Generic;

namespace Delete.Feature.Tips.Models
{
    public class TipsCarouselViewModel
	{
        public List<Tip> Tips { get; set; }

        public Guid FilterConfiguration { get; set; }

        public string Header { get; set; }

        public bool GridView { get; set; }
    }
}
