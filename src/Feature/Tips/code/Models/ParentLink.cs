﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Delete.Feature.Tips.Models
{
    public class ParentLink
    {
        public string Url { get; set; }
        public string Title { get; set; }
    }
}