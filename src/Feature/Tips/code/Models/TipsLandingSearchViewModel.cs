﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Delete.Feature.GenericComponents.Models;
using Delete.Foundation.DeleteFoundationCore.Models;
using Glass.Mapper.Sc.Fields;

namespace Delete.Feature.Tips.Models
{
    public class TipsLandingSearchViewModel
    {
        public TipsLanding PageData { get; set; }
        public Link SearchResultsPage { get; set; }
    }
}