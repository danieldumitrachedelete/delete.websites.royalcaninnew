﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Delete.Feature.Tips.Models.Taxonomy;
using Delete.Foundation.DataTemplates.Interfaces;
using Delete.Foundation.DataTemplates.Models.Taxonomy;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Models;
using Delete.Foundation.DeleteFoundationCore.SchemaOrg;
using Delete.Foundation.DeleteFoundationCore.SchemaOrg.Extensions;
using Delete.Foundation.DeleteFoundationCore.SchemaOrg.Interfaces;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Sitecore.ContentSearch;
using DictionaryEntry = Delete.Foundation.DataTemplates.Models.Dictionary.DictionaryEntry;

namespace Delete.Feature.Tips.Models
{
    using System.ComponentModel;
    using Sitecore.ContentSearch.Converters;

    public partial interface ITip : IFilterRelatedEntities
    {
    }


    /// <summary>
    /// As there are no possibilities in Code Generation to inherit one base template and implement fields from another base template
    /// and because we have to inherit taxonomy class for searching
    /// we do this dirty hack in hope to fix Code Generator in some future
    /// </summary>
    public static partial class TipConstants
	{
        public const string DateFieldId = "{ee1c4d9f-5117-45f7-b0bd-fb6b12d19c95}";
        public const string HeadingFieldId = "{3e0d3222-fc81-4105-8496-f340ba2219b8}";
        public const string SummaryFieldId = "{dcbe0623-bbd4-4d7b-91c8-9339fe0400e6}";
        public const string ThumbnailImageFieldId = "{6fd15459-5a23-42fe-a352-6894c12fe253}";
        public const string ContentFieldId = "{4bfa1c61-4105-4f2b-ae99-7b0e91879c17}";
        public const string HeroImageFieldId = "{fe7aa554-e2a8-47be-8206-1fa76ea9503e}";
        //author section
        public const string AuthorSubtitleId = "{bdda2c16-851a-42a3-988f-7b6570dfe8fc}";
        public const string AuthorImageId = "{aa3e34f2-1656-4377-9394-5c6dc69bb6d9}";
        public const string AuthorNameId = "{921abf33-a827-4c78-812e-91cd189d56be}";
        public const string AuthorTitleId = "{84f598e5-9009-4c4a-a695-3ca68d3a203a}";

        public const string DateFieldName = "Date";
        public const string HeadingFieldName = "Heading";
        public const string SummaryFieldName = "Summary";
        public const string ThumbnailImageFieldName = "Thumbnail Image";
        public const string ContentFieldName = "Content";
        public const string HeroImageFieldName = "Hero Image";
        // author section
        public const string AuthorSubtitle = "Author Subtitle";
        public const string AuthorImage = "Author Image";
        public const string AuthorName = "Author Name";
        public const string AuthorTitle = "Author Title";

    }

    public partial class Tip : IMicrodata
    {
        [SitecoreField(FieldId = TipConstants.DateFieldId, FieldName = TipConstants.DateFieldName)]
        [IndexField(TipConstants.DateFieldName)]
        public virtual DateTime Date { get; set; }

        [SitecoreField(FieldId = TipConstants.HeadingFieldId, FieldName = TipConstants.HeadingFieldName)]
        [IndexField(TipConstants.HeadingFieldName)]
        public virtual string Heading { get; set; }

        [SitecoreField(FieldId = TipConstants.SummaryFieldId, FieldName = TipConstants.SummaryFieldName)]
        [IndexField(TipConstants.SummaryFieldName)]
        public virtual string Summary { get; set; }

        [SitecoreField(FieldId = TipConstants.ThumbnailImageFieldId, FieldName = TipConstants.ThumbnailImageFieldName)]
        [IndexField(TipConstants.ThumbnailImageFieldName)]
        public virtual ExtendedImage ThumbnailImage { get; set; }

        [SitecoreField(FieldId = TipConstants.ContentFieldId, FieldName = TipConstants.ContentFieldName, ReadOnly = true)]
        [IndexField(TipConstants.ContentFieldName)]
        public virtual string Content { get; set; }

        [SitecoreField(FieldId = TipConstants.HeroImageFieldId, FieldName = TipConstants.HeroImageFieldName)]
        [IndexField(TipConstants.HeroImageFieldName)]
        public virtual Delete.Foundation.DeleteFoundationCore.Models.ExtendedImage HeroImage { get; set; }
        
        // author section start
        [SitecoreField(FieldId = TipConstants.AuthorSubtitleId, FieldName = TipConstants.AuthorSubtitle)]
        [IndexField(TipConstants.AuthorSubtitle)]
        public virtual string AuthorSubtitle { get; set; }

        [SitecoreField(FieldId = TipConstants.AuthorImageId, FieldName = TipConstants.AuthorImage)]
        [IndexField(TipConstants.AuthorImage)]
        public virtual Image AuthorImage { get; set; }

        [SitecoreField(FieldId = TipConstants.AuthorNameId, FieldName = TipConstants.AuthorName)]
        [IndexField(TipConstants.AuthorName)]
        public virtual string AuthorName { get; set; }

        [SitecoreField(FieldId = TipConstants.AuthorTitleId, FieldName = TipConstants.AuthorTitle)]
        [IndexField(TipConstants.AuthorTitle)]
        public virtual string AuthorTitle { get; set; }

        // author section end

        [IndexField("tipsTopics")]
        public virtual string SearchableTipsTopicsText { get; set; }

        [IndexField("tipsLifestage")]
        public virtual string SearchableTipsLifestageText { get; set; }

        [IndexField("tipsAnimalSize")]
        public virtual string SearchableTipsAnimalSizeText { get; set; }

        [IndexField("tipsRelatedEntities")]
        public virtual List<Guid> SearchableRelatedEntitiesIds { get; set; }

        public IEnumerable<DictionaryEntry> TaxonomyTags => TipsTopics.Union(Species).Union(Sizes).Where(x => x?.Text.NotEmpty() == true);
        
        public GlassBase GetListingRoot()
        {
            var parent = Parent;

            while (parent != null && parent.TemplateId == TipConstants.TemplateId)
            {
                parent = parent.Parent;
            }

            return parent?.TemplateId != TipConstants.TemplateId ? parent : Parent;
        }

        public Thing GetMicrodata(IAuthorResolver authorResolver)
        {

            var author = new Person
            {
                Name = AuthorName,
                Url = new Uri(Url ?? String.Empty, UriKind.RelativeOrAbsolute)
            };

            var result = new QAPage
            {
                MainEntity = new Question
                {
                    Name = Heading ?? String.Empty,
                    Text = Heading ?? String.Empty,
                    DateCreated = Created.ToDateTimeOffset(),
                    Author = author,
                    AnswerCount = 1,
                    AcceptedAnswer = new Answer
                    {
                        Text = StripHtmlFromText(Content) ?? String.Empty,
                        Author = author,
                        DateCreated = Created.ToDateTimeOffset(),
                        Url = new Uri(Url ?? String.Empty, UriKind.RelativeOrAbsolute),
                        UpvoteCount = 1
                    }
                }
            };
            return result;
        }

        public string StripHtmlFromText(string text)
        {
            string result = Regex.Replace(text, @"<(.|\n)*?>", string.Empty);
            result = result.Replace("&nbsp;", " ");
            result = result.Replace("\n", " ");

            return result;
        }
    }
}
