﻿// <autogenerated>
//   This file was generated by T4 code generator ScriptRunner.tt.
//   Any changes made to this file manually will be lost next time the file is regenerated.
// </autogenerated>

using System;
using Sitecore.Data;
using Sitecore.Common;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Sitecore.Globalization;
using System.ComponentModel;
using Delete.Foundation.DeleteFoundationCore.Models;
using Sitecore.ContentSearch;
using Delete.Feature.Tips;

namespace Delete.Feature.Tips.Models
{
	public partial interface ITip : IGlassBase, Delete.Feature.Tips.Models.Taxonomy.ITipsTaxonomy, Delete.Foundation.DataTemplates.Models.Interfaces.I_PostItem
	{
	}

	public static partial class TipConstants {
		public const string TemplateIdString = "{068881bc-1768-4f2e-81ce-3f222c3dabc1}";
		public static readonly ID TemplateId = new ID(TemplateIdString);
		public const string TemplateName = "Tip";

	
	
	}

	
	/// <summary>
	/// Tip
	/// <para>Path: /sitecore/templates/Feature/Tips/Tip</para>	
	/// <para>ID: {068881bc-1768-4f2e-81ce-3f222c3dabc1}</para>	
	/// </summary>
	[SitecoreType(TemplateId=TipConstants.TemplateIdString)] //, Cachable = true
	public partial class Tip  : Delete.Feature.Tips.Models.Taxonomy.TipsTaxonomy, ITip
	{
	
	}
}
