﻿// <autogenerated>
//   This file was generated by T4 code generator ScriptRunner.tt.
//   Any changes made to this file manually will be lost next time the file is regenerated.
// </autogenerated>

using System;
using Sitecore.Data;
using Sitecore.Common;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Sitecore.Globalization;
using System.ComponentModel;
using Delete.Foundation.DeleteFoundationCore.Models;
using Sitecore.ContentSearch;
using Delete.Feature.Tips;

namespace Delete.Feature.Tips.Models
{
	public partial interface ITipsCardPanel : IGlassBase, global::Delete.Feature.Tips.Models.Taxonomy.ITipsTaxonomy, global::Delete.Foundation.DataTemplates.Models.Interfaces.I_BaseLayout
	{
		Int32 Count  {get; set;}
		string Header  {get; set;}
		IEnumerable<Delete.Feature.Tips.Models.Tip> Tips  {get; set;}
	}

	public static partial class TipsCardPanelConstants {
		public const string TemplateIdString = "{f7b57343-064e-4b6b-8fbd-014ebee4fff1}";
		public static readonly ID TemplateId = new ID(TemplateIdString);
		public const string TemplateName = "Tips Card Panel";

		public const string CountFieldId = "{9bd72675-d238-48e2-9d92-a1c09c8f8bac}";
		public const string HeaderFieldId = "{3da58115-cfb4-4706-8977-0f549d93d769}";
		public const string TipsFieldId = "{fd301bd6-3312-4d72-b497-1e03164daba6}";
		public const string TipsTopicsFieldId = "{44e0dd8a-fe74-4848-ad07-7076972489b5}";
		public const string BreedsFieldId = "{beaa9dd6-5ed0-4689-9adc-8e86aedc5cee}";
		public const string ContentTagsFieldId = "{62e4ef91-adbf-439c-9758-53d73a102346}";
		public const string LifestagesFieldId = "{f3dc0387-7c59-4bea-a855-25f2e8a1577c}";
		public const string SizesFieldId = "{59450a7d-2396-4bd0-a83d-c5e295519e36}";
		public const string SpeciesFieldId = "{10c063f0-c785-4440-9751-567adc409edf}";
		public const string ExcludeFromSearchFieldId = "{5dfd5573-00e5-4c68-b6cd-49ddde34abfc}";
		public const string ExcludedRenderingsFieldId = "{3e48eb52-8ae3-4be2-ae53-05db0eb7265e}";
		public const string SpacersFieldId = "{99d4a39c-10e0-4e70-a79a-a581b189566a}";
		public const string WidthFieldId = "{9a5b83f0-5d62-4ebd-8aa6-56675499d228}";
	
		public const string CountFieldName = "Count";
		public const string HeaderFieldName = "Header";
		public const string TipsFieldName = "Tips";
		public const string TipsTopicsFieldName = "Tips Topics";
		public const string BreedsFieldName = "Breeds";
		public const string ContentTagsFieldName = "Content Tags";
		public const string LifestagesFieldName = "Lifestages";
		public const string SizesFieldName = "Sizes";
		public const string SpeciesFieldName = "Species";
		public const string ExcludeFromSearchFieldName = "Exclude From Search";
		public const string ExcludedRenderingsFieldName = "Excluded Renderings";
		public const string SpacersFieldName = "Spacers";
		public const string WidthFieldName = "Width";
	
	}

	
	/// <summary>
	/// TipsCardPanel
	/// <para>Path: /sitecore/templates/Feature/Tips/Tips Card Panel</para>	
	/// <para>ID: {f7b57343-064e-4b6b-8fbd-014ebee4fff1}</para>	
	/// </summary>
	[SitecoreType(TemplateId=TipsCardPanelConstants.TemplateIdString)] //, Cachable = true
	public partial class TipsCardPanel  : GlassBase, ITipsCardPanel
	{
		/// <summary>
		/// The Count field.
		/// <para>Field Type: Integer</para>		
		/// <para>Field ID: {9bd72675-d238-48e2-9d92-a1c09c8f8bac}</para>
		/// </summary>
		[SitecoreField(FieldId = TipsCardPanelConstants.CountFieldId, FieldName = TipsCardPanelConstants.CountFieldName)]
		[IndexField(TipsCardPanelConstants.CountFieldName)]
		public virtual Int32 Count  {get; set;}

		/// <summary>
		/// The Header field.
		/// <para>Field Type: Single-Line Text</para>		
		/// <para>Field ID: {3da58115-cfb4-4706-8977-0f549d93d769}</para>
		/// </summary>
		[SitecoreField(FieldId = TipsCardPanelConstants.HeaderFieldId, FieldName = TipsCardPanelConstants.HeaderFieldName)]
		[IndexField(TipsCardPanelConstants.HeaderFieldName)]
		public virtual string Header  {get; set;}

		/// <summary>
		/// The Tips field.
		/// <para>Field Type: Treelist</para>		
		/// <para>Field ID: {fd301bd6-3312-4d72-b497-1e03164daba6}</para>
		/// </summary>
		[SitecoreField(FieldId = TipsCardPanelConstants.TipsFieldId, FieldName = TipsCardPanelConstants.TipsFieldName)]
		[IndexField(TipsCardPanelConstants.TipsFieldName)]
		public virtual IEnumerable<Delete.Feature.Tips.Models.Tip> Tips  {get; set;}

		/// <summary>
		/// The Tips Topics field.
		/// <para>Field Type: Multilist</para>		
		/// <para>Field ID: {44e0dd8a-fe74-4848-ad07-7076972489b5}</para>
		/// </summary>
		[SitecoreField(FieldId = TipsCardPanelConstants.TipsTopicsFieldId, FieldName = TipsCardPanelConstants.TipsTopicsFieldName)]
		[IndexField(TipsCardPanelConstants.TipsTopicsFieldName)]
		public virtual IEnumerable<Delete.Feature.Tips.Models.Taxonomy.TipsTopic> TipsTopics  {get; set;}

		/// <summary>
		/// The Breeds field.
		/// <para>Field Type: Treelist</para>		
		/// <para>Field ID: {beaa9dd6-5ed0-4689-9adc-8e86aedc5cee}</para>
		/// </summary>
		[SitecoreField(FieldId = TipsCardPanelConstants.BreedsFieldId, FieldName = TipsCardPanelConstants.BreedsFieldName)]
		[IndexField(TipsCardPanelConstants.BreedsFieldName)]
		public virtual IEnumerable<Delete.Foundation.DataTemplates.Models.Dictionary.DictionaryEntry> Breeds  {get; set;}

		/// <summary>
		/// The Content Tags field.
		/// <para>Field Type: Treelist</para>		
		/// <para>Field ID: {62e4ef91-adbf-439c-9758-53d73a102346}</para>
		/// </summary>
		[SitecoreField(FieldId = TipsCardPanelConstants.ContentTagsFieldId, FieldName = TipsCardPanelConstants.ContentTagsFieldName)]
		[IndexField(TipsCardPanelConstants.ContentTagsFieldName)]
		public virtual IEnumerable<Delete.Foundation.DataTemplates.Models.Dictionary.DictionaryEntry> ContentTags  {get; set;}

		/// <summary>
		/// The Lifestages field.
		/// <para>Field Type: Treelist</para>		
		/// <para>Field ID: {f3dc0387-7c59-4bea-a855-25f2e8a1577c}</para>
		/// </summary>
		[SitecoreField(FieldId = TipsCardPanelConstants.LifestagesFieldId, FieldName = TipsCardPanelConstants.LifestagesFieldName)]
		[IndexField(TipsCardPanelConstants.LifestagesFieldName)]
		public virtual IEnumerable<Delete.Foundation.DataTemplates.Models.Dictionary.DictionaryEntry> Lifestages  {get; set;}

		/// <summary>
		/// The Sizes field.
		/// <para>Field Type: Treelist</para>		
		/// <para>Field ID: {59450a7d-2396-4bd0-a83d-c5e295519e36}</para>
		/// </summary>
		[SitecoreField(FieldId = TipsCardPanelConstants.SizesFieldId, FieldName = TipsCardPanelConstants.SizesFieldName)]
		[IndexField(TipsCardPanelConstants.SizesFieldName)]
		public virtual IEnumerable<Delete.Foundation.DataTemplates.Models.Dictionary.DictionaryEntry> Sizes  {get; set;}

		/// <summary>
		/// The Species field.
		/// <para>Field Type: Multilist</para>		
		/// <para>Field ID: {10c063f0-c785-4440-9751-567adc409edf}</para>
		/// </summary>
		[SitecoreField(FieldId = TipsCardPanelConstants.SpeciesFieldId, FieldName = TipsCardPanelConstants.SpeciesFieldName)]
		[IndexField(TipsCardPanelConstants.SpeciesFieldName)]
		public virtual IEnumerable<Delete.Foundation.DataTemplates.Models.Dictionary.DictionaryEntry> Species  {get; set;}

		/// <summary>
		/// The Exclude From Search field.
		/// <para>Field Type: Checkbox</para>		
		/// <para>Field ID: {5dfd5573-00e5-4c68-b6cd-49ddde34abfc}</para>
		/// </summary>
		[SitecoreField(FieldId = TipsCardPanelConstants.ExcludeFromSearchFieldId, FieldName = TipsCardPanelConstants.ExcludeFromSearchFieldName)]
		[IndexField(TipsCardPanelConstants.ExcludeFromSearchFieldName)]
		public virtual Boolean ExcludeFromSearch  {get; set;}

		/// <summary>
		/// The Excluded Renderings field.
		/// <para>Field Type: Treelist</para>		
		/// <para>Field ID: {3e48eb52-8ae3-4be2-ae53-05db0eb7265e}</para>
		/// </summary>
		[SitecoreField(FieldId = TipsCardPanelConstants.ExcludedRenderingsFieldId, FieldName = TipsCardPanelConstants.ExcludedRenderingsFieldName)]
		[IndexField(TipsCardPanelConstants.ExcludedRenderingsFieldName)]
		public virtual IEnumerable<GlassBase> ExcludedRenderings  {get; set;}

		/// <summary>
		/// The Spacers field.
		/// <para>Field Type: Droplink</para>		
		/// <para>Field ID: {99d4a39c-10e0-4e70-a79a-a581b189566a}</para>
		/// </summary>
		[SitecoreField(FieldId = TipsCardPanelConstants.SpacersFieldId, FieldName = TipsCardPanelConstants.SpacersFieldName)]
		[IndexField(TipsCardPanelConstants.SpacersFieldName)]
		public virtual Delete.Foundation.DataTemplates.Models.Dictionary.DictionaryEntry Spacers  {get; set;}

		/// <summary>
		/// The Width field.
		/// <para>Field Type: Droplink</para>		
		/// <para>Field ID: {9a5b83f0-5d62-4ebd-8aa6-56675499d228}</para>
		/// </summary>
		[SitecoreField(FieldId = TipsCardPanelConstants.WidthFieldId, FieldName = TipsCardPanelConstants.WidthFieldName)]
		[IndexField(TipsCardPanelConstants.WidthFieldName)]
		public virtual Delete.Foundation.DataTemplates.Models.Dictionary.DictionaryEntry Width  {get; set;}

	
	}
}
