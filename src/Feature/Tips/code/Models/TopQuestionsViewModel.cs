﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Delete.Feature.Tips.Models
{
    public class TopQuestionsViewModel
    {
        public SelectedTips PageData { get; set; }
        public IEnumerable<Tip> Tips { get; set; }
    }
}