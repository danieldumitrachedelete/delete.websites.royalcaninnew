﻿using System.Collections.Generic;

namespace Delete.Feature.Tips.Models
{
    public class TipsCardPanelViewModel
	{
        public List<Tip> Tips { get; set; }

        public string Header { get; set; }


        public Delete.Foundation.DataTemplates.Models.Dictionary.DictionaryEntry Spacers { get; set; }

        public Delete.Foundation.DataTemplates.Models.Dictionary.DictionaryEntry Width { get; set; }
    }
}
