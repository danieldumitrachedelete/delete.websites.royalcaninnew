﻿using System.Collections.Generic;
using Delete.Foundation.DataTemplates.Models.Filter;

namespace Delete.Feature.Tips.Models
{
    public class SearchResultsGroupViewModel
    {
        public FilterItem Key { get; set; }

        public IEnumerable<Tip> Items { get; set; }

        public int ItemsMaxCount { get; set; }
    }
}