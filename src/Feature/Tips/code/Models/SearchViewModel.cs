﻿using Delete.Foundation.DeleteFoundationCore.Models;
using Sitecore.Data.Items;

namespace Delete.Feature.Tips.Models
{
    public class SearchViewModel
    {
        public GlassBase ParentItem { get; set; }
        public SearchFilterViewModel FilterModel { get; set; }

        public SearchResultsViewModel SearchResultsModel { get;set; }
    }
}