﻿using System;

namespace Delete.Feature.Tips.Models
{
    public class TipsFilterModel
	{
        public string SearchQuery { get; set; }

        public Guid Species { get; set; }

        public Guid? ListingRootId { get; set; }

        public string Sizes { get; set; }

        public string TipsTopics { get; set; }

        public string Lifestages { get; set; }

		public TipsFilterModel(TipsFilterViewModel viewModel)
        {
            if (viewModel == null) return;

            SearchQuery = viewModel.SearchQuery;
            Sizes = viewModel.Sizes;
            TipsTopics = viewModel.TipsTopics;
            Lifestages = viewModel.Lifestages;
        }
    }
}