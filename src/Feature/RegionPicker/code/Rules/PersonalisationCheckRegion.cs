﻿using System;
using System.Linq;
using System.Net.Configuration;
using System.Text;
using System.Web;
using Castle.Core.Internal;
using Delete.Feature.RegionPicker.Managers;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.DependencyInjection;
using Sitecore.Rules;
using Sitecore.Rules.Conditions;

namespace Delete.Feature.RegionPicker.Rules
{
    public class PersonalisationCheckRegion<T> : WhenCondition<T> where T : RuleContext
    {
        //private readonly IRegionSearchManager RegionSearchManager =  ServiceLocator.ServiceProvider.GetService<IRegionSearchManager>();
        
        public string RegionName { get; set; }

        protected override bool Execute(T ruleContext)
        {
            var manualRegionCookie = HttpContext.Current.Request.Cookies["ManualRegion"];
            if (manualRegionCookie == null)
            {
                var browserRegionСookie = HttpContext.Current.Request.Cookies["BrowserRegion"];
                if (browserRegionСookie == null)
                {
                    var serverRegionСookie = HttpContext.Current.Request.Cookies["ServerRegion"];
                    if (serverRegionСookie == null)
                    {
                        return false;
                    }
                    return string.Equals(HttpUtility.UrlDecode(serverRegionСookie.Value), RegionName, StringComparison.CurrentCultureIgnoreCase); 
                }
                return string.Equals(HttpUtility.UrlDecode(browserRegionСookie.Value), RegionName, StringComparison.CurrentCultureIgnoreCase);
            }
            return string.Equals(HttpUtility.UrlDecode(manualRegionCookie.Value), RegionName, StringComparison.CurrentCultureIgnoreCase);


        }
        
    }
}