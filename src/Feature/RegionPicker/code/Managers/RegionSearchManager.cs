﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.DeleteFoundationCore.Managers;
using Delete.Foundation.DataTemplates.Models;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Models;
using Sitecore.ContentSearch.Linq.Utilities;

namespace Delete.Feature.RegionPicker.Managers
{
    public interface IRegionSearchManager : IBaseSearchManager<Region>
    {
        Region GetRegionForYandex(string YandexValue, IGlassBase root = null);
        Region GetRegionForSitecore(string SitecoreValue, IGlassBase root = null);
        List<Region> GetUnMostPopularRegion(List<Guid> mostPopularRegions, IGlassBase root = null);
        List<Region> GetAllRegion(IGlassBase root = null);
    }
    public class RegionSearchManager : BaseSearchManager<Region>, IRegionSearchManager
    {
        public Region GetRegionForYandex(string YandexValue, IGlassBase root = null)
        {
            var query = this.GetQueryable();
            query = query.Where(x => x.RegionNameYandex == YandexValue);
            var queryInternal = this.FilterLocal(query, root);
            return queryInternal.AsEnumerable().MapResults(this.SitecoreContext, true).FirstOrDefault();
        }

        public List<Region> GetAllRegion(IGlassBase root = null)
        {
            var query = this.GetQueryable();
            var queryInternal = this.FilterLocal(query, root);
            return queryInternal.AsEnumerable().MapResults(this.SitecoreContext, true).ToList();
        }
        public Region GetRegionForSitecore(string SitecoreValue, IGlassBase root = null)
        {
            var query = this.GetQueryable();
            query = query.Where(x => x.RegionIdSitecore.Contains(SitecoreValue));
            var queryInternal = this.FilterLocal(query, root);
            return queryInternal.AsEnumerable().MapResults(this.SitecoreContext, true).FirstOrDefault();
        }
        
        public List<Region> GetUnMostPopularRegion(List<Guid> mostPopularRegions, IGlassBase root = null)
        {
            var query = this.GetQueryable();
            
            if (mostPopularRegions.Any())
            {
                var predicate = PredicateBuilder.True<Region>();
                predicate = mostPopularRegions.Aggregate(predicate, (current, mostPopularRegion) => current.And(x => x.Id != mostPopularRegion));
                query = query.Where(predicate).OrderBy(x => x.RegionNameYandex);
            }

            var queryInternal = this.FilterLocal(query, root);
            return queryInternal.AsEnumerable().MapResults(this.SitecoreContext, true).ToList();
        }

        protected override IQueryable<Region> GetQueryable()
        {
            return base.GetQueryable()
                .Where(x => x.TemplateId == RegionConstants.TemplateId
                            || x.BaseTemplates.Contains(RegionConstants.TemplateId.Guid));
        }
    }
}