﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Delete.Foundation.DataTemplates.Models;

namespace Delete.Feature.RegionPicker.Models
{
    public class SortedRegions
    {
        public List<Region> MostPopular { get; set; }
        public List<Region> Popular { get; set; }
        public List<Region> Other { get; set; }

    }
}