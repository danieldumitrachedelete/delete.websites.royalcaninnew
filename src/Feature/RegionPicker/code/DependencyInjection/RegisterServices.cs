using AutoMapper;
using Delete.Feature.RegionPicker.Managers;
using Delete.Foundation.InitializeDependencyInjection.Extensions;
using Microsoft.Extensions.DependencyInjection;
using Delete.Feature.RegionPicker.Repositories;
using Sitecore.DependencyInjection;

namespace Delete.Feature.RegionPicker.DependencyInjection
{
    public class RegisterServices : IServicesConfigurator
    {
        public void Configure(IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IRegionPickerRepository, RegionPickerRepository>();
            serviceCollection.AddScoped<IRegionSearchManager, RegionSearchManager>();

            serviceCollection.AddMvcControllersInCurrentAssembly();
        }
    }
}
