using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using AutoMapper;
using Delete.Feature.RegionPicker.Managers;
using Delete.Feature.RegionPicker.Models;
using Delete.Foundation.DataTemplates.Models;
using Delete.Foundation.DeleteFoundationCore;
using Delete.Foundation.DeleteFoundationCore.Controllers;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Glass.Mapper.Sc;
using Sitecore.Analytics;
using Sitecore.Pipelines.HttpRequest;

namespace Delete.Feature.RegionPicker.Controllers
{
    using System;
    using System.Web.Mvc;
    using Sitecore.Data;
    using Repositories;

    public class RegionPickerController : BaseController
    {
        private IRegionSearchManager RegionSearchManager;
        public RegionPickerController(
            ISitecoreContext sitecoreContext, 
            IMapper mapper, 
            ICacheManager cacheManager,
            IRegionSearchManager regionSearchManager) : base(sitecoreContext, mapper, cacheManager)
        {
            this.RegionSearchManager = regionSearchManager;
        }

        public ActionResult RegionPicker()
        {
            
            var mostPopularRegions = GetDataSourceItem<Regions>(inferType: true)?.MostPopularRegions.ToList();
            SortedRegions sortedRegions;
            if (mostPopularRegions != null)
            {
                var otherRegions = RegionSearchManager.GetUnMostPopularRegion(mostPopularRegions.Select(x => x.Id).ToList()).OrderBy(x => x.RegionNameYandex).ToList();
                sortedRegions = new SortedRegions
                {
                    MostPopular = mostPopularRegions,
                    Popular = otherRegions.Where(x => x.Popular).ToList(),
                    Other = otherRegions.Where(x => !x.Popular).ToList()
                };
                
            }
            else
            {
                var regions = RegionSearchManager.GetAllRegion().OrderBy(x => x.RegionNameYandex).ToList();
                sortedRegions = new SortedRegions
                {
                    Popular = regions.Where(x => x.Popular).ToList(),
                    Other = regions.Where(x => !x.Popular).ToList()
                };

            }
            return this.View("~/Views/Feature/RegionPicker/ManualRegionPicker.cshtml", sortedRegions);
        }        
    }
}
