﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Castle.Core.Internal;
using Delete.Feature.RegionPicker.Managers;
using Delete.Foundation.DeleteFoundationCore;
using Glass.Mapper.Sc;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.DependencyInjection;
using Sitecore.Pipelines.HttpRequest;

namespace Delete.Feature.RegionPicker.Pipeline
{
    public class DetectMyGeolocation : HttpRequestProcessor
    {
        private const string _LocationDetectionFailedCode = "Serverside location detection failed";

        private readonly ISitecoreContext sitecoreContext;
        private IGetIp GetIp;
        //private IRegionSearchManager RegionSearchManager;
        private readonly IRegionSearchManager RegionSearchManager = ServiceLocator.ServiceProvider.GetService<IRegionSearchManager>();

        public DetectMyGeolocation(
            ISitecoreContext sitecoreContext,
            IGetIp getIp)
        {
            this.sitecoreContext = sitecoreContext;
            this.GetIp = getIp;
            //this.RegionSearchManager = regionSearchManager;
        }

        public override void Process(HttpRequestArgs args)
        {
            var BrowserRegionСookie = args.HttpContext.Request.Cookies["BrowserRegion"];
            var ManualRegionCookie = args.HttpContext.Request.Cookies["ManualRegion"];
            if (BrowserRegionСookie?.Value == null || ManualRegionCookie?.Value == null)
            {
                var ServerRegionCookie = args.HttpContext.Request.Cookies["ServerRegion"];
                if (ServerRegionCookie?.Value == null)
                {
                    ServerRegionCookie = new HttpCookie("ServerRegion");
                    try
                    {
                        var ipString = GetIp.GetIpFromRequest(args.HttpContext);
                        var result = Sitecore.Analytics.Lookups.LookupManager.GetInformationByIp(ipString);
                        if (!result.Region.IsNullOrEmpty())
                        {
                            var regionItem = RegionSearchManager.GetRegionForSitecore(result.Region);
                            ServerRegionCookie.Value = regionItem != null ? HttpUtility.UrlEncode(regionItem.RegionNameYandex) : _LocationDetectionFailedCode;
                        }
                    }
                    catch
                    {
                        ServerRegionCookie.Value = _LocationDetectionFailedCode;
                    }
                }

                args.HttpContext.Response.Cookies.Set(ServerRegionCookie);
            }



        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
    }
}