﻿using System;
using System.Collections.Generic;
using System.Linq;
using Delete.Feature.LanguagePicker.Models;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.DeleteFoundationCore.Managers;
using Sitecore.Data.Items;

namespace Delete.Feature.LanguagePicker.Managers
{
    public interface ICountryService : IBaseSearchManager<Country>
    {
        IEnumerable<Country> GetIndexedResults(Item languagePickerFolder);
    }
    public class CountryService : BaseSearchManager<Country>, ICountryService
    {
        public IEnumerable<Country> GetIndexedResults(Item languagePickerFolder)
        {
            var filteredResults = GetQueryable().Where(x => x.Fullpath.StartsWith(languagePickerFolder.Paths.FullPath));

            return filteredResults.ToList();
        }

        protected override IQueryable<Country> GetQueryable()
        {
            return base.GetQueryable().Where(x => x.TemplateId == CountryConstants.TemplateId);
        }
    }
}