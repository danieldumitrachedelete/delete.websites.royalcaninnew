﻿using System.Collections.Generic;
using System.Linq;
using Delete.Feature.LanguagePicker.Models;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.DeleteFoundationCore.Managers;
using Sitecore.Data.Items;

namespace Delete.Feature.LanguagePicker.Managers
{
    public interface IRegionService : IBaseSearchManager<Region>
    {
        IEnumerable<Region> GetAll(Item languagePickerFolder);
    }
    public class RegionService : BaseSearchManager<Region>, IRegionService
    {
        public IEnumerable<Region> GetAll(Item languagePickerFolder)
        {
            var filteredResults = GetQueryable().Where(x => x.Fullpath.StartsWith(languagePickerFolder.Paths.FullPath)).ToList().GroupBy(x => x.Id).Select(y => y.First());
            return filteredResults.MapResults(SitecoreContext, true).SitecoreSort().ToList();
        }

        protected override IQueryable<Region> GetQueryable()
        {
            return base.GetQueryable().Where(x => x.TemplateId == RegionConstants.TemplateId);
        }
    }
}