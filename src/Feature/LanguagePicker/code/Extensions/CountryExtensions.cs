﻿using System;
using Delete.Feature.LanguagePicker.Models;
using Delete.Foundation.RoyalCaninCore.Extensions;
using Sitecore.Mvc.Extensions;
using Sitecore.StringExtensions;

namespace Delete.Feature.LanguagePicker.Extensions
{
    public static class CountryExtensions
    {
        public static bool IsLanguageUsed(this Country country, string languageCode)
        {
            return country?.RelatedLanguage != null
                   && (country.RelatedLanguage.GetRegionalIsoCodeWithFallback().Equals(languageCode, StringComparison.OrdinalIgnoreCase) ||
                       country.RelatedLanguage.Name.Equals(languageCode, StringComparison.OrdinalIgnoreCase));
                       
        }

        public static string GetWebSiteURL(this Country country)
        {
            if (country == null) return "/";

            if (country.WebsiteUrl.IsNullOrEmpty()) return $"/{country.UrlCode.ToLower()}";

            return country.WebsiteUrl;
        }

        public static string GetURLCode(this Country country)
        {
            if (country == null || country.UrlCode.IsEmpty()) return string.Empty;

            return $"/{country.UrlCode.ToLower()}";
        }
    }
}