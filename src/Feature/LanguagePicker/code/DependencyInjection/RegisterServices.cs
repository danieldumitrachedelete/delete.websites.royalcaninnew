using Delete.Feature.LanguagePicker.Managers;
using Delete.Foundation.InitializeDependencyInjection.Extensions;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.DependencyInjection;

namespace Delete.Feature.LanguagePicker.DependencyInjection
{
    public class RegisterServices : IServicesConfigurator
    {
        public void Configure(IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<ICountryService, CountryService>();
            serviceCollection.AddScoped<IRegionService, RegionService>();

            serviceCollection.AddMvcControllersInCurrentAssembly();
        }
    }
}
