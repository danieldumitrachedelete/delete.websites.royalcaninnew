using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Delete.Feature.LanguagePicker.Extensions;
using Delete.Feature.LanguagePicker.Managers;
using Delete.Feature.LanguagePicker.Models;
using Delete.Foundation.DeleteFoundationCore.Controllers;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Glass.Mapper.Sc;
using Sitecore.Data.Items;

namespace Delete.Feature.LanguagePicker.Controllers
{
    using System.Web.Mvc;

    using Foundation.RoyalCaninCore.Extensions;

    public class LanguagePickerController : BaseController
    {
        private const string AllRegionsCacheKey = "allregions";
        private readonly IRegionService regionService;
        private readonly ICountryService countryService;
        private readonly string languageRegionsCountriesFolderPath = "/sitecore/content/global configuration/system settings/feature/languagepicker";

        public LanguagePickerController(
            IRegionService regionService,
            ICountryService countryService,
            ISitecoreContext sitecoreContext,
            IMapper mapper,
            ICacheManager cacheManager) : base(sitecoreContext, mapper, cacheManager)
        {
            this.regionService = regionService;
            this.countryService = countryService;
        }

        public ActionResult LanguagePicker()
        {
            var regions = this.GetAllRegions();

            return this.View("~/Views/Feature/LanguagePicker/LanguagePicker.cshtml", regions);
        }

        public ActionResult Trigger()
        {
            var model = this.GetLanguagePickerViewModel();
            model.ThemeKey = "default";

            return this.View("~/Views/Feature/LanguagePicker/LanguagePickerTrigger.cshtml", model);
        }

        public ActionResult TriggerLight()
        {
            var model = this.GetLanguagePickerViewModel();
            model.ThemeKey = "inverse";

            return this.View("~/Views/Feature/LanguagePicker/LanguagePickerTrigger.cshtml", model);
        }

        private IEnumerable<Region> GetAllRegions()
        {
            var regions = this._cacheManager.CacheResults(() => this.regionService.GetAll(this.GetEntitiesFolder()).ToList(), AllRegionsCacheKey);
            foreach (var region in regions)
            {
                region.IsCurrent = region.Countries?.Any(x => x.IsLanguageUsed(Sitecore.Context.Language.Name)) ?? false;
            }

            return regions;
        }

        private LanguagePickerTriggerViewModel GetLanguagePickerViewModel()
        {
            var currentRegion = this.GetAllRegions().FirstOrDefault(x => x.IsCurrent);
            return new LanguagePickerTriggerViewModel()
            {
                Country = currentRegion?.Countries?.FirstOrDefault(x => x.IsLanguageUsed(Sitecore.Context.Language.Name))
            };
        }

        private Item GetEntitiesFolder()
        {
            return this._context.Database.GetItem(this.languageRegionsCountriesFolderPath);
        }
    }
}
