﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Delete.Feature.LanguagePicker.Models
{
    public class LanguagePickerTriggerViewModel
    {
        public Country Country { get; set; }

        public string ThemeKey { get; set; }
    }
}