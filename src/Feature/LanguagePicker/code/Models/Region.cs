﻿using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration.Attributes;

namespace Delete.Feature.LanguagePicker.Models
{
    public partial class Region
    {
        public bool IsCurrent { get; set; }

        [SitecoreChildren(InferType = true)]
        public IEnumerable<Country> Countries { get; set; }
    }
}
