import './styles.scss';
import BaseComponent from 'Foundation/src/js/BaseComponent';
import dLScriptsObserver from 'Foundation/src/js/dLScriptsObserver';
import GTM from 'Foundation/src/js/gtm/gtm';
import Utils from 'Foundation/src/js/utils';

export default class LanguagePicker extends BaseComponent {
    constructor(el) {
        super(el);
        this.items = [...this.el.querySelectorAll('[data-toggle]')];
        this.countries = [...this.el.querySelectorAll('[data-country-code]')];
        this.buttonsWrapper = this.el.querySelector('[data-toggle-group]');
        this.modal = document.querySelector('[data-modal-target="country-lang-selector"]');        
        this._isModalOpened = false;
        this.isClickInit = null;
        dLScriptsObserver.onLoad(() => {
            if (this.isModalOpened()) {
                this.init();
            } else {
                this.modalObserver();
            }
        });
    }

    static get tagName() {
        return 'languagepicker';
    }

    addListeners() {
        this.countries.forEach((item) => {
            this.addListener(item, 'click', e => this.onCountryClick(e));
        });
    }
    init() {        
        this.edgeFix();
        this.addListeners();
        this.initLanguagePicker();
    }
    isModalOpened() {
        return (!(this.modal.classList.contains('rc-hidden')));
    }
    modalObserver() {
        Utils.classWatcher(this.modal, 'rc-hidden', null, () => { // init when rc-hidden removed (modal is shown)
            // this timeout required to get DL scripts finish their work on modal opening
            setTimeout(() => {
                this.init();
            }, 100);
        });
    }

    edgeFix() {
        if (!(document.querySelector('html').classList.contains('edge'))) return;
        if (this.modal.classList.contains('rc-hidden')) {            
            Utils.classWatcher(this.modal, 'rc-hidden', null, () => {
                // this timeout required to get DL scripts finish their work on modal opening
                setTimeout(() => { 
                    window.RCDL.features.ToggleGroup.init('[data-toggle-group]', this.buttonsWrapper);
                }, 100);
            });
        } else {
            window.RCDL.features.ToggleGroup.init('[data-toggle-group]', this.buttonsWrapper);
        }
    }    
    
    initGTA() {
        this.items.forEach((item) => {
            this.addListener(item, 'click', e => this.onClick(e));
        });
    }
    onClick(e) {
        const item = Utils.getClosestNodes(e.target, '[data-ref="language-continent"]')[0];
        const trackCategory = item.getAttribute('data-track-category');
        const trackLabel = item.getAttribute('data-track-label');
        const trackEvent = trackCategory + ' ' + trackLabel;
        if (!this.isClickInit) {
            GTM.push(trackCategory, 'Click', trackLabel, trackEvent);
        } else {
            this.isClickInit = !this.isClickInit;
        }
    }
    onCountryClick(e) {
        const item = Utils.getClosestNodes(e.target, '[data-ref="language-country"]')[0];
        const countryCode = item.getAttribute('data-country-code');
        if (countryCode && countryCode.length) {
            this.setCookie('selected_country', countryCode, 365);
        }
    }
    setCookie(cname, cvalue, exdays) {
        const date = new Date();
        date.setTime(date.getTime() + exdays * 24 * 60 * 60 * 1000);
        const expires = 'expires=' + date.toUTCString();
        document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/';
    }
    initLanguagePicker() {
        // if no buttons stop function
        if (!this.buttonsWrapper.querySelector('[role=tab]')) return;

        this.items.forEach((item) => {
            if (item.getAttribute('data-active-continent')) {
                this.isClickInit = true;
                item.click();
            }
        });
        this.initGTA();
    }
}
