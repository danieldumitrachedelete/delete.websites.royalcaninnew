﻿using System;
using System.Web;

namespace Delete.Feature.NotFound.Repositories
{
    public static class ItemNotFoundStatusRepository
    {
        public static bool Get()
        {
            return HttpContext.Current.Items[Constants.NotFoundItemPropertyKey] != null 
                   && (bool)HttpContext.Current.Items[Constants.NotFoundItemPropertyKey];
        }

        public static void Set(bool status)
        {
            HttpContext.Current.Items[Constants.NotFoundItemPropertyKey] = status;
        }
    }
}