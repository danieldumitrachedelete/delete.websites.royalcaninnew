﻿using System;
using System.Net;
using System.Web;
using Delete.Feature.NotFound.Repositories;
using Sitecore.Pipelines.HttpRequest;

namespace Delete.Feature.NotFound.Pipeline
{
    public class SetNotFoundStatusCode : HttpRequestProcessor
    {
        public override void Process(HttpRequestArgs args)
        {
            if (!ItemNotFoundStatusRepository.Get()) return;

            HttpContext.Current.Response.StatusCode = (int) HttpStatusCode.NotFound;
            HttpContext.Current.Response.TrySkipIisCustomErrors = true;
        }
    }
}