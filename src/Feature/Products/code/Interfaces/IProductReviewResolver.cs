﻿using System.Collections.Generic;
using Delete.Foundation.DeleteFoundationCore.SchemaOrg;

namespace Delete.Feature.Products.Interfaces
{
    public interface IProductReviewResolver
    {
        AggregateRating GetAggregateRating(string productId);
        List<Review> GetReviews(string productId);
    }
}