﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using Delete.Feature.Products.Models;
using Delete.Feature.Products.Models.Bazaarvoice;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Newtonsoft.Json;

namespace Delete.Feature.Products.Services
{
    public interface IBazaarvoiceService
    {
        Dictionary<string, ReviewStatistics> GetReviewStatisticsDictionary();
        IEnumerable<Review> GetProductReviews(string productId);
    }

    public class BazaarvoiceService : IBazaarvoiceService
    {
        protected readonly string BaseCacheKey = $"royalcanin|custom|{nameof(BazaarvoiceService)}|{{0}}|{{1}}";

        private readonly ICacheManager cacheManager;
        private readonly IReviewServiceConfiguration reviewServiceConfiguration;


        public BazaarvoiceService(ICacheManager cacheManager, IReviewServiceConfiguration reviewServiceConfiguration)
        {
            this.cacheManager = cacheManager;
            this.reviewServiceConfiguration = reviewServiceConfiguration;
        }

        public Dictionary<string, ReviewStatistics> GetReviewStatisticsDictionary()
        {
            if (reviewServiceConfiguration == null) return new Dictionary<string, ReviewStatistics>();

            var fullCacheKey = string.Format(BaseCacheKey, nameof(GetReviewStatisticsDictionary), Sitecore.Context.Language.Name);

            return cacheManager.CacheResults(() => GetReviewStatisticsDictionaryInternal(reviewServiceConfiguration),
                fullCacheKey,
                new TimeSpan(0, reviewServiceConfiguration.CacheMinutes, 0));
        }

        public IEnumerable<Review> GetProductReviews(string productId)
        {
            if (reviewServiceConfiguration == null || string.IsNullOrWhiteSpace(productId)) return new List<Review>();

            var cacheKeyPart = $"{Sitecore.Context.Language.Name}|productid={productId}";
            var fullCacheKey = string.Format(BaseCacheKey, nameof(GetProductReviews), cacheKeyPart);

            return cacheManager.CacheResults(() => GetProductReviewsInternal(reviewServiceConfiguration, productId),
                fullCacheKey,
                new TimeSpan(0, reviewServiceConfiguration.CacheMinutes, 0));
        }

        protected virtual Dictionary<string, ReviewStatistics> GetReviewStatisticsDictionaryInternal(IReviewServiceConfiguration configuration)
        {
            this.LogMethodEnter();

            var result = new Dictionary<string, ReviewStatistics>();

            try
            {
                long limit = configuration.ItemsLoadLimit;
                long offset = 0;
                var totalResults = long.MaxValue;
                ProductsBazaarvoiceResponse allProducts = null;

                while (offset < totalResults)
                {
                    var requestUrl = string.Format(configuration.ProductReviewStatisticsURL, configuration.APIVersion,
                        configuration.Passkey, limit, offset);

                    var request = WebRequest.Create(requestUrl);
                    var responseStream = request.GetResponse().GetResponseStream();
                    if (responseStream == default(Stream)) break;

                    string responseText;
                    using (var sr = new StreamReader(responseStream))
                    {
                        responseText = sr.ReadToEnd();
                    }

                    var pageOfProducts = JsonConvert.DeserializeObject<ProductsBazaarvoiceResponse>(responseText);

                    if (allProducts == default(ProductsBazaarvoiceResponse))
                    {
                        allProducts = pageOfProducts;
                        allProducts.Offset += pageOfProducts.Limit;
                    }
                    else
                    {
                        allProducts.Merge(pageOfProducts);
                    }

                    offset = allProducts.Offset;
                    totalResults = allProducts.TotalResults;

                    if (allProducts.Errors != null && allProducts.Errors.Any())
                    {
                        foreach (var error in allProducts.Errors)
                        {
                            this.LogError($"Bazaarvoice error [{error.Code}]: {error.Message}");
                        }
                        break;
                    }
                }

                if (allProducts?.Results != null && allProducts.Results.Any())
                {
                    result = allProducts.Results.ToDictionary(product => product.Id,
                        product => product.ReviewStatistics);
                }
            }
            catch (Exception exception)
            {
                this.LogError(exception, true);
            }

            this.LogMethodOut();
            return result;
        }

        protected internal IEnumerable<Review> GetProductReviewsInternal(IReviewServiceConfiguration configuration, string productId)
        {
            this.LogMethodEnter();

            var result = new List<Review>();

            if (string.IsNullOrWhiteSpace(productId)) return result;

            try
            {
                long limit = configuration.ItemsLoadLimit;
                long offset = 0;
                var totalResults = long.MaxValue;
                ReviewsBazaarvoiceResponse allReviews = null;

                while (offset < totalResults)
                {
                    var requestUrl = string.Format(configuration.ProductReviewsURL, configuration.APIVersion,
                        configuration.Passkey, productId, limit, offset);

                    var request = WebRequest.Create(requestUrl);
                    var responseStream = request.GetResponse().GetResponseStream();
                    if (responseStream == default(Stream)) break;

                    string responseText;
                    using (var sr = new StreamReader(responseStream))
                    {
                        responseText = sr.ReadToEnd();
                    }

                    var pageOfReviews = JsonConvert.DeserializeObject<ReviewsBazaarvoiceResponse>(responseText);

                    if (allReviews == default(ReviewsBazaarvoiceResponse))
                    {
                        allReviews = pageOfReviews;
                        allReviews.Offset += pageOfReviews.Limit;
                    }
                    else
                    {
                        allReviews.Merge(pageOfReviews);
                    }

                    offset = allReviews.Offset;
                    totalResults = allReviews.TotalResults;

                    if (allReviews.Errors != null && allReviews.Errors.Any())
                    {
                        foreach (var error in allReviews.Errors)
                        {
                            this.LogError($"Bazaarvoice error [{error.Code}]: {error.Message}");
                        }
                        break;
                    }
                }

                if (allReviews?.Results != null && allReviews.Results.Any())
                {
                    result = allReviews.Results;
                }
            }
            catch (Exception exception)
            {
                this.LogError(exception, true);
            }

            this.LogMethodOut();
            return result;
        }
    }
}