using AutoMapper;
using Delete.Feature.Products.Models;
using Delete.Foundation.DataTemplates.Models;
using JetBrains.Annotations;

namespace Delete.Feature.Products.Profiles
{
    [UsedImplicitly]
    public class ProductsProfile : Profile
    {
        public ProductsProfile()
        {
            CreateMap<LocalProduct, PredictiveSearchItemJsonModel>()
                .ForMember(x => x.Title, opt => opt.MapFrom(z => z.ExtendedProductName))
                .ForMember(x => x.Description, opt => opt.MapFrom(z => string.Empty))
                .ForMember(x => x.Url, opt => opt.MapFrom(z => z.Url))
                ;

            CreateMap<LocalProduct, ProductFinderViewModel>()
                .ForMember(x => x.ProductName, opt => opt.MapFrom(z => z.ExtendedProductName))
                .ForMember(x => x.Image, opt =>
                {
                    opt.Condition(x => x.ExtendedImage1_BAG != null);
                    opt.MapFrom(x => x.ExtendedImage1_BAG.Src);
                })
                .ForMember(x => x.Url, opt => opt.MapFrom(x => x.Url))
                .ForMember(x => x.MainItemId, opt =>
                {
                    opt.Condition(x => x.GlobalProduct != null);
                    opt.MapFrom(x => x.GlobalProduct.MainItemID);
                })
                .ForMember(x => x.WhereToBuyUrl, opt => opt.Ignore());
        }
    }
}
