﻿using System;
using System.Collections.Generic;
using Delete.Foundation.DataTemplates.Models.Taxonomy;
using Delete.Foundation.ItemImport.ImportProcessors;
using Delete.Foundation.ItemImport.LogManagerImport;
using Glass.Mapper.Sc;
using Sitecore.Globalization;

namespace Delete.Feature.Products.Import.Processors
{
    using Foundation.ItemImport.Models;

    using Glass.Mapper;

    public interface IProductTypeItemProcessor : IBaseImportItemProcessor<ProductType, ProductTypeDto>
    {
    }

    public class ProductTypeItemProcessor : BaseImportItemProcessor<ProductType, ProductTypeDto>, IProductTypeItemProcessor
    {
        public ProductTypeItemProcessor(ISitecoreContext sitecoreContext, ILogManagerImport logManagerImport, string locationPathOverride = null) : base(sitecoreContext, logManagerImport, locationPathOverride)
        {
        }

        protected override Func<ProductType, string> IdStringFromSitecoreItem => productType => productType?.Value;

        protected override Func<ProductTypeDto, string> IdStringFromImportObj => GetIdString;

        protected override Func<ProductTypeDto, string> ItemNameFromImportObj => GetIdString;

        protected override string DefaultLocation =>
            Sitecore.Configuration.Settings.GetSetting("Products.DefaultProductTypesLocation",
                "/sitecore/content/Global Configuration/Taxonomy/Product Types");
        protected override bool OverwriteValuesOnUpdate => false;
        protected override bool MapDatabaseFields => true;

	    public override ProductType ProcessItem(ProductTypeDto importObj, IEnumerable<Language> languageVersions, string pathOverride = null)
        {
            var id = this.CalculateItemId(importObj);
            if (!id.HasValue())
            {
                return null;
            }

            ProductType item;

            using (new VersionCountDisabler())
            {
                item = this.GetItem(id);
            }
            return item;
        }

        private static string GetIdString(ProductTypeDto productType)
        {
            return productType?.Name ?? string.Empty;
        }
    }
}