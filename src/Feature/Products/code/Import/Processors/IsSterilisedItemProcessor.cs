﻿using System;
using System.Collections.Generic;
using Delete.Foundation.DataTemplates.Models.Taxonomy;
using Delete.Foundation.ItemImport.ImportProcessors;
using Delete.Foundation.ItemImport.LogManagerImport;
using Glass.Mapper;
using Glass.Mapper.Sc;
using Sitecore.Globalization;

namespace Delete.Feature.Products.Import.Processors
{
    using Foundation.ItemImport.Models;

    public interface IIsSterilisedItemProcessor : IBaseImportItemProcessor<IsSterilised,
        SpecialNeedDto>
    {
    }

    public class IsSterilisedItemProcessor :
        SpeciesSpecificImportItemProcessorBase<IsSterilised, SpecialNeedDto>,
        IIsSterilisedItemProcessor
    {
        public IsSterilisedItemProcessor(
            ISitecoreContext sitecoreContext,
            Species species,
            ILogManagerImport logManagerImport,
            string locationPathOverride = null)
            : base(sitecoreContext, logManagerImport, species, locationPathOverride)
        {
        }

        protected override Func<IsSterilised, string> IdStringFromSitecoreItem => item => item?.ImportMappingValue;

        protected override Func<SpecialNeedDto, string> IdStringFromImportObj => item => item?.Key;

        protected override Func<SpecialNeedDto, string> ItemNameFromImportObj => item => item?.Name;

        protected override string DefaultLocation =>
            Sitecore.Configuration.Settings.GetSetting("Products.DefaultIsSterilizedLocation",
                "/sitecore/content/Global Configuration/Taxonomy/Is Sterilised");
        protected override bool OverwriteValuesOnUpdate => false;
        public override IsSterilised ProcessItem(SpecialNeedDto importObj, IEnumerable<Language> languageVersions,
             string pathOverride = null)
        {
            var id = this.CalculateItemId(importObj);
            if (!id.HasValue())
            {
                return null;
            }

            IsSterilised item;

            using (new VersionCountDisabler())
            {
                item = this.GetItem(id);
            }
            return item;
        }
    }

    public interface IDogIsSterilisedProcessor : IIsSterilisedItemProcessor
    {
    }

    public class DogIsSterilisedProcessor : IsSterilisedItemProcessor, IDogIsSterilisedProcessor
    {
        public DogIsSterilisedProcessor(ISitecoreContext sitecoreContext, ILogManagerImport logManagerImport, string locationPathOverride = null)
            : base(sitecoreContext, Species.Dogs, logManagerImport, locationPathOverride)
        {
        }
    }

    public interface ICatIsSterilisedProcessor : IIsSterilisedItemProcessor
    {
    }

    public class CatIsSterilisedProcessor : IsSterilisedItemProcessor, ICatIsSterilisedProcessor
    {
        public CatIsSterilisedProcessor(ISitecoreContext sitecoreContext, ILogManagerImport logManagerImport, string locationPathOverride = null)
            : base(sitecoreContext, Species.Cats, logManagerImport, locationPathOverride)
        {
        }
    }
}