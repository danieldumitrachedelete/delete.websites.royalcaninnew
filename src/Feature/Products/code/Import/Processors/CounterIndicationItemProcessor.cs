﻿using System;
using System.Collections.Generic;
using Delete.Foundation.DataTemplates.Models.Taxonomy;
using Delete.Foundation.ItemImport.ImportProcessors;
using Delete.Foundation.ItemImport.LogManagerImport;
using Glass.Mapper;
using Glass.Mapper.Sc;
using Sitecore.Globalization;

namespace Delete.Feature.Products.Import.Processors
{
    using Foundation.ItemImport.Models;

    public interface ICounterIndicationItemProcessor : IBaseImportItemProcessor<Counterindication,
        CounterIndicationDto>
    {
    }

    public class CounterIndicationItemProcessor :
        SpeciesSpecificImportItemProcessorBase<Counterindication, CounterIndicationDto>,
        ICounterIndicationItemProcessor
    {
        public CounterIndicationItemProcessor(
            ISitecoreContext sitecoreContext,
            Species species, 
            ILogManagerImport logManagerImport,
            string locationPathOverride = null)
            : base(sitecoreContext, logManagerImport, species,  locationPathOverride)
        {
        }

        protected override Func<Counterindication, string> IdStringFromSitecoreItem => item => item.Value;

        protected override Func<CounterIndicationDto, string> IdStringFromImportObj => item => item.Id;

        protected override Func<CounterIndicationDto, string> ItemNameFromImportObj => item => item.Name;

        protected override string DefaultLocation =>
            Sitecore.Configuration.Settings.GetSetting("Products.DefaultCounterIndicationsLocation",
                "/sitecore/content/Global Configuration/Taxonomy/Counterindications");
        protected override bool OverwriteValuesOnUpdate => false;
        protected override Counterindication MapDefaultVersionFields(Counterindication item, CounterIndicationDto importObj)
        {
            item = base.MapDefaultVersionFields(item, importObj);
            item.Value = this.IdStringFromImportObj(importObj);

            return item;
        }

        protected override Counterindication MapLanguageVersionFields(Counterindication item,
            CounterIndicationDto importObj,  IEnumerable<Language> languages)
        {
            item = base.MapLanguageVersionFields(item, importObj, languages);

            item.Text = item.Name;
            item.FilterLabel = item.Name;

            return item;
        }

        public override Counterindication ProcessItem(CounterIndicationDto importObj,
            IEnumerable<Language> languageVersions,  string pathOverride = null)
        {
            var createCounterIndications =
                Sitecore.Configuration.Settings.GetBoolSetting("Products.CreateNewCounterIndicationsDuringProductImport",
                    false);

            if (createCounterIndications)
            {
                return base.ProcessItem(importObj, languageVersions,  pathOverride);
            }

            var id = this.CalculateItemId(importObj);
            if (!id.HasValue())
            {
                return null;
            }

            Counterindication item;

            using (new VersionCountDisabler())
            {
                item = this.GetItem(id);
            }
            return item;
        }
    }

    public interface IDogCounterIndicationProcessor : ICounterIndicationItemProcessor
    {
    }

    public class DogCounterIndicationProcessor : CounterIndicationItemProcessor, IDogCounterIndicationProcessor
    {
        public DogCounterIndicationProcessor(ISitecoreContext sitecoreContext, ILogManagerImport logManagerImport, string locationPathOverride = null)
            : base(sitecoreContext, Species.Dogs, logManagerImport, locationPathOverride)
        {
        }
    }

    public interface ICatCounterIndicationProcessor : ICounterIndicationItemProcessor
    {
    }

    public class CatCounterIndicationProcessor : CounterIndicationItemProcessor, ICatCounterIndicationProcessor
    {
        public CatCounterIndicationProcessor(ISitecoreContext sitecoreContext, ILogManagerImport logManagerImport, string locationPathOverride = null)
            : base(sitecoreContext, Species.Cats, logManagerImport, locationPathOverride)
        {
        }
    }
}