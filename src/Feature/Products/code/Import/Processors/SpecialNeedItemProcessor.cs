﻿using System;
using System.Collections.Generic;
using Delete.Foundation.DataTemplates.Models.Taxonomy;
using Delete.Foundation.ItemImport.ImportProcessors;
using Delete.Foundation.ItemImport.LogManagerImport;
using Glass.Mapper;
using Glass.Mapper.Sc;
using Sitecore.Globalization;

namespace Delete.Feature.Products.Import.Processors
{
    using Foundation.ItemImport.Models;

    public interface ISpecialNeedItemProcessor : IBaseImportItemProcessor<SpecialNeed,
        SpecialNeedDto>
    {
    }

    public class SpecialNeedItemProcessor :
        SpeciesSpecificImportItemProcessorBase<SpecialNeed, SpecialNeedDto>,
        ISpecialNeedItemProcessor
    {
        
        public SpecialNeedItemProcessor(
            ISitecoreContext sitecoreContext,
            Species species,
            ILogManagerImport logManagerImport,
            string locationPathOverride = null)
            : base(sitecoreContext, logManagerImport, species,  locationPathOverride)
        {
        }

        protected override Func<SpecialNeed, string> IdStringFromSitecoreItem => item => item?.Value;

        protected override Func<SpecialNeedDto, string> IdStringFromImportObj => item => item?.Key;

        protected override Func<SpecialNeedDto, string> ItemNameFromImportObj => item => item?.Name;

        protected override string DefaultLocation =>
            Sitecore.Configuration.Settings.GetSetting("Products.DefaultSpecialNeedsLocation",
                "/sitecore/content/Global Configuration/Taxonomy/Special Needs");

        protected override bool OverwriteValuesOnUpdate => false;

        protected override SpecialNeed MapDefaultVersionFields(SpecialNeed item, SpecialNeedDto importObj)
        {
            item = base.MapDefaultVersionFields(item, importObj);
            item.Value = this.IdStringFromImportObj(importObj);

            return item;
        }

        protected override SpecialNeed MapLanguageVersionFields(SpecialNeed item, SpecialNeedDto importObj,
             IEnumerable<Language> languages)
        {
            item = base.MapLanguageVersionFields(item, importObj, languages);

            item.Text = item.Name;
            item.FilterLabel = item.Name;

            return item;
        }

        public override SpecialNeed ProcessItem(SpecialNeedDto importObj, IEnumerable<Language> languageVersions,
             string pathOverride = null)
        {
            var createSpecialNeeds =
                Sitecore.Configuration.Settings.GetBoolSetting("Products.CreateNewSpecialNeedsDuringProductImport",
                    false);

            if (createSpecialNeeds)
            {
                return base.ProcessItem(importObj, languageVersions, pathOverride);
            }

            var id = this.CalculateItemId(importObj);
            if (!id.HasValue())
            {
                return null;
            }

            SpecialNeed item;

            using (new VersionCountDisabler())
            {
                item = base.GetItem(id);
            }
            return item;
        }
        
    }
    
    public interface IDogSpecialNeedProcessor : ISpecialNeedItemProcessor
    {
    }

    public class DogSpecialNeedProcessor : SpecialNeedItemProcessor, IDogSpecialNeedProcessor
    {
        public DogSpecialNeedProcessor(ISitecoreContext sitecoreContext, ILogManagerImport logManagerImport, string locationPathOverride = null)
            : base(sitecoreContext, Species.Dogs, logManagerImport, locationPathOverride)
        {
        }
    }

    public interface ICatSpecialNeedProcessor : ISpecialNeedItemProcessor
    {
    }

    public class CatSpecialNeedProcessor : SpecialNeedItemProcessor, ICatSpecialNeedProcessor
    {
        public CatSpecialNeedProcessor(ISitecoreContext sitecoreContext, ILogManagerImport logManagerImport, string locationPathOverride = null)
            : base(sitecoreContext, Species.Cats, logManagerImport, locationPathOverride)
        {
        }
    }
}