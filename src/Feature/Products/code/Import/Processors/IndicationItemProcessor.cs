﻿using System;
using System.Collections.Generic;
using Delete.Foundation.DataTemplates.Models.Taxonomy;
using Delete.Foundation.ItemImport.ImportProcessors;
using Delete.Foundation.ItemImport.LogManagerImport;
using Glass.Mapper;
using Glass.Mapper.Sc;
using Sitecore.Globalization;

namespace Delete.Feature.Products.Import.Processors
{
    using Foundation.ItemImport.Models;

    public interface IIndicationItemProcessor : IBaseImportItemProcessor<Indication,
        IndicationDto>
    {
    }

    public class IndicationItemProcessor :
        SpeciesSpecificImportItemProcessorBase<Indication, IndicationDto>,
        IIndicationItemProcessor
    {
        public IndicationItemProcessor(
            ISitecoreContext sitecoreContext,
            Species species,
            ILogManagerImport logManagerImport,
            string locationPathOverride = null)
            : base(sitecoreContext, logManagerImport, species, locationPathOverride)
        {
        }

        protected override Func<Indication, string> IdStringFromSitecoreItem => item => item.Value;

        protected override Func<IndicationDto, string> IdStringFromImportObj => item => item.Id;

        protected override Func<IndicationDto, string> ItemNameFromImportObj => item => item.Name;

        protected override string DefaultLocation =>
            Sitecore.Configuration.Settings.GetSetting("Products.DefaultIndicationsLocation",
                "/sitecore/content/Global Configuration/Taxonomy/Indications");
        protected override bool OverwriteValuesOnUpdate => false;
        protected override Indication MapDefaultVersionFields(Indication item, IndicationDto importObj)
        {
            item = base.MapDefaultVersionFields(item, importObj);
            item.Value = this.IdStringFromImportObj(importObj);

            return item;
        }

        protected override Indication MapLanguageVersionFields(Indication item, IndicationDto importObj,
             IEnumerable<Language> languages)
        {
            item = base.MapLanguageVersionFields(item, importObj,  languages);

            item.Text = item.Name;
            item.FilterLabel = item.Name;

            return item;
        }

        public override Indication ProcessItem(IndicationDto importObj, IEnumerable<Language> languageVersions,
             string pathOverride = null)
        {
            var createIndications =
                Sitecore.Configuration.Settings.GetBoolSetting("Products.CreateNewIndicationsDuringProductImport",
                    false);

            if (createIndications)
            {
                return base.ProcessItem(importObj, languageVersions, pathOverride);
                
            }

            var id = this.CalculateItemId(importObj);
            if (!id.HasValue())
            {
                return null;
            }

            Indication item;

            using (new VersionCountDisabler())
            {
                item = this.GetItem(id);
            }
            return item;
        }
    }

    public interface IDogIndicationProcessor : IIndicationItemProcessor
    {
    }

    public class DogIndicationProcessor : IndicationItemProcessor, IDogIndicationProcessor
    {
        public DogIndicationProcessor(ISitecoreContext sitecoreContext, ILogManagerImport logManagerImport, string locationPathOverride = null)
            : base(sitecoreContext, Species.Dogs, logManagerImport, locationPathOverride)
        {
        }
    }

    public interface ICatIndicationProcessor : IIndicationItemProcessor
    {
    }

    public class CatIndicationProcessor : IndicationItemProcessor, ICatIndicationProcessor
    {
        public CatIndicationProcessor(ISitecoreContext sitecoreContext, ILogManagerImport logManagerImport, string locationPathOverride = null)
            : base(sitecoreContext, Species.Cats, logManagerImport, locationPathOverride)
        {
        }
    }
}