﻿using Delete.Foundation.ItemImport.LogManagerImport;

namespace Delete.Feature.Products.Import.Processors
{
    using Foundation.DataTemplates.Models.Taxonomy;

    using Glass.Mapper.Sc;

    public interface IDogLifestageItemProcessor : ILifestageItemProcessor
    {
    }

    public class DogLifestageItemProcessor : LifestageItemProcessor, IDogLifestageItemProcessor
    {
        public DogLifestageItemProcessor(
            ISitecoreContext sitecoreContext,
            ILogManagerImport logManagerImport,
            string locationPathOverride = null)
            : base(sitecoreContext, Species.Dogs, logManagerImport, locationPathOverride)
        {
        }
    }
}