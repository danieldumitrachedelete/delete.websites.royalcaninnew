﻿using System;
using System.Collections.Generic;
using Delete.Foundation.DataTemplates.Models.Taxonomy;
using Delete.Foundation.ItemImport.ImportProcessors;
using Delete.Foundation.ItemImport.LogManagerImport;
using Glass.Mapper.Sc;
using Sitecore.Globalization;

namespace Delete.Feature.Products.Import.Processors
{
    using Foundation.ItemImport.Models;

    using Glass.Mapper;

    public interface ILifestageItemProcessor : IBaseImportItemProcessor<Lifestage, LifestageDto>
    {
    }

    public class LifestageItemProcessor : SpeciesSpecificImportItemProcessorBase<Lifestage, LifestageDto>, ILifestageItemProcessor
    {
        public LifestageItemProcessor(
            ISitecoreContext sitecoreContext,
            Species species,
            ILogManagerImport logManagerImport,
            string locationPathOverride = null)
            : base(sitecoreContext, logManagerImport, species, locationPathOverride)
        {
        }

        private static Dictionary<string, string> MappingDictionary => new Dictionary<string, string>
        {
            {"kitten", "kitten"},
            {"puppy", "puppy"},
            {"adult", "adult"},
            {"mature", "mature"},
            {"senior", "mature"}
        };

        protected override Func<Lifestage, string> IdStringFromSitecoreItem => size => size?.Value;

        protected override Func<LifestageDto, string> IdStringFromImportObj => GetIdString;

        protected override Func<LifestageDto, string> ItemNameFromImportObj => GetIdString;

        protected override string DefaultLocation => Sitecore.Configuration.Settings.GetSetting(
            "Products.DefaultLifestagesLocation", "/sitecore/content/Global Configuration/Taxonomy/Lifestages");
        protected override bool OverwriteValuesOnUpdate => false;
        public override Lifestage ProcessItem(LifestageDto importObj, IEnumerable<Language> languageVersions,
             string pathOverride = null)
        {
            var id = this.CalculateItemId(importObj);
            if (!id.HasValue())
            {
                return null;
            }

            Lifestage item;

            using (new VersionCountDisabler())
            {
                item = this.GetItem(id);
            }
            return item;
        }

        private static string GetIdString(LifestageDto age)
        {
            if (age == null) return String.Empty;

            var nameKey = age.Name;
            return MappingDictionary.ContainsKey(nameKey) ? MappingDictionary[nameKey] : nameKey;
        }
    }
}