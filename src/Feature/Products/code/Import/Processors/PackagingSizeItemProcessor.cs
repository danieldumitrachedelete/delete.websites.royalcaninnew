﻿using System;
using System.Collections.Generic;
using Delete.Feature.Products.Models;
using Delete.Foundation.ItemImport.ImportProcessors;
using Delete.Foundation.ItemImport.LogManagerImport;
using Glass.Mapper.Sc;
using Sitecore.Globalization;

namespace Delete.Feature.Products.Import.Processors
{
    using System.Globalization;

    using Foundation.ItemImport.Models;

    public interface IPackagingSizeItemProcessor : IBaseImportItemProcessor<PackagingSize,
        PackagingDto>
    {
        void DeleteItem(PackagingSize localPackgeSize);
    }

    public class PackagingSizeItemProcessor :
        BaseImportItemProcessor<PackagingSize, PackagingDto>,
        IPackagingSizeItemProcessor
    {
        protected readonly IMeasurementUnitItemProcessor MeasurementUnitItemProcessor;

        public PackagingSizeItemProcessor(
            ISitecoreContext sitecoreContext,
            IMeasurementUnitItemProcessor measurementUnitItemProcessor,
            ILogManagerImport logManagerImport,
            string locationPathOverride = null) : base(sitecoreContext, logManagerImport, locationPathOverride)
        {
            this.MeasurementUnitItemProcessor = measurementUnitItemProcessor;
        }

        protected override Func<PackagingSize, string> IdStringFromSitecoreItem => x => this.BuildKey(x.Size, x.Unit?.Value);

        protected override Func<PackagingDto, string> IdStringFromImportObj => x => this.BuildKey(x.Size, x.Unit?.Code);

        protected override Func<PackagingDto, string> ItemNameFromImportObj => x => this.BuildName(x.Size, x.Unit?.Code);

        // not used, paths are handled by overriding CalculateItemLocation
        protected override string DefaultLocation => string.Empty;

        protected override bool CacheItems => false;

        protected override bool MapDatabaseFields => true;

        private string BuildKey(decimal amount, string unit)
        {
            return $"{amount.ToString("0.##", CultureInfo.InvariantCulture)}_{unit}".Replace('.', '_');
        }

        private string BuildName(decimal amount, string unit)
        {
            return $"{amount.ToString("0.##", CultureInfo.InvariantCulture)} {unit}".Replace('.', '_');
        }

        protected override PackagingSize MapDefaultVersionFields(PackagingSize item, PackagingDto importObj)
        {
            item = base.MapDefaultVersionFields(item, importObj);
            var entry = new List<ImportLogEntry>();
            item.Size = importObj.Size;
            item.Unit = this.MeasurementUnitItemProcessor.ProcessItem(importObj.Unit, new List<Language>());

            return item;
        }

        public void DeleteItem(PackagingSize deltedItem)
        {
            this.GenericItemRepository.Delete(deltedItem);
        }

        protected override PackagingSize MapLanguageVersionFields(PackagingSize item, PackagingDto importObj,
            IEnumerable<Language> languages)
        {
            item = base.MapLanguageVersionFields(item, importObj, languages);
            item.Value = this.IdStringFromImportObj(importObj);
            item.Text = this.ItemNameFromImportObj(importObj);
            item.SKU = importObj.SKU;

            return item;
        }
    }
}