﻿using Delete.Foundation.ItemImport.LogManagerImport;

namespace Delete.Feature.Products.Import.Processors
{
    using System;

    using Foundation.DataTemplates.Models.Interfaces;
    using Foundation.DataTemplates.Models.Taxonomy;
    using Foundation.ItemImport.Interfaces;

    using Glass.Mapper.Sc;

    public interface IGlobalCatProductItemProcessor : IGlobalProductItemProcessor
    {
    }

    public class GlobalCatProductItemProcessor : GlobalProductItemProcessor, IGlobalCatProductItemProcessor
    {
        public GlobalCatProductItemProcessor(
            ISitecoreContext sitecoreContext,
            IDogSizeItemProcessor dogSizeItemProcessor,
            IPillarIdItemProcessor pillarIdItemProcessor,
            ILogManagerImport logManagerImport,
            Func<Species, ILifestageItemProcessor> lifestageItemServiceAccessor,
            Func<Species, IGlobalBreedItemProcessor<I_GlobalBreed>> globalBreedItemServiceAccessor/*,
            string locationPathOverride = null*/) : base(
            sitecoreContext,
            Species.Cats,
            dogSizeItemProcessor,
            pillarIdItemProcessor,
            lifestageItemServiceAccessor,
            globalBreedItemServiceAccessor,
            logManagerImport/*,
            locationPathOverride*/)
        {
        }
    }
}