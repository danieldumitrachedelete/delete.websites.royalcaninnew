﻿using System;
using System.Collections.Generic;
using Delete.Foundation.DataTemplates.Models.Taxonomy;
using Delete.Foundation.ItemImport.ImportProcessors;
using Delete.Foundation.ItemImport.LogManagerImport;
using Glass.Mapper.Sc;
using Sitecore.Globalization;

namespace Delete.Feature.Products.Import.Processors
{
    using Foundation.ItemImport.Models;

    using Glass.Mapper;

    public interface IDogSizeItemProcessor : IBaseImportItemProcessor<Size, AnimalSizeDto>
    {
    }

    public class DogSizeItemProcessor : BaseImportItemProcessor<Size, AnimalSizeDto>, IDogSizeItemProcessor
    {
        public DogSizeItemProcessor(ISitecoreContext sitecoreContext, ILogManagerImport logManagerImport, string locationPathOverride = null) : base(sitecoreContext, logManagerImport, locationPathOverride)
        {
        }

        private static Dictionary<string, string> MappingDictionary =>
            new Dictionary<string, string>
            {
                {"xsmall", "x-small"},
                {"small (0-10 kg)", "small"},
                {"mini_1_10_kg", "small"},
                {"medium_11_25_kg", "medium"},
                {"large (over 11 kg)", "large"},
                {"maxi_26_44_kg", "large"},
                {"giant_from_45_kg", "giant"}
            };

        protected override Func<Size, string> IdStringFromSitecoreItem => size => size.Value;

        protected override Func<AnimalSizeDto, string> IdStringFromImportObj => GetIdString;

        protected override Func<AnimalSizeDto, string> ItemNameFromImportObj => GetIdString;

        protected override string DefaultLocation => Sitecore.Configuration.Settings.GetSetting(
            "Products.DefaultAnimalSizesLocation", "/sitecore/content/Global Configuration/Taxonomy/Sizes/Dog");

        public override Size ProcessItem(AnimalSizeDto importObj, IEnumerable<Language> languageVersions,
             string pathOverride = null)
        {
            var id = this.CalculateItemId(importObj);
            if (!id.HasValue())
            {
                return null;
            }

            Size item;

            using (new VersionCountDisabler())
            {
                item = this.GetItem(id);
            }
            return item;
        }

        private static string GetIdString(AnimalSizeDto size)
        {
            var nameKey = size.Code;
            if (MappingDictionary.ContainsKey(nameKey))
            {
                return MappingDictionary[nameKey];
            }

            return nameKey;
        }
    }
}