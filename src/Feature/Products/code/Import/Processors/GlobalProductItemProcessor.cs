﻿using System;
using System.Collections.Generic;
using System.Linq;
using Delete.Feature.Products.Models;
using Delete.Foundation.DataTemplates.Models.Interfaces;
using Delete.Foundation.DataTemplates.Models.Taxonomy;
using Delete.Foundation.ItemImport.ImportProcessors;
using Delete.Foundation.ItemImport.Interfaces;
using Delete.Foundation.ItemImport.LogManagerImport;
using Glass.Mapper;
using Glass.Mapper.Sc;
using Sitecore.Globalization;

namespace Delete.Feature.Products.Import.Processors
{
    using Foundation.ItemImport.Models;

    public interface
        IGlobalProductItemProcessor : ISpeciesSpecificImportItemProcessorBase<GlobalProduct,
            ImportedProductDto>
    {
    }

    public class GlobalProductItemProcessor :
        SpeciesSpecificImportItemProcessorBase<GlobalProduct, ImportedProductDto>,
        IGlobalProductItemProcessor
    {
        protected readonly IDogSizeItemProcessor DogSizeItemProcessor;
        protected readonly IPillarIdItemProcessor PillarIdItemProcessor;
        protected readonly Func<Species, ILifestageItemProcessor> LifestageItemServiceAccessor;
        protected readonly Func<Species, IGlobalBreedItemProcessor<I_GlobalBreed>> GlobalBreedItemServiceAccessor;

        public GlobalProductItemProcessor(
            ISitecoreContext sitecoreContext,
            Species species,
            IDogSizeItemProcessor dogSizeItemProcessor,
            IPillarIdItemProcessor pillarIdItemProcessor,
            Func<Species, ILifestageItemProcessor> lifestageItemServiceAccessor,
            Func<Species, IGlobalBreedItemProcessor<I_GlobalBreed>> globalBreedItemServiceAccessor,
            ILogManagerImport logManagerImport,
            string locationPathOverride = null) : base(sitecoreContext, logManagerImport, species, locationPathOverride)
        {
            this.DogSizeItemProcessor = dogSizeItemProcessor;
            this.PillarIdItemProcessor = pillarIdItemProcessor;
            this.LifestageItemServiceAccessor = lifestageItemServiceAccessor;
            this.GlobalBreedItemServiceAccessor = globalBreedItemServiceAccessor;
        }

        protected override Func<GlobalProduct, string> IdStringFromSitecoreItem => x => x.MainItemID;

        protected override Func<ImportedProductDto, string> IdStringFromImportObj => x => x.MainItemId;

        protected override Func<ImportedProductDto, string> ItemNameFromImportObj => x => x.ProductName;

        protected override string DefaultLocation => 
            Sitecore.Configuration.Settings.GetSetting("Products.DefaultGlobalProductsLocation",
                "/sitecore/content/Global Configuration/Products");

        protected override bool MapDatabaseFields => true;

        private bool CreateNew => Sitecore.Configuration.Settings.GetBoolSetting("Products.CreateNewGlobalProductsDuringProductImport", false);

        protected override GlobalProduct MapDefaultVersionFields(GlobalProduct item, ImportedProductDto importObj)
        {
            item = base.MapDefaultVersionFields(item, importObj);

            item.Specie = this.Context.GetItem<Specie>(SpecieConstants.SpecieGuid(importObj.Species));
            var entry = new List<ImportLogEntry>();
            var breed = this.GlobalBreedItemServiceAccessor(importObj.Species)
                .ProcessItem(importObj.Breed, new List<Language>());
            if (breed != null)
            {
                item.Breed = breed.Id;
            }
            
            if (importObj.IsDog)
            {
                item.AnimalSizes =
                    importObj.AnimalSizes?.Select(x => this.DogSizeItemProcessor.ProcessItem(x, new List<Language>()));
            }

            item.Lifestages = importObj.Ages.Select(age =>
                this.LifestageItemServiceAccessor(importObj.Species).ProcessItem(age, new List<Language>())).ToList();

            var pillar = this.PillarIdItemProcessor.ProcessItem(importObj.Pillar, new List<Language>());
            if (pillar != null)
            {
                item.PillarIDCode = pillar.Id;
            }

            item.MainItemID = importObj.MainItemId;
            return item;
        }

        public override GlobalProduct ProcessItem(ImportedProductDto importObj, IEnumerable<Language> languageVersions,
            string pathOverride = null)
        {
            if (CreateNew)
            {
                return base.ProcessItem(importObj, languageVersions);
            }

            var id = this.CalculateItemId(importObj);
            if (!id.HasValue())
            {
                return null;
            }

            GlobalProduct item;

            using (new VersionCountDisabler())
            {
                item = this.GetItem(id);
            }
            return item;
        }
    }
}