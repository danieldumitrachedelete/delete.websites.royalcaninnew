﻿using Delete.Foundation.ItemImport.LogManagerImport;

namespace Delete.Feature.Products.Import.Processors
{
    using Foundation.DataTemplates.Models.Taxonomy;

    using Glass.Mapper.Sc;

    public interface ICatLifestageItemProcessor : ILifestageItemProcessor
    {
    }

    public class CatLifestageItemProcessor : LifestageItemProcessor, ICatLifestageItemProcessor
    {
        public CatLifestageItemProcessor(
            ISitecoreContext sitecoreContext,
            ILogManagerImport logManagerImport,
            string locationPathOverride = null)
            : base(sitecoreContext,  Species.Cats, logManagerImport, locationPathOverride)
        {
        }
    }
}