﻿using System;
using System.Collections.Generic;
using Delete.Foundation.ItemImport.ImportProcessors;
using Delete.Foundation.ItemImport.LogManagerImport;
using Delete.Foundation.MultiSite.Models;
using Glass.Mapper.Sc;
using Sitecore.Globalization;

namespace Delete.Feature.Products.Import.Processors
{
    using Foundation.ItemImport.Models;

    using Glass.Mapper;

    public interface IMeasurementUnitItemProcessor : IBaseImportItemProcessor<MeasurementUnit, MeasurementUnitDto>
    {
    }

    public class MeasurementUnitItemProcessor : BaseImportItemProcessor<MeasurementUnit, MeasurementUnitDto>, IMeasurementUnitItemProcessor
    {
        public MeasurementUnitItemProcessor(ISitecoreContext sitecoreContext, ILogManagerImport logManagerImport, string locationPathOverride = null) : base(sitecoreContext, logManagerImport, locationPathOverride)
        {
        }

        protected override Func<MeasurementUnit, string> IdStringFromSitecoreItem => x => x.Value;

        protected override Func<MeasurementUnitDto, string> IdStringFromImportObj => GetIdString;

        protected override Func<MeasurementUnitDto, string> ItemNameFromImportObj => GetIdString;

        protected override string DefaultLocation =>
            Sitecore.Configuration.Settings.GetSetting("Products.DefaultMeasurementUnitsLocation",
                "/sitecore/content/Global Configuration/Measurement Units/Weight-Products");
        protected override bool OverwriteValuesOnUpdate => false;
        public override MeasurementUnit ProcessItem(MeasurementUnitDto importObj,
            IEnumerable<Language> languageVersions,  string pathOverride = null)
        {
            var id = this.CalculateItemId(importObj);
            if (!id.HasValue())
            {
                return null;
            }

            MeasurementUnit item;

            using (new VersionCountDisabler())
            {
                item = this.GetItem(id);
            }
            return item;
        }

        private static string GetIdString(MeasurementUnitDto measurementUnit)
        {
            return measurementUnit?.Code ?? string.Empty;
        }
    }
}