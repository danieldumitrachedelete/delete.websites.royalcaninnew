﻿using System;
using System.Collections.Generic;
using Delete.Foundation.DataTemplates.Models.Taxonomy;
using Delete.Foundation.ItemImport.ImportProcessors;
using Delete.Foundation.ItemImport.LogManagerImport;
using Glass.Mapper.Sc;
using Sitecore.Globalization;

namespace Delete.Feature.Products.Import.Processors
{
    using Foundation.ItemImport.Models;

    using Glass.Mapper;

    public interface IPillarIdItemProcessor : IBaseImportItemProcessor<Pillar, PillarDto>
    {
    }

    public class PillarIdItemProcessor : BaseImportItemProcessor<Pillar, PillarDto>, IPillarIdItemProcessor
    {
        public PillarIdItemProcessor(ISitecoreContext sitecoreContext, ILogManagerImport logManagerImport, string locationPathOverride = null) : base(sitecoreContext, logManagerImport, locationPathOverride)
        {
        }

        protected override Func<Pillar, string> IdStringFromSitecoreItem => pillar => pillar?.Value;

        protected override Func<PillarDto, string> IdStringFromImportObj => GetIdString;

        protected override Func<PillarDto, string> ItemNameFromImportObj => GetIdString;

        protected override string DefaultLocation =>
            Sitecore.Configuration.Settings.GetSetting("Products.DefaultPillarsLocation",
                "/sitecore/content/Global Configuration/Taxonomy/Pillars");
        protected override bool OverwriteValuesOnUpdate => false;
        public override Pillar ProcessItem(PillarDto importObj, IEnumerable<Language> languageVersions,
             string pathOverride = null)
        {
            var id = this.CalculateItemId(importObj);
            if (!id.HasValue())
            {
                return null;
            }

            Pillar item;

            using (new VersionCountDisabler())
            {
                item = this.GetItem(id);
            }
            return item;
        }

        private string GetIdString(PillarDto pillar)
        {
            return pillar?.Code ?? string.Empty;
        }
    }
}