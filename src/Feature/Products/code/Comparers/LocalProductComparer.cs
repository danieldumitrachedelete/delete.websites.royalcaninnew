﻿using System.Collections.Generic;
using Delete.Feature.Products.Models;

namespace Delete.Feature.Products.Comparers
{
    public class LocalProductComparer : IEqualityComparer<LocalProduct>
    {
        public bool Equals(LocalProduct x, LocalProduct y)
        {
            if (x == null || y == null) return false;

            return x.Id == y.Id;
        }

        public int GetHashCode(LocalProduct obj)
        {
            return obj.Id.GetHashCode();
        }
    }
}