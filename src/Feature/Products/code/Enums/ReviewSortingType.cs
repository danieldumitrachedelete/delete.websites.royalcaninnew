﻿namespace Delete.Feature.Products.Enums
{
    public enum ReviewSortingType
    {
        DateDesc = 0,
        Date = 1,
        RatingDesc = 2,
        Rating = 3
    }
}