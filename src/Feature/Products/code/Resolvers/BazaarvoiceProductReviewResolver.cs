﻿using System;
using System.Collections.Generic;
using System.Linq;
using Delete.Feature.Products.Interfaces;
using Delete.Feature.Products.Models.Bazaarvoice;
using Delete.Feature.Products.Services;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.SchemaOrg;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.DependencyInjection;
using Review = Delete.Foundation.DeleteFoundationCore.SchemaOrg.Review;

namespace Delete.Feature.Products.Resolvers
{
    public class BazaarvoiceProductReviewResolver : IProductReviewResolver
    {
        public AggregateRating GetAggregateRating(string productId)
        {
            if (productId.Empty()) return null;

            var reviewService = ServiceLocator.ServiceProvider.GetService<IBazaarvoiceService>();
            ReviewStatistics statistics;
            IList<Models.Bazaarvoice.Review> reviews;

            var allProductStatistics = reviewService.GetReviewStatisticsDictionary();
            if (allProductStatistics.ContainsKey(productId))
            {
                statistics = allProductStatistics[productId];
                reviews = statistics.TotalReviewCount > 0 
                    ? reviewService.GetProductReviews(productId).ToList()
                    : new List<Models.Bazaarvoice.Review>();
            }
            else
            {
                statistics = new ReviewStatistics();
                reviews = new List<Models.Bazaarvoice.Review>();
            }
            
            return reviews.Any() && statistics.TotalReviewCount > 0
                ? new AggregateRating
                {
                    RatingValue = Convert.ToDouble(statistics.AverageOverallRating),
                    BestRating = Convert.ToDouble(reviews.Any() ? reviews.Max(x => x.Rating) : 0),
                    WorstRating = Convert.ToDouble(reviews.Any() ? reviews.Min(x => x.Rating) : 0),
                    RatingCount = Convert.ToInt32(statistics.TotalReviewCount),
                    ReviewCount = Convert.ToInt32(statistics.TotalReviewCount)
                }
                : null;
        }

        public List<Review> GetReviews(string productId)
        {
            var reviewService = ServiceLocator.ServiceProvider.GetService<IBazaarvoiceService>();

            return reviewService.GetProductReviews(productId).Select(x => new Review
            {
                Name = x.Title ?? String.Empty,
                ReviewBody = x.ReviewText ?? String.Empty,
                ReviewRating = new Rating
                {
                    RatingValue = Convert.ToDouble(x.Rating)
                },
                DatePublished = x.LastModificationTime,
                Author = new Person
                {
                    Name = x.UserNickname ?? String.Empty
                }
            }).ToList();
        }
    }
}