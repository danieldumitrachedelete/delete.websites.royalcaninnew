﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Delete.Feature.Products.Enums;
using Delete.Feature.Products.Models;
using Delete.Feature.Products.Models.Bazaarvoice;
using Delete.Feature.Products.Services;
using Delete.Foundation.DeleteFoundationCore.Controllers;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.MultiSite.Models;
using Glass.Mapper.Sc;

namespace Delete.Feature.Products.Controllers
{
    public class ReviewController : BaseController
    {
        private readonly IBazaarvoiceService bazaarvoiceService;
        private readonly IReviewServiceConfiguration reviewServiceConfiguration;
        private readonly ILocalMarketSettings localMarketSettings;

        private readonly List<ProductReviewsOrderByRule> orderByRules = new List<ProductReviewsOrderByRule>
        {
            new ProductReviewsOrderByRule{Key = ReviewSortingType.DateDesc, OrderByExpression = reviews => reviews.OrderByDescending(x => x.SubmissionTime)},
            new ProductReviewsOrderByRule{Key = ReviewSortingType.Rating, OrderByExpression = reviews => reviews.OrderBy(x => x.Rating)},
            new ProductReviewsOrderByRule{Key = ReviewSortingType.RatingDesc, OrderByExpression = reviews => reviews.OrderByDescending(x => x.Rating)}
        };

        public ReviewController(ISitecoreContext sitecoreContext,
            IMapper mapper,
            ICacheManager cacheManager,
            IBazaarvoiceService bazaarvoiceService,
            IReviewServiceConfiguration reviewServiceConfiguration,
            ILocalMarketSettings localMarketSettings) : base(sitecoreContext, mapper, cacheManager)
        {
            this.bazaarvoiceService = bazaarvoiceService;
            this.reviewServiceConfiguration = reviewServiceConfiguration;
            this.localMarketSettings = localMarketSettings;
        }

        public ActionResult ProductRating(string mainItemId, bool isClickable = true)
        {
            if (localMarketSettings == null || reviewServiceConfiguration == null)
            {
                return new EmptyResult();
            }

            var productId = string.IsNullOrWhiteSpace(mainItemId)
                ? GetContextItem<LocalProduct>()?.GlobalProduct?.MainItemID ?? String.Empty
                : mainItemId;

            var allProductsStatistics = bazaarvoiceService.GetReviewStatisticsDictionary();
            var reviewStatistics = allProductsStatistics.ContainsKey(productId)
                ? allProductsStatistics[productId]
                : new ReviewStatistics();
            ViewBag.IsClickable = isClickable;
            return View("~/Views/Feature/Products/ProductRating.cshtml", reviewStatistics);
        }

        public ActionResult ProductReviews(ProductReviewsPagingViewModel parameters)
        {
            if (localMarketSettings == null || reviewServiceConfiguration == null)
            {
                return new EmptyResult();
            }

            var isAjax = Request.IsAjaxRequest();

            var productId = isAjax
                ? parameters.MainItemId
                : GetContextItem<LocalProduct>()?.GlobalProduct?.MainItemID;

            if (string.IsNullOrWhiteSpace(productId))
            {
                return new EmptyResult();
            }

            ReviewStatistics reviewStatistics;
            IEnumerable<Review> reviews;
            var allProductsStatistics = bazaarvoiceService.GetReviewStatisticsDictionary();
            if (allProductsStatistics.ContainsKey(productId))
            {
                reviewStatistics = allProductsStatistics[productId];
                reviews = reviewStatistics.TotalReviewCount > 0 ? bazaarvoiceService.GetProductReviews(productId) : Enumerable.Empty<Review>();
                var orderRule = orderByRules.FirstOrDefault(x => x.Key == parameters.SortBy)?.OrderByExpression;
                if (orderRule != null)
                {
                    reviews = orderRule(reviews);
                }
            }
            else
            {
                reviewStatistics = new ReviewStatistics();
                reviews = Enumerable.Empty<Review>();
            }

            parameters.PageSize = reviewServiceConfiguration.ItemsPerPage;
            parameters.TotalResults = reviews.Count();

            var model = new ProductReviewsViewModel
            {
                MainItemId = productId,
                ReviewStatistics = reviewStatistics,
                Reviews = reviews,
                OrderByRules = orderByRules,
                Pagination = parameters,
                JavaScriptUrl = reviewServiceConfiguration.JavaScriptURL
            };

            if (isAjax)
            {
                return View("~/Views/Feature/Products/_ProductReviewsList.cshtml", model);
            }
            return View("~/Views/Feature/Products/ProductReviews.cshtml", model);
        }
    }
}