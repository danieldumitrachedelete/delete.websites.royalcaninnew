using Delete.Foundation.DataTemplates.Models.Interfaces;
using Sitecore.Data;
using Context = Sitecore.Context;

namespace Delete.Feature.Products.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using AutoMapper;
    using Comparers;
    using Delete.Feature.Breeds.Models;
    using Delete.Foundation.DeleteFoundationCore.Extensions;
    using Delete.Foundation.MultiSite.Models;

    using Foundation.DataTemplates.Managers;
    using Foundation.DataTemplates.Models;
    using Foundation.DataTemplates.Models.Taxonomy;
    using Foundation.DeleteFoundationCore;
    using Foundation.DeleteFoundationCore.Controllers;
    using Foundation.DeleteFoundationCore.Interfaces;
    using Foundation.DeleteFoundationCore.Models;
    using Glass.Mapper.Sc;
    using Managers;
    using Models;

    using Sitecore.Collections;
    using Sitecore.Mvc.Extensions;

    public class ProductsController : BaseController
    {
        private const int InitialPageIndex = 1;
        public const int RecentlyViewedProductsNumber = 2;

        private const string DefaultFacetsCacheKey = "productfacets_{0}_{1}_{2}_{3}";

        private readonly IFilterGroupFolderSearchManager filterGroupFolderSearchManager;
        private readonly IProductSearchManager productSearchManager;
        private readonly I_StockistSearchManager stockistSearchManager;


        public ProductsController(
            IProductSearchManager productSearchManager,
            IFilterGroupFolderSearchManager filterGroupFolderSearchManager,
            ISitecoreContext sitecoreContext,
            IMapper mapper,
            ICacheManager cacheManager,
            I_StockistSearchManager stockistSearchManager) : base(sitecoreContext, mapper, cacheManager)
        {
            this.productSearchManager = productSearchManager;
            this.filterGroupFolderSearchManager = filterGroupFolderSearchManager;
            this.stockistSearchManager = stockistSearchManager;
        }

        private const string ProductsFilterGroupsCacheKey = "productsfiltergroups_{0}_{1}";
        private const string AllProductsCacheKey = "allproducts_{0}_{1}_{2}_{3}";
        private const string DefaultAllProductsCacheKey = "allproducts_{0}";
        private const string ProductsByKeywordCacheKey = "productbykeyword_{0}_{1}_{2}";
        private const string ProductCarouselCacheKey = "allproducts_{0}_{1}_{2}_{3}_{4}";
        private const string ProductsByGlobalIdsCacheKey = "localproductsbyglobalids_{0}_{1}";
        private const string RelatedProductsCacheKey = "relatedproducts_{0}_{1}";

        [HttpGet]
        public ActionResult Search(ProductsFilterViewModel filterConfiguration)
        {
            var productFilterConfiguration = this.GetDataSourceItem<ProductFilterConfiguration>();
            if (productFilterConfiguration == null && Sitecore.Context.PageMode.IsExperienceEditorEditing)
            {
                return this.Content("<p>Please select product filter configuration</p>");
            }

            var species = productFilterConfiguration.Species;
            var pillar = productFilterConfiguration.Pillar;
            var selectedFilterGroups = productFilterConfiguration.Filters;
            var pageSize = productFilterConfiguration.PageSize;

            var groupsCacheKey = string.Format(ProductsFilterGroupsCacheKey, species.Id, pillar.Id);
            var allFilterGroups =
                this._cacheManager.CacheResults(() => this.filterGroupFolderSearchManager.GetAll(), groupsCacheKey);

            var sortedFilterGroups = selectedFilterGroups
                .Select(x => x.Id)
                .Select(filterGroupId => allFilterGroups.FirstOrDefault(x => x.Id == filterGroupId))
                .Where(g => g != null)
                .ToList();
            var root = this.SitecoreContext.GetRootItemRelative<IGlassBase>();

            var facetsCacheKey = string.Format(DefaultFacetsCacheKey, Context.Language, root.Id, species.Id, pillar.Id);
            var defaultFacets = this._cacheManager.CacheResults(() => this.productSearchManager.GetFacets(root, species.Id, pillar.Id), facetsCacheKey);

            var productsCacherKey = string.Format(AllProductsCacheKey,
                string.Join("|", filterConfiguration.FiltersDictionary.Values.Where(x => x.NotEmpty())), root,
                species.Id, pillar.Id);

            var products = this._cacheManager.CacheResults(
                () => this.productSearchManager.Filter(filterConfiguration, root, species.Id, pillar.Id),
                productsCacherKey);

            var result = new Delete.Feature.Products.Models.SearchViewModel
            {
                FilterConfiguration = productFilterConfiguration.Id,
                FilterModel = new Delete.Feature.Products.Models.SearchFilterViewModel
                {
                    FilterGroups = sortedFilterGroups,
                    Specie = species,
                    PillarId = pillar.Id,
                    DefaultFacets = defaultFacets,
                    CurrentFacets = products.Facets,
                    Filters = filterConfiguration
                },
                SearchResultsModel = new SearchResultsViewModel
                {
                    SearchResults = products,
                    Page = InitialPageIndex,
                    PageSize = pageSize,
                    TotalResults = products.Results.Count
                }
            };

            return this.View("~/Views/Feature/Products/Search.cshtml", result);
        }

        [HttpGet]
        public ActionResult SearchProducts(ProductsFilterViewModel filterConfiguration)
        {
            var productFilterConfiguration = this.SitecoreContext.GetItem<ProductFilterConfiguration>(filterConfiguration.Filter, false, true);
            var species = productFilterConfiguration.Species;
            var pillar = productFilterConfiguration.Pillar;
            var currentPage = filterConfiguration.Page;
            var pageSize = productFilterConfiguration.PageSize;

            var root = this.SitecoreContext.GetRootItemRelative<IGlassBase>();

            var productsCacherKey = string.Format(AllProductsCacheKey,
                string.Join("|", filterConfiguration.FiltersDictionary.Values.Where(x => x.NotEmpty())), root,
                species.Id, pillar.Id);

            var products = this._cacheManager.CacheResults(
                () => this.productSearchManager.Filter(filterConfiguration, root, species.Id, pillar.Id),
                productsCacherKey);

            var result = new SearchResultsViewModel
            {
                SearchResults = products,
                Page = currentPage,
                PageSize = pageSize,
                TotalResults = products.Results.Count
            };

            return this.View("~/Views/Feature/Products/_ProductsSearchResults.cshtml", result);
        }

        [HttpGet]
        public ActionResult FindProducts(ProductFinderModel model)
        {
            Guard.ArgumentNotNull(model, nameof(model));

            var cacheKey = string.Format(DefaultAllProductsCacheKey, model.Species);
            var products = this._cacheManager.CacheResults(
                () => this.productSearchManager.GetAll(model.Species).Where(x => x.GlobalProduct != null).ToList(), cacheKey);

            var wetProducts = new List<LocalProduct>();
            var dryProducts = new List<LocalProduct>();

            if (model.IDs?.Wet != null && model.IDs.Wet.Any())
            {
                wetProducts.AddRange(products
                    .Where(x => x.ExtendedProductType == ProductTypeConstants.WetProductTypeId.Guid)
                    .Where(x => model.IDs.Wet.Contains(x.GlobalProduct.MainItemID)));
            }

            if (model.IDs?.Dry != null && model.IDs.Dry.Any())
            {
                dryProducts.AddRange(products
                    .Where(x => x.ExtendedProductType == ProductTypeConstants.DryProductTypeId.Guid)
                    .Where(x => model.IDs.Dry.Contains(x.GlobalProduct.MainItemID)));
            }

            var result = new
            {
                DRY = this._mapper.Map<IEnumerable<ProductFinderViewModel>>(dryProducts),
                WET = this._mapper.Map<IEnumerable<ProductFinderViewModel>>(wetProducts)
            };

            return this.Json(result, JsonRequestBehavior.AllowGet);
        }

        // http://royalcanin.deletedev.com/en-us/api/products/FindByKeyword?species={42B60B8C-D080-477D-BB15-B5F8DDBFFE3A}
        [HttpGet]
        public ActionResult FindByKeyword(Guid? species, Guid? pillarId, string keyword = "")
        {
            var localBreedJson = this.GetPredictiveSearchResults(species, pillarId, keyword);

            return this.Json(localBreedJson, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewProduct()
        {
            var dataSource = this.GetDataSourceItem<ViewProductConfiguration>();
            if (dataSource == null)
            {
                return new EmptyResult();
            }

            var globalBreed = GetContextItem<LocalBreed>()?.GlobalBreed;

            if (!dataSource.Breeds.Any())
            {
                var globalBreedId = globalBreed?.Id;

                if (globalBreedId != null)
                {
                    dataSource.BreedIds = new List<Guid>();
                    dataSource.BreedIds.Add(globalBreedId.Value);
                }
            }

            if (!dataSource.Sizes.Any())
            {
                var globalBreedSize = globalBreed?.Size;

                if (globalBreedSize != null)
                {
                    dataSource.SizeIds = new List<Guid>();
                    dataSource.SizeIds.Add(globalBreedSize.Id);
                }
            }

            if (dataSource.Product == null)
            {
                dataSource.Product = this.productSearchManager.SearchByTaxonomyForTimeline(dataSource, 1).FirstOrDefault();
            }

            return this.View("~/Views/Feature/Products/ViewProduct.cshtml", dataSource);
        }

        public ActionResult LogProductViewActivity()
        {
            var currentProduct = GetContextItem<LocalProduct>();

            var recentlyViewedProducts = ProductSessionManager.GetCurrent().RecentlyViewedProducts;
            if (currentProduct?.GlobalProduct == null)
            {
                return new EmptyResult();
            }
            else if (recentlyViewedProducts.Contains(currentProduct.GlobalProduct.Id))
            {
                recentlyViewedProducts.RemoveAll(x => x == currentProduct.GlobalProduct.Id);
                recentlyViewedProducts.Add(currentProduct.GlobalProduct.Id);
                return new EmptyResult();
            }

            recentlyViewedProducts.Add(currentProduct.GlobalProduct.Id);

            return new EmptyResult();
        }

        public ActionResult SeeRecentlyViewedProducts()
        {
            var currentProduct = GetContextItem<LocalProduct>();

            var currentGlobalProductId = currentProduct?.GlobalProduct?.Id ?? Guid.Empty;
            var recentlyViewedProductIds = ProductSessionManager.GetCurrent()
                .RecentlyViewedProducts
                .Except(new[] {currentGlobalProductId})
                .ToList();

            if (recentlyViewedProductIds.Count > RecentlyViewedProductsNumber)
            {
                recentlyViewedProductIds = recentlyViewedProductIds.GetRange(recentlyViewedProductIds.Count - RecentlyViewedProductsNumber, RecentlyViewedProductsNumber);
            }

            var rootItem = this.SitecoreContext.GetRootItem<LocalMarketSettings>();

            var cacheKey = string.Format(ProductsByGlobalIdsCacheKey, rootItem?.Id, string.Join("|", recentlyViewedProductIds));
            var products = this._cacheManager.CacheResults(
                    () => this.productSearchManager.GetByGlobalProductIds(rootItem, recentlyViewedProductIds)
                        .Distinct(new LocalProductComparer()).ToList(), cacheKey);

            return this.View("~/Views/Feature/Products/RecentlyViewedProducts.cshtml", products);
        }

        public ActionResult ProductStockists()
        {
            var rootItem = this.SitecoreContext.GetRootItemRelative<LocalMarketSettings>();
            var homePage = this.SitecoreContext.GetHomeItem <GlassBase> ();
            if (rootItem.StockistHideOnlineRetailers)
            {
                return new EmptyResult();
            }
            else
            {
                var currentLocalProduct = this.SitecoreContext.GetCurrentItem<LocalProduct>();
                
                var eRetailItemId = new Guid(Sitecore.Configuration.Settings.GetSetting("Feature.Stockists.ERetailItemId"));
                var eRetailStockistsFolder = Sitecore.Configuration.Settings.GetSetting("Feature.Stockists.ERetailStockistsFolder");
                var stockistsFolder = homePage.Children.FirstOrDefault(x => x.Name == eRetailStockistsFolder);
                var stockistTemplateId = new Guid(Sitecore.Configuration.Settings.GetSetting("Feature.Stockist.TemplateId"));


                var stockists = stockistSearchManager
                    .FindStockistsByPillarAndType(currentLocalProduct.ExtendedPillarIDCode, eRetailItemId, stockistTemplateId, stockistsFolder?.Id)
                    .Take(4)
                    .ToList();

                

                var model = new ProductStockistsViewModel
                {
                    Stockists = stockists,
                    WhereToBuyLink = rootItem.WhereToBuyPage
                };

                return this.View("~/Views/Feature/Products/ProductStockists.cshtml", model);
            }
        }

        public ActionResult RelatedProducts()
        {
            var currentLocalProduct = this.SitecoreContext.GetCurrentItem<LocalProduct>();

            if (currentLocalProduct != null)
            {
                var searchCriteria = new RelatedProductSearchCriteria(currentLocalProduct);

                var cacheKey = string.Format(RelatedProductsCacheKey, currentLocalProduct.Id,
                    string.Join("|", searchCriteria.Criterias));

                var model = this._cacheManager.CacheResults(() => this.productSearchManager.GetRelated(searchCriteria, 4), cacheKey);
                //var model = this.productSearchManager.GetRelated(searchCriteria, 4);

                return this.View("~/Views/Feature/Products/RelatedProducts.cshtml", model);
            }

            return new EmptyResult();
        }

        public ActionResult ProductCarousel()
        {
            var model = this.GetDataSourceItem<ProductCarousel>();
            if (model == null)
            {
                return new EmptyResult();
            }

            var rootItem = this.SitecoreContext.GetRootItemRelative<LocalMarketSettings>();

            if (model.GlobalProducts != null && model.GlobalProducts.Any())
            {
                var cacheKey = string.Format(ProductsByGlobalIdsCacheKey, rootItem?.Id, string.Join("|", model.GlobalProducts));
                model.Products = this._cacheManager.CacheResults(
                    this.productSearchManager.GetByGlobalProductIds(rootItem, model.GlobalProducts.ToList()).ToList, cacheKey);
            }
            else
            {
                var filters = new ProductsFilterViewModel
                                  {
                                      Breeds = string.Join(",", model.Breeds.Select(x => x.Id.ToString("N"))),
                                      FeedingPeriod = model.FeedingPeriod,
                                      ProductRanges = string.Join(",", model.ProductRanges.Select(x => x.Id.ToString("N"))),
                                      ProductTypes = model.ProductType != Guid.Empty ? model.ProductType.ToString("N") : string.Empty,
                                      Sizes = string.Join(",", model.AnimalSizes.Select(x => x.Id.ToString("N"))),
                                      SpecialNeeds = string.Join(",", model.SpecialNeeds.Select(x => x.ToString("N")))
                                  };

                var productsCacheKey = string.Format(ProductCarouselCacheKey,
                    string.Join("|", filters.FiltersDictionary.Values.Where(x => x.NotEmpty())), rootItem, model.Specie, model.PillarID, model.Count);

                model.Products = this._cacheManager.CacheResults(
                    () => this.productSearchManager.FilterWithoutFacets(filters, rootItem, model.Specie, model.PillarID, model.Count),
                    productsCacheKey);

                var productCatalogue = rootItem.ProductRootItems.FirstOrDefault(
                    x => x.PCPillars.Any(z => z.Id == model.PillarID) && x.PCSpecies.Any(z => z.Id == model.Specie));
                if (productCatalogue != null)
                {
                    model.ViewAllLinkUrl = this.Url.UrlWithParameters(productCatalogue.Url, filters.FiltersDictionary.Where(x => x.Value.NotEmpty()).ToDictionary()); 
                }
            }

            if (model.ViewAllLink != null && model.ViewAllLink.Url.NotEmpty())
            {
                model.ViewAllLinkUrl = model.ViewAllLink.BuildUrl(new SafeDictionary<string>());
            }

            return this.View("~/Views/Feature/Products/ProductCarousel.cshtml", model);
        }

        protected PredictiveSearchJsonModel GetPredictiveSearchResults(Guid? species, Guid? pillar, string keyword = "")
        {
            if (!species.HasValue || species == Guid.Empty)
            {
                species = SpecieConstants.DogItemId;
            }

            var cacheKey = string.Format(ProductsByKeywordCacheKey, species, pillar, keyword);
            var products =
                this._cacheManager.CacheResults(() => this.productSearchManager.FindByKeyword(species, pillar, keyword),
                    cacheKey);
            var predictiveItems = this._mapper.Map<IEnumerable<PredictiveSearchItemJsonModel>>(products);

            return new PredictiveSearchJsonModel
            {
                Items = predictiveItems
            };
        }
    }
}

