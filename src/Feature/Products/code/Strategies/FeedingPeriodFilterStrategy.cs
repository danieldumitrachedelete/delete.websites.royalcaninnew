﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using Delete.Feature.Products.Managers;
using Delete.Foundation.DataTemplates.Managers;
using Delete.Foundation.DataTemplates.Models.Filter;
using Delete.Foundation.DataTemplates.Strategies;
using Delete.Foundation.RoyalCaninCore;
using JetBrains.Annotations;
using Sitecore.Mvc.Extensions;

namespace Delete.Feature.Products.Strategies
{
    using Sitecore.Data.Items;

    [UsedImplicitly]
    public class FeedingPeriodFilterStrategy : BaseFilterStrategy
    {
        private readonly IDictionaryEntryManager dictionaryEntryManager;

        public FeedingPeriodFilterStrategy()
        {
            this.dictionaryEntryManager = new DictionaryEntryManager();
        }

        public override IEnumerable<FilterItem> GetFilterItems(Guid species)
        {
            var feedingPeriodConversions =
                this.dictionaryEntryManager
                    .GetAllInFolder(Constants.SitecoreIds.FeeedingPeriodConvertions.FolderID.Guid)
                    .OrderBy(x => Convert.ToDecimal(x.Value, new NumberFormatInfo {NumberDecimalSeparator = "."}))
                    .ToList();

            return feedingPeriodConversions
                .Select(x => new FilterItem
                {
                    Value = x.Value,
                    Text = x.Text,
                    Age = x.Value,
                });
        }

        public override IEnumerable<FilterItem> GetFilterItems(Item parentItem)
        {
            return new List<FilterItem>();
        }
    }
}