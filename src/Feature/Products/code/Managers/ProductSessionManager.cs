﻿using System;
using System.Collections.Generic;
using Delete.Foundation.RoyalCaninCore.Managers;

namespace Delete.Feature.Products.Managers
{
    [Serializable]
    public class ProductSessionManager : SessionManager<ProductSessionManager>
    {
        public List<Guid> RecentlyViewedProducts = new List<Guid>();
    }
}