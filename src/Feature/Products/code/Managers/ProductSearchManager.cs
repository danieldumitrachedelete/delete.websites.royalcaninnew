﻿using Castle.Core.Internal;
using Delete.Feature.Products.Models;
using Delete.Foundation.DataTemplates.Models.Taxonomy;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.DeleteFoundationCore.Managers;
using Sitecore.ContentSearch.Linq;
using Sitecore.ContentSearch.Linq.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using LocalProduct = Delete.Feature.Products.Models.LocalProduct;

namespace Delete.Feature.Products.Managers
{
    using Foundation.DeleteFoundationCore.Models;

    public interface IProductSearchManager : IBaseSearchManager<LocalProduct>
    {
        IEnumerable<LocalProduct> GetAll(Guid species);

        FacetedResults<LocalProduct> GetAllLocal(IGlassBase root = null, Guid? species = null, Guid? pillar = null);

        IEnumerable<LocalProduct> SearchByTaxonomy(Taxonomy taxonomy, int count, bool ignoreVetProducts = true);

        IEnumerable<LocalProduct> SearchByTaxonomyForTimeline(Taxonomy taxonomy, int count, bool ignoreVetProducts = true);

        IEnumerable<LocalProduct> GetRelated(RelatedProductSearchCriteria searchCriteria, int? count = null);

        IEnumerable<LocalProduct> FindByKeyword(Guid? species, Guid? pillar, string keyword);

        FacetedResults<LocalProduct> Filter(
            ProductsFilterViewModel filters,
            IGlassBase root = null,
            Guid? species = null,
            Guid? pillar = null);

        IDictionary<string, int> GetFacets(
            IGlassBase root = null,
            Guid? species = null,
            Guid? pillar = null);

        IList<LocalProduct> FilterWithoutFacets(
            ProductsFilterViewModel filters,
            IGlassBase root = null,
            Guid? species = null,
            Guid? pillar = null,
            int count = 0);

        IList<LocalProduct> GetByGlobalProductIds(IGlassBase root, IList<Guid> globalProductIds);
    }

    public class ProductSearchManager : BaseSearchManager<LocalProduct>, IProductSearchManager
    {
        private Pillar _vetPillar = null;
        protected Pillar VetPillar => _vetPillar ?? (_vetPillar = SitecoreContext.GetItem<Pillar>(PillarConstants.VetPillarItemPath));

        public IEnumerable<LocalProduct> GetAll(Guid species)
        {
            return this.GetQueryableInternal(species: species)
                .MapResults(this.SitecoreContext, true)
                .ToList();
        }

        public FacetedResults<LocalProduct> GetAllLocal(IGlassBase root = null, Guid? species = null, Guid? pillar = null)
        {
            return new FacetedResults<LocalProduct>
            {
                Results = this.GetQueryableInternal(root, species, pillar).MapResults(this.SitecoreContext, true).ToList(),
                Facets = this.GetFacetsInternal(this.GetQueryableInternal(root, species, pillar))
            };
        }

        public IEnumerable<LocalProduct> SearchByTaxonomyForTimeline(Taxonomy taxonomy, int count, bool ignoreVetProducts = true)
        {
            // todo: pass root item
            var predicate = PredicateBuilder.True<LocalProduct>();

            predicate = taxonomy.Lifestages.Aggregate(predicate, (current, lifestage) => current.And(x => x.SearchableLifestages.Contains(lifestage.Id)));
            predicate = taxonomy.Species.Aggregate(predicate, (current, specie) => current.And(x => x.SearchableSpecie == specie.Id.ToString("N")));



            if (ignoreVetProducts && VetPillar != null)
            {
                var ignoreVetProductsPredicate = PredicateBuilder.True<LocalProduct>()
                    .And(x => x.SearchablePillar != VetPillar.Id.ToString("N"));
                predicate = predicate.And(ignoreVetProductsPredicate);

            }

            var predicateWithExactBreedMatch = predicate;

            if (taxonomy.Breeds.Any())
            {
                predicateWithExactBreedMatch = taxonomy.Breeds.Aggregate(predicateWithExactBreedMatch, (current, breed) => current.And(x => x.SearchableBreed == breed.Id));
            }
            else
            {
                predicateWithExactBreedMatch = taxonomy.BreedIds.Aggregate(predicateWithExactBreedMatch, (current, breed) => current.And(x => x.SearchableBreed == breed));
            }


            var result = this.GetQueryableInternal()
                .Where(predicateWithExactBreedMatch)
                .Take(count)
                .MapResults(this.SitecoreContext, true)
                .ToList();

            if (!result.IsNullOrEmpty())
            {
                return result;
            }
            else
            {
                var emptyBreed = new Guid("{00000000-0000-0000-0000-000000000000}");
                var predicateProductsWithoutBreed = taxonomy.BreedIds.Aggregate(predicate,
                    (current, breed) => current.And(x => x.SearchableBreed == emptyBreed));

                if (taxonomy.Sizes.Any())
                {
                    predicateProductsWithoutBreed = taxonomy.Sizes.Aggregate(predicateProductsWithoutBreed, (current, size) => current.And(x => x.SearchableAnimalSizes.Contains(size.Id)));
                }
                else
                {
                    predicateProductsWithoutBreed = taxonomy.SizeIds?.Aggregate(predicateProductsWithoutBreed, (current, size) => current.And(x => x.SearchableAnimalSizes.Contains(size)));
                }

                result = this.GetQueryableInternal()
                    .Where(predicateProductsWithoutBreed)
                    .Take(count)
                    .MapResults(this.SitecoreContext, true)
                    .ToList();

                return result;
            }
        }

        public IEnumerable<LocalProduct> SearchByTaxonomy(Taxonomy taxonomy, int count, bool ignoreVetProducts = true)
        {
            // todo: pass root item
            var predicate = PredicateBuilder.False<LocalProduct>();
            predicate = taxonomy.Sizes.Aggregate(predicate, (current, size) => current.Or(x => x.SearchableAnimalSizes.Contains(size.Id)));
            if (taxonomy.Breeds.Any())
            {
                predicate = taxonomy.Breeds.Aggregate(predicate, (current, breed) => current.Or(x => x.SearchableBreed == breed.Id));
            }
            else
            {
                predicate = taxonomy.BreedIds.Aggregate(predicate, (current, breed) => current.Or(x => x.SearchableBreed == breed));
            }
            predicate = taxonomy.Lifestages.Aggregate(predicate, (current, lifestage) => current.Or(x => x.SearchableLifestages.Contains(lifestage.Id)));

            predicate = taxonomy.Species.Aggregate(predicate, (current, specie) => current.Or(x => x.SearchableSpecie == specie.Id.ToString("N")));

            if (ignoreVetProducts && VetPillar != null)
            {
                var ignoreVetProductsPredicate = PredicateBuilder.True<LocalProduct>()
                    .And(x => x.SearchablePillar != VetPillar.Id.ToString("N"));
                predicate = predicate.And(ignoreVetProductsPredicate);
            }

            return this.GetQueryableInternal()
                .Where(predicate)
                .Take(count)
                .MapResults(this.SitecoreContext, true)
                .ToList();
        }

        public IEnumerable<LocalProduct> GetRelated(RelatedProductSearchCriteria searchCriteria, int? count = null)
        {
            if (searchCriteria.Pillars != VetPillar.Id)
            {
                var speciesPredicate = PredicateBuilder.False<LocalProduct>()
                    .Or(x => x.SearchableSpecie == searchCriteria.Species.ToString("N"));

                var ignoredProductsPredicate = PredicateBuilder.False<LocalProduct>();
                ignoredProductsPredicate = searchCriteria.ProductsToIgnore.Aggregate(
                    ignoredProductsPredicate,
                    (current, productId) => current.Or(x => x.Id != productId));

                var criteriaPredicate = PredicateBuilder.False<LocalProduct>()
                    .Or(x => x.SearchableBreed == searchCriteria.Breed)
                    .Or(x => x.SearchableProductType == searchCriteria.ProductType);

                criteriaPredicate = searchCriteria.Lifestages.Aggregate(
                    criteriaPredicate,
                    (current, lifestage) => current.Or(x => x.SearchableLifestages.Contains(lifestage)));

                var predicate = speciesPredicate
                    .And(ignoredProductsPredicate)
                    .And(criteriaPredicate);

                IEnumerable<SearchHit<LocalProduct>> results = this.GetQueryableInternal()
                    .Where(predicate)
                    .GetResults().Hits
                    .OrderByDescending(x => x.Score)
                    .Where(x => x.Document.ParentId.Guid == searchCriteria.Category);

                if (count.HasValue)
                {
                    results = results.Take(count.Value);
                }

                return results.Select(x => x.Document).MapResults(this.SitecoreContext, true).ToList();
            }
            else
            {
                return new List<LocalProduct>();
            }

        }

        public IEnumerable<LocalProduct> FindByKeyword(Guid? species, Guid? pillar, string keyword)
        {
            var query = this.GetQueryableInternal(species: species, pillar: pillar);

            query = this.FindByKeywordInternal(query, keyword);

            query = query.OrderBy(x => x.SearchableProductName);

            return query
                .MapResults(this.SitecoreContext)
                .ToList();
        }

        public FacetedResults<LocalProduct> Filter(ProductsFilterViewModel filters, IGlassBase root = null, Guid? species = null, Guid? pillar = null)
        {
            var query = this.GetQueryableInternal(root, species, pillar);

            query = this.FindByKeywordInternal(query, filters.SearchQuery);

            query = this.FilterInternal(query, filters);

            return new FacetedResults<LocalProduct>
            {
                Results = query.MapResults(this.SitecoreContext, true).ToList(),
                Facets = this.GetFacetsInternal(query)
            };
        }

        public IDictionary<string, int> GetFacets(IGlassBase root = null, Guid? species = null, Guid? pillar = null)
        {
            var query = this.GetQueryableInternal(root, species, pillar);
            return this.GetFacetsInternal(query);
        }

        public IList<LocalProduct> FilterWithoutFacets(ProductsFilterViewModel filters, IGlassBase root = null, Guid? species = null, Guid? pillar = null, int count = 0)
        {
            var query = this.GetQueryableInternal(root, species, pillar);

            query = this.FindByKeywordInternal(query, filters.SearchQuery);

            query = this.FilterInternal(query, filters);

            if (count > 0)
            {
                query = query.Take(count);
            }

            return query.MapResults(this.SitecoreContext).ToList();
        }

        public IList<LocalProduct> GetByGlobalProductIds(IGlassBase root, IList<Guid> globalProductIds)
        {
            var query = this.GetQueryableInternal(root);

            var predicate = globalProductIds.Aggregate(PredicateBuilder.False<LocalProduct>(), (current, globalProductId) => current.Or(x => x.GlobalProductId == globalProductId));
            query = query.Where(predicate);

            return query.MapResults(this.SitecoreContext).OrderBy(x => globalProductIds.IndexOf(x.GlobalProductId)).ToList();
        }

        protected IQueryable<LocalProduct> FindByKeywordInternal(IQueryable<LocalProduct> products, string keyword)
        {
            if (keyword.Empty())
            {
                return products;
            }

            var terms = keyword.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            if (!terms.Any())
            {
                return products;
            }

            var predicate = PredicateBuilder.False<LocalProduct>();

            predicate = predicate.Or(x => x.SearchableProductName.StartsWith(keyword)

                                          || x.BenefitTextOverride.StartsWith(keyword)
                                          || (x.BenefitText.StartsWith(keyword) && x.BenefitTextOverride == null)

                                          || x.MainBenefitOverride.StartsWith(keyword)
                                          || (x.MainBenefit.StartsWith(keyword) && x.MainBenefitOverride == null)

                                          || x.SecondaryBenefitOverride.StartsWith(keyword)
                                          || (x.SecondaryBenefit.StartsWith(keyword) && x.SecondaryBenefitOverride == null)

                                          || x.TextSpare2Override.StartsWith(keyword)
                                          || (x.TextSpare2.StartsWith(keyword) && x.TextSpare2Override == null)

                                          || x.SearchableSpecialNeedsText.StartsWith(keyword)

                                          || x.SearchableSterilisedText.StartsWith(keyword));

            if (terms.Length > 1)
            {
                var firstWord = terms.FirstOrDefault();

                var predicateName = PredicateBuilder.True<LocalProduct>();
                predicateName = predicateName.And(x => x.SearchableProductName.StartsWith(firstWord));
                predicateName = terms.Skip(1).Aggregate(predicateName, (current, term) => current.And(x => x.SearchableProductName.Contains(term)));

                var predicateBenefitOverride = PredicateBuilder.True<LocalProduct>();
                predicateBenefitOverride = predicateBenefitOverride.And(x => x.BenefitTextOverride.StartsWith(firstWord));
                predicateBenefitOverride = terms.Skip(1).Aggregate(predicateBenefitOverride, (current, term) => current.And(x => x.BenefitTextOverride.Contains(term)));
                var predicateBenefit = PredicateBuilder.True<LocalProduct>();
                predicateBenefit = predicateBenefit.And(x => x.BenefitText.StartsWith(firstWord) && x.BenefitTextOverride == null);
                predicateBenefit = terms.Skip(1).Aggregate(predicateBenefit, (current, term) => current.And(x => x.BenefitText.Contains(term)));

                var predicateMainBenefitOverride = PredicateBuilder.True<LocalProduct>();
                predicateMainBenefitOverride = predicateMainBenefitOverride.And(x => x.MainBenefitOverride.StartsWith(firstWord));
                predicateMainBenefitOverride = terms.Skip(1).Aggregate(predicateMainBenefitOverride, (current, term) => current.And(x => x.MainBenefitOverride.Contains(term)));
                var predicateMainBenefit = PredicateBuilder.True<LocalProduct>();
                predicateMainBenefit = predicateMainBenefit.And(x => x.MainBenefit.StartsWith(firstWord) && x.MainBenefitOverride == null);
                predicateMainBenefit = terms.Skip(1).Aggregate(predicateMainBenefit, (current, term) => current.And(x => x.MainBenefit.Contains(term)));

                var predicateSecondaryBenefitOverride = PredicateBuilder.True<LocalProduct>();
                predicateSecondaryBenefitOverride = predicateSecondaryBenefitOverride.And(x => x.SecondaryBenefitOverride.StartsWith(firstWord));
                predicateSecondaryBenefitOverride = terms.Skip(1).Aggregate(predicateSecondaryBenefitOverride, (current, term) => current.And(x => x.SecondaryBenefitOverride.Contains(term)));
                var predicateSecondaryBenefit = PredicateBuilder.True<LocalProduct>();
                predicateSecondaryBenefit = predicateSecondaryBenefit.And(x => x.SecondaryBenefit.StartsWith(firstWord) && x.SecondaryBenefitOverride == null);
                predicateSecondaryBenefit = terms.Skip(1).Aggregate(predicateSecondaryBenefit, (current, term) => current.And(x => x.SecondaryBenefit.Contains(term)));

                var predicateSpareOverride = PredicateBuilder.True<LocalProduct>();
                predicateSpareOverride = predicateSpareOverride.And(x => x.TextSpare2Override.StartsWith(firstWord));
                predicateSpareOverride = terms.Skip(1).Aggregate(predicateSpareOverride, (current, term) => current.And(x => x.TextSpare2Override.Contains(term)));
                var predicateSpare = PredicateBuilder.True<LocalProduct>();
                predicateSpare = predicateSpare.And(x => x.TextSpare2.StartsWith(firstWord) && x.TextSpare2Override == null);
                predicateSpare = terms.Skip(1).Aggregate(predicateSpare, (current, term) => current.And(x => x.TextSpare2.Contains(term)));

                var predicateNeeds = PredicateBuilder.True<LocalProduct>();
                predicateNeeds = predicateNeeds.And(x => x.SearchableSpecialNeedsText.StartsWith(firstWord));
                predicateNeeds = terms.Skip(1).Aggregate(predicateNeeds, (current, term) => current.And(x => x.SearchableSpecialNeedsText.Contains(term)));

                var predicateSterilised = PredicateBuilder.True<LocalProduct>();
                predicateSterilised = predicateSterilised.And(x => x.SearchableSterilisedText.StartsWith(firstWord));
                predicateSterilised = terms.Skip(1).Aggregate(predicateSterilised, (current, term) => current.And(x => x.SearchableSterilisedText.Contains(term)));

                predicate = predicate.Or(predicateName);

                predicate = predicate.Or(predicateBenefitOverride);
                predicate = predicate.Or(predicateBenefit);

                predicate = predicate.Or(predicateMainBenefitOverride);
                predicate = predicate.Or(predicateMainBenefit);

                predicate = predicate.Or(predicateSecondaryBenefitOverride);
                predicate = predicate.Or(predicateSecondaryBenefit);

                predicate = predicate.Or(predicateSpareOverride);
                predicate = predicate.Or(predicateSpare);

                predicate = predicate.Or(predicateNeeds);
                predicate = predicate.Or(predicateSterilised);
            }

            return products.Where(predicate);
        }

        protected IQueryable<LocalProduct> FilterInternal(IQueryable<LocalProduct> products, ProductsFilterViewModel filters)
        {
            if (filters.FeedingPeriod > 0)
            {
                products = products.Where(x =>
                    x.SearchableProductFeedingPeriodFrom <= filters.FeedingPeriod &&
                    x.SearchableProductFeedingPeriodTo >= filters.FeedingPeriod);
            }

            if (filters.Breeds.NotEmpty())
            {
                products = products.Filter(filters.Breeds.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(x => new Guid(x))
                    .Aggregate(PredicateBuilder.False<LocalProduct>(), (current, breedId) => current.Or(x => x.SearchableBreed == breedId)));
            }

            if (filters.ProductTypes.NotEmpty())
            {
                products = products.Filter(filters.ProductTypes.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(x => new Guid(x))
                    .Aggregate(PredicateBuilder.False<LocalProduct>(), (current, breedId) => current.Or(x => x.SearchableProductType == breedId)));
            }

            if (filters.Sizes.NotEmpty())
            {
                products = products.Filter(filters.Sizes.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(x => new Guid(x))
                    .Aggregate(PredicateBuilder.False<LocalProduct>(), (current, breedId) => current.Or(x => x.SearchableAnimalSizes.Contains(breedId))));
            }

            if (filters.SpecialNeeds.NotEmpty())
            {
                products = products.Filter(filters.SpecialNeeds.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(x => new Guid(x))
                    .Aggregate(PredicateBuilder.False<LocalProduct>(), (current, breedId) => current.Or(x => x.SearchableSpecialNeeds.Contains(breedId))));
            }

            if (filters.Indications.NotEmpty())
            {
                products = products.Filter(filters.Indications.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(x => new Guid(x))
                    .Aggregate(PredicateBuilder.False<LocalProduct>(), (current, breedId) => current.Or(x => x.SearchableIndications.Contains(breedId))));
            }

            if (filters.CounterIndications.NotEmpty())
            {
                products = products.Filter(filters.CounterIndications.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(x => new Guid(x))
                    .Aggregate(PredicateBuilder.False<LocalProduct>(), (current, breedId) => current.Or(x => x.SearchableCounterIndications.Contains(breedId))));
            }

            if (filters.ProductRanges.NotEmpty())
            {
                products = products.Filter(filters.ProductRanges.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(x => new Guid(x))
                    .Aggregate(PredicateBuilder.False<LocalProduct>(), (current, rangeId) => current.Or(x => x.SearchableProductRanges.Contains(rangeId))));
            }

            return products;
        }

        protected Dictionary<string, int> GetFacetsInternal(IQueryable<LocalProduct> products)
        {
            products = products.FacetOn(x => x.SearchableBreed, 1);
            products = products.FacetOn(x => x.SearchableProductType, 1);
            products = products.FacetOn(x => x.SearchableAnimalSizes, 1);
            products = products.FacetOn(x => x.SearchableSpecialNeeds, 1);
            products = products.FacetOn(x => x.SearchableIndications, 1);
            products = products.FacetOn(x => x.SearchableCounterIndications, 1);
            products = products.FacetOn(x => x.SearchableProductRanges, 1);

            return products.GetFacetsMultiselect()
                .Categories
                .SelectMany(x => x.Values)
                .ToDictionary(k => k.Name, v => v.AggregateCount, StringComparer.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Override of the default GetQueryable to filter items that are of or are based on LocalProduct template.
        /// Consider using GetQueryableInternal() instead to get items correctly filtered filtered for current GlobalRegion
        /// </summary>
        /// <returns></returns>
        protected override IQueryable<LocalProduct> GetQueryable()
        {
            return base.GetQueryable()
                .Where(x => x.TemplateId == LocalProductConstants.TemplateId
                            || x.BaseTemplates.Contains(LocalProductConstants.TemplateId.Guid));
        }

        protected IQueryable<LocalProduct> GetQueryableInternal(IGlassBase root = null, Guid? species = null, Guid? pillar = null)
        {
            var query = this.GetQueryable();

            if (species.HasValue) query = query.Where(x => x.SearchableSpecie == species.Value.ToString("N"));

            if (pillar.HasValue) query = query.Where(x => x.SearchablePillar == pillar.Value.ToString("N"));

            return this.FilterLocal(query, root);
        }
    }
}