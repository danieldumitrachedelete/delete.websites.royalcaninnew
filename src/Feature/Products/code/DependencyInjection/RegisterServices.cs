using Delete.Feature.Products.Import.Processors;
using Delete.Feature.Products.Managers;
using Delete.Feature.Products.Models;
using Delete.Feature.Products.Services;
using Delete.Foundation.DataTemplates.Models.Taxonomy;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.DeleteFoundationCore.Managers;
using Delete.Foundation.InitializeDependencyInjection.Extensions;
using Delete.Foundation.ItemImport.DependencyInjection;
using Delete.Foundation.ItemImport.Repositories;
using Delete.Foundation.MultiSite.Models;
using Glass.Mapper.Sc;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.DependencyInjection;

namespace Delete.Feature.Products.DependencyInjection
{
    public class RegisterServices : IServicesConfigurator
    {
        public void Configure(IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IBazaarvoiceService, BazaarvoiceService>();
            serviceCollection.AddScoped<IProductSearchManager, ProductSearchManager>();
            serviceCollection.AddScoped<IReviewServiceConfiguration, ReviewServiceConfiguration>(provider =>
                new SitecoreContext().GetItem<ReviewServiceConfiguration>(provider.GetService<ILocalMarketSettings>().ReviewServiceIntegration));

            ConfigureImport(serviceCollection);

            serviceCollection.AddMvcControllersInCurrentAssembly();

            RegisterIndexes();
        }

        private static void RegisterIndexes()
        {
            SearchManagersConfiguration.RegisterIndexConfiguration(typeof(LocalProduct), ProductsConstants.CustomIndexAlias);
            SearchManagersConfiguration.RegisterIndexConfiguration(typeof(GlobalProduct), ProductsConstants.CustomIndexAlias);
            SearchManagersConfiguration.RegisterIndexConfiguration(typeof(PackagingSize), ProductsConstants.CustomIndexAlias);
            SearchManagersConfiguration.RegisterIndexConfiguration(typeof(Indication), ProductsConstants.CustomIndexAlias);
            SearchManagersConfiguration.RegisterIndexConfiguration(typeof(Counterindication), ProductsConstants.CustomIndexAlias);
            SearchManagersConfiguration.RegisterIndexConfiguration(typeof(SpecialNeed), ProductsConstants.CustomIndexAlias);
            SearchManagersConfiguration.RegisterIndexConfiguration(typeof(Pillar), ProductsConstants.CustomIndexAlias);
            SearchManagersConfiguration.RegisterIndexConfiguration(typeof(ProductType), ProductsConstants.CustomIndexAlias);
        }

        private static void ConfigureImport(IServiceCollection serviceCollection)
        {
            serviceCollection
                .AddScoped<IGenericSitecoreItemRepository<GlobalProduct>, GenericSitecoreItemRepository<GlobalProduct>>();

            CatDogDiscerningServiceFactory<IGlobalProductItemProcessor, IGlobalCatProductItemProcessor,
                    IGlobalDogProductItemProcessor, GlobalCatProductItemProcessor, GlobalDogProductItemProcessor>
                .Register(serviceCollection);

            CatDogDiscerningServiceFactory<ILifestageItemProcessor, ICatLifestageItemProcessor,
                    IDogLifestageItemProcessor, CatLifestageItemProcessor, DogLifestageItemProcessor>
                .Register(serviceCollection);

            CatDogDiscerningServiceFactory<ISpecialNeedItemProcessor, ICatSpecialNeedProcessor,
                    IDogSpecialNeedProcessor, CatSpecialNeedProcessor, DogSpecialNeedProcessor>
                .Register(serviceCollection);
            CatDogDiscerningServiceFactory<IIsSterilisedItemProcessor, ICatIsSterilisedProcessor,
                    IDogIsSterilisedProcessor, CatIsSterilisedProcessor, DogIsSterilisedProcessor>
                .Register(serviceCollection);
            CatDogDiscerningServiceFactory<IIndicationItemProcessor, ICatIndicationProcessor,
                    IDogIndicationProcessor, CatIndicationProcessor, DogIndicationProcessor>
                .Register(serviceCollection);
            CatDogDiscerningServiceFactory<ICounterIndicationItemProcessor, ICatCounterIndicationProcessor,
                    IDogCounterIndicationProcessor, CatCounterIndicationProcessor, DogCounterIndicationProcessor>
                .Register(serviceCollection);

            serviceCollection.AddScoped<IDogSizeItemProcessor, DogSizeItemProcessor>();
            serviceCollection.AddScoped<IPillarIdItemProcessor, PillarIdItemProcessor>();
            serviceCollection.AddScoped<ILifestageItemProcessor, LifestageItemProcessor>();
            serviceCollection.AddScoped<IPackagingSizeItemProcessor, PackagingSizeItemProcessor>();
            serviceCollection.AddScoped<ISpecialNeedItemProcessor, SpecialNeedItemProcessor>();
            serviceCollection.AddScoped<IProductTypeItemProcessor, ProductTypeItemProcessor>();
            serviceCollection.AddScoped<IMeasurementUnitItemProcessor, MeasurementUnitItemProcessor>();

            serviceCollection.AddTransient<IBaseSearchManager<Lifestage>, BaseSearchManager<Lifestage>>();
            serviceCollection.AddTransient<IBaseSearchManager<Counterindication>, BaseSearchManager<Counterindication>>();
            serviceCollection.AddTransient<IBaseSearchManager<Size>, BaseSearchManager<Size>>();
            serviceCollection.AddTransient<IBaseSearchManager<GlobalProduct>, BaseSearchManager<GlobalProduct>>();
            serviceCollection.AddTransient<IBaseSearchManager<Indication>, BaseSearchManager<Indication>>();
            serviceCollection.AddTransient<IBaseSearchManager<IsSterilised>, BaseSearchManager<IsSterilised>>();
            serviceCollection.AddTransient<IBaseSearchManager<MeasurementUnit>, BaseSearchManager<MeasurementUnit>>();
            serviceCollection.AddTransient<IBaseSearchManager<PackagingSize>, BaseSearchManager<PackagingSize>>();
            serviceCollection.AddTransient<IBaseSearchManager<Pillar>, BaseSearchManager<Pillar>>();
            serviceCollection.AddTransient<IBaseSearchManager<ProductType>, BaseSearchManager<ProductType>>();
            serviceCollection.AddTransient<IBaseSearchManager<SpecialNeed>, BaseSearchManager<SpecialNeed>>();

        }
    }
}