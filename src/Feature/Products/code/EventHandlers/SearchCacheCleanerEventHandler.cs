﻿using System;
using System.Web;
using System.Collections;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Sitecore.Events;

namespace Delete.Feature.Products.EventHandlers
{
    public class SearchCacheCleanerEventHandler
    {
        private const string ProductsCacheKey = "allproducts_";
        private const string WebIndexKey = "Products.SearchIndex";
        private const string ProductsIndexKey = "Products.ProductsSearchIndex";
        public void OnIndexRebuild(object sender, EventArgs args)
        {
            try
            {
                var webIndex = Sitecore.Configuration.Settings.GetSetting(WebIndexKey);
                var productsIndex = Sitecore.Configuration.Settings.GetSetting(ProductsIndexKey);

                var eventArgs = args as SitecoreEventArgs;

                var indexName = eventArgs?.Parameters[0] as string;

                if (indexName == null || (!indexName.Equals(webIndex) && !indexName.Equals(productsIndex))) return;

                foreach (DictionaryEntry item in HttpRuntime.Cache)
                {
                    if (item.Key.ToString().Contains(ProductsCacheKey))
                    {
                        this.LogInfo($"Removing from cache key:{item.Key}");

                        HttpRuntime.Cache.Remove(item.Key as string);
                    }
                }
            }
            catch (Exception e)
            {
                this.LogError($"An error has occurred while trying to clear the cache. Error message: {e.GetAllMessages()}", e);
            }
        }
    }
}