﻿using System;

namespace Delete.Feature.Products.Models
{
    using System.Collections.Generic;

    public class ProductsFilterViewModel
    {
        private int page;


        private IDictionary<string, string> filtersDictionary;

        public string SearchQuery { get; set; }

        public Guid Filter { get; set; }

        public decimal FeedingPeriod { get; set; }

        public string Breeds { get; set; }

        public string ProductTypes { get; set; }

        public string Sizes { get; set; }

        public string SpecialNeeds { get; set; }

        public string CounterIndications { get; set; }

        public string Indications { get; set; }

        public string ProductRanges { get; set; }

        public int Page
        {
            get => this.page < 1 ? (this.page = 1) : this.page;
            set => this.page = value;
        }

        public IDictionary<string, string> FiltersDictionary
        {
            get 
            {
                if (this.filtersDictionary != null)
                {
                    return this.filtersDictionary;
                }

                return this.filtersDictionary = new Dictionary<string, string>
                                                    {
                                                        {nameof(this.SearchQuery), this.SearchQuery},
                                                        {nameof(this.Filter), this.Filter != Guid.Empty ? this.Filter.ToString("N") : string.Empty},
                                                        {nameof(this.FeedingPeriod), this.FeedingPeriod > 0 ? this.FeedingPeriod.ToString() : string.Empty},
                                                        {nameof(this.Breeds), this.Breeds},
                                                        {nameof(this.ProductTypes), this.ProductTypes},
                                                        {nameof(this.Sizes), this.Sizes},
                                                        {nameof(this.SpecialNeeds), this.SpecialNeeds},
                                                        {nameof(this.CounterIndications), this.CounterIndications},
                                                        {nameof(this.Indications), this.Indications},
                                                        {nameof(this.ProductRanges), this.ProductRanges},
                                                        {nameof(this.Page), this.Page > 1 ? this.Page.ToString() : string.Empty},
                                                    };
            }
        }
    }
}