﻿using System;
using System.Collections.Generic;

using Delete.Foundation.DataTemplates.Models.Folder;
using Delete.Foundation.DataTemplates.Models.Taxonomy;

namespace Delete.Feature.Products.Models
{
    public class SearchFilterViewModel
    {
        public Specie Specie { get; set; }

        public Guid PillarId { get; set; }

        public IEnumerable<FilterGroupFolder> FilterGroups { get; set; }

        public IDictionary<string, int> DefaultFacets { get; set; }

        public IDictionary<string, int> CurrentFacets { get; set; }

        public ProductsFilterViewModel Filters { get; set; }
    }
}