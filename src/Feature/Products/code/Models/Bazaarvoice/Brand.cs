﻿using Newtonsoft.Json;

namespace Delete.Feature.Products.Models.Bazaarvoice
{
    public partial class Brand
    {
        [JsonProperty("Id", NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        [JsonProperty("Name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }
    }
}