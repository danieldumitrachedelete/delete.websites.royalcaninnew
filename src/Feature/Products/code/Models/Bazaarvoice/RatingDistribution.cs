﻿using Newtonsoft.Json;

namespace Delete.Feature.Products.Models.Bazaarvoice
{
    public partial class RatingDistribution
    {
        [JsonProperty("RatingValue", NullValueHandling = NullValueHandling.Ignore)]
        public long RatingValue { get; set; }

        [JsonProperty("Count", NullValueHandling = NullValueHandling.Ignore)]
        public long Count { get; set; }
    }
}