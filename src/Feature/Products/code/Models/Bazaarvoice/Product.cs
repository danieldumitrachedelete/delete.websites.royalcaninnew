﻿using Newtonsoft.Json;

namespace Delete.Feature.Products.Models.Bazaarvoice
{
    public partial class Product
    {
        //[JsonProperty("Description", NullValueHandling = NullValueHandling.Ignore)]
        //public string Description { get; set; }

        //[JsonProperty("ModelNumbers", NullValueHandling = NullValueHandling.Ignore)]
        //public List<string> ModelNumbers { get; set; }

        //[JsonProperty("ManufacturerPartNumbers", NullValueHandling = NullValueHandling.Ignore)]
        //public List<string> ManufacturerPartNumbers { get; set; }

        //[JsonProperty("UPCs", NullValueHandling = NullValueHandling.Ignore)]
        //public List<string> UpCs { get; set; }

        //[JsonProperty("ImageUrl", NullValueHandling = NullValueHandling.Ignore)]
        //public string ImageUrl { get; set; }

        //[JsonProperty("Name", NullValueHandling = NullValueHandling.Ignore)]
        //public string Name { get; set; }

        [JsonProperty("Id", NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        //[JsonProperty("CategoryId", NullValueHandling = NullValueHandling.Ignore)]
        //public string CategoryId { get; set; }

        //[JsonProperty("BrandExternalId", NullValueHandling = NullValueHandling.Ignore)]
        //public string BrandExternalId { get; set; }

        //[JsonProperty("Brand", NullValueHandling = NullValueHandling.Ignore)]
        //public Brand Brand { get; set; }

        [JsonProperty("Active", NullValueHandling = NullValueHandling.Ignore)]
        public bool? Active { get; set; }

        //[JsonProperty("ProductPageUrl", NullValueHandling = NullValueHandling.Ignore)]
        //public string ProductPageUrl { get; set; }

        //[JsonProperty("Disabled", NullValueHandling = NullValueHandling.Ignore)]
        //public bool? Disabled { get; set; }

        //[JsonProperty("ISBNs", NullValueHandling = NullValueHandling.Ignore)]
        //public List<object> IsbNs { get; set; }

        //[JsonProperty("ReviewIds", NullValueHandling = NullValueHandling.Ignore)]
        //public List<object> ReviewIds { get; set; }

        //[JsonProperty("FamilyIds", NullValueHandling = NullValueHandling.Ignore)]
        //public List<object> FamilyIds { get; set; }

        //[JsonProperty("QuestionIds", NullValueHandling = NullValueHandling.Ignore)]
        //public List<object> QuestionIds { get; set; }

        //[JsonProperty("AttributesOrder", NullValueHandling = NullValueHandling.Ignore)]
        //public List<string> AttributesOrder { get; set; }

        //[JsonProperty("Attributes", NullValueHandling = NullValueHandling.Ignore)]
        //public Dictionary<string, object> Attributes { get; set; }

        //[JsonProperty("StoryIds", NullValueHandling = NullValueHandling.Ignore)]
        //public List<object> StoryIds { get; set; }

        //[JsonProperty("EANs", NullValueHandling = NullValueHandling.Ignore)]
        //public List<object> EaNs { get; set; }

        [JsonProperty("ReviewStatistics", NullValueHandling = NullValueHandling.Ignore)]
        public ReviewStatistics ReviewStatistics { get; set; }

        [JsonProperty("TotalReviewCount", NullValueHandling = NullValueHandling.Ignore)]
        public long TotalReviewCount { get; set; }
    }
}