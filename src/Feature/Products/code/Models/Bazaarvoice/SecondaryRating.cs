﻿using Newtonsoft.Json;

namespace Delete.Feature.Products.Models.Bazaarvoice
{
    public partial class SecondaryRating
    {
        [JsonProperty("Id", NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        [JsonProperty("AverageRating", NullValueHandling = NullValueHandling.Ignore)]
        public decimal AverageRating { get; set; }

        [JsonProperty("MaxLabel")]
        public string MaxLabel { get; set; }

        [JsonProperty("MinLabel")]
        public string MinLabel { get; set; }

        [JsonProperty("DisplayType", NullValueHandling = NullValueHandling.Ignore)]
        public string DisplayType { get; set; }

        [JsonProperty("ValueRange", NullValueHandling = NullValueHandling.Ignore)]
        public long ValueRange { get; set; }

        [JsonProperty("Label", NullValueHandling = NullValueHandling.Ignore)]
        public string Label { get; set; }

        [JsonProperty("Value", NullValueHandling = NullValueHandling.Ignore)]
        public long Value { get; set; }

        [JsonProperty("ValueLabel")]
        public string ValueLabel { get; set; }
    }
}