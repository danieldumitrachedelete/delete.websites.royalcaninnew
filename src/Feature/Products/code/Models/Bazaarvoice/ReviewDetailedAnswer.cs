﻿using Newtonsoft.Json;

namespace Delete.Feature.Products.Models.Bazaarvoice
{
    public partial class ReviewDetailedAnswer
    {
        [JsonProperty("Value", NullValueHandling = NullValueHandling.Ignore)]
        public string Value { get; set; }

        [JsonProperty("Id", NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        [JsonProperty("ValueLabel", NullValueHandling = NullValueHandling.Ignore)]
        public string ValueLabel { get; set; }

        [JsonProperty("DimensionLabel", NullValueHandling = NullValueHandling.Ignore)]
        public string DimensionLabel { get; set; }
    }
}