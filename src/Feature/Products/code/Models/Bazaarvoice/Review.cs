﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Delete.Feature.Products.Models.Bazaarvoice
{
    public partial class Review
    {
        [JsonProperty("Id", NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        //[JsonProperty("CID", NullValueHandling = NullValueHandling.Ignore)]
        //public string Cid { get; set; }

        //[JsonProperty("SourceClient", NullValueHandling = NullValueHandling.Ignore)]
        //public string SourceClient { get; set; }

        [JsonProperty("LastModeratedTime", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? LastModeratedTime { get; set; }

        [JsonProperty("LastModificationTime", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? LastModificationTime { get; set; }

        [JsonProperty("ProductId", NullValueHandling = NullValueHandling.Ignore)]
        public string ProductId { get; set; }

        //[JsonProperty("CampaignId")]
        //public string CampaignId { get; set; }

        [JsonProperty("ContextDataValuesOrder", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> ReviewDetailedAnswersOrder { get; set; }

        [JsonProperty("UserLocation")]
        public string UserLocation { get; set; }

        //[JsonProperty("AuthorId", NullValueHandling = NullValueHandling.Ignore)]
        //public string AuthorId { get; set; }

        [JsonProperty("ContentLocale", NullValueHandling = NullValueHandling.Ignore)]
        public string ContentLocale { get; set; }

        //[JsonProperty("IsFeatured", NullValueHandling = NullValueHandling.Ignore)]
        //public bool? IsFeatured { get; set; }

        //[JsonProperty("TotalClientResponseCount", NullValueHandling = NullValueHandling.Ignore)]
        //public long TotalClientResponseCount { get; set; }

        //[JsonProperty("TotalCommentCount", NullValueHandling = NullValueHandling.Ignore)]
        //public long TotalCommentCount { get; set; }

        [JsonProperty("Rating", NullValueHandling = NullValueHandling.Ignore)]
        public long Rating { get; set; }

        [JsonProperty("SecondaryRatingsOrder", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> SecondaryRatingsOrder { get; set; }

        [JsonProperty("IsRatingsOnly", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsRatingsOnly { get; set; }

        [JsonProperty("IsRecommended")]
        public bool? IsRecommended { get; set; }

        [JsonProperty("TotalFeedbackCount", NullValueHandling = NullValueHandling.Ignore)]
        public long TotalFeedbackCount { get; set; }

        [JsonProperty("TotalNegativeFeedbackCount", NullValueHandling = NullValueHandling.Ignore)]
        public long TotalNegativeFeedbackCount { get; set; }

        [JsonProperty("TotalPositiveFeedbackCount", NullValueHandling = NullValueHandling.Ignore)]
        public long TotalPositiveFeedbackCount { get; set; }

        //[JsonProperty("ModerationStatus", NullValueHandling = NullValueHandling.Ignore)]
        //public string ModerationStatus { get; set; }

        //[JsonProperty("SubmissionId", NullValueHandling = NullValueHandling.Ignore)]
        //public string SubmissionId { get; set; }

        [JsonProperty("SubmissionTime", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? SubmissionTime { get; set; }

        [JsonProperty("ReviewText", NullValueHandling = NullValueHandling.Ignore)]
        public string ReviewText { get; set; }

        [JsonProperty("Title", NullValueHandling = NullValueHandling.Ignore)]
        public string Title { get; set; }

        [JsonProperty("UserNickname", NullValueHandling = NullValueHandling.Ignore)]
        public string UserNickname { get; set; }

        [JsonProperty("ContextDataValues", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, ReviewDetailedAnswer> ReviewDetailedAnswers { get; set; }

        [JsonProperty("SecondaryRatings", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, SecondaryRating> SecondaryRatings { get; set; }

        //[JsonProperty("AdditionalFields", NullValueHandling = NullValueHandling.Ignore)]
        //public Dictionary<string, ReviewAdditionalDetailedAnswer> ReviewAdditionalDetailedAnswers { get; set; }

        [JsonProperty("Helpfulness")]
        public object Helpfulness { get; set; }

        [JsonProperty("Photos", NullValueHandling = NullValueHandling.Ignore)]
        public List<Photo> Photos { get; set; }

        //[JsonProperty("ProductRecommendationIds", NullValueHandling = NullValueHandling.Ignore)]
        //public List<object> ProductRecommendationIds { get; set; }

        //[JsonProperty("CommentIds", NullValueHandling = NullValueHandling.Ignore)]
        //public List<object> CommentIds { get; set; }

        //[JsonProperty("BadgesOrder", NullValueHandling = NullValueHandling.Ignore)]
        //public List<string> BadgesOrder { get; set; }

        //[JsonProperty("TagDimensions", NullValueHandling = NullValueHandling.Ignore)]
        //public Dictionary<string, object> TagDimensions { get; set; }

        //[JsonProperty("Pros")]
        //public object Pros { get; set; }

        //[JsonProperty("Badges", NullValueHandling = NullValueHandling.Ignore)]
        //public Dictionary<string, Badge> Badges { get; set; }

        //[JsonProperty("Cons")]
        //public object Cons { get; set; }

        //[JsonProperty("TagDimensionsOrder", NullValueHandling = NullValueHandling.Ignore)]
        //public List<string> TagDimensionsOrder { get; set; }

        [JsonProperty("ClientResponses", NullValueHandling = NullValueHandling.Ignore)]
        public List<ClientResponse> ClientResponses { get; set; }

        [JsonProperty("Videos", NullValueHandling = NullValueHandling.Ignore)]
        public List<Video> Videos { get; set; }

        //[JsonProperty("AdditionalFieldsOrder", NullValueHandling = NullValueHandling.Ignore)]
        //public List<string> ReviewAdditionalDetailedAnswersOrder { get; set; }

        [JsonProperty("RatingRange", NullValueHandling = NullValueHandling.Ignore)]
        public long RatingRange { get; set; }

        [JsonProperty("IsSyndicated", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsSyndicated { get; set; }
    }
}