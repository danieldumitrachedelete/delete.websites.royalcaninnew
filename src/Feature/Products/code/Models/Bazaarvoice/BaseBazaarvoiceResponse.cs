﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Delete.Feature.Products.Models.Bazaarvoice
{
    public partial class BazaarvoiceResponse<T>
    {
        [JsonProperty("Limit", NullValueHandling = NullValueHandling.Ignore)]
        public long Limit { get; set; }

        [JsonProperty("Offset", NullValueHandling = NullValueHandling.Ignore)]
        public long Offset { get; set; }

        [JsonProperty("TotalResults", NullValueHandling = NullValueHandling.Ignore)]
        public long TotalResults { get; set; }

        [JsonProperty("Locale", NullValueHandling = NullValueHandling.Ignore)]
        public string Locale { get; set; }

        [JsonProperty("HasErrors", NullValueHandling = NullValueHandling.Ignore)]
        public bool? HasErrors { get; set; }

        [JsonProperty("Errors", NullValueHandling = NullValueHandling.Ignore)]
        public List<Error> Errors { get; set; }

        //[JsonProperty("Includes", NullValueHandling = NullValueHandling.Ignore)]
        //public Includes Includes { get; set; }

        [JsonProperty("Results", NullValueHandling = NullValueHandling.Ignore)]
        public List<T> Results { get; set; }

        public void Merge(BazaarvoiceResponse<T> anotherResponse)
        {
            Limit = anotherResponse.Limit;
            Offset = Limit + anotherResponse.Offset;
            TotalResults = anotherResponse.TotalResults;
            Locale = anotherResponse.Locale;
            HasErrors = (HasErrors ?? false) || (anotherResponse.HasErrors ?? false);
            Errors.AddRange(anotherResponse.Errors);
            Results.AddRange(anotherResponse.Results);
        }
    }
}