﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Delete.Feature.Products.Models.Bazaarvoice
{
    public partial class Includes
    {
        [JsonProperty("Products", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, Product> Products { get; set; }

        [JsonProperty("ProductsOrder", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> ProductsOrder { get; set; }
    }
}