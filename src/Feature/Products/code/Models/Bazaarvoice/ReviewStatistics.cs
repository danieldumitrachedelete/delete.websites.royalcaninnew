﻿using Newtonsoft.Json;

namespace Delete.Feature.Products.Models.Bazaarvoice
{
    public partial class ReviewStatistics
    {
        //[JsonProperty("SecondaryRatingsAveragesOrder", NullValueHandling = NullValueHandling.Ignore)]
        //public List<string> SecondaryRatingsAveragesOrder { get; set; }

        //[JsonProperty("SecondaryRatingsAverages", NullValueHandling = NullValueHandling.Ignore)]
        //public Dictionary<string, SecondaryRating> SecondaryRatingsAverages { get; set; }

        //[JsonProperty("FeaturedReviewCount", NullValueHandling = NullValueHandling.Ignore)]
        //public long FeaturedReviewCount { get; set; }

        //[JsonProperty("NotRecommendedCount", NullValueHandling = NullValueHandling.Ignore)]
        //public long NotRecommendedCount { get; set; }

        //[JsonProperty("RecommendedCount", NullValueHandling = NullValueHandling.Ignore)]
        //public long RecommendedCount { get; set; }

        //[JsonProperty("ContextDataDistributionOrder", NullValueHandling = NullValueHandling.Ignore)]
        //public List<string> ContextDataDistributionOrder { get; set; }

        //[JsonProperty("ContextDataDistribution", NullValueHandling = NullValueHandling.Ignore)]
        //public Dictionary<string, ContextDataDistribution> ContextDataDistribution { get; set; }

        //[JsonProperty("NotHelpfulVoteCount", NullValueHandling = NullValueHandling.Ignore)]
        //public long NotHelpfulVoteCount { get; set; }

        [JsonProperty("TotalReviewCount", NullValueHandling = NullValueHandling.Ignore)]
        public long TotalReviewCount { get; set; }

        //[JsonProperty("HelpfulVoteCount", NullValueHandling = NullValueHandling.Ignore)]
        //public long HelpfulVoteCount { get; set; }

        [JsonProperty("AverageOverallRating", NullValueHandling = NullValueHandling.Ignore)]
        public decimal AverageOverallRating { get; set; }

        //[JsonProperty("RatingsOnlyReviewCount", NullValueHandling = NullValueHandling.Ignore)]
        //public long RatingsOnlyReviewCount { get; set; }

        //[JsonProperty("FirstSubmissionTime", NullValueHandling = NullValueHandling.Ignore)]
        //public DateTimeOffset? FirstSubmissionTime { get; set; }

        //[JsonProperty("LastSubmissionTime", NullValueHandling = NullValueHandling.Ignore)]
        //public DateTimeOffset? LastSubmissionTime { get; set; }

        //[JsonProperty("RatingDistribution", NullValueHandling = NullValueHandling.Ignore)]
        //public List<RatingDistribution> OverallRatingDistribution { get; set; }

        [JsonProperty("OverallRatingRange", NullValueHandling = NullValueHandling.Ignore)]
        public long OverallRatingRange { get; set; }

        //[JsonProperty("TagDistribution", NullValueHandling = NullValueHandling.Ignore)]
        //public Dictionary<string, object> TagDistribution { get; set; }

        //[JsonProperty("TagDistributionOrder", NullValueHandling = NullValueHandling.Ignore)]
        //public List<string> TagDistributionOrder { get; set; }
    }
}