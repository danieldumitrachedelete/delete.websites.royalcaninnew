﻿using System.Collections.Generic;
using Delete.Feature.Products.Models.Bazaarvoice;

namespace Delete.Feature.Products.Models
{
    public class ProductReviewsViewModel
    {
        public string MainItemId { get; set; }
        public ReviewStatistics ReviewStatistics { get; set; }
        public IEnumerable<Review> Reviews { get; set; }
        public IEnumerable<ProductReviewsOrderByRule> OrderByRules { get; set; }
        public ProductReviewsPagingViewModel Pagination { get; set; }
        public string JavaScriptUrl { get; set; }
    }
}