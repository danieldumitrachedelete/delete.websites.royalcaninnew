﻿using System.Runtime.Serialization;

namespace Delete.Feature.Products.Models
{
    [DataContract]
    public class ProductGtmObject
    {
        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "id")]
        public string Id { get; set; }

        [DataMember(Name = "price")]
        public string Price { get; set; }

        [DataMember(Name = "brand")]
        public string Brand { get; set; }

        [DataMember(Name = "category")]
        public string Category { get; set; }

        [DataMember(Name = "variant")]
        public string Variant { get; set; }

        [DataMember(Name = "list")]
        public string List { get; set; }

        [DataMember(Name = "position")]
        public int? Position { get; set; }
    }
}