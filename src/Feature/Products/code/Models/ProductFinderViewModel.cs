﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Delete.Feature.Products.Models
{
    public class ProductFinderViewModel
    {
        public string MainItemId { get; set; }

        public string ProductName { get; set; }

        public string Image { get; set; }

        public string Url { get; set; }

        public string WhereToBuyUrl { get; set; }
    }
}