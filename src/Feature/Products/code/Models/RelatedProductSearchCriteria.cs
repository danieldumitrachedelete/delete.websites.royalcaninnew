using System;
using System.Collections.Generic;
using System.Linq;

namespace Delete.Feature.Products.Models
{
    public class RelatedProductSearchCriteria
    {
        public IList<Guid> ProductsToIgnore { get; set; }
        public Guid Species { get; set; }
        public List<Guid> Lifestages { get; set; }
        public Guid Breed { get; set; }
        public Guid ProductType { get; set; }
        public Guid Pillars { get; set; }
        public Guid Category { get; set; }

        public RelatedProductSearchCriteria()
        {
            ProductsToIgnore = new List<Guid>();
        }

        public RelatedProductSearchCriteria(LocalProduct product)
        {
            ProductsToIgnore = new List<Guid> {product.Id};
            Species = product.GlobalProduct.Specie.Id;
            Lifestages = product.ExtendedLifestages.Select(x => x.Id).ToList();
            Breed = product.ExtendedBreed;
            ProductType = product.ExtendedProductType;
            Pillars = product.ExtendedPillarIDCode;
            Category = product.Parent.Id;
        }

        public List<Guid> Criterias
        {
            get { return new[] {Species, Breed, ProductType, Pillars}.Union(Lifestages).Where(x => x != Guid.Empty).ToList(); }
        }
    }
}