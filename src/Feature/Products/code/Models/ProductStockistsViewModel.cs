using System.Collections.Generic;
using Delete.Foundation.DataTemplates.Models.Interfaces;
using Glass.Mapper.Sc.Fields;

namespace Delete.Feature.Products.Models
{
    public class ProductStockistsViewModel
    {
        public IList<_Stockist> Stockists { get; set; }
        public Link WhereToBuyLink { get; set; }
    }
}