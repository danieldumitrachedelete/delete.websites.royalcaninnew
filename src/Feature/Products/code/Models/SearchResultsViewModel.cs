﻿using Delete.Foundation.DeleteFoundationCore.Models;
using Delete.Foundation.RoyalCaninCore.Models;

namespace Delete.Feature.Products.Models
{
    using System;

    public class SearchResultsViewModel : PaginatedResults
    {
        public Guid Filter { get; set; }

        public FacetedResults<LocalProduct> SearchResults { get; set; }
    }
}