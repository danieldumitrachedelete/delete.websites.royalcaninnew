﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Delete.Feature.Products.Interfaces;
using Delete.Foundation.DataTemplates.Models.Taxonomy;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Models;
using Delete.Foundation.DeleteFoundationCore.SchemaOrg;
using Delete.Foundation.DeleteFoundationCore.SchemaOrg.Extensions;
using Delete.Foundation.DeleteFoundationCore.SchemaOrg.Interfaces;
using Delete.Foundation.MultiSite.Models;
using Glass.Mapper.Sc;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Sitecore.ContentSearch;

namespace Delete.Feature.Products.Models
{
    public partial class LocalProduct
    {
        private IList<ExtendedImage> _images;
        private IEnumerable<Guid> _relatedEntities;

        public IList<ExtendedImage> Images
        {
            get
            {
                if (_images != null)
                {
                    return _images;
                }

                var images = new List<ExtendedImage>
                {
                    ExtendedImage1_BAG,
                    ExtendedImage4_KIBBLE,
                    ExtendedImage5_EMBLEMATIC,
                    ExtendedImage6,
                    ExtendedImage7,
                    ExtendedImage8
                };

                return _images = images.Where(x => x != null).ToList();
            }
        }

        public IEnumerable<Guid> RelatedEntities
        {
            get
            {
                if (_relatedEntities != null) return _relatedEntities;

                var result = new List<Guid>();
                result.Add(ExtendedProductType);
                result.AddRange(ExtendedIndications.Select(x => x.Id));
                result.AddRange(ExtendedCounterIndications.Select(x => x.Id));
                if (ExtendedIsSterilised != null) result.Add(ExtendedIsSterilised.Id);
                result.AddRange(ExtendedSpecialNeeds.Select(x => x.Id));

                result.Add(ExtendedBreed);
                result.AddRange(ExtendedAnimalSize.Select(x => x.Id));
                if (ExtendedLifestages != null && ExtendedLifestages.Any())
                {
                    result.AddRange(ExtendedLifestages.Select(x => x.Id));
                }

                if (GlobalProduct?.Specie != null) result.Add(GlobalProduct.Specie.Id);

                return (_relatedEntities = result);
            }
        }

        [IndexField("productPillar")]
        public virtual string SearchablePillar { get; set; }
        
        [IndexField("productAnimalSizes")]
        public virtual IEnumerable<Guid> SearchableAnimalSizes { get; set; }

        [IndexField("productBreed")]
        public virtual Guid SearchableBreed { get; set; }

        [IndexField("productLifestages")]
        public virtual IEnumerable<Guid> SearchableLifestages { get; set; }

        [IndexField("productSpecie")]
        public virtual string SearchableSpecie { get; set; }

        [IndexField("productType")]
        public virtual Guid SearchableProductType { get; set; }

        [IndexField("productSpecialNeeds")]
        public virtual IEnumerable<Guid> SearchableSpecialNeeds { get; set; }

        [IndexField("productIndications")]
        public virtual IEnumerable<Guid> SearchableIndications { get; set; }

        [IndexField("productCounterIndications")]
        public virtual IEnumerable<Guid> SearchableCounterIndications { get; set; }

        [IndexField("productSpecialNeedsText")]
        public virtual string SearchableSpecialNeedsText { get; set; }

        [IndexField("productSterilised")]
        public virtual string SearchableSterilisedText { get; set; }

        [IndexField("productName")]
        public virtual string SearchableProductName { get; set; }

        [IndexField("productFeedingPeriodFrom")]
        public virtual int SearchableProductFeedingPeriodFrom { get; set; }

        [IndexField("productFeedingPeriodTo")]
        public virtual int SearchableProductFeedingPeriodTo { get; set; }

        [IndexField("productRanges")]
        public virtual IEnumerable<Guid> SearchableProductRanges { get; set; }

        [SitecoreField(FieldName = LocalProductConstants.GlobalProductFieldName, FieldId = LocalProductConstants.GlobalProductFieldId)]
        [IndexField(LocalProductConstants.GlobalProductFieldName)]
        public virtual Guid GlobalProductId { get; set; }

        public IEnumerable<Size> ExtendedAnimalSize => AnimalSizeOverrides ?? GlobalProduct?.AnimalSizes;

        public string ExtendedBenefitText => BenefitTextOverride.OrDefault(BenefitText);

        public Guid ExtendedBreed => BreedOverride != Guid.Empty ? BreedOverride : GlobalProduct?.Breed ?? Guid.Empty;

        public IEnumerable<Counterindication> ExtendedCounterIndications => CounterIndicationsOverrides != null && CounterIndicationsOverrides.Any() ? CounterIndicationsOverrides : CounterIndications;

        public Int32 ExtendedFeedingPeriodFrom => FeedingPeriodFromOverride > 0 ? FeedingPeriodFromOverride : FeedingPeriodFrom;

        public Int32 ExtendedFeedingPeriodTo => FeedingPeriodToOverride > 0 ? FeedingPeriodToOverride : FeedingPeriodTo;

        public ExtendedImage ExtendedImage1_BAG => Image1_BAGOverride ?? Image1_BAG;

        public ExtendedImage ExtendedImage4_KIBBLE => Image4_KIBBLEOverride ?? Image4_KIBBLE;

        public ExtendedImage ExtendedImage5_EMBLEMATIC => Image5_EMBLEMATICOverride ?? Image5_EMBLEMATIC;

        public ExtendedImage ExtendedImage6 => Image6Override ?? Image6;

        public ExtendedImage ExtendedImage7 => Image7Override ?? Image7;

        public ExtendedImage ExtendedImage8 => Image8Override ?? Image8;

        public ExtendedImage ExtendedFeedingGuideImage => FeedingGuideImageOverride ?? FeedingGuideImage;

        public IEnumerable<Indication> ExtendedIndications => IndicationsOverrides != null && IndicationsOverrides.Any() ? IndicationsOverrides : Indications;

        public string ExtendedIngredients => IngredientsOverride.OrDefault(Ingredients);

        public string ExtendedGuaranteedAnalysis => GuaranteedAnalysisOverride.OrDefault(GuaranteedAnalysis);

        public IEnumerable<Lifestage> ExtendedLifestages => LifestagesOverrides != null && LifestagesOverrides.Any() ? LifestagesOverrides : (GlobalProduct?.Lifestages ?? new List<Lifestage>());

        public IEnumerable<PackagingSize> ExtendedPackagingSizes => PackagingSizeOverrides != null && PackagingSizeOverrides.Any() ? PackagingSizeOverrides : (PackagingSizes ?? new List<PackagingSize>());

        public string ExtendedMainBenefit => MainBenefitOverride.OrDefault(MainBenefit);

        public string ExtendedMainDescription => MainDescriptionOverride.OrDefault(MainDescription);

        public string ExtendedMetaData => MetaDataOverride.OrDefault(MetaData);

        public string ExtendedMetaKeywords => this.MetaKeywordsOverride.OrDefault(this.MetaDataKeywords);

        public Guid ExtendedPillarIDCode => PillarIDCodeOverride != Guid.Empty ? PillarIDCodeOverride : (GlobalProduct?.PillarIDCode ?? Guid.Empty);

        public string ExtendedProductDescription => ProductDescriptionOverride.OrDefault(ProductDescription);

        public string ExtendedProductName => ProductNameOverride.OrDefault(ProductName);

        public Guid ExtendedProductType => ProductTypeOverride != Guid.Empty ? ProductTypeOverride : ProductType?.Id ?? Guid.Empty;

	    public Guid ExtendedTechnology => TechnologyOverride != Guid.Empty ? TechnologyOverride : Technology?.Id ?? Guid.Empty;

        public string ExtendedSecondaryBenefit => SecondaryBenefitOverride.OrDefault(SecondaryBenefit);

        public IEnumerable<SpecialNeed> ExtendedSpecialNeeds => SpecialNeedsOverrides != null && SpecialNeedsOverrides.Any() ? SpecialNeedsOverrides : SpecialNeeds;

        public IsSterilised ExtendedIsSterilised => IsSterilisedOverride ?? IsSterilised;

        public string ExtendedText => TextOverride.OrDefault(Text);

        public string ExtendedTextSpare1 => TextSpare1Override.OrDefault(TextSpare1);

        public string ExtendedTextSpare2 => TextSpare2Override.OrDefault(TextSpare2);

	    public string ExtendedBenefitText4 => BenefitText4Override.OrDefault(BenefitText4);

	    public string ExtendedBenefitText5 => BenefitText5Override.OrDefault(BenefitText5);

	    public string ExtendedBenefitText6 => BenefitText6Override.OrDefault(BenefitText6);

	    public string ExtendedBenefitTitle1 => BenefitTitle1Override.OrDefault(BenefitTitle1);

	    public string ExtendedBenefitTitle2 => BenefitTitle2Override.OrDefault(BenefitTitle2);

	    public string ExtendedBenefitTitle3 => BenefitTitle3Override.OrDefault(BenefitTitle3);

	    public string ExtendedBenefitTitle4 => BenefitTitle4Override.OrDefault(BenefitTitle4);

	    public string ExtendedBenefitTitle5 => BenefitTitle5Override.OrDefault(BenefitTitle5);

	    public string ExtendedBenefitTitle6 => BenefitTitle6Override.OrDefault(BenefitTitle6);

	    public string ExtendedFeedingGuide => FeedingGuideOverride.OrDefault(FeedingGuide);

	    public string ExtendedAdditives => AdditivesOverride.OrDefault(Additives);

	    public string ExtendedAConstituents => AConstituentsOverride.OrDefault(AConstituents);

		public string MedicalStatement
        {
            get
            {
                if(GlobalProduct?.MedicalStatements?.Any() == false)
                    return null;

                var statements = 
                    GlobalProduct
                        ?.MedicalStatements
                        ?.Select(x => x.Text?.Trim()?.EnsureEndsWith("."))
                        .Where(x => x.NotEmpty())
                        .ToArray() ?? new string[0];

                return statements.Any() ? string.Join(" ", statements) : null;
            }
        }

        #region doubling fields for RichText fields
         /* by default glassmapper processes Rich Text fields so that they appear formatted to the client
          * which makes them effectively read-only. To work around that, we need to define a doubling
          * field with setting 'SitecoreFieldSettings.RichTextRaw' and use it instead
          */

        /// <summary>
        /// The Product Description field.
        /// <para>Field Type: Rich Text</para>		
        /// <para>Field ID: {2ffe04d7-4c40-4298-8cb9-e8ca74e8e180}</para>
        /// </summary>
        [SitecoreField(FieldId = LocalProductConstants.ProductDescriptionFieldId,
            FieldName = LocalProductConstants.ProductDescriptionFieldName, Setting = SitecoreFieldSettings.RichTextRaw)]
        /*[IndexField(LocalProductConstants.ProductDescriptionFieldName)]*/
        public virtual string ProductDescriptionRaw { get; set; }

        /// <summary>
        /// The Product Description Override field.
        /// <para>Field Type: Rich Text</para>		
        /// <para>Field ID: {492d7ab6-4764-427a-b02b-57154088cbe5}</para>
        /// </summary>
        [SitecoreField(FieldId = LocalProductConstants.ProductDescriptionOverrideFieldId,
            FieldName = LocalProductConstants.ProductDescriptionOverrideFieldName,
            Setting = SitecoreFieldSettings.RichTextRaw)]
        /*[IndexField(LocalProductConstants.ProductDescriptionOverrideFieldName)]*/
        public virtual string ProductDescriptionOverrideRaw { get; set; }

        /// <summary>
        /// The Ingredients field.
        /// <para>Field Type: Rich Text</para>		
        /// <para>Field ID: {39841278-6245-401a-915b-4b3095a89bfd}</para>
        /// </summary>
        [SitecoreField(FieldId = LocalProductConstants.IngredientsFieldId,
            FieldName = LocalProductConstants.IngredientsFieldName, Setting = SitecoreFieldSettings.RichTextRaw)]
        /*[IndexField(LocalProductConstants.IngredientsFieldName)]*/
        public virtual string IngredientsRaw { get; set; }

        /// <summary>
        /// The Ingredients Override field.
        /// <para>Field Type: Rich Text</para>		
        /// <para>Field ID: {1a319d25-07e0-43c9-8f28-4ba289c2c370}</para>
        /// </summary>
        [SitecoreField(FieldId = LocalProductConstants.IngredientsOverrideFieldId,
            FieldName = LocalProductConstants.IngredientsOverrideFieldName,
            Setting = SitecoreFieldSettings.RichTextRaw)]
        /*[IndexField(LocalProductConstants.IngredientsOverrideFieldName)]*/
        public virtual string IngredientsOverrideRaw { get; set; }

        /// <summary>
        /// The Guaranteed Analysis field.
        /// <para>Field Type: Rich Text</para>		
        /// <para>Field ID: {757692a2-a7e1-4d6c-b8ac-7d226e7e3649}</para>
        /// </summary>
        [SitecoreField(FieldId = LocalProductConstants.GuaranteedAnalysisFieldId, 
            FieldName = LocalProductConstants.GuaranteedAnalysisFieldName,
            Setting = SitecoreFieldSettings.RichTextRaw)]
        /*[IndexField(LocalProductConstants.GuaranteedAnalysisFieldName)]*/
        public virtual string GuaranteedAnalysisRaw { get; set; }

        /// <summary>
        /// The Guaranteed Analysis Override field.
        /// <para>Field Type: Rich Text</para>		
        /// <para>Field ID: {d6de395f-cbce-4289-b8e2-8b380b4a9101}</para>
        /// </summary>
        [SitecoreField(FieldId = LocalProductConstants.GuaranteedAnalysisOverrideFieldId,
            FieldName = LocalProductConstants.GuaranteedAnalysisOverrideFieldName,
            Setting = SitecoreFieldSettings.RichTextRaw)]
        /*[IndexField(LocalProductConstants.GuaranteedAnalysisOverrideFieldName)]*/
        public virtual string GuaranteedAnalysisOverrideRaw { get; set; }

		/// <summary>
		/// The A Constituents field.
		/// <para>Field Type: Rich Text</para>		
		/// <para>Field ID: {1304f747-d020-4de7-a8e7-d023443b427b}</para>
		/// </summary>
		[SitecoreField(FieldId = LocalProductConstants.AConstituentsFieldId, FieldName = LocalProductConstants.AConstituentsFieldName, Setting = SitecoreFieldSettings.RichTextRaw)]
		/*[IndexField(LocalProductConstants.AConstituentsFieldName)]*/
		public virtual string AConstituentsRaw { get; set; }

		/// <summary>
		/// The A Constituents Override field.
		/// <para>Field Type: Rich Text</para>		
		/// <para>Field ID: {c781d464-ed31-4f1c-92fe-5f0d5ff5c87f}</para>
		/// </summary>
		[SitecoreField(FieldId = LocalProductConstants.AConstituentsOverrideFieldId, FieldName = LocalProductConstants.AConstituentsOverrideFieldName, Setting = SitecoreFieldSettings.RichTextRaw)]
		/*[IndexField(LocalProductConstants.AConstituentsOverrideFieldName)]*/
		public virtual string AConstituentsOverrideRaw { get; set; }

		/// <summary>
		/// The Additives field.
		/// <para>Field Type: Rich Text</para>		
		/// <para>Field ID: {78cb46b9-b43d-4339-b5b7-14f0ed7d83fa}</para>
		/// </summary>
		[SitecoreField(FieldId = LocalProductConstants.AdditivesFieldId, FieldName = LocalProductConstants.AdditivesFieldName, Setting = SitecoreFieldSettings.RichTextRaw)]
		/*[IndexField(LocalProductConstants.AdditivesFieldName)]*/
		public virtual string AdditivesRaw { get; set; }

		/// <summary>
		/// The Additives Override field.
		/// <para>Field Type: Rich Text</para>		
		/// <para>Field ID: {2be60224-9f5c-4485-8ac1-e47560886676}</para>
		/// </summary>
		[SitecoreField(FieldId = LocalProductConstants.AdditivesOverrideFieldId, FieldName = LocalProductConstants.AdditivesOverrideFieldName, Setting = SitecoreFieldSettings.RichTextRaw)]
		/*[IndexField(LocalProductConstants.AdditivesOverrideFieldName)]*/
		public virtual string AdditivesOverrideRaw { get; set; }

		/// <summary>
		/// The Feeding Guide field.
		/// <para>Field Type: Rich Text</para>		
		/// <para>Field ID: {24f4ff86-06b3-4121-8920-52b33c11c4a8}</para>
		/// </summary>
		[SitecoreField(FieldId = LocalProductConstants.FeedingGuideFieldId, FieldName = LocalProductConstants.FeedingGuideFieldName, Setting = SitecoreFieldSettings.RichTextRaw)]
		/*[IndexField(LocalProductConstants.FeedingGuideFieldName)]*/
		public virtual string FeedingGuideRaw { get; set; }

		/// <summary>
		/// The Feeding Guide Override field.
		/// <para>Field Type: Rich Text</para>		
		/// <para>Field ID: {a452e545-754e-4562-907c-ef7e3e22237d}</para>
		/// </summary>
		[SitecoreField(FieldId = LocalProductConstants.FeedingGuideOverrideFieldId, FieldName = LocalProductConstants.FeedingGuideOverrideFieldName, Setting = SitecoreFieldSettings.RichTextRaw)]
		/*[IndexField(LocalProductConstants.FeedingGuideOverrideFieldName)]*/
		public virtual string FeedingGuideOverrideRaw { get; set; }
		#endregion

		public Thing GetMicrodata(IAuthorResolver authorResolver, IBrandResolver brandResolver, IProductReviewResolver reviewResolver, bool includeReviews = false)
        {
            var product = new Product
            {
                Name = ExtendedProductName ?? String.Empty,
                Image = ExtendedImage1_BAG.GetMicrodata(authorResolver),
                Description = ExtendedProductDescription ?? String.Empty,
                Brand = brandResolver.GetBrand(),
                Sku = SKU,
                ProductID = $"{GlobalProduct?.MainItemID}"
            };

            var rating = reviewResolver.GetAggregateRating(GlobalProduct?.MainItemID);
            if (rating != null) product.AggregateRating = rating;

            if (rating != null && includeReviews)
            {
                var reviews = reviewResolver.GetReviews(GlobalProduct?.MainItemID);
                if (reviews.Any()) product.Review = reviews;
            }

            return product;
        }

        public ProductGtmObject GetGTMData(string list = null, int position = -1)
        {
            var productRange = GlobalProduct?.ProductRanges?.Count() > 0 
                ? string.Join("/", GlobalProduct.ProductRanges.Select(x => x.Text)) 
                : "Not Specified";

            return new ProductGtmObject
            {
                Name = $"{ExtendedProductName}",
                Id = $"{GlobalProduct?.MainItemID}",
                Price = null,
                Brand = "Royal Canin",
                Category = $"{GlobalProduct?.Specie?.Name}/{productRange}/{ProductType?.Text}",
                Variant = null,
                List = list,
                Position = position >= 0 ? position : (int?)null,
            };
        }

        public ProductDescriptionSplitTextObject GetProductDescriptionSplitTextObject()
        {
            const int numberOfSentences = 2;

           var sentences = Regex.Split(ExtendedProductDescription, @"(?<=['""A-Za-z0-9][\.\!\?])\s+(?=[A-Z])");

            ProductDescriptionSplitTextObject result = null;
            
            if (sentences.Length >= numberOfSentences)
            {
                result = new ProductDescriptionSplitTextObject()
                {
                    ReadLessText = string.Join("",sentences.Take(numberOfSentences)),
                    ReadMoreText = string.Join("", sentences.Skip(numberOfSentences))
                };
            }
            else
            {
                result = new ProductDescriptionSplitTextObject()
                {
                    ReadLessText = ExtendedProductDescription
                };
            }

            return result;
        }

		public bool HasBenefitsText => !string.IsNullOrEmpty(this.ExtendedMainBenefit) ||
		                           !string.IsNullOrEmpty(this.ExtendedBenefitTitle1) ||
		                           !string.IsNullOrEmpty(this.ExtendedSecondaryBenefit) ||
		                           !string.IsNullOrEmpty(this.ExtendedBenefitTitle2) ||
		                           !string.IsNullOrEmpty(this.ExtendedBenefitText) ||
		                           !string.IsNullOrEmpty(this.ExtendedBenefitTitle3) ||
		                           !string.IsNullOrEmpty(this.ExtendedBenefitText4) ||
		                           !string.IsNullOrEmpty(this.ExtendedBenefitTitle4) ||
		                           !string.IsNullOrEmpty(this.ExtendedBenefitText5) ||
		                           !string.IsNullOrEmpty(this.ExtendedBenefitTitle5) ||
		                           !string.IsNullOrEmpty(this.ExtendedBenefitText6) ||
		                           !string.IsNullOrEmpty(this.ExtendedBenefitTitle6);

	    public bool HasDescriptionText => !string.IsNullOrEmpty(this.ExtendedProductDescription);
    }
}
