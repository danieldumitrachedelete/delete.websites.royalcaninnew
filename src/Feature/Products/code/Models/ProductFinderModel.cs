﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Delete.Feature.Products.Models
{
    public class ProductFinderModel
    {
        public Guid Species { get; set; }

        public ProductFinderTypedModel IDs { get; set; }
    }

    public class ProductFinderTypedModel
    {
        public List<string> Dry { get; set; }

        public List<string> Wet { get; set; }
    }
}