﻿using System.Web;

namespace Delete.Feature.Products.Models
{
    public class ProductDescriptionSplitTextObject
    {
        public string ReadLessText { get; set; }

        public string ReadMoreText { get; set; }
    }
}