﻿using Delete.Feature.Products.Enums;
using Delete.Foundation.RoyalCaninCore.Models;

namespace Delete.Feature.Products.Models
{
    public class ProductReviewsPagingViewModel : PaginatedResults
    {
        public string MainItemId { get; set; }
        public ReviewSortingType SortBy { get; set; }
    }
}