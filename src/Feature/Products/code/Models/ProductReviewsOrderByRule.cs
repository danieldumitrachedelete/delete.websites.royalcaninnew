﻿using System;
using System.Collections.Generic;
using System.Linq;
using Delete.Feature.Products.Enums;
using Delete.Feature.Products.Models.Bazaarvoice;

namespace Delete.Feature.Products.Models
{
    public class ProductReviewsOrderByRule
    {
        public ReviewSortingType Key { get; set; }
        public Func<IEnumerable<Review>, IOrderedEnumerable<Review>> OrderByExpression { get; set; }
    }
}