﻿namespace Delete.Feature.Products.Models
{
    using System.Collections.Generic;

    public partial class ProductCarousel
    {
        public IList<LocalProduct> Products { get; set; }

        public string ViewAllLinkUrl { get; set; }
    }
}