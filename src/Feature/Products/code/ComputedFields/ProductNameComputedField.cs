﻿using System;
using Delete.Feature.Products.Models;
using Delete.Foundation.DeleteFoundationCore;
using Delete.Foundation.DeleteFoundationCore.ComputedFields;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;
using Sitecore.ContentSearch.Diagnostics;

namespace Delete.Feature.Products.ComputedFields
{
    public class ProductNameComputedField : BaseComputedField, IComputedIndexField
    {
        public string FieldName { get; set; }

        public string ReturnType { get; set; }

        public object ComputeFieldValue(IIndexable indexable)
        {
            Guard.ArgumentNotNull(indexable, nameof(indexable));

            var indexedItem = this.GetItemHavingBaseTemplate(indexable, LocalProductConstants.TemplateId);
            if (indexedItem == null)
            {
                return null;
            }

            try
            {
                var productName = indexedItem.Fields[LocalProductConstants.ProductNameOverrideFieldName].Value
                    .OrDefault(indexedItem.Fields[LocalProductConstants.ProductNameFieldName].Value);
                return productName;
            }
            catch (Exception e)
            {
                CrawlingLog.Log.Warn(
                    $"Unexpected error when computing {this.GetType()} index field value for item [ID:{indexedItem.ID}]. Error message: {e.Message}");

                return null;
            }
        }
    }
}