﻿using System;
using Delete.Feature.Products.Models;
using Delete.Foundation.DeleteFoundationCore;
using Delete.Foundation.DeleteFoundationCore.ComputedFields;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;
using Sitecore.ContentSearch.Diagnostics;
using Sitecore.Data;
using Sitecore.Sites;

namespace Delete.Feature.Products.ComputedFields
{
    public class ProductLifestagesComputedField : BaseComputedField, IComputedIndexField
    {
        public string FieldName { get; set; }

        public string ReturnType { get; set; }

        public object ComputeFieldValue(IIndexable indexable)
        {
            Guard.ArgumentNotNull(indexable, nameof(indexable));

            var indexedItem = this.GetItemHavingBaseTemplate(indexable, LocalProductConstants.TemplateId);
            if (indexedItem == null)
            {
                return null;
            }

            try
            {
                var globalProductId = indexedItem.Fields[LocalProductConstants.GlobalProductFieldName].Value;
                if (!globalProductId.NotEmpty()) return null;

                using (new SiteContextSwitcher(SiteContext.GetSite(DefaultWebsite)))
                {
                    var globalProduct = indexedItem.Database.GetItem(new ID(globalProductId), indexedItem.Language);
                    var lifestages = globalProduct?.Fields[GlobalProductConstants.LifestagesFieldName].Value;
                    return lifestages?.ToSearchableGuid().Split(new[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
                }
            }
            catch (Exception e)
            {
                CrawlingLog.Log.Warn($"Unexpected error when computing {this.GetType()} index field value for item [ID:{indexedItem.ID}]. Error message: {e.Message}");

                return null;
            }
        }
    }
}