﻿using System.Linq;
using Delete.Feature.Products.Models;
using Glass.Mapper.Sc;
using Sitecore;
using Sitecore.Data.Managers;
using Sitecore.Pipelines.HttpRequest;

namespace Delete.Feature.Products.Pipeline
{
    public class CheckGlobalProduct : HttpRequestProcessor
    {
        private readonly ISitecoreContext sitecoreContext;

        public CheckGlobalProduct(ISitecoreContext sitecoreContext)
        {
            this.sitecoreContext = sitecoreContext;
        }

        public override void Process(HttpRequestArgs args)
        {
            if (Context.Item != null)
            {
                var template = TemplateManager.GetTemplate(Context.Item);
                var baseTemplates = template.GetBaseTemplates().Select(x => x.ID).ToList();
                if (baseTemplates.Contains(LocalProductConstants.TemplateId))
                {
                    var product = this.sitecoreContext.GetCurrentItem<LocalProduct>();
                    if (product != null && product.GlobalProduct == null)
                    {
                        Context.Item = null;
                    }
                }
            }
        }
    }
}