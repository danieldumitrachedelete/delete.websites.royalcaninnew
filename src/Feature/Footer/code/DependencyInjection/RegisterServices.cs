using Delete.Feature.Footer.Repositories;
using Delete.Foundation.InitializeDependencyInjection.Extensions;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.DependencyInjection;

namespace Delete.Feature.Footer.DependencyInjection
{
    public class RegisterServices : IServicesConfigurator
    {
        public void Configure(IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IFooterRepository, FooterRepository>();

            serviceCollection.AddMvcControllersInCurrentAssembly();
        }
    }
}
