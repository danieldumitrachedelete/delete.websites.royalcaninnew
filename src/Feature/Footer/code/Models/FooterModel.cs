﻿using Delete.Foundation.DataTemplates.Models.Menu;

namespace Delete.Feature.Footer.Models
{
    public class FooterModel
    {
        public Foundation.MultiSite.Models.ILocalMarketSettings LocalMarketSettings { get; set; }

        public MenuFolder Navigation { get; set; }
    }
}