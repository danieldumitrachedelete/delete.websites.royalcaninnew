using Delete.Feature.Footer.Models;
using Delete.Foundation.DataTemplates.Managers;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.MultiSite.Models;
using Glass.Mapper.Sc;

namespace Delete.Feature.Footer.Repositories
{
    public class FooterRepository : IFooterRepository
    {
        private readonly ISitecoreContext sitecoreContext;
        private readonly IMenuSearchManager menuSearchManager;

        public FooterRepository(
            ISitecoreContext sitecoreContext,
            IMenuSearchManager menuSearchManager)
        {
            this.sitecoreContext = sitecoreContext;
            this.menuSearchManager = menuSearchManager;
        }

        public FooterModel GetModel()
        {
            var localMarketSettings = this.sitecoreContext.GetRootItemRelative<LocalMarketSettings>(inferType: true, isLazy: true);
            var navigation = this.menuSearchManager.GetFooterFolderForLocal();
            return new FooterModel
            {
                LocalMarketSettings = localMarketSettings,
                Navigation = navigation
            };
        }
    }
}