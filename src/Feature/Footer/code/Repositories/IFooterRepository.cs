using Delete.Feature.Footer.Models;

namespace Delete.Feature.Footer.Repositories
{
    public interface IFooterRepository
    {
        FooterModel GetModel();
    }
}