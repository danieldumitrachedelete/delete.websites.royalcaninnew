using System.Linq;
using System.Web;
using AutoMapper;
using Delete.Foundation.DeleteFoundationCore.Controllers;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.MultiSite.Extensions;
using Delete.Foundation.MultiSite.Models;
using Glass.Mapper.Sc;

namespace Delete.Feature.Footer.Controllers
{
    using System;
    using System.Web.Mvc;
    using Sitecore.Data;
    using Repositories;
    using Delete.Foundation.DeleteFoundationCore.Helpers;

    public class FooterController : BaseController
    {
        private readonly IFooterRepository _footerRepository;
        private string FooterCacheKey = "Footer_Component";

        public FooterController(
            IFooterRepository footerRepository,
            ISitecoreContext sitecoreContext,
            IMapper mapper,
            ICacheManager cacheManager
        ) : base(sitecoreContext, mapper, cacheManager)
        {
            _footerRepository = footerRepository;
        }

        public ActionResult Footer()
        {
            var model = _cacheManager.CacheResults(() => _footerRepository.GetModel(), FooterCacheKey);

            if (model.Navigation != null)
            { 
                if (model.LocalMarketSettings.EnableYandexTracking)
                {
                    model.Navigation.RegionName = TranslateHelper.TextByDomainCached("Translations", "translations.feature.regionpicker.defaultregion");
                
                    foreach (var menuItem in model.Navigation.MenuItems)
                    {
                        menuItem.RegionName = model.Navigation.RegionName;
                    }
                    model.Navigation.MenuItems.Select(x => x.RegionName == null ? model.Navigation.RegionName : null);
                }
                else
                {
                    model.Navigation.RegionName = null;
                }
            }
            
            return View("~/Views/Feature/Footer/FooterNew.cshtml", model);
        }

        public ActionResult GlobalFooter()
        {
            var model = SitecoreContext.GetRootItemRelative<LocalMarketSettings>(inferType: true, isLazy: true);
            return View("~/Views/Feature/Footer/GlobalFooter.cshtml", model);
        }
    }
}
