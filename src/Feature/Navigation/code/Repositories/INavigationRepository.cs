using Delete.Feature.Navigation.Models;

namespace Delete.Feature.Navigation.Repositories
{
    public interface INavigationRepository
  {
      SectionalNavigationModel GetSectionalNavigationModel(string sectionName);
  }
}
