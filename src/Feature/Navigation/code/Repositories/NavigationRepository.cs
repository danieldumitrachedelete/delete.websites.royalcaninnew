using System;
using Delete.Feature.Navigation.Models;
using Delete.Foundation.DataTemplates.Managers;
using Delete.Foundation.DeleteFoundationCore.Helpers;
using Delete.Foundation.MultiSite.Extensions;
using Glass.Mapper.Sc;

namespace Delete.Feature.Navigation.Repositories
{
    public class NavigationRepository : INavigationRepository
    {
        private readonly ISitecoreContext _sitecoreContext;
        private readonly IMenuSearchManager _menuSearchManager;

        public NavigationRepository(ISitecoreContext sitecoreContext, IMenuSearchManager  menuSearchManager)
        {
            _sitecoreContext = sitecoreContext;
            _menuSearchManager = menuSearchManager;
        }

        public SectionalNavigationModel GetSectionalNavigationModel(string sectionName)
        {
            var localMarketSettings = _sitecoreContext.GetLocalMarketSettings();
            var navigation = _menuSearchManager.GetSectionalNavigationForLocal(sectionName);

            return new SectionalNavigationModel
            {
                HiddenSeoTitle = TranslateHelper.TextByDomainCached("Translations", "translations.feature.navigation.sectionalnavigation"),
                Navigation = navigation,
                LocalMarketSettings = localMarketSettings
            };
        }
    }
}