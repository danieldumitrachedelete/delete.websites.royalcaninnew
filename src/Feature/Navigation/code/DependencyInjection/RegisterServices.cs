using AutoMapper;
using Delete.Foundation.InitializeDependencyInjection.Extensions;
using Delete.Foundation.DataTemplates.Models.Menu;
using Delete.Foundation.DeleteFoundationCore.Managers;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.DependencyInjection;
using Delete.Feature.Navigation.Repositories;

namespace Delete.Feature.Navigation.DependencyInjection
{
    public class RegisterServices : IServicesConfigurator
    {
        public void Configure(IServiceCollection serviceCollection)
        {
            RegisterIndexes();

            serviceCollection.AddScoped<INavigationRepository, NavigationRepository>();

            serviceCollection.AddMvcControllersInCurrentAssembly();
        }

        private static void RegisterIndexes()
        {
            SearchManagersConfiguration.RegisterIndexConfiguration(typeof(MenuFolder), NavigationConstants.CustomIndexAlias);
            SearchManagersConfiguration.RegisterIndexConfiguration(typeof(MenuInfoItem), NavigationConstants.CustomIndexAlias);
            SearchManagersConfiguration.RegisterIndexConfiguration(typeof(MenuGroup), NavigationConstants.CustomIndexAlias);
            SearchManagersConfiguration.RegisterIndexConfiguration(typeof(MenuItem), NavigationConstants.CustomIndexAlias);
        }
    }
}
