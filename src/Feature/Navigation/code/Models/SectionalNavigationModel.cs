﻿using Delete.Foundation.DataTemplates.Models.Menu;
using Delete.Foundation.DeleteFoundationCore.Models;

namespace Delete.Feature.Navigation.Models
{
    public class SectionalNavigationModel
    {
        public string HiddenSeoTitle { get; set; }

        public Foundation.MultiSite.Models.ILocalMarketSettings LocalMarketSettings { get; set; }

        public MenuFolder Navigation { get; set; }

        public IGlassBase CurrentPage { get; set; }
    }
}