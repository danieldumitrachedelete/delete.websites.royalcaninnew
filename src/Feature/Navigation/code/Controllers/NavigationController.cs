using System.Linq;
using System.Web;
using AutoMapper;
using Delete.Foundation.DeleteFoundationCore.Controllers;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.MultiSite.Extensions;
using Glass.Mapper.Sc;

namespace Delete.Feature.Navigation.Controllers
{
    using System.Web.Mvc;
    using Repositories;
    using System.Collections.Generic;
    using Delete.Foundation.DeleteFoundationCore.Models;

    public class NavigationController : BaseController
    {
        private readonly INavigationRepository _navRepository;
        private string SectionalNavigationCacheKey = "Navigation_SectionalNavigation_Section{0}";

        public NavigationController(
            INavigationRepository navRepository,
            ISitecoreContext sitecoreContext, 
            IMapper mapper, 
            ICacheManager cacheManager
        ) : base(sitecoreContext, mapper, cacheManager)
        {
            _navRepository = navRepository;
        }

        public ActionResult SectionalNavigation()
        {
            string sectionName = HttpUtility.ParseQueryString(this.RenderingContextWrapper.GetRenderingParameters())["sectionName"];
               
            var model = _cacheManager.CacheResults(() => _navRepository.GetSectionalNavigationModel(sectionName), string.Format(SectionalNavigationCacheKey, sectionName));
            
            model.CurrentPage = GetContextItem<IGlassBase>();
            
            return View("~/Views/Feature/Navigation/SectionalNavigation.cshtml", model);
        }
    }
}
