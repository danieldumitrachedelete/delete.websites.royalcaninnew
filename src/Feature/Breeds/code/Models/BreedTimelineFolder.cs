﻿using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration.Attributes;

namespace Delete.Feature.Breeds.Models
{
    public partial class BreedTimelineFolder
    {
        [SitecoreChildren]
        public virtual IEnumerable<BreedTimelineAge> Ages { get; set; }
    }
}