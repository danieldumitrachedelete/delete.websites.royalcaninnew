﻿using System;
using System.Collections.Generic;
using System.Linq;
using Delete.Foundation.DeleteFoundationCore.Extensions;

namespace Delete.Feature.Breeds.Models
{
    public class BreedsFilterModel
    {
        public BreedsFilterModel(BreedsFilterViewModel viewModel)
        {
            if (viewModel == null)
            {
                return;
            }

            this.SearchQuery = viewModel.SearchQuery;
            this.Sizes = viewModel.Sizes;
            this.Characteristics = viewModel.Characteristics;
            this.Coats = viewModel.Coats;
            this.EnergyLevels = viewModel.EnergyLevels;
            this.Habitats = viewModel.Habitats;
        }

        public string SearchQuery { get; set; }

        public Guid Species { get; set; }

        public string Sizes { get; set; }

        public string Characteristics { get; set; }

        public string Coats { get; set; }

        public string EnergyLevels { get; set; }

        public string Habitats { get; set; }

        public IEnumerable<string> Values
        {
            get
            {
                return new[] {SearchQuery, Sizes, Characteristics, Coats, EnergyLevels, Habitats, Species.ToString()}
                    .Where(x => x.NotEmpty());
            }
        }
    }
}