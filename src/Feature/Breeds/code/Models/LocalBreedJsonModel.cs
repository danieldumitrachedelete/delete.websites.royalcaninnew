﻿namespace Delete.Feature.Breeds.Models
{
    public partial class ContentSearchPanel
    {
        public string LocalBreedsJson { get; set; }
    }

    public partial class MixedBreedBlock
    {
        public string LocalBreedsJson { get; set; }
    }
}