﻿using System;
using System.Collections.Generic;
using System.Linq;
using Delete.Foundation.DataTemplates.Interfaces;
using Delete.Foundation.DataTemplates.Models.Interfaces;
using Glass.Mapper.Sc.Configuration.Attributes;
using Sitecore.ContentSearch;

namespace Delete.Feature.Breeds.Models
{
    public partial class LocalDogBreed : IFilterRelatedEntities
    {
        private IEnumerable<Guid> relatedEntities;

        [SitecoreField(FieldId = _LocalBreedConstants.GlobalBreedInterfaceFieldId, FieldName = _LocalBreedConstants.GlobalBreedInterfaceFieldName)]
        [IndexField(_LocalBreedConstants.GlobalBreedInterfaceFieldName)]
        public virtual GlobalDogBreed GlobalDogBreed { get; set; }

        [IndexField("breedEnergyLevel")]
        public virtual Guid SearchableEnergyLevel { get; set; }

        [IndexField("breedEnergyLevelText")]
        public virtual string SearchableEnergyLevelText { get; set; }

        [IndexField("breedGroup")]
        public virtual string SearchableGroupText { get; set; }

        public override IEnumerable<Guid> RelatedEntities
        {
            get
            {
                if (this.relatedEntities != null)
                {
                    return this.relatedEntities;
                }

                var result = base.RelatedEntities.ToList();

                if (this.GlobalDogBreed?.EnergyLevel?.Id != null)
                {
                    result.Add(this.GlobalDogBreed.EnergyLevel.Id);
                }

                return this.relatedEntities = result;
            }
        }
    }
}
