﻿using System.Linq;
using Delete.Foundation.DataTemplates.Models.Dictionary;
using Delete.Foundation.DataTemplates.Models.Interfaces;
using Glass.Mapper.Sc.Configuration.Attributes;
using Sitecore.ContentSearch;

namespace Delete.Feature.Breeds.Models
{
    public partial class GlobalBreed
    {
        public string CharacteristicsLine => string.Join(" / ", Characteristics.Select(x => x.Text));
    }
}