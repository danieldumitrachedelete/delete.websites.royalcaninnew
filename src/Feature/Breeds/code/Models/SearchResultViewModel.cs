﻿using System.Collections.Generic;
using System.Linq;
using Delete.Foundation.DataTemplates.Models.Filter;
using Delete.Foundation.DataTemplates.Models.Folder;
using Delete.Foundation.DataTemplates.Models.Taxonomy;

namespace Delete.Feature.Breeds.Models
{
    public class SearchResultViewModel
    {
        public int PageSize { get; set; }

        public FilterGroupFolder MainFilterGroup { get; set; }

        public Specie Specie { get; set; }

        public IEnumerable<SearchResultsGroupViewModel> SearchResults { get; set; }
    }
}