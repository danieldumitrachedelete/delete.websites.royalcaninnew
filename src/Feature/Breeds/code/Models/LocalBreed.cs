﻿using System;
using System.Collections.Generic;
using System.Linq;
using Delete.Foundation.DataTemplates.Interfaces;
using Delete.Foundation.DataTemplates.Models.Interfaces;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.SchemaOrg;
using Delete.Foundation.DeleteFoundationCore.SchemaOrg.Extensions;
using Delete.Foundation.DeleteFoundationCore.SchemaOrg.Interfaces;
using Sitecore.ContentSearch;

namespace Delete.Feature.Breeds.Models
{
    public partial interface ILocalBreed : IFilterRelatedEntities, IMicrodata
    {
        GlobalBreed GlobalBreed { get; set; }
    }

    public partial class LocalBreed
    {
        private IEnumerable<Guid> relatedEntities;

        public virtual IEnumerable<Guid> RelatedEntities
        {
            get
            {
                if (this.relatedEntities != null)
                {
                    return this.relatedEntities;
                }

                var result = new List<Guid>();

                if (this.GlobalBreed?.Size?.Id != null)
                {
                    result.Add(this.GlobalBreed.Size.Id);
                }

                if (this.GlobalBreed?.Characteristics != null && this.GlobalBreed.Characteristics.Any())
                {
                    result.AddRange(this.GlobalBreed.Characteristics.Select(x => x.Id));
                }

                return this.relatedEntities = result;
            }
        }

        public virtual GlobalBreed GlobalBreed
        {
            get => this.GlobalBreedInterface as GlobalBreed;
            set => this.GlobalBreedInterface = value as I_GlobalBreed;
        }

        [IndexField("breedSize")]
        public virtual Guid SearchableSize { get; set; }

        [IndexField("breedSizeText")]
        public virtual string SearchableSizeText { get; set; }

        [IndexField("breedName")]
        public virtual string SearchableBreedName { get; set; }

        [IndexField("breedCharacteristics")]
        public virtual IEnumerable<Guid> SearchableCharacteristics { get; set; }

        [IndexField("breedCharacteristicsText")]
        public virtual string SearchableCharacteristicsText { get; set; }

        [IndexField("hideBreedGlobally")]
        public virtual bool SearchableHideBreedGlobally { get; set; }

        public Thing GetMicrodata(IAuthorResolver authorResolver)
        {
            return new NewsArticle
            {
                Headline = LocalBreedName.OrDefault(GlobalBreed?.BreedName) ?? String.Empty,
                Image = GlobalBreed?.Thumbnail.GetMicrodata(authorResolver),
                DatePublished = Created.ToDateTimeOffset(),
                DateModified = Updated.ToDateTimeOffset(),
                Description = LocalBreedName.OrDefault(GlobalBreed?.BreedName) ?? String.Empty,
                Url = new Uri(Url ?? String.Empty, UriKind.RelativeOrAbsolute),
                Author = authorResolver.GetAuthor(),
                Publisher = authorResolver.GetAuthor(),
                MainEntityOfPage = new WebPage
                {
                    Id = new Uri(Url ?? String.Empty, UriKind.RelativeOrAbsolute)
                }
            };
        }
    }
}