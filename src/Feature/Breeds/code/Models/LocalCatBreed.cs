﻿using System;
using System.Collections.Generic;
using System.Linq;
using Delete.Foundation.DataTemplates.Interfaces;
using Delete.Foundation.DataTemplates.Models.Interfaces;
using Glass.Mapper.Sc.Configuration.Attributes;
using Sitecore.ContentSearch;

namespace Delete.Feature.Breeds.Models
{
    public partial class LocalCatBreed : IFilterRelatedEntities
    {
        private IEnumerable<Guid> relatedEntities;

        [SitecoreField(FieldId = _LocalBreedConstants.GlobalBreedInterfaceFieldId, FieldName = _LocalBreedConstants.GlobalBreedInterfaceFieldName)]
        [IndexField(_LocalBreedConstants.GlobalBreedInterfaceFieldName)]
        public virtual GlobalCatBreed GlobalCatBreed { get; set; }

        [IndexField("breedCoat")]
        public virtual Guid SearchableCoat { get; set; }

        [IndexField("breedCoatText")]
        public virtual string SearchableCoatText { get; set; }

        [IndexField("breedHabitat")]
        public virtual Guid SearchableHabitat { get; set; }

        [IndexField("breedHabitatText")]
        public virtual string SearchableHabitatText { get; set; }

        public override IEnumerable<Guid> RelatedEntities
        {
            get
            {
                if (this.relatedEntities != null)
                {
                    return this.relatedEntities;
                }

                var result = base.RelatedEntities.ToList();


                if (this.GlobalCatBreed?.Coat?.Id != null)
                {
                    result.Add(this.GlobalCatBreed.Coat.Id);
                }

                if (this.GlobalCatBreed?.Habitat?.Id != null)
                {
                    result.Add(this.GlobalCatBreed.Habitat.Id);
                }

                return this.relatedEntities = result;
            }
        }
    }
}
