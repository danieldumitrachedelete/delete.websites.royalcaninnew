﻿using System.Collections.Generic;

namespace Delete.Feature.Breeds.Models
{
    public class TimelineViewModel
    {
        public GlobalBreed Breed { get; set; }

        public IList<BreedTimelineAge> Ages { get; set; }
    }
}