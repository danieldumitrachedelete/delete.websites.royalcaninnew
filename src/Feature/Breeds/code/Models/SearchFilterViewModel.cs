﻿using System.Collections.Generic;
using Delete.Foundation.DataTemplates.Interfaces;
using Delete.Foundation.DataTemplates.Models.Folder;
using Delete.Foundation.DataTemplates.Models.Taxonomy;

namespace Delete.Feature.Breeds.Models
{
    public class SearchFilterViewModel
    {
        public Specie Specie { get; set; }

        public IEnumerable<FilterGroupFolder> FilterGroups { get; set; }

        public IEnumerable<IFilterRelatedEntities> FilterRelatedSearchResults { get; set; }
    }
}