﻿using System;
using System.Collections.Generic;
using Delete.Foundation.DataTemplates.Models.Filter;

namespace Delete.Feature.Breeds.Models
{
    public class SearchResultsGroupViewModel
    {
        public FilterItem Key { get; set; }

        public IEnumerable<LocalBreed> Items { get; set; }

        public int ItemsMaxCount { get; set; }
    }
}