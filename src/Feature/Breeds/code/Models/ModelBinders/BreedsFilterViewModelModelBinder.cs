﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;

namespace Delete.Feature.Breeds.Models.ModelBinders
{
    public class BreedsFilterViewModelModelBinder: DefaultModelBinder
    {
        private readonly IMapper _mapper;

        public BreedsFilterViewModelModelBinder()
        {
            _mapper = DependencyResolver.Current.GetService<IMapper>();
        }

        public BreedsFilterViewModelModelBinder(IMapper mapper)
        {
            _mapper = mapper;
        }

        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var model = base.BindModel(controllerContext, bindingContext) as BreedsFilterViewModel;
            if (model != null)
            {
                return model;
            }

            model = _mapper.Map<BreedsFilterViewModel>(controllerContext);

            return model;
        }
    }
}