﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Delete.Foundation.DataTemplates.Models.Taxonomy;

namespace Delete.Feature.Breeds.Models
{
    public class SearchViewModel
    {
        public Guid FilterConfiguration { get; set; }

        public SearchFilterViewModel FilterModel { get; set; }

        public SearchResultViewModel SearchResultModel { get; set; }
    }
}