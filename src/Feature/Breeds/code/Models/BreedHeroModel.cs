﻿namespace Delete.Feature.Breeds.Models
{
    public class BreedHeroModel
    {
        public LocalBreed LocalBreed { get; set; }

        public GlobalBreed GlobalBreed { get; set; }
    }
}