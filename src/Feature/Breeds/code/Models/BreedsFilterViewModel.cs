﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Delete.Feature.Breeds.Models
{
    public class BreedsFilterViewModel
    {
        public string SearchQuery {get; set; }

        public Guid Filter { get; set; }

        public string Sizes { get; set; }

        public string Characteristics { get; set; }

        public string Coats { get; set; }

        public string EnergyLevels { get; set; }

        public string Habitats { get; set; }

        public bool ViewAll { get; set; }
    }
}