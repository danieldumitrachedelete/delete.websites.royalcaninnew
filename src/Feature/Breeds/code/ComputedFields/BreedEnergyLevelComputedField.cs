﻿using System;
using Delete.Feature.Breeds.Models;
using Delete.Foundation.DataTemplates.ComputedFields;
using Delete.Foundation.DataTemplates.Models.Interfaces;
using Delete.Foundation.DeleteFoundationCore;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;
using Sitecore.ContentSearch.Diagnostics;
using Sitecore.Data;
using Sitecore.Sites;

namespace Delete.Feature.Breeds.ComputedFields
{
    public class BreedEnergyLevelComputedField : BaseDictionaryTextComputedField, IComputedIndexField
    {
        public object ComputeFieldValue(IIndexable indexable)
        {
            Guard.ArgumentNotNull(indexable, nameof(indexable));

            var indexedItem = GetItemHavingBaseTemplate(indexable, LocalDogBreedConstants.TemplateId);
            if (indexedItem == null)
            {
                return null;
            }

            try
            {
                var globalBreedId = indexedItem.Fields[_LocalBreedConstants.GlobalBreedInterfaceFieldName].Value;
                if (globalBreedId.NotEmpty())
                {
                    using (new SiteContextSwitcher(SiteContext.GetSite(DefaultWebsite)))
                    {
                        var globalBreed = indexedItem.Database.GetItem(new ID(globalBreedId), indexedItem.Language);
                        if (globalBreed != null)
                        {
                            var energyLevel = globalBreed.Fields[GlobalDogBreedConstants.EnergyLevelFieldName].Value;
                            return energyLevel?.ToSearchableGuid();
                        }
                    }
                }
                return null;
            }
            catch (Exception e)
            {
                CrawlingLog.Log.Warn($"Unexpected error when computing {GetType()} index field value for item [ID:{indexedItem.ID}]. Error message: {e.Message}");

                return null;
            }
        }

        public string FieldName { get; set; }
        public string ReturnType { get; set; }
    }
}