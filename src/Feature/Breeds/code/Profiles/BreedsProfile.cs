using AutoMapper;
using Delete.Feature.Breeds.Models;
using Delete.Foundation.DataTemplates.Models;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using JetBrains.Annotations;

namespace Delete.Feature.Breeds.Profiles
{
    [UsedImplicitly]
    public class BreedsProfile : Profile
    {
        public BreedsProfile()
        {
            CreateMap<LocalBreed, PredictiveSearchItemJsonModel>()
                .ForMember(x => x.Title, opt => opt.MapFrom(z => z.LocalBreedName.OrDefault(z.GlobalBreed.BreedName)))
                .ForMember(x => x.Description, opt => opt.MapFrom(z => z.LocalAlias))
                .ForMember(x => x.Url, opt => opt.MapFrom(z => z.Url))
                ;
        }
    }
}
