﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Delete.Feature.Breeds.Models;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.DeleteFoundationCore.Managers;
using Delete.Foundation.DeleteFoundationCore.Models;
using Sitecore.ContentSearch;

namespace Delete.Feature.Breeds.Managers
{
    public interface IBreedSearchLocalConfigurationSearchManager : IBaseSearchManager<BreedSearchLocalConfiguration>
    {
        IEnumerable<BreedSearchLocalConfiguration> GetAll();
    }

    public class BreedSearchLocalConfigurationSearchManager : BaseSearchManager<BreedSearchLocalConfiguration>, IBreedSearchLocalConfigurationSearchManager
    {
        public IEnumerable<BreedSearchLocalConfiguration> GetAll()
        {
            var filtered = GetQueryable();

            filtered = FilterLocal(filtered);

            return filtered.MapResults(SitecoreContext, true).ToList();
        }

        protected override IQueryable<BreedSearchLocalConfiguration> GetQueryable()
        {
            return base.GetQueryable()
                .Where(x => x.TemplateId == BreedSearchLocalConfigurationConstants.TemplateId
                            || x.BaseTemplates.Contains(BreedSearchLocalConfigurationConstants.TemplateId.Guid));
        }
    }
}