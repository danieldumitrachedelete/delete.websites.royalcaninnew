﻿using System;
using System.Collections.Generic;
using System.Linq;
using Delete.Feature.Breeds.Models;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Sitecore.ContentSearch.Linq.Utilities;

namespace Delete.Feature.Breeds.Managers
{
    public interface ILocalCatBreedSearchManager : IBaseSearchManager<LocalCatBreed>
    {
        IEnumerable<LocalCatBreed> GetAll();

        IEnumerable<LocalCatBreed> FindByKeyword(string keyword);
    }

    public class LocalCatBreedSearchManager : BaseLocalBreedSearchManager<LocalCatBreed>, ILocalCatBreedSearchManager
    {
        public IEnumerable<LocalCatBreed> GetAll()
        {
            var filtered = this.GetQueryable();

            return filtered.MapResults(this.SitecoreContext, true).ToList();
        }

        public IEnumerable<LocalCatBreed> FindByKeyword(string keyword)
        {
            var filtered = this.GetQueryable();

            if (keyword.NotEmpty())
            {
                var predicate = this.FilterByKeyword(keyword);
                
                predicate = predicate.Or(x => x.SearchableCoatText.StartsWith(keyword) || x.SearchableHabitatText.StartsWith(keyword));

                var terms = keyword.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                
                if (terms.Length > 1)
                {
                    var firstWord = terms.FirstOrDefault();

                    var predicateCoat = PredicateBuilder.True<LocalCatBreed>();
                    predicateCoat = predicateCoat.And(x => x.SearchableCoatText.StartsWith(firstWord));
                    predicateCoat = terms.Skip(1).Aggregate(predicateCoat, (current, term) => current.And(x => x.SearchableCoatText.Contains(term)));

                    var predicateHabitat = PredicateBuilder.True<LocalCatBreed>();
                    predicateHabitat = predicateHabitat.And(x => x.SearchableHabitatText.StartsWith(firstWord));
                    predicateHabitat = terms.Skip(1).Aggregate(predicateHabitat, (current, term) => current.And(x => x.SearchableHabitatText.Contains(term)));

                    predicate = predicate.Or(predicateCoat);
                    predicate = predicate.Or(predicateHabitat);
                }

                filtered = filtered.Where(predicate);
            }

            return filtered.MapResults(this.SitecoreContext, true).ToList();
        }

        protected IQueryable<LocalCatBreed> ApplyFilters(IQueryable<LocalCatBreed> breeds, BreedsFilterModel filter)
        {
            breeds = this.ApplyBaseFilters(breeds, filter);

            if (filter.Coats.NotEmpty())
            {
                breeds = breeds.Where(this.GetFilterItemIds(filter.Coats)
                    .Aggregate(PredicateBuilder.False<LocalCatBreed>(), (current, filterItemId) => current.Or(x => x.SearchableCoat == filterItemId)));
            }

            if (filter.Habitats.NotEmpty())
            {
                breeds = breeds.Where(this.GetFilterItemIds(filter.Habitats)
                    .Aggregate(PredicateBuilder.False<LocalCatBreed>(), (current, filterItemId) => current.Or(x => x.SearchableHabitat == filterItemId)));
            }

            return breeds;
        }

        protected override IQueryable<LocalCatBreed> GetQueryable()
        {
            var breeds = this.FilterLocal(base.GetQueryable());
            return breeds
                .Where(x => x.TemplateId == LocalCatBreedConstants.TemplateId 
                            || x.BaseTemplates.Contains(LocalCatBreedConstants.TemplateId.Guid));
        }
    }
}