﻿using System;
using System.Collections.Generic;
using System.Linq;
using Delete.Feature.Breeds.Models;
using Delete.Foundation.DataTemplates.Models.Taxonomy;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.DeleteFoundationCore.Managers;

namespace Delete.Feature.Breeds.Managers
{
    using Sitecore.Data.Items;

    public interface IBreedSearchManager : IBaseSearchManager<LocalBreed>
    {
        IEnumerable<LocalBreed> GetAll();

        IEnumerable<LocalBreed> GetAllLocalForSpecies(Guid speciesGuid);

        IEnumerable<LocalBreed> GetAllLocalForSearch(Item parentItem);
    }

    public class BreedSearchManager : BaseSearchManager<LocalBreed>, IBreedSearchManager
    {
        public IEnumerable<LocalBreed> GetAll()
        {
            var filtered = this.GetQueryable();

            return filtered.MapResults(this.SitecoreContext, true).ToList();
        }

        public IEnumerable<LocalBreed> GetAllLocalForSpecies(Guid speciesGuid)
        {
            var filtered = this.GetQueryable();
            filtered = FilterSpecies(speciesGuid, filtered);
            filtered = this.FilterLocal(filtered);

            return filtered.MapResults(this.SitecoreContext, true).Where(x => x.GlobalBreed != null).ToList();
        }

        public IEnumerable<LocalBreed> GetAllLocalForSearch(Item parentItem)
        {
            var filtered = this.GetQueryable();
            filtered = filtered.Where(x => x.Fullpath.StartsWith(parentItem.Paths.FullPath));

            return filtered.MapResults(this.SitecoreContext, true).Where(x => x.GlobalBreed != null).ToList();
        }

        protected static IQueryable<LocalBreed> FilterSpecies(Guid speciesGuid, IQueryable<LocalBreed> filtered)
        {
            if (speciesGuid == SpecieConstants.DogItemId)
            {
                filtered = filtered.Where(x => x.TemplateId == LocalDogBreedConstants.TemplateId
                                               || x.BaseTemplates.Contains(LocalDogBreedConstants.TemplateId.Guid));
            }
            else
            {
                filtered = filtered.Where(x => x.TemplateId == LocalCatBreedConstants.TemplateId
                                               || x.BaseTemplates.Contains(LocalCatBreedConstants.TemplateId.Guid));
            }

            return filtered;
        }

        protected override IQueryable<LocalBreed> GetQueryable()
        {
            return base.GetQueryable()
                .Where(x => x.TemplateId == LocalBreedConstants.TemplateId
                            || x.BaseTemplates.Contains(LocalBreedConstants.TemplateId.Guid)
                            || x.TemplateId == LocalDogBreedConstants.TemplateId
                            || x.BaseTemplates.Contains(LocalDogBreedConstants.TemplateId.Guid)
                            || x.TemplateId == LocalCatBreedConstants.TemplateId
                            || x.BaseTemplates.Contains(LocalCatBreedConstants.TemplateId.Guid))
                .Where(x => !x.HideBreedLocally && !x.SearchableHideBreedGlobally);
        }
    }
}