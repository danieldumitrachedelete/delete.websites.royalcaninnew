﻿using System;
using System.Collections.Generic;
using System.Linq;
using Delete.Feature.Breeds.Models;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Sitecore.ContentSearch.Linq.Utilities;

namespace Delete.Feature.Breeds.Managers
{
    public interface ILocalDogBreedSearchManager : IBaseSearchManager<LocalDogBreed>
    {
        IEnumerable<LocalDogBreed> GetAll();

        IEnumerable<LocalDogBreed> FindByKeyword(string keyword);
    }

    public class LocalDogBreedSearchManager : BaseLocalBreedSearchManager<LocalDogBreed>, ILocalDogBreedSearchManager
    {
        public IEnumerable<LocalDogBreed> GetAll()
        {
            var filtered = this.GetQueryable();

            return filtered.MapResults(this.SitecoreContext, true).ToList();
        }

        public IEnumerable<LocalDogBreed> FindByKeyword(string keyword)
        {
            var filtered = this.GetQueryable();

            if (keyword.NotEmpty())
            {
                var predicate = this.FilterByKeyword(keyword);

                predicate = predicate.Or(x => x.SearchableEnergyLevelText.StartsWith(keyword) || x.SearchableGroupText.StartsWith(keyword));
                
                var terms = keyword.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                if (terms.Length > 1)
                {
                    var firstWord = terms.FirstOrDefault();

                    var predicateEnergy = PredicateBuilder.True<LocalDogBreed>();
                    predicateEnergy = predicateEnergy.And(x => x.SearchableEnergyLevelText.StartsWith(firstWord));
                    predicateEnergy = terms.Skip(1).Aggregate(predicateEnergy, (current, term) => current.And(x => x.SearchableEnergyLevelText.Contains(term)));

                    var predicateGroup = PredicateBuilder.True<LocalDogBreed>();
                    predicateGroup = predicateGroup.And(x => x.SearchableGroupText.StartsWith(firstWord));
                    predicateGroup = terms.Skip(1).Aggregate(predicateGroup, (current, term) => current.And(x => x.SearchableGroupText.Contains(term)));

                    predicate = predicate.Or(predicateEnergy);
                    predicate = predicate.Or(predicateGroup);
                }

                filtered = filtered.Where(predicate);
            }

            return filtered.MapResults(this.SitecoreContext, true).ToList();
        }

        protected IQueryable<LocalDogBreed> ApplyFilters(IQueryable<LocalDogBreed> breeds, BreedsFilterModel filter)
        {
            breeds = this.ApplyBaseFilters(breeds, filter);

            if (filter.EnergyLevels.NotEmpty())
            {
                breeds = breeds.Where(this.GetFilterItemIds(filter.EnergyLevels)
                    .Aggregate(PredicateBuilder.False<LocalDogBreed>(), (current, filterItemId) => current.Or(x => x.SearchableEnergyLevel == filterItemId)));
            }

            return breeds;
        }

        protected override IQueryable<LocalDogBreed> GetQueryable()
        {
            var breeds = this.FilterLocal(base.GetQueryable());
            return breeds
                .Where(x => x.TemplateId == LocalDogBreedConstants.TemplateId 
                            || x.BaseTemplates.Contains(LocalDogBreedConstants.TemplateId.Guid));
        }
    }
}