﻿using System;
using System.Linq.Expressions;
using Delete.Feature.Breeds.Models;
using Delete.Foundation.DeleteFoundationCore.Managers;
using Sitecore.ContentSearch.Linq.Utilities;

namespace Delete.Feature.Breeds.Managers
{
    using System.Collections.Generic;
    using System.Linq;

    using Delete.Foundation.DeleteFoundationCore.Extensions;

    public class BaseLocalBreedSearchManager<T> : BaseSearchManager<T> where T: LocalBreed
    {
        protected Expression<Func<T, bool>> FilterByKeyword(string keyword)
        {
            var predicate = PredicateBuilder.False<T>();

            predicate = predicate.Or(x => x.SearchableBreedName.StartsWith(keyword)
                                          || x.LocalBreedName.StartsWith(keyword)
                                          || x.LocalAlias.StartsWith(keyword)
                                          || x.SearchableSizeText.StartsWith(keyword)
                                          || x.SearchableCharacteristicsText.StartsWith(keyword));
            
            var terms = keyword.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            if(terms.Length > 1)
            {
                var firstWord = terms.FirstOrDefault();

                var predicateSearchableName = PredicateBuilder.True<T>();
                predicateSearchableName = predicateSearchableName.And(x => x.SearchableBreedName.StartsWith(firstWord));
                predicateSearchableName = terms.Skip(1).Aggregate(predicateSearchableName, (current, term) => current.And(x => x.SearchableBreedName.Contains(term)));

                var predicateBreedName = PredicateBuilder.True<T>();
                predicateBreedName = predicateBreedName.And(x => x.LocalBreedName.StartsWith(firstWord));
                predicateBreedName = terms.Skip(1).Aggregate(predicateBreedName, (current, term) => current.And(x => x.LocalBreedName.Contains(term)));


                var predicateAlias = PredicateBuilder.True<T>();
                predicateAlias = predicateAlias.And(x => x.LocalAlias.StartsWith(firstWord));
                predicateAlias = terms.Skip(1).Aggregate(predicateAlias, (current, term) => current.And(x => x.LocalAlias.Contains(term)));

                var predicateSize = PredicateBuilder.True<T>();
                predicateSize = predicateSize.And(x => x.SearchableSizeText.StartsWith(firstWord));
                predicateSize = terms.Skip(1).Aggregate(predicateSize, (current, term) => current.And(x => x.SearchableSizeText.Contains(term)));

                var predicateCharacteristics = PredicateBuilder.True<T>();
                predicateCharacteristics = predicateCharacteristics.And(x => x.SearchableCharacteristicsText.StartsWith(firstWord));
                predicateCharacteristics = terms.Skip(1).Aggregate(predicateCharacteristics, (current, term) => current.And(x => x.SearchableCharacteristicsText.Contains(term)));


                predicate = predicate.Or(predicateSearchableName);
                predicate = predicate.Or(predicateBreedName);
                predicate = predicate.Or(predicateAlias);
                predicate = predicate.Or(predicateSize);
                predicate = predicate.Or(predicateCharacteristics);
            }

            return predicate;
        }

        protected override IQueryable<T> GetQueryable()
        {
            return base.GetQueryable()
                .Where(x => !x.HideBreedLocally && !x.SearchableHideBreedGlobally);
        }

        protected IQueryable<T> ApplyBaseFilters(IQueryable<T> breeds, BreedsFilterModel filter)
        {
            if (filter.Sizes.NotEmpty())
            {
                breeds = breeds.Where(this.GetFilterItemIds(filter.Sizes)
                    .Aggregate(PredicateBuilder.False<T>(), (current, filterItemId) => current.Or(x => x.SearchableSize == filterItemId)));
            }

            if (filter.Characteristics.NotEmpty())
            {
                breeds = breeds.Where(this.GetFilterItemIds(filter.Characteristics)
                    .Aggregate(PredicateBuilder.False<T>(), (current, filterItemId) => current.Or(x => x.SearchableCharacteristics.Contains(filterItemId))));
            }

            return breeds;
        }

        protected IEnumerable<Guid> GetFilterItemIds(string filterValues)
        {
            return filterValues.SplitStringByComma().Where(x => Guid.TryParse(x, out _)).Select(Guid.Parse);
        }
    }
}