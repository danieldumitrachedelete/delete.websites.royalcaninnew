﻿using Delete.Foundation.ItemImport.LogManagerImport;

namespace Delete.Feature.Breeds.Import.Processors
{
    using Foundation.DataTemplates.Models.Taxonomy;
    using Foundation.ItemImport.Interfaces;

    using Glass.Mapper.Sc;

    using Models;

    public interface IDogGlobalBreedItemProcessor : IGlobalBreedItemProcessor<GlobalDogBreed>
    {
    }

    public class DogGlobalBreedItemProcessor : GlobalBreedItemProcessor<GlobalDogBreed>, IDogGlobalBreedItemProcessor
    {
        public DogGlobalBreedItemProcessor(
            ISitecoreContext sitecoreContext,
            ILogManagerImport logManagerImport,
            string locationPathOverride = null) 
            : base(sitecoreContext, logManagerImport, Species.Dogs, locationPathOverride)
        {
        }
    }
}