﻿using System;
using System.Collections.Generic;
using Delete.Feature.Breeds.Models;
using Delete.Foundation.DataTemplates.Models.Interfaces;
using Delete.Foundation.DataTemplates.Models.Taxonomy;
using Delete.Foundation.ItemImport.ImportProcessors;
using Delete.Foundation.ItemImport.Interfaces;
using Delete.Foundation.ItemImport.LogManagerImport;
using Glass.Mapper;
using Glass.Mapper.Sc;
using Sitecore.Globalization;

namespace Delete.Feature.Breeds.Import.Processors
{
    using Foundation.ItemImport.Models;

    public class GlobalBreedItemProcessor<TGlobalBreed> : SpeciesSpecificImportItemProcessorBase<TGlobalBreed, BreedDto>, IGlobalBreedItemProcessor<TGlobalBreed> where TGlobalBreed : GlobalBreed, I_GlobalBreed
    {
        public GlobalBreedItemProcessor(ISitecoreContext sitecoreContext, ILogManagerImport logManagerImport, Species species, string locationPathOverride = null) : base(sitecoreContext, logManagerImport, species,  locationPathOverride)
        {
        }

        protected override Func<TGlobalBreed, string> IdStringFromSitecoreItem => breed => breed.LSHBreedID;

        protected override Func<BreedDto, string> IdStringFromImportObj => breed => breed?.BreedId;
        
        protected override Func<BreedDto, string> ItemNameFromImportObj => breed => breed?.Name;

        protected override string DefaultLocation => Sitecore.Configuration.Settings.GetSetting(
            "Breeds.DefaultGlobalBreedsLocation", "/sitecore/content/Global Configuration/Breeds/Global Breeds");

        public override TGlobalBreed ProcessItem(BreedDto importObj, IEnumerable<Language> languageVersions, string pathOverride = null)
        {
            var createGlobalBreed =
                Sitecore.Configuration.Settings.GetBoolSetting("Breeds.CreateNewGlobalBreedsDuringProductImport",
                    false);

            if (createGlobalBreed)
            {
                return base.ProcessItem(importObj, languageVersions, pathOverride);
            }

            var id = this.CalculateItemId(importObj);
            if (!id.HasValue())
            {
                return null;
            }

            TGlobalBreed item;

            using (new VersionCountDisabler())
            {
                item = this.GetItem(id);
            }
            return item;
        }

        protected override TGlobalBreed MapDefaultVersionFields(TGlobalBreed item, BreedDto importObj)
        {
            item = base.MapDefaultVersionFields(item, importObj);

            item.BreedName = this.ItemNameFromImportObj(importObj);
            item.LSHBreedID = this.IdStringFromImportObj(importObj);

            return item;
        }
    }
}