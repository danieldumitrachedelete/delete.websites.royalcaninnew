﻿using Delete.Foundation.ItemImport.LogManagerImport;

namespace Delete.Feature.Breeds.Import.Processors
{
    using Foundation.DataTemplates.Models.Taxonomy;
    using Foundation.ItemImport.Interfaces;

    using Glass.Mapper.Sc;

    using Models;

    public interface ICatGlobalBreedItemProcessor : IGlobalBreedItemProcessor<GlobalCatBreed>
    {
    }

    public class CatGlobalBreedItemProcessor : GlobalBreedItemProcessor<GlobalCatBreed>, ICatGlobalBreedItemProcessor
    {
        public CatGlobalBreedItemProcessor(
            ISitecoreContext sitecoreContext,
            ILogManagerImport logManagerImport,
            string locationPathOverride = null)
            : base(sitecoreContext, logManagerImport, Species.Cats,  locationPathOverride)
        {
        }
    }
}