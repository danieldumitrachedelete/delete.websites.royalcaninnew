using Delete.Feature.Breeds.Import.Processors;
using Delete.Feature.Breeds.Managers;
using Delete.Feature.Breeds.Models;
using Delete.Foundation.DataTemplates.Managers;
using Delete.Foundation.DataTemplates.Models.Interfaces;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.DeleteFoundationCore.Managers;
using Delete.Foundation.InitializeDependencyInjection.Extensions;
using Delete.Foundation.ItemImport.DependencyInjection;
using Delete.Foundation.ItemImport.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.DependencyInjection;

namespace Delete.Feature.Breeds.DependencyInjection
{
    public class RegisterServices : IServicesConfigurator
    {
        public void Configure(IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IBreedSearchManager, BreedSearchManager>();
            serviceCollection.AddScoped<ILocalCatBreedSearchManager, LocalCatBreedSearchManager>();
            serviceCollection.AddScoped<ILocalDogBreedSearchManager, LocalDogBreedSearchManager>();
            serviceCollection.AddScoped<IBreedSearchLocalConfigurationSearchManager, BreedSearchLocalConfigurationSearchManager>();
            serviceCollection.AddScoped<IFilterGroupFolderSearchManager, FilterGroupFolderSearchManager>();

            ConfigureImport(serviceCollection);

            serviceCollection.AddMvcControllersInCurrentAssembly();

            RegisterIndexes();
        }

        private static void RegisterIndexes()
        {
            SearchManagersConfiguration.RegisterIndexConfiguration(typeof(LocalBreed), BreedsConstants.CustomIndexAlias);
            SearchManagersConfiguration.RegisterIndexConfiguration(typeof(LocalCatBreed), BreedsConstants.CustomIndexAlias);
            SearchManagersConfiguration.RegisterIndexConfiguration(typeof(LocalDogBreed), BreedsConstants.CustomIndexAlias);
        }

        private static void ConfigureImport(IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<ICatGlobalBreedItemProcessor, CatGlobalBreedItemProcessor>();
            serviceCollection.AddScoped<IDogGlobalBreedItemProcessor, DogGlobalBreedItemProcessor>();
            CatDogDiscerningAccessorFactory<IGlobalBreedItemProcessor<GlobalBreed>, ICatGlobalBreedItemProcessor,
                IDogGlobalBreedItemProcessor>.Register(serviceCollection);
            CatDogDiscerningAccessorFactory<IGlobalBreedItemProcessor<I_GlobalBreed>, ICatGlobalBreedItemProcessor,
                IDogGlobalBreedItemProcessor>.Register(serviceCollection);
            serviceCollection.AddTransient<IBaseSearchManager<GlobalCatBreed>, BaseSearchManager<GlobalCatBreed>>();
            serviceCollection.AddTransient<IBaseSearchManager<GlobalDogBreed>, BaseSearchManager<GlobalDogBreed>>();
        }
    }
}
