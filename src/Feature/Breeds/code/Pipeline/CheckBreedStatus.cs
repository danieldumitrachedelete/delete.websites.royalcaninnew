﻿using System.Linq;
using Delete.Feature.Breeds.Models;
using Glass.Mapper.Sc;
using Sitecore;
using Sitecore.Data.Managers;
using Sitecore.Pipelines.HttpRequest;

namespace Delete.Feature.Breeds.Pipeline
{
    public class CheckBreedStatus : HttpRequestProcessor
    {
        private readonly ISitecoreContext sitecoreContext;

        public CheckBreedStatus(ISitecoreContext sitecoreContext)
        {
            this.sitecoreContext = sitecoreContext;
        }

        public override void Process(HttpRequestArgs args)
        {
            if (Context.Item == null) return;

            var template = TemplateManager.GetTemplate(Context.Item);
            var baseTemplates = template.GetBaseTemplates().Select(x => x.ID).ToList();
            if (!baseTemplates.Contains(LocalDogBreedConstants.TemplateId)
                && !baseTemplates.Contains(LocalCatBreedConstants.TemplateId)) return;

            var breed = this.sitecoreContext.GetCurrentItem<LocalBreed>();
            if (breed != null && (breed.HideBreedLocally || breed.GlobalBreed == null || breed.GlobalBreed.HideBreedGlobally))
            {
                Context.Item = null;
            }
        }
    }
}