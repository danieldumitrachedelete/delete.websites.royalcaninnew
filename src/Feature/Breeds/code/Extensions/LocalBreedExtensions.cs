﻿using Delete.Feature.Breeds.Models;
using Glass.Mapper;

namespace Delete.Feature.Breeds.Extensions
{
    public static class LocalBreedExtensions
    {
        public static string GetBreedName(this LocalBreed localBreed)
        {
            if (localBreed == null)
            {
                return string.Empty;
            }

            if (localBreed.LocalBreedName.HasValue())
            {
                return localBreed.LocalBreedName;
            }

            return localBreed.GlobalBreed?.BreedName ?? string.Empty;
        }
    }
}