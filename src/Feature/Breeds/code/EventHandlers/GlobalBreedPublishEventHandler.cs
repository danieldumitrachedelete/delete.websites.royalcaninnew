﻿using System;
using Sitecore.Publishing.Pipelines.PublishItem;

namespace Delete.Feature.Breeds.EventHandlers
{
    public class GlobalBreedPublishEventHandler : GlobalBreedEventHandlerBase
    {
        public void OnItemPublishHandler(object sender, EventArgs args)
        {
            var itemProcessedEventArgs = args as ItemProcessedEventArgs;

            var context = itemProcessedEventArgs?.Context;

            if (context == null)
            {
                return;
            }

            var database = context.PublishOptions.TargetDatabase;

            var item = database.Items.GetItem(context.ItemId);
            if (!ValidateContextItem(item))
            {
                return;
            }

            ProcessItem(item);
        }
    }
}