﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Delete.Feature.Breeds.Models;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Managers;
using Sitecore;
using Sitecore.ContentSearch;
using Sitecore.Data;
using Sitecore.Data.Items;

namespace Delete.Feature.Breeds.EventHandlers
{
    public abstract class GlobalBreedEventHandlerBase
    {
        protected bool ValidateContextItem(Item contextItem)
        {
            return contextItem != null
                   && contextItem.Name != Foundation.DeleteFoundationCore.Constants.Constants.Sitecore.StandardValuesItemName
                   && contextItem.IsDerived(GlobalBreedConstants.TemplateId);
        }

        protected void ProcessItem(Item contextItem)
        {
            var linkedItems = GetLinkedLocalBreeds(contextItem);

            RefreshIndex(linkedItems, contextItem.Database);
        }

        protected IEnumerable<Item> GetLinkedLocalBreeds(Item item)
        {
            var itemLinks = Globals.LinkDatabase.GetReferrers(item);
            if (itemLinks == null)
            {
                return Enumerable.Empty<Item>();
            }
            
            var items = new List<Item>(itemLinks.Length);
            foreach (var itemLink in itemLinks)
            {
                if (itemLink.SourceDatabaseName == item.Database.Name) 
                {
                    var linkItem = item.Database.Items[itemLink.SourceItemID];
                    if (linkItem != null && linkItem.IsDerived(LocalBreedConstants.TemplateId))
                    {
                        items.Add(linkItem);
                    }
                }
            }

            return items;
        }

        protected void RefreshIndex(IEnumerable<Item> items, Database database)
        {
            var indexNames = new[] {
                SearchManagersConfiguration.GetIndexNameByType(typeof(LocalBreed), database.Name),
                SearchManagersConfiguration.GetIndexNameByType(typeof(GlobalBreed), database.Name)  };

            var indexes = indexNames.Select(x => ContentSearchManager.GetIndex(x)).ToList();

            foreach (var searchIndex in indexes)
            {
                foreach (var item in items)
                {
                    searchIndex.RefreshAsync(new SitecoreIndexableItem(item), IndexingOptions.ForcedIndexing, CancellationToken.None);
                }
            }
        }
    }
}