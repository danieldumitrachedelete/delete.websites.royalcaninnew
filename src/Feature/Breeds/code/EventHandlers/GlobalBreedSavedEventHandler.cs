﻿using System;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Sitecore.Data.Items;
using Sitecore.Events;

namespace Delete.Feature.Breeds.EventHandlers
{
    public class GlobalBreedSavedEventHandler: GlobalBreedEventHandlerBase
    {
        private static Item GetContextItem(EventArgs args)
        {
            var eventArgs = args as SitecoreEventArgs;

            var item = eventArgs?.Parameters[0] as Item;

            return item;
        }

        public void OnItemSaved(object sender, EventArgs args)
        {
            try
            {
                var eventArgs = args as SitecoreEventArgs;

                var contextItem = eventArgs?.Parameters[0] as Item;

                if (!ValidateContextItem(contextItem))
                {
                    return;
                }

                ProcessItem(contextItem);
            }
            catch (Exception e)
            {
                this.LogError($"An error has occurred while executing the event handler. Error message: {e.GetAllMessages()}", e);
            }
        }
    }
}