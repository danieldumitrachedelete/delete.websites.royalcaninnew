﻿using System.Collections.Generic;
using System.Linq;
using Delete.Feature.Breeds.Managers;
using Delete.Feature.Breeds.Models;
using Delete.Foundation.DataTemplates.Managers;

namespace Delete.Feature.Breeds.Strategies
{
    public class DogBreedFilterSearchStrategy : BaseBreedFilterSearchStrategy
    {
        private readonly ILocalDogBreedSearchManager localDogBreedSearchManager;

        public DogBreedFilterSearchStrategy(
            ILocalDogBreedSearchManager localDogBreedSearchManager,
            IFilterGroupFolderSearchManager filterGroupFolderSearchManager)
            : base(filterGroupFolderSearchManager)
        {
            this.localDogBreedSearchManager = localDogBreedSearchManager;
        }

        public override IEnumerable<LocalBreed> FindBreeds(BreedsFilterModel filter)
        {
            var allBreads = this.localDogBreedSearchManager.FindByKeyword(filter.SearchQuery);

            allBreads = this.ApplyBaseBreedFilters(allBreads, filter).Where(x => x.GlobalBreed != null).Cast<LocalDogBreed>();

            return allBreads;
        }
    }
}