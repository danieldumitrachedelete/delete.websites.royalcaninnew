﻿using System.Collections.Generic;
using System.Linq;
using Delete.Feature.Breeds.Managers;
using Delete.Feature.Breeds.Models;
using Delete.Foundation.DataTemplates.Managers;

namespace Delete.Feature.Breeds.Strategies
{
    public class CatBreedFilterSearchStrategy : BaseBreedFilterSearchStrategy
    {
        private readonly ILocalCatBreedSearchManager localCatBreedSearchManager;

        public CatBreedFilterSearchStrategy(
            ILocalCatBreedSearchManager localCatBreedSearchManager,
            IFilterGroupFolderSearchManager filterGroupFolderSearchManager) : base(filterGroupFolderSearchManager)
        {
            this.localCatBreedSearchManager = localCatBreedSearchManager;
        }

        public override IEnumerable<LocalBreed> FindBreeds(BreedsFilterModel filter)
        {
            var allBreads = this.localCatBreedSearchManager.FindByKeyword(filter.SearchQuery);

            allBreads = this.ApplyBaseBreedFilters(allBreads, filter).Where(x => x.GlobalBreed != null).Cast<LocalCatBreed>();

            return allBreads;
        }
    }
}