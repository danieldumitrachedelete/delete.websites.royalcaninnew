﻿using System;
using System.Collections.Generic;
using System.Linq;
using Delete.Feature.Breeds.Models;
using Delete.Foundation.DataTemplates.Managers;
using Delete.Foundation.DataTemplates.Models.Filter;
using Delete.Foundation.DataTemplates.Models.Taxonomy;
using Glass.Mapper;
using Sitecore.Data;

namespace Delete.Feature.Breeds.Strategies
{
    using Foundation.DeleteFoundationCore.Extensions;

    public interface IBreedFilterSearchStrategy
    {
        IEnumerable<LocalBreed> FindBreeds(BreedsFilterModel filter);
    }

    public abstract class BaseBreedFilterSearchStrategy : IBreedFilterSearchStrategy
    {
        private readonly IFilterGroupFolderSearchManager filterGroupFolderSearchManager;

        protected BaseBreedFilterSearchStrategy(IFilterGroupFolderSearchManager filterGroupFolderSearchManager)
        {
            this.filterGroupFolderSearchManager = filterGroupFolderSearchManager;
        }

        public abstract IEnumerable<LocalBreed> FindBreeds(BreedsFilterModel filter);

        protected IEnumerable<LocalBreed> ApplyBaseBreedFilters(IEnumerable<LocalBreed> itemsToFilter, BreedsFilterModel filter)
        {
            if (itemsToFilter == null)
            {
                return new List<LocalBreed>();
            }

            var applyBaseBreedFilters = itemsToFilter.ToList();
            if (!applyBaseBreedFilters.Any() || filter == null)
            {
                return applyBaseBreedFilters;
            }

            var filters = new Dictionary<string, IEnumerable<Guid>>
            {
                { nameof(filter.Sizes), this.GetFilterItemIds(filter.Sizes) },
                { nameof(filter.Characteristics), this.GetFilterItemIds(filter.Characteristics) },
                { nameof(filter.Coats), this.GetFilterItemIds(filter.Coats) },
                { nameof(filter.EnergyLevels), this.GetFilterItemIds(filter.EnergyLevels) },
                { nameof(filter.Habitats), this.GetFilterItemIds(filter.Habitats) },
            };

            foreach (var filterGroup in filters)
            {
                var filterValues = filterGroup.Value.Where(x => x != Guid.Empty).ToList();
                if (!filterValues.Any())
                {
                    continue;
                }

                applyBaseBreedFilters = applyBaseBreedFilters.Where(x => x.RelatedEntities.Intersect(filterValues).Any()).ToList();
            }

            return applyBaseBreedFilters;
        }

        private IEnumerable<Guid> GetFilterItemIds(string filterValues)
        {
            return filterValues.ToGuidList();
        }
    }
}