﻿using System;
using System.Collections.Generic;
using System.Linq;
using Delete.Feature.Breeds.Extensions;
using Delete.Feature.Breeds.Managers;
using Delete.Foundation.DataTemplates.Models.Filter;
using Delete.Foundation.DataTemplates.Strategies;
using JetBrains.Annotations;

namespace Delete.Feature.Breeds.Strategies
{
    using Foundation.DataTemplates.Comparers;

    using Sitecore.Data.Items;

    [UsedImplicitly]
    public class BreedFilterStrategy : BaseFilterStrategy
    {
        private readonly BreedSearchManager breedSearchManager;

        public BreedFilterStrategy()
        {
            this.breedSearchManager = new BreedSearchManager();
        }

        public static IEqualityComparer<FilterItem> BreedSearchManagerComparer { get; } = new FilterItemEqualityComparer();

        public override IEnumerable<FilterItem> GetFilterItems(Guid species)
        {
            return this.breedSearchManager.GetAllLocalForSpecies(species)
                .Select(x => new FilterItem
                {
                    Value = (x.GlobalBreed?.Id ?? Guid.Empty).ToString(),
                    Text = x.GetBreedName(),
                    BreedId = x.GlobalBreed?.LSHBreedID,
                }.SetId(x.GlobalBreed?.Id ?? Guid.Empty))
                .Distinct(BreedSearchManagerComparer);
        }

        public override IEnumerable<FilterItem> GetFilterItems(Item parentItem)
        {
            return this.breedSearchManager.GetAllLocalForSearch(parentItem)
                .Select(x => new FilterItem
                {
                    Value = (x.GlobalBreed?.Id ?? Guid.Empty).ToString(),
                    Text = x.GetBreedName(),
                    BreedId = x.GlobalBreed?.LSHBreedID,
                }.SetId(x.GlobalBreed?.Id ?? Guid.Empty))
                .Distinct(BreedSearchManagerComparer);
        }
    }
}