using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using AutoMapper;
using Delete.Feature.Breeds.Managers;
using Delete.Feature.Breeds.Models;
using Delete.Feature.Breeds.Strategies;
using Delete.Foundation.DataTemplates.Comparers;
using Delete.Foundation.DataTemplates.Managers;
using Delete.Foundation.DataTemplates.Models;
using Delete.Foundation.DataTemplates.Models.Taxonomy;
using Delete.Foundation.DeleteFoundationCore.Controllers;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Helpers;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.DeleteFoundationCore.Models;
using Glass.Mapper.Sc;

namespace Delete.Feature.Breeds.Controllers
{
    using System.Web.Mvc;

    using Foundation.DataTemplates.Models.Filter;

    public class BreedsController : BaseController
    {
        private const string AllLocalBreedsCacheKey = "localbreeds_{0}";
        private const string BreedsFilterGroupsCacheKey = "breedfiltergroups_{0}";
        private const string FilteredBreedsCacheKey = "filteredbreeds_{0}";
        private const string BreedsByKeywordCacheKey = "breedsbykeyword_{0}_{1}";
        private const string MixedBreedsLinkCacheKey = "mixedbreedslink_{0}";
        private readonly IFilterGroupFolderSearchManager filterGroupFolderSearchManager;
        private readonly IBreedSearchManager breedSearchManager;
        private readonly ILocalCatBreedSearchManager localCatBreedSearchManager;
        private readonly ILocalDogBreedSearchManager localDogBreedSearchManager;

        private readonly IBreedSearchLocalConfigurationSearchManager breedSearchLocalConfigurationSearchManager;

        public BreedsController(
            IFilterGroupFolderSearchManager filterGroupFolderSearchManager,
            IBreedSearchManager breedSearchManager,
            ILocalCatBreedSearchManager localCatBreedSearchManager,
            ILocalDogBreedSearchManager localDogBreedSearchManager,
            ISitecoreContext sitecoreContext,
            IMapper mapper,
            ICacheManager cacheManager,
            IBreedSearchLocalConfigurationSearchManager breedSearchLocalConfigurationSearchManager)
            : base(sitecoreContext, mapper, cacheManager)
        {
            this.filterGroupFolderSearchManager = filterGroupFolderSearchManager;
            this.breedSearchManager = breedSearchManager;
            this.localCatBreedSearchManager = localCatBreedSearchManager;
            this.localDogBreedSearchManager = localDogBreedSearchManager;
            this.breedSearchLocalConfigurationSearchManager = breedSearchLocalConfigurationSearchManager;
        }

        private static IEqualityComparer<FilterItem> SizeComparerInstance { get; } = new FilterItemEqualityComparer();

        public ActionResult BreedTimeline()
        {
            var breed = this.GetContextGlobalBreed();
            var timeline = this.GetDataSourceItem<BreedTimelineFolder>();
            if (breed == null || timeline == null)
            {
                return new EmptyResult();
            }

            var model = new TimelineViewModel
            {
                Breed = breed,
                Ages = timeline.Ages?.ToList()
            };

            return this.View("~/Views/Feature/Breeds/BreedTimeline.cshtml", model);
        }

        [HttpGet]
        public ActionResult Search()
        {
            var breedFilterConfiguration = this.GetDataSourceItem<BreedFilterConfiguration>();
            if (breedFilterConfiguration == null)
            {
                return this.Content(Sitecore.Context.PageMode.IsExperienceEditorEditing ? "<p>Please select breed filter configuration</p>" : null);
            }

            var species = breedFilterConfiguration.Species;
            var cacheKey = string.Format(AllLocalBreedsCacheKey, species.Id);
            var localBreeds = this._cacheManager.CacheResults(()=> this.breedSearchManager.GetAllLocalForSpecies(species.Id).ToList(), cacheKey);
            var selectedFilterGroups = breedFilterConfiguration.Filters;

            var groupsCacheKey = string.Format(BreedsFilterGroupsCacheKey, species.Id);
            var allFilterGroups = this._cacheManager.CacheResults(() => this.filterGroupFolderSearchManager.GetAll(), groupsCacheKey);

            var sortedFilterGroups = selectedFilterGroups
                .Select(x => x.Id)
                .Select(filterGroupId => allFilterGroups.FirstOrDefault(x => x.Id == filterGroupId))
                .Where(g => g != null)
                .ToList();

            var pageSize = breedFilterConfiguration.PageSize;
            var maxPageCount = breedFilterConfiguration.MaxPageCount;

            var model = new SearchViewModel
            {
                FilterConfiguration = breedFilterConfiguration.Id,
                FilterModel = new SearchFilterViewModel
                {
                    Specie = species,
                    FilterGroups = sortedFilterGroups,
                    FilterRelatedSearchResults = localBreeds
                },
                SearchResultModel = new SearchResultViewModel
                {
                    PageSize = pageSize,
                    MainFilterGroup = breedFilterConfiguration.MainFilter,
                    Specie = species,
                    SearchResults = this.GroupBySpecifiedFilter(localBreeds, species)
                        .Select(x =>
                            new SearchResultsGroupViewModel
                            {
                                Key = x.Key,
                                Items = x.Take(pageSize * maxPageCount).ToList(),
                                ItemsMaxCount = x.Count(),
                            })
                        .OrderBy(x => x.Key.Sortorder)
                }
            };

            return this.View("~/Views/Feature/Breeds/Search.cshtml", model);
        }

        [HttpGet]
        public ActionResult SearchBreeds(BreedsFilterViewModel filterConfiguration)
        {
            var breedFilterConfiguration = this.SitecoreContext.GetItem<BreedFilterConfiguration>(filterConfiguration.Filter, false, true);
            var specie = breedFilterConfiguration.Species;
            var strategy = this.GetSpeciesRelatedFilterStrategy(specie);

            var filterModel = new BreedsFilterModel(filterConfiguration)
            {
                Species = breedFilterConfiguration.Species.Id
            };
            var cacheKey = string.Format(FilteredBreedsCacheKey, string.Join("|", filterModel.Values));
            var allBreads = this._cacheManager.CacheResults(() => strategy.FindBreeds(filterModel), cacheKey);
            var foundBreeds = this.GroupBySpecifiedFilter(allBreads, specie)
                .Select(x =>
                    new SearchResultsGroupViewModel
                    {
                        Key = x.Key,
                        Items = filterConfiguration.ViewAll ? x.ToList() : x.Take(breedFilterConfiguration.PageSize * breedFilterConfiguration.MaxPageCount).ToList(),
                        ItemsMaxCount = x.Count(),
                    })
                .OrderBy(x => x.Key.Sortorder);

            var searchResultModel = new SearchResultViewModel
            {
                PageSize = breedFilterConfiguration.PageSize,
                MainFilterGroup = breedFilterConfiguration.MainFilter,
                Specie = specie,
                SearchResults = foundBreeds
            };

            var view = filterConfiguration.ViewAll || foundBreeds.Count() == 1
                           ? "~/Views/Feature/Breeds/_AllBreedSearchResults.cshtml"
                           : "~/Views/Feature/Breeds/_BreedSearchResults.cshtml";
            return this.View(view, searchResultModel);
        }

        public ActionResult SizeTable()
        {
            var breed = this.GetContextGlobalBreed();
            if (breed != null)
            {
                return this.View("~/Views/Feature/Breeds/SizeTable.cshtml", breed);
            }

            return new EmptyResult();
        }

        // http://royalcanin.deletedev.com/en-us/api/breeds/FindByKeyword?species={42B60B8C-D080-477D-BB15-B5F8DDBFFE3A}
        [HttpGet]
        public ActionResult FindByKeyword(Guid? species, string keyword = "")
        {
            var localBreedJson = this.GetPredictiveSearchResults(species, keyword);

            return this.Json(localBreedJson, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ContentSearchPanel()
        {
            var contentSearchPanel = this.GetDataSourceItem<ContentSearchPanel>();
            return this.View("~/Views/Feature/Breeds/ContentSearchPanel.cshtml", contentSearchPanel);
        }

        public ActionResult MixedBreedBlock()
        {
            var mixedBreedBlock = this.GetDataSourceItem<MixedBreedBlock>();
            return this.View("~/Views/Feature/Breeds/MixedBreedBlock.cshtml", mixedBreedBlock);
        }

        public ActionResult BreedHeroPanel()
        {
            var model = new BreedHeroModel();
            var globalBreed = this.GetContextGlobalBreed();
            if (globalBreed != null)
            {
                model.GlobalBreed = globalBreed;
                var localBreed = this.GetContextItem<LocalBreed>();
                if (localBreed.BaseTemplates.Contains(LocalCatBreedConstants.TemplateId.Guid)
                    || localBreed.BaseTemplates.Contains(LocalDogBreedConstants.TemplateId.Guid))
                {
                    model.LocalBreed = localBreed;
                }

                return this.View("~/Views/Feature/Breeds/BreedHeroPanel.cshtml", model);
            }

            return new EmptyResult();
        }

        public ActionResult AboutBreedPanel()
        {
            var globalBreed = this.GetContextGlobalBreed();
            if (globalBreed != null)
            {
                return this.View("~/Views/Feature/Breeds/AboutBreedPanel.cshtml", globalBreed);
            }

            return new EmptyResult();
        }

        public ActionResult BreedOriginPanel()
        {
            var globalBreed = this.GetContextGlobalBreed();
            if (globalBreed != null)
            {
                return this.View("~/Views/Feature/Breeds/BreedOriginPanel.cshtml", globalBreed);
            }

            return new EmptyResult();
        }

        protected GlobalBreed GetContextGlobalBreed()
        {
            var contextItem = this.GetContextItem<GlassBase>(inferType: true);
            var globalBreed = contextItem as GlobalBreed;
            if (globalBreed == null 
                && (contextItem.BaseTemplates.Contains(LocalCatBreedConstants.TemplateId.Guid) 
                    || contextItem.BaseTemplates.Contains(LocalDogBreedConstants.TemplateId.Guid)))
            {
                var localBreed = this.GetContextItem<LocalBreed>();
                if (localBreed?.GlobalBreed != null)
                {
                    globalBreed = localBreed.GlobalBreed;
                }
            }

            var stripHtml = globalBreed.BreedTimelineText.StripHtml();

            if (stripHtml.IsNullOrEmpty()) globalBreed.BreedTimelineText = null;
            
            return globalBreed;
        }

        protected PredictiveSearchJsonModel GetPredictiveSearchResults(Guid? species, string keyword = "")
        {
            if (!species.HasValue || species == Guid.Empty)
            {
                species = SpecieConstants.DogItemId;
            }

            IEnumerable<LocalBreed> breeds;
            var cacheKey = string.Format(BreedsByKeywordCacheKey, species.Value, keyword);
            if (species.Value == SpecieConstants.DogItemId)
            {
                breeds = this._cacheManager.CacheResults(()=> this.localDogBreedSearchManager.FindByKeyword(keyword), cacheKey);
            }
            else
            {
                breeds = this._cacheManager.CacheResults(() => this.localCatBreedSearchManager.FindByKeyword(keyword), cacheKey);
            }

            var predictiveItems = this._mapper.Map<IEnumerable<PredictiveSearchItemJsonModel>>(breeds);

            var mixedBreedsCacheKey = string.Format(MixedBreedsLinkCacheKey, species.Value);
            return new PredictiveSearchJsonModel
            {
                Items = predictiveItems,
                FeaturedItems = this._cacheManager.CacheResults(()=> this.GetMixedBreedLinkJsonModel(species.Value), mixedBreedsCacheKey)
            };
        }

        protected IEnumerable<PredictiveSearchItemJsonModel> GetMixedBreedLinkJsonModel(Guid species)
        {
            var breedSearchConfiguration = this.breedSearchLocalConfigurationSearchManager.GetAll().FirstOrDefault();

            var mixedBreedPageUrl = species == SpecieConstants.DogItemId
                ? breedSearchConfiguration?.MixedBreedPageDogs?.Url
                : breedSearchConfiguration?.MixedBreedPageCats?.Url;

            if (mixedBreedPageUrl.Empty())
            {
                return Enumerable.Empty<PredictiveSearchItemJsonModel>();
            }

            return new List<PredictiveSearchItemJsonModel>()
            {
                new PredictiveSearchItemJsonModel
                    {
                        Title = TranslateHelper.TextByDomainCached(
                            "Translations",
                            "translations.feature.breeds.mixedbreed"),
                        Url = mixedBreedPageUrl
                }
            };
        }

        private static string GetBreedValueForSorting(LocalBreed breed)
        {
            return breed.LocalBreedName.IsNullOrEmpty() ? breed.GlobalBreed?.BreedName : breed.LocalBreedName;
        }

        private IEnumerable<IGrouping<FilterItem, LocalBreed>> GroupBySpecifiedFilter(
            IEnumerable<LocalBreed> breeds,
            Specie species)
        {
            return species.Id == SpecieConstants.DogItemId
                ? breeds.Where(x => x.GlobalBreed?.Size != null).OrderBy(GetBreedValueForSorting).GroupBy(
                    x => x.GlobalBreed.Size,
                    SizeComparerInstance)
                : breeds.Where(x => (x as LocalCatBreed)?.GlobalCatBreed?.Coat != null).OrderBy(GetBreedValueForSorting).GroupBy(
                    x => (x as LocalCatBreed)?.GlobalCatBreed?.Coat,
                    SizeComparerInstance);
        }

        private IBreedFilterSearchStrategy GetSpeciesRelatedFilterStrategy(Specie species)
        {
            if (species.Id == SpecieConstants.DogItemId)
            {
                return new DogBreedFilterSearchStrategy(this.localDogBreedSearchManager, this.filterGroupFolderSearchManager);
            }

            if (species.Id == SpecieConstants.CatItemId)
            {
                return new CatBreedFilterSearchStrategy(this.localCatBreedSearchManager, this.filterGroupFolderSearchManager);
            }

            throw new ArgumentException($"specie {species.Id} not found");
        }
    }
}
