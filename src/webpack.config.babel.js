import merge from 'webpack-merge';
import baseConfig from './webpack/webpack.base.js';
import devConfig from './webpack/webpack.dev.js';
import prodConfig from './webpack/webpack.prod.js';
import serverConfig from './webpack/webpack.server.js';

export default (args) => {
    const isProd = !!args && !!args.prod;
    const isServer = !!args && !!args.server;

    if (isProd) {
        return merge.smart(baseConfig(args), prodConfig(args));
    } else if (isServer) {
        return merge.smart(baseConfig(args), serverConfig(args));
    }
    return merge.smart(baseConfig(args), devConfig(args));
};
