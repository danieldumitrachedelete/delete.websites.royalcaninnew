﻿using System;
using System.Collections.Generic;
using System.Linq;
using Delete.Foundation.DataTemplates.Models.Filter;
using Delete.Foundation.DataTemplates.Models.Folder;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.DeleteFoundationCore.Managers;
using Sitecore.Data;

namespace Delete.Foundation.DataTemplates.Managers
{
    public interface IFilterGroupFolderSearchManager : IBaseSearchManager<FilterGroupFolder>
    {
        IEnumerable<FilterGroupFolder> GetAll();

        IEnumerable<FilterItem> FindFilterItems(string[] values, ID templateId, Guid specie);
    }

    public class FilterGroupFolderSearchManager : BaseSearchManager<FilterGroupFolder>, IFilterGroupFolderSearchManager
    {
        public IEnumerable<FilterGroupFolder> GetAll()
        {
            var filtered = this.GetQueryable();

            return filtered.MapResults(this.SitecoreContext, true).ToList();
        }

        public IEnumerable<FilterItem> FindFilterItems(string[] values, ID templateId, Guid specie)
        {
            var items = this.GetAll();

            var result = new List<FilterItem>();
            foreach (var filterGroupFolder in items)
            {
                var folderItems = this.FindFilterItemsByName(filterGroupFolder.FilterItems, values, templateId).ToList();
                if (folderItems.Any())
                {
                    result.AddRange(folderItems);
                }

                var specieRelatedFolder = filterGroupFolder.SpecieFolders.FirstOrDefault(x => x.Specie == specie);
                if (specieRelatedFolder == null)
                {
                    continue;
                }

                folderItems = this.FindFilterItemsByName(specieRelatedFolder.FilterItems, values, templateId).ToList();
                if (!folderItems.Any())
                {
                    continue;
                }
                
                result.AddRange(folderItems);
            }

            return result;
        }

        protected override IQueryable<FilterGroupFolder> GetQueryable()
        {
            return base.GetQueryable().Where(x => x.TemplateId == FilterGroupFolderConstants.TemplateId);
        }

        private IEnumerable<FilterItem> FindFilterItemsByName(IEnumerable<FilterItem> items, string[] values, ID templateId)
        {
            return items?.Where(x => x.TemplateId == templateId)
                       .Where(x => values.Contains(x.Value)).ToList() ?? new List<FilterItem>();
        }
    }
}