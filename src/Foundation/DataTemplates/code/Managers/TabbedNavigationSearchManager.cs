﻿namespace Delete.Foundation.DataTemplates.Managers
{
    using System.Collections.Generic;
    using System.Linq;

    using Delete.Foundation.DataTemplates.Models.Interfaces;
    using Delete.Foundation.DeleteFoundationCore.Extensions;
    using Delete.Foundation.DeleteFoundationCore.Interfaces;
    using Delete.Foundation.DeleteFoundationCore.Managers;

    using Sitecore.Data;

    public interface ITabbedNavigationSearchManager : IBaseSearchManager<_TabbedNavigationItem>
    {
        IList<_TabbedNavigationItem> FindChildren(ID parent);
    }

    public class TabbedNavigationSearchManager : BaseSearchManager<_TabbedNavigationItem>, ITabbedNavigationSearchManager
    {
        public IList<_TabbedNavigationItem> FindChildren(ID parent)
        {
            return base.GetQueryable()
                .Where(x => x.ParentId == parent && x.ShowInTabs).ToList().GroupBy(x => x.Id).Select(y => y.Last())
                .MapResults(this.SitecoreContext).SitecoreSort().ToList();
        }
    }
}