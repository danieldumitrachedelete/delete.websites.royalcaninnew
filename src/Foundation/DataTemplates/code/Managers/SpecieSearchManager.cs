﻿using System.Collections.Generic;
using System.Linq;
using Delete.Foundation.DataTemplates.Models.Taxonomy;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.DeleteFoundationCore.Managers;

namespace Delete.Foundation.DataTemplates.Managers
{
    public interface ISpecieSearchManager : IBaseSearchManager<Specie>
    {
        IEnumerable<Specie> GetAll();
    }

    public class SpecieSearchManager : BaseSearchManager<Specie>, ISpecieSearchManager
    {
        public IEnumerable<Specie> GetAll()
        {
            return GetQueryable().MapResults(SitecoreContext, true, false);
        }

        protected override IQueryable<Specie> GetQueryable()
        {
            return base.GetQueryable().Where(x => x.TemplateId == SpecieConstants.TemplateId);
        }
    }
}