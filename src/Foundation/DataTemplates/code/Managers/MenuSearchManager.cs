﻿using System.Collections.Generic;
using System.Linq;
using Delete.Foundation.DataTemplates.Models.Menu;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.DeleteFoundationCore.Managers;
using Delete.Foundation.DeleteFoundationCore.Models;

namespace Delete.Foundation.DataTemplates.Managers
{
    public interface IMenuSearchManager : IBaseSearchManager<MenuFolder>
    {
        IEnumerable<MenuFolder> GetAllForLocal();

        MenuFolder GetHeaderFolderForLocal();

        MenuFolder GetFooterFolderForLocal();

        MenuFolder GetSectionalNavigationForLocal(string sectionName);
    }

    public class MenuSearchManager : BaseSearchManager<MenuFolder>, IMenuSearchManager
    {
        public IEnumerable<MenuFolder> GetAllForLocal()
        {
            var rootItem = this.SitecoreContext.GetRootItemRelative<IGlassBase>();
            return this.GetQueryable()
                .Where(x => x.Fullpath.StartsWith(rootItem.Fullpath))
                .MapResults(this.SitecoreContext, true, false);
        }

        public MenuFolder GetHeaderFolderForLocal()
        {
            return this.GetAllForLocal().FirstOrDefault(x => x.Name.Equals("Header"));
        }

        public MenuFolder GetFooterFolderForLocal()
        {
            return this.GetAllForLocal().FirstOrDefault(x => x.Name.Equals("Footer"));
        }

        /// <summary>
        /// gets MenuFolder with the specified name
        /// </summary>
        /// <param name="sectionName">name of the Menu Folder item</param>
        /// <returns></returns>
        public MenuFolder GetSectionalNavigationForLocal(string sectionName)
        {
            return this.GetAllForLocal().FirstOrDefault(x => x.Name.Equals(sectionName));
        }

        protected override IQueryable<MenuFolder> GetQueryable()
        {
            return base.GetQueryable().Where(x => x.TemplateId == MenuFolderConstants.TemplateId);
        }
    }
}