﻿using System;
using Delete.Foundation.DeleteFoundationCore.SchemaOrg;
using Glass.Mapper;
using Sitecore.ContentSearch.Linq;
using Sitecore.ContentSearch.Linq.Utilities;
using Sitecore.Data.Query;
using SolrNet.Mapping.Validation;

namespace Delete.Foundation.DataTemplates.Managers
{
    using System.Collections.Generic;
    using System.Linq;

    using Delete.Foundation.DataTemplates.Models.Interfaces;
    using Delete.Foundation.DeleteFoundationCore.Extensions;
    using Delete.Foundation.DeleteFoundationCore.Interfaces;
    using Delete.Foundation.DeleteFoundationCore.Managers;

    using Sitecore.Data;

    public interface I_StockistSearchManager : IBaseSearchManager<_Stockist>
    {
        List<_Stockist> FindStockistsByPillarAndType(Guid pillarId, Guid contactTypeId, Guid stockistTemplateId, 
            Guid? stockistsFolderId = null);
    }

    public class _StockistSearchManager : BaseSearchManager<_Stockist>, I_StockistSearchManager
    {
        public List<_Stockist> FindStockistsByPillarAndType(Guid pillarId, Guid contactTypeId, Guid stockistTemplateId,
            Guid? stockistsFolder = null)
        {

            var predicate = PredicateBuilder.True<_Stockist>();
            predicate = predicate.And(x => x.SearchableRelatedEntities.Contains(pillarId) && x.BaseTemplates.Contains(stockistTemplateId) && x.SearchableRelatedEntities.Contains(contactTypeId));
            
            if (stockistsFolder.HasValue)
            {
                var idstockistsFolder = new ID(stockistsFolder.Value);
                predicate = predicate.And(x => x.Paths.Contains(idstockistsFolder));
            }
            
            var query = base.GetQueryable().Where(predicate);

            var result = query.GetResults().Select(x => x.Document);

            result = result.OrderByDescending(x => x.Quality).ThenBy(x => x.NameString.ToUpper());

            return result.MapResults(this.SitecoreContext, true).ToList();
            
        }

        
    }
}