﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Delete.Foundation.DataTemplates.Models.Dictionary;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.DeleteFoundationCore.Managers;
using Delete.Foundation.DeleteFoundationCore.Models;
using Sitecore.Data;
using Sitecore.Data.Items;

namespace Delete.Foundation.DataTemplates.Managers
{
    public interface IDictionaryEntryManager : IBaseSearchManager<DictionaryEntry>
    {
        IEnumerable<DictionaryEntry> GetAll();

        IEnumerable<DictionaryEntry> GetAllInFolder(Guid folderGuid);
    }

    public class DictionaryEntryManager : BaseSearchManager<DictionaryEntry>, IDictionaryEntryManager
    {
        public IEnumerable<DictionaryEntry> GetAll()
        {
            return GetQueryable().MapResults(SitecoreContext, true, false);
        }

        public IEnumerable<DictionaryEntry> GetAllInFolder(Guid folderGuid)
        {
            var folder = SitecoreContext.GetItem<GlassBase>(folderGuid);
            return GetQueryable()
                .Where(x => x.Fullpath.StartsWith(folder.Fullpath))
                .MapResults(SitecoreContext, true, false);
        }

        protected override IQueryable<DictionaryEntry> GetQueryable()
        {
            return base.GetQueryable().Where(x => x.TemplateId == DictionaryEntryConstants.TemplateId);
        }
    }
}