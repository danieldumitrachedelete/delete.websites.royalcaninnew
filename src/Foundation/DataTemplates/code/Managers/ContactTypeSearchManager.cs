﻿using System.Collections.Generic;
using System.Linq;
using Delete.Foundation.DataTemplates.Models.Taxonomy;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.DeleteFoundationCore.Managers;

namespace Delete.Foundation.DataTemplates.Managers
{
    using System;

    public interface IContactTypeSearchManager : IBaseSearchManager<ContactType>
    {
        IEnumerable<ContactType> GetAll();

        ContactType GetByValue(string value);
    }

    public class ContactTypeSearchManager : BaseSearchManager<ContactType>, IContactTypeSearchManager
    {
        public IEnumerable<ContactType> GetAll()
        {
            return this.GetQueryable().MapResults(this.SitecoreContext, true, false);
        }

        public ContactType GetByValue(string value)
        {
            var result = this.GetQueryable().FirstOrDefault(x => x.Value.Equals(value, StringComparison.InvariantCultureIgnoreCase));
            return result == null ? null : this.SitecoreContext.MapResult(result, true);
        }


        protected override IQueryable<ContactType> GetQueryable()
        {
            return base.GetQueryable().Where(x => x.TemplateId == ContactTypeConstants.TemplateId);
        }
    }
}