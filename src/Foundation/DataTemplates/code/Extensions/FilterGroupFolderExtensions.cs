﻿using System.Collections.Generic;
using System.Linq;
using Delete.Foundation.DataTemplates.Interfaces;
using Delete.Foundation.DataTemplates.Models.Filter;

namespace Delete.Foundation.DataTemplates.Extensions
{
    public static class FilterGroupFolderExtensions
    {
        public static IEnumerable<FilterItem> OnlyExistingInSearchResults(this IEnumerable<FilterItem> source, IEnumerable<IFilterRelatedEntities> searchResults)
        {
            if (source == null || !source.Any()) return source;

            var distinctSearchResults = searchResults.SelectMany(x => x.RelatedEntities).Distinct();

            return source.Where(x => distinctSearchResults.Contains(x.Id));
        }
    }
}