﻿namespace Delete.Foundation.DataTemplates.Extensions
{
    using System.Collections.Generic;
    using System.Linq;

    using DeleteFoundationCore.Models;

    using Models.Interfaces;

    public static class SearchResultItemsWithHighlightsExtenstions
    {
        public static IEnumerable<T> ToSearchableContent<T>(this SearchResultItemsWithHighlights<T> items)
            where T : _SearchableContent
        {
            foreach (var itemsResult in items.Results)
            {
                itemsResult.Highlights = items.Highlights
                    .Where(x => x.Key.Contains(itemsResult.Id.ToString().ToLowerInvariant()))
                    .SelectMany(x => x.Value.Values.SelectMany(v => v)).ToList();
            }

            return items.Results;
        }
    }
}