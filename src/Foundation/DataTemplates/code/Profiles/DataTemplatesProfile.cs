using AutoMapper;

namespace Delete.Foundation.DataTemplates.Profiles
{
    using DeleteFoundationCore.Extensions;

    using Models;
    using Models.Interfaces;

    public class DataTemplatesProfile : Profile
    {
        public DataTemplatesProfile()
        {
            this.CreateMap<_SearchableContent, PredictiveSearchItemJsonModel>()
                .ForMember(x => x.Title, opt => opt.MapFrom(z => z.DisplayName.NotEmpty() ? z.DisplayName : z.Name))
                .ForMember(x => x.Url, opt => opt.MapFrom(z => z.Url))
                .ForAllOtherMembers(x => x.Ignore())
                ;
        }
    }
}
