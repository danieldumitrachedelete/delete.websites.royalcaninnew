﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Delete.Foundation.DataTemplates.Models.Filter;

namespace Delete.Foundation.DataTemplates.Strategies
{
    using Sitecore.Data.Items;

    public interface IBaseFilterStrategy
    {
        IEnumerable<FilterItem> GetFilterItems(Guid species);

        IEnumerable<FilterItem> GetFilterItems(Item parentItem);
    }

    public abstract class BaseFilterStrategy : IBaseFilterStrategy
    {
        public abstract IEnumerable<FilterItem> GetFilterItems(Guid species);

        public abstract IEnumerable<FilterItem> GetFilterItems(Item parentItem);
    }
}