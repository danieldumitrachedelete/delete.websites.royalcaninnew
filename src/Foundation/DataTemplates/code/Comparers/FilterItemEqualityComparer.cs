﻿using System;
using System.Collections.Generic;
using Delete.Foundation.DataTemplates.Models.Filter;

namespace Delete.Foundation.DataTemplates.Comparers
{
    public sealed class FilterItemEqualityComparer : IEqualityComparer<FilterItem>
    {
        public bool Equals(FilterItem x, FilterItem y)
        {
            if (ReferenceEquals(x, y))
            {
                return true;
            }

            if (ReferenceEquals(x, null))
            {
                return false;
            }

            if (ReferenceEquals(y, null))
            {
                return false;
            }

            if (x.GetType() != y.GetType())
            {
                return false;
            }

            return x.Value.Equals(y.Value, StringComparison.InvariantCultureIgnoreCase);
        }

        public int GetHashCode(FilterItem obj)
        {
            return StringComparer.InvariantCultureIgnoreCase.GetHashCode(obj.Value);
        }
    }
}