﻿using System;
using System.Collections.Generic;
using Delete.Foundation.DataTemplates.Models.Dictionary;
using Delete.Foundation.DataTemplates.Models.Filter;
using Delete.Foundation.DeleteFoundationCore.ComputedFields;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Sitecore.ContentSearch.Diagnostics;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Sites;

namespace Delete.Foundation.DataTemplates.ComputedFields
{
    public class BaseDictionaryTextComputedField : BaseComputedField
    {
        protected object GetDictionaryText(Item indexedItem, string fieldName)
        {
            try
            {
                var dictionaryItems = indexedItem.Fields[fieldName].Value;
                if (!dictionaryItems.NotEmpty())
                {
                    return null;
                }

                using (new SiteContextSwitcher(SiteContext.GetSite(DefaultWebsite)))
                {
                    var dictionaryItemIds = dictionaryItems.Split(new[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
                    var dictionaryTexts = new List<string>();
                    foreach (var dictionaryItemId in dictionaryItemIds)
                    {
                        var dictionaryItem = indexedItem.Database.GetItem(new ID(dictionaryItemId), indexedItem.Language);

                        if (dictionaryItem == null)
                        {
                            continue;
                        }

                        var dictionaryText = dictionaryItem.Fields[DictionaryEntryConstants.TextFieldName].Value;
                        if (dictionaryText.NotEmpty())
                        {
                            dictionaryTexts.Add(dictionaryText);
                        }

                        var fitlerLabel = dictionaryItem.Fields[FilterItemConstants.FilterLabelFieldName].Value;
                        if (fitlerLabel.NotEmpty())
                        {
                            dictionaryTexts.Add(fitlerLabel);
                        }
                    }

                    return dictionaryTexts.Count > 0 ? string.Join(", ", dictionaryTexts) : null;
                }
            }
            catch (Exception e)
            {
                CrawlingLog.Log.Warn($"Unexpected error when computing {GetType()} index field value for item [ID:{indexedItem.ID}]. Error message: {e.Message}");
                return null;
            }
        }
    }
}