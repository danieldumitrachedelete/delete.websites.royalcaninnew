﻿using System;
using System.Collections.Generic;
using System.Linq;
using Delete.Foundation.DeleteFoundationCore;
using Delete.Foundation.DeleteFoundationCore.ComputedFields;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using JetBrains.Annotations;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;
using Sitecore.ContentSearch.Diagnostics;

namespace Delete.Foundation.DataTemplates.ComputedFields
{
    using Glass.Mapper;

    using Models.Dictionary;
    using Models.Filter;
    using Models.Folder;

    using Strategies;

    [UsedImplicitly]
    public class FilterGroupFolderItemValuesComputedField : BaseComputedField, IComputedIndexField
    {
        public string FieldName { get; set; }

        public string ReturnType { get; set; }

        public object ComputeFieldValue(IIndexable indexable)
        {
            Guard.ArgumentNotNull(indexable, nameof(indexable));

            var indexedItem = this.GetItem(indexable, FilterGroupFolderConstants.TemplateId);
            if (indexedItem == null)
            {
                return null;
            }

            try
            {
                var result = new List<string>();

                try
                {
                    var children = indexedItem
                        .GetChildren()
                        .Where(x => x.IsDerived(FilterItemConstants.TemplateId))
                        .Select(x => x[DictionaryEntryConstants.ValueFieldName])
                        .ToList();
                    result.AddRange(children);

                    var filterStrategy = indexedItem[FilterGroupFolderConstants.FilterStrategyFieldName];
                    if (filterStrategy.HasValue())
                    {
                        var filterStrategyType = Type.GetType(filterStrategy);
                        if (filterStrategyType != null)
                        {
                            var strategy = (BaseFilterStrategy)Activator.CreateInstance(filterStrategyType);
                            result.AddRange(strategy.GetFilterItems(indexedItem).Select(x => x.Value));
                        }
                    }

                    var specieFolders = indexedItem
                        .GetChildren()
                        .Where(x => x.TemplateID == SpecieRelatedFolderConstants.TemplateId)
                        .SelectMany(x => x.GetChildren().Where(i => i.IsDerived(FilterItemConstants.TemplateId)))
                        .Select(x => x[DictionaryEntryConstants.ValueFieldName])
                        .ToList();

                    if (specieFolders.Any())
                    {
                        result.AddRange(specieFolders);
                    }
                }
                catch (Exception e)
                {
                    CrawlingLog.Log.Warn($"Unexpected error when computing {this.GetType()} index field value for item [ID:{indexedItem.ID}]. Error message: {e.Message}");
                }

                return result;
            }
            catch (Exception e)
            {
                CrawlingLog.Log.Warn($"Unexpected error when computing {this.GetType()} index field value for item [ID:{indexedItem.ID}]. Error message: {e.Message}");

                return null;
            }
        }
    }
}