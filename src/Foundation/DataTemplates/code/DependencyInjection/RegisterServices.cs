using Delete.Foundation.DataTemplates.Managers;
using Delete.Foundation.InitializeDependencyInjection.Extensions;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.DependencyInjection;

namespace Delete.Foundation.DataTemplates.DependencyInjection
{
    public class RegisterServices : IServicesConfigurator
    {
        public void Configure(IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IMenuSearchManager, MenuSearchManager>();
            serviceCollection.AddScoped<ISpecieSearchManager, SpecieSearchManager>();
            serviceCollection.AddScoped<IFilterGroupFolderSearchManager, FilterGroupFolderSearchManager>();
            serviceCollection.AddScoped<IDictionaryEntryManager, DictionaryEntryManager>();
            serviceCollection.AddScoped<IContactTypeSearchManager, ContactTypeSearchManager>();
            serviceCollection.AddScoped<ITabbedNavigationSearchManager, TabbedNavigationSearchManager>();
            serviceCollection.AddScoped<I_StockistSearchManager, _StockistSearchManager>();

            serviceCollection.AddMvcControllersInCurrentAssembly();
        }
    }
}
