﻿using System;
using System.Collections.Generic;

namespace Delete.Foundation.DataTemplates.Interfaces
{
    public interface IFilterRelatedEntities
    {
        IEnumerable<Guid> RelatedEntities { get; }
    }
}