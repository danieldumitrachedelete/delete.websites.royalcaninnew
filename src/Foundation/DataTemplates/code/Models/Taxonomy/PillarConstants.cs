﻿using System;
using Sitecore.Data;

namespace Delete.Foundation.DataTemplates.Models.Taxonomy
{
    public static partial class PillarConstants
    {
        public static readonly Guid PosItemGuid = new ID("{352E1675-3AD8-40EB-98DC-F9D6AD8C2B5A}").Guid;

        public static readonly Guid VetItemGuid = new ID("{41511285-8626-4E4D-94B8-EFCC8EBC8E39}").Guid;

        public static string SeoPillarStringByGuid(Guid itemId)
        {
            if (itemId == PosItemGuid) return "Retail";
            if (itemId == VetItemGuid) return "Vet";

            return string.Empty;
        }

        public static readonly string PosPillarItemPath = Sitecore.Configuration.Settings.GetSetting(
            "Products.PosPillarItemPath",
            "/sitecore/content/Global Configuration/Taxonomy/Pillars/POS");

        public static readonly string VetPillarItemPath = Sitecore.Configuration.Settings.GetSetting(
            "Products.VetPillarItemPath",
            "/sitecore/content/Global Configuration/Taxonomy/Pillars/VET");
    }
}