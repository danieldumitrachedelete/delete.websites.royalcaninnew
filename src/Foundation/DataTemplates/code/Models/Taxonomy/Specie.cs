﻿using System;
using Sitecore.Data;

namespace Delete.Foundation.DataTemplates.Models.Taxonomy
{
    public enum Species
    {
        Dogs = 1,
        Cats = 2,
    }

    public static partial class SpecieConstants
    {
        public static readonly Guid CatItemId = new ID("{B60472DA-918E-4F5E-8082-FD1D51C07762}").Guid;

        public static readonly Guid DogItemId = new ID("{42B60B8C-D080-477D-BB15-B5F8DDBFFE3A}").Guid;

        public static Guid SpecieGuid(Species species)
        {
            switch (species)
            {
                case Species.Cats:
                    return CatItemId;
                case Species.Dogs:
                default:
                    return DogItemId;
            }
        }

        public static string GetSeoString(Species? species)
        {
            switch (species)
            {
                case Species.Cats:
                    return "Cat";
                case Species.Dogs:
                    return "Dog";
                default:
                    return string.Empty;
            }
        }

        public static Species? SpeciesByGuid(Guid itemId)
        {
            if (itemId == CatItemId) return Species.Cats;
            if (itemId == DogItemId) return Species.Dogs;

            return null;
        }
    }
}
