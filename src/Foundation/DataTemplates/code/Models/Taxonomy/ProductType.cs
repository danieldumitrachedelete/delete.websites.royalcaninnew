﻿using Sitecore.Data;

namespace Delete.Foundation.DataTemplates.Models.Taxonomy
{
    public static partial class ProductTypeConstants
    {
        public static readonly ID WetProductTypeId = new ID("{7b7b75f8-da33-454a-9fcb-de56836cac56}");

        public static readonly ID DryProductTypeId = new ID("{4ea8fd96-e88c-417f-abb8-416b87332782}");
    }
}
