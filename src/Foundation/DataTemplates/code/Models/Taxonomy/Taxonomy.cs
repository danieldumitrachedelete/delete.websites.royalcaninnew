﻿using System;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration.Attributes;
using Sitecore.ContentSearch;

namespace Delete.Foundation.DataTemplates.Models.Taxonomy
{
    using System.ComponentModel;

    using Sitecore.ContentSearch.Converters;

    public partial class Taxonomy
    {
        [SitecoreField(FieldId = TaxonomyConstants.BreedsFieldId, FieldName = TaxonomyConstants.BreedsFieldName)]
        [IndexField(TaxonomyConstants.BreedsFieldName)]
        public virtual List<Guid> BreedIds { get; set; }

        [SitecoreField(FieldId = TaxonomyConstants.ContentTagsFieldId, FieldName = TaxonomyConstants.ContentTagsFieldName)]
        [IndexField(TaxonomyConstants.ContentTagsFieldName)]
        public virtual List<Guid> ContentTagIds { get; set; }

        [SitecoreField(FieldId = TaxonomyConstants.LifestagesFieldId, FieldName = TaxonomyConstants.LifestagesFieldName)]
        [IndexField(TaxonomyConstants.LifestagesFieldName)]
        public virtual List<Guid> LifestageIds { get; set; }

        [SitecoreField(FieldId = TaxonomyConstants.SizesFieldId, FieldName = TaxonomyConstants.SizesFieldName)]
        [IndexField(TaxonomyConstants.SizesFieldName)]
        public virtual List<Guid> SizeIds { get; set; }

        [SitecoreField(FieldId = TaxonomyConstants.SpeciesFieldId, FieldName = TaxonomyConstants.SpeciesFieldName)]
        [IndexField(TaxonomyConstants.SpeciesFieldName)]
        [TypeConverter(typeof(IndexFieldGuidValueConverter))]
        public virtual List<Guid> SpecieIds { get; set; }
    }
}
