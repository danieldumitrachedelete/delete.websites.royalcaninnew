﻿using Sitecore.ContentSearch;
using SolrNet.Attributes;

namespace Delete.Foundation.DataTemplates.Models.Interfaces
{
    using System.Collections.Generic;

    using DeleteFoundationCore.Models;

    using Glass.Mapper.Sc.Configuration.Attributes;

    public partial class _SearchableContentConstants
    {
        public const string SearchableContentFieldName = "searchableContent";

        public const string SearchableTaxonomyFieldName = "searchableTaxonomy";

        public const string IsSearchableItemFieldName = "isSearchableItem";

        public const string IsVisibleItemFieldName = "isVisibleItem";
    }

    public partial class _SearchableContent
    {
        [IndexField(_SearchableContentConstants.SearchableContentFieldName)]
        [SitecoreIgnore]
        public string SearchableContent { get; set; }
        [IndexField(_SearchableContentConstants.SearchableTaxonomyFieldName)]
        [SitecoreIgnore]
        public string SearchableTaxonomy { get; set; }

        [IndexField(_SearchableContentConstants.IsSearchableItemFieldName)]
        [SitecoreIgnore]
        public bool IsSearchableItem { get; set; }

        [IndexField(_SearchableContentConstants.IsVisibleItemFieldName)]
        [SitecoreIgnore]
        public bool IsVisibleItem { get; set; }

        public List<string> Highlights { get; set; }

        public List<GlassBase> Parents { get; set; }

        [SolrField("score")]
        public object Score { get; set; }
    }
}
