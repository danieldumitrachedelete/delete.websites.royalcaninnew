﻿using System;
using System.Collections.Generic;
using Sitecore.ContentSearch;

namespace Delete.Foundation.DataTemplates.Models.Interfaces
{
    public partial class _Stockist
    {
        [IndexField("stockistRelatedEntities")]
        public virtual IEnumerable<Guid> SearchableRelatedEntities { get; set; }

        [IndexField("stockistquality")]
        public virtual int Quality { get; set; }
    }
}