﻿// <autogenerated>
//   This file was generated by T4 code generator ScriptRunner.tt.
//   Any changes made to this file manually will be lost next time the file is regenerated.
// </autogenerated>

using System;
using Sitecore.Data;
using Sitecore.Common;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Sitecore.Globalization;
using System.ComponentModel;
using Delete.Foundation.DeleteFoundationCore.Models;
using Sitecore.ContentSearch;
using Delete.Foundation.DataTemplates;

namespace Delete.Foundation.DataTemplates.Models.Interfaces
{
	public partial interface I_LocalBreed : IGlassBase, global::Delete.Foundation.DataTemplates.Models.Interfaces.I_SearchableContent
	{
		Delete.Foundation.DataTemplates.Models.Interfaces.I_GlobalBreed GlobalBreedInterface  {get; set;}
		string LocalAlias  {get; set;}
		string LocalBreedName  {get; set;}
	}

	public static partial class _LocalBreedConstants {
		public const string TemplateIdString = "{ee69aa1a-f134-48ab-9c72-9fa83960375d}";
		public static readonly ID TemplateId = new ID(TemplateIdString);
		public const string TemplateName = "_Local Breed";

		public const string GlobalBreedInterfaceFieldId = "{ab235369-1690-4f18-9993-2d84bce73710}";
		public const string LocalAliasFieldId = "{45e4ef24-24d0-4847-9de4-e945dfc1d1d7}";
		public const string LocalBreedNameFieldId = "{90b26adf-bab5-476e-a840-a84a60d7fccf}";
	
		public const string GlobalBreedInterfaceFieldName = "Global Breed";
		public const string LocalAliasFieldName = "Local Alias";
		public const string LocalBreedNameFieldName = "Local Breed Name";
	
	}

	
	/// <summary>
	/// _LocalBreed
	/// <para>Path: /sitecore/templates/Foundation/DataTemplates/Interfaces/_Local Breed</para>	
	/// <para>ID: {ee69aa1a-f134-48ab-9c72-9fa83960375d}</para>	
	/// </summary>
	[SitecoreType(TemplateId=_LocalBreedConstants.TemplateIdString)] //, Cachable = true
	public partial class _LocalBreed  : global::Delete.Foundation.DataTemplates.Models.Interfaces._SearchableContent, I_LocalBreed
	{
		/// <summary>
		/// The Global Breed field.
		/// <para>Field Type: Droptree</para>		
		/// <para>Field ID: {ab235369-1690-4f18-9993-2d84bce73710}</para>
		/// </summary>
		[SitecoreField(FieldId = _LocalBreedConstants.GlobalBreedInterfaceFieldId, FieldName = _LocalBreedConstants.GlobalBreedInterfaceFieldName, Setting = SitecoreFieldSettings.InferType)]
		[IndexField(_LocalBreedConstants.GlobalBreedInterfaceFieldName)]
		public virtual Delete.Foundation.DataTemplates.Models.Interfaces.I_GlobalBreed GlobalBreedInterface  {get; set;}

		/// <summary>
		/// The Local Alias field.
		/// <para>Field Type: Single-Line Text</para>		
		/// <para>Field ID: {45e4ef24-24d0-4847-9de4-e945dfc1d1d7}</para>
		/// </summary>
		[SitecoreField(FieldId = _LocalBreedConstants.LocalAliasFieldId, FieldName = _LocalBreedConstants.LocalAliasFieldName)]
		[IndexField(_LocalBreedConstants.LocalAliasFieldName)]
		public virtual string LocalAlias  {get; set;}

		/// <summary>
		/// The Local Breed Name field.
		/// <para>Field Type: Single-Line Text</para>		
		/// <para>Field ID: {90b26adf-bab5-476e-a840-a84a60d7fccf}</para>
		/// </summary>
		[SitecoreField(FieldId = _LocalBreedConstants.LocalBreedNameFieldId, FieldName = _LocalBreedConstants.LocalBreedNameFieldName)]
		[IndexField(_LocalBreedConstants.LocalBreedNameFieldName)]
		public virtual string LocalBreedName  {get; set;}

	
	}
}
