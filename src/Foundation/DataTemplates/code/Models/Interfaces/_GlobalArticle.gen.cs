﻿// <autogenerated>
//   This file was generated by T4 code generator ScriptRunner.tt.
//   Any changes made to this file manually will be lost next time the file is regenerated.
// </autogenerated>

using System;
using Sitecore.Data;
using Sitecore.Common;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Sitecore.Globalization;
using System.ComponentModel;
using Delete.Foundation.DeleteFoundationCore.Models;
using Sitecore.ContentSearch;
using Delete.Foundation.DataTemplates;

namespace Delete.Foundation.DataTemplates.Models.Interfaces
{
	public partial interface I_GlobalArticle : IGlassBase
	{
		DateTime Date  {get; set;}
		string Heading  {get; set;}
		string Summary  {get; set;}
		Delete.Foundation.DeleteFoundationCore.Models.ExtendedImage ThumbnailImage  {get; set;}
	}

	public static partial class _GlobalArticleConstants {
		public const string TemplateIdString = "{1d44e70e-2424-422a-a2ff-7257eadc9b46}";
		public static readonly ID TemplateId = new ID(TemplateIdString);
		public const string TemplateName = "_Global Article";

		public const string DateFieldId = "{ee1c4d9f-5117-45f7-b0bd-fb6b12d19c95}";
		public const string HeadingFieldId = "{3e0d3222-fc81-4105-8496-f340ba2219b8}";
		public const string SummaryFieldId = "{dcbe0623-bbd4-4d7b-91c8-9339fe0400e6}";
		public const string ThumbnailImageFieldId = "{6fd15459-5a23-42fe-a352-6894c12fe253}";
	
		public const string DateFieldName = "Date";
		public const string HeadingFieldName = "Heading";
		public const string SummaryFieldName = "Summary";
		public const string ThumbnailImageFieldName = "Thumbnail Image";
	
	}

	
	/// <summary>
	/// _GlobalArticle
	/// <para>Path: /sitecore/templates/Foundation/DataTemplates/Interfaces/_Global Article</para>	
	/// <para>ID: {1d44e70e-2424-422a-a2ff-7257eadc9b46}</para>	
	/// </summary>
	[SitecoreType(TemplateId=_GlobalArticleConstants.TemplateIdString)] //, Cachable = true
	public partial class _GlobalArticle  : GlassBase, I_GlobalArticle
	{
		/// <summary>
		/// The Date field.
		/// <para>Field Type: Date</para>		
		/// <para>Field ID: {ee1c4d9f-5117-45f7-b0bd-fb6b12d19c95}</para>
		/// </summary>
		[SitecoreField(FieldId = _GlobalArticleConstants.DateFieldId, FieldName = _GlobalArticleConstants.DateFieldName)]
		[IndexField(_GlobalArticleConstants.DateFieldName)]
		public virtual DateTime Date  {get; set;}

		/// <summary>
		/// The Heading field.
		/// <para>Field Type: Single-Line Text</para>		
		/// <para>Field ID: {3e0d3222-fc81-4105-8496-f340ba2219b8}</para>
		/// </summary>
		[SitecoreField(FieldId = _GlobalArticleConstants.HeadingFieldId, FieldName = _GlobalArticleConstants.HeadingFieldName)]
		[IndexField(_GlobalArticleConstants.HeadingFieldName)]
		public virtual string Heading  {get; set;}

		/// <summary>
		/// The Summary field.
		/// <para>Field Type: Multi-Line Text</para>		
		/// <para>Field ID: {dcbe0623-bbd4-4d7b-91c8-9339fe0400e6}</para>
		/// </summary>
		[SitecoreField(FieldId = _GlobalArticleConstants.SummaryFieldId, FieldName = _GlobalArticleConstants.SummaryFieldName)]
		[IndexField(_GlobalArticleConstants.SummaryFieldName)]
		public virtual string Summary  {get; set;}

		/// <summary>
		/// The Thumbnail Image field.
		/// <para>Field Type: Image</para>		
		/// <para>Field ID: {6fd15459-5a23-42fe-a352-6894c12fe253}</para>
		/// </summary>
		[SitecoreField(FieldId = _GlobalArticleConstants.ThumbnailImageFieldId, FieldName = _GlobalArticleConstants.ThumbnailImageFieldName)]
		[IndexField(_GlobalArticleConstants.ThumbnailImageFieldName)]
		public virtual Delete.Foundation.DeleteFoundationCore.Models.ExtendedImage ThumbnailImage  {get; set;}

	
	}
}
