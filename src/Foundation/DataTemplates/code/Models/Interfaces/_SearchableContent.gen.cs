﻿// <autogenerated>
//   This file was generated by T4 code generator ScriptRunner.tt.
//   Any changes made to this file manually will be lost next time the file is regenerated.
// </autogenerated>

using System;
using Sitecore.Data;
using Sitecore.Common;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Sitecore.Globalization;
using System.ComponentModel;
using Delete.Foundation.DeleteFoundationCore.Models;
using Sitecore.ContentSearch;
using Delete.Foundation.DataTemplates;

namespace Delete.Foundation.DataTemplates.Models.Interfaces
{
	public partial interface I_SearchableContent : IGlassBase
	{
		Boolean ExcludeFromSearch  {get; set;}
		IEnumerable<GlassBase> ExcludedRenderings  {get; set;}
	}

	public static partial class _SearchableContentConstants {
		public const string TemplateIdString = "{f581f94b-9bd9-4070-bc41-4e63f9cf1cd7}";
		public static readonly ID TemplateId = new ID(TemplateIdString);
		public const string TemplateName = "_SearchableContent";

		public const string ExcludeFromSearchFieldId = "{5dfd5573-00e5-4c68-b6cd-49ddde34abfc}";
		public const string ExcludedRenderingsFieldId = "{3e48eb52-8ae3-4be2-ae53-05db0eb7265e}";
	
		public const string ExcludeFromSearchFieldName = "Exclude From Search";
		public const string ExcludedRenderingsFieldName = "Excluded Renderings";
	
	}

	
	/// <summary>
	/// _SearchableContent
	/// <para>Path: /sitecore/templates/Foundation/DataTemplates/Interfaces/_SearchableContent</para>	
	/// <para>ID: {f581f94b-9bd9-4070-bc41-4e63f9cf1cd7}</para>	
	/// </summary>
	[SitecoreType(TemplateId=_SearchableContentConstants.TemplateIdString)] //, Cachable = true
	public partial class _SearchableContent  : GlassBase, I_SearchableContent
	{
		/// <summary>
		/// The Exclude From Search field.
		/// <para>Field Type: Checkbox</para>		
		/// <para>Field ID: {5dfd5573-00e5-4c68-b6cd-49ddde34abfc}</para>
		/// </summary>
		[SitecoreField(FieldId = _SearchableContentConstants.ExcludeFromSearchFieldId, FieldName = _SearchableContentConstants.ExcludeFromSearchFieldName)]
		[IndexField(_SearchableContentConstants.ExcludeFromSearchFieldName)]
		public virtual Boolean ExcludeFromSearch  {get; set;}

		/// <summary>
		/// The Excluded Renderings field.
		/// <para>Field Type: Treelist</para>		
		/// <para>Field ID: {3e48eb52-8ae3-4be2-ae53-05db0eb7265e}</para>
		/// </summary>
		[SitecoreField(FieldId = _SearchableContentConstants.ExcludedRenderingsFieldId, FieldName = _SearchableContentConstants.ExcludedRenderingsFieldName)]
		[IndexField(_SearchableContentConstants.ExcludedRenderingsFieldName)]
		public virtual IEnumerable<GlassBase> ExcludedRenderings  {get; set;}

	
	}
}
