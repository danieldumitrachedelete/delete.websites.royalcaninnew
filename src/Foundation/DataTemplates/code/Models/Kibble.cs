﻿using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration.Attributes;

namespace Delete.Foundation.DataTemplates.Models
{
    public partial class Kibble
    {
        [SitecoreChildren]
        public virtual IEnumerable<KibbleNote> Notes { get; set; }
    }
}