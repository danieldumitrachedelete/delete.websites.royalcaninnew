﻿// <autogenerated>
//   This file was generated by T4 code generator ScriptRunner.tt.
//   Any changes made to this file manually will be lost next time the file is regenerated.
// </autogenerated>

using System;
using Sitecore.Data;
using Sitecore.Common;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Sitecore.Globalization;
using System.ComponentModel;
using Delete.Foundation.DeleteFoundationCore.Models;
using Sitecore.ContentSearch;
using Delete.Foundation.DataTemplates;

namespace Delete.Foundation.DataTemplates.Models.Folder
{
	public partial interface ISearchAndFilterConfigurationFolder : IGlassBase
	{
	}

	public static partial class SearchAndFilterConfigurationFolderConstants {
		public const string TemplateIdString = "{ceef9812-0663-4b06-8ad2-48a87d489624}";
		public static readonly ID TemplateId = new ID(TemplateIdString);
		public const string TemplateName = "Search And Filter Configuration Folder";

	
	
	}

	
	/// <summary>
	/// SearchAndFilterConfigurationFolder
	/// <para>Path: /sitecore/templates/Foundation/DataTemplates/Folder/Search And Filter Configuration Folder</para>	
	/// <para>ID: {ceef9812-0663-4b06-8ad2-48a87d489624}</para>	
	/// </summary>
	[SitecoreType(TemplateId=SearchAndFilterConfigurationFolderConstants.TemplateIdString, EnforceTemplate = SitecoreEnforceTemplate.Template)] //, Cachable = true
	public partial class SearchAndFilterConfigurationFolder  : GlassBase, ISearchAndFilterConfigurationFolder
	{
	
	}
}
