﻿using System;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration.Attributes;
using Delete.Foundation.DataTemplates.Models.Filter;

namespace Delete.Foundation.DataTemplates.Models.Folder
{
    public partial class SpecieRelatedFolder
    {
        [SitecoreChildren(InferType = true, IsLazy = false)]
        public virtual IEnumerable<FilterItem> FilterItems { get; set; }
    }
}
