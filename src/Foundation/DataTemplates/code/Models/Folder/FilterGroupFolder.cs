﻿using System;
using System.Collections.Generic;
using System.Linq;
using Delete.Foundation.DataTemplates.Models.Filter;
using Delete.Foundation.DataTemplates.Strategies;
using Glass.Mapper;
using Glass.Mapper.Sc.Configuration.Attributes;
using Sitecore.Data;

namespace Delete.Foundation.DataTemplates.Models.Folder
{
    using Sitecore.ContentSearch;

    public partial interface IFilterGroupFolder
    {
        IEnumerable<FilterItem> FilterItems { get; set; }

        IEnumerable<SpecieRelatedFolder> SpecieFolders { get; set; }

        IEnumerable<FilterItem> GetItems(Guid species);
    }

    public partial class FilterGroupFolder
    {
        [SitecoreChildren(InferType = true, IsLazy = false)]
        public IEnumerable<FilterItem> FilterItems { get; set; }

        [SitecoreChildren(InferType = true, IsLazy = false)]
        public IEnumerable<SpecieRelatedFolder> SpecieFolders { get; set; }

        [IndexField("filterGroupFolderSearchableItemValues")]
        [SitecoreIgnore]
        public IEnumerable<string> SearchableValues { get; set; }

        private IEnumerable<FilterItem> Items { get; set; }

        public IEnumerable<FilterItem> GetItems(Guid species)
        {
            if (this.Items != null)
            {
                return this.Items;
            }

            if (this.FilterStrategy != null && this.FilterStrategy.Value.HasValue())
            {
                var strategy = (BaseFilterStrategy)Activator.CreateInstance(Type.GetType(this.FilterStrategy.Value));
                return this.Items = strategy.GetFilterItems(species);
            }

            if (this.FilterItems != null && this.FilterItems.Any())
            {
                return this.Items = this.FilterItems;
            }
            
            return this.Items = this.SpecieFolders.FirstOrDefault(x => x.Specie == species)?.FilterItems ?? new List<FilterItem>();
        }
    }
}