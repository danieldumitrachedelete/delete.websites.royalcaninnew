﻿// <autogenerated>
//   This file was generated by T4 code generator ScriptRunner.tt.
//   Any changes made to this file manually will be lost next time the file is regenerated.
// </autogenerated>

using System;
using Sitecore.Data;
using Sitecore.Common;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Sitecore.Globalization;
using System.ComponentModel;
using Delete.Foundation.DeleteFoundationCore.Models;
using Sitecore.ContentSearch;
using Delete.Foundation.DataTemplates;

namespace Delete.Foundation.DataTemplates.Models.Folder
{
	public partial interface ISpecieRelatedFolder : IGlassBase
	{
		Guid Specie  {get; set;}
	}

	public static partial class SpecieRelatedFolderConstants {
		public const string TemplateIdString = "{ead01e9a-990e-4143-83f5-4939ece807a8}";
		public static readonly ID TemplateId = new ID(TemplateIdString);
		public const string TemplateName = "Specie Related Folder";

		public const string SpecieFieldId = "{bca312fb-917a-44b0-a3b9-064342d12651}";
	
		public const string SpecieFieldName = "Specie";
	
	}

	
	/// <summary>
	/// SpecieRelatedFolder
	/// <para>Path: /sitecore/templates/Foundation/DataTemplates/Folder/Specie Related Folder</para>	
	/// <para>ID: {ead01e9a-990e-4143-83f5-4939ece807a8}</para>	
	/// </summary>
	[SitecoreType(TemplateId=SpecieRelatedFolderConstants.TemplateIdString, EnforceTemplate = SitecoreEnforceTemplate.Template)] //, Cachable = true
	public partial class SpecieRelatedFolder  : GlassBase, ISpecieRelatedFolder
	{
		/// <summary>
		/// The Specie field.
		/// <para>Field Type: Droplink</para>		
		/// <para>Field ID: {bca312fb-917a-44b0-a3b9-064342d12651}</para>
		/// </summary>
		[SitecoreField(FieldId = SpecieRelatedFolderConstants.SpecieFieldId, FieldName = SpecieRelatedFolderConstants.SpecieFieldName)]
		[IndexField(SpecieRelatedFolderConstants.SpecieFieldName)]
		public virtual Guid Specie  {get; set;}

	
	}
}
