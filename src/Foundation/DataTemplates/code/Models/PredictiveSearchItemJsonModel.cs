﻿namespace Delete.Foundation.DataTemplates.Models
{
    public class PredictiveSearchItemJsonModel
    {
        public string Title { get;set; }

        public string Description { get; set; }

        public string Url { get; set; }
    }
}