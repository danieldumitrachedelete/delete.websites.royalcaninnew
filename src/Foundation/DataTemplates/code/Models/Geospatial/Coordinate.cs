﻿using Sitecore.ContentSearch;

namespace Delete.Foundation.DataTemplates.Models.Geospatial
{
    public partial interface ICoordinate
    {
        Sitecore.ContentSearch.Data.Coordinate ItemLocation { get; }
    }

    public partial class Coordinate
    {
        [IndexField("Coordinate")]
        public Sitecore.ContentSearch.Data.Coordinate ItemLocation => new Sitecore.ContentSearch.Data.Coordinate((double)Latitude, (double)Longitude);
    }
}
