﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Delete.Foundation.DataTemplates.Models.Filter
{
    public partial class FilterItem
    {
        public FilterItem SetId(Guid id)
        {
            Id = id;
            return this;
        }

        public string BreedId { get; set; }
        public string Age { get; set; }
    }
}