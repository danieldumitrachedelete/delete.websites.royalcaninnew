﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Delete.Foundation.DataTemplates.Models
{
    public class PredictiveSearchJsonModel
    {
        public virtual IEnumerable<PredictiveSearchItemJsonModel> Items { get; set; }

        public virtual IEnumerable<PredictiveSearchItemJsonModel> FeaturedItems { get; set; }
    }
}