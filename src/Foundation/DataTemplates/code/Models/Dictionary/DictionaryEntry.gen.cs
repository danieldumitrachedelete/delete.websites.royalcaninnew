﻿// <autogenerated>
//   This file was generated by T4 code generator ScriptRunner.tt.
//   Any changes made to this file manually will be lost next time the file is regenerated.
// </autogenerated>

using System;
using Sitecore.Data;
using Sitecore.Common;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Sitecore.Globalization;
using System.ComponentModel;
using Delete.Foundation.DeleteFoundationCore.Models;
using Sitecore.ContentSearch;
using Delete.Foundation.DataTemplates;

namespace Delete.Foundation.DataTemplates.Models.Dictionary
{
	public partial interface IDictionaryEntry : IGlassBase
	{
		string Text  {get; set;}
		string Value  {get; set;}
	}

	public static partial class DictionaryEntryConstants {
		public const string TemplateIdString = "{502bc534-1f7d-4ab2-90e8-546081a16501}";
		public static readonly ID TemplateId = new ID(TemplateIdString);
		public const string TemplateName = "Dictionary Entry";

		public const string TextFieldId = "{4b6feab0-602e-4efb-9952-5cb3b9ba9342}";
		public const string ValueFieldId = "{d84693b6-c514-4826-b389-023ffadc3455}";
	
		public const string TextFieldName = "Text";
		public const string ValueFieldName = "Value";
	
	}

	
	/// <summary>
	/// DictionaryEntry
	/// <para>Path: /sitecore/templates/Foundation/DataTemplates/Dictionary/Dictionary Entry</para>	
	/// <para>ID: {502bc534-1f7d-4ab2-90e8-546081a16501}</para>	
	/// </summary>
	[SitecoreType(TemplateId=DictionaryEntryConstants.TemplateIdString)] //, Cachable = true
	public partial class DictionaryEntry  : GlassBase, IDictionaryEntry
	{
		/// <summary>
		/// The Text field.
		/// <para>Field Type: Single-Line Text</para>		
		/// <para>Field ID: {4b6feab0-602e-4efb-9952-5cb3b9ba9342}</para>
		/// </summary>
		[SitecoreField(FieldId = DictionaryEntryConstants.TextFieldId, FieldName = DictionaryEntryConstants.TextFieldName)]
		[IndexField(DictionaryEntryConstants.TextFieldName)]
		public virtual string Text  {get; set;}

		/// <summary>
		/// The Value field.
		/// <para>Field Type: Single-Line Text</para>		
		/// <para>Field ID: {d84693b6-c514-4826-b389-023ffadc3455}</para>
		/// </summary>
		[SitecoreField(FieldId = DictionaryEntryConstants.ValueFieldId, FieldName = DictionaryEntryConstants.ValueFieldName)]
		[IndexField(DictionaryEntryConstants.ValueFieldName)]
		public virtual string Value  {get; set;}

	
	}
}
