﻿using System;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration.Attributes;

namespace Delete.Foundation.DataTemplates.Models.Menu
{
    public partial class MenuGroup
    {
        [SitecoreChildren(InferType = true)]
        public virtual IEnumerable<MenuItem> MenuItems { get; set; }
    }
}
