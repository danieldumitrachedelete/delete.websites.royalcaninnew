﻿using System;
using System.Collections.Generic;
using System.Linq;
using Glass.Mapper.Sc.Configuration.Attributes;

namespace Delete.Foundation.DataTemplates.Models.Menu
{
    public partial class MenuFolder
    {
        
        [SitecoreChildren(InferType = true)]
        public virtual IEnumerable<MenuFolder> MenuFolders{ get; set; }

        public MenuFolder Primary => MenuFolders?.FirstOrDefault(x => x.Name.Equals("Primary"));

        public MenuFolder Secondary => MenuFolders?.FirstOrDefault(x => x.Name.Equals("Secondary"));

        public MenuFolder Modal => MenuFolders?.FirstOrDefault(x => x.Name.Equals("Modal"));

        public string RegionName { get; set; }

        [SitecoreChildren(InferType = true)]
        public virtual IEnumerable<MenuInfoItem> MenuInfoItems { get; set; }

        [SitecoreChildren(InferType = true)]
        public virtual IEnumerable<MenuGroup> MenuGroups{ get; set; }

        
        [SitecoreChildren(InferType = true)]
        public virtual IEnumerable<MenuItem> MenuItems { get; set; }
    }
}
