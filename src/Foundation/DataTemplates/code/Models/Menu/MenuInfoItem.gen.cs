﻿// <autogenerated>
//   This file was generated by T4 code generator ScriptRunner.tt.
//   Any changes made to this file manually will be lost next time the file is regenerated.
// </autogenerated>

using System;
using Sitecore.Data;
using Sitecore.Common;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Sitecore.Globalization;
using System.ComponentModel;
using Delete.Foundation.DeleteFoundationCore.Models;
using Sitecore.ContentSearch;
using Delete.Foundation.DataTemplates;

namespace Delete.Foundation.DataTemplates.Models.Menu
{
	public partial interface IMenuInfoItem : IGlassBase
	{
		string Content  {get; set;}
		Delete.Foundation.DeleteFoundationCore.Models.ExtendedImage Image  {get; set;}
		string Title  {get; set;}
	}

	public static partial class MenuInfoItemConstants {
		public const string TemplateIdString = "{0cd4b009-d51e-4a03-abea-49d8d016f82d}";
		public static readonly ID TemplateId = new ID(TemplateIdString);
		public const string TemplateName = "Menu Info Item";

		public const string ContentFieldId = "{36a97cc9-ee8c-4948-a316-3845ed6e7602}";
		public const string ImageFieldId = "{f929ed28-7ec9-47ac-b1d2-2fd7fb87ef2a}";
		public const string TitleFieldId = "{74526020-301e-4edf-b8de-9b7a21dd5112}";
	
		public const string ContentFieldName = "Content";
		public const string ImageFieldName = "Image";
		public const string TitleFieldName = "Title";
	
	}

	
	/// <summary>
	/// MenuInfoItem
	/// <para>Path: /sitecore/templates/Foundation/DataTemplates/Menu/Menu Info Item</para>	
	/// <para>ID: {0cd4b009-d51e-4a03-abea-49d8d016f82d}</para>	
	/// </summary>
	[SitecoreType(TemplateId=MenuInfoItemConstants.TemplateIdString, EnforceTemplate = SitecoreEnforceTemplate.Template)] //, Cachable = true
	public partial class MenuInfoItem  : GlassBase, IMenuInfoItem
	{
		/// <summary>
		/// The Content field.
		/// <para>Field Type: Single-Line Text</para>		
		/// <para>Field ID: {36a97cc9-ee8c-4948-a316-3845ed6e7602}</para>
		/// </summary>
		[SitecoreField(FieldId = MenuInfoItemConstants.ContentFieldId, FieldName = MenuInfoItemConstants.ContentFieldName)]
		[IndexField(MenuInfoItemConstants.ContentFieldName)]
		public virtual string Content  {get; set;}

		/// <summary>
		/// The Image field.
		/// <para>Field Type: Image</para>		
		/// <para>Field ID: {f929ed28-7ec9-47ac-b1d2-2fd7fb87ef2a}</para>
		/// </summary>
		[SitecoreField(FieldId = MenuInfoItemConstants.ImageFieldId, FieldName = MenuInfoItemConstants.ImageFieldName)]
		[IndexField(MenuInfoItemConstants.ImageFieldName)]
		public virtual Delete.Foundation.DeleteFoundationCore.Models.ExtendedImage Image  {get; set;}

		/// <summary>
		/// The Title field.
		/// <para>Field Type: Single-Line Text</para>		
		/// <para>Field ID: {74526020-301e-4edf-b8de-9b7a21dd5112}</para>
		/// </summary>
		[SitecoreField(FieldId = MenuInfoItemConstants.TitleFieldId, FieldName = MenuInfoItemConstants.TitleFieldName)]
		[IndexField(MenuInfoItemConstants.TitleFieldName)]
		public virtual string Title  {get; set;}

	
	}
}
