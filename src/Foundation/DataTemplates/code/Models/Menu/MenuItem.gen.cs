﻿// <autogenerated>
//   This file was generated by T4 code generator ScriptRunner.tt.
//   Any changes made to this file manually will be lost next time the file is regenerated.
// </autogenerated>

using System;
using Sitecore.Data;
using Sitecore.Common;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Sitecore.Globalization;
using System.ComponentModel;
using Delete.Foundation.DeleteFoundationCore.Models;
using Sitecore.ContentSearch;
using Delete.Foundation.DataTemplates;

namespace Delete.Foundation.DataTemplates.Models.Menu
{
	public partial interface IMenuItem : IGlassBase, global::Delete.Foundation.DataTemplates.Models.Links.ILinkWithIcon
	{
		string DataTrackCustomAction  {get; set;}
		string DataTrackCustomCategory  {get; set;}
		Boolean DataTrackCustomEnable  {get; set;}
		string DataTrackCustomEvent  {get; set;}
		string DataTrackCustomLabel  {get; set;}
		IEnumerable<Delete.Foundation.DataTemplates.Models.Region> Regions  {get; set;}
	}

	public static partial class MenuItemConstants {
		public const string TemplateIdString = "{cf5c0b6f-3ab8-4d0b-ad98-143413d3c080}";
		public static readonly ID TemplateId = new ID(TemplateIdString);
		public const string TemplateName = "Menu Item";

		public const string DataTrackCustomActionFieldId = "{3bffa85e-e9a5-47ef-9d9f-18b72ec50060}";
		public const string DataTrackCustomCategoryFieldId = "{e33e37b2-346e-44be-a2ab-ca3c139b86ce}";
		public const string DataTrackCustomEnableFieldId = "{fc91abd9-2da5-4520-ae8c-e265587f9d63}";
		public const string DataTrackCustomEventFieldId = "{a0744656-0d99-4358-b812-afa0e562e9ce}";
		public const string DataTrackCustomLabelFieldId = "{97d7ca13-a482-4be6-8165-f00548148566}";
		public const string RegionsFieldId = "{73f89deb-d9b9-43db-88b5-9af045e40083}";
	
		public const string DataTrackCustomActionFieldName = "Data Track Custom Action";
		public const string DataTrackCustomCategoryFieldName = "Data Track Custom Category";
		public const string DataTrackCustomEnableFieldName = "Data Track Custom Enable";
		public const string DataTrackCustomEventFieldName = "Data Track Custom Event";
		public const string DataTrackCustomLabelFieldName = "Data Track Custom Label";
		public const string RegionsFieldName = "Region";
	
	}

	
	/// <summary>
	/// MenuItem
	/// <para>Path: /sitecore/templates/Foundation/DataTemplates/Menu/Menu Item</para>	
	/// <para>ID: {cf5c0b6f-3ab8-4d0b-ad98-143413d3c080}</para>	
	/// </summary>
	[SitecoreType(TemplateId=MenuItemConstants.TemplateIdString, EnforceTemplate = SitecoreEnforceTemplate.Template)] //, Cachable = true
	public partial class MenuItem  : global::Delete.Foundation.DataTemplates.Models.Links.LinkWithIcon, IMenuItem
	{
		/// <summary>
		/// The Data Track Custom Action field.
		/// <para>Field Type: Single-Line Text</para>		
		/// <para>Field ID: {3bffa85e-e9a5-47ef-9d9f-18b72ec50060}</para>
		/// </summary>
		[SitecoreField(FieldId = MenuItemConstants.DataTrackCustomActionFieldId, FieldName = MenuItemConstants.DataTrackCustomActionFieldName)]
		[IndexField(MenuItemConstants.DataTrackCustomActionFieldName)]
		public virtual string DataTrackCustomAction  {get; set;}

		/// <summary>
		/// The Data Track Custom Category field.
		/// <para>Field Type: Single-Line Text</para>		
		/// <para>Field ID: {e33e37b2-346e-44be-a2ab-ca3c139b86ce}</para>
		/// </summary>
		[SitecoreField(FieldId = MenuItemConstants.DataTrackCustomCategoryFieldId, FieldName = MenuItemConstants.DataTrackCustomCategoryFieldName)]
		[IndexField(MenuItemConstants.DataTrackCustomCategoryFieldName)]
		public virtual string DataTrackCustomCategory  {get; set;}

		/// <summary>
		/// The Data Track Custom Enable field.
		/// <para>Field Type: Checkbox</para>		
		/// <para>Field ID: {fc91abd9-2da5-4520-ae8c-e265587f9d63}</para>
		/// </summary>
		[SitecoreField(FieldId = MenuItemConstants.DataTrackCustomEnableFieldId, FieldName = MenuItemConstants.DataTrackCustomEnableFieldName)]
		[IndexField(MenuItemConstants.DataTrackCustomEnableFieldName)]
		public virtual Boolean DataTrackCustomEnable  {get; set;}

		/// <summary>
		/// The Data Track Custom Event field.
		/// <para>Field Type: Single-Line Text</para>		
		/// <para>Field ID: {a0744656-0d99-4358-b812-afa0e562e9ce}</para>
		/// </summary>
		[SitecoreField(FieldId = MenuItemConstants.DataTrackCustomEventFieldId, FieldName = MenuItemConstants.DataTrackCustomEventFieldName)]
		[IndexField(MenuItemConstants.DataTrackCustomEventFieldName)]
		public virtual string DataTrackCustomEvent  {get; set;}

		/// <summary>
		/// The Data Track Custom Label field.
		/// <para>Field Type: Single-Line Text</para>		
		/// <para>Field ID: {97d7ca13-a482-4be6-8165-f00548148566}</para>
		/// </summary>
		[SitecoreField(FieldId = MenuItemConstants.DataTrackCustomLabelFieldId, FieldName = MenuItemConstants.DataTrackCustomLabelFieldName)]
		[IndexField(MenuItemConstants.DataTrackCustomLabelFieldName)]
		public virtual string DataTrackCustomLabel  {get; set;}

		/// <summary>
		/// The Region field.
		/// <para>Field Type: Treelist</para>		
		/// <para>Field ID: {73f89deb-d9b9-43db-88b5-9af045e40083}</para>
		/// </summary>
		[SitecoreField(FieldId = MenuItemConstants.RegionsFieldId, FieldName = MenuItemConstants.RegionsFieldName)]
		[IndexField(MenuItemConstants.RegionsFieldName)]
		public virtual IEnumerable<Delete.Foundation.DataTemplates.Models.Region> Regions  {get; set;}

	
	}
}
