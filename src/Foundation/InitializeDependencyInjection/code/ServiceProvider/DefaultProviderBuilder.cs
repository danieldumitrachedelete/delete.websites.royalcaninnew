﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.DependencyInjection;

namespace Delete.Foundation.InitializeDependencyInjection.ServiceProvider
{
    public class DefaultProviderBuilder : DefaultServiceProviderBuilder
    {
        protected override IServiceProvider BuildServiceProvider(IServiceCollection serviceCollection)
        {
            // Register custom IoC container here
            return serviceCollection.BuildServiceProvider();
        }
    }
}