﻿namespace Delete.Foundation.CreateFromBranchTemplate.Models
{
    public enum DisablerState
    {
        Disabled,
        Enabled
    }
}