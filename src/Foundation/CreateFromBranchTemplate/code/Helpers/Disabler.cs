﻿using Delete.Foundation.CreateFromBranchTemplate.Models;
using Sitecore.Common;

namespace Delete.Foundation.CreateFromBranchTemplate.Helpers
{
    public abstract class Disabler<TSwitchType> : Switcher<DisablerState, TSwitchType>
    {
        // ReSharper disable once PublicConstructorInAbstractClass
        public Disabler() : base(DisablerState.Enabled)
        {
        }

        public static bool IsActive => CurrentValue == DisablerState.Enabled;
    }
}