﻿using Delete.Foundation.DeleteFoundationCore.Managers;

namespace Delete.Foundation.AssetsManagement
{
    public class AssetsManagementConfiguration  : SitecoreConfigurationManagerWrapper
    {
        public static string RegionName => GetSetting(Constants.Configuration.AppSettings.RegionName);
        public static string ServiceName => GetSetting(Constants.Configuration.AppSettings.ServiceName);
        public static string Algorithm => GetSetting(Constants.Configuration.AppSettings.Algorithm);
        public static string ContentType => GetSetting(Constants.Configuration.AppSettings.ContentType);
        public static string SignedHeaders => GetSetting(Constants.Configuration.AppSettings.SignedHeaders);

        public static string Host => GetSetting(Constants.Configuration.AppSettings.Host);

        public static string NewHost => GetSetting(Constants.Configuration.AppSettings.NewHost);

        public static string Prefix => GetSetting(Constants.Configuration.AppSettings.Prefix);

        public static string AccessKey => GetSetting(Constants.Configuration.AppSettings.AccessKey);
        public static string SecretKey => GetSetting(Constants.Configuration.AppSettings.SecretKey);

        public static string ClientId => GetSetting(Constants.Configuration.AppSettings.ClientId);
        public static string ClientSecret => GetSetting(Constants.Configuration.AppSettings.ClientSecret);
    }
}