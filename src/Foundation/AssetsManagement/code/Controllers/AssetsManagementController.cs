using System;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using AutoMapper;
using Delete.Foundation.AssetsManagement.Models;
using Delete.Foundation.AssetsManagement.Services;
using Delete.Foundation.DeleteFoundationCore.Controllers;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.DeleteFoundationCore.Models;
using Glass.Mapper.Sc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Delete.Foundation.AssetsManagement.Controllers
{
    public class AssetsManagementController : BaseController
    {
        private readonly IWeShareService _weShareService;

        public AssetsManagementController(ISitecoreContext sitecoreContext, IMapper mapper, ICacheManager cacheManager, IWeShareService weShareService)
            : base(sitecoreContext, mapper, cacheManager)
        {
            _weShareService = weShareService;
        }

        [HttpGet]
        public ActionResult Search(string query, int startFrom = 0, int pageSize = 20)
        {
            var result = _weShareService.Search(query, startFrom, pageSize);

            var list = result.hits.hits.Select(x => CreateMediaItem(x, query)).ToList();

            var item = CreateMediaLibraryAlbum(query);
            list.Insert(0, item);

            var serializerSettings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };

            var model = new
            {
                total = result.hits.total,
                list = list,
            };

            var json = model.ToJson(serializerSettings);

            return new ContentResult
            {
                Content = json,
                ContentEncoding = Encoding.UTF8,
                ContentType = "application/json"
            };
        }

        private ExternalImage CreateMediaLibraryAlbum(string query)
        {
            return new ExternalImage
            {
                ID = query,
                AlbumID = "/",
                Title = query,
                Date = DateTime.Now,
                Path = query,
                Kind = "album",
                ThumbnailSrc = "/assets/img/simplegallery/folder.svg",
            };
        }

        private ExternalImage CreateMediaItem(WeShareSearchClasses.Hit mediaItem, string albumId)
        {
            var imageSize = mediaItem._source.metadata?.Specifications?.imageSize ?? string.Empty;
            var sizes = imageSize.Split('x');

            return new ExternalImage
            {
                ID = mediaItem._id,
                AlbumID = albumId,
                Title = mediaItem._source.name,
                Date = mediaItem._source.updated,
                Path = mediaItem._source.public_url,
                Kind = "image",
                Description = mediaItem._source.name,
                ImgWidth = int.Parse(sizes.Length > 1 ? sizes[0].OrDefault("0") : "0"),
                ImgHeight = int.Parse(sizes.Length > 1 ? sizes[1].OrDefault("0") : "0"),
                ThumbnailSrc = $"data:{mediaItem._source.thumbnail.mime_type};base64, {mediaItem._source.thumbnail.base64}",
                Src = mediaItem._source.public_url,
            };
        }
    }
}
