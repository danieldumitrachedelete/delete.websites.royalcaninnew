﻿using System;
using System.Net;
using System.Text;
using System.IO;
using System.Security.Cryptography;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using Delete.Foundation.AssetsManagement.Models;
using Newtonsoft.Json;

using Config = Delete.Foundation.AssetsManagement.AssetsManagementConfiguration;

namespace Delete.Foundation.AssetsManagement.Services
{
    public interface IWeShareService
    {
        WeShareSearchClasses.SearchResponse Search(string query, int startFrom, int pageSize);

		WeShareSearchClasses.SearchResponse SearchById(string queryId);
	}


	public class WeShareService : IWeShareService
	{
        public WeShareSearchClasses.SearchResponse Search(string query, int startFrom = 0, int pageSize = 20)
        {
            string requestString = GetRequestQueryString(query, startFrom, pageSize);

            var canonicalUrl = Config.Prefix + "/search";

            WebRequest request = RequestPost(canonicalUrl, "", requestString);
            using (WebResponse response = request.GetResponse())
            {
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string result = reader.ReadToEnd();

                    return JsonConvert.DeserializeObject<WeShareSearchClasses.SearchResponse>(result);
                }
            }
		}

		public WeShareSearchClasses.SearchResponse SearchById(string queryId)
		{
			string requestString = GetRequestForId(queryId);
            return JsonConvert.DeserializeObject<WeShareSearchClasses.SearchResponse>(RequestPost(requestString));
        }

		private static string GetRequestQueryString(string query, int startFrom, int pageSize)
        {
            var requestObject = new
            {
                query = new
                {
                    multi_match = new
                    {
                        query = query,
                        fields = new[]
                        {
                            "combined_fields"
                        },
                        @operator = "and",
                        type = "best_fields",
                        fuzziness = "auto",
                        minimum_should_match = 0
                    }
                },
                from = startFrom,
                size = pageSize,
                sort = new object[]
                {
                    "_score",
                    new
                    {
                        updated = "desc"
                    },
                    "_id"
                }
            };

            var requestString = JsonConvert.SerializeObject(requestObject);
            return requestString;
		}

        private static string GetRequestForId(string query)
        {
            var requestParam = $"[ {query} ]";
            var incorrectParam = '"' + requestParam + '"';
            var requestObject = new
            {
                query = new
                {
                    terms = new
                    {
                        id = requestParam
                    }
                },
                size = 10
            };

            var requestString = JsonConvert.SerializeObject(requestObject);
            return requestString.Replace(incorrectParam, requestParam);
        }

        private WebRequest RequestPost(string canonicalUri, string canonicalQueriString, string jsonString)
        {
            string hashedRequestPayload = CreateRequestPayload(jsonString);

            string authorization = Sign(hashedRequestPayload, "POST", canonicalUri, canonicalQueriString);
            string requestDate = DateTime.UtcNow.ToString("yyyyMMddTHHmmss") + "Z";

            var requestUriString = $"https://{Config.Host}{canonicalUri}";

            WebRequest webRequest = WebRequest.Create(requestUriString);

            webRequest.Timeout = 20000;
            webRequest.Method = "POST";
            webRequest.ContentType = Config.ContentType;
            webRequest.Headers.Add("X-Amz-date", requestDate);
            webRequest.Headers.Add("Authorization", authorization);
            webRequest.Headers.Add("x-amz-content-sha256", hashedRequestPayload);
            webRequest.ContentLength = jsonString.Length;

            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] data = encoding.GetBytes(jsonString);

            Stream newStream = webRequest.GetRequestStream();
            newStream.Write(data, 0, data.Length);


            return webRequest;
        }

        private String RequestPost(string jsonString)
        {
            var requestUriString = $"https://{Config.NewHost}";

            String result = "";

            using (var httpClient = new HttpClient(new HttpClientHandler
            {
                AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
            }))
            using (var requestMessage = new HttpRequestMessage(HttpMethod.Post, requestUriString))
            {
                httpClient.Timeout = new TimeSpan(0, 0, 2, 0);
                requestMessage.Headers.Add("client_id", Config.ClientId);
                requestMessage.Headers.Add("client_secret", Config.ClientSecret);
                requestMessage.Headers.Add("sourceType", "TheHub-Dev");
                requestMessage.Content = new StringContent(jsonString, Encoding.UTF8, "application/json");
                var response =
                    httpClient.SendAsync(requestMessage).GetAwaiter().GetResult();
                result = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();
            }

            return result;
        }

        private string CreateRequestPayload(string jsonString)
        {
            //Here should be JSON object of the model we are sending with POST request
            //var jsonToSerialize = new { Data = String.Empty };

            //We pass an empty string to the serializer if we are makeing GET request
            //string requestPayload = new JavaScriptSerializer().Serialize(jsonToSerialize);

            string hashedRequestPayload = HexEncode(Hash(ToBytes(jsonString)));

            return hashedRequestPayload;
        }

        private string Sign(string hashedRequestPayload, string requestMethod, string canonicalUri, string canonicalQueryString)
        {
            var currentDateTime = DateTime.UtcNow;
            var accessKey = Config.AccessKey;
            var secretKey = Config.SecretKey;

            var dateStamp = currentDateTime.ToString("yyyyMMdd");
            var requestDate = currentDateTime.ToString("yyyyMMddTHHmmss") + "Z";
            var credentialScope = $"{dateStamp}/{Config.RegionName}/{Config.ServiceName}/aws4_request";

            var headers = new SortedDictionary<string, string> {
                { "content-type", Config.ContentType },
                { "host", Config.Host  },
                { "x-amz-date", requestDate }
            };

            string canonicalHeaders = string.Join("\n", headers.Select(x => x.Key.ToLowerInvariant() + ":" + x.Value.Trim())) + "\n";

            // Task 1: Create a Canonical Request For Signature Version 4
            string canonicalRequest = requestMethod + "\n" 
                              + canonicalUri + "\n" 
                              + canonicalQueryString + "\n" 
                              + canonicalHeaders + "\n" 
                              + Config.SignedHeaders + "\n" 
                              + hashedRequestPayload;

            string hashedCanonicalRequest = HexEncode(Hash(ToBytes(canonicalRequest)));

            // Task 2: Create a String to Sign for Signature Version 4
            string stringToSign = Config.Algorithm + "\n" + requestDate + "\n" + credentialScope + "\n" + hashedCanonicalRequest;

            // Task 3: Calculate the AWS Signature Version 4
            byte[] signingKey = GetSignatureKey(secretKey, dateStamp, Config.RegionName, Config.ServiceName);
            string signature = HexEncode(HmacSha256(stringToSign, signingKey));

            // Task 4: Prepare a signed request
            // Authorization: algorithm Credential=access key ID/credential scope, SignedHeadaers=SignedHeaders, Signature=signature

            string authorization = string.Format("{0} Credential={1}/{2}/{3}/{4}/aws4_request, SignedHeaders={5}, Signature={6}",
                Config.Algorithm, 
                accessKey, 
                dateStamp, 
                Config.RegionName, 
                Config.ServiceName, 
                Config.SignedHeaders, 
                signature);

            return authorization;
        }

        private static byte[] GetSignatureKey(string key, string dateStamp, string regionName, string serviceName)
        {
            byte[] kDate = HmacSha256(dateStamp, ToBytes("AWS4" + key));
            byte[] kRegion = HmacSha256(regionName, kDate);
            byte[] kService = HmacSha256(serviceName, kRegion);
            return HmacSha256("aws4_request", kService);
        }

        #region utilities
        
        private static byte[] ToBytes(string str)
        {
            return Encoding.UTF8.GetBytes(str.ToCharArray());
        }

        private static string HexEncode(byte[] bytes)
        {
            return BitConverter.ToString(bytes).Replace("-", string.Empty).ToLowerInvariant();
        }

        private static byte[] Hash(byte[] bytes)
        {
            return SHA256.Create().ComputeHash(bytes);
        }

        private static byte[] HmacSha256(string data, byte[] key)
        {
            return new HMACSHA256(key).ComputeHash(ToBytes(data));
        }

        #endregion
    }
}