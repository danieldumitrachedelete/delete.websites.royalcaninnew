﻿using System.Drawing;
using System;
using System.Text.RegularExpressions;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Glass.Mapper;

namespace Delete.Foundation.AssetsManagement.Services
{
    public class WeShareImageProcessor : IExternalImageProcessor
    {
        private static Regex WidthRegex = new Regex(@"w=(?<width>\d+)", RegexOptions.Compiled);
        private static Regex HeightRegex = new Regex(@"h=(?<height>\d+)", RegexOptions.Compiled);
        private static Regex PointForCutImageRegex = new Regex("([0-9]+)");

        public string Resize(string url, int width, string additionalParameters)
        {
            if (url.Empty())
            {
                return null;
            }

            var separator = url.Contains("?") ? "&" : "?";

            if (additionalParameters.NotEmpty())
            {
                var sizeIsPredefined = false;

                additionalParameters = additionalParameters.Trim('&', '?');

                int sourceWidth = 0, sourceHeight = 0;

                var widthMatch = WidthRegex.Match(additionalParameters);
                if (widthMatch.Success)
                {
                    sourceWidth = widthMatch.Groups["width"].Value.ToInt();
                }
                
                var heightMatch = HeightRegex.Match(additionalParameters);
                if (heightMatch.Success)
                {
                    sourceHeight = heightMatch.Groups["height"].Value.ToInt();
                }

               


                if (sourceWidth > 0 & sourceHeight > 0)
                {
                    sizeIsPredefined = true;
                    var newHeight = width * sourceHeight / sourceWidth;
                    additionalParameters = additionalParameters.Replace(widthMatch.Groups["width"].Value, width.ToString());
                    additionalParameters = additionalParameters.Replace(heightMatch.Groups["height"].Value, newHeight.ToString());
                }

                if (!sizeIsPredefined)
                {
                    additionalParameters = $"w={width}&{additionalParameters}";
                }

                return $"{url}{separator}{additionalParameters}";
            }

            return $"{url}{separator}w={width}";
        }

        public string Crop(string url, int width, int height)
        {
            if (url.Empty())
            {
                return null;
            }

            var separator = url.Contains("?") ? "&" : "?";
            return $"{url}{separator}w={width}&h={height}&fit=crop&crop=edges";
        }

        public bool GetSize(ref int width, ref int height, string additionalParameters)
        {
            if (additionalParameters.NotEmpty())
            {
                var points = PointForCutImageRegex.Matches(additionalParameters);
                if (points.Count == 4)
                {
                    width = points[2].Value.ToInt();
                    height = points[3].Value.ToInt();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }
    }
}
