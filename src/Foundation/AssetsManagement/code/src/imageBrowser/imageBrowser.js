import './ImageBrowser.scss';

export default class ImageBrowser {
    getTopWindow() {
        if (window.top.jQuery) return window.top;
        if (window.jQuery) return window;
        return null;
    }

    init() {
        if (typeof window.top.Sitecore === 'undefined') {
            window.top.Sitecore = {};
        }

        const browserInstance = this;

        window.top.Sitecore.openAdvancedMediaPicker = function (args) {
            browserInstance.openAdvancedMediaPicker(args);
        };
    }

    openAdvancedMediaPicker(args) {
        const popup = this.getPickerPopup();

        const pickerOptions = this.getImagePickerOptions();

        const browser = this;

        const imagePicker = popup.simpleGallery(pickerOptions);
        imagePicker.simpleGallery('Open', {
            album: '.',
            options: {
                selectMultiple: false
            },
            ok(items) {
                if (args.isCommand) {
                    browser.commandEditorCallback(items, args);
                } else {
                    browser.fieldEditorCallback(items, args);
                }
            }
        });
    }

    getPickerPopup() {
        const topWindow = this.getTopWindow();
        let popup = topWindow.find('#image-picker-container');

        if (!popup.length) {
            const $el = topWindow.jQuery('body');

            const $clear = topWindow.jQuery('<div id=\'image-picker-container\' class=\'image-picker-container\'></div>');
            $el.append($clear);
            popup = $el.find('#image-picker-container');
        }

        return popup;
    }

    getAlbumPath(args, cookieName) {
        const currentFolder = args.current;
        const sourceFolder = args.source;
        const lastUsedFolder = window.top.jQuery.cookie(cookieName);
        const defaultFolder = '/';

        let path;

        if (currentFolder) {
            path = currentFolder;
        } else if (sourceFolder) {
            if (lastUsedFolder) {
                path = lastUsedFolder;
            } else {
                path = sourceFolder;
            }
        } else if (lastUsedFolder) {
            path = lastUsedFolder;
        } else {
            path = defaultFolder;
        }

        return path;
    }

    getSelectedItem(items) {
        return items[0];
    }

    getImagePickerOptions() {
        return {
            jsonProvider: '/en-us/api/assetsmanagement/search',
            uploadProvider: '',
            album: '',
            showCheckboxes: true,
            paginationMaxLinesPerPage: 3,
            thumbnailWidth: 350,
            thumbnailHeight: 200,
            paginationDots: false,
            paginationSwipe: true,
            paginationVisiblePages: 10
        };
    }

    fieldEditorCallback(items, args) {
        const id = args.id;
        const item = this.getSelectedItem(items);
        let sender = window.scForm;

        const isExperienceEditor = document
            .querySelector('body')
            .classList.contains('sitecore-editing');
        if (isExperienceEditor) {
            sender =
                window.top.frames.jqueryModalDialogsFrame.contentWindow.frames.scContentIframeId0
                    .contentWindow.scForm;
        }

        sender.postRequest(
            '',
            '',
            '',
            args.command +
                ':save(id=' +
                id +
                ', value=' +
                item.src +
                ', date=' +
                item.date +
                ', w=' +
                item.width +
                ', h=' +
                item.height +
                ')'
        );
    }

    commandEditorCallback(items, args) {
        const id = args.id;
        const item = this.getSelectedItem(items);

        let iframe;

        for (let i = 0; i < window.top.frames.length; i++) {
            iframe = window.top.frames[i];
            try {
                const iframeId = iframe.frameElement.id;
                if (iframeId === 'scWebEditRibbon') {
                    break;
                }
            } catch (e) {}
        }

        iframe.window.scForm.postRequest(
            '',
            '',
            '',
            args.command +
                '(fieldid=' +
                id +
                ', result=' +
                item.src +
                ', date=' +
                item.date +
                ', w=' +
                item.width +
                ', h=' +
                item.height +
                ', itemid=' +
                args.itemid +
                ', language=' +
                args.lang +
                ', controlid=' +
                args.controlid +
                ', version=' +
                args.version +
                ', fieldValue="' +
                args.fieldValue +
                '")'
        );
    }
}
