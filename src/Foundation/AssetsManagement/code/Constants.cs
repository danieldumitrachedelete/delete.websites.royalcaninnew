﻿namespace Delete.Foundation.AssetsManagement
{
    public static class Constants
    {
        public static class Configuration
        {
            public static class AppSettings
            {
                public const string RegionName = "AssetsManagement.WeShare.RegionName";
                public const string ServiceName = "AssetsManagement.WeShare.ServiceName";
                public const string Algorithm = "AssetsManagement.WeShare.Algorithm";
                public const string ContentType = "AssetsManagement.WeShare.ContentType";
                public const string SignedHeaders = "AssetsManagement.WeShare.SignedHeaders";

                public const string Host = "AssetsManagement.WeShare.Host";
                public const string NewHost = "AssetsManagement.WeShare.NewHost";
                public const string Prefix = "AssetsManagement.WeShare.Prefix";

                public const string AccessKey = "AssetsManagement.WeShare.AccessKey";
                public const string SecretKey = "AssetsManagement.WeShare.SecretKey";

                public const string ClientId = "AssetsManagement.WeShare.ClientId";
                public const string ClientSecret = "AssetsManagement.WeShare.ClientSecret";
            }
        }
    }
}