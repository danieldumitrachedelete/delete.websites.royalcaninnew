﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;


namespace Delete.Foundation.AssetsManagement.Models
{
    public class WeShareSearchClasses
    {
        public class SearchResponse
        {
            [JsonProperty("took")] public int took { get; set; }

            [JsonProperty("timed_out")] public bool timed_out { get; set; }

            [JsonProperty("_shards")] public Shards _shards { get; set; }

            [JsonProperty("hits")] public Hits hits { get; set; }
        }

        public class Shards
        {
            [JsonProperty("total")] public int total { get; set; }

            [JsonProperty("successful")] public int successful { get; set; }

            [JsonProperty("skipped")] public int skipped { get; set; }

            [JsonProperty("failed")] public int failed { get; set; }
        }

        public class Hits
        {
            [JsonProperty("total")] public int total { get; set; }

            [JsonProperty("max_score")] public double? max_score { get; set; }

            [JsonProperty("hits")] public IList<Hit> hits { get; set; }
        }

        public class Hit
        {
            [JsonProperty("_index")] public string _index { get; set; }

            [JsonProperty("_type")] public string _type { get; set; }

            [JsonProperty("_id")] public string _id { get; set; }

            [JsonProperty("_score")] public double _score { get; set; }

            [JsonProperty("_source")] public Source _source { get; set; }
        }

        public class Source
        {
            [JsonProperty("author")] public string author { get; set; }
            
            [JsonProperty("combined_fields")] public string combined_fields { get; set; }

            [JsonProperty("country")] public string country { get; set; }

            [JsonProperty("id")] public long id { get; set; }

            [JsonProperty("last_indexed_date")] public DateTime last_indexed_date { get; set; }

            [JsonProperty("metadata")] public Metadata metadata { get; set; }

            [JsonProperty("name")] public string name { get; set; }

            [JsonProperty("public_url")] public string public_url { get; set; }

            [JsonProperty("relations")] public object relations { get; set; }
            
            [JsonProperty("status")] public string status { get; set; }

            [JsonProperty("storage_items_keys")] public IList<string> storage_items_keys { get; set; }

            [JsonProperty("thumbnail")] public Thumbnail thumbnail { get; set; }

            [JsonProperty("type")] public string type { get; set; }

            [JsonProperty("updated")] public DateTime updated { get; set; }

            [JsonProperty("visibility")] public string visibility { get; set; }
        }

        public class Thumbnail
        {
            [JsonProperty("mime_type")] public string mime_type { get; set; }

            [JsonProperty("base64")] public string base64 { get; set; }
        }

        public class Metadata
        {
            [JsonProperty("Breed Identity")] public dynamic BreedIdentity{ get; set; }

            [JsonProperty("Characteristics")] public dynamic Characteristics { get; set; }

            [JsonProperty("Details")] public dynamic Details { get; set; }

            [JsonProperty("FCI information")] public dynamic FciInformation { get; set; }

            [JsonProperty("Hidden")] public dynamic Hidden { get; set; }

            [JsonProperty("Lifestage")] public dynamic Lifestage { get; set; }

            [JsonProperty("Overview")] public dynamic Overview { get; set; }

            [JsonProperty("Relations")] public dynamic Relations { get; set; }

            [JsonProperty("Specifications")] public Specifications Specifications { get; set; }
        }

        public class Specifications
        {
            [JsonProperty("File / Color space")] public string colorSpace { get; set; }
            [JsonProperty("File / File size")] public string fileSize { get; set; }
            [JsonProperty("File / Image size")] public string imageSize { get; set; }
            [JsonProperty("File / Resolution")] public string resolution{ get; set; }
        }
    }
}