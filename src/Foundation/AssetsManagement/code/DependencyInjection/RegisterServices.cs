using Delete.Foundation.AssetsManagement.Services;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.InitializeDependencyInjection.Extensions;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.DependencyInjection;

namespace Delete.Foundation.AssetsManagement.DependencyInjection
{
    public class RegisterServices : IServicesConfigurator
    {
        public void Configure(IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IWeShareService, WeShareService>();
            serviceCollection.AddScoped<IExternalImageProcessor, WeShareImageProcessor>();

            serviceCollection.AddMvcControllersInCurrentAssembly();
        }
    }
}
