﻿using System;
using Sitecore.Data;

namespace Delete.Foundation.RoyalCaninCore
{
    public static class Constants
    {
        public static class Search
        {
            public static class BoostLevels
            {
                public static float Tertiary => 0.25f;
                public static float Secondary => 0.5f;
                public static float Primary => 1.0f;
            }
        }

        public static class SitecoreIds
        {
            public static class FilterControlTypes
            {
                public const string SliderControlTypeId = "{abfb2db8-62d7-4c7f-8a1e-c7ba50b74be1}";
                public static readonly ID SliderId = new ID(SliderControlTypeId);
            }

            public static class FeeedingPeriodConvertions
            {
                public const string ItemID = "{e37b7020-b4d7-40df-9965-6714c3a19d38}";
                public static readonly ID FolderID = new ID(ItemID);
            }

            public static class ImageOverrideParams
            {
                public const string ItemID = "{11b5e6c3-1733-43fc-b506-d15a998c0c69}";
                public static readonly ID ImageOverrideParamsFolderID = new ID(ItemID);
            }
        }

        public static class FilterByPlatform
        {
            public const string WeShare = "weshare";
        }
    }
}