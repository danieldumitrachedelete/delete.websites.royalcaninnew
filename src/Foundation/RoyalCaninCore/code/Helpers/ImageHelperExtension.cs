﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using Delete.Foundation.DataTemplates.Managers;
using Delete.Foundation.DataTemplates.Models.Dictionary;
using Delete.Foundation.DeleteFoundationCore.Models;

namespace Delete.Foundation.RoyalCaninCore.Helpers
{
    public static class ImageHelperExtension
    {
        private static readonly IDictionaryEntryManager DictionaryEntryManager = new DictionaryEntryManager();

        private static List<DictionaryEntry> _globalImageParams;

        public static string Test { get; set; }

        public static List<DictionaryEntry> GlobalImageParams
        {
            get
            {
                if (_globalImageParams == null)
                {
                    var dictionaryManager = new DictionaryEntryManager();
                    var items = dictionaryManager.GetAllInFolder(Constants.SitecoreIds.ImageOverrideParams
                        .ImageOverrideParamsFolderID.Guid)?.ToList();
                    _globalImageParams = items;
                }

                return _globalImageParams;
            }
        }

        public static ExtendedImage GetExtendedImageWithGlobalSettings(this ExtendedImage image, string platform = "")
        {
            

            if (GlobalImageParams != null && GlobalImageParams.Any() && !string.IsNullOrEmpty(image.Src) && image.Src.Contains(platform) && image.AdditionalParameters != null)
            {
                var qs = HttpUtility.ParseQueryString(image.AdditionalParameters);

                foreach (var globalImageParam in _globalImageParams)
                {
                    qs.Set(globalImageParam.Value, globalImageParam.Text);
                }

                image.AdditionalParameters = qs.ToString();
            }

            return image;
        }
    }
}