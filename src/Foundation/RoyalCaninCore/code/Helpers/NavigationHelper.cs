﻿using System;

namespace Delete.Foundation.RoyalCaninCore.Helpers
{
    public static class NavigationHelper
    {
        public static int GetPagesCount(int itemsCount, int pageSize)
        {
            if (pageSize == 0) return 0;
            return (int)Math.Ceiling((decimal)itemsCount / pageSize);
        }
    }
}