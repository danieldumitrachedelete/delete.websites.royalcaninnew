﻿namespace Delete.Foundation.RoyalCaninCore.Helpers
{
    using System.IO;
    using System.IO.Compression;

    using Castle.Core.Internal;

    public static class FileHelper
    {
        public static string AddLanguageToFilepath(string filepath, string language)
        {
            var filename = AddLanguageToFilename(Path.GetFileName(filepath), language);
            return Path.Combine(Path.GetDirectoryName(filepath) ?? string.Empty, filename);
        }

        public static string AddLanguageToFilename(string filename, string language)
        {
            var fileExtension = Path.GetExtension(filename);
            var filenameWithoutExtension = Path.GetFileNameWithoutExtension(filename);

            return $"{filenameWithoutExtension}{(language.IsNullOrEmpty() ? string.Empty : ".")}{language}{fileExtension}";
        }

        public static string GZipCompress(string sourceFileName)
        {
            using (var siteMapFile = new FileStream(sourceFileName, FileMode.Open))
            {
                var outputFileName = $"{sourceFileName}.gz";

                if (File.Exists(outputFileName))
                {
                    File.Delete(outputFileName);
                }

                using (var compressedFile = File.Create(outputFileName))
                {
                    using (var gzipStream = new GZipStream(compressedFile, CompressionMode.Compress))
                    {
                        siteMapFile.CopyTo(gzipStream);
                    }
                }

                return outputFileName;
            }
        }
    }
}