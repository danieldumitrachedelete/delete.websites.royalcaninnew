﻿using JetBrains.Annotations;
using Sitecore.Data.Items;
using Sitecore.Pipelines.GetLookupSourceItems;
using Sitecore.Shell.Applications.ContentEditor;
using Sitecore.Text;
using Sitecore.Web;

namespace Delete.Foundation.RoyalCaninCore.Pipeline
{
    [UsedImplicitly]
    public class ResolveRelativeQuerySource
    {
        public void Process(GetLookupSourceItemsArgs args)
        {
            if (!args.Source.StartsWith("query:")) return;

            Item contextItem = null;
            var url = WebUtil.GetQueryString();
            if (string.IsNullOrWhiteSpace(url) || !url.Contains("hdl")) return;
            
            var parameters = FieldEditorOptions.Parse(new UrlString(url)).Parameters;
            var currentItemId = parameters["contentitem"];
            if (string.IsNullOrEmpty(currentItemId)) return;
            
            var contentItemUri = new Sitecore.Data.ItemUri(currentItemId);
            contextItem = Sitecore.Data.Database.GetItem(contentItemUri);
            args.Item = contextItem;
        }
    }
}