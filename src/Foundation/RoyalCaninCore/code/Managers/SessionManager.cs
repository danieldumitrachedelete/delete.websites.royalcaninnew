﻿using System;
using System.Web;

namespace Delete.Foundation.RoyalCaninCore.Managers
{
    [Serializable]
    public class SessionManager<T> where T : SessionManager<T>, new()
    {
        protected SessionManager()
        {
        }

        private static string Key => typeof(SessionManager<T>).FullName;

        private static T Value
        {
            get => HttpContext.Current.Session[Key] as T;
            set => HttpContext.Current.Session[Key] = value;
        }

        public static T GetCurrent()
        {
            return new Lazy<T>(() => Value ?? (Value = new T())).Value;
        }
    }
}