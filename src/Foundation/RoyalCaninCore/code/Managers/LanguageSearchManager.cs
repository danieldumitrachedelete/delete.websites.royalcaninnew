﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.DeleteFoundationCore.Managers;
using Delete.Foundation.RoyalCaninCore.Models;

namespace Delete.Foundation.RoyalCaninCore.Managers
{
    public interface ILanguageSearchManager : IBaseSearchManager<Language>
    {
        IEnumerable<Language> GetAll();
    }

    public class LanguageSearchManager : BaseSearchManager<Language>, ILanguageSearchManager
    {
        public IEnumerable<Language> GetAll()
        {
            return GetQueryable()
                .MapResults(SitecoreContext, true);
        }

        protected override IQueryable<Language> GetQueryable()
        {
            return base.GetQueryable().Where(x => x.TemplateId == LanguageConstants.TemplateId);
        }
    }
}