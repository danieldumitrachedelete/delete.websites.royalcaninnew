﻿namespace Delete.Foundation.RoyalCaninCore.Models
{
    using Helpers;

    public class PaginatedResults
    {
        private const int InitialPageIndex = 1;

        private int page;

        public int TotalResults { get; set; }

        public int Page
        {
            get
            {
                if (this.page < InitialPageIndex)
                {
                    return this.page = InitialPageIndex;
                }

                return this.page > this.TotalPages ? this.TotalPages : this.page;
            }

            set => this.page = value;
        }

        public int PageSize { get; set; }

        public int TotalPages => NavigationHelper.GetPagesCount(this.TotalResults, this.PageSize);

        public int GetSkippedItemsCountToCurrentPage()
        {
            return (this.Page - InitialPageIndex) * this.PageSize;
        }
    }
}