﻿using System;
using Delete.Foundation.RoyalCaninCore.Models;
using Glass.Mapper;

namespace Delete.Foundation.RoyalCaninCore.Extensions
{
    using DeleteFoundationCore;

    public static class LanguageExtensions
    {
        public static bool IsDefaultLanguage(this string language)
        {
            return language.Equals(Configuration.DefaultLanguage, StringComparison.InvariantCultureIgnoreCase);
        }

        public static string GetRegionalIsoCodeWithFallback(this ILanguage language)
        {
            if (language == null)
            {
                return string.Empty;
            }

            if (language.RegionalIsoCode.HasValue())
            {
                return language.RegionalIsoCode;
            }

            return language.Name;
        }
    }
}