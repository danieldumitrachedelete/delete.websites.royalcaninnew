﻿using System;
using Sitecore.Data.Items;

namespace Delete.Foundation.RoyalCaninCore.Extensions
{
    public static class SitecoreItemExtensions
    {
        public static bool HasContextLanguage(this Item item)
        {
            if (item?.Versions == null || item.Versions.Count == 0) return false;

            var latestLanguageVersion = item.Versions.GetLatestVersion();
            return (latestLanguageVersion != null) && (latestLanguageVersion.Versions.Count > 0);
        }
    }
}