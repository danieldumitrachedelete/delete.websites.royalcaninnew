﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Serialization;

namespace Delete.Foundation.RoyalCaninCore.Extensions
{
    public static class ObjectExtensions
    {
        public static string ToLowerCaseJson(this object anObject)
        {
            return anObject.ToJson(new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                StringEscapeHandling = StringEscapeHandling.EscapeHtml
            });
        }

        public static string ToJson(this object item, JsonSerializerSettings jsonSerializerSettings = null)
        {
            if (jsonSerializerSettings == null)
            {
                jsonSerializerSettings = DefaultSettings;
            }

            var jsonString = JsonConvert.SerializeObject(item, Formatting.None, jsonSerializerSettings);

            return jsonString;
        }        

        private static readonly JsonSerializerSettings DefaultSettings = new JsonSerializerSettings
        {
            NullValueHandling = NullValueHandling.Ignore,
            StringEscapeHandling = StringEscapeHandling.EscapeHtml
        };
    }
}