﻿namespace Delete.Foundation.RoyalCaninCore.Extensions
{
    using System;
    using System.IO;

    public static class FileInfoExtensions
    {
        public static bool IsCsv(this FileInfo fileInfo)
        {
            return fileInfo.HasExtension(".csv");
        }

        public static bool IsXlsx(this FileInfo fileInfo)
        {
            return fileInfo.HasExtension(".xlsx");
        }

        private static bool HasExtension(this FileInfo fileInfo, string extension)
        {
            return fileInfo != null && fileInfo.Extension.Equals(extension, StringComparison.InvariantCultureIgnoreCase);
        }
    }
}