﻿using System.Web.Mvc;
using System.Web.Mvc.Html;
using Castle.Core.Internal;
using Delete.Foundation.DataTemplates.Models.Dictionary;
using Delete.Foundation.DeleteFoundationCore.Models;
using Glass.Mapper.Sc.Fields;

namespace Delete.Foundation.DesignLanguage.Extensions
{
    public static class HtmlHelperExtensions
    {
        public static MvcHtmlString Button(this HtmlHelper helper, Link link, object htmlAttributes = null)
        {
            if (link != null)
            {
                helper.ViewBag.HtmlAttributes = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);
                return helper.Partial("~/Views/Foundation/DesignLanguage/Link.cshtml", link);
            }

            return new MvcHtmlString(string.Empty);
        }

        public static MvcHtmlString Background(this HtmlHelper helper, IDictionaryEntry backgroundType, ExtendedImage backgroundImage, File backgroundVideo)
        {
            if (backgroundType == null)
            {
                return new MvcHtmlString(string.Empty);
            }

            if (backgroundType.Value.Contains("background-image") && backgroundImage != null)
            {
                if (backgroundType.Value == "background-image-parallax")
                {
                    return helper.BackgroundImageParallax(backgroundImage);
                }

                if (backgroundType.Value == "background-image-responsive")
                {
                    return helper.BackgroundImageResponsive(backgroundImage);
                }

                if (backgroundType.Value == "background-image-responsive-cover")
                {
                    return helper.BackgroundImageResponsive(backgroundImage, "--cover");
                }

                return helper.BackgroundImage(backgroundImage);
            }

            if (backgroundType.Value.Contains("background-video") && backgroundVideo != null)
            {
                return helper.BackgroundVideo(backgroundVideo);
            }

            return new MvcHtmlString(string.Empty);
        }

        public static MvcHtmlString BackgroundImage(this HtmlHelper helper, ExtendedImage image)
        {
            if (image != null)
            {
                return helper.Partial("~/Views/Foundation/DesignLanguage/BackgroundImage.cshtml", image);
            }

            return new MvcHtmlString(string.Empty);
        }

        public static MvcHtmlString BackgroundImageParallax(this HtmlHelper helper, ExtendedImage image)
        {
            if (image != null)
            {
                return helper.Partial("~/Views/Foundation/DesignLanguage/BackgroundImageParallax.cshtml", image);
            }

            return new MvcHtmlString(string.Empty);
        }
        
        public static MvcHtmlString BackgroundImageResponsive(this HtmlHelper helper, ExtendedImage image, string modifier = "")
        {
            if (image != null)
            {
                return helper.Partial("~/Views/Foundation/DesignLanguage/BackgroundImageResponsive.cshtml", image, new ViewDataDictionary(new {modifier}));
            }

            return new MvcHtmlString(string.Empty);
        }

        public static MvcHtmlString BackgroundVideo(this HtmlHelper helper, File video)
        {
            if (video != null)
            {
                return helper.Partial("~/Views/Foundation/DesignLanguage/BackgroundVideo.cshtml", video);
            }

            return new MvcHtmlString(string.Empty);
        }

        public static string GetContentBlockLayoutConteinerColumnCssClass(this HtmlHelper helper, IDictionaryEntry entry)
        {
            return entry?.Value ?? string.Empty;
        }

        public static bool ShouldRenderSecondColumn(this HtmlHelper helper, IDictionaryEntry entry)
        {
            return entry != null && entry.Value != "rc-one-column";
        }

        public static string GetColumnWidth(this HtmlHelper helper, IDictionaryEntry entry, string doubleWidthType)
        {
            if (entry == null || entry.Value.IsNullOrEmpty())
            {
                return string.Empty;
            }

            if (entry.Value.Contains(doubleWidthType))
            {
                return "rc-double-width";
            }

            return string.Empty;
        }
    }
}