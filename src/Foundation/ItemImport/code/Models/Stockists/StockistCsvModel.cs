﻿namespace Delete.Foundation.ItemImport.Models.Stockists
{
    public class StockistCsvModel : StockistImportFileModel
    {
        public string AddContactType { get; set; }

        public string ProfileType { get; set; }

        public string AddressLine2 {get; set; }

        public string AddressLine3 {get; set; }

        public string Country { get; set; }

        public string Fax { get; set; }

        public string Email { get; set; }

        public string WebsiteURL { get; set; }

        public string Description { get; set; }

        public string State { get; set; }
    }
}