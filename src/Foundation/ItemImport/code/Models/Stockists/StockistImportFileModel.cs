﻿namespace Delete.Foundation.ItemImport.Models.Stockists
{
    public class StockistImportFileModel
    {
        public string CompanyName { get; set; }

        public string UniqueCustomerId { get; set; }

        public string ContactType { get; set; }

        public string Address { get; set; }

        public string City { get; set; }

        public string Postcode { get; set; }

        public string Phone { get; set; }

        public double? Latitude { get; set; }
        
        public double? Longitude { get; set; }
    }
}