﻿using Delete.Foundation.DeleteFoundationCore.Models;

namespace Delete.Foundation.ItemImport.Models
{
    public class ImageDto
    {
        public string Url { get; set; }

        public string Alt { get; set; }

	    public ExternalImage ExternalImage { get; set; }
    }
}