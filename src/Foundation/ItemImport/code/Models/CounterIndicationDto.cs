﻿namespace Delete.Foundation.ItemImport.Models
{
    public class CounterIndicationDto
    {
        public string Id { get; set; }

        public string Name { get; set; }
    }
}