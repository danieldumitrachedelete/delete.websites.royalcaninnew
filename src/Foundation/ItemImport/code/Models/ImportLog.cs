﻿using System;
using System.Collections.Generic;

namespace Delete.Foundation.ItemImport.Models
{
   
    public enum MessageLevel
    {
        Info, Error, Critical
    }

    public enum ImportAction
    {
        Undefined, Imported, Rejected, Skipped, Deleted, New, Updated, NewImage, UpdatedImage
    }

    public class ImportLog
    {
        public ImportLog()
        {
            this.Entries = new List<ImportLogEntry>();
            this.ItemsProcessed = 0;
        }

        public List<ImportLogEntry> Entries { get; set; }

        public int ItemsProcessed { get; set; }
    }

    public class ImportLogEntry
    {
        public Guid Id { get; set; }

        public string Message { get; set; }

        public MessageLevel Level { get; set; }

        public ImportAction Action { get; set; }
    }

    public class SkippableImportLogException : ImportLogException
    {
        public ImportLogEntry Entry { get; set; }
    }

    public class ImportLogException : Exception
    {
    }
}