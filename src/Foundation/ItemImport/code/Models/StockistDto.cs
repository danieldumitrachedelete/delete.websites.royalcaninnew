﻿using System.Collections.Generic;

namespace Delete.Foundation.ItemImport.Models
{
    public class StockistDto
    {
        public string CustomerReference { get; set; }

        public string CompanyName { get; set; }

        public IEnumerable<ContactTypeDto> ContactTypes { get; set; }

        public string Phone { get; set; }

        public string Fax { get; set; }

        public string Email { get; set; }

        public string WebsiteUrl { get; set; }

        public string Country { get; set; }

        public string CountyOrState { get; set; }
        
        public string Postcode { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string AddressLine3 { get; set; }

        public string City { get; set; }

        public string Description { get; set; }

        public ProfileTypeDto ProfileType { get; set; }

        public decimal Latitude { get; set; }

        public decimal Longitude { get; set; }

        public GooglePlaceDto GooglePlaceData { get; set; }
    }
}