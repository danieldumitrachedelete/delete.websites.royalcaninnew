﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using Delete.Foundation.DataTemplates.Models.Taxonomy;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Delete.Foundation.ItemImport.Models.SalsifyImportFeed
{
    public partial class SalsifyProductImport
    {
        [JsonProperty("products", NullValueHandling = NullValueHandling.Ignore)]
        public Product[] Products { get; set; }
    }

    public class PackagingImport
    {
        public string SitecorePackSku { get; set; }

        public decimal SitecorePackSize { get; set; }

        public string SitecorePackUnit { get; set; }

        public string SitecorePackUpc { get; set; }

        public string SitecorePackEan { get; set; }
    }

    public partial class Product
    {
        public bool CheckSpeciesDogandCat()
        {
            if (this.SitecoreSpecies.ToLower() == "cat/dog" || this.SitecoreSpecies.ToLower() == "dog/cat")
            {
                return true;
            }

            return false;

        }
        public bool IsCat => string.Equals(this.SitecoreSpecies, "cat", StringComparison.InvariantCultureIgnoreCase);

        public bool IsDog => string.Equals(this.SitecoreSpecies, "dog", StringComparison.InvariantCultureIgnoreCase);

        public Species Species => this.IsCat ? Species.Cats : Species.Dogs;

        public bool IsCatDog => CheckSpeciesDogandCat();

        public PackagingImport CurrentPackaging => new PackagingImport
        {
            SitecorePackEan = this.SitecorePackEan,
            SitecorePackSku = this.SitecorePackSku,
            SitecorePackSize = this.SitecorePackSize,
            SitecorePackUnit = this.SitecorePackUnit,
            SitecorePackUpc = this.SitecorePackUpc
        };

        public IList<PackagingImport> Packages = new List<PackagingImport>();
    }

    public partial class Product
    {
        [JsonProperty("Sitecore - Ages")]
        public string SitecoreAges { get; set; }

        [JsonProperty("Sitecore - Benefit Text", NullValueHandling = NullValueHandling.Ignore)]
        public string SitecoreBenefitText { get; set; }

        [JsonProperty("Sitecore - Feeding Period From")]
        public string SitecoreFeedingPeriodFrom { get; set; }

        [JsonProperty("Sitecore - Feeding Period To")]
        public string SitecoreFeedingPeriodTo { get; set; }

        [JsonProperty("Sitecore - Image 1", NullValueHandling = NullValueHandling.Ignore)]
        public string SitecoreImage1 { get; set; }

        [JsonProperty("Sitecore - Image 1 Alt", NullValueHandling = NullValueHandling.Ignore)]
        public string SitecoreImage1Alt { get; set; }

        [JsonProperty("Sitecore - Image 2", NullValueHandling = NullValueHandling.Ignore)]
        public string SitecoreImage2 { get; set; }

        [JsonProperty("Sitecore - Image 2 Alt", NullValueHandling = NullValueHandling.Ignore)]
        public string SitecoreImage2Alt { get; set; }

        [JsonProperty("Sitecore - Image 3", NullValueHandling = NullValueHandling.Ignore)]
        public string SitecoreImage3 { get; set; }

        [JsonProperty("Sitecore - Image 3 Alt", NullValueHandling = NullValueHandling.Ignore)]
        public string SitecoreImage3Alt { get; set; }

        [JsonProperty("Sitecore - Image 4", NullValueHandling = NullValueHandling.Ignore)]
        public string SitecoreImage4 { get; set; }

        [JsonProperty("Sitecore - Image 4 Alt", NullValueHandling = NullValueHandling.Ignore)]
        public string SitecoreImage4Alt { get; set; }

        [JsonProperty("Sitecore - Image 5", NullValueHandling = NullValueHandling.Ignore)]
        public string SitecoreImage5 { get; set; }

        [JsonProperty("Sitecore - Image 5 Alt", NullValueHandling = NullValueHandling.Ignore)]
        public string SitecoreImage5Alt { get; set; }

        [JsonProperty("Sitecore - Image 6", NullValueHandling = NullValueHandling.Ignore)]
        public string SitecoreImage6 { get; set; }

        [JsonProperty("Sitecore - Image 6 Alt", NullValueHandling = NullValueHandling.Ignore)]
        public string SitecoreImage6Alt { get; set; }

        [JsonProperty("Sitecore - Image 7", NullValueHandling = NullValueHandling.Ignore)]
        public string SitecoreImage7 { get; set; }

        [JsonProperty("Sitecore - Image 7 Alt", NullValueHandling = NullValueHandling.Ignore)]
        public string SitecoreImage7Alt { get; set; }

        [JsonProperty("Sitecore - Image 8", NullValueHandling = NullValueHandling.Ignore)]
        public string SitecoreImage8 { get; set; }

        [JsonProperty("Sitecore - Image 8 Alt", NullValueHandling = NullValueHandling.Ignore)]
        public string SitecoreImage8Alt { get; set; }

        [JsonProperty("Sitecore - Feeding Guide Image", NullValueHandling = NullValueHandling.Ignore)]
        public string SitecoreFeedingGuideImage { get; set; }

        [JsonProperty("Sitecore - Feeding Guide Image Alt", NullValueHandling = NullValueHandling.Ignore)]
        public string SitecoreFeedingGuideImageAlt { get; set; }
        
        [JsonProperty("Sitecore - Feeding Guide", NullValueHandling = NullValueHandling.Ignore)]
        public string SitecoreFeedingGuide { get; set; }

        [JsonProperty("Sitecore - Ingredients")]
        public string SitecoreIngredients { get; set; }

        [JsonProperty("Sitecore - Keywords")]
        public string SitecoreKeywords { get; set; }

        [JsonProperty("Sitecore - Last Date Modified")]
        public DateTime SitecoreLastDateModified { get; set; }

        [JsonProperty("Sitecore - Main benefit")]
        public string SitecoreMainBenefit { get; set; }

        [JsonProperty("Sitecore - Main Description")]
        public string SitecoreMainDescription { get; set; }

        [JsonProperty("Sitecore - Main ITEM ID", NullValueHandling = NullValueHandling.Ignore)]
        public string SitecoreMainItemId { get; set; }

        [JsonProperty("Sitecore - Pack EAN")]
        public string SitecorePackEan { get; set; }

        [JsonProperty("Sitecore - Pack Quantity", NullValueHandling = NullValueHandling.Ignore)]
        public string SitecorePackQuantity { get; set; }

        [JsonProperty("Sitecore - Pack Size")]
        public decimal SitecorePackSize { get; set; }

        [JsonProperty("Sitecore - Pack SKU")]
        public string SitecorePackSku { get; set; }

        [JsonProperty("Sitecore - Pack Unit")]
        public string SitecorePackUnit { get; set; }

        [JsonProperty("Sitecore - Pack UPC")]
        public string SitecorePackUpc { get; set; }

        [JsonProperty("Sitecore - Pillar ID Code")]
        public string SitecorePillarIdCode { get; set; }

        [JsonProperty("Sitecore - Product Description")]
        public string SitecoreProductDescription { get; set; }

        [JsonProperty("Sitecore - Product Name")]
        public string SitecoreProductName { get; set; }

        [JsonProperty("Sitecore - Product Status", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ParseStringConverter))]
        public int SitecoreProductStatus { get; set; }

        [JsonProperty("Sitecore - Product Type")]
        public string SitecoreProductType { get; set; }

        [JsonProperty("Sitecore - Prouct Type Name")]
        public string SitecoreProuctTypeName { get; set; }

        [JsonProperty("Sitecore - Secondary Benefit", NullValueHandling = NullValueHandling.Ignore)]
        public string SitecoreSecondaryBenefit { get; set; }

        [JsonProperty("Sitecore - SKU")]
        public string SitecoreSku { get; set; }

        [JsonProperty("Sitecore - Species")]
        public string SitecoreSpecies { get; set; }

        [JsonProperty("Sitecore - Fourth Benefit", NullValueHandling = NullValueHandling.Ignore)]
        public string SitecoreFourthBenefit { get; set; }

        [JsonProperty("Sitecore - Fifth Benefit", NullValueHandling = NullValueHandling.Ignore)]
        public string SitecoreFifthBenefit { get; set; }

        [JsonProperty("Sitecore - Sixth Benefit", NullValueHandling = NullValueHandling.Ignore)]
        public string SitecoreSixthBenefit { get; set; }

        [JsonProperty("Sitecore - Animal Size", NullValueHandling = NullValueHandling.Ignore)]
        public string SitecoreAnimalSize { get; set; }

        [JsonProperty("Sitecore - BreedID", NullValueHandling = NullValueHandling.Ignore)]
        public string SitecoreBreedId { get; set; }

        [JsonProperty("Sitecore - Is Sterilized", NullValueHandling = NullValueHandling.Ignore)]
        public string SitecoreIsSterilized { get; set; }

        [JsonProperty("Sitecore - Special Need", NullValueHandling = NullValueHandling.Ignore)]
        public string SitecoreSpecialNeed { get; set; }

        [JsonProperty("Sitecore - Images Source", NullValueHandling = NullValueHandling.Ignore)]
        public bool SitecoreImagesSource { get; set; }

	    [JsonProperty("Sitecore - Guaranteed Analysis")]
	    public string SitecoreGuaranteedAnalysis { get; set; }
	}

    public partial class SalsifyProductImport
    {
        public static SalsifyProductImport[] FromJson(string json) => JsonConvert.DeserializeObject<SalsifyProductImport[]>(json, Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                // SalsifyAttributeGroupConverter.Singleton,
                ParseStringConverter.Singleton,
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }

    internal class ParseStringConverter : JsonConverter
    {
        public static readonly ParseStringConverter Singleton = new ParseStringConverter();

        public override bool CanConvert(Type t) => t == typeof(int) || t == typeof(int?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            if (int.TryParse(value, out var result))
            {
                return result;
            }

            return -1;
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }

            var value = (long)untypedValue;
            serializer.Serialize(writer, value.ToString());
            return;
        }
    }
}