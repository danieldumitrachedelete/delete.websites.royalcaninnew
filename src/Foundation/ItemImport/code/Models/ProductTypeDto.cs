﻿namespace Delete.Foundation.ItemImport.Models
{
    public class ProductTypeDto
    {
        public string Name { get; set; }
    }
}