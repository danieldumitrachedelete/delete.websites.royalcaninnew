﻿namespace Delete.Foundation.ItemImport.Models
{
    using System;

    using DataTemplates.Models.Taxonomy;

    public class ImportedProductDto
    {
        public DateTime UpdatedAt { get; set; }

        public string MainItemId { get; set; }

        public DateTime CreatedAt { get; set; }

        public string FeedingPeriodFrom { get; set; }

        public string FeedingPeriodTo { get; set; }

        public string Ingredients { get; set; }

        public string Tags { get; set; }

        public string Keywords { get;set; }

        public string BenefitText { get;set; }

        public string MainBenefit { get; set; }

        public string SecondaryBenefit { get; set; }

        public string MainDescription { get; set; }

        public string Text { get; set; }

        public string ProductDescription { get; set; }

        public string ProductName { get; set; }

        public int ProductStatus { get; set; }

        public string ProductTypeName { get; set; }

		public string SKU { get; set; }

        public string EAN { get; set; }

        public Species Species { get; set; }

        public bool IsDog { get; set; }

        public bool IsCatDog { get; set; }

        public CounterIndicationDto[] CounterIndications { get; set; }

        public IndicationDto[] Indications { get; set; }

        public SpecialNeedDto[] SpecialNeeds { get; set; }

        public SpecialNeedDto IsSterilized { get; set; }

        public ImageDto Image1 { get; set; }

        public ImageDto Image2 { get; set; }

        public ImageDto Image3 { get; set; }

        public ImageDto Image4 { get; set; }

        public ImageDto Image5 { get; set; }

        public ImageDto Image6 { get; set; }

        public ImageDto Image7 { get; set; }

        public ImageDto Image8 { get; set; }

        public ImageDto FeedingGuideImage { get; set; }

        public PackagingDto[] Packaging { get; set; }

        public LifestageDto[] Ages { get; set; }

        public BreedDto Breed { get; set; }

        public ProductTypeDto ProductType { get; set; }

	    public ProductTypeDto Technology { get; set; }

		public PillarDto Pillar { get; set; }

        public AnimalSizeDto[] AnimalSizes { get; set; }

	    public string GuaranteedAnalysis { get; set; }

	    public string BenefitText4 { get; set; }

	    public string BenefitText5 { get; set; }

	    public string BenefitText6 { get; set; }

	    public string BenefitTitle1 { get; set; }

	    public string BenefitTitle2 { get; set; }

	    public string BenefitTitle3 { get; set; }

	    public string BenefitTitle4 { get; set; }

	    public string BenefitTitle5 { get; set; }

	    public string BenefitTitle6 { get; set; }

	    public string FeedingGuide { get; set; }

	    public string Additives { get; set; }

	    public string AConstituents { get; set; }
	}
}