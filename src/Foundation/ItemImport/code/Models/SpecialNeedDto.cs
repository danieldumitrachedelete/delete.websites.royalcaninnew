﻿namespace Delete.Foundation.ItemImport.Models
{
    public class SpecialNeedDto
    {
        public string Name { get; set; }

        public string Key { get; set; }
    }
}