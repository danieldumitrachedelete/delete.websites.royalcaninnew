﻿namespace Delete.Foundation.ItemImport.Models
{
    public class IndicationDto
    {
        public string Name { get; set; }

        public string Id { get; set; }
    }
}