﻿namespace Delete.Foundation.ItemImport.Models
{
    public class BreedDto
    {
        public string Name { get; set; }

        public string BreedId { get; set; }
    }
}