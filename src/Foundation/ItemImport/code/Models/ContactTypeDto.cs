﻿namespace Delete.Foundation.ItemImport.Models
{
    public class ContactTypeDto
    {
        public string Code { get; set; }
    }
}