﻿namespace Delete.Foundation.ItemImport.Models
{
    public class PackagingDto
    {
        public decimal Size { get; set; }

        public string SKU { get; set; }

        public string EAN { get; set; }

        public string UPC { get; set; }

        public MeasurementUnitDto Unit { get; set; }
    }
}