﻿using System.IO;
using Delete.Foundation.DeleteFoundationCore.Models;
using Glass.Mapper.Sc.Configuration.Attributes;

namespace Delete.Foundation.ItemImport.Models
{
    [SitecoreType(AutoMap = true, TemplateId = "{DAF085E8-602E-43A6-8299-038FF171349F}")]
    public class GlassMediaItem : GlassBase, IGlassBase
    {
        //[SitecoreField("Mime Type")]
        //public virtual string MimeType { get; set; }

        //[SitecoreField("__Icon")]
        //public virtual string Icon { get; set; }

        //public virtual Guid Id { get; set; }
        //public virtual byte[] Blob { get; set; }
        //public virtual Stream Blob { get; set; }
        //public virtual string Extension { get; set; }
        //public virtual int Size { get; set; }
        //public virtual string Alt { get; set; }
        //public virtual int Width { get; set; }
        //public virtual int Height { get; set; }

        //keep adding other field that you want to copy here
    }

    [SitecoreType(TemplateId = "{FE5DD826-48C6-436D-B87A-7C4210C7413B}")] //, Cachable = true
    public class MediaFolder : GlassBase, IGlassBase
    {
    }
}