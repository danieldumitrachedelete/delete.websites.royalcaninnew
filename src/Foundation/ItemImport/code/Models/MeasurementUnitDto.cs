﻿namespace Delete.Foundation.ItemImport.Models
{
    public class MeasurementUnitDto
    {
        public string Code { get; set; }
    }
}