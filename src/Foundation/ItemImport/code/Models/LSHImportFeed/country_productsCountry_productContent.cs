﻿namespace Delete.Foundation.ItemImport.Models.LSHImportFeed
{
    // ReSharper disable once InconsistentNaming
    public partial class country_productsCountry_productContent
    {
        public bool IsImageContent => this.typeField == TypeConstants.ImageContent;

        public bool IsImageParagraphContent => this.typeField == TypeConstants.ImageParagraphContent;

        public bool IsParagraphContent => this.typeField == TypeConstants.ParagraphContent;

        public bool IsTextContent => this.typeField == TypeConstants.TextContent;

        public bool IsMetaTagsKeywords => this.nomenclatura.key == NomenclaturaKeyConstants.MetaTagsKeywords;

        public bool IsMetaTagsDescription => this.nomenclatura.key == NomenclaturaKeyConstants.MetaTagsDescription;

        public bool IsProductHeader => this.nomenclatura.key == NomenclaturaKeyConstants.ProductHeader;

        public bool IsProductNamePic => this.nomenclatura.key == NomenclaturaKeyConstants.ProductNamePic;

        public bool IsProductBag => this.nomenclatura.key == NomenclaturaKeyConstants.ProductBag;

        public bool IsBenefittext => this.nomenclatura.key == NomenclaturaKeyConstants.Benefittext;

        public bool IsProductDescription => this.nomenclatura.key == NomenclaturaKeyConstants.ProductDescription;

        public bool IsMainBenefit => this.nomenclatura.key == NomenclaturaKeyConstants.MainBenefit;

        public bool IsSecondaryBenefit => this.nomenclatura.key == NomenclaturaKeyConstants.SecondaryBenefit;

        public bool IsProductKibble => this.nomenclatura.key == NomenclaturaKeyConstants.ProductKibble;

        public bool IsProductEmblematic => this.nomenclatura.key == NomenclaturaKeyConstants.ProductEmblematic;

        public bool IsProduct_Bag_Large => this.nomenclatura.key == NomenclaturaKeyConstants.Product_Bag_Large;

        public static class TypeConstants
        {
            public const string ImageContent = "ImageContent";

            public const string ImageParagraphContent = "ImageParagraphContent";

            public const string ParagraphContent = "ParagraphContent";

            public const string TextContent = "TextContent";
        }

        public static class NomenclaturaKeyConstants
        {
            public const string MetaTagsKeywords = "MetaTagsKeywords";

            public const string MetaTagsDescription = "MetaTagsDescription";

            public const string ProductHeader = "ProductHeader";

            public const string ProductNamePic = "ProductNamePic";

            public const string ProductBag = "ProductBag";

            public const string Benefittext = "Benefittext";

            public const string ProductDescription = "ProductDescription";

            public const string MainBenefit = "MainBenefit";

            public const string SecondaryBenefit = "SecondaryBenefit";

            public const string ProductKibble = "ProductKibble";

            public const string ProductEmblematic = "ProductEmblematic";

            public const string Product_Bag_Large = "Product_Bag_Large";
        }
    }
}