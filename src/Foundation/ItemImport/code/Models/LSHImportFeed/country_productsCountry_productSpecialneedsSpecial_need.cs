﻿using System;

namespace Delete.Foundation.ItemImport.Models.LSHImportFeed
{
    public partial class country_productsCountry_productSpecialneedsSpecial_need
    {
        public bool IsSterilised => this.name_key.Equals("sterilised", StringComparison.InvariantCultureIgnoreCase);
    }
}