﻿using System;
using System.Globalization;
using Delete.Foundation.DataTemplates.Models.Taxonomy;

namespace Delete.Foundation.ItemImport.Models.LSHImportFeed
{
    public partial class country_productsCountry_product
    {
        private const string DateFormat = "ddd MMM dd yyyy HH:mm:ss 'GMT+0000 (UTC)'";

        public bool IsCat =>
            string.Equals(this.product.productfamily.species.name_key, "cat", StringComparison.InvariantCultureIgnoreCase);

        public bool IsDog =>
            string.Equals(this.product.productfamily.species.name_key, "dog", StringComparison.InvariantCultureIgnoreCase);

        public Species Species => this.IsCat ? Species.Cats : Species.Dogs;

        public DateTime UpdatedAt => DateTime.TryParseExact(this.updated_at, DateFormat, CultureInfo.InvariantCulture, DateTimeStyles.AllowWhiteSpaces, out var date) ? date : DateTime.MinValue;
    }
}