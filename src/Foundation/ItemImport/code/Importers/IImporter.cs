﻿using System;
using System.Collections.Generic;
using Delete.Foundation.ItemImport.Models;
using Sitecore.Publishing;

namespace Delete.Foundation.ItemImport.Importers
{
    public interface IImporter
    {
        IList<ImportLogEntry> Publish(Guid root, PublishMode mode = PublishMode.Smart, bool publishRelatedItems = true, bool republishAll = false);
    }
}