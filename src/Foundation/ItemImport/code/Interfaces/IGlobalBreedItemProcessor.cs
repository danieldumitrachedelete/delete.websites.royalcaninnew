﻿using Delete.Foundation.DataTemplates.Models.Interfaces;
using Delete.Foundation.ItemImport.ImportProcessors;

namespace Delete.Foundation.ItemImport.Interfaces
{
    using Models;

    public interface IGlobalBreedItemProcessor<out TGlobalBreed> : ISpeciesSpecificImportItemProcessorBase<TGlobalBreed, BreedDto> where TGlobalBreed : I_GlobalBreed
    {
    }
}