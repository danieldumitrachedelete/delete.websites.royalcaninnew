using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.DeleteFoundationCore.Managers;
using Delete.Foundation.InitializeDependencyInjection.Extensions;
using Delete.Foundation.ItemImport.Importers;
using Delete.Foundation.ItemImport.ImportProcessors;
using Delete.Foundation.ItemImport.Models;
using Delete.Foundation.ItemImport.Services;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.DependencyInjection;

namespace Delete.Foundation.ItemImport.DependencyInjection
{
    public class RegisterServices : IServicesConfigurator
    {
        public void Configure(IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IMediaItemItemProcessor, MediaItemItemProcessor>();
            serviceCollection.AddScoped<IMediaItemService, MediaItemService>();
            serviceCollection.AddScoped<IImporter, BaseImporter>();
            serviceCollection.AddTransient<IBaseSearchManager<GlassMediaItem>, BaseSearchManager<GlassMediaItem>>();

            serviceCollection.AddMvcControllersInCurrentAssembly();
        }
    }
}
