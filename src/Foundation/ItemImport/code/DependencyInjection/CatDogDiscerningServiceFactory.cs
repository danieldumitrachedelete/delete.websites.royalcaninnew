using System;
using Delete.Foundation.DataTemplates.Models.Taxonomy;
using Microsoft.Extensions.DependencyInjection;

namespace Delete.Foundation.ItemImport.DependencyInjection
{
    public static class CatDogDiscerningServiceFactory<
        TBaseServiceInterface,
        TCatServiceInterface,
        TDogServiceInterface,
        TCatServiceImplementation,
        TDogServiceImplementation>
        where TCatServiceInterface : class, TBaseServiceInterface
        where TDogServiceInterface : class, TBaseServiceInterface
        where TDogServiceImplementation : class, TDogServiceInterface
        where TCatServiceImplementation : class, TCatServiceInterface
    {
        public static IServiceCollection Register(IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<TDogServiceInterface, TDogServiceImplementation>();
            serviceCollection.AddScoped<TCatServiceInterface, TCatServiceImplementation>();
            return serviceCollection.AddScoped<Func<Species, TBaseServiceInterface>>(
                CatDogDiscerningAccessorFactory<TBaseServiceInterface, TCatServiceInterface, TDogServiceInterface>
                    .GetResolveFunc());
        }
    }
}