using System;
using System.Collections.Generic;
using Delete.Foundation.DataTemplates.Models.Taxonomy;
using Microsoft.Extensions.DependencyInjection;

namespace Delete.Foundation.ItemImport.DependencyInjection
{
    public static class CatDogDiscerningAccessorFactory<
        TBaseServiceInterface,
        TCatServiceInterface,
        TDogServiceInterface>
        where TCatServiceInterface : class, TBaseServiceInterface
        where TDogServiceInterface : class, TBaseServiceInterface
    {
        public static Func<IServiceProvider, Func<Species, TBaseServiceInterface>> GetResolveFunc()
        {
            return factory =>
                key =>
                {
                    switch (key)
                    {
                        case Species.Cats:
                            return factory.GetService<TCatServiceInterface>();
                        case Species.Dogs:
                            return factory.GetService<TDogServiceInterface>();
                        default:
                            throw new KeyNotFoundException();
                    }
                };
        }

        public static IServiceCollection Register(IServiceCollection serviceCollection)
        {
            return serviceCollection.AddScoped<Func<Species, TBaseServiceInterface>>(GetResolveFunc());
        }
    }
}