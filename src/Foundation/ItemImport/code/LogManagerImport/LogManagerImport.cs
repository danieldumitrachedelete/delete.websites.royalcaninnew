﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Delete.Foundation.ItemImport.LogContainer;
using Delete.Foundation.ItemImport.Models;
using log4net.spi;
using LogManager = log4net.LogManager;
using ILog = log4net.ILog;

namespace Delete.Foundation.ItemImport.LogManagerImport
{
    public interface ILogManagerImport
    {
        void AddEntryToLog(ImportLogEntry entry);

        void AddEntriesToLog(List<ImportLogEntry> entries);

        void AddItemsProcessed(int count);
        ILogContainer GetValue();
        void InitLogger(string name);
        void WriteToLog(string Message);
    }

    public class LogManagerImport:ILogManagerImport
    {
        private ILogContainer LocalLogContainer;
        private ImportLog LocalLog;
        private ILog logger;

        public LogManagerImport(ILogContainer logContainer)
        {
            logContainer.Log = new ImportLog();
            logContainer.Log.Entries = new List<ImportLogEntry>();
            LocalLogContainer = logContainer;
            LocalLog = LocalLogContainer.Log;
        }
        public void AddEntryToLog(ImportLogEntry entry)
        {
            LocalLog.Entries.Add(entry);
        }

        public void InitLogger(string name)
        {
            logger = LogManager.GetLogger(name);
        }

        public void WriteToLog(string Message)
        {
            logger.Info(Message);
        }
        public void AddEntriesToLog(List<ImportLogEntry> entries)
        {
            LocalLog.Entries.AddRange(entries);
        }

        public void AddItemsProcessed(int count)
        {
            LocalLog.ItemsProcessed += count;
        }

        public ILogContainer GetValue()
        {
            return LocalLogContainer;
        }
    }
}
