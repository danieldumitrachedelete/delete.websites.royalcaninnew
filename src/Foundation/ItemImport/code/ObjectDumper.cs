﻿using System;
using System.IO;
using System.Reflection;

namespace Delete.Foundation.ItemImport
{
    using System.Collections;

    public class ObjectDumper
    {
        private readonly int depth;

        private TextWriter writer;
        private int pos;
        private int level;

        private ObjectDumper(int depth)
        {
            this.depth = depth;
        }

        public static void Write(object element)
        {
            Write(element, 0);
        }

        public static void Write(object element, int depth)
        {
            Write(element, depth, Console.Out);
        }

        public static void Write(object element, int depth, TextWriter log)
        {
            var dumper = new ObjectDumper(depth);
            dumper.writer = log;
            dumper.WriteObject(null, element);
        }

        private void Write(string s)
        {
            if (s == null)
            {
                return;
            }

            this.writer.Write(s);
            this.pos += s.Length;
        }

        private void WriteIndent()
        {
            for (int i = 0; i < this.level; i++)
            {
                this.writer.Write("  ");
            }
        }

        private void WriteLine()
        {
            this.writer.WriteLine();
            this.pos = 0;
        }

        private void WriteTab()
        {
            this.Write("  ");
            while (this.pos % 8 != 0)
            {
                this.Write(" ");
            }
        }

        private bool IsSimpleObject(object element)
        {
            return element == null || element is ValueType || element is string;
        }

        private void WriteSimpleObject(string prefix, object element)
        {
            this.WriteIndent();
            this.Write(prefix);
            this.WriteValue(element);
            this.WriteLine();
        }

        private bool IsEnumerable(object element)
        {
            return element is IEnumerable;
        }

        private void WriteEnumerableObject(string prefix, IEnumerable element)
        {
            foreach (var item in element)
            {
                if (item is IEnumerable && !(item is string))
                {
                    this.WriteIndent();
                    this.Write(prefix);
                    this.Write("...");
                    this.WriteLine();
                    if (this.level >= this.depth)
                    {
                        continue;
                    }

                    this.level++;
                    this.WriteObject(prefix, item);
                    this.level--;
                }
                else
                {
                    this.WriteObject(prefix, item);
                }
            }
        }

        private bool WritePropertyValue(MemberInfo memberInfo, object element, bool propertyAlreadyWritten)
        {
            var fieldInfo = memberInfo as FieldInfo;
            var propertyInfo = memberInfo as PropertyInfo;
            if (fieldInfo == null && propertyInfo == null)
            {
                return false;
            }

            if (propertyAlreadyWritten)
            {
                this.WriteTab();
            }

            this.Write(memberInfo.Name);
            this.Write("=");
            var type = fieldInfo != null ? fieldInfo.FieldType : propertyInfo.PropertyType;
            if (type.IsValueType || type == typeof(string))
            {
                this.WriteValue(fieldInfo != null ? fieldInfo.GetValue(element) : propertyInfo.GetValue(element, null));
            }
            else
            {
                this.Write(typeof(IEnumerable).IsAssignableFrom(type) ? "..." : "{ }");
            }

            return true;
        }

        private void WritePropertyAdditional(MemberInfo memberInfo, object element)
        {
            var fieldInfo = memberInfo as FieldInfo;
            var propertyInfo = memberInfo as PropertyInfo;
            if (fieldInfo == null && propertyInfo == null)
            {
                return;
            }

            var type = fieldInfo != null ? fieldInfo.FieldType : propertyInfo.PropertyType;
            if (type.IsValueType || type == typeof(string))
            {
                return;
            }

            var value = fieldInfo != null ? fieldInfo.GetValue(element) : propertyInfo.GetValue(element, null);
            if (value == null)
            {
                return;
            }

            this.level++;
            this.WriteObject(memberInfo.Name + ": ", value);
            this.level--;
        }

        private void WriteComplexObject(string prefix, object element)
        {
            var members = element.GetType().GetMembers(BindingFlags.Public | BindingFlags.Instance);
            this.WriteIndent();
            this.Write(prefix);
            var propWritten = false;
            foreach (var memberInfo in members)
            {
                var tempResult = this.WritePropertyValue(memberInfo, element, propWritten);
                propWritten = tempResult || propWritten;
            }

            if (propWritten)
            {
                this.WriteLine();
            }

            if (this.level >= this.depth)
            {
                return;
            }

            foreach (var memberInfo in members)
            {
                this.WritePropertyAdditional(memberInfo, element);
            }
        }

        private void WriteObject(string prefix, object element)
        {
            if (this.IsSimpleObject(element))
            {
                this.WriteSimpleObject(prefix, element);
                return;
            }

            if (this.IsEnumerable(element))
            {
                this.WriteEnumerableObject(prefix, element as IEnumerable);
                return;
            }

            this.WriteComplexObject(prefix, element);
        }

        private void WriteValue(object o)
        {
            if (o == null)
            {
                this.Write("null");
            }
            else if (o is DateTime time)
            {
                this.Write(time.ToShortDateString());
            }
            else if (o is ValueType || o is string)
            {
                this.Write(o.ToString());
            }
            else if (o is IEnumerable)
            {
                this.Write("...");
            }
            else
            {
                this.Write("{ }");
            }
        }
    }
}