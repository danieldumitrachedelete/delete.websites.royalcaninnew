﻿using System;
using System.Net;
using System.Text;
using System.Xml.Serialization;

namespace Delete.Foundation.ItemImport.Controllers
{
    using Delete.Foundation.DeleteFoundationCore.Extensions;

    public class XmlFeedDownloadController<T>
    {
        private readonly XmlSerializer xmlSerializer;

        public XmlFeedDownloadController()
        {
            this.xmlSerializer = new XmlSerializer(typeof(T));
        }

        public T GetData(string url, string username = "", string password = "")
        {
            T result;
            var headers = new WebHeaderCollection();

            if (username.NotEmpty() && password.NotEmpty())
            {
                var credentials = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{username}:{password}"));
                headers.Add(HttpRequestHeader.Authorization.ToString(), $"Basic {credentials}");
            }

            using (var wc = new WebClient { Encoding = Encoding.UTF8, Headers = headers })
            {
                var stream = wc.OpenRead(url);
                if (stream != null)
                {
                    result = (T)this.xmlSerializer.Deserialize(stream);
                }
                else
                {
                    throw new NullReferenceException($"Got null stream when retrieving xml from '{url}'");
                }
            }

            return result;
        }
    }
}