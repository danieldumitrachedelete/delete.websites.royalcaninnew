﻿namespace Delete.Foundation.ItemImport.Loggers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using log4net;

    using Models;

    public interface IImportJobLogger
    {
        void LogImportResults(IList<ImportLogEntry> importResults);
    }

    public class ImportJobLogger : IImportJobLogger
    {
        private readonly ILog logger;

        public ImportJobLogger(ILog logger)
        {
            this.logger = logger;
        }

        public void LogImportResults(IList<ImportLogEntry> importResults)
        {
            if (importResults == null)
            {
                return;
            }

            var generalMessages = importResults.Where(x => x.Action == ImportAction.Undefined);
            var imported = importResults.Where(x => x.Action == ImportAction.Imported).ToList();
            imported.AddRange(importResults.Where(x => x.Action == ImportAction.New).ToList());
            imported.AddRange(importResults.Where(x => x.Action == ImportAction.Updated).ToList());
            var rejected = importResults.Where(x => x.Action == ImportAction.Rejected).ToList();
            var deleted = importResults.Where(x => x.Action == ImportAction.Deleted).ToList();
            var skipped = importResults.Where(x => x.Action == ImportAction.Skipped).ToList();

            foreach (var generalMessage in generalMessages)
            {
                this.logger.Info(generalMessage.Message);
            }

            var stats = new StringBuilder();

            if (imported.Any() || rejected.Any() || deleted.Any())
            {
                stats.AppendLine(this.logger.Logger.Name);
                stats.AppendLine($"{imported.Count} imported");
                stats.AppendLine($"{rejected.Count} rejected");
                stats.AppendLine($"{skipped.Count} skipped");
                stats.AppendLine($"{deleted.Count} deleted");
                stats.AppendLine();

                stats.AppendLine("Imported items");
                foreach (var importedProduct in imported)
                {
                    stats.AppendLine(importedProduct.Message);
                }

                stats.AppendLine();

                stats.AppendLine("Rejected items");
                foreach (var rejectedProduct in rejected)
                {
                    stats.AppendLine(rejectedProduct.Message);
                }

                stats.AppendLine();

                stats.AppendLine("Deleted items");
                foreach (var deletedProduct in deleted)
                {
                    stats.AppendLine(deletedProduct.Message);
                }
            }

            this.logger.Info(stats.ToString());
        }
    }
}