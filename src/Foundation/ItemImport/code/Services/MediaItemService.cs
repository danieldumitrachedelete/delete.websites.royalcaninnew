﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Models;
using Delete.Foundation.ItemImport.LogManagerImport;
using Delete.Foundation.ItemImport.Models;
using Delete.Foundation.MultiSite.Models;
using Delete.Foundation.MultiSite.Services;
using Glass.Mapper.Sc;
using Sitecore.Data.Items;
using Sitecore.Resources.Media;

namespace Delete.Foundation.ItemImport.Services
{
    public interface IMediaItemService
    {
        MediaFolder GetLocalMarketMediaFolder();

        MediaFolder GetOrCreateMediaFolder(GlassBase parent, string name);

        MediaItem CreateOrUpdateMediaItem(string fullPath, string alt, Uri imageUrl, bool failSilently = false, bool skipExistingImages = false);

        Stream DownloadImage(Uri uri);
    }

    public class MediaItemService : IMediaItemService
    {
        private readonly ISitecoreContext sitecoreContext;

        private readonly ILocalMarketSettings marketSettings;

        private ILogManagerImport LogManagerLocalImport;

        public MediaItemService(ISitecoreContext sitecoreContext, ILocalMarketSettings marketSettings, ILogManagerImport LogManagerImport)
        {
            this.sitecoreContext = sitecoreContext;
            this.marketSettings = marketSettings;
            this.LogManagerLocalImport = LogManagerImport;
        }

        public MediaFolder GetLocalMarketMediaFolder()
        {
            return this.sitecoreContext.GetItem<MediaFolder>(this.marketSettings.MediaFolder);
        }

        public MediaFolder GetOrCreateMediaFolder(GlassBase parent, string name)
        {
            var existing = parent.Children?.Where(x => x.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase)).ToList();
            if (existing != null && existing.Any())
            {
                return this.sitecoreContext.GetItem<MediaFolder>(existing.First().Id);
            }

            return this.sitecoreContext.Create<MediaFolder, GlassBase>(parent, name);
        }

        public MediaItem CreateOrUpdateMediaItem(string fullPath, string alt, Uri imageUrl, bool failSilently = false, bool skipExistingImages = false)
        {
            try
            {
                this.ParseValidExtensionAndMime(imageUrl, out var extension, out var mimeType);

                var options = new MediaCreatorOptions
                {
                    FileBased = false,
                    IncludeExtensionInItemName = false,
                    OverwriteExisting = true,
                    Versioned = false,
                    Destination = fullPath,
                    AlternateText = alt,
                    Database = this.sitecoreContext.Database,
                    Language = SiteContextResolutionService.CurrentContext.ContentLanguage
                };
                MediaItem mediaItem;
                ImportAction action;
                var imageItem = Sitecore.Configuration.Factory.GetDatabase("master").GetItem(fullPath);
                if (imageItem == null)
                {
                    action = ImportAction.NewImage;
                }
                else if (skipExistingImages)
                {
                    return new MediaItem(imageItem);
                }
                else
                {
                    action = ImportAction.UpdatedImage;
                }

                LogManagerLocalImport.AddEntryToLog(new ImportLogEntry
                {
                    Action = action,
                    Level = MessageLevel.Info,
                    Message = $"Name image: {imageUrl.ToString().GetFileNameFromUrl() + extension}"
                });

                using (var stream = this.DownloadImage(imageUrl))
                {
                    mediaItem = new MediaCreator().CreateFromStream(stream, fullPath + extension, options);
                    if (mediaItem?.InnerItem != null)
                    {
                        var image = Image.FromStream(stream);
                        mediaItem.InnerItem.Editing.BeginEdit();
                        if (mediaItem.InnerItem.Fields["Height"] != null)
                        {
                            mediaItem.InnerItem.Fields["Height"].Value = image.Height.ToString();
                        }
                        if (mediaItem.InnerItem.Fields["Width"] != null)
                        {
                            mediaItem.InnerItem.Fields["Width"].Value = image.Width.ToString();
                        }
                        if (mediaItem.InnerItem.Fields["Mime Type"] != null)
                        {
                            mediaItem.InnerItem.Fields["Mime Type"].Value = mimeType;
                        }
						if (mediaItem.InnerItem.Fields["Alt"] != null)
						{
							mediaItem.InnerItem.Fields["Alt"].Value = mediaItem.Alt;
						}
						mediaItem.InnerItem.Editing.EndEdit();
                    }
                }

                return mediaItem;
            }
            catch (Exception e)
            {
                if (!failSilently)
                {
                    // todo: log error information
                    throw;
                }
            }

            return null;
        }

        private void ParseValidExtensionAndMime(Uri imageUrl, out string extension, out string mimeType)
        {
            string imageFileName;

            if (!string.IsNullOrWhiteSpace(imageUrl.Query))
            {
                var queryParameters = imageUrl.ParseQueryString();
                imageFileName = queryParameters.Get("f").Split('/').Last();
            }
            else
            {
                imageFileName = imageUrl.LocalPath;
            }

            extension = Path.GetExtension(imageFileName);
            mimeType = MimeMapping.GetMimeMapping(imageFileName);
        }


        public Stream DownloadImage(Uri uri)
        {
            using (var client = new WebClient())
            {
                var bytes = client.DownloadData(uri);
                return new MemoryStream(bytes);
            }
        }
    }
}