﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using Delete.Foundation.AssetsManagement.Models;
using Delete.Foundation.AssetsManagement.Services;
using Delete.Foundation.DeleteFoundationCore.Models;
using Delete.Foundation.ItemImport.Models.Stockists;
using Delete.Foundation.ItemImport.Models.WeShareImportFeed;
using Delete.Foundation.MultiSite.Models;
using Glass.Mapper;
using Sitecore.Globalization;
using Sitecore.Mvc.Extensions;
using Sitecore.Shell.Framework.Commands.Masters;

namespace Delete.Foundation.ItemImport.Profiles
{
    using System;
    using System.Linq;

    using AutoMapper;

    using DeleteFoundationCore.Extensions;

    using JetBrains.Annotations;


    using Models;
    using Models.LSHImportFeed;

    [UsedImplicitly]
    public class ItemImportProfile : Profile
    {
        public ItemImportProfile()
        {
            this.CreateMap<Models.SalsifyImportFeed.Product, ImportedProductDto>()
                .ForMember(x => x.MainItemId, opt => opt.MapFrom(z => z.SitecoreMainItemId))
                .ForMember(x => x.UpdatedAt, opt => opt.MapFrom(z => z.SitecoreLastDateModified))
                .ForMember(
                    x => x.Ages,
                    opt => opt.MapFrom(z => z.SitecoreAges.SafelySplit(",").Select(a => new LifestageDto { Name = a })))
                .ForMember(x => x.Breed, opt => opt.MapFrom(z => new BreedDto { BreedId = z.SitecoreBreedId }))
                .ForMember(x => x.FeedingPeriodFrom, opt => opt.MapFrom(z => z.SitecoreFeedingPeriodFrom))
                .ForMember(x => x.FeedingPeriodTo, opt => opt.MapFrom(z => z.SitecoreFeedingPeriodTo))
                .ForMember(x => x.Image1, opt => opt.MapFrom(z => new ImageDto { Url = z.SitecoreImage1, Alt = z.SitecoreImage1Alt }))
                .ForMember(x => x.Image2, opt => opt.MapFrom(z => new ImageDto { Url = String.Empty, Alt = String.Empty }))
                .ForMember(x => x.Image3, opt => opt.MapFrom(z => new ImageDto { Url = String.Empty, Alt = String.Empty }))
                .ForMember(x => x.Image4, opt => opt.MapFrom(z => new ImageDto { Url = z.SitecoreImage2, Alt = z.SitecoreImage2Alt }))
                .ForMember(x => x.Image5, opt => opt.MapFrom(z => new ImageDto { Url = z.SitecoreImage3, Alt = z.SitecoreImage3Alt }))
                .ForMember(x => x.Image6, opt => opt.MapFrom(z => new ImageDto { Url = z.SitecoreImage4, Alt = z.SitecoreImage4Alt }))
                .ForMember(x => x.Image7, opt => opt.MapFrom(z => new ImageDto { Url = z.SitecoreImage5, Alt = z.SitecoreImage5Alt }))
                .ForMember(x => x.Image8, opt => opt.MapFrom(z => new ImageDto { Url = z.SitecoreImage6, Alt = z.SitecoreImage6Alt }))
                .ForMember(x => x.FeedingGuideImage, opt => opt.MapFrom(z => new ImageDto { Url = z.SitecoreFeedingGuideImage, Alt = z.SitecoreFeedingGuideImageAlt }))
                .ForMember(x => x.FeedingGuide, opt => opt.MapFrom(z => z.SitecoreFeedingGuide))
                .ForMember(x => x.Ingredients, opt => opt.MapFrom(z => z.SitecoreIngredients))
                .ForMember(x => x.Keywords, opt => opt.MapFrom(z => z.SitecoreKeywords))
                .ForMember(x => x.BenefitText, opt => opt.MapFrom(z => z.SitecoreBenefitText))
                .ForMember(x => x.SecondaryBenefit, opt => opt.MapFrom(z => z.SitecoreSecondaryBenefit))
                .ForMember(x => x.MainBenefit, opt => opt.MapFrom(z => z.SitecoreMainBenefit))
                .ForMember(x => x.BenefitText4, opt => opt.MapFrom(z => z.SitecoreFourthBenefit))
                .ForMember(x => x.BenefitText5, opt => opt.MapFrom(z => z.SitecoreFifthBenefit))
                .ForMember(x => x.BenefitText6, opt => opt.MapFrom(z => z.SitecoreSixthBenefit))
                .ForMember(x => x.MainDescription, opt => opt.MapFrom(z => z.SitecoreMainDescription))
                .ForMember(
                    x => x.Packaging,
                    opt => opt.MapFrom(
                        z => z.Packages.Select(p => new PackagingDto
                        {
                            EAN = p.SitecorePackEan,
                            Size = p.SitecorePackSize,
                            SKU = p.SitecorePackSku,
                            Unit = new MeasurementUnitDto{ Code = p.SitecorePackUnit },
                            UPC = p.SitecorePackUpc
                        })))
                .ForMember(x => x.Pillar, opt => opt.MapFrom(z => new PillarDto { Code = z.SitecorePillarIdCode }))
                .ForMember(x => x.ProductDescription, opt => opt.MapFrom(z => z.SitecoreProductDescription))
                .ForMember(x => x.ProductName, opt => opt.MapFrom(z => z.SitecoreProductName))
                .ForMember(x => x.ProductStatus, opt => opt.MapFrom(z => z.SitecoreProductStatus))
                .ForMember(x => x.GuaranteedAnalysis, opt => opt.MapFrom(z => z.SitecoreGuaranteedAnalysis))
                .ForMember(
                    x => x.ProductType,
                    opt => opt.MapFrom(z => new ProductTypeDto { Name = z.SitecoreProductType }))
                .ForMember(x => x.ProductTypeName, opt => opt.MapFrom(z => z.SitecoreProuctTypeName))
                .ForMember(x => x.SKU, opt => opt.MapFrom(z => z.SitecoreSku))
                .ForMember(x => x.EAN, opt => opt.MapFrom(z => z.SitecorePackEan))
                .ForMember(x => x.IsDog, opt => opt.MapFrom(z => z.IsDog))
                .ForMember(x => x.IsCatDog, opt => opt.MapFrom(z => z.IsCatDog))
                .ForMember(x => x.Species, opt => opt.MapFrom(z => z.Species))
                .ForMember(
                    x => x.AnimalSizes,
                    opt => opt.MapFrom(
                        z => z.SitecoreAnimalSize.SafelySplit(",").Select(a => new AnimalSizeDto { Code = a })))
                .ForMember(
                    x => x.SpecialNeeds,
                    opt => opt.MapFrom(z => z.SitecoreSpecialNeed == null
                        ? null
                        : z.SitecoreSpecialNeed.SafelySplit(",").Select(x => new SpecialNeedDto
                        {
                            Name = x, Key = x
                        })))
                .ForMember(
                    x => x.IsSterilized,
                    opt => opt.MapFrom(z => z.SitecoreSpecialNeed == null
                        ? null
                        : new SpecialNeedDto
                        {
                            Name = z.SitecoreIsSterilized, Key = z.SitecoreIsSterilized
                        }))
                .ForAllOtherMembers(opt => opt.Ignore());

            this.CreateMap<country_productsCountry_product, ImportedProductDto>()
                .ForMember(x => x.MainItemId, opt => opt.MapFrom(z => z.product.main_item_id.ToString()))
                .ForMember(x => x.UpdatedAt, opt => opt.MapFrom(z => z.UpdatedAt))
                .ForMember(
                    x => x.Ages,
                    opt => opt.MapFrom(z => z.product == null || z.product.ages == null || z.product.ages.age == null
                                                ? null
                                                : z.product.ages.age.Select(p => new LifestageDto { Name = p.name_key })))
                .ForMember(
                    x => x.Breed,
                    opt => opt.MapFrom(z => z.product == null || z.product.breeds == null || z.product.breeds.breed == null
                                                ? null
                                                : new BreedDto
                                                {
                                                    BreedId = z.product.breeds.breed.id.ToString("D"),
                                                    Name = z.product.breeds.breed.Value
                                                }))
                .ForMember(x => x.FeedingPeriodFrom, opt => opt.MapFrom(z => z.feedingperiod == null || z.feedingperiod.from == null ? string.Empty : z.feedingperiod.from.Value))
                .ForMember(x => x.FeedingPeriodTo, opt => opt.MapFrom(z => z.feedingperiod == null || z.feedingperiod.to == null ? string.Empty : z.feedingperiod.to.Value))
                .ForMember(
                    x => x.Image1,
                    opt => opt.MapFrom(z => GetContentMainImageByPredicate(z, c => c.IsProductBag)))
                .ForMember(
                    x => x.Image4,
                    opt => opt.MapFrom(z => GetContentMainImageByPredicate(z, c => c.IsProductKibble)))
                .ForMember(
                    x => x.Image5,
                    opt => opt.MapFrom(z => GetContentMainImageByPredicate(z, c => c.IsProductEmblematic)))
                .ForMember(
                    x => x.Ingredients,
                    opt => opt.MapFrom(z => z.legal_ingredients != null ? z.legal_ingredients.Value : string.Empty))
                .ForMember(
                    x => x.Keywords,
                    opt => opt.MapFrom(z => GetContentByPredicate(z, c => c.IsMetaTagsKeywords)))
                .ForMember(x => x.BenefitText, opt => opt.MapFrom(z => GetContentByPredicate(z, c => c.IsSecondaryBenefit, 1)))
                .ForMember(x => x.SecondaryBenefit, opt => opt.MapFrom(z => GetContentByPredicate(z, c => c.IsSecondaryBenefit, 0)))
                .ForMember(x => x.MainBenefit, opt => opt.MapFrom(z => GetContentByPredicate(z, c => c.IsMainBenefit)))
                .ForMember(x => x.MainDescription, opt => opt.MapFrom(z => z.legal_text_heading))
                .ForMember(
                    x => x.Packaging,
                    opt => opt.MapFrom(z => z.packagingsizes == null || z.packagingsizes.packaging_size == null
                                                 ? null
                                                 : z.packagingsizes.packaging_size.Select(s => new PackagingDto
                                                 {
                                                     Size = s.amount.Value,
                                                     Unit = new MeasurementUnitDto { Code = s.unit.name_key }
                                                 }).ToArray()))
                .ForMember(x => x.Pillar, opt => opt.MapFrom(z => z.product == null || z.product.pillar == null ? null : new PillarDto { Code = z.product.pillar.code }))
                .ForMember(x => x.ProductDescription, opt => opt.MapFrom(z => z.eRetailDescriptionMedium))
                .ForMember(x => x.ProductName, opt => opt.MapFrom(z => z.product.name))
                .ForMember(x => x.ProductStatus, opt => opt.MapFrom(z => !string.Equals(z.product_status, "active", StringComparison.OrdinalIgnoreCase) ? 0 : 1))
                .ForMember(
                    x => x.ProductType,
                    opt => opt.MapFrom(z => new ProductTypeDto { Name = z.product == null || z.product.product_type == null ? null : z.product.product_type.name }))
                .ForMember(x => x.IsDog, opt => opt.MapFrom(z => z.IsDog))
                .ForMember(x => x.Species, opt => opt.MapFrom(z => z.Species))
                .ForMember(x => x.Tags, opt => opt.MapFrom(z => GetContentByPredicate(z, c => c.IsMetaTagsDescription)))
                .ForMember(x => x.Text, opt => opt.MapFrom(z => GetContentByPredicate(z, c => c.IsTextContent)))
                .ForMember(
                    x => x.SpecialNeeds,
                    opt => opt.MapFrom(z => z.specialneeds == null || z.specialneeds.special_need == null
                                                ? null
                                                : z.specialneeds.special_need.Where(x => !x.IsSterilised).Select(s => new SpecialNeedDto
                                                {
                                                    Key = s.name_key,
                                                    Name = s.name
                                                })))
                .ForMember(
                    x => x.IsSterilized,
                    opt => opt.MapFrom(z => z.specialneeds == null || z.specialneeds.special_need == null
                                                ? null
                                                : z.specialneeds.special_need.Where(x => x.IsSterilised).Select(s => new SpecialNeedDto
                                                {
                                                    Key = s.name_key,
                                                    Name = s.name
                                                }).FirstOrDefault()))
                /*  these two fields are gone from the updated LSH feed XSD schema (while working on RC-518).                                                    
                .ForMember(
                    x => x.Indications,
                    opt => opt.MapFrom(z => z.indications != null || z.indications.indication == null
                                                ? null
                                                : z.indications.indication.Select(s => new IndicationDto
                                                {
                                                    Id = s.id.ToString(),
                                                    Name = s.name
                                                })))
                .ForMember(
                    x => x.CounterIndications,
                    opt => opt.MapFrom(z => z.counterindications != null || z.counterindications.counterindication == null
                                                ? null
                                                : z.counterindications.counterindication.Select(s => new CounterIndicationDto
                                                {
                                                    Id = s.id.ToString(),
                                                    Name = s.name
                                                })))
                */
                .ForMember(
                    x => x.AnimalSizes,
                    opt => opt.MapFrom(
                        z => z.product.animalsizes.animalsize.Select(a => new AnimalSizeDto { Code = a.name_key })))
                .ForAllOtherMembers(opt => opt.Ignore());

	        this.CreateMap<Delete.Foundation.ItemImport.Models.WeShareImportFeed.Hit, ImportedProductDto>()
		        .ForMember(x => x.MainItemId, opt => opt.MapFrom(z => z.Source.Metadata.Overview.MainItem))
		        .ForMember(x => x.ProductName, opt => opt.MapFrom(z => GetProductNameFromWeShareProduct(z)))
		        .ForMember(x => x.UpdatedAt, opt => opt.MapFrom(z => z.Source.Updated))
		        .ForMember(x => x.FeedingPeriodFrom, opt => opt.MapFrom(z => ParseWeShareFeedingPeriod(
			        z.Source.Metadata.Hidden.Technical.FeedingPeriodMinimumAge, 
			        z.Source.Metadata.Hidden.Technical.FeedingPeriodMinimumAgeUnit)))
		        .ForMember(x => x.FeedingPeriodTo, opt => opt.MapFrom(z => ParseWeShareFeedingPeriod(
			        z.Source.Metadata.Hidden.Technical.FeedingPeriodMaximumAge, 
			        z.Source.Metadata.Hidden.Technical.FeedingPeriodMaximumAgeUnit)))
		        .ForMember(x => x.Ingredients, opt => opt.ResolveUsing((z, dest, destMember, context) => GetIngredientsFromWeShareProduct(z, context.Items[LocalMarketSettingsConstants.TemplateName] as LocalMarketSettings)))
		        .ForMember(x => x.MainBenefit, opt => opt.MapFrom(z => GetBenefitTextFromWeShareProduct(z, 0)))
		        .ForMember(x => x.SecondaryBenefit, opt => opt.MapFrom(z => GetBenefitTextFromWeShareProduct(z, 1)))
		        .ForMember(x => x.BenefitText, opt => opt.MapFrom(z => GetBenefitTextFromWeShareProduct(z, 2)))
		        .ForMember(x => x.BenefitText4, opt => opt.MapFrom(z => GetBenefitTextFromWeShareProduct(z, 3)))
		        .ForMember(x => x.BenefitText5, opt => opt.MapFrom(z => GetBenefitTextFromWeShareProduct(z, 4)))
		        .ForMember(x => x.BenefitText6, opt => opt.MapFrom(z => GetBenefitTextFromWeShareProduct(z, 5)))
		        .ForMember(x => x.BenefitTitle1, opt => opt.MapFrom(z => GetBenefitTitleFromWeShareProduct(z, 0)))
		        .ForMember(x => x.BenefitTitle2, opt => opt.MapFrom(z => GetBenefitTitleFromWeShareProduct(z, 1)))
		        .ForMember(x => x.BenefitTitle3, opt => opt.MapFrom(z => GetBenefitTitleFromWeShareProduct(z, 2)))
		        .ForMember(x => x.BenefitTitle4, opt => opt.MapFrom(z => GetBenefitTitleFromWeShareProduct(z, 3)))
		        .ForMember(x => x.BenefitTitle5, opt => opt.MapFrom(z => GetBenefitTitleFromWeShareProduct(z, 4)))
		        .ForMember(x => x.BenefitTitle6, opt => opt.MapFrom(z => GetBenefitTitleFromWeShareProduct(z, 5)))
				.ForMember(x => x.MainDescription, opt => opt.MapFrom(z => GetMainDescriptionFromWeShareProduct(z)))
		        .ForMember(x => x.GuaranteedAnalysis, opt => opt.MapFrom(z => GetCompositionsFromWeShareProductByKey(z, "ANALYTICAL_CONSTITUANTS")))
		        .ForMember(x => x.Additives, opt => opt.MapFrom(z => GetCompositionsFromWeShareProductByKey(z, "ADDITIVES")))
		        .ForMember(x => x.AConstituents, opt => opt.MapFrom(z => GetCompositionsFromWeShareProductByKey(z, "ANALYTICAL_CONSTITUANTS")))
		        .ForMember(x => x.FeedingGuide, opt => opt.MapFrom(z => GetFeedingGuideTextFromWeShareProduct(z)))
		        .ForMember(x => x.FeedingGuideImage, opt => opt.MapFrom(z => GetImageFromWeShareRelation(GetFeedingGuideImageIdFromWeShareProduct(z), 0)))
				//.ForMember(x => x.Text, opt => opt.MapFrom(z => z.Source.))
				.ForMember(x => x.ProductDescription, opt => opt.MapFrom(z => GetProductDescriptionFromWeShareProduct(z)))
		        .ForMember(x => x.ProductStatus, opt => opt.MapFrom( z => 1 )) //only active products will be included in the feed
				.ForMember(x => x.SKU, opt => opt.MapFrom(z => GetPropertyFromPacks(z.Source.Metadata.Packs, pack => pack.Scode)))
		        .ForMember(x => x.EAN, opt => opt.MapFrom(z => GetPropertyFromPacks(z.Source.Metadata.Packs, pack => pack.Ean)))
		        .ForMember(x => x.Species, opt => opt.MapFrom(z => z.Species))
                .ForMember(x => x.IsDog, opt => opt.MapFrom(z => z.IsDog))
                //.ForMember(x => x.IsCatDog, opt => opt.MapFrom(z => z.IsCatDog))
                .ForMember(x => x.Indications, opt => opt.MapFrom(z => 
			        z.Source.Metadata.Hidden.Technical.RecommendedForsCode
				        .Select(code => new IndicationDto{Id = code, Name = code})))
				.ForMember(x => x.CounterIndications, opt => opt.MapFrom(z =>
			        z.Source.Metadata.Hidden.Technical.NotRecommendedForsCode
				        .Select(code => new CounterIndicationDto { Id = code, Name = code })))
				.ForMember(x => x.SpecialNeeds, opt => opt.MapFrom(z =>
			        z.Source.Metadata.Hidden.Technical.SpecificNeedsCode
						.Where(code => !code.Equals("sterilised", StringComparison.InvariantCultureIgnoreCase))
				        .Select(code => new SpecialNeedDto { Key = code, Name = code })))
		        .ForMember(x => x.IsSterilized, opt => opt.MapFrom(z =>
			        z.Source.Metadata.Hidden.Technical.SpecificNeedsCode
				        .Where(code => code.Equals("sterilised", StringComparison.InvariantCultureIgnoreCase))
				        .Select(code => new SpecialNeedDto { Key = code, Name = code })
						.FirstOrDefault()))
		        .ForMember(x => x.Image1, opt => opt.MapFrom(z => GetImageFromWeShareRelation(z.Source.Relations.AssociatedBagImage, 0)))
		        //.ForMember(x => x.Image2, opt => opt.MapFrom(z => GetImageFromWeShareRelation(z.Source.Relations., 0)))
		        //.ForMember(x => x.Image3, opt => opt.MapFrom(z => GetImageFromWeShareRelation(z.Source.Relations., 0)))
		        .ForMember(x => x.Image4, opt => opt.MapFrom(z => GetImageFromWeShareRelation(z.Source.Relations.AssociatedKibbleImage, 0)))
		        .ForMember(x => x.Image5, opt => opt.MapFrom(z => GetImageFromWeShareRelation(z.Source.Relations.AssociatedEmblematicImage, 0)))
		        .ForMember(x => x.Image6, opt => opt.MapFrom(z => GetImageFromWeShareRelation(z.Source.Relations.OthersImages, 0)))
		        .ForMember(x => x.Image7, opt => opt.MapFrom(z => GetImageFromWeShareRelation(z.Source.Relations.OthersImages, 1)))
		        .ForMember(x => x.Image8, opt => opt.MapFrom(z => GetImageFromWeShareRelation(z.Source.Relations.OthersImages, 2)))
		        .ForMember(x => x.Packaging, opt => opt.MapFrom(z => z.Source.Metadata.Packs.Select(pack => 
			        new PackagingDto
			        {
						EAN = pack.Ean,
						Size = pack.WeightAmount,
						SKU = pack.Scode,
						Unit = pack.WeightUnit
			        })))
		        .ForMember(x => x.Ages, opt => opt.MapFrom(z => z.Source.Metadata.Hidden.Technical.LifestagesCode.Select(code => 
			        new LifestageDto
			        {
				        Name = code
			        })))
		        .ForMember(x => x.Breed, opt => opt.MapFrom(z => BuildBreedDtoFromWeShareProduct(z)))
		        .ForMember(x => x.ProductType, opt => opt.MapFrom(z => new ProductTypeDto{ Name = z.Source.Metadata.Hidden.Technical.TechnologyCode }))
		        .ForMember(x => x.Technology, opt => opt.MapFrom(z => new ProductTypeDto { Name = z.Source.Metadata.Hidden.Technical.TechnologyCode }))
				.ForMember(x => x.Pillar, opt => opt.MapFrom(z => new PillarDto
		        {
			        Code = WeSharePillarCodesDictionary.ContainsKey(z.Source.Metadata.Hidden.Technical.PillarCode) 
				        ? WeSharePillarCodesDictionary[z.Source.Metadata.Hidden.Technical.PillarCode] 
				        : z.Source.Metadata.Hidden.Technical.PillarCode
		        }))
		        .ForMember(x => x.AnimalSizes, opt => opt.MapFrom(z => z.Source.Metadata.TargetPet.Size.Select(code => new AnimalSizeDto
		        {
			        Code = WeShareSizeCodesDictionary.ContainsKey(code)
						? WeShareSizeCodesDictionary[code]
						: code
				})))
                .ForAllOtherMembers(opt => opt.Ignore());

			this.CreateMap<StockistCsvModel, StockistDto>()
                .ForMember(x => x.CustomerReference, opt => opt.MapFrom(z => z.UniqueCustomerId))
                .ForMember(x => x.CompanyName, opt => opt.MapFrom(z => z.CompanyName))
                .ForMember(x => x.ContactTypes,
                    opt => opt.MapFrom(z =>
                        new List<string> {z.ContactType, z.AddContactType}
                            .Where(t => t.HasValue())
                            .Distinct()
                            .Select(t => new ContactTypeDto {Code = t})))
                .ForMember(x => x.Phone, opt => opt.MapFrom(z => z.Phone))
                .ForMember(x => x.Latitude, opt => opt.MapFrom(z => z.Latitude))
                .ForMember(x => x.Longitude, opt => opt.MapFrom(z => z.Longitude))
                .ForMember(x => x.CountyOrState, opt => opt.MapFrom(z => z.State))
                .ForMember(x => x.Fax, opt => opt.MapFrom(z => z.Fax))
                .ForMember(x => x.Email, opt => opt.MapFrom(z => z.Email))
                .ForMember(x => x.WebsiteUrl, opt => opt.MapFrom(z => z.WebsiteURL))
                .ForMember(x => x.Country, opt => opt.MapFrom(z => z.Country))
                .ForMember(x => x.Postcode, opt => opt.MapFrom(z => z.Postcode))
                .ForMember(x => x.AddressLine1, opt => opt.MapFrom(z => z.Address))
                .ForMember(x => x.AddressLine2, opt => opt.MapFrom(z => z.AddressLine2))
                .ForMember(x => x.AddressLine3, opt => opt.MapFrom(z => z.AddressLine3))
                .ForMember(x => x.City, opt => opt.MapFrom(z => z.City))
                .ForMember(x => x.Description, opt => opt.MapFrom(z => z.Description))
                .ForMember(x => x.ProfileType, opt => opt.MapFrom(z => new ProfileTypeDto {Code = z.ProfileType}))
                .ForAllOtherMembers(opt => opt.Ignore());

            this.CreateMap<StockistXlsxModel, StockistDto>()
                .ForMember(x => x.CustomerReference, opt => opt.MapFrom(z => z.UniqueCustomerId))
                .ForMember(x => x.CompanyName, opt => opt.MapFrom(z => z.CompanyName))
                .ForMember(x => x.ContactTypes,
                    opt => opt.MapFrom(z => new List<ContactTypeDto>
                    {
                        new ContactTypeDto {Code = z.ContactType}
                    }))
                .ForMember(x => x.Phone, opt => opt.MapFrom(z => z.Phone))
                .ForMember(x => x.Latitude, opt => opt.MapFrom(z => z.Latitude))
                .ForMember(x => x.Longitude, opt => opt.MapFrom(z => z.Longitude))
                .ForMember(x => x.CountyOrState, opt => opt.MapFrom(z => z.State))
                .ForMember(x => x.Postcode, opt => opt.MapFrom(z => z.Postcode))
                .ForMember(x => x.AddressLine1, opt => opt.MapFrom(z => z.Address))
                .ForMember(x => x.City, opt => opt.MapFrom(z => z.City))
                .ForAllOtherMembers(opt => opt.Ignore());
        }

	    private static string GetContentByPredicate(
            country_productsCountry_product importObj,
            Func<country_productsCountry_productContent, bool> predicate)
        {
            var content = importObj.contents.FirstOrDefault(predicate);
            return content?.text;
        }

        private static string GetContentByPredicate(
            country_productsCountry_product importObj,
            Func<country_productsCountry_productContent, bool> predicate,
            int index)
        {
            var content = importObj.contents.Where(predicate).ElementAtOrDefault(index);
            return content?.text;
        }

        private static ImageDto GetContentMainImageByPredicate(
            country_productsCountry_product importObj,
            Func<country_productsCountry_productContent, bool> predicate)
        {
            var content = importObj.contents.FirstOrDefault(predicate);
            if (content == null)
            {
                return null;
            }

            return new ImageDto
            {
                Url = content.filename,
                Alt = !string.IsNullOrEmpty(content.alt as string)
                                     ? (string)content.alt
                                     : content.nomenclatura?.name
            };
        }

        private static ImageDto GetContentGeneralImageByPredicate(
            country_productsCountry_product importObj,
            Func<country_productsCountry_productContent, bool> predicate,
            int index)
        {
            var images = importObj.contents.Where(predicate).ToList();
            if (images.Count <= index)
            {
                return null;
            }

            var content = images[index];
            return new ImageDto
            {
                Url = content.filename,
                Alt = !string.IsNullOrEmpty(content.alt as string)
                                     ? (string)content.alt
                                     : content.nomenclatura?.name
            };
        }

		#region WeShare import

	    private static IWeShareService _weShareService;

		private static IWeShareService weShareService
		{
			get { return _weShareService ?? (_weShareService = new WeShareService()); }
		}

		private static readonly Dictionary<string, int> multiplierDictionary = new Dictionary<string, int>
		{
			{"month", 4},
			{"year", 48}
		};

	    private static readonly Regex trailingDecimalRegex = new Regex("[\\.,].+$", RegexOptions.Compiled);

		private static string ParseWeShareFeedingPeriod(string age, string unit)
		{
			int value = 0;
			if (!int.TryParse(trailingDecimalRegex.Replace(age, ""), out value))
			{
				return String.Empty;
			};

			var key = unit.ToLower();
			if (multiplierDictionary.ContainsKey(key))
			{
				return (value * multiplierDictionary[key]).ToString();
			}
			else
			{
				return value.ToString();
			}
		}

	    protected virtual string GetPropertyFromPacks(IList<Pack> packs, Func<Pack, string> func)
	    {
		    var pack = packs.FirstOrDefault();
		    return pack != null ? func(pack) : string.Empty;
		}

	    protected virtual ImageDto GetImageFromWeShareRelation(IList<int> imageCode, int n = 0)
	    {
		    if (imageCode != null && imageCode.Count > n && imageCode[n] != default(int))
		    {
			    var code = imageCode[n].ToString();
			    var response = weShareService.SearchById(code);
			    if (response.hits.total > 0)
			    {
				    var externalImage = CreateMediaItem(response.hits.hits[0], code);
				    return new ImageDto
					{
						Url = externalImage.Path,
						Alt = externalImage.Title,
						ExternalImage = externalImage
					};
			    }
				else
			    {
				    return null;
			    }
		    }
		    else
			{
				return null;
			}
	    }

	    private ExternalImage CreateMediaItem(WeShareSearchClasses.Hit mediaItem, string albumId)
	    {
		    var imageSize = mediaItem._source.metadata?.Specifications?.imageSize ?? string.Empty;
		    var sizes = imageSize.Split('x');

		    return new ExternalImage
		    {
			    ID = mediaItem._id,
			    AlbumID = albumId,
			    Title = mediaItem._source.name,
			    Date = mediaItem._source.updated,
			    Path = mediaItem._source.public_url,
			    Kind = "image",
			    Description = mediaItem._source.name,
			    ImgWidth = int.Parse(sizes.Length > 1 ? sizes[0].OrDefault("0") : "0"),
			    ImgHeight = int.Parse(sizes.Length > 1 ? sizes[1].OrDefault("0") : "0"),
			    ThumbnailSrc = $"data:{mediaItem._source.thumbnail.mime_type};base64, {mediaItem._source.thumbnail.base64}",
			    Src = mediaItem._source.public_url,
		    };
	    }

	    private static readonly Dictionary<string, string> WeShareCatBreedIdsDictionary = new Dictionary<string, string>
	    {
			{"49", "896283252"}, //SIBERIAN
		    {"48", "896282442"}, //SIAMESE
		    {"11", "896283247"}, //BRITISH_LONGHAIR
		    {"36", "896282441"}, //PERSIAN
		    //{"15", "896283253"}, //YORK_CHOCOLATE
		    {"31", "896283246"}, //MUNCHKIN
		    {"43", "896283255"}, //SCOTTISH_FOLD
		    {"9", "896283245"}, //BENGAL
		    {"33", "896283254"}, //NORWEGIAN_FOREST
		    {"29", "896282440"}, //MAINE_COON
		    {"1", "896283248"}, //ABYSSINIAN
		    //{"", "896282444"}, //ALL_CATS
		    {"12", "896282453"}, //BRITISH_SHORTHAIR
		    {"54", "896282454"}, //SPHYNX
		    {"13", "896283251"}, //EUROPEAN
		    {"40", "896283256"}, //RAGDOLL
		    {"30", "896283262"}, //MANX
		    {"19", "896283257"}, //RUSSIAN_BLUE
		    {"57", "896283258"}, //TURKISH_ANGORA
		    {"51", "896283260"}, //SNOWSHOE
		    {"15", "896283259"}, //CHARTREUX
		    {"32", "896283271"}, //TONKANESEN
		    {"10", "896283263"}, //BOMBAY
		    {"46", "896283267"}, //SELKIRK_REX
		    {"3", "896283269"}, //AMERICAN_CURL
		    {"58", "896283268"}, //TURKISH_VAN
		    {"50", "896283264"}, //SINGAPURA
		    {"2", "896283272"}, //AMERICAN_BOBTAIL
		    {"21", "896283273"}, //EXOTIC_SHORTHAIR
		    {"4", "896283274"}, //AMERICAN_SHORTHAIR
		    {"5", "896283275"}, //AMERICAN_WIREHAIR
		    {"28", "896283276"}, //LAPERM
		    {"22", "896283277"}, //GERMAN_REX
		    {"52", "896283279"}, //SOKOKE
		    {"8", "896283278"}, //BALINESEN
		    {"42", "896283281"}, //MANDARIN
		    {"23", "896283280"}, //HAVANA
		    //{"43", "896283282"}, //SOMALIKATZE
		    //{"13", "896283284"}, //BURMESEN
		    {"24", "896283283"}, //JAPANESE_BOBTAIL
		    {"26", "896283287"}, //KORAT
		    //{"46", "896283285"}, //JAVANESEN
		    //{"48", "896283288"}, //CEYLON
		    {"38", "896283289"}, //PIXIE_BOB
		    {"34", "896283290"}, //OCICAT
		    {"18", "896283291"}, //DEVON_REX
		    {"16", "896283292"}, //CORNISH_REX
		    {"53", "896283293"}, //OJOS_AZULES
		    {"17", "896283294"}, //CYMRIC
		    {"55", "896283304"} //THAI
	    };
	    private static readonly Dictionary<string, string> WeShareDogBreedIdsDictionary = new Dictionary<string, string>
	    {
		    {"263", "896283194"}, //LABRADOR_RETRIEVER
		    {"9", "896282848"}, //AUSTRALIAN_KELPIE
		    {"11", "896282850"}, //DUTCH_SHEPHERD
		    {"104", "896282781"}, //DACHSHUND
		    {"5", "896282844"}, //SMOOTH_FACED_PYRENEAN_SHEPHERD
		    {"12", "896282851"}, //AUSTRALIAN_STUMPY_TAIL_CATTLE_DOG
		    {"15", "896282854"}, //BOUVIERDES_FLANDRES
		    {"10", "896282849"}, //ROUGH_COLLIE
		    {"13", "896282852"}, //KOMONDOR
		    {"6", "896282845"}, //PULI
		    {"3", "896282842"}, //OLD_ENGLISH_SHEEPDOG
		    {"2", "896282841"}, //SHETLAND_SHEEPDOG
		    {"7", "896282846"}, //BRIARD
		    {"1", "896282840"}, //WHITE_SWISS_SHEPHERD_DOG
		    {"8", "896282847"}, //CADE_BESTIAR
		    {"4", "896282843"}, //WELSH_CORGI_CARDIGAN
		    {"14", "896282853"}, //SLOVAK_CUVAC
		    {"18", "896282857"}, //ROMANIAN_MIORTIC_SHEPHERD_DOG
		    {"21", "896282861"}, //WELSH_CORGI_PEMBROKE
		    {"23", "896282863"}, //ROMANIAN_CARPATHIAN_SHEPHERD_DOG
		    {"20", "896282860"}, //MAREMMAANDTHE_ABBRUZES_SHEEPDOG
		    {"24", "896282864"}, //GERMAN_SHEPHERD_DOG
		    {"17", "896282856"}, //PYRENEAN_SHEPHERD
		    {"16", "896282855"}, //AUSTRALIAN_SHEPHERD_DOG
		    {"22", "896282862"}, //CATALONIAN_SHEEPDOG
		    {"19", "896282858"}, //CROATIAN_SHEPHERD_DOG
		    {"25", "896282865"}, //PUMI
		    {"26", "896282866"}, //SCHAPENDOES
		    {"27", "896282867"}, //SAARLOOS_WOLFDOG
		    {"28", "896282868"}, //TATRA_SHEPHERD_DOG
		    {"29", "896282869"}, //PORTUGUESE_SHEEPDOG
		    {"30", "896282870"}, //SCHIPPERKE
		    {"33", "896282874"}, //SMOOTH_COLLIE
		    {"31", "896282871"}, //BELGIAN_SHEPHERD_DOG
		    {"36", "896282877"}, //BOUVIERDES_ARDENNES
		    {"38", "896282879"}, //POLISH_LOWLAND_SHEEPDOG
		    {"37", "896282878"}, //BEAUCE_SHEEP_DOG
		    {"32", "896282873"}, //BORDER_COLLIE
		    {"34", "896282875"}, //MUDI
		    {"35", "896282876"}, //KUVASZ
		    {"39", "896282880"}, //BEARDED_COLLIE
		    {"40", "896282881"}, //BERGAMASCO_SHEPHERD
		    {"41", "896282882"}, //CZECHOSLOVAKIAN_WOLFDOG
		    {"42", "896282883"}, //PICARDY_SHEPHERD
		    {"44", "896282885"}, //DOGO_CANARIO
		    {"45", "896282886"}, //DANISH_SWEDISH_FARMDOG
		    {"49", "896282890"}, //ENTLEBUCH_CATTLE_DOG
		    {"50", "896282891"}, //ESTRELA_MOUNTAIN_DOG
		    {"53", "896282894"}, //STANDARD_SCHNAUZER
		    {"51", "896282892"}, //MINIATURE_SCHNAUZER
		    {"47", "896282888"}, //GREATER_SWISS_MOUNTAIN_DOG
		    {"48", "896282889"}, //ROTTWEILER
		    {"46", "896282887"}, //HOVAWART
		    {"54", "896282896"}, //CHINESE_SHAR_PEI
		    {"52", "896282893"}, //MASTIFF
		    {"55", "896282897"}, //ANATOLIAN_SHEPHERD_DOG
		    {"56", "896282898"}, //KARST_SHEPHERD_DOG
		    {"57", "896282899"}, //TIBETAN_MASTIFF
		    {"58", "896282900"}, //BROHOLMER
		    {"59", "896282901"}, //CAO_FILADE_SAO_MIGUEL
		    {"60", "896282902"}, //BERNESE_MOUNTAIN_DOG
		    {"62", "896282904"}, //NEWFOUNDLAND
		    {"63", "896282905"}, //CANE_CORSO
		    {"64", "896282906"}, //DOGUEDE_BORDEAUX
		    {"66", "896282908"}, //LANDSEER
		    {"61", "896282903"}, //CAODE_CASTRO_LABOREIRO
		    {"67", "896282909"}, //GREAT_DANE
		    {"65", "896282907"}, //CIMARRON_URUGUAYO
		    {"68", "896282910"}, //DOGO_ARGENTINO
		    {"69", "896282911"}, //SAINT_BERNARD
		    {"70", "896282912"}, //GERMAN_PINSCHER
		    {"72", "896282913"}, //BULLMASTIFF
		    {"76", "896282916"}, //GREAT_PYRENEES
		    {"74", "896282914"}, //ENGLISH_BULLDOG
		    {"82", "896282920"}, //APPENZELL_CATTLE_DOG
		    {"78", "896282918"}, //PYRENEAN_MASTIFF
		    {"86", "896282923"}, //BOXER
		    {"88", "896282924"}, //AUSTRIAN_PINSCHER
		    {"84", "896282922"}, //DUTCH_SMOUSHOND
		    {"80", "896282919"}, //BLACK_RUSSIAN_TERRIER
		    {"90", "896282925"}, //RAFEIRODO_ALENTEJO
		    {"92", "896282926"}, //AIDI
		    {"94", "896282927"}, //AFFENPINSCHER
		    {"96", "896282928"}, //MINIATURE_PINSCHER
		    {"98", "896282929"}, //DOBERMAN_PINSCHER
		    {"100", "896282930"}, //PERRODE_PRESA_MALLORQUIN
		    {"102", "896282931"}, //NEAPOLITAN_MASTIFF
		    {"105", "896282932"}, //LEONBERGER
		    {"109", "896282935"}, //GIANT_SCHNAUZER
		    {"79", "896282940"}, //LAKELAND_TERRIER
		    {"77", "896282939"}, //SCOTTISH_TERRIER
		    {"73", "896282937"}, //CAIRN_TERRIER
		    {"75", "896282938"}, //GLENOF_IMAAL_TERRIER
		    {"71", "896282936"}, //SKYE_TERRIER
		    {"107", "896282934"}, //SPANISH_MASTIFF
		    {"81", "896282941"}, //NORFOLK_TERRIER
		    {"83", "896282942"}, //CESKY_TERRIER
		    {"85", "896282943"}, //BEDLINGTON_TERRIER
		    {"87", "896282944"}, //BRAZILIAN_TERRIER
		    {"89", "896282945"}, //WELSH_TERRIER
		    {"91", "896282946"}, //MANCHESTER_TERRIER
		    {"93", "896282947"}, //AIREDALE_TERRIER
		    {"95", "896282949"}, //WIRE_FOX_TERRIER
		    {"103", "896282953"}, //NORWICH_TERRIER
		    {"101", "896282952"}, //SOFT_COATED_WHEATEN_TERRIER
		    {"111", "896282958"}, //IRISH_TERRIER
		    {"108", "896282956"}, //SEALYHAM_TERRIER
		    {"99", "896282951"}, //BLACKAND_TAN_ENGLISH_TOY_TERRIER
		    {"112", "896282959"}, //JAGD_TERRIER
		    {"97", "896282950"}, //KERRY_BLUE_TERRIER
		    {"110", "896282957"}, //JACK_RUSSELL_TERRIER
		    {"106", "896282954"}, //YORKSHIRE_TERRIER
		    {"113", "896282960"}, //BULL_TERRIER
		    {"114", "896282961"}, //BORDER_TERRIER
		    {"115", "896282962"}, //WEST_HIGHLAND_WHITE_TERRIER
		    {"116", "896282963"}, //SILKY_TERRIER
		    {"117", "896282964"}, //PARSON_RUSSELL_TERRIER
		    {"118", "896282965"}, //DANDIE_DINMONT_TERRIER
		    {"123", "896282970"}, //CIRNECODELL_ETNA
		    {"122", "896282969"}, //AKITA_INU
		    {"120", "896282967"}, //JAPANESE_TERRIER
		    {"125", "896282972"}, //EAST_SIBERIAN_LAIKA
		    {"119", "896282966"}, //SMOOTH_FOX_TERRIER
		    {"128", "896282975"}, //CANAAN_DOG
		    {"121", "896282968"}, //AUSTRALIAN_TERRIER
		    {"126", "896282973"}, //ICELANDIC_SHEEPDOG
		    {"124", "896282971"}, //SHIBA_INU
		    {"127", "896282974"}, //THAI_RIDGEBACK
		    {"129", "896282976"}, //NORWEGIAN_BUHUND
		    {"130", "896282977"}, //EURASIER
		    {"131", "896282978"}, //XOLOITZCUINTLE
		    {"132", "896282979"}, //VOLPINO_ITALIANO
		    {"133", "896282980"}, //FINNISH_LAPPHUND
		    {"134", "896282981"}, //SWEDISH_VALLHUND
		    {"136", "896282983"}, //WEST_SIBERIAN_LAIKA
		    {"135", "896282982"}, //BASENJI
		    {"143", "896282990"}, //AMERICAN_AKITA
		    {"141", "896282988"}, //SAMOYED
		    {"142", "896282989"}, //GRAY_NORWEGIAN_ELKHOUND_GRAY
		    {"144", "896282991"}, //GREENLAND_DOG
		    {"137", "896282984"}, //KOREAN_JINDO_DOG
		    {"138", "896282985"}, //NORWEGIAN_LUNDEHUND
		    {"140", "896282987"}, //SWEDISH_ELKHOUND
		    {"139", "896282986"}, //SIBERIAN_HUSKY
		    {"146", "896282992"}, //NORWEGIAN_ELKHOUND_BLACK
		    {"148", "896282993"}, //JAPANESE_SPITZ
		    {"150", "896282994"}, //ALASKAN_MALAMUTE
		    {"152", "896282995"}, //IBIZAN_HOUND
		    {"154", "896282996"}, //KISHU
		    {"156", "896282997"}, //KAI_KEN
		    {"158", "896282998"}, //RUSSO_EUROPEAN_LAIKA
		    {"160", "896282999"}, //PERUVIAN_HAIRLESS_DOG
		    {"172", "896283005"}, //LAPPONIAN_HERDER
		    {"166", "896283002"}, //TAIWAN_DOG
		    {"162", "896283000"}, //NORBOTTENSPETS
		    {"170", "896283004"}, //SWEDISH_LAPPHUND
		    {"164", "896283001"}, //HOKKAIDO
		    {"168", "896283003"}, //PORTUGUESE_PODENGO
		    {"174", "896283006"}, //FINNISH_SPITZ
		    {"176", "896283007"}, //SHIKOKU
		    {"178", "896283008"}, //GERMAN_SPITZ
		    {"180", "896283009"}, //PHARAOH_HOUND
		    {"182", "896283011"}, //PODENCO_CANARIO
		    {"184", "896283012"}, //CHOW_CHOW
		    {"145", "896283081"}, //BLOODHOUND
		    {"147", "896283082"}, //PORCELAINE
		    {"153", "896283085"}, //SCHILLER_HOUND
		    {"155", "896283086"}, //FRANCAIS_BLANCET_ORANGE
		    {"149", "896283083"}, //BRIQUET_GRIFFON_VENDEEN
		    {"151", "896283084"}, //NORWEGIAN_HOUND
		    {"157", "896283087"}, //BASSET_BLEUDE_GASCOGNE
		    {"165", "896283091"}, //SPANISH_HOUND
		    {"163", "896283090"}, //PETIT_BASSET_GRIFFON_VENDEEN
		    {"161", "896283089"}, //ENGLISH_FOXHOUND
		    {"159", "896283088"}, //ANGLO_FRANCAISDE_PETITE_VENERIE
		    {"167", "896283092"}, //BASSET_ARTESIEN_NORMAND
		    {"169", "896283093"}, //OTTERHOUND
		    {"171", "896283094"}, //HUNGARIAN_HOUND
		    {"173", "896283095"}, //ITALIAN_COARSEHAIRED_HOUND
		    {"175", "896283096"}, //BOSNIAN_COARSEHAIRED_HOUND_BARAK
		    {"177", "896283097"}, //HELLENIC_HOUND
		    {"183", "896283100"}, //DREVER
		    {"179", "896283098"}, //BASSET_FAUVEDE_BRETAGNE
		    {"294", "896283107"}, //FINNISH_HOUND
		    {"195", "896283106"}, //PETIT_BLEUDE_GASCOGNE
		    {"187", "896283102"}, //CHIEN_FRANCAIS_TRICOLORE
		    {"193", "896283105"}, //GRAND_BLEUDE_GASCOGNE
		    {"181", "896283099"}, //SLOVAKIAN_HOUND
		    {"189", "896283103"}, //STYRIAN_COARSE_HAIRED_HOUND
		    {"191", "896283104"}, //HARRIER
		    {"185", "896283101"}, //GRIFFON_NIVERNAIS
		    {"197", "896283108"}, //SERBIAN_HOUND
		    {"199", "896283109"}, //SERBIAN_TRICOLOR_HOUND
		    {"201", "896283110"}, //GRIFFON_FAUVEDE_BRETAGNE
		    {"203", "896283111"}, //BASSET_HOUND
		    {"205", "896283112"}, //GREAT_ANGLO_FRANCAIS_TRICOLOR_HOUND
		    {"207", "896283113"}, //AMERICAN_FOXHOUND
		    {"209", "896283114"}, //GRAND_GRIFFON_VENDEEN
		    {"211", "896283115"}, //ARTOIS_HOUND
		    {"214", "896283116"}, //TYROLEAN_HOUND
		    {"223", "896283121"}, //RHODESIAN_RIDGEBACK
		    {"222", "896283120"}, //BEAGLE
		    {"216", "896283117"}, //SMALANDSSTOVARE
		    {"217", "896283118"}, //ARIEGEOIS
		    {"225", "896283122"}, //DALMATIAN
		    {"220", "896283119"}, //SMALL_SWISS_HOUND
		    {"228", "896283123"}, //HAMILTONSTOVARE
		    {"230", "896283124"}, //GREAT_ANGLO_FRANCAIS_WHITEAND_BLACK_HOUND
		    {"232", "896283125"}, //POSAVATZ_HOUND
		    {"234", "896283126"}, //GREAT_ANGLO_FRANCAIS_WHITEAND_ORANGE
		    {"235", "896283127"}, //BLACKAND_TAN_COONHOUND
		    {"241", "896283129"}, //HANOVERIAN_HOUND
		    {"244", "896283130"}, //BEAGLE_HARRIER
		    {"247", "896283131"}, //ITALIAN_SHORTHAIRED_HOUND
		    {"255", "896283134"}, //ALPINE_DACHSBRACKE
		    {"237", "896283128"}, //ISTRIAN_WIREHAIRED_HOUND
		    {"250", "896283132"}, //GRAND_BASSET_GRIFFON_VENDEEN
		    {"253", "896283133"}, //WESTPHALIAN_DACHSBRACKE
		    {"261", "896283136"}, //DEUTSCHE_BRACKE
		    {"258", "896283135"}, //MONTENEGRIN_MOUNTAIN_HOUND
		    {"265", "896283137"}, //POLISH_HOUND
		    {"267", "896283138"}, //CHIEN_FRANCAIS_BLANCET_NOIR
		    {"269", "896283139"}, //HYGENHUND
		    {"273", "896283141"}, //POLISH_HUNTING_DOG
		    {"271", "896283140"}, //GRIFFON_BLEUDE_GASCOGNE
		    {"274", "896283142"}, //POITEVIN
		    {"278", "896283144"}, //HALDEN_HOUND
		    {"280", "896283145"}, //BAVARIAN_MOUNTAIN_HOUND
		    {"285", "896283147"}, //SWISS_HOUND
		    {"282", "896283146"}, //GASCON_SAINTONGEOIS
		    {"276", "896283143"}, //AUSTRIAN_BLACKAND_TAN_HOUND
		    {"288", "896283148"}, //ISTRIAN_SHORTHAIRED_HOUND
		    {"291", "896283149"}, //BILLY
		    {"186", "896283150"}, //PONT_AUDEMER_SPANIEL
		    {"188", "896283151"}, //LARGE_MUNSTERLANDER
		    {"190", "896283152"}, //BRAQUEDU_BOURBONNAIS
		    {"192", "896283153"}, //IRISH_REDAND_WHITE_SETTER
		    {"194", "896283154"}, //PORTUGUESE_POINTER
		    {"196", "896283155"}, //SPINONE_ITALIANO
		    {"198", "896283156"}, //DRENTSCHE_PATRIJSHOND
		    {"208", "896283161"}, //WIREHAIRED_VIZSLA
		    {"200", "896283157"}, //BRACCO_ITALIANO
		    {"204", "896283159"}, //CESKY_FOUSEK
		    {"202", "896283158"}, //BRAQUE_FRANCAIS_GASCOGNE_TYPE
		    {"206", "896283160"}, //DEUTSCH_STICHELHAAR
		    {"210", "896283162"}, //GERMAN_WIREHAIRED_POINTER
		    {"212", "896283163"}, //BRAQUED_AUVERGNE
		    {"213", "896283164"}, //SMALL_MUNSTERLANDER
		    {"215", "896283165"}, //OLD_DANISH_POINTER
		    {"218", "896283166"}, //POINTER
		    {"219", "896283167"}, //BRAQUE_FRANCAIS_PYRENEES_TYPE
		    {"221", "896283168"}, //GORDON_SETTER
		    {"224", "896283169"}, //BRITTANY
		    {"226", "896283170"}, //PERDIGUERODE_BURGOS
		    {"231", "896283173"}, //BRAQUEDEL_ARIEGE
		    {"229", "896283172"}, //SLOVAKIAN_WIREHAIRED_POINTER
		    {"236", "896283175"}, //GERMAN_SHORTHAIRED_POINTER
		    {"227", "896283171"}, //PUDELPOINTER
		    {"233", "896283174"}, //WEIMARANER
		    {"238", "896283176"}, //IRISH_SETTER
		    {"240", "896283177"}, //VIZSLA
		    {"243", "896283178"}, //WIREHAIRED_POINTING_GRIFFON
		    {"246", "896283179"}, //BRAQUE_SAINT_GERMAIN
		    {"249", "896283180"}, //GERMAN_LONGHAIRED_POINTER
		    {"252", "896283181"}, //FRENCH_SPANIEL
		    {"256", "896283182"}, //PICARDY_SPANIEL
		    {"259", "896283183"}, //ENGLISH_SETTER
		    {"262", "896283184"}, //STABYHOUN
		    {"264", "896283185"}, //BLUE_PICARDY_SPANIEL
		    {"245", "896283188"}, //CHESAPEAKE_BAY_RETRIEVER
		    {"242", "896283187"}, //IRISH_WATER_SPANIEL
		    {"251", "896283190"}, //LAGOTTO_ROMAGNOLO
		    {"248", "896283189"}, //DEUTSCHER_WACHTELHUND
		    {"257", "896283192"}, //COCKER_SPANIEL
		    {"254", "896283191"}, //AMERICAN_WATER_SPANIEL
		    {"239", "896283186"}, //FIELD_SPANIEL
		    {"260", "896283193"}, //CLUMBER_SPANIEL
		    {"266", "896283195"}, //CURLY_COATED_RETRIEVER
		    {"268", "896283196"}, //WELSH_SPRINGER_SPANIEL
		    {"270", "896283197"}, //SPANISH_WATER_DOG
		    {"272", "896283198"}, //KOOIKERHONDJE
		    {"275", "896283199"}, //ENGLISH_COCKER_SPANIEL
		    {"277", "896283200"}, //FRISIAN_WATER_DOG
		    {"279", "896283201"}, //BARBET
		    {"284", "896283203"}, //FLAT_COATED_RETRIEVER
		    {"281", "896283202"}, //PORTUGUESE_WATER_DOG
		    {"290", "896283205"}, //GOLDEN_RETRIEVER
		    {"287", "896283204"}, //NOVA_SCOTIA_DUCK_TOLLING_RETRIEVER
		    {"293", "896283206"}, //SUSSEX_SPANIEL
		    {"296", "896283207"}, //ENGLISH_SPRINGER_SPANIEL
		    {"289", "896283210"}, //JAPANESE_CHIN
		    {"283", "896283208"}, //ENGLISH_TOY_SPANIEL
		    {"286", "896283209"}, //HAVANESE
		    {"292", "896283211"}, //GRIFFONS_BELGES
		    {"295", "896283212"}, //PEKINGESE
		    {"297", "896283213"}, //CHINESE_CRESTED_DOG
		    {"298", "896283214"}, //POODLE
		    {"299", "896283215"}, //LOWCHEN
		    {"300", "896283216"}, //FRENCH_BULLDOG
		    {"301", "896283217"}, //BOSTON_TERRIER
		    {"307", "896283223"}, //PUG
		    {"303", "896283219"}, //COTONDU_TULEAR
		    {"304", "896283220"}, //MALTESE
		    {"306", "896283222"}, //RUSSIAN_TOY
		    {"305", "896283221"}, //CONTINENTAL_TOY_SPANIEL
		    {"309", "896283225"}, //LHASA_APSO
		    {"302", "896283218"}, //CAVALIER_KING_CHARLES_SPANIEL
		    {"308", "896283224"}, //TIBETAN_SPANIEL
		    {"310", "896283226"}, //CHIHUAHUA
		    {"311", "896283227"}, //BOLOGNESE
		    {"312", "896283228"}, //TIBETAN_TERRIER
		    {"313", "896283229"}, //KROMFOHRLANDER
		    {"314", "896283230"}, //SHIH_TZU
		    {"316", "896283231"}, //BICHON_FRISE
		    {"315", "896283232"}, //ITALIAN_GREYHOUND
		    {"317", "896283233"}, //SLOUGHI
		    {"319", "896283237"}, //SCOTTISH_DEERHOUND
		    {"320", "896283239"}, //MAGYAR_AGAR
		    {"322", "896283242"}, //WHIPPET
		    {"321", "896283240"}, //SPANISH_GREYHOUND
		    {"323", "896283243"}, //IRISH_WOLFHOUND
		    {"318", "896283235"} //SALUKI
		    //{"", "896282443"} //ALL_DOGS
	    };

	    private static readonly Dictionary<string, string> WeSharePillarCodesDictionary = new Dictionary<string, string>
	    {
		    {"sptretail", "POS"},
		    {"spt retail", "POS"},
		    {"vet", "VET"},
		    {"pro", "PRO"}
	    };

	    private static readonly Dictionary<string, string> WeShareSizeCodesDictionary = new Dictionary<string, string>
	    {
		    {"xsmall", "xsmall"}, //XSmall
		    {"mini", "mini_1_10_kg"}, //Mini
		    {"medium", "medium_11_25_kg"}, //Medium
		    {"maxi", "maxi_26_44_kg"}, //Maxi
		    {"giant", "giant_from_45_kg"} //Giant
	    };

	    private BreedDto BuildBreedDtoFromWeShareProduct(Hit product)
	    {
		    var code = product.Source.Metadata.Hidden.Technical?.BreedsCode?.FirstOrDefault();
		    if (!string.IsNullOrEmpty(code))
			{
				var breed = new BreedDto
				{
					BreedId = product.IsDog
						? (WeShareDogBreedIdsDictionary.ContainsKey(code) ? WeShareDogBreedIdsDictionary[code] : code)
						: (WeShareCatBreedIdsDictionary.ContainsKey(code) ? WeShareCatBreedIdsDictionary[code] : code)
				};
				return breed;
			}

		    return null;
	    }

	    private static readonly string WeShareVariantMappingSplittingToken = " OR ";
	    private static readonly string WeShareParagraphSeparator = "<br><br>";

	    private static string GetProductNameFromWeShareProduct(Hit product)
	    {
		    var result = $"{product.Source.Metadata.Details.CommercialLabel}";
		    return result;
		}

	    private static string GetMainDescriptionFromWeShareProduct(Hit z)
	    {
		    var result = $"{z.Source.Metadata.Details.LegalTextHeading}";
		    return result;
		}

        private static string GetAllCompositionsFromWeShareProductByKey(Hit z)
        {
            var contents = z.Source.Contents.Where(content => content.Title == "Compositions")
                .SelectMany(content => content.ContentValue)
                .Where(content => !content.Value.IsNullOrWhiteSpace())
                .Select(x => x.Value);

            var result = string.Join(WeShareParagraphSeparator, contents);
            return result;
        }

        private static string GetCompositionsFromWeShareProductByKey(Hit z, string key)
	    {
	        var contents = z.Source.Contents.Where(content => content.Title == "Compositions")
	            .SelectMany(content => content.ContentValue)
	            .Where(content => !content.Value.IsNullOrWhiteSpace());

		    var result = contents
			    .FirstOrDefault(x => x.Key.Equals(key, StringComparison.InvariantCultureIgnoreCase))
		        ?.Value;

		    return result;
	    }

		private static string GetIngredientsFromWeShareProduct(Hit z, LocalMarketSettings localMarketSettings)
	    {
            if (z.Source.Contents == null)
                return "";

	        if (localMarketSettings != null && localMarketSettings.ConcatenateAllCompositions_WeShare)
	        {
	            return GetAllCompositionsFromWeShareProductByKey(z);
            }
            else
	        {
	            return GetCompositionsFromWeShareProductByKey(z, "COMPOSITION");
            }
        }

	    private static string GetProductDescriptionFromWeShareProduct(Hit z)
	    {
            if (z.Source.Contents == null)
                return "";

		    var contents = z.Source.Contents.Where(content => content.Title == "Text")
			    .SelectMany(content => content.ContentValue)
			    .Where(content => !content.Value.IsNullOrWhiteSpace());

		    /*var longDescription = contents
			    .Where(x => x.Key.Equals("EretailLong Description", StringComparison.InvariantCultureIgnoreCase))
			    .Select(x => x.Value);*/

		    var shortDescription = contents
			    .Where(x => x.Key.Equals("EretailShort Description", StringComparison.InvariantCultureIgnoreCase))
			    .Select(x => x.Value);

			var result = string.Join(WeShareParagraphSeparator, shortDescription);
			return result;
		}

        //private static string GetContentValueByPredicate(
        //    IList<Content> contents,
        //    Func<Content, bool> predicate)
        //{
        //    var content = contents.Where(predicate).FirstOrDefault();
        //    return content?.ContentValue?.FirstOrDefault()?.Value;
        //}

        private static ContentItem GetContentByPredicate(
		    IList<Content> contents,
		    Func<Content, bool> predicate,
		    int nToSkip = 0)
	    {
		    ContentItem content = contents
			    .Where(predicate)
			    .SelectMany(x => x?.ContentValue)
			    .Skip(nToSkip)
			    .FirstOrDefault();
		    return content;
	    }

		private static string GetBenefitTextFromWeShareProduct(Hit z, int n)
	    {
            if (z.Source.Contents == null)
                return "";

		    var benefit = GetContentByPredicate(z.Source.Contents, content => content.Title == "Benefits", n);

		    var result = benefit?.Value;
		    return result;
		}

		private static string GetBenefitTitleFromWeShareProduct(Hit z, int n)
	    {
            if (z.Source.Contents == null)
                return "";

		    var benefit = GetContentByPredicate(z.Source.Contents, content => content.Title == "Benefits", n);

		    var result = benefit?.Key;
		    return result;
		}

	    private static string GetFeedingGuideTextFromWeShareProduct(Hit z)
	    {
            if (z.Source.Contents == null)
                return "";

		    var feedingGuide = GetContentByPredicate(z.Source.Contents, content => content.Title == "Feeding Guide Lines");
		    return feedingGuide?.Table.Description;
	    }

	    private static IList<int> GetFeedingGuideImageIdFromWeShareProduct(Hit z)
	    {
	        var result = new List<int>();

            if (z.Source.Contents == null)
                return result;

		    var feedingGuide = GetContentByPredicate(z.Source.Contents, content => content.Title == "Feeding Guide Lines");
		    var idText = feedingGuide?.Table.AssetId;

	        if (int.TryParse(idText, out var id))
	        {
                result.Add(id);
	        }

	        return result;
	    }
		#endregion
	}
}