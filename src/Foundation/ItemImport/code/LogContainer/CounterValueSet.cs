﻿using System;
using Delete.Foundation.ItemImport.Models;
using ILog = log4net.ILog;

namespace Delete.Foundation.ItemImport.LogContainer
{
    public interface ILogContainer
    {
        ILog logger { get; set; }
        ImportLog Log { get; set; }
    }
    public class CounterValueSet:ILogContainer
    {
        public ILog logger { get; set; }
        public ImportLog Log { get; set; }
    }
}