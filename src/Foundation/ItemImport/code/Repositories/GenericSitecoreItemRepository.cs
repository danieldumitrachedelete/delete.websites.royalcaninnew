﻿using System;
using System.Collections.Generic;
using System.Linq;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.DeleteFoundationCore.Models;
using Glass.Mapper.Sc;
using Sitecore.Data;
using Sitecore.Globalization;

namespace Delete.Foundation.ItemImport.Repositories
{
    public interface IGenericSitecoreItemRepository<TItem> where TItem : GlassBase, IGlassBase
    {
        TItem GetByGuid(Guid id);

        IEnumerable<TItem> GetByPath(string path, Language language = null, bool mapDatabaseFields = false);

        TItem GetOneByQuery(string query);

        IEnumerable<TItem> GetByQuery(string query, Language language = null);

        TItem Create(string itemName, GlassBase parent, Language language = null);

        TItem Create(string itemName, string path, Language language = null);

        void Save(TItem item);

        void Delete(TItem item);
    }

    public class GenericSitecoreItemRepository<TItem> : IGenericSitecoreItemRepository<TItem> where TItem : GlassBase, IGlassBase
    {
        private readonly ISitecoreContext context;

        private readonly Dictionary<string, GlassBase> cachedParents = new Dictionary<string, GlassBase>();

        private readonly IBaseSearchManager<TItem> searchManager;

        public GenericSitecoreItemRepository(ISitecoreContext sitecoreContext, IBaseSearchManager<TItem> searchManager)
        {
            this.context = sitecoreContext;
            this.searchManager = searchManager;
        }

        public TItem GetByGuid(Guid id)
        {
            return this.context.GetItem<TItem>(id, inferType: true);
        }

        public IEnumerable<TItem> GetByPath(string path, Language language = null, bool mapDatabaseFields = false)
        {
            return searchManager.GetAllWithTemplate(path: path, mapResults: mapDatabaseFields, language: language).ToList();
        }

        public TItem GetOneByQuery(string query)
        {
            // same note as in GetByQuery
            return this.context.QuerySingle<TItem>(query, inferType: true);
        }

        public IEnumerable<TItem> GetByQuery(string query, Language language = null)
        {
            // todo: fix this
            // in order for relative paths to work (which would be useful for retrieving local-market paths easier), we would need to implement querying relatively from an arbitrary item, or construct absolute paths in our code
            /*return _context.QueryRelative<TItem>(query, inferType: true);*/

            return language == null
                ? this.context.Query<TItem>(query, inferType: true)
                : this.context.Query<TItem>(query, language, inferType: true);
        }

        public TItem Create(string itemName, GlassBase parent, Language language = null)
        {
            // we are creating items via itemName so that the Standard Values are applied correctly
            return this.context.Create<TItem, GlassBase>(parent, itemName, language);
        }

        public TItem Create(string itemName, string path, Language language = null)
        {
            var parent = language == null 
                ? this.context.GetItem<GlassBase>(path)
                : this.context.GetItem<GlassBase>(path, language);
            return this.Create(itemName, parent, language);
        }

        public void Save(TItem item)
        {
            this.context.Save(item, silent: true);

            var id = new ID(item.Id);
            this.context.Database.Caches.DataCache.RemoveItemInformation(id);
            this.context.Database.Caches.ItemCache.RemoveItem(id);
            this.context.Database.Caches.PathCache.RemoveKeysContaining(id.ToString());
        }

        public void Delete(TItem item)
        {
            this.context.Delete(item);
        }
    }
}