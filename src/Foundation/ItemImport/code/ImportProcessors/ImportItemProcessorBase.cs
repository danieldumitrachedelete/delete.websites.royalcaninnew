﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.DeleteFoundationCore.Models;
using Delete.Foundation.ItemImport.LogManagerImport;
using Delete.Foundation.ItemImport.Models;
using Delete.Foundation.ItemImport.Repositories;
using Glass.Mapper.Sc;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using Sitecore.DependencyInjection;
using Sitecore.Globalization;

namespace Delete.Foundation.ItemImport.ImportProcessors
{
    public interface IBaseImportItemProcessor<out TItem, TImportObj> where TItem : IGlassBase
    {
        TItem ProcessItem(TImportObj importObj, IEnumerable<Language> languageVersions, string pathOverride = null);


    }

    public abstract class BaseImportItemProcessor<TItem, TImportObj> : IBaseImportItemProcessor<TItem, TImportObj> where TItem : GlassBase, IGlassBase
    {
        protected static readonly Regex MultipleWhitespacesRegex = new Regex(@"\s+", RegexOptions.Compiled);

        #region helper functions for transforming item location path in descendants

        protected readonly string LocationPathOverride;

        protected IGenericSitecoreItemRepository<TItem> GenericItemRepository { get; }

        protected ISitecoreContext Context { get; }

        protected abstract Func<TItem, string> IdStringFromSitecoreItem { get; }

        protected abstract Func<TImportObj, string> IdStringFromImportObj { get; }

        protected abstract Func<TImportObj, string> ItemNameFromImportObj { get; }

        protected abstract string DefaultLocation { get; }

        protected virtual string ItemLocation =>
            this.LocationPathOverride
            ?? this.DefaultLocation;

        /// <summary>
        /// Should be set to true if processor operates with Sitecore entity in IdStringFromSitecoreItem method
        /// <example>PackagingSizeItemProcessor</example>
        /// </summary>
        protected virtual bool MapDatabaseFields => false;
        
        protected virtual bool OverwriteValuesOnUpdate => true;

        protected virtual bool CacheItems => true;

        protected IDictionary<string, IList<TItem>> ItemsCache;

        protected ILogManagerImport LogManagerLocalImport;
        protected BaseImportItemProcessor(ISitecoreContext sitecoreContext, ILogManagerImport logManagerImport, string locationPathOverride = null)
        {
            this.Context = sitecoreContext;
            var searchManager = ServiceLocator.ServiceProvider.GetService<IBaseSearchManager<TItem>>();
            this.GenericItemRepository = new GenericSitecoreItemRepository<TItem>(sitecoreContext, searchManager);
            this.LocationPathOverride = locationPathOverride;
            this.ItemsCache = new Dictionary<string, IList<TItem>>();
            this.LogManagerLocalImport = logManagerImport;
        }

        public virtual TItem GetItem(
            string targetIdString,
            TImportObj importObj = default(TImportObj),
            string locationOverride = null,
            Language language = null,
            Func<TItem, bool> defaultItemSelector = null)
        {
            if (string.IsNullOrEmpty(targetIdString)) return null;

            var itemLocation = locationOverride ?? this.CalculateItemLocation(importObj);

            var items = this.GetItems(itemLocation, language);

            var matchedItems = items
                .Where(x => targetIdString.Equals(this.CalculateItemId(x), StringComparison.OrdinalIgnoreCase))
                .ToList();

            if (!matchedItems.Any() && defaultItemSelector != null)
            {
                matchedItems = items.Where(defaultItemSelector).ToList();
            }

            if (matchedItems.Count > 1)
            {
                LogManagerLocalImport.AddEntryToLog(new ImportLogEntry
                {
                    Level = MessageLevel.Error,
                    Message = $"{this.GetType()}: Multiple existing matches found in \"{itemLocation}\" for Id=\"{targetIdString}\""
                });
            }

            return matchedItems.FirstOrDefault();
        }

        public virtual IEnumerable<TItem> GetItems(string rootPath, Language language = null)
        {
            var cacheKey = (language?.Name ?? String.Empty) + rootPath;

            if (CacheItems && ItemsCache != null && ItemsCache.ContainsKey(cacheKey))
            {
                return ItemsCache[cacheKey];
            }

            var items = this.GenericItemRepository.GetByPath(rootPath, language ?? Language.Current, MapDatabaseFields);

            if (CacheItems)
            {
                ItemsCache?.Add(cacheKey, items.ToList());
            }

            return items;
        }

        public virtual TItem ProcessItem(TImportObj importObj, IEnumerable<Language> languageVersions,
            string pathOverride = null)
        {
            if (importObj == null)
            {
                return null;
            }
            
            var defaultLanguage = LanguageManager.DefaultLanguage;

            var newId = this.CalculateItemId(importObj);
            var itemLocationInTargetContext = pathOverride ?? this.CalculateItemLocation(importObj);

            var defaultItem = this.GetItem(newId, importObj, itemLocationInTargetContext, defaultLanguage);

            if (defaultItem == null)
            {
                defaultItem = this.CreateItem(importObj, itemLocationInTargetContext, defaultLanguage);
            }
            else
            {
                if (!OverwriteValuesOnUpdate)
                {
                    return defaultItem;
                }
            }

            //var defaultItem = this.GetItem(newId, importObj, itemLocationInTargetContext, defaultLanguage) ?? this.CreateItem(importObj, itemLocationInTargetContext, defaultLanguage);

            defaultItem = this.MapDefaultVersionFields(defaultItem, importObj);
            defaultItem.Language = defaultLanguage.Name;
            this.SaveItem(defaultItem);

            var languageList = languageVersions ?? new List<Language>();

            if (languageList.Any())
            {
                foreach (var language in languageList)
                {
                    var languageItem = this.GetItem(newId, importObj, itemLocationInTargetContext, language)
                                       ?? defaultItem;

                    languageItem = this.MapLanguageVersionFields(languageItem, importObj, languageList );

                    languageItem.Language = language.Name;
                    this.SaveItem(languageItem);
                }
            }



            return defaultItem;


        }

        

        protected virtual TItem SaveItem(TItem updatedItem)
        {
            this.GenericItemRepository.Save(updatedItem);
            return updatedItem;
        }
        

        protected virtual TItem CreateItem(TImportObj importObj, string itemLocationOverride = null, Language language = null)
        {
            var newItemName = this.ProposeSitecoreItemName(this.ItemNameFromImportObj(importObj));
            var itemLocation = itemLocationOverride ?? this.CalculateItemLocation(importObj);
            var cacheKey = (language?.Name ?? String.Empty) + itemLocation;

            var newItem = this.GenericItemRepository.Create(newItemName, itemLocation, language);

            if (CacheItems && ItemsCache != null && ItemsCache.ContainsKey(cacheKey))
            {
                ItemsCache[cacheKey].Add(newItem);
            }

            return newItem;
        }

        #endregion

        
        protected virtual TItem MapLanguageVersionFields(TItem item, TImportObj importObj, IEnumerable<Language> languages)
        {
            var itemName = this.ItemNameFromImportObj(importObj).Trim();
            item.DisplayName = this.AmendDisplayName(itemName);
            item.DisplayNameString = this.AmendDisplayName(itemName);
            return item;
        }

        protected virtual TItem MapDefaultVersionFields(TItem item, TImportObj importObj)
        {
            return item;
        }

        protected virtual string ProposeSitecoreItemName(string name)
        {
            return MultipleWhitespacesRegex.Replace(ItemUtil.ProposeValidItemName(name), " ").Trim(' ');
        }

        protected virtual string AmendDisplayName(string name)
        {
            return MultipleWhitespacesRegex.Replace(name, " ").Trim(' ');
        }

        // method to be used for adding custom conditional item location resolution
        protected virtual string CalculateItemLocation(TImportObj importObj)
        {
            return this.ItemLocation;
        }

        protected string CalculateItemId(TItem item)
        {
            return this.IdStringFromSitecoreItem(item);
        }

        protected string CalculateItemId(TImportObj importObj)
        {
            return this.IdStringFromImportObj(importObj);
        }
    }
}