﻿using System;
using System.Collections.Generic;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.ItemImport.LogManagerImport;
using Delete.Foundation.ItemImport.Models;
using Delete.Foundation.ItemImport.Services;
using Delete.Foundation.MultiSite.Models;
using Glass.Mapper.Sc;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Globalization;

namespace Delete.Foundation.ItemImport.ImportProcessors
{
    public interface IMediaItemItemProcessor : IBaseImportItemProcessor<GlassMediaItem,
        ImageDto>
    {
    }

    public class MediaItemItemProcessor :
        BaseImportItemProcessor<GlassMediaItem, ImageDto>,
        IMediaItemItemProcessor
    {
        private const string IconUrlTemplate = "~/media/{0}.ashx?h=16&thn=1&w=16";

        private readonly IMediaItemService mediaItemService;

        public MediaItemItemProcessor(
            ISitecoreContext sitecoreContext,
            IMediaItemService mediaItemService,
            ILogManagerImport logManagerImport,
            string locationPathOverride = null)
            : base(sitecoreContext, logManagerImport, locationPathOverride)
        {
            this.mediaItemService = mediaItemService;
        }

        protected override Func<GlassMediaItem, string> IdStringFromSitecoreItem => x => x.Name;

        protected override Func<ImageDto, string> IdStringFromImportObj => GetFilename;

        protected override Func<ImageDto, string> ItemNameFromImportObj => GetFilename;

        // not used, paths are handled by overriding CalculateItemLocation
        protected override string DefaultLocation => string.Empty;

        public override GlassMediaItem ProcessItem(ImageDto importObj, IEnumerable<Language> languageVersions,
            string pathOverride = null)
        {
            if (importObj == null || importObj.Url.IsNullOrEmpty() || importObj.Url.AsHttpUrl() == null)
            {
                return null;
            }

            var mediaItem = this.CreateOrUpdateMediaItem(importObj, pathOverride);
            var item = mediaItem != null && mediaItem.ID != (ID)null
                ? this.Context.GetItem<GlassMediaItem>(mediaItem.ID.Guid)
                : null;
            return item;
        }

        private string GetFilename(ImageDto importObj)
        {
            var imageUrl = importObj.Url.GetFileNameFromUrl();
            return imageUrl.Truncate(100);
        }

        private MediaItem CreateOrUpdateMediaItem(ImageDto importObj, string pathOverride)
        {
            var imageUrl = importObj.Url.AsHttpUrl();
            var fullPath = pathOverride + "/" + this.ProposeSitecoreItemName(this.ItemNameFromImportObj(importObj));

            var localMarketSettings = this.Context.GetRootItem<LocalMarketSettings>();
            var skipExistingImages = localMarketSettings.SkipExistingImages_Salsify;

            var mediaItem = this.mediaItemService.CreateOrUpdateMediaItem(fullPath, importObj.Alt, imageUrl,  failSilently: true, skipExistingImages: skipExistingImages);
            return mediaItem;
        }
    }
}