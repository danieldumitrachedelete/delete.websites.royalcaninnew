﻿using System;
using System.Collections.Generic;
using Delete.Foundation.DataTemplates.Models.Taxonomy;
using Delete.Foundation.DeleteFoundationCore.Models;
using Delete.Foundation.ItemImport.LogManagerImport;
using Glass.Mapper.Sc;

namespace Delete.Foundation.ItemImport.ImportProcessors
{
    public interface ISpeciesSpecificImportItemProcessorBase<out TItem, TImportObj> 
        : IBaseImportItemProcessor<TItem, TImportObj> 
        where TItem : IGlassBase
    {
    }

    public abstract class SpeciesSpecificImportItemProcessorBase<TItem, TImportObj> 
        : BaseImportItemProcessor<TItem, TImportObj>, ISpeciesSpecificImportItemProcessorBase<TItem, TImportObj> 
        where TItem : GlassBase, IGlassBase
    {
        protected static readonly Dictionary<Species, string> DefaultSpeciesPathDictionary =
            new Dictionary<Species, string>
        {
            {
                Species.Cats, "Cat"
            },
            {
                Species.Dogs, "Dog"
            }
        };

        private readonly Species species;

        protected SpeciesSpecificImportItemProcessorBase(
            ISitecoreContext sitecoreContext,
            ILogManagerImport logManagerImport,
            Species species, 
            string locationPathOverride = null) : base(sitecoreContext, logManagerImport, locationPathOverride)
        {
            this.species = species;
        }

        protected override string ItemLocation => base.ItemLocation + "/" + this.GetSpeciesPathSuffix(this.species);

        protected virtual string GetSpeciesPathSuffix(Species species)
        {
            return DefaultSpeciesPathDictionary[species];
        }
    }
}