﻿using Delete.Foundation.Geocoding.Models.Core;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Delete.Foundation.Geocoding.Managers
{
	public interface IBatchGeocoder
	{
		Task<IEnumerable<ResultItem>> GeocodeAsync(IEnumerable<string> addresses);
		Task<IEnumerable<ResultItem>> ReverseGeocodeAsync(IEnumerable<Location> locations);
	}
}
