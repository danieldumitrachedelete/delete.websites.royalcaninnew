using Delete.Foundation.InitializeDependencyInjection.Extensions;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.DependencyInjection;
using Delete.Foundation.Geocoding.Managers;

namespace Delete.Foundation.Geocoding.DependencyInjection
{
    public class RegisterServices : IServicesConfigurator
    {
        public void Configure(IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IGoogleGeocoder, GoogleGeocoder>();

            serviceCollection.AddMvcControllersInCurrentAssembly();
        }
    }
}
