﻿using System;

namespace Delete.Foundation.Geocoding.Models.Core
{
	public enum DistanceUnits
	{
		Miles,
		Kilometers
	}
}
