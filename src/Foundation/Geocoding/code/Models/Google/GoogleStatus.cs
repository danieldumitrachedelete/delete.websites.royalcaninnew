namespace Delete.Foundation.Geocoding.Models.Core.Google
{
	public enum GoogleStatus
	{
		Error,
		Ok,
		ZeroResults,
		OverQueryLimit,
		RequestDenied,
		InvalidRequest
	}
}