﻿namespace Delete.Foundation.Geocoding.Models.Core.Google
{
	public class GoogleViewport
	{
		public Location Northeast { get; set; }
		public Location Southwest { get; set; }
	}
}
