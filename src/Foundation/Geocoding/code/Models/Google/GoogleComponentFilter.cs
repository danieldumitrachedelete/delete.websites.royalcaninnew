﻿namespace Delete.Foundation.Geocoding.Models.Core.Google
{
    public class GoogleComponentFilter
    {
        public GoogleComponentFilter(string component, string value)
        {
            this.ComponentFilter = $"{component}:{value}";
        }
        
        public string ComponentFilter { get; set; }

        public static class Components
        {
            public static string Country => "country";
        }
    }
}
