class DLScriptsObserver {
    constructor() {
        this._isLoaded = null;
        this.subscribers = [];
        this.onDLWebpackDone = this.onDLWebpackDone.bind(this);
    }

    onDLWebpackDone() {
        this._isLoaded = true;
        this.subscribers.forEach(cb => cb());
    }

    isLoaded() {
        if (this._isLoaded === null) {
            if (window.webpackComplete) {
                this._isLoaded = true;
            } else {
                this._isLoaded = false;
                document.addEventListener('rc_webpack_done', this.onDLWebpackDone.bind(this));
            }
        }

        return this._isLoaded;
    }

    onLoad(cb) {
        if (this.isLoaded()) {
            cb();
        } else {
            this.subscribers.push(cb);
        }
    }
}

const instance = new DLScriptsObserver();

export default instance;
