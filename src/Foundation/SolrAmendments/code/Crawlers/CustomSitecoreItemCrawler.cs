﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Castle.Core.Internal;
using Delete.Foundation.MultiSite.Helpers;
using Sitecore;
using Sitecore.Abstractions;
using Sitecore.Common;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Diagnostics;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.LanguageFallback;
using Sitecore.Diagnostics;
using Sitecore.Globalization;
using Sitecore.Sites;

namespace Delete.Foundation.SolrAmendments.Crawlers
{
    public class CustomSitecoreItemCrawler : SitecoreItemCrawler
    {
        protected override void DoAdd(IProviderUpdateContext context, SitecoreIndexableItem indexable)
        {
            Assert.ArgumentNotNull(context, nameof(context));
            Assert.ArgumentNotNull(indexable, nameof(indexable));

            var allowedLanguagesResult = GetItemAllowedLanguages(indexable.Item);

            using (new LanguageFallbackItemSwitcher(context.Index.EnableItemLanguageFallback))
            {
#pragma warning disable 618
                this.Index.Locator.GetInstance<IEvent>().RaiseEvent("indexing:adding", context.Index.Name, indexable.UniqueId, indexable.AbsolutePath);
#pragma warning restore 618

                if (!this.IsExcludedFromIndex(indexable))
                {
                    foreach (var language in allowedLanguagesResult.IsLocalMarketItem ? allowedLanguagesResult.AllowedLanguages : indexable.Item.Languages)
                    {
                        Item obj1;
                        using (new WriteCachesDisabler())
                            obj1 = indexable.Item.Database.GetItem(indexable.Item.ID, language, Sitecore.Data.Version.Latest);
                        if (obj1 == null)
                        {
                            CrawlingLog.Log.Warn(
                                $"SitecoreItemCrawler : AddItem : Could not build document data {indexable.Item.Uri} - Latest version could not be found. Skipping.");
                        }
                        else
                        {
                            Item[] objArray1;
                            using (new WriteCachesDisabler())
                            {
                                var objArray2 = obj1.IsFallback ? new[] { obj1 } : obj1.Versions.GetVersions(false);
                                objArray1 = objArray2;
                            }
                            foreach (var obj2 in objArray1)
                            {
                                SitecoreIndexableItem sitecoreIndexableItem1 = obj2;
                                var sitecoreIndexableItem2 = sitecoreIndexableItem1;
                                ((IIndexableBuiltinFields)sitecoreIndexableItem2).IsLatestVersion = ((IIndexableBuiltinFields)sitecoreIndexableItem2).Version == obj1.Version.Number;
                                sitecoreIndexableItem1.IndexFieldStorageValueFormatter = context.Index.Configuration.IndexFieldStorageValueFormatter;
                                this.Operations.Add(sitecoreIndexableItem1, context, this.index.Configuration);
                            }
                        }
                    }
                }
#pragma warning disable 618
                this.Index.Locator.GetInstance<IEvent>().RaiseEvent("indexing:added", context.Index.Name, indexable.UniqueId, indexable.AbsolutePath);
#pragma warning restore 618
            }
        }

        protected override void UpdateItemVersion(
            IProviderUpdateContext context,
            Item version,
            IndexEntryOperationContext operationContext)
        {
            var allowedLanguagesResult = GetItemAllowedLanguages(version);

            var versionIndexable = this.PrepareIndexableVersion(version, context);

            if (allowedLanguagesResult.IsLocalMarketItem)
            {
                if (allowedLanguagesResult.AllowedLanguages.Any(x => x == version.Language))
                {
                    this.Operations.Update(versionIndexable, context, context.Index.Configuration);
                }
            }
            else
            {
                this.Operations.Update(versionIndexable, context, context.Index.Configuration);
            }

            this.UpdateClones(context, versionIndexable);
            this.CustomUpdateLanguageFallbackDependentItems(context, versionIndexable, operationContext, allowedLanguagesResult);
        }

        private void UpdateClones(
            IProviderUpdateContext context,
            SitecoreIndexableItem versionIndexable)
        {
            IEnumerable<Item> clones;
            using (new WriteCachesDisabler())
                clones = versionIndexable.Item.GetClones(false);
            foreach (var clone in clones)
            {
                var sitecoreIndexableItem = this.PrepareIndexableVersion(clone, context);
                if (!this.IsExcludedFromIndex(clone))
                    this.Operations.Update(sitecoreIndexableItem, context, context.Index.Configuration);
            }
        }

        protected void CustomUpdateLanguageFallbackDependentItems(IProviderUpdateContext context,
            SitecoreIndexableItem versionIndexable,
            IndexEntryOperationContext operationContext,
            ItemLanguageResolvingResult allowedLanguagesResult)
        {
            if (operationContext == null || operationContext.NeedUpdateAllLanguages)
                return;
            var obj = versionIndexable.Item;
            var currentValue = Switcher<bool?, LanguageFallbackFieldSwitcher>.CurrentValue;
            var flag1 = true;
            if ((currentValue.GetValueOrDefault() == flag1 ? (!currentValue.HasValue ? 1 : 0) : 1) != 0)
            {
                currentValue = Switcher<bool?, LanguageFallbackItemSwitcher>.CurrentValue;
                var flag2 = true;
                if ((currentValue.GetValueOrDefault() == flag2 ? (!currentValue.HasValue ? 1 : 0) : 1) != 0 || StandardValuesManager.IsStandardValuesHolder(obj) && obj.Fields[FieldIDs.EnableItemFallback].GetValue(false) != "1")
                    return;
                using (new LanguageFallbackItemSwitcher(new bool?(false)))
                {
                    if (obj.Fields[FieldIDs.EnableItemFallback].GetValue(true, true, false) != "1")
                        return;
                }
            }
            if (!obj.Versions.IsLatestVersion())
                return;

            var languageVersions = 
                this.GetItem(obj)
                    .Select(item => this.PrepareIndexableVersion(item, context))
                    .ToList();

            if (allowedLanguagesResult.IsLocalMarketItem)
                languageVersions = languageVersions
                    .Where(indexable => allowedLanguagesResult.AllowedLanguages
                        .Any(y => y == indexable.Item.Language))
                    .ToList();

            languageVersions
                .ForEach(sitecoreIndexableItem => this.Operations
                    .Update(sitecoreIndexableItem, context, context.Index.Configuration));
        }

        internal SitecoreIndexableItem PrepareIndexableVersion(
            Item item,
            IProviderUpdateContext context)
        {
            SitecoreIndexableItem sitecoreIndexableItem = item;
            ((IIndexableBuiltinFields)sitecoreIndexableItem).IsLatestVersion = item.Versions.IsLatestVersion();
            sitecoreIndexableItem.IndexFieldStorageValueFormatter = context.Index.Configuration.IndexFieldStorageValueFormatter;
            return sitecoreIndexableItem;
        }

        private static ConcurrentDictionary<string, IEnumerable<string>> AllowedLanguagesDictionary { get; } 
            = new ConcurrentDictionary<string, IEnumerable<string>>();

        private static void InitializeLangDictionary()
        {
            var websites = SiteManagerHelper
                .GetAvailableWebsites()
                .ToList();

            var groupedPathLangPairs = websites
                .Select(x => SiteContext.GetSite(x.Name))
                .Select(x => new KeyValuePair<string, string>(x.ContentStartPath, x.Language))
                .GroupBy(x => x.Key);

            foreach (var group in groupedPathLangPairs)
            {
                AllowedLanguagesDictionary.TryAdd(group.Key, group.Select(x => x.Value).Distinct());
            }
        }

        private static ItemLanguageResolvingResult GetItemAllowedLanguages(Item item)
        {
            try
            {
                if (AllowedLanguagesDictionary.IsNullOrEmpty())
                    InitializeLangDictionary();

                var marketKey = AllowedLanguagesDictionary.Keys
                    .FirstOrDefault(key => item.Paths.FullPath
                        .StartsWith(key, StringComparison.OrdinalIgnoreCase));

                if (!string.IsNullOrWhiteSpace(marketKey))
                {
                    var itemAllowedLanguages = AllowedLanguagesDictionary[marketKey].Select(Language.Parse).ToList();

                    if (!itemAllowedLanguages.Any(x => string.Equals(x.Name, "en", StringComparison.OrdinalIgnoreCase)))
                        itemAllowedLanguages.Add(Language.Parse("en"));

                    return new ItemLanguageResolvingResult
                    {
                        AllowedLanguages = itemAllowedLanguages.ToArray(),
                        ResolvedItemMarketPath = marketKey,
                        IsLocalMarketItem = true
                    };
                }

                return new ItemLanguageResolvingResult();
            }
            catch (Exception exception)
            {
                CrawlingLog.Log.Warn($"Get Item Allowed Languages failed, returning fallback value. Item ID: {item.ID}", exception);

                return new ItemLanguageResolvingResult();
            }
        }
    }
}