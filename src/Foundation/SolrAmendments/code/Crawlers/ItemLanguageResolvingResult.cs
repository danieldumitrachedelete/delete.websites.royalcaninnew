﻿using Sitecore.Globalization;

namespace Delete.Foundation.SolrAmendments.Crawlers
{
    public class ItemLanguageResolvingResult
    {
        public Language[] AllowedLanguages { get; set; }

        public string ResolvedItemMarketPath { get; set; }

        public bool IsLocalMarketItem { get; set; }
    }
}