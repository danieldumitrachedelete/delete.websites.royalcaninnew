﻿using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using Sitecore.Configuration;
using Sitecore.ContentSearch.SolrProvider.Pipelines.PopulateSolrSchema;
using Sitecore.Xml;
using SolrNet.Schema;

namespace Delete.Foundation.SolrAmendments.Pipelines
{
    public class CustomSchemaPopulatorHelper : ISchemaPopulateHelper
    {
        private readonly SolrSchema _solrSchema;
        private readonly string _indexName;

        public CustomSchemaPopulatorHelper(SolrSchema solrSchema, string indexName)
        {
            _solrSchema = solrSchema;
            _indexName = indexName;
        }

        public IEnumerable<XElement> GetAllFields()
        {
            var elements = new List<XElement>();

            foreach (XmlNode commands in Factory.GetConfigNodes("contentSearch/customSolrManagedSchema/commands"))
            {
                if (IsApplicable(commands, _indexName))
                {
                    elements.AddRange(commands.ChildNodes.Cast<XmlNode>().Select(n => ParseCommand(n.OuterXml, _solrSchema)));
                }
            }

            return elements;
        }

        private XElement ParseCommand(string xml, SolrSchema schema)
        {
            var element = XElement.Parse(xml);

            // set "add" or "replace" depending if field type already exists in schema
            if (element.Name == "add-or-replace-field-type")
            {
                var fieldTypeName = element.Elements().FirstOrDefault(e => e.Name == "name");
                if (fieldTypeName != null && !string.IsNullOrEmpty(fieldTypeName.Value))
                {
                    if (schema.SolrFieldTypes.Any(f => f.Name == fieldTypeName.Value))
                    {
                        element = new XElement("replace-field-type", element.Attributes(), element.Elements());
                    }
                    else
                    {
                        element = new XElement("add-field-type", element.Attributes(), element.Elements());
                    }
                }
            }

            return element;
        }

        private bool IsApplicable(XmlNode commands, string indexName)
        {
            var indexes = XmlUtil.GetAttribute("applyToIndex", commands);
            if (string.IsNullOrEmpty(indexes))
            {
                return false;
            }

            return indexes.Split('|').Any(i => i.ToLower().Equals(indexName.ToLower()));
        }
    }
}