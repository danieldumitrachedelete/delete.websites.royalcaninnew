﻿using System;
using Sitecore.ContentSearch.SolrProvider.Pipelines.PopulateSolrSchema;
using Sitecore.Diagnostics;

namespace Delete.Foundation.SolrAmendments.Pipelines
{
    public class CustomSchemaPopulator : PopulateFields
    {
        protected string IndexName;

        public override void Process(PopulateManagedSchemaArgs args)
        {
            IndexName = args.Index.Name;
            base.Process(args);
        }

        protected override ISchemaPopulateHelper GetHelper(SolrNet.Schema.SolrSchema schema)
        {
            Assert.ArgumentNotNull(schema, "schema");
            return new CustomSchemaPopulatorHelper(schema, IndexName);
        }
    }
}