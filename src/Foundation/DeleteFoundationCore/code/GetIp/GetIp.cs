﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Glass.Mapper.Sc.Web.Mvc;
using Sitecore.Pipelines.HttpRequest;

namespace Delete.Foundation.DeleteFoundationCore
{
    public interface IGetIp
    {
        string GetIpFromRequest(HttpContextBase context);
    }
    public class GetIp : IGetIp
    {
        public string GetIpFromRequest(HttpContextBase context)
        {
            var request = context.Request;
            var ip = string.Empty;
            //todo: get name header from config-files
            var xForwardedFor = request.Headers["X-Forwarded-For"];
            var remoteAddress = request.ServerVariables["REMOTE_ADDR"];
            if (string.IsNullOrEmpty(xForwardedFor))
            {
                this.LogDebug($"New request: IP from Remote_Address {remoteAddress}; url: {request.Url}");

                ip = remoteAddress.SafelySplit(":").FirstOrDefault();
            }
            else
            {
                ip = xForwardedFor.SafelySplit(",").FirstOrDefault()
                    .SafelySplit(":").FirstOrDefault();

                this.LogDebug($"New request: IP {ip} from HTTP_X_FORWARDED_FOR {xForwardedFor}; url: {request.Url}");
            }

            return ip;
        }
    }
}