namespace Delete.Foundation.DeleteFoundationCore.SchemaOrg
{
    using System.Runtime.Serialization;

    /// <summary>
    /// A patient is any person recipient of health care services.
    /// </summary>
    public enum Patient
    {
    }
}
