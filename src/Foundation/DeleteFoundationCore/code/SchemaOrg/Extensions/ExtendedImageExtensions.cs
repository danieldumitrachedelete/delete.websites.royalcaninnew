﻿using System;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Models;
using Delete.Foundation.DeleteFoundationCore.SchemaOrg.Interfaces;

namespace Delete.Foundation.DeleteFoundationCore.SchemaOrg.Extensions
{
    public static class ExtendedImageExtensions
    {
        public static ImageObject GetMicrodata(this ExtendedImage image, IAuthorResolver authorResolver)
        {
            return new ImageObject
            {
                Author = authorResolver?.GetAuthor() ?? new Organization(),
                ContentUrl = new Uri(image?.Src ?? String.Empty, UriKind.RelativeOrAbsolute),
                Url = new Uri(image?.Src ?? String.Empty, UriKind.RelativeOrAbsolute),
                Description = image?.Alt ?? String.Empty,
                ThumbnailUrl = new Uri(image?.Src ?? String.Empty, UriKind.RelativeOrAbsolute),
                UploadDate = (image?.DateUpdated ?? new DateTime()).ToDateTimeOffset()
            };
        }
    }
}