﻿namespace Delete.Foundation.DeleteFoundationCore.SchemaOrg.Interfaces
{
    public interface IAuthorResolver
    {
        Values<Organization, Person> GetAuthor();
    }
}