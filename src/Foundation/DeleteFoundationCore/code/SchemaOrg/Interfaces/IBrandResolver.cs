﻿namespace Delete.Foundation.DeleteFoundationCore.SchemaOrg.Interfaces
{
    public interface IBrandResolver
    {
        Values<Brand, Organization> GetBrand();
    }
}