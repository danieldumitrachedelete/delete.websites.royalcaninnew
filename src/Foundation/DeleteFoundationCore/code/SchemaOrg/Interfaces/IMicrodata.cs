﻿namespace Delete.Foundation.DeleteFoundationCore.SchemaOrg.Interfaces
{
    public interface IMicrodata
    {
        Thing GetMicrodata(IAuthorResolver authorResolver);
    }
}