namespace Delete.Foundation.DeleteFoundationCore.SchemaOrg
{
    using System.Runtime.Serialization;

    /// <summary>
    /// A DeliveryMethod in which an item is made available via locker.
    /// </summary>
    public enum LockerDelivery
    {
    }
}
