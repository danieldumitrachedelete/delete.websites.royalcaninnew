namespace Delete.Foundation.DeleteFoundationCore.SchemaOrg
{
    using System.Runtime.Serialization;

    /// <summary>
    /// A payment method using a credit, debit, store or other card to associate the payment with an account.
    /// </summary>
    public enum PaymentCard
    {
    }
}
