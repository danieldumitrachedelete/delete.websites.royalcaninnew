namespace Delete.Foundation.DeleteFoundationCore.SchemaOrg
{
    using System;
    using System.Runtime.Serialization;
    using Newtonsoft.Json;

    /// <summary>
    /// A photograph.
    /// </summary>
    [DataContract]
    public partial class Quotation : CreativeWork
    {
        /// <summary>
        /// Gets the name of the type as specified by schema.org.
        /// </summary>
        [DataMember(Name = "@type", Order = 1)]
        public override string Type => "Quotation";

        /// <summary>
        /// The (e.g. fictional) character, Person or Organization to whom the quotation is attributed within the containing CreativeWork.
        /// </summary>
        [DataMember(Name = "spokenByCharacter", Order = 206)]
        [JsonConverter(typeof(ValuesConverter))]
        public Values<Organization, Person>? SpokenByCharacter { get; set; }
    }
}
