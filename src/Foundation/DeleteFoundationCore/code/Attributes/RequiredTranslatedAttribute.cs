﻿using System.Collections;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Mvc;
using Sitecore.Globalization;

namespace Delete.Foundation.DeleteFoundationCore.Attributes
{
    public class RequiredTranslatedAttribute : RequiredAttribute
    {
        public override string FormatErrorMessage(string name)
        {
            return string.Format(CultureInfo.CurrentCulture, Translate.Text(ErrorMessageString), name);
        }

        public IEnumerable GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            return new[] { new ModelClientValidationRequiredRule(Translate.Text(ErrorMessage)) };
        }
    }
}