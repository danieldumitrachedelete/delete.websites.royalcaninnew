﻿using System.Linq;

namespace Delete.Foundation.DeleteFoundationCore.Extensions
{
    using System;
    using System.IO;

    public static class UriExtensions
    {
        public static string GetFileName(this Uri uri)
        {
            return Path.GetFileNameWithoutExtension(uri.LocalPath);
        }

        public static string GetFileNameWithExtension(this Uri uri)
        {
            return Path.GetFileName(uri.LocalPath);
        }

        public static string GetExtension(this Uri uri)
        {
            return Path.GetExtension(uri.LocalPath);
        }

        public static Uri Append(this Uri uri, params string[] paths)
        {
            return new Uri(paths.Aggregate(uri.AbsoluteUri, (current, path) => string.Format("{0}/{1}", current.TrimEnd('/'), path.TrimStart('/'))));
        }
    }
}