﻿using System.Collections.Generic;
using System.Linq;
using Delete.Foundation.DeleteFoundationCore.Models;

namespace Delete.Foundation.DeleteFoundationCore.Extensions
{
    using JetBrains.Annotations;
    using Sitecore.Data;

    public static class GlassBaseExtensions
    {
        /// <summary>
        /// Check if current item template or 1st level base templates are equals to passed template. Method do not load any data from db or index, so use it correctly
        /// </summary>
        /// <param name="item"></param>
        /// <param name="templateId"></param>
        /// <returns></returns>
        public static bool IsDerivedFrom([NotNull] this GlassBase item, [NotNull] ID templateId)
        {

            return item.TemplateId == templateId || item.BaseTemplates.Contains(templateId.Guid);
        }

        public static IOrderedEnumerable<TItem> SitecoreSort<TItem>(this IEnumerable<TItem> items) where TItem : GlassBase
        {
            return items.OrderBy(x => x?.Sortorder).ThenBy(x => x?.Name);
        }

        public static GlassBase GetAscendant<TItem>(this TItem currentItem, GlassBase stopItem) where TItem : GlassBase
        {
            var homeItemId = stopItem.Id;

            var item = currentItem as GlassBase;
            while (item != null)
            {
                if (item?.Parent?.Id == homeItemId)
                {
                    break;
                }

                item = item.Parent;
            }

            return item;
        }

        public static GlassBase GetAscendantOfTemplate<TItem>(this TItem currentItem, ID templateId) where TItem : GlassBase
        {
            var item = currentItem as GlassBase;
            while (item != null)
            {
                if (item.TemplateId == templateId)
                {
                    return item;
                }

                if (item.Parent == null)
                {
                    break;
                }

                item = item.Parent;
            }

            return null;
        }
    }
}
