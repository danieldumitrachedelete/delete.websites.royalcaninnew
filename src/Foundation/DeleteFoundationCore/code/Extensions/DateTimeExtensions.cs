﻿using System;

namespace Delete.Foundation.DeleteFoundationCore.Extensions
{
    public static class DateTimeExtensions
    {
        public static string ToGoogleDate(this DateTime date)
        {
            return date.ToString("yyyy-MM-dd");
        }

        public static DateTimeOffset ToDateTimeOffset(this DateTime dateTime)
        {
            return dateTime.ToUniversalTime() <= DateTimeOffset.MinValue.UtcDateTime
                ? DateTimeOffset.MinValue
                : new DateTimeOffset(dateTime);
        }
    }
}