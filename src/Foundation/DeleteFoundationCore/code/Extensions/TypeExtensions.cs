﻿using System;
using System.Linq;

namespace Delete.Foundation.DeleteFoundationCore.Extensions
{
    public static class TypeExtensions
    {
        public static string GetFriendlyName(this Type type)
        {
            var friendlyName = type.Name;
            if (type.IsGenericType)
            {
                var idxBacktick = friendlyName.IndexOf('`');
                if (idxBacktick > 0)
                {
                    friendlyName = friendlyName.Remove(idxBacktick);
                }

                var typeParameters = type.GetGenericArguments();
                var parameterNames = typeParameters.Select(typeParameter => typeParameter.GetFriendlyName()).ToList();
                friendlyName += $"<{string.Join(",", parameterNames)}>";
            }

            return friendlyName;
        }
    }
}