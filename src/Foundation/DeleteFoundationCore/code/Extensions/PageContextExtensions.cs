﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sitecore.Mvc.Presentation;

namespace Delete.Foundation.DeleteFoundationCore.Extensions
{
    public static class PageContextExtensions
    {
        public static bool IsDynamicPlaceholderNotEmpty(this PageContext context, string dynamicPlaceholderName, int seed)
        {
            var dynamicKey =
                $"{dynamicPlaceholderName}-{Sitecore.Mvc.Presentation.RenderingContext.Current.Rendering.UniqueId.ToString("B").ToUpperInvariant()}-{seed}";

            return context.IsPlaceholderNotEmpty(dynamicKey);
        }

        public static bool IsPlaceholderNotEmpty(this PageContext context, string placeholderKey)
        {
            var pageContext = context;
            var pageDefinition = pageContext.PageDefinition;
            return pageDefinition.Renderings.Any(x =>
                x.Placeholder.IndexOf(placeholderKey, StringComparison.InvariantCulture) >= 0);

        }

        public static bool DoesPlaceholderContainRenderingByName(this PageContext context,
            List<string> names, string dynamicPlaceholderName, int seed)
        {
            return context.DoesPlaceholderContainRenderingByCondition(x => names.Contains(x.RenderingItem.Name),
                dynamicPlaceholderName, seed);
        }

        public static bool DoesPlaceholderContainRenderingByGuid(this PageContext context,
            List<Guid> guids, string dynamicPlaceholderName, int seed)
        {
            return context.DoesPlaceholderContainRenderingByCondition(x => guids.Contains(x.RenderingItem.ID.Guid),
                dynamicPlaceholderName, seed);
        }

        public static bool DoesPlaceholderContainRenderingByCondition(this PageContext context, Func<Rendering, bool> condition, string dynamicPlaceholderName, int seed)
        {
            var dynamicKey =
                $"{dynamicPlaceholderName}-{Sitecore.Mvc.Presentation.RenderingContext.Current.Rendering.UniqueId.ToString("B").ToUpperInvariant()}-{seed}";

            return context.DoesPlaceholderContainRenderingByCondition(dynamicKey, condition);
        }

        public static bool DoesPlaceholderContainRenderingByCondition(this PageContext context, string placeholderKey,
            Func<Rendering, bool> condition)
        {
            var pageContext = context;
            var pageDefinition = pageContext.PageDefinition;
            return pageDefinition.Renderings.Any(x => x.Placeholder.IndexOf(placeholderKey, StringComparison.InvariantCulture) >= 0 && condition(x));
        }
    }
}