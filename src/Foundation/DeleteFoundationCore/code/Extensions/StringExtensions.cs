﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Glass.Mapper;
using Sitecore.Diagnostics;
using Sitecore.Pipelines.GetFileIcon.Processors;

namespace Delete.Foundation.DeleteFoundationCore.Extensions
{
    public static class StringExtensions
    {
        private static Regex regexToReplaceAllNonWordCharsInTheEnd = new Regex("([^\\w>]*)?$", RegexOptions.Compiled);

        private static Regex regexToReplaceAllNonWordCharsInTheBeginning = new Regex("^[^A-Za-z0-9_<]*", RegexOptions.Compiled);

        private const string MetadataTrimEnd = " ...";

        public static bool SafeEquals(this string obj, string value)
        {
            if ((obj.IsNullOrEmpty() && value.NotEmpty())
                || (obj.NotEmpty() && value.IsNullOrEmpty()))
            {
                return false;
            }

            return obj.Equals(value, StringComparison.InvariantCultureIgnoreCase);
        }

        public static bool Empty(this string value)
        {
            return string.IsNullOrEmpty(value);
        }

        public static bool IsNullOrEmpty(this string value)
        {
            return string.IsNullOrEmpty(value);
        }

        public static bool NotEmpty(this string value)
        {
            return !string.IsNullOrEmpty(value) && !string.IsNullOrWhiteSpace(value);
        }

        public static string OrDefault(this string value, string defaultValue)
        {
            return value.NotEmpty() ? value : defaultValue;
        }

        public static bool In(this string value, IEnumerable<string> targetValues)
        {
            if (targetValues == null)
            {
                throw new ArgumentNullException(nameof(targetValues));
            }

            return value.In(targetValues.ToArray());
        }

        public static bool In(this string value, params string[] array)
        {
            if (string.IsNullOrEmpty(value))
            {
                return false;
            }

            return array.Contains(value, StringComparer.OrdinalIgnoreCase);
        }

        public static string[] SafelySplit(this string value, string separator)
        {
            Guard.ArgumentNotEmpty(separator, nameof(separator));

            if (string.IsNullOrEmpty(value))
            {
                return Enumerable.Empty<string>().ToArray();
            }

            return value.Split(new[] { separator }, StringSplitOptions.RemoveEmptyEntries);
        }

        public static int ParseOrDefault(this string value, int defaultValue)
        {
            if (value.Empty())
            {
                return defaultValue;
            }

            return int.TryParse(value, out var parsedValue)
                ? parsedValue
                : defaultValue;
        }

        public static DateTime? ParseOrDefault(this string value, string format, DateTime? defaultValue = null)
        {
            if (value.Empty())
            {
                return defaultValue;
            }

            return DateTime.TryParseExact(value, format, null, DateTimeStyles.AssumeLocal, out var parsedValue)
                ? parsedValue
                : defaultValue;
        }

        public static decimal ParseOrDefault(this string value, decimal defaultValue)
        {
            if (value.Empty())
            {
                return defaultValue;
            }

            return decimal.TryParse(value, out var parsedValue)
                ? parsedValue
                : defaultValue;
        }

        public static double ParseOrDefault(this string value, double defaultValue)
        {
            if (value.Empty())
            {
                return defaultValue;
            }

            return double.TryParse(value, out var parsedValue)
                ? parsedValue
                : defaultValue;
        }

        public static double ParseInvariantOrDefault(this string value, double defaultValue)
        {
            if (value.Empty())
            {
                return defaultValue;
            }

            return double.TryParse(value.Trim(), NumberStyles.Any, new NumberFormatInfo(), out var resultDouble)
                       ? resultDouble
                       : defaultValue;
        }

        public static string ToSearchableGuid(this string value)
        {
            if (value.NotEmpty())
            {
                var regex = new Regex(Constants.Constants.Sitecore.GuidReplacementRegex);
                return regex.Replace(value, string.Empty).ToLowerInvariant();
            }

            return null;
        }

        public static Guid ParseOrDefault(this string value, Guid defaultValue)
        {
            if (value.Empty())
            {
                return defaultValue;
            }

            return Guid.TryParse(value, out var parsedValue)
                ? parsedValue
                : defaultValue;
        }

        public static string TruncateToWord(this string text, int maxLength, string appendIfCut = "...")
        {
            if (text.Empty() || text.Length <= maxLength)
            {
                return text;
            }

            if (appendIfCut.Empty())
            {
                appendIfCut = string.Empty;
            }

            var result = text.Substring(0, maxLength - appendIfCut.Length);

            var lastSpaceIndex = result.LastIndexOf(' ');

            if (lastSpaceIndex > 0)
            {
                result = result.Substring(0, lastSpaceIndex);
            }

            return result + appendIfCut;
        }

        public static string Truncate(this string text, int maxLength)
        {
            if (text.Empty() || text.Length <= maxLength)
            {
                return text;
            }

            return text.Substring(0, maxLength);
        }

        public static string EnsureEndsWith(this string text, string end)
        {
            if (text.IsNullOrEmpty() || text.EndsWith(end))
            {
                return text;
            }

            return $"{text}{end}";
        }

        public static IEnumerable<string> SplitStringByComma(this string value)
        {
            return value.SafelySplit(",")
                .Select(x => x.HasValue() ? x.Trim() : string.Empty)
                .ToList();
        }

        public static List<Guid> ToGuidList(this string value, string separator = ",")
        {
            return value.SafelySplit(separator)
                .Select(x => x.HasValue() ? x.Trim() : string.Empty)
                .Where(x => Guid.TryParse(x, out _))
                .Select(Guid.Parse)
                .ToList();
        }

        public static string StripHtml(this string value)
        {
            return Regex.Replace(value, "<.*?>|&.*?;", string.Empty);
        }

        public static string GetFileNameFromUrl(this string url)
        {
            Uri uri;
            if (!Uri.TryCreate(url, UriKind.Absolute, out uri))
            {
                var baseUri = new Uri(Sitecore.Sites.SiteContext.Current.HostName);
                uri = new Uri(baseUri, url);
            }

            // for some Markets image name comes as a URL parameter and not as path (which is the same for all images in this case)
            string suffix = string.Empty;
            if (!String.IsNullOrEmpty(uri.Query)) 
            { 
                var parameters = HttpUtility.ParseQueryString(uri.Query);
                var suffixes = new List<string>();
                foreach (var key in parameters.AllKeys)
                {
                    var values = parameters.GetValues(key);
                    suffixes.AddRange(values.Select(s => Path.GetFileNameWithoutExtension(s)));
                }

                suffix = String.Join("", suffixes);
            }

            return $"{Path.GetFileNameWithoutExtension(uri.LocalPath)}{suffix}";
        }

        /// <summary>
        /// Converts string as an URI and verifies it's proper http(s), returns null otherwise
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static Uri AsHttpUrl(this string url)
        {
            Uri itemUri;
            var itemUriIsValid = Uri.TryCreate(url, UriKind.Absolute, out itemUri) &&
                                 (itemUri.Scheme == Uri.UriSchemeHttp || itemUri.Scheme == Uri.UriSchemeHttps);
            if (itemUri == null || !itemUriIsValid)
            {
                return null;
            }

            return itemUri;
        }

        public static string ToGeneralUrl(this string url)
        {
            if (url.IsNullOrEmpty())
            {
                return string.Empty;
            }

            return Regex.Replace(url, "^http(s)?:", "");
        }

        public static string WrapToEllipsis(this string content)
        {
            if (content.IsNullOrEmpty())
            {
                return string.Empty;
            }

            if (!content.EndsWith("."))
            {
                content = $"{regexToReplaceAllNonWordCharsInTheEnd.Replace(content, "")}...";
            }

            if (content.Length > 0 && !char.IsUpper(content[0]))
            {
                content = $"...{regexToReplaceAllNonWordCharsInTheBeginning.Replace(content, "")}";
            }

            if (content.Length > 0 && char.IsLower(content[0]))
            {
                content = $"...{content}";
            }

            return content;
        }

        public static string TrimForMetadata(this string metadata, int maxNumberOfCharacter)
        {
            if (metadata == null || metadata.Length <= maxNumberOfCharacter || maxNumberOfCharacter - MetadataTrimEnd.Length <= 0)
            {
                return metadata;
            }

            var count = maxNumberOfCharacter - MetadataTrimEnd.Length;
            var index = metadata.LastIndexOf(' ', count, count);

            if (index > 0)
            {
                metadata = $"{metadata.Substring(0, index)}{MetadataTrimEnd}";
            }

            return metadata;
        }
    }
}