﻿namespace Delete.Foundation.DeleteFoundationCore.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Web;
    using System.Web.Mvc;

    public static class TagBuilderExtensions
    {
        public static MvcHtmlString RenderTagAttributes(this TagBuilder tagBuilder)
        {
            var sb = new StringBuilder();
            foreach (var attribute in tagBuilder.Attributes)
            {
                string key = attribute.Key;
                if (!string.Equals(key, "id", StringComparison.Ordinal) || !string.IsNullOrEmpty(attribute.Value))
                {
                    string str = HttpUtility.HtmlAttributeEncode(attribute.Value);
                    sb.Append(' ').Append(key).Append("=\"").Append(str).Append('"');
                }
            }

            return new MvcHtmlString(sb.ToString());
        }
    }
}