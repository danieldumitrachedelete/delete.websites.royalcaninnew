﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Glass.Mapper.Sc.Fields;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Sitecore.DependencyInjection;
using Sitecore.Resources.Media;
using File = System.IO.File;

namespace Delete.Foundation.DeleteFoundationCore.Extensions
{
    using System;
    using System.Text;

    using Models;

    using Utils;

    public static partial class HtmlHelperExtensions
    {
        private const string DistPath = "/dist";

        private const string ManifestJsonPath = DistPath + "/manifest.json";

        private const string CurrentContextCustomDimensionsKey = "CurrentContextCustomDimensions";

        private const string CurrentContextIsHomePageKey = "CurrentContextIsHomePageKey";

        private const string CurrentContextPageValuesKey = "CurrentContextPageValuesKey";
        private const string CurrentContextPageTitleKey = "CurrentContextPageTitleKey";
        private const string CurrentContextItemNameKey = "CurrentContextItemNameKey";

        private static readonly string RegisteredTagKey = "TagAlreadyRegistered_{0}";

        private static readonly IExternalImageProcessor ImageProcessor = ServiceLocator.ServiceProvider.GetService<IExternalImageProcessor>();

        public const string CurrentContextStylesKey = "CurrentContextStylesKey";

        public const string CurrentContextScriptsKey = "CurrentContextScriptsKey";

        public const string CurrentContextSvgFilesKey = "CurrentContextSvgFilesKey";

        public const string ComponentAssetPattern = "<script type=\"{0}/component\">{1}</script>";

        public const string SvgComponentType = "svg";

        public const string JavascriptComponentType = "javascript";

        public const string CssComponentType = "css";

        public static bool IsHomePage(this HtmlHelper htmlHelper)
        {
            return htmlHelper.IsKeyRegistered(CurrentContextIsHomePageKey);
        }

        public static void SetHomePageFlag(this HtmlHelper htmlHelper, bool value)
        {
            if (!value)
            {
                return;
            }

            htmlHelper.RegisterKey(CurrentContextIsHomePageKey);
        }

        public static string GetPageTitle(this HtmlHelper htmlHelper)
        {
            var pageValues = htmlHelper.GetPageValuesDictionary();

            return pageValues.TryGetValue(CurrentContextPageTitleKey, out var pageTitle) ? pageTitle : string.Empty;
        }

        public static void SetPageTitle(this HtmlHelper htmlHelper, string value)
        {
            htmlHelper.SetPageValue(CurrentContextPageTitleKey, value);
        }

        public static string GetItemName(this HtmlHelper htmlHelper)
        {
            var pageValues = htmlHelper.GetPageValuesDictionary();

            return pageValues.TryGetValue(CurrentContextItemNameKey, out var s) ? s : string.Empty;
        }

        public static void SetItemName(this HtmlHelper htmlHelper, string value)
        {
            htmlHelper.SetPageValue(CurrentContextItemNameKey, value);
        }

        private static void SetPageValue(this HtmlHelper htmlHelper, string key, string value)
        {
            htmlHelper.AddCustomContextDataDictionaryEntry(CurrentContextPageValuesKey, key, value);
        }

        private static IDictionary<string, string> GetPageValuesDictionary(this HtmlHelper htmlHelper)
        {
            return htmlHelper.GetCustomContextDataDictionary<string, string>(CurrentContextPageValuesKey);
        }

        public static bool IsKeyRegistered(this HtmlHelper htmlHelper, string keyName)
        {
            bool.TryParse(htmlHelper.ViewContext.HttpContext.Items[keyName]?.ToString() ?? string.Empty, out var keyRegistered);
            return keyRegistered;
        }

        public static void RegisterKey(this HtmlHelper htmlHelper, string keyName)
        {
            htmlHelper.ViewContext.HttpContext.Items[keyName] = true;
        }

        public static IList<StaticAsset> GetRegisteredStyles(this HtmlHelper htmlHelper)
        {
            return htmlHelper.GetRegisteredAssets(CurrentContextStylesKey);
        }

        public static IList<StaticAsset> GetRegisteredScripts(this HtmlHelper htmlHelper)
        {
            return htmlHelper.GetRegisteredAssets(CurrentContextScriptsKey);
        }

        public static string RegisterStyles(this HtmlHelper htmlHelper, string pathToFile, bool isDeferred = false)
        {
            return htmlHelper.RegisterAssets(CurrentContextStylesKey, pathToFile, isDeferred);
        }

        public static string RegisterScripts(this HtmlHelper htmlHelper, string pathToFile, bool isDeferred = false)
        {
            return htmlHelper.RegisterAssets(CurrentContextScriptsKey, pathToFile, isDeferred);
        }

        public static string RegisterSvg(this HtmlHelper htmlHelper, string pathToFile)
        {
            return htmlHelper.RegisterAssets(CurrentContextSvgFilesKey, pathToFile);
        }

        public static MvcHtmlString RegisterStyleEntryPoint(this HtmlHelper htmlHelper, string entryPoint)
        {
            return htmlHelper.RegisterStyleEntryPoints(new[] { entryPoint });
        }

        public static MvcHtmlString RegisterScriptEntryPoint(this HtmlHelper htmlHelper, string entryPoint)
        {
            return htmlHelper.RegisterScriptEntryPoints(new[] { entryPoint });
        }

        public static MvcHtmlString RegisterSvgEntryPoint(this HtmlHelper htmlHelper, string entryPoint)
        {
            return htmlHelper.RegisterSvgEntryPoints(new[] { entryPoint });
        }

        public static MvcHtmlString RegisterStyleEntryPoints(this HtmlHelper htmlHelper, IEnumerable<string> entryPoints)
        {
            return MvcHtmlString.Create(string.Format(ComponentAssetPattern, CssComponentType, htmlHelper.RegisterEntryPoints(entryPoints)));
        }

        public static MvcHtmlString RegisterScriptEntryPoints(this HtmlHelper htmlHelper, IEnumerable<string> entryPoints)
        {
            return MvcHtmlString.Create(string.Format(ComponentAssetPattern, JavascriptComponentType, htmlHelper.RegisterEntryPoints(entryPoints)));
        }

        public static MvcHtmlString RegisterSvgEntryPoints(this HtmlHelper htmlHelper, IEnumerable<string> entryPoints)
        {
            return MvcHtmlString.Create(string.Format(ComponentAssetPattern, SvgComponentType, htmlHelper.RegisterEntryPoints(entryPoints)));
        }

        public static MvcHtmlString ReadFromManifestJson(this HtmlHelper htmlHelper, string attributes = "", bool isLink = true, bool isScript = false, params string[] keys)
        {
            var pattern = string.Empty;
            if (isLink)
            {
                pattern = "<link rel=\"stylesheet\" href=\"{0}\" {1}>";
            }

            if (isScript)
            {
                pattern = "<script src=\"{0}\" {1} defer></script>";
            }

            var files = ResolveFilePath(keys);
            var linksBuilder = new StringBuilder();
            foreach (var file in files)
            {
                linksBuilder.AppendFormat(pattern, file, attributes);
            }

            return new MvcHtmlString(linksBuilder.ToString());
        }

        public static MvcHtmlString PreloadFilesFromDirectory(this HtmlHelper htmlHelper, string folderPath, string mask, string type, bool addCrossOrigin = false)
        {
            // <link rel="preload" href="/assets/app/general/fonts/gothamblack-webfont.woff2" as="font">
            var serverFolderPath = HttpContext.Current.Server.MapPath(folderPath);

            if (string.IsNullOrEmpty(serverFolderPath) || !Directory.Exists(serverFolderPath))
            {
                return null;
            }

            var directoryInfo = new DirectoryInfo(serverFolderPath);
            var filesInDirectory = directoryInfo.GetFiles(mask);
            var linkPattern = "<link rel=\"preload\" href=\"{0}/{1}\" as=\"{2}\">";
            if (addCrossOrigin)
            {
                linkPattern = "<link rel=\"preload\" href=\"{0}/{1}\" as=\"{2}\" crossorigin>";
            }

            var linksBuilder = new StringBuilder();
            foreach (var file in filesInDirectory)
            {
                linksBuilder.AppendFormat(linkPattern, folderPath, file.Name, type);
            }

            return new MvcHtmlString(linksBuilder.ToString());
        }

        public static MvcHtmlString RenderCriticalCss(this HtmlHelper htmlHelper, string criticalCssFileMask, string attributeCustom = null)
        {
            var distServerPath = HttpContext.Current.Server.MapPath(DistPath);

            if (string.IsNullOrEmpty(distServerPath) || !Directory.Exists(distServerPath))
            {
                return null;
            }

            var directoryInfo = new DirectoryInfo(distServerPath);
            var filesInDirectory = directoryInfo.GetFiles(criticalCssFileMask);

            if (!filesInDirectory.Any())
            {
                return null;
            }

            var criticalCssPath = $"{distServerPath.EnsureEndsWith("/")}{filesInDirectory.First().Name}";
            var isCriticalExist = !StringExtensions.IsNullOrEmpty(distServerPath) && File.Exists(criticalCssPath);
            if (!isCriticalExist)
            {
                return null;
            }

            var criticalTagFormat = "<style id=\"criticalcss\" {1}>{0}</style>";
            var criticalCssContent = File.ReadAllText(criticalCssPath);
            var criticalCssLink = string.Format(criticalTagFormat, criticalCssContent, attributeCustom);
            return new MvcHtmlString(criticalCssLink);
        }

        public static string GetWebPath(this HtmlHelper htmlHelper, string manifestKey)
        {
            return ResolveFilePath(new[] { manifestKey }).FirstOrDefault();
        }

        public static string GetContent(this HtmlHelper htmlHelper, string webPath, string prefix = "")
        {
            if (string.IsNullOrEmpty(webPath)) return string.Empty;

            var distServerPath = HttpContext.Current.Server.MapPath(DistPath);
            var processedValue = webPath.Replace(DistPath.EnsureEndsWith("/"), string.Empty);
            var fullFilePath = $"{distServerPath}\\{prefix}{processedValue}";
            return File.Exists(fullFilePath) ? File.ReadAllText(fullFilePath) : string.Empty;
        }

        public static IDisposable UseTag(this HtmlHelper htmlHelper, string firstTag, string secondTag, string attributes)
        {
            if (htmlHelper == null)
            {
                return new DisposableHtmlWrapper(() => { }, () => { });
            }

            var key = GetTagKey(firstTag);
            var tag = firstTag;

            if (htmlHelper.IsKeyRegistered(key))
            {
                tag = secondTag;
            }

            htmlHelper.RegisterKey(GetTagKey(tag));

            return new DisposableHtmlWrapper(
                () => htmlHelper.ViewContext.Writer.Write($"<{tag} {attributes}>"),
                () => htmlHelper.ViewContext.Writer.Write($"</{tag}>"));
        }

        public static MvcHtmlString RenderJson(this HtmlHelper helper, object model, bool wrapSrcipt = true, string mimeType = "application/ld+json")
        {
            if (model == null)
            {
                return new MvcHtmlString(string.Empty);
            }

            var scriptTagBegin = mimeType.NotEmpty() ? $"<script type=\"{mimeType}\">" : "<script>";
            var stringBuilder = wrapSrcipt
                                    ? new StringBuilder(scriptTagBegin)
                                    : new StringBuilder();

            stringBuilder.Append(model);

            if (wrapSrcipt)
            {
                stringBuilder.Append("</script>");
            }

            return new MvcHtmlString(stringBuilder.ToString());
        }

        private static IEnumerable<string> ResolveFilePath(string[] manifestKeys)
        {
            if (manifestKeys == null || !manifestKeys.Any())
            {
                return new List<string>();
            }

            var serverManifestJsonFilePath = HttpContext.Current.Server.MapPath(ManifestJsonPath);
            var isManifestExist = !StringExtensions.IsNullOrEmpty(serverManifestJsonFilePath) && File.Exists(serverManifestJsonFilePath);
            if (!isManifestExist)
            {
                return manifestKeys.ToList();
            }

            var fileContent = File.ReadAllText(serverManifestJsonFilePath);
            dynamic parsedManifestJson = JsonConvert.DeserializeObject(fileContent);
            var result = new List<string>();
            foreach (var key in manifestKeys)
            {
                var temp = parsedManifestJson[key];
                result.Add(temp == null ? key : temp.ToString());
            }

            return result;
        }

        private static IList<StaticAsset> GetRegisteredAssets(this HtmlHelper htmlHelper, string key)
        {
            var dictionary = htmlHelper.ViewContext.HttpContext.Items[key] as Dictionary<string, StaticAsset>
                             ?? new Dictionary<string, StaticAsset>();

            return dictionary.Select(x =>
                {
                    var asset = x.Value;
                    asset.Path = htmlHelper.GetWebPath(asset.Path);
                    return x.Value;
                }).ToList();
        }

        private static string RegisterAssets(this HtmlHelper htmlHelper, string key, string pathToFile, bool isDeferred = false)
        {
            if (StringExtensions.IsNullOrEmpty(key) || StringExtensions.IsNullOrEmpty(pathToFile))
            {
                return string.Empty;
            }

            var dictionary = htmlHelper.ViewContext.HttpContext.Items[key] as Dictionary<string, StaticAsset>
                             ?? new Dictionary<string, StaticAsset>();

            if (dictionary.ContainsKey(pathToFile))
            {
                return string.Empty;
            }

            dictionary.Add(pathToFile, new StaticAsset { Path = pathToFile, IsDeferred = isDeferred });
            htmlHelper.ViewContext.HttpContext.Items[key] = dictionary;
            return string.Empty;
        }

        private static string GetTagKey(string tag)
        {
            return string.Format(RegisteredTagKey, tag).ToLower();
        }

        private static string AddCustomContextDataDictionaryEntry<TKey, TValue>(this HtmlHelper htmlHelper, string contextKey, TKey internalKey, TValue valueToAdd, bool overwriteExisting = false)
        {
            if (StringExtensions.IsNullOrEmpty(contextKey) || internalKey.Equals(default(TKey)))
            {
                return string.Empty;
            }

            var dictionary = htmlHelper.ViewContext.HttpContext.Items[contextKey] as Dictionary<TKey, TValue>
                             ?? new Dictionary<TKey, TValue>();

            if (dictionary.ContainsKey(internalKey))
            {
                if (overwriteExisting)
                {
                    dictionary[internalKey] = valueToAdd;
                }
                else
                {
                    return string.Empty;
                }
            }
            else
            {
                dictionary.Add(internalKey, valueToAdd);
            }

            htmlHelper.ViewContext.HttpContext.Items[contextKey] = dictionary;
            return string.Empty;
        }

        private static IDictionary<TKey, TValue> GetCustomContextDataDictionary<TKey, TValue>(
            this HtmlHelper htmlHelper,
            string contextKey)
        {
            var dictionary = htmlHelper.ViewContext.HttpContext.Items[contextKey] as Dictionary<TKey, TValue>
                             ?? new Dictionary<TKey, TValue>();

            return dictionary;
        }
        
        private static string RegisterEntryPoints(this HtmlHelper htmlHelper, IEnumerable<string> entryPoints)
        {
            if (entryPoints == null || !entryPoints.Any()) return string.Empty;

            return JsonConvert.SerializeObject(entryPoints.Select(x => new { Path = x }));
        }
    }
}
