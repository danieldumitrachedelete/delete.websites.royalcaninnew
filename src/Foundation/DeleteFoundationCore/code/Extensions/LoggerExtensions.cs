﻿using System;
using System.Diagnostics;

namespace Delete.Foundation.DeleteFoundationCore.Extensions
{
    public static class LoggerExtensions
    {
        public static void LogMethodEnter(this object owner, string loggerName = null)
        {
            Guard.ArgumentNotNull(owner, nameof(owner));

            var methodName = GetMethodName(owner, true, 2);

            owner.LogDebug(string.Format(Constants.LogMessages.MethodIn, methodName), loggerName, false);
        }

        public static void LogMethodOut(this object owner, string loggerName = null)
        {
            Guard.ArgumentNotNull(owner, nameof(owner));

            var methodName = GetMethodName(owner, true, 2);

            owner.LogDebug(string.Format(Constants.LogMessages.MethodOut, methodName), loggerName, false);
        }

        public static string GetMethodName(this object owner, bool includeOwnerType = false, int skipFrames = 1)
        {
            var frame = new StackFrame(skipFrames);
            var method = frame.GetMethod();
            var methodName = method.Name;
            return includeOwnerType
                ? $"{owner.GetType().GetFriendlyName()}.{methodName}"
                : methodName;
        }

        public static void LogStart(this object owner)
        {
            Guard.ArgumentNotNull(owner, nameof(owner));

            owner.LogInfo(Constants.LogMessages.LogStart);
        }

        public static void LogFinish(this object owner)
        {
            Guard.ArgumentNotNull(owner, nameof(owner));

            owner.LogInfo(Constants.LogMessages.LogEnd);
        }

        public static void LogInfo(this object owner, string message, string loggerName = null)
        {
            Guard.ArgumentNotNull(owner, nameof(owner));
            Guard.ArgumentNotNull(message, nameof(message));

            message = $"[{GetMethodName(owner, true, 2)}]: {message}";

            if (loggerName.Empty())
            {
                Sitecore.Diagnostics.Log.Info(message, owner);
            }
            else
            {
                Sitecore.Diagnostics.Log.Info(message, loggerName);
            }
        }

        public static void LogDebug(this object owner, string message, string loggerName = null, bool includeMethodName = true)
        {
            Guard.ArgumentNotNull(owner, nameof(owner));
            Guard.ArgumentNotNull(message, nameof(message));

            if (includeMethodName)
            {
                message = $"[{GetMethodName(owner, true, 2)}]: {message}";
            }

            if (loggerName.Empty())
            {
                Sitecore.Diagnostics.Log.Debug(message, owner);
            }
            else
            {
                Sitecore.Diagnostics.Log.Debug(message, loggerName);
            }
        }
        public static void LogWarn(this object owner, string message, string loggerName = null)
        {
            Guard.ArgumentNotNull(owner, nameof(owner));
            Guard.ArgumentNotNull(message, nameof(message));

            message = $"[{GetMethodName(owner, true, 2)}]: {message}";

            Sitecore.Diagnostics.Log.Warn(message, loggerName.Empty() ? owner : loggerName);
        }

        public static void LogWarn(this object owner, string message, Exception exception, string loggerName = null)
        {
            Guard.ArgumentNotNull(owner, nameof(owner));
            Guard.ArgumentNotNull(message, nameof(message));

            message = $"[{GetMethodName(owner, true, 2)}]: {message}";

            Sitecore.Diagnostics.Log.Warn(message, exception, loggerName.Empty() ? owner : loggerName);
        }

        public static void LogError(this object owner, string message, Exception exception, string loggerName = null)
        {
            Guard.ArgumentNotNull(owner, nameof(owner));
            Guard.ArgumentNotNull(message, nameof(message));
            Guard.ArgumentNotNull(exception, nameof(exception));

            message = $"[{GetMethodName(owner, true, 2)}]: {message}";

            if (loggerName.Empty())
            {
                Sitecore.Diagnostics.Log.Error(message, exception, owner);
            }
            else
            {
                Sitecore.Diagnostics.Log.Error(message, exception, loggerName);
            }
        }

        public static void LogError(this object owner, string message, string loggerName = null)
        {
            Guard.ArgumentNotNull(owner, nameof(owner));
            Guard.ArgumentNotNull(message, nameof(message));

            message = $"[{GetMethodName(owner, true, 2)}]: {message}";

            if (loggerName.NotEmpty())
            {
                Sitecore.Diagnostics.Log.Error(message, null, loggerName);
            }
            else
            {
                Sitecore.Diagnostics.Log.Error(message, null, owner);
            }
        }

        public static void LogError(this object owner, Exception exception, bool logStackTrace = false)
        {
            Guard.ArgumentNotNull(owner, nameof(owner));
            Guard.ArgumentNotNull(exception, nameof(exception));

            var message = logStackTrace
                ? $"[{GetMethodName(owner, true, 2)}]: {exception.Message}\r\nStack trace:\r\n{exception.StackTrace}"
                : $"[{ GetMethodName(owner, true, 2)}]: {exception.Message}";

            Sitecore.Diagnostics.Log.Error(message, owner);
        }
    }
}