﻿using System;

namespace Delete.Foundation.DeleteFoundationCore.Extensions
{
    public static class GuidExtensions
    {
        public static string ToDigitsString(this Guid guid)
        {
            return guid.ToString("N").ToLowerInvariant();
        }
    }
}