﻿using System;
using Delete.Foundation.DeleteFoundationCore.Models;

namespace Delete.Foundation.DeleteFoundationCore.Extensions
{
    public static class ImageExtensions
    {
        public static bool HasExternalSource(this ExtendedImage image)
        {
            if (image == null)
            {
                return false;
            }

            return image.Src.NotEmpty() && image.MediaId == Guid.Empty;
        }
    }
}