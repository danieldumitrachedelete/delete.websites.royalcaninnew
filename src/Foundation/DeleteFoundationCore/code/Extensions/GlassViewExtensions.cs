﻿using System;
using System.Linq.Expressions;
using System.Web;
using Glass.Mapper.Sc.Fields;
using Glass.Mapper.Sc.Web.Mvc;

namespace Delete.Foundation.DeleteFoundationCore.Extensions
{
    public static class GlassViewExtensions
    {
        public static HtmlString RenderLazyImage<TModel>(this GlassView<TModel> view, Expression<Func<TModel, object>> field, string srcSet) where TModel : class
        {
            return view.RenderLazyImage(view.Model, field, srcSet);
        }

        public static HtmlString RenderLazyImage<VModel, TModel>(this GlassView<VModel> view, TModel model, Expression<Func<TModel, object>> field, string srcSet) 
            where TModel : class
            where VModel: class
        {
            if (view.IsInEditingMode)
            {
                return view.RenderImage(model, field, isEditable: true);
            }
            else
            {
                var alt = (field.Compile()(model) as Image)?.Alt;
                return new HtmlString($"<img data-srcset=\"{srcSet}\" class=\"lazyload\" data-sizes=\"auto\" alt=\"{alt}\"/>");
            }
        }
    }
}