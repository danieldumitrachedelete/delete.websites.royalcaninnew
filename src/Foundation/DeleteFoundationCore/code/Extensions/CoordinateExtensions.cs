﻿using System.Device.Location;
using Sitecore.ContentSearch.Data;

namespace Delete.Foundation.DeleteFoundationCore.Extensions
{
    public static class CoordinateExtensions
    {
        public static double DistanceTo(this Coordinate from, Coordinate to)
        {
            if (from == null || to == null) return 0;

            var distanceInMeters = from.ToGeoCoordinate().GetDistanceTo(to.ToGeoCoordinate());
            var distanceInKilometers = distanceInMeters / 1000;
            return distanceInKilometers;
        }

        public static GeoCoordinate ToGeoCoordinate(this Coordinate coordinate)
        {
            return new GeoCoordinate(coordinate.Latitude, coordinate.Longitude);
        }
    }
}