﻿using System.Web;
using System.Web.Mvc;

namespace Delete.Foundation.DeleteFoundationCore.Extensions
{
    using System;
    using System.Net.Http.Headers;
    public class CacheControl : ActionFilterAttribute
    {
        public int MaxAge { get; set; }
        public int SharedMaxAge { get; set; }

        public CacheControl()
        {
            MaxAge = 3600;
            SharedMaxAge = 3600;
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            System.Web.HttpContext.Current.Response.Headers.Set("Cache-Control", "public, max-age=31536000, s-maxage=0");
        }

        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            HttpCachePolicyBase cache = filterContext.HttpContext.Response.Cache;
            cache.SetCacheability(HttpCacheability.Public);
            cache.SetMaxAge(TimeSpan.FromSeconds(MaxAge));
            cache.AppendCacheExtension($"s-maxage={SharedMaxAge}");
            base.OnResultExecuted(filterContext);
        }
    }
}