﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Delete.Foundation.DeleteFoundationCore.Interfaces;

namespace Delete.Foundation.DeleteFoundationCore.Extensions
{
    public static class CacheManagerExtensions
    {
        public static TResult CacheResults<TResult>(this ICacheManager cacheManager, Func<TResult> dataGetter, string cacheKey, TimeSpan? expirationTime) where TResult : class
        {
            Guard.ArgumentNotNull(cacheManager, nameof(cacheManager));
            Guard.ArgumentNotNull(dataGetter, nameof(dataGetter));
            Guard.ArgumentNotEmpty(cacheKey, nameof(cacheKey));

            var results = cacheManager.Get<TResult>(cacheKey);
            if (results == null)
            {
                results = dataGetter();
                if (results != null)
                {
                    if (expirationTime.HasValue)
                    {
                        cacheManager.Insert(results, cacheKey, expirationTime.Value);
                    }
                    else
                    {
                        cacheManager.Insert(results, cacheKey);
                    }
                }
            }
            return results;
        }

        public static TResult CacheResults<TResult>(this ICacheManager cacheManager, Func<TResult> dataGetter, string cacheKey) where TResult : class
        {
            return CacheResults(cacheManager, dataGetter, cacheKey, null);
        }

        public static IEnumerable<TResult> CacheResults<TResult>(this ICacheManager cacheManager, Func<IEnumerable<TResult>> dataGetter, string cacheKey, TimeSpan? expirationTime) where TResult : class
        {
            Guard.ArgumentNotNull(cacheManager, nameof(cacheManager));
            Guard.ArgumentNotNull(dataGetter, nameof(dataGetter));
            Guard.ArgumentNotEmpty(cacheKey, nameof(cacheKey));

            var results = cacheManager.Get<ICollection<TResult>>(cacheKey);
            if (results == null)
            {
                results = dataGetter().ToList();
                if (results.Any())
                {
                    if (expirationTime.HasValue)
                    {
                        cacheManager.Insert(results, cacheKey, expirationTime.Value);
                    }
                    else
                    {
                        cacheManager.Insert(results, cacheKey);
                    }
                }
            }

            return results;
        }

        public static IEnumerable<TResult> CacheResults<TResult>(this ICacheManager cacheManager, Func<IEnumerable<TResult>> dataGetter, string cacheKey) where TResult : class
        {
            return CacheResults(cacheManager, dataGetter, cacheKey, null);
        }
    }
}