﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using JetBrains.Annotations;
using Sitecore.Resources.Media;
using Sitecore.Shell.Applications.Layouts.GridDesigner.Commands;

namespace Delete.Foundation.DeleteFoundationCore.Extensions
{
    using Models;

    public static partial class HtmlHelperExtensions
    {
        private static readonly List<int> DefaultSrcSetSizes = new List<int> { 320, 360, 640, 720, 960, 1280, 1440 };

        public static string GetCroppedMediaUrl(this HtmlHelper helper, ExtendedImage image, int width, int height)
        {
            return helper.GetCroppedMediaUrl(image?.Src, width, height, image.HasExternalSource());
        }

        /// <summary>
        /// Don't use if the image fields itself is available
        /// as this method doesn't work correctly with WeShare images
        /// </summary>
        public static string GetCroppedMediaUrl(this HtmlHelper helper, string url, int width, int height, bool externalSource = false)
        {
            if (string.IsNullOrWhiteSpace(url))
            {
                return PlaceholderImageUrl(width, height);
            }

            if (externalSource)
            {
                return ImageProcessor.Crop(url, width, height);
            }

            var separator = url.Contains("?") ? "&" : "?";
            var mediaUrl = $"{url}{separator}w={width}&h={height}&crop=1";
            return HashingUtils.ProtectAssetUrl(mediaUrl);
        }

        public static string GetCroppedSrcSet(this HtmlHelper helper, ExtendedImage image, params int[][] sizes)
        {
            return helper.GetCroppedSrcSet(image?.Src, image.HasExternalSource(), sizes);
        }

        /// <summary>
        /// Don't use if the image fields itself is available
        /// as this method doesn't work correctly with WeShare images
        /// </summary>
        public static string GetCroppedSrcSet(this HtmlHelper helper, string url, params int[][] sizes)
        {
            return helper.GetCroppedSrcSet(url, false, sizes);
        }

        private static string GetCroppedSrcSet(this HtmlHelper helper, string url, bool externalSource, params int[][] sizes)
        {
            var srcSetFormat = "{0} {1}w";
            var srcSetList = new List<string>();
            foreach (var size in sizes)
            {
                var croppedUrl = GetCroppedMediaUrl(helper, url, size[0], size[1], externalSource);
                srcSetList.Add(string.Format(srcSetFormat, croppedUrl, size[0]));
            }

            return string.Join(",", srcSetList);
        }


        public static string GetResizedMediaUrl(this HtmlHelper helper, ExtendedImage image, int maxWidth)
        {
            return helper.GetResizedMediaUrl(image?.Src, maxWidth, image?.AdditionalParameters, image.HasExternalSource());
        }

        /// <summary>
        /// Don't use if the image fields itself is available
        /// as this method doesn't work correctly with WeShare images
        /// </summary>
        public static string GetResizedMediaUrl(this HtmlHelper helper, string url, int maxWidth, string additionalParameters, bool externalSource = false)
        {
            if (string.IsNullOrWhiteSpace(url))
            {
                return null;
            }

            if (externalSource)
            {
                return ImageProcessor.Resize(url, maxWidth, additionalParameters);
            }

            var separator = url.Contains("?") ? "&" : "?";
            var mediaUrl = $"{url}{separator}mw={maxWidth}";
            return HashingUtils.ProtectAssetUrl(mediaUrl);
        }

        

        public static string GetResizedSrcSet(this HtmlHelper helper, ExtendedImage image, params int[] sizes)
        {
            
            return helper.GetResizedSrcSet(image?.Src, image?.AdditionalParameters, image.HasExternalSource(), sizes);
        }

        public static string GetResizedSrcSet(this HtmlHelper helper, ExtendedImage image, ref int width, ref int height, params int[] sizes)
        {
            ImageProcessor.GetSize(ref width, ref height, image?.AdditionalParameters);
            return helper.GetResizedSrcSet(image?.Src, image?.AdditionalParameters, image.HasExternalSource(), sizes);
        }

        /// <summary>
        /// Don't use if the image fields itself is available
        /// as this method doesn't work correctly with WeShare images
        /// </summary>
        public static string GetResizedSrcSet(this HtmlHelper helper, string url, ref int width, ref int height, params int[] sizes)
        {
            
            return helper.GetResizedSrcSet(url, null, false, sizes);
        }

        private static string GetResizedSrcSet(this HtmlHelper helper, string url, string additionalParameters, bool externalSource, params int[] sizes)
        {
            var srcSetFormat = "{0} {1}w";
            var sizesList = new List<int>();
            sizesList.AddRange(DefaultSrcSetSizes);
            if (sizes != null)
            {
                sizesList.AddRange(sizes);
            }

            sizesList = sizesList.OrderBy(x => x).ToList();

            var srcSetList = new List<string>();
            foreach (var size in sizesList)
            {
                var resizedUrl = GetResizedMediaUrl(helper, url, size, additionalParameters, externalSource);
                srcSetList.Add(string.Format(srcSetFormat, resizedUrl, size));
            }

            return string.Join(",", srcSetList);
        }
        
        public static string PlaceholderImageUrl(int width, int height)
        {
            return $"//via.placeholder.com/{width}x{height}";
        }

        public static string GetCroppedByRatioMediaUrl(this HtmlHelper helper, ExtendedImage image, int xRatio, int yRatio, int width)
        {
            return helper.GetCroppedByRatioMediaUrl(image?.Src, xRatio, yRatio, width, image?.AdditionalParameters, image.HasExternalSource());
        }

        /// <summary>
        /// Don't use if the image fields itself is available
        /// as this method doesn't work correctly with WeShare images
        /// </summary>
        public static string GetCroppedByRatioMediaUrl(this HtmlHelper helper, string url, int xRatio, int yRatio, int width, string additionalParameters, bool externalSource = false)
        {
            var height = (int)((double)width * yRatio / xRatio);

            if (string.IsNullOrWhiteSpace(url))
            {
                return PlaceholderImageUrl(width, height);
            }

            if (externalSource)
            {
                if (additionalParameters.NotEmpty())
                {
                    return ImageProcessor.Resize(url, width, additionalParameters);
                }

                return ImageProcessor.Crop(url, width, height);
            }

            var separator = url.Contains("?") ? "&" : "?";
            var mediaUrl = $"{url}{separator}w={width}&h={height}&crop=1";
            return HashingUtils.ProtectAssetUrl(mediaUrl);
        }

        public static string GetCroppedByRatioSrcSet(this HtmlHelper helper, ExtendedImage image, int xRatio, int yRatio)
        {
            return helper.GetCroppedByRatioSrcSet(image?.Src, image?.AdditionalParameters, image.HasExternalSource(), xRatio, yRatio);
        }

        /// <summary>
        /// Don't use if the image fields itself is available
        /// as this method doesn't work correctly with WeShare images
        /// </summary>
        public static string GetCroppedByRatioSrcSet(this HtmlHelper helper, string url, int xRatio, int yRatio)
        {
            return helper.GetCroppedByRatioSrcSet(url, null, false, xRatio, yRatio);
        }

        public static string GetCroppedByRatioSrcSet(this HtmlHelper helper, string url, string additionalParameters, bool externalSource, int xRatio, int yRatio, params int[] sizes)
        {
            var sizesList = new List<int>();
            sizesList.AddRange(DefaultSrcSetSizes);
            if (sizes != null)
            {
                sizesList.AddRange(sizes);
            }

            sizesList = sizesList.OrderBy(x => x).ToList();
            
            var srcSetFormat = "{0} {1}w";
            var srcSetList = new List<string>();
            foreach (var size in sizesList)
            {
                var croppedByRatioUrl = GetCroppedByRatioMediaUrl(helper, url, xRatio, yRatio, size, additionalParameters, externalSource);
                srcSetList.Add(string.Format(srcSetFormat, croppedByRatioUrl, size));
            }

            return string.Join(",", srcSetList);
        }

    }
}
