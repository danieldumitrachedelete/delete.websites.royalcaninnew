﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Delete.Foundation.DeleteFoundationCore.Extensions
{
    using System.Reflection;

    public static class ObjectExtensions
    {
        public static string ToJson(this object anObject, JsonSerializerSettings jsonSerializerSettings = null, bool ignoreDefaultValuesHandling = true, bool ignoreNullValueHandling = true)
        {
            if (jsonSerializerSettings == null)
            {
                jsonSerializerSettings = new JsonSerializerSettings
                {
                    DefaultValueHandling = ignoreDefaultValuesHandling ? DefaultValueHandling.Ignore : DefaultValueHandling.Include,
                    NullValueHandling = ignoreNullValueHandling ? NullValueHandling.Ignore : NullValueHandling.Include,
                    DateFormatString = "yyyy-MM-ddTHH:mm:ss",
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                };
            }

            var jsonString = JsonConvert.SerializeObject(anObject, jsonSerializerSettings);

            return jsonString;
        }

        public static T GetPrivateFieldValue<T>(this object source, string propertyName)
        {
            var bindingFlags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance;
            var field = source.GetType().GetField(propertyName, bindingFlags);
            return (T)field?.GetValue(source);
        }
    }
}