﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Delete.Foundation.DeleteFoundationCore.Models;

namespace Delete.Foundation.DeleteFoundationCore.ExpressionExtensions
{
    using Sitecore.ContentSearch.Linq.Utilities;

    public static class ExpressionExtensions
    {
        public static Expression<Func<T, bool>> BuildFilterGroupExpression<T>(
            IDictionary<string, IEnumerable<Guid>> filterGroups,
            Func<T, Guid, bool> filterFunction)
            where T : GlassBase
        {
            var expression = PredicateBuilder.True<T>();
            foreach (var filterGroup in filterGroups)
            {
                var currentExpression = PredicateBuilder.True<T>();

                foreach (var filter in filterGroup.Value)
                {
                    var filterValue = filter;
                    currentExpression = currentExpression.Or(x => filterFunction(x, filterValue));
                }

                expression = expression.And(currentExpression);
            }

            return expression;
        }

        public static Expression<Func<T, bool>> BuildFilterGroupExpression<T>(
            IEnumerable<IGrouping<string, IEnumerable<Guid>>> filterGroups,
            Func<T, Guid, bool> filterFunction)
            where T : GlassBase
        {
            var expression = Begin<T>(true);
            return (from filterGroup in filterGroups
                    let currentExpression = Begin<T>(true)
                    select filterGroup.Aggregate(
                        currentExpression,
                        (current1, filterGroupItems) => filterGroupItems.Aggregate(
                                    current1,
                                    (current, filterItem) => Or(current, x => filterFunction(x, filterItem)))))
                .Aggregate(expression, (current2, currentExpression) => And(current2, currentExpression));
        }

        public static Expression<Func<T, bool>> Begin<T>(bool value = false)
        {
            if (value)
                return parameter => true;

            return parameter => false;
        }

        public static string GetMethodInfo<T, P>(this Expression<Func<T, P>> action)
        {
            var expression = action.Body as MemberExpression;
            return expression?.Member.Name ?? string.Empty;
        }

        public static Expression<Func<T, bool>> And<T>(this Expression<Func<T, bool>> left, Expression<Func<T, bool>> right)
        {
            return CombineLambdas(left, right, ExpressionType.AndAlso);
        }

        public static Expression<Func<T, bool>> Or<T>(this Expression<Func<T, bool>> left, Expression<Func<T, bool>> right)
        {
            return CombineLambdas(left, right, ExpressionType.OrElse);
        }

        private static Expression<Func<T, bool>> CombineLambdas<T>(this Expression<Func<T, bool>> left, Expression<Func<T, bool>> right, ExpressionType expressionType)
        {
            if (IsExpressionBodyConstant(left))
                return (right);

            var p = left.Parameters[0];

            var visitor = new SubstituteParameterVisitor { Sub = { [right.Parameters[0]] = p } };

            var body = Expression.MakeBinary(expressionType, left.Body, visitor.Visit(right.Body));
            return Expression.Lambda<Func<T, bool>>(body, p);
        }

        private static bool IsExpressionBodyConstant<T>(Expression<Func<T, bool>> left)
        {
            return left.Body.NodeType == ExpressionType.Constant;
        }

        private class SubstituteParameterVisitor : ExpressionVisitor
        {
            public Dictionary<Expression, Expression> Sub = new Dictionary<Expression, Expression>();

            protected override Expression VisitParameter(ParameterExpression node)
            {
                return Sub.TryGetValue(node, out var newValue) ? newValue : node;
            }
        }
    }
}