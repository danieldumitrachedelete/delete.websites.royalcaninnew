﻿namespace Delete.Foundation.DeleteFoundationCore.Extensions
{
    using System;
    using System.IO;
    using System.Web.Mvc;

    public static class ControllerExtensions
    {
        public static HtmlHelper GetHtmlHelper(this Controller controller)
        {
            var viewContext = new ViewContext(controller.ControllerContext, new FakeView(), controller.ViewData, controller.TempData, TextWriter.Null);
            return new HtmlHelper(viewContext, new ViewPage());
        }

        public class FakeView : IView
        {
            public void Render(ViewContext viewContext, TextWriter writer)
            {
                // this view mustn't render anything
            }
        }
    }
}