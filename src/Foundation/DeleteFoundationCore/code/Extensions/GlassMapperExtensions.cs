﻿using System;
using System.Collections.Generic;
using Delete.Foundation.DeleteFoundationCore.Models;
using Glass.Mapper;
using Glass.Mapper.Sc;
using Glass.Mapper.Sc.Configuration;

namespace Delete.Foundation.DeleteFoundationCore.Extensions
{
    using Glass.Mapper.Sc.DataMappers;

    public static class GlassMapperExtensions
    {
        public static IEnumerable<TItem> MapResults<TItem>(this IEnumerable<TItem> results, ISitecoreContext sitecoreContext, bool inferType = false, bool isLazy = false) where TItem : GlassBase
        {
            Guard.ArgumentNotNull(sitecoreContext, nameof(sitecoreContext));

            if (results != null)
            {
                foreach (var result in results)
                {
                    yield return sitecoreContext.MapResult(result, inferType, isLazy);
                }
            }
        }

        public static TItem MapResult<TItem>(this ISitecoreContext sitecoreContext, TItem target, bool inferType, bool isLazy = false) where TItem : class
        {
            if (sitecoreContext == null)
            {
                throw new ArgumentNullException(nameof(sitecoreContext));
            }

            if (target == null)
            {
                throw new ArgumentNullException(nameof(target));
            }

            if (Sitecore.Context.Site == null)
            {
                Sitecore.Context.SetActiveSite("website");
            }

            var config = sitecoreContext.GlassContext.GetTypeConfiguration<SitecoreTypeConfiguration>(target, false, true);
            if (config == null)
            {
                throw new MapperException($"No configuration for type {typeof(TItem).Name}. Load configuration using Attribute or Fluent configuration.");
            }

            var item = config.ResolveItem(target, sitecoreContext.Database);
            if (item == null)
            {
                return target;
            }

            var creationContext = new SitecoreTypeCreationContext();
            creationContext.TemplateId = item.TemplateID;
            creationContext.SitecoreService = sitecoreContext;
            creationContext.RequestedType = typeof(TItem);
            creationContext.ConstructorParameters = new object[0];
            creationContext.Item = item;
            creationContext.InferType = inferType;
            creationContext.IsLazy = isLazy;
            creationContext.Parameters = new Dictionary<string, object>();
            var result = sitecoreContext.InstantiateObject(creationContext) as TItem;
            config.MapPropertiesToObject(result, sitecoreContext, creationContext);
            return result;

        }
    }
}