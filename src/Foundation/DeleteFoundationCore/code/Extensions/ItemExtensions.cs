﻿using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;

namespace Delete.Foundation.DeleteFoundationCore.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public static class ItemExtensions
    {
        public static bool IsDerived([NotNull] this Item item, [NotNull] ID templateId)
        {
            var itemTemplate = TemplateManager.GetTemplate(item);

            return itemTemplate != null && itemTemplate.IsDerived(templateId);
        }

        public static IEnumerable<Guid> GetSitecoreIdsFromFieldValues(this Item item, string fieldName)
        {
            var fieldValues = item.Fields[fieldName]?.Value;
            return fieldValues.Empty()
                       ? new List<Guid>()
                       : fieldValues.ToGuidList("|");
        }

        public static IEnumerable<string> GetFieldValues(this Item item, string fieldName)
        {
            return item.GetSitecoreIdsFromFieldValues(fieldName).Select(x => x.ToDigitsString());
        }
    }
}