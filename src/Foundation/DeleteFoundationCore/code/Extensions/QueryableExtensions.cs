﻿using System;
using System.Linq;
using Delete.Foundation.DeleteFoundationCore.Index;
using Sitecore.ContentSearch.Linq;
using Sitecore.ContentSearch.SolrProvider;

namespace Delete.Foundation.DeleteFoundationCore.Extensions
{
    using System.Reflection;

    using Sitecore.ContentSearch.Linq.Parsing;
    using Sitecore.ContentSearch.Linq.Solr;
    using Sitecore.ContentSearch.SolrProvider.SolrNetIntegration;
    using Sitecore.Diagnostics;

    using SolrNet;
    using SolrNet.Commands.Parameters;

    public static class QueryableExtensions
    {
        public static IQueryable<TItem> PaginateQuery<TItem>(this IQueryable<TItem> query, int pageIndex, int pageSize)
        {
            if (pageIndex > 0)
            {
                query = query.Skip(pageIndex * pageSize);
            }

            return query.Take(pageSize);
        }

        public static SolrQueryResults<T> GetResultsWithHighlights<T>(this IQueryable<T> source, string[] fieldNames, int surroundingcharacters = 20, string htmltag = "b")
        {
            Guard.ArgumentNotNull(source, nameof(source));

            var queryable = source as GenericQueryable<T, SolrCompositeQuery>;
            Guard.ArgumentNotNull(queryable, nameof(queryable));

            var expression = source.Expression;

            // Build a native query 
            var intermidQuery = (SolrCompositeQuery)queryable.GetType().InvokeMember(
                "GetQuery",
                BindingFlags.InvokeMethod | BindingFlags.Instance | BindingFlags.NonPublic,
                Type.DefaultBinder,
                queryable,
                new object[] { expression });

            var linqIndex = (LinqToSolrIndex<T>)queryable.GetType().InvokeMember(
                "Index",
                BindingFlags.GetProperty | BindingFlags.Instance | BindingFlags.NonPublic,
                Type.DefaultBinder,
                queryable,
                null);

            var searchContext = (SolrSearchContext)linqIndex.GetType().InvokeMember(
                "context",
                BindingFlags.GetField | BindingFlags.Instance | BindingFlags.NonPublic,
                Type.DefaultBinder,
                linqIndex,
                null);

            Assert.IsNotNull(linqIndex, "Can't get an extended linqToSolrIndex...");

            var translatedFieldNames = fieldNames.Select(field => linqIndex.Parameters.FieldNameTranslator.GetIndexFieldName(field)).ToArray();

            // Execute the resulting query
            return searchContext.Query<T>(
                intermidQuery.ToString(),
                new QueryOptions()
                {
                    Fields = new[] {"*", "score"},
                    Highlight = new HighlightingParameters()
                    {
                        Fields = translatedFieldNames,
                        Fragsize = surroundingcharacters,
                        BeforeTerm = $"<{htmltag}>",
                        AfterTerm = $"</{htmltag}>"
                    }
                });
        }

        public static FacetResults GetFacetsMultiselect<T>(this IQueryable<T> source)
        {
            Guard.ArgumentNotNull(source, nameof(source));

            var queryable = source as GenericQueryable<T, SolrCompositeQuery>;
            Guard.ArgumentNotNull(queryable, nameof(queryable));

            // Build a native query 
            var linqIndex = (LinqToSolrIndex<T>)queryable.GetType().InvokeMember(
                "Index",
                BindingFlags.GetProperty | BindingFlags.Instance | BindingFlags.NonPublic,
                Type.DefaultBinder,
                queryable,
                null);

            var searchContext = (SolrSearchContext)linqIndex.GetType().InvokeMember(
                "context",
                BindingFlags.GetField | BindingFlags.Instance | BindingFlags.NonPublic,
                Type.DefaultBinder,
                linqIndex,
                null);

            Assert.IsNotNull(searchContext, "Can't get an extended linqToSolrIndex...");

            var linqToSolr = new FacetLinqToSolrIndex<T>(searchContext, null);
            
            return linqToSolr.Execute<FacetResults>((SolrCompositeQuery)((IHasNativeQuery)source).Query);
        }
    }
}