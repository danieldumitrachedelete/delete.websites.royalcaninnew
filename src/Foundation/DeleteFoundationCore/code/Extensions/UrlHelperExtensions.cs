﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;

namespace Delete.Foundation.DeleteFoundationCore.Extensions
{
    using System.Web;

    public static class UrlHelperExtensions
    {
        public static string UrlWithParameters(this UrlHelper urlHelper, string url, IDictionary<string, string> parameters = null, bool replace = true)
        {
            if (parameters == null || !parameters.Any())
            {
                return url;
            }

            var parameterString = "";

            if (replace)
            {
                parameterString = string.Join("&", parameters.Select(kv => $"{urlHelper.Encode(kv.Key)}={urlHelper.Encode(kv.Value.Replace(' ', '-'))}"));
            }
            else
            {
                parameterString = string.Join("&", parameters.Select(kv => $"{urlHelper.Encode(kv.Key)}={kv.Value.Replace(' ', '-')}"));
            }
            

            return $"{url}?{parameterString}";
        }
        
        public static string ApiAction(this UrlHelper urlHelper, string actionName, string controllerName, RouteValueDictionary routeValues = null)
        {
            routeValues = routeValues ?? new RouteValueDictionary();
            routeValues.Add("scLanguage", Sitecore.Context.Site.VirtualFolder.Trim('/'));
            return urlHelper.Action(actionName, controllerName, routeValues);
        }

        public static string AbsoluteUrl(this UrlHelper urlHelper, string url, string protocol = null)
        {
            var uri = new Uri(url, UriKind.RelativeOrAbsolute);

            if (uri.IsAbsoluteUri)
            {
                return url;
            }

            var requestUrl = urlHelper.RequestContext.HttpContext.Request.Url;
            if (requestUrl == null)
            {
                return url;
            }

            if (url.StartsWith("/"))
            {
                url = url.Insert(0, "~");
            }

            if (!url.StartsWith("~/"))
            {
                url = url.Insert(0, "~/");
            }

            var port = string.Empty;
            if (protocol.IsNullOrEmpty())
            {
                port = requestUrl.Port == 80
                           ? string.Empty : requestUrl.Port == 443 && requestUrl.Scheme.Equals("https", StringComparison.InvariantCultureIgnoreCase)
                               ? string.Empty : ":" + requestUrl.Port;
            }

            return $"{protocol ?? requestUrl.Scheme}://{requestUrl.Host}{port}{VirtualPathUtility.ToAbsolute(url)}";
        }
    }
}