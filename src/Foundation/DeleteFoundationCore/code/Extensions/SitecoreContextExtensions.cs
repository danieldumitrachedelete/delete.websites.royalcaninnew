﻿using System;
using System.Collections.Generic;
using System.Linq;
using Glass.Mapper.Sc;

namespace Delete.Foundation.DeleteFoundationCore.Extensions
{
    public static class SitecoreContextExtensions
    {
        private const string RelativeRootQuery = "./ancestor-or-self::*[@@templatekey='local market settings']";

        /// <summary>
        /// Mimics GetRootItem but with a relative query. 
        /// Implemented to work around a problem with Experience Editor not resolving root item correctly despite Preview.ResolveSite setting set to true.
        /// Doesn't work with AJAX requests because requires the context item
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sitecoreContext"></param>
        /// <param name="isLazy"></param>
        /// <param name="inferType"></param>
        /// <returns></returns>
        public static T GetRootItemRelative<T>(this ISitecoreContext sitecoreContext, bool isLazy = false,
            bool inferType = false) where T : class
        {
            if (!Sitecore.Context.PageMode.IsNormal && Sitecore.Context.Item != null)
            {
                return sitecoreContext.QuerySingleRelative<T>(RelativeRootQuery, isLazy, inferType);
            }

            return sitecoreContext.GetRootItem<T>(isLazy, inferType);
        }

        public static bool IsDynamicPlaceholderNotEmpty(this ISitecoreContext context, string dynamicPlaceholderName,
            int seed)
        {
            return Sitecore.Mvc.Presentation.PageContext.Current.IsDynamicPlaceholderNotEmpty(dynamicPlaceholderName,
                seed);
        }

        public static bool DoesPlaceholderContainRenderingByName(this ISitecoreContext context,
            List<string> names, string dynamicPlaceholderName, int seed)
        {
            return Sitecore.Mvc.Presentation.PageContext.Current.DoesPlaceholderContainRenderingByName(names,
                dynamicPlaceholderName,
                seed);
        }

        public static bool DoesPlaceholderContainRenderingByGuid(this ISitecoreContext context,
            List<Guid> guids, string dynamicPlaceholderName, int seed)
        {
            return Sitecore.Mvc.Presentation.PageContext.Current.DoesPlaceholderContainRenderingByGuid(guids,
                dynamicPlaceholderName,
                seed);
        }

    }
}