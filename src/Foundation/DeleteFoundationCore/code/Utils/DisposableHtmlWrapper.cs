﻿namespace Delete.Foundation.DeleteFoundationCore.Utils
{
    using System;

    public sealed class DisposableHtmlWrapper : IDisposable
    {
        private readonly Action end;

        public DisposableHtmlWrapper(Action begin, Action end)
        {
            this.end = end;
            begin();
        }

        public void Dispose()
        {
            this.end();
        }
    }
}