﻿using Glass.Mapper.Configuration;
using Glass.Mapper.Sc;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.DataMappers;
using Glass.Mapper.Sc.Fields;
using Sitecore.Data.Fields;
using Delete.Foundation.DeleteFoundationCore.Models;
using Sitecore;
using Context = Glass.Mapper.Context;

namespace Delete.Foundation.DeleteFoundationCore.FieldMappers
{
    public class ExtendedFileFieldMapper : SitecoreFieldFileMapper
    {
        public override bool CanHandle(AbstractPropertyConfiguration configuration, Context context)
        {
            return configuration is SitecoreFieldConfiguration &&
                   configuration.PropertyInfo.PropertyType == typeof(ExtendedFile);
        }

        public override object GetField(Field field, SitecoreFieldConfiguration config, SitecoreDataMappingContext context)
        {
            var fileField = new FileField(field);
            var file = base.GetField(field, config, context) as File;

            // Custom field type pass in the base Glass resolved type
            var extFile = new ExtendedFile(file);

            if (fileField.MediaItem != null)
            {
                // Extended File Attributes
                extFile.DateUpdated = DateUtil.IsoDateToDateTime(fileField.MediaItem.Fields[Constants.StandardTemplate.FieldNames.UpdatedDate].Value);
                extFile.DateCreated = DateUtil.IsoDateToDateTime(fileField.MediaItem.Fields[Constants.StandardTemplate.FieldNames.CreatedDate].Value);
            }

            return extFile;
        }
    }
}



