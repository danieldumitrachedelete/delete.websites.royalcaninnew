﻿using System;
using Glass.Mapper;
using Glass.Mapper.Configuration;
using Glass.Mapper.Sc;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.DataMappers;
using Glass.Mapper.Sc.Fields;

using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Resources.Media;

using Delete.Foundation.DeleteFoundationCore.FieldTypes;
using Delete.Foundation.DeleteFoundationCore.Models;
using Sitecore;
using Sitecore.Links;
using Context = Glass.Mapper.Context;

namespace Delete.Foundation.DeleteFoundationCore.FieldMappers
{
    public class ExtendedImageFieldMapper : SitecoreFieldImageMapper
    {
        public override bool CanHandle(AbstractPropertyConfiguration configuration, Context context)
        {
            return configuration is SitecoreFieldConfiguration &&
                   (configuration.PropertyInfo.PropertyType == typeof(Image) || configuration.PropertyInfo.PropertyType == typeof(ExtendedImage));
        }

        public override object GetField(Field field, SitecoreFieldConfiguration config, SitecoreDataMappingContext context)
        {
            if (field.Value.IsNullOrEmpty())
            {
                return null;
            }

            ExtendedImage img = new ExtendedImage();
            ExtendedImageField scImg = new ExtendedImageField(field);

            MapToImage(img, scImg);

            return img;
        }

        public static void MapToImage(ExtendedImage img, ExtendedImageField field)
        {
            int.TryParse(field.Height, out var height);
            int.TryParse(field.Width, out var width);
            int.TryParse(field.HSpace, out var hSpace);
            int.TryParse(field.VSpace, out var vSpace);

            img.Alt = field.Alt;
            img.Border = field.Border;
            img.Class = field.Class;
            img.Height = height;
            img.HSpace = hSpace;
            img.MediaId = field.MediaID.Guid;

            if (field.MediaItem != null)
            {
                img.Src = MediaManager.GetMediaUrl(field.MediaItem);
                var fieldTitle = field.MediaItem.Fields["Title"];
                if (fieldTitle != null)
                {
                    img.Title = fieldTitle.Value;
                }
                img.DateUpdated = DateUtil.IsoDateToDateTime(field.MediaItem.Fields[Constants.StandardTemplate.FieldNames.UpdatedDate].Value);
                img.DateCreated = DateUtil.IsoDateToDateTime(field.MediaItem.Fields[Constants.StandardTemplate.FieldNames.CreatedDate].Value);
            }
            else if(!field.ExternalSource.IsNullOrWhiteSpace())
            {
                img.Src = field.ExternalSource;
                if (System.DateTime.TryParse(field.ExternalDate, out var date))
                {
                    img.DateUpdated = date;
                    img.DateCreated = date;
                }

                img.AdditionalParameters = field.AdditionalParameters;
            }

            img.VSpace = vSpace;
            img.Width = width;
            img.Language = field.MediaLanguage;
        }

		public override object GetFieldValue(string fieldValue, SitecoreFieldConfiguration config, SitecoreDataMappingContext context)
        {
            var item = context.Service.Database.GetItem(new ID(fieldValue));

            if (item == null)
            {
                return null;
            }

            var imageItem = new MediaItem(item);
            var image = new ExtendedImage();
            MapToImage(image, imageItem);
            return image;
		}

		public override void SetField(Field field, object value, SitecoreFieldConfiguration config, SitecoreDataMappingContext context)
		{
			ExtendedImage image = value as ExtendedImage;
			Item obj = field.Item;
			if (field == null)
				return;
			ExtendedImageFieldMapper.MapToField(new ExtendedImageField(field), image, obj);
		}

		public static void MapToField(ExtendedImageField field, ExtendedImage image, Item item)
		{
			if (image == null)
			{
				field.Clear();
			}
			else
			{
				if (field.MediaID.Guid != image.MediaId)
				{
					if (image.MediaId == Guid.Empty)
					{
						ItemLink itemLink = new ItemLink(item.Database.Name, item.ID, field.InnerField.ID, field.MediaItem.Database.Name, field.MediaID, field.MediaItem.Paths.Path);
						field.RemoveLink(itemLink);
					}
					else
					{
						ID itemId = new ID(image.MediaId);
						Item obj = item.Database.GetItem(itemId);
						if (obj != null)
						{
							field.MediaID = itemId;
							ItemLink itemLink = new ItemLink(item.Database.Name, item.ID, field.InnerField.ID, obj.Database.Name, obj.ID, obj.Paths.FullPath);
							field.UpdateLink(itemLink);
						}
						else
							throw new MapperException("No item with ID {0}. Can not update Media Item field".Formatted((object)itemId));
					}
				}

				if (image.MediaId == Guid.Empty)
				{
					if (!image.Src.IsNullOrWhiteSpace())
					{
						field.ExternalSource = image.Src ?? string.Empty;
						field.ExternalDate = DateUtil.ToIsoDate(image.DateUpdated > image.DateCreated ? image.DateUpdated : image.DateCreated) ?? string.Empty;
						field.AdditionalParameters = image.AdditionalParameters ?? string.Empty;
					}
				}

				if (image.Height > 0)
					field.Height = image.Height.ToString();
				if (image.Width > 0)
					field.Width = image.Width.ToString();
				if (image.HSpace > 0)
					field.HSpace = image.HSpace.ToString();
				if (image.VSpace > 0)
					field.VSpace = image.VSpace.ToString();
				if (field.Alt.HasValue() || image.Alt.HasValue())
					field.Alt = image.Alt ?? string.Empty;
				if (field.Border.HasValue() || image.Border.HasValue())
					field.Border = image.Border ?? string.Empty;
				if (field.Class.HasValue() || image.Class.HasValue())
					field.Class = image.Class ?? string.Empty;

			}
		}

	}
}



