﻿using System;
using System.Web.UI;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Sitecore.Diagnostics;
using Sitecore.Pipelines;

namespace Delete.Foundation.DeleteFoundationCore
{
    public class InjectScripts
    {
        public void Process(PipelineArgs args)
        {
            AddControls("CustomContentEditorJavascript",  string.Empty, false, true);
            AddControls("CustomContentEditorCss", "media=\"print\" onload=\"this.onload=null;this.media='all'\"", true, false);
        }

        private void AddControls(string configKey, string attributes = "", bool isLink = true, bool isScript = false)
        {
            Assert.IsNotNullOrEmpty(configKey, "Content Editor resource config key cannot be null");

            string resources = Sitecore.Configuration.Settings.GetSetting(configKey);

            if (String.IsNullOrEmpty(resources))
                return;

            var scriptNames = resources.Split('|');
            
            var str = HtmlHelperExtensions.ReadFromManifestJson(null, attributes, isLink, isScript, scriptNames);

            Sitecore.Context.Page.Page.Header.Controls.Add(new LiteralControl(str.ToHtmlString()));
        }
    }
}
