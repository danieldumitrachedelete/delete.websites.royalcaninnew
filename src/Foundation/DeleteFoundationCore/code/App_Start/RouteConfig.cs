using System;
using System.Web.Mvc;
using System.Web.Routing;
using Sitecore.Diagnostics;

namespace Delete.Foundation.DeleteFoundationCore
{
    public static class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            try
            {
                routes.MapRoute(
                    "RoyalCanin_Formbuilder_Default",
                    "formbuilder",
                    new { controller = "FormBuilder", action = "Index" });

                routes.MapRoute(
                    "RoyalCanin_Api",
                    "{scLanguage}/api/{controller}/{action}");

                routes.MapRoute(
                    "RoyalCanin_Formbuilder",
                    "{scLanguage}/formbuilder",
                    new { controller = "FormBuilder", action = "Index" });

                routes.MapRoute(
                    "RoyalCanin_MultilanguageMarket_Api",
                    "{countryCode}/{scLanguage}/api/{controller}/{action}");

                routes.MapRoute(
                    "RoyalCanin_MultilanguageMarket_Formbuilder",
                    "{countryCode}/{scLanguage}/formbuilder",
                    new { controller = "FormBuilder", action = "Index" });
            }
            catch (Exception exception)
            {
                var msg = "An error occured when initialising RoyalCaninMvcRoutes. APIs would be inaccessible.\r\n" +
                          "Error details:\r\n" +
                          $"{exception.Message}\r\n" +
                          $"{exception.StackTrace}";
                Log.Error(msg, typeof(RouteConfig));
                throw;
            }
        }
    }
}