﻿using System.Web.Mvc;
using Delete.Foundation.DeleteFoundationCore.Events;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Sitecore.Pipelines;

namespace Delete.Foundation.DeleteFoundationCore.Pipeline
{
    public class ConfigureSitecoreEventHandlers
    {
        public void Process(PipelineArgs args)
        {
            var cacheManager = DependencyResolver.Current.GetService<ICacheManager>();
            var eventHandlers = DependencyResolver.Current.GetServices<BaseEventHandler>();
            EventConfigurator.Configure(eventHandlers, cacheManager);
        }
    }
}