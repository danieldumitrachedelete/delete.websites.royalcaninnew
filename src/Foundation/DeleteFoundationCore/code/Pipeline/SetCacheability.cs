﻿using Delete.Foundation.DeleteFoundationCore.Extensions;

namespace Delete.Foundation.DeleteFoundationCore.Pipeline
{
    public class SetCacheability : Sitecore.Mvc.Pipelines.Response.RenderRendering.SetCacheability
    {
        protected override bool IsCacheable(Sitecore.Mvc.Presentation.Rendering rendering, Sitecore.Mvc.Pipelines.Response.RenderRendering.RenderRenderingArgs args)
        {
            var cancelCaching = rendering.Parameters["Cancel Cache Settings"];
            if (cancelCaching.NotEmpty() && cancelCaching == "1")
            {
                return false;
            }

            return base.IsCacheable(rendering, args);
        }
    }
}