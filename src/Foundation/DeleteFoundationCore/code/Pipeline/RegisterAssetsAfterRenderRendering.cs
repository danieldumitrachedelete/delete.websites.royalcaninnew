﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Delete.Foundation.DeleteFoundationCore.Pipeline
{
    using System.IO;
    using System.Text.RegularExpressions;

    using Extensions;
    using Glass.Mapper;
    using Models;

    using Newtonsoft.Json;

    using Sitecore.Caching;
    using Sitecore.Diagnostics;
    using Sitecore.Mvc.Extensions;
    using Sitecore.Mvc.Pipelines.Response.RenderRendering;

    [JetBrains.Annotations.UsedImplicitly]
    public class RegisterAssetsAfterRenderRendering : RenderRenderingProcessor
    {
        private const string PatternFiles = "(<script.*type=\"({0})\\/component\"(?:(?!>).)*)(>)(((?!<\\/script.*>).|\n|\r\n)*)(<\\/\\s*script\\s*>)";

        private const int ComponentGroup = 4;

        private const int ComponentTypeGroup = 2;

        private static readonly string PatternAssets = string.Format(
            PatternFiles,
            $"{HtmlHelperExtensions.CssComponentType}|{HtmlHelperExtensions.JavascriptComponentType}|{HtmlHelperExtensions.SvgComponentType}");

        private static Dictionary<string, Action<string>> actions =
            new Dictionary<string, Action<string>>
                {
                    {
                        HtmlHelperExtensions.CssComponentType, x => RegisterComponent(HtmlHelperExtensions.CurrentContextStylesKey, x)
                    },
                    {
                        HtmlHelperExtensions.SvgComponentType, x => RegisterComponent(HtmlHelperExtensions.CurrentContextSvgFilesKey, x)
                    },
                    {
                        HtmlHelperExtensions.JavascriptComponentType, x => RegisterComponent(HtmlHelperExtensions.CurrentContextScriptsKey, x)
                    }
                };

        public override void Process(RenderRenderingArgs args)
        {
            Assert.ArgumentNotNull((object)args, nameof(args));
            if (args.Rendering.RenderingType.Equals("Layout", StringComparison.OrdinalIgnoreCase)) return;

            var outputHtml = GetHtmlFromCache(args);
            if (!outputHtml.HasValue())
            {
                var writer = args.Writer as StringWriter;
                if (writer == null) return;

                outputHtml = writer.ToString();
            }

            FindScriptsInHtmlAndRegisterThem(outputHtml, PatternAssets);
        }

        private static string GetHtmlFromCache(RenderRenderingArgs args)
        {
            if (!args.CacheKey.HasValue()) return string.Empty;

            var htmlCache = Sitecore.Context.Site.ValueOrDefault(CacheManager.GetHtmlCache);
            return htmlCache?.GetHtml(args.CacheKey) ?? string.Empty;
        }

        private static void FindScriptsInHtmlAndRegisterThem(string html, string pattern)
        {
            if (!html.HasValue()) return;

            var regexScripts = new Regex(pattern, RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.CultureInvariant);
            var matchCollection = regexScripts.Matches(html);

            foreach (Match match in matchCollection)
            {
                if (match.Groups.Count < Math.Max(ComponentGroup, ComponentTypeGroup) + 1) continue;

                ParseJsonObjectAndRegisterComponent(
                    match.Groups[ComponentTypeGroup].Value,
                    match.Groups[ComponentGroup].Value);
            }
        }

        private static void ParseJsonObjectAndRegisterComponent(string componentType, string componentText)
        {
            if (!componentType.HasValue()) return;
            if (!componentText.HasValue()) return;

            actions[GetComponentType(componentType)](componentText);
        }

        private static string GetComponentType(string componentType)
        {
            if (componentType.Equals(
                HtmlHelperExtensions.CssComponentType,
                StringComparison.OrdinalIgnoreCase))
            {
               return HtmlHelperExtensions.CssComponentType;
            }

            if (componentType.Equals(
                HtmlHelperExtensions.JavascriptComponentType,
                StringComparison.OrdinalIgnoreCase))
            {
                return HtmlHelperExtensions.JavascriptComponentType;
            }

            if (componentType.Equals(
                HtmlHelperExtensions.SvgComponentType,
                StringComparison.OrdinalIgnoreCase))
            {
                return HtmlHelperExtensions.SvgComponentType;
            }

            return string.Empty;
        }

        private static void RegisterComponent(string contextKey, string componentText)
        {
            if (!contextKey.HasValue()) return;
            if (!componentText.HasValue()) return;

            var items = HttpContext.Current.Items[contextKey] as Dictionary<string, StaticAsset>
                        ?? new Dictionary<string, StaticAsset>();

            var assets = JsonConvert.DeserializeObject<List<StaticAsset>>(componentText);
            if (assets == null || !assets.Any()) return;

            foreach (var staticAsset in assets)
            {
                if (items.ContainsKey(staticAsset.Path)) continue;

                items.Add(staticAsset.Path, staticAsset);
            }

            HttpContext.Current.Items[contextKey] = items;
        }
    }
}
