﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Delete.Foundation.DeleteFoundationCore.Attributes;
using Sitecore.Pipelines;

namespace Delete.Foundation.DeleteFoundationCore.Pipeline
{
    public class RegisterSitecoreRequiredAttributeAdapter
    {
        public void Process(PipelineArgs args)
        {
            DataAnnotationsModelValidatorProvider.RegisterAdapter(typeof(RequiredAttribute), typeof(SitecoreRequiredAttributeAdapter));
        }
    }
}