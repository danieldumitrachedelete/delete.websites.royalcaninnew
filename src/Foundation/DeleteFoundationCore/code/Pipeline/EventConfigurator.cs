﻿using System.Collections.Generic;
using Delete.Foundation.DeleteFoundationCore.Events;
using Delete.Foundation.DeleteFoundationCore.Interfaces;

namespace Delete.Foundation.DeleteFoundationCore.Pipeline
{
    public static class EventConfigurator
    {
        public static void Configure(IEnumerable<BaseEventHandler> eventHandlers, ICacheManager cacheManager)
        {
            foreach (var eventHandler in eventHandlers)
            {
                eventHandler.CacheManager = cacheManager;
                foreach (var eventName in eventHandler.EventsList)
                {
                    Sitecore.Events.Event.Subscribe(eventName, eventHandler.OnEvent);
                }
            }
        }
    }
}