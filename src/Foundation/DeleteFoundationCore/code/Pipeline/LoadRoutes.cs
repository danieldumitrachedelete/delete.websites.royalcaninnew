﻿using System.Web.Routing;
using Sitecore.Pipelines;

namespace Delete.Foundation.DeleteFoundationCore.Pipeline
{
  public class LoadRoutes
  {
    public void Process(PipelineArgs args)
    {
        RouteConfig.RegisterRoutes(RouteTable.Routes);
    }
  }
}