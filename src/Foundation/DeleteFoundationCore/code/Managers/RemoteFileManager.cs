﻿namespace Delete.Foundation.DeleteFoundationCore.Managers
{
    using System.Collections.Generic;

    public interface IRemoteFileManager
    {
        IList<string> GetFiles(string remotePath, string localPath);

        void PutFiles(string localPath, string remotePath);

        void ArchiveFiles(string remotePathFrom, string remotePathTo, IList<string> filenames);
    }
}