﻿using System;
using System.Linq;
using System.Reflection;

namespace Delete.Foundation.DeleteFoundationCore.Managers
{
    public static class AssemblyManager
    {
        public static Assembly FindAssemblyByName(string assemblyName)
        {
            return AppDomain.CurrentDomain.GetAssemblies()
                .FirstOrDefault(assembly =>
                {
                    var nameToMatch = assembly.GetName().Name;
                    return nameToMatch.Equals(assemblyName, StringComparison.InvariantCultureIgnoreCase);
                });
        }

        public static Type GetTypeByFullName(string classFullName)
        {
            return Type.GetType(classFullName);
        }

        public static string[] GetAssembliesNames()
        {
            return GetAssemblies().Select(a => a.GetName().Name).ToArray();
        }

        public static Assembly[] GetAssemblies()
        {
            // Provide the current application domain evidence for the assembly.
            AppDomain currentDomain = AppDomain.CurrentDomain;

            // Create an assembly called CustomLibrary to run this sample.
            currentDomain.Load(Assembly.GetCallingAssembly().GetName());

            // Make an array for the list of assemblies.
            Assembly[] assemblies = currentDomain.GetAssemblies().Where(x => (x.FullName.Contains(".Feature.") ||
                                                                              x.FullName.Contains(".Foundation.") ||
                                                                              x.FullName.Contains(".Project.")) &&
                                                                             x.GetName().Name != nameof(AutoMapper)).ToArray();
            return assemblies;
        }
    }
}