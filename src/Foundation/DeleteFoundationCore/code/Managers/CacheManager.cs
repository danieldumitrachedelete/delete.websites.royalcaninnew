﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using Delete.Foundation.DeleteFoundationCore.Configurations;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Sitecore.Configuration;
using Sitecore.Data;

namespace Delete.Foundation.DeleteFoundationCore.Managers
{
    public class CacheManager : ICacheManager
    {
        private const string DependencyKey = "Delete.Websites.Helix.BaseCacheManager.Dependency";

        static CacheManager()
        {
            HttpRuntime.Cache.Add(DependencyKey,
              DateTime.Now,
              null,
              Cache.NoAbsoluteExpiration,
              Cache.NoSlidingExpiration,
              CacheItemPriority.Default,
              null);

            var customCacheDependenciesConfig = Factory.CreateObject("customCacheKeyDependencies/list", false) as CacheKeyCustomDependenciesConfiguration;
            if (customCacheDependenciesConfig?.CacheDependencies != null && customCacheDependenciesConfig.CacheDependencies.Any())
            {
                var customDependencies = customCacheDependenciesConfig.CacheDependencies.Select(x => x.Value).Distinct();
                foreach (var customDependency in customDependencies)
                {
                    HttpRuntime.Cache.Add(customDependency,
                      DateTime.Now,
                      null,
                      Cache.NoAbsoluteExpiration,
                      Cache.NoSlidingExpiration,
                      CacheItemPriority.Default,
                      null);
                }
            }
        }

        public T Get<T>(string key) where T : class
        {
            try
            {
#if DEBUG

                if (Sitecore.Context.Site != null && !Sitecore.Context.PageMode.IsNormal)
                {
                    return null;
                }
#else
            // in release never cache unless normal view
            if (!Sitecore.Context.PageMode.IsNormal)
            {
                // Only use caching in normal mode.
                return null;
            }
#endif
                return HttpRuntime.Cache[GetKey(key)] as T;
            }
            catch
            {
                return null;
            }
        }

        public void Insert<T>(T item, string key)
        {
            Insert(item, key, Configuration.DefaultCacheTime);
        }

        public void Insert<T>(T item, string key, DateTime expirationTime)
        {
            if (item == null)
            {
                return;
            }

            var cacheDependency = GetCacheDependency(key);
            HttpRuntime.Cache.Insert(GetKey(key), item, cacheDependency, expirationTime, Cache.NoSlidingExpiration);
        }


        public void Insert<T>(T item, string key, TimeSpan expirationSpan)
        {
            if (item == null)
            {
                return;
            }

            var cacheDependency = GetCacheDependency(key);
            HttpRuntime.Cache.Insert(GetKey(key), item, cacheDependency, Cache.NoAbsoluteExpiration, expirationSpan);
        }

        public void Clear()
        {
            Cache c = HttpRuntime.Cache;

            if (c != null)
            {
                c.Insert(DependencyKey,
                    DateTime.Now,
                    null,
                    Cache.NoAbsoluteExpiration,
                    Cache.NoSlidingExpiration,
                    CacheItemPriority.Default,
                    null);

            }
        }

        public void ClearCustomCacheByItemId(ID itemId)
        {
            Cache c = HttpRuntime.Cache;
            var searcableItemId = itemId.Guid.ToString();
            var customCacheDependenciesConfig = Factory.CreateObject("customCacheCleanerKeyTypeDependencies/list", false) as CacheCleanerKeyTypeDependenciesConfiguration;
            if (customCacheDependenciesConfig?.TypeCacheKeyDependencies != null && customCacheDependenciesConfig.TypeCacheKeyDependencies.Any(x => x.Key == searcableItemId))
            {
                var customDependencyRule = customCacheDependenciesConfig.TypeCacheKeyDependencies.FirstOrDefault(x => x.Key == searcableItemId).Value;
                if (customDependencyRule != null)
                {
                    c.Insert(customDependencyRule,
                    DateTime.Now,
                    null,
                    Cache.NoAbsoluteExpiration,
                    Cache.NoSlidingExpiration,
                    CacheItemPriority.Default,
                    null);
                }
            }
        }

        private static string GetKey(string baseKey)
        {
            if (Sitecore.Context.Site != null && Sitecore.Context.Database != null)
            {
                return $"{Sitecore.Context.Site.Name}_{Sitecore.Context.Database.Name}_{baseKey}";
            }
            else
            {
                return baseKey;
            }
        }

        private static CacheDependency GetCacheDependency(string key)
        {
            var customCacheDependenciesConfig = Factory.CreateObject("customCacheKeyDependencies/list", false) as CacheKeyCustomDependenciesConfiguration;
            CacheDependency cacheDependency;
            if (customCacheDependenciesConfig?.CacheDependencies != null &&
                customCacheDependenciesConfig.CacheDependencies.Any() &&
                customCacheDependenciesConfig.CacheDependencies.Select(x => x.Key).Any(key.Contains))
            {
                var customDependency = customCacheDependenciesConfig.CacheDependencies.FirstOrDefault(x => key.Contains(x.Key));
                cacheDependency = new CacheDependency(null, new[] { customDependency.Value });
            }
            else
            {
                cacheDependency = new CacheDependency(null, new[] { DependencyKey });
            }
            return cacheDependency;
        }
    }
}