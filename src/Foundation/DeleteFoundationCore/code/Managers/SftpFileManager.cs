﻿namespace Delete.Foundation.DeleteFoundationCore.Managers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Extensions;

    using WinSCP;

    public class SftpFileManager : IRemoteFileManager
    {
        private readonly SessionOptions sessionOptions;

        public SftpFileManager(string hostname, string username, string password)
        {
            this.sessionOptions = new SessionOptions
            {
                Protocol = Protocol.Ftp,
                HostName = hostname,
                UserName = username,
                Password = password,
                FtpSecure = FtpSecure.None,
                WebdavSecure = false
            };
        }

        public IList<string> GetFiles(string remotePath, string localPath)
        {
            try
            {
                using (var session = new Session())
                {
                    session.Open(this.sessionOptions);

                    var result = session.GetFiles(remotePath, localPath, false);

                    // throw on any errors
                    result.Check();

                    return result.Transfers.Select(x => x.FileName).ToList();
                }
            }
            catch (Exception exception)
            {
                Sitecore.Diagnostics.Log.Error("Error getting files", exception, this);
                return new List<string>();
            }
        }

        public void PutFiles(string localPath, string remotePath)
        {
            try
            {
                using (var session = new Session())
                {
                    session.Open(this.sessionOptions);

                    var transferOptions = new TransferOptions { TransferMode = TransferMode.Binary };

                    var result = session.PutFiles(localPath, remotePath, options: transferOptions);

                    result.Check();
                }
            }
            catch (Exception exception)
            {
                Sitecore.Diagnostics.Log.Error("Error putting files", exception, this);
            }
        }

        public virtual void ArchiveFiles(string remotePathFrom, string remotePathTo, IList<string> filenames)
        {
            try
            {
                using (var session = new Session())
                {
                    session.Open(this.sessionOptions);
                    foreach (var filename in filenames)
                    {
                        var fileSource = remotePathFrom.EnsureEndsWith("/") + filename;

                        // save processed file with timestamp for historical investigation
                        var fileDestination = remotePathTo.EnsureEndsWith("/") + DateTime.Now.ToString("yyyyMMdd-hhmmssfff") + "-" + filename;
                        session.MoveFile(fileSource, fileDestination);
                    }
                }
            }
            catch (Exception exception)
            {
                Sitecore.Diagnostics.Log.Error("Error archiving files", exception, this);
            }
        }
    }
}