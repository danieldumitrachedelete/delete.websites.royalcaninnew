﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.ContentSearch;

namespace Delete.Foundation.DeleteFoundationCore.Managers
{
    public static class SearchManagersConfiguration
    {
        private const string CustomIndexNameFormat = "{0}_index"; //example: predictive_locations_index
        private const string SitecoreManagedCustomIndexNameFormat = "sitecore_{0}_{1}_index"; //example: sitecore_property_master_index
        private const string DefaultIndexNameFormat = "sitecore_{0}_index"; //example: sitecore_master_index

        private static readonly List<IndexConfigurationItem> CurrentConfigurations =
            new List<IndexConfigurationItem>
            {
            };


        public static void RegisterIndexConfiguration(Type assignedType, string indexAlias)
        {
            CurrentConfigurations.Add(
                new IndexConfigurationItem
                {
                    ManagedBySitecore = true,
                    AssignedType = assignedType,
                    ContextDatabaseName = "master",
                    IndexAlias = indexAlias
                });

            CurrentConfigurations.Add(
                new IndexConfigurationItem
                {
                    ManagedBySitecore = true,
                    AssignedType = assignedType,
                    ContextDatabaseName = "web",
                    IndexAlias = indexAlias
                });
        }

        public static string GetIndexNameByType(Type currenItemType, string databaseName)
        {
            var configuration = CurrentConfigurations.FirstOrDefault(x => x.AssignedType == currenItemType && (!x.ManagedBySitecore || x.ContextDatabaseName == databaseName));

            if (configuration != null)
            {
                if (configuration.ManagedBySitecore)
                {
                    var sitecoreIndexName = string.Format(SitecoreManagedCustomIndexNameFormat, configuration.IndexAlias, databaseName);

                    if (ContentSearchManager.Indexes.FirstOrDefault(x => x.Name.Equals(sitecoreIndexName)) != null)
                    {
                        return sitecoreIndexName;
                    }
                }
                else
                {
                    return string.Format(CustomIndexNameFormat, configuration.IndexAlias);
                }
            }

            return string.Format(DefaultIndexNameFormat, databaseName);
        }

        public static string GetSecondaryIndexNameByType(Type currenItemType, string databaseName)
        {
            var indexName = GetIndexNameByType(currenItemType, databaseName);

            return $"{indexName}_sec";
        }
    }

    public class IndexConfigurationItem
    {
        public bool ManagedBySitecore { get; set; }

        public Type AssignedType { get; set; }

        public string ContextDatabaseName { get; set; }

        public string IndexAlias { get; set; }
    }
}