﻿using System;
using System.Collections.Generic;
using System.Linq;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.DeleteFoundationCore.Models;
using Glass.Mapper;
using Glass.Mapper.Sc;
using Glass.Mapper.Sc.Configuration.Attributes;
using Sitecore.ContentSearch;
using Sitecore.Data;
using Sitecore.Globalization;

namespace Delete.Foundation.DeleteFoundationCore.Managers
{
    public class BaseSearchManager<TItem> : IBaseSearchManager<TItem> where TItem : GlassBase
    {
        protected IProviderSearchContext SearchContext { get; }

        protected ISitecoreContext SitecoreContext { get; }

        public virtual IEnumerable<TItem> GetAll(IProviderSearchContext context = null, bool mapResults = false, bool inferType = false, bool isLazy = false, string path = null, Language language = null)
        {
            var query = GetQueryable();

            if (path.HasValue())
            {
                query = query.Where(x => x.Fullpath.StartsWith(path));
            }

            if (language != null)
            {
                query = query.Where(x => x.Language.Equals(language.Name));
            }

            if (mapResults || inferType)
            {
                return query.MapResults(SitecoreContext, inferType, isLazy);
            }

            return query;
        }

        public virtual IEnumerable<TItem> GetAllWithTemplate(IProviderSearchContext context = null, bool mapResults = false, bool inferType = false, bool isLazy = false, string path = null, Language language = null)
        {
            var query = GetQueryableAllLanguages();

            if (Attribute.GetCustomAttribute(typeof(TItem), typeof(SitecoreTypeAttribute)) is SitecoreTypeAttribute template)
            {
                var templateId = new ID(template.TemplateId);
                query = query.Where(x => x.TemplateId == templateId || x.BaseTemplates.Contains(templateId.Guid));
            }

            if (path.HasValue())
            {
                 query = query.Where(x => x.Fullpath.StartsWith(path));
            }

            if (language != null)
            {
                query = query.Where(x => x.Language.Equals(language.Name));
            }

            if (mapResults || inferType)
            {
                return query.MapResults(SitecoreContext, inferType, isLazy);
            }

            return query;
        }

        public TItem FindItem(Guid itemGuid)
        {
            //find items in solr and do not map all of them
            return GetAll(null, false, false)
                .Where(x => x.Id == itemGuid)
                .MapResults(SitecoreContext, true)
                .FirstOrDefault();
        }

        public IEnumerable<TItem> GetChunk(int chunkSize, int chunksToSkip = 0, IProviderSearchContext context = null, bool mapResults = false, bool inferType = false, bool isLazy = false)
        {
            var query = GetQueryable();

            query = query.PaginateQuery(chunksToSkip, chunkSize);

            if (inferType)
            {
                return query.MapResults(SitecoreContext, true, isLazy);
            }

            return query;
        }

        public BaseSearchManager(string contextDatabaseName)
        {
            Guard.ArgumentNotEmpty(contextDatabaseName, nameof(contextDatabaseName));

            SearchContext = CreateSearchContext(contextDatabaseName);
            SitecoreContext = CreateSitecoreContext(contextDatabaseName);
        }

        public BaseSearchManager(IProviderSearchContext context = null, ISitecoreContext sitecoreContext = null)
        {
            SearchContext = context ?? CreateSearchContext();
            SitecoreContext = sitecoreContext ?? CreateSitecoreContext();
        }

        protected virtual IQueryable<TItem> GetQueryable()
        {
            return SearchContext
                .GetQueryable<TItem>()
                .Where(i => i.Name != Constants.Constants.Sitecore.StandardValuesItemName 
                            && i.Name != Constants.Constants.Sitecore.BranchItemName 
                            && i.Language == Sitecore.Context.Language.Name
                            && i.IsLatestVersion);
        }

        protected virtual IQueryable<TItem> GetQueryableAllLanguages()
        {
            return SearchContext
                .GetQueryable<TItem>()
                .Where(i => i.Name != Constants.Constants.Sitecore.StandardValuesItemName && i.Name != Constants.Constants.Sitecore.BranchItemName && i.IsLatestVersion);
        }

        private static IProviderSearchContext CreateSearchContext(string contextDatabaseName = null)
        {
            var contextDbName = contextDatabaseName ?? Sitecore.Context.Database.Name;
            var targetIndexName = SearchManagersConfiguration.GetIndexNameByType(typeof(TItem), contextDbName);
            return ContentSearchManager.GetIndex(targetIndexName).CreateSearchContext();
        }

        private static ISitecoreContext CreateSitecoreContext(string contextDatabaseName = null)
        {
            var contextDbName = contextDatabaseName ?? Sitecore.Context.Database.Name;
            return new SitecoreContext(Database.GetDatabase(contextDbName));
        }

        protected IQueryable<TItem> FilterLocal(IQueryable<TItem> filtered, IGlassBase root = null)
        {
            var rootItem = root ?? SitecoreContext.GetRootItemRelative<IGlassBase>();
            return filtered.Where(x => x.Fullpath.StartsWith(rootItem.Fullpath));
        }
    }
}