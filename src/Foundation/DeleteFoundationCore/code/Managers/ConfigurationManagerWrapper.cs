﻿using System.Configuration;
using Delete.Foundation.DeleteFoundationCore.Extensions;

namespace Delete.Foundation.DeleteFoundationCore.Managers
{
    public class ConfigurationManagerWrapper
    {
        protected static string GetSetting(string settingName, string defaultValue = null)
        {
            var settingValue = ConfigurationManager.AppSettings[settingName];

            if (string.IsNullOrEmpty(settingValue))
            {
                settingValue = defaultValue;
            }

            return settingValue;
        }

        protected static int GetSetting(string settingName, int defaultValue)
        {
            var settingValue = ConfigurationManager.AppSettings[settingName];

            return settingValue.ParseOrDefault(defaultValue);
        }

        protected static double GetSetting(string settingName, double defaultValue)
        {
            var settingValue = ConfigurationManager.AppSettings[settingName];

            return settingValue.ParseOrDefault(defaultValue);
        }

        protected static bool GetSetting(string settingName, bool defaultValue)
        {
            var settingValue = ConfigurationManager.AppSettings[settingName];

            bool parsedValue;
            if (bool.TryParse(settingValue, out parsedValue))
            {
                return parsedValue;
            }

            return defaultValue;
        }

        protected static string[] GetCommaSeparatedSetting(string settingName)
        {
            var settingValue = ConfigurationManager.AppSettings[settingName];

            if (!string.IsNullOrEmpty(settingValue))
            {
                return settingValue.SafelySplit(",");
            }

            return new string[0];
        }
    }
}