﻿
namespace Delete.Foundation.DeleteFoundationCore.Managers
{
    public class SitecoreConfigurationManagerWrapper
    {
        protected static string GetSetting(string settingName, string defaultValue = null)
        {
            var settingValue = Sitecore.Configuration.Settings.GetSetting(settingName);

            if (string.IsNullOrEmpty(settingValue))
            {
                settingValue = defaultValue;
            }

            return settingValue;
        }

        protected static int GetSetting(string settingName, int defaultValue)
        {
            return Sitecore.Configuration.Settings.GetIntSetting(settingName, defaultValue);
        }

        protected static double GetSetting(string settingName, double defaultValue)
        {
            return Sitecore.Configuration.Settings.GetDoubleSetting(settingName, defaultValue);
        }

        protected static bool GetSetting(string settingName, bool defaultValue)
        {
            return Sitecore.Configuration.Settings.GetBoolSetting(settingName, defaultValue);
        }
    }
}