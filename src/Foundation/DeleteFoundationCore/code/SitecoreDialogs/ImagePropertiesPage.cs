﻿using System;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Diagnostics;
using Sitecore.Shell.Applications.ContentEditor;
using Sitecore.Web;
using Sitecore.Web.UI.XamlSharp.Xaml;
using Sitecore.Controls;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Web.UI.Sheer;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Globalization;
using Delete.Foundation.DeleteFoundationCore.Extensions;

namespace Delete.Foundation.DeleteFoundationCore.SitecoreDialogs
{
    public class ImagePropertiesPage : DialogPage
    {
        protected TextBox Alt;
        protected TextBox AdditionalParameters;
        protected Sitecore.Web.UI.HtmlControls.Checkbox Aspect;
        protected TextBox HSpace;
        protected TextBox HeightEdit;
        protected Sitecore.Web.UI.HtmlControls.Literal OriginalSize;
        protected TextBox OriginalText;
        protected Border SizeWarning;
        protected TextBox VSpace;
        protected TextBox WidthEdit;

        public int ImageHeight
        {
            get
            {
                return (int)this.ViewState[nameof(ImageHeight)];
            }
            set
            {
                this.ViewState[nameof(ImageHeight)] = value;
            }
        }

        public int ImageWidth
        {
            get
            {
                return (int)this.ViewState[nameof(ImageWidth)];
            }
            set
            {
                this.ViewState[nameof(ImageWidth)] = value;
            }
        }

        private XmlValue XmlValue
        {
            get
            {
                return new XmlValue(StringUtil.GetString(this.ViewState[nameof(XmlValue)]), "image");
            }
            set
            {
                Assert.ArgumentNotNull(value, nameof(value));
                this.ViewState[nameof(XmlValue)] = value.ToString();
            }
        }

        protected void ChangeHeight()
        {
            if (this.ImageHeight == 0)
                return;
            var num = MainUtil.GetInt(this.HeightEdit.Text, 0);
            if (num > 0)
            {
                if (num > 8192)
                {
                    num = 8192;
                    this.HeightEdit.Text = "8192";
                    SheerResponse.SetAttribute(this.HeightEdit.ClientID, "value", this.HeightEdit.Text);
                }
                if (this.Aspect.Checked)
                {
                    this.WidthEdit.Text = ((int)(num / (double)this.ImageHeight * this.ImageWidth)).ToString();
                    SheerResponse.SetAttribute(this.WidthEdit.ClientID, "value", this.WidthEdit.Text);
                }
            }
            SheerResponse.SetReturnValue(true);
        }

        protected void ChangeWidth()
        {
            if (this.ImageWidth == 0)
                return;
            var num = MainUtil.GetInt(this.WidthEdit.Text, 0);
            if (num > 0)
            {
                if (num > 8192)
                {
                    num = 8192;
                    this.WidthEdit.Text = "8192";
                    SheerResponse.SetAttribute(this.WidthEdit.ClientID, "value", this.WidthEdit.Text);
                }
                if (this.Aspect.Checked)
                {
                    this.HeightEdit.Text = ((int)(num / (double)this.ImageWidth * this.ImageHeight)).ToString();
                    SheerResponse.SetAttribute(this.HeightEdit.ClientID, "value", this.HeightEdit.Text);
                }
            }
            SheerResponse.SetReturnValue(true);
        }

        protected override void OK_Click()
        {
            var xmlValue = this.XmlValue;
            Assert.IsNotNull(xmlValue, "XmlValue");
            xmlValue.SetAttribute("alt", this.Alt.Text);
            xmlValue.SetAttribute("height", this.HeightEdit.Text);
            xmlValue.SetAttribute("width", this.WidthEdit.Text);
            xmlValue.SetAttribute("hspace", this.HSpace.Text);
            xmlValue.SetAttribute("vspace", this.VSpace.Text);
            xmlValue.SetAttribute("params", this.AdditionalParameters.Text);
            SheerResponse.SetDialogValue(xmlValue.ToString());
            base.OK_Click();
        }


        protected override void OnLoad(EventArgs e)
        {
            Assert.ArgumentNotNull(e, nameof(e));
            base.OnLoad(e);

            if (XamlControl.AjaxScriptManager.IsEvent)
            {
                return;
            }

            this.ImageWidth = 0;
            this.ImageHeight = 0;

            var externalSource = WebUtil.GetQueryString("source");

            if (externalSource.NotEmpty())
            {
                var urlHandle = UrlHandle.Get();
                var xmlValue = new XmlValue(urlHandle["xmlvalue"], "image");
                this.XmlValue = xmlValue;
                this.Alt.Text = xmlValue.GetAttribute("alt");
                this.HeightEdit.Text = xmlValue.GetAttribute("height");
                this.WidthEdit.Text = xmlValue.GetAttribute("width");
                this.HSpace.Text = xmlValue.GetAttribute("hspace");
                this.VSpace.Text = xmlValue.GetAttribute("vspace");
                this.AdditionalParameters.Text = xmlValue.GetAttribute("params");
            }
            else
            {
                var queryString = ItemUri.ParseQueryString();
                if (queryString == null)
                {
                    return;
                }

                var obj = Database.GetItem(queryString);
                if (obj == null)
                {
                    return;
                }

                var text = obj["Dimensions"];
                if (!string.IsNullOrEmpty(text))
                {
                    var length = text.IndexOf('x');
                    if (length >= 0)
                    {
                        this.ImageWidth = MainUtil.GetInt(StringUtil.Left(text, length).Trim(), 0);
                        this.ImageHeight = MainUtil.GetInt(StringUtil.Mid(text, length + 1).Trim(), 0);
                    }
                }

                if (this.ImageWidth <= 0 || this.ImageHeight <= 0)
                {
                    this.Aspect.Checked = false;
                    this.Aspect.Disabled = true;
                }
                else
                {
                    this.Aspect.Checked = true;
                }

                if (this.ImageWidth > 0)
                {
                    this.OriginalSize.Text = Translate.Text("Original Dimensions: {0} x {1}", this.ImageWidth, this.ImageHeight);
                }

                if (MainUtil.GetLong(obj["Size"], 0L) >= Settings.Media.MaxSizeInMemory)
                {
                    this.HeightEdit.Enabled = false;
                    this.WidthEdit.Enabled = false;
                    this.Aspect.Disabled = true;
                }
                else
                {
                    this.SizeWarning.Visible = false;
                }

                this.OriginalText.Text = StringUtil.GetString(new[] {obj["Alt"], Translate.Text("[none]")});
                var urlHandle = UrlHandle.Get();
                var xmlValue = new XmlValue(urlHandle["xmlvalue"], "image");
                this.XmlValue = xmlValue;
                this.Alt.Text = xmlValue.GetAttribute("alt");
                this.HeightEdit.Text = xmlValue.GetAttribute("height");
                this.WidthEdit.Text = xmlValue.GetAttribute("width");
                this.HSpace.Text = xmlValue.GetAttribute("hspace");
                this.VSpace.Text = xmlValue.GetAttribute("vspace");
                this.AdditionalParameters.Text = xmlValue.GetAttribute("params");

                if (MainUtil.GetBool(urlHandle["disableheight"], false))
                {
                    this.HeightEdit.Enabled = false;
                    this.Aspect.Checked = false;
                    this.Aspect.Disabled = true;
                }

                if (!MainUtil.GetBool(urlHandle["disablewidth"], false))
                {
                    return;
                }

                this.WidthEdit.Enabled = false;
                this.Aspect.Checked = false;
                this.Aspect.Disabled = true;
            }
        }
    }
}