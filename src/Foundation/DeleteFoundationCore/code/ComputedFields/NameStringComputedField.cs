﻿namespace Delete.Foundation.DeleteFoundationCore.ComputedFields
{
    using DeleteFoundationCore;

    using Sitecore.ContentSearch;
    using Sitecore.ContentSearch.ComputedFields;

    [JetBrains.Annotations.UsedImplicitly]
    public class NameStringComputedField : BaseComputedField, IComputedIndexField
    {
        public string FieldName { get; set; }

        public string ReturnType { get; set; }

        public object ComputeFieldValue(IIndexable indexable)
        {
            Guard.ArgumentNotNull(indexable, nameof(indexable));

            return this.GetItem(indexable)?.Name;
        }
    }
}