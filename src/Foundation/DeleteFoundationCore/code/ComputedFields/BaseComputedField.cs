﻿using System;
using System.Collections.Generic;
using System.Linq;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Diagnostics;
using Sitecore.Data;
using Sitecore.Data.Items;

namespace Delete.Foundation.DeleteFoundationCore.ComputedFields
{
    public class BaseComputedField
    {
        protected const string DefaultWebsite = "website";

        protected Item GetItem(IIndexable indexable, ID targetTemplateId)
        {
            return this.GetItem(indexable, new[] { targetTemplateId });
        }

        protected Item GetItem(IIndexable indexable, IEnumerable<ID> targetTemplates)
        {
            var item = this.GetItem(indexable);

            if (item != null && targetTemplates.Contains(item.TemplateID))
            {
                return item;
            }

            return null;
        }

        protected Item GetItemHavingBaseTemplate(IIndexable indexable, ID targetTemplate)
        {
            var item = this.GetItem(indexable);

            if (item != null && item.IsDerived(targetTemplate))
            {
                return item;
            }

            return null;
        }

        protected Item GetItem(IIndexable indexable)
        {
            var indexableItem = indexable as SitecoreIndexableItem;
            if (indexableItem == null)
            {
                CrawlingLog.Log.Warn(this + " : unsupported IIndexable type : " + indexable.GetType());
                return null;
            }

            var item = indexableItem.Item;
            if (item == null)
            {
                CrawlingLog.Log.Warn(this + " : unsupported SitecoreIndexableItem type : " + indexableItem.GetType());
                return null;
            }

            // optimization to reduce indexing time
            // by skipping this logic for items in the Core database
            if (string.Compare(item.Database.Name, "core", StringComparison.OrdinalIgnoreCase) == 0)
            {
                return null;
            }

            if (!item.Name.Equals(Constants.Constants.Sitecore.StandardValuesItemName, StringComparison.OrdinalIgnoreCase) &&
                !item.Name.Equals(Constants.Constants.Sitecore.BranchItemName, StringComparison.OrdinalIgnoreCase))
            {
                return item;
            }

            return null;
        }
    }
}