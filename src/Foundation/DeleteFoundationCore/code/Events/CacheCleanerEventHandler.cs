﻿using System;
using Delete.Foundation.DeleteFoundationCore.Helpers;

namespace Delete.Foundation.DeleteFoundationCore.Events
{
    public class CacheCleanerEventHandler : BaseEventHandler
    {
        public CacheCleanerEventHandler(string eventName) : base(eventName)
        {
        }

        public CacheCleanerEventHandler(params string[] eventsList) : base(eventsList)
        {
        }

        protected override void InternalHandleEvent(object sender, EventArgs args)
        {
            if (CacheManager == null)
            {
                return;
            }

            CacheManager.Clear();

            EventHelper.GetPublishingInfo(args, out var rootItem, out var database);

            if (rootItem == null) return;

            CacheManager.ClearCustomCacheByItemId(rootItem.ID);
        }
    }
}