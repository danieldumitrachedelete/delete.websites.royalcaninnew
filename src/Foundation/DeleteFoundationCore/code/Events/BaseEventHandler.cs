﻿using System;
using System.Collections.Generic;
using System.Linq;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Interfaces;

namespace Delete.Foundation.DeleteFoundationCore.Events
{
    public abstract class BaseEventHandler
    {
        public IEnumerable<string> EventsList { get; }

        public ICacheManager CacheManager { get; set; }

        protected BaseEventHandler(string eventName)
        {
            Guard.ArgumentNotEmpty(eventName, nameof(eventName));

            EventsList = new[] { eventName };
        }
        protected BaseEventHandler(params string[] eventsList)
        {
            Guard.ArgumentNotNull(eventsList, nameof(eventsList));
            if (!eventsList.Any())
            {
                throw new ArgumentException($"{nameof(eventsList)} must contain at least one value");
            }

            EventsList = eventsList;
        }

        public void OnEvent(object sender, EventArgs args)
        {
            try
            {
                InternalHandleEvent(sender, args);
            }
            catch (Exception e)
            {
                this.LogError("An error has occurred when handling an event.", e);
            }
        }

        protected abstract void InternalHandleEvent(object sender, EventArgs args);
    }
}