﻿namespace Delete.Foundation.DeleteFoundationCore.Interfaces
{
    public interface IExternalImageProcessor
    {
        string Resize(string url, int width, string additionalParameters);
        string Crop(string url, int width, int height);

        bool GetSize(ref int width, ref int height, string additionalParameters);
    }
}
