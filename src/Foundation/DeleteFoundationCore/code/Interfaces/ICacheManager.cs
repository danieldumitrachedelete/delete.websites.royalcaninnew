﻿using System;
using Sitecore.Data;

namespace Delete.Foundation.DeleteFoundationCore.Interfaces
{
    public interface ICacheManager
    {
        T Get<T>(string key) where T : class;
        void Insert<T>(T item, string key);
        void Insert<T>(T item, string key, DateTime expirationTime);
        void Insert<T>(T item, string key, TimeSpan expirationSpan);
        void Clear();
        void ClearCustomCacheByItemId(ID itemId);
    }
}