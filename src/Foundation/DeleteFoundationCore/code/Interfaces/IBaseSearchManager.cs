﻿using System;
using System.Collections.Generic;
using Delete.Foundation.DeleteFoundationCore.Models;
using Sitecore.ContentSearch;
using Sitecore.Globalization;

namespace Delete.Foundation.DeleteFoundationCore.Interfaces
{
    public interface IBaseSearchManager<out TItem> where TItem : class, IGlassBase
    {
        TItem FindItem(Guid itemGuid);

        IEnumerable<TItem> GetAll(IProviderSearchContext context = null, bool mapResults = false, bool inferType = false, bool isLazy = false, string path = null, Language language = null);

        IEnumerable<TItem> GetAllWithTemplate(IProviderSearchContext context = null, bool mapResults = false, bool inferType = false, bool isLazy = false, string path = null, Language language = null);

        IEnumerable<TItem> GetChunk(int chunkSize, int chunksToSkip = 0, IProviderSearchContext context = null, bool mapResults = false, bool inferType = false, bool isLazy = false);
    }
}
