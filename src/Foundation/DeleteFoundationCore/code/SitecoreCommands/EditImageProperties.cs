﻿using System;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.FieldTypes;
using Sitecore;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Globalization;
using Sitecore.Shell.Applications.ContentEditor;
using Sitecore.Shell.Applications.WebEdit.Commands;
using Sitecore.Shell.Framework.Commands;
using Sitecore.Text;
using Sitecore.Web;
using Sitecore.Web.UI.Sheer;

namespace Delete.Foundation.DeleteFoundationCore.SitecoreCommands
{
    [Serializable]
    public class EditImageProperties : WebEditImageCommand
    {
        private static Language ContentLanguage
        {
            get
            {
                Language result;
                if (!Language.TryParse(Context.Request.QueryString["la"], out result))
                    result = Context.ContentLanguage;
                return result;
            }
        }

        public override void Execute(CommandContext context)
        {
            Assert.ArgumentNotNull(context, nameof(context));
            WebEditCommand.ExplodeParameters(context);
            var formValue = WebUtil.GetFormValue("scPlainValue");
            context.Parameters.Add("fieldValue", formValue);
            Context.ClientPage.Start(this, "Run", context.Parameters);
        }

        protected static void Run(ClientPipelineArgs args)
        {
            Assert.ArgumentNotNull(args, nameof(args));
            var parameter1 = args.Parameters["language"];
            var language = string.IsNullOrEmpty(parameter1) ? EditImageProperties.ContentLanguage : Language.Parse(parameter1);

            var item = Context.ContentDatabase.GetItem(args.Parameters["itemid"], language);
            Assert.IsNotNull(item, typeof(Item));

            var field = item.Fields[args.Parameters["fieldid"]];
            Assert.IsNotNull(field, typeof(Field));

            var controlId = args.Parameters["controlid"];

            if (args.IsPostBack)
            {
                if (!args.HasResult)
                {
                    return;
                }

                SheerResponse.SetAttribute("scHtmlValue", "value", WebEditImageCommand.RenderImage(args, args.Result));
                SheerResponse.SetAttribute("scPlainValue", "value", args.Result);
                SheerResponse.Eval("scSetHtmlValue('" + controlId + "')");
            }
            else
            {
                var urlString = new UrlString("/sitecore/shell/~/xaml/Sitecore.Shell.Applications.Media.ImageProperties.aspx");
                var fieldValue = args.Parameters["fieldValue"];

                if (string.IsNullOrEmpty(fieldValue))
                {
                    SheerResponse.Alert("Select an image from the Media Library first.", Array.Empty<string>());
                }
                else
                {
                    var xmlValue = new XmlValue(fieldValue, "image");

                    var mediaItem = ((ImageField)field).MediaItem ?? new ImageField(field, fieldValue).MediaItem;
                    var externalSource = string.Empty;
                    if (mediaItem == null)
                    {
                        externalSource= xmlValue.GetAttribute(ExtendedImageField.ExternalSourceAttributeName);
                    }

                    if(mediaItem == null && externalSource.Empty())
                    {
                        SheerResponse.Alert("The item \"{0}\" could not be found.\n\nIt may have been deleted by another user.", Array.Empty<string>());
                    }
                    else if(mediaItem != null)
                    {
                        mediaItem.Uri.AddToUrlString(urlString);
                        var urlHandle = new UrlHandle();
                        urlHandle["xmlvalue"] = fieldValue;
                        if (!string.IsNullOrEmpty(args.Parameters["height"]) ||
                            !string.IsNullOrEmpty(args.Parameters["h"]))
                        {
                            urlHandle["disableheight"] = "true";
                        }

                        if (!string.IsNullOrEmpty(args.Parameters["width"]) ||
                            !string.IsNullOrEmpty(args.Parameters["w"]))
                        {
                            urlHandle["disablewidth"] = "true";
                        }

                        urlHandle.Add(urlString);
                        SheerResponse.ShowModalDialog(urlString.ToString(), true);
                        args.WaitForPostBack();
                    }
                    else
                    {
                        urlString["source"] = externalSource;

                        new UrlHandle()
                        {
                            ["xmlvalue"] = xmlValue.ToString()
                        }.Add(urlString);

                        SheerResponse.ShowModalDialog(urlString.ToString(), true);
                        args.WaitForPostBack();
                    }
                }
            }
        }
    }
}
