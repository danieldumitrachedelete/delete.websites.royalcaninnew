﻿using System;
using System.Collections.Specialized;
using System.Web;
using Delete.Foundation.DeleteFoundationCore.FieldTypes;
using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Globalization;
using Sitecore.Shell.Applications.ContentEditor;
using Sitecore.Shell.Applications.WebEdit.Commands;
using Sitecore.Shell.Framework.Commands;
using Sitecore.Web;
using Sitecore.Web.UI.Sheer;
using Sitecore.Web.UI.WebControls;



using Sitecore.Collections;
using Sitecore.Pipelines;
using Sitecore.Pipelines.RenderField;
using Sitecore.Xml.Xsl;
using System.ComponentModel;
using System.Web.UI;
using Sitecore.Web.UI;

namespace Delete.Foundation.DeleteFoundationCore.SitecoreCommands
{
    [Serializable]
    public class OpenExternalSource : WebEditImageCommand
    {
        public override void Execute(CommandContext context)
        {
            Assert.ArgumentNotNull(context, "context");
            WebEditCommand.ExplodeParameters(context);
            var formValue = WebUtil.GetFormValue("scPlainValue");
            context.Parameters.Add("fieldValue", formValue);
            Context.ClientPage.Start(this, "Run", context.Parameters);
        }

        protected static void Run(ClientPipelineArgs args)
        {
            Assert.ArgumentNotNull(args, "args");

            var itemNotNull = Client.GetItemNotNull(args.Parameters["itemid"], Language.Parse(args.Parameters["language"]));
            itemNotNull.Fields.ReadAll();
            var field = itemNotNull.Fields[args.Parameters["fieldid"]];

            Assert.IsNotNull(field, "field");

            var xml = args.Parameters["fieldValue"];

            var library = "/sitecore/media library";

            var source = field?.Source?.Replace(library, "");

            var db = Client.ContentDatabase.Name;
            var lang = itemNotNull.Language;

            var id = args.Parameters["fieldid"];

            var fullPath = "";

            if (xml.Length > 0)
            {
                xml = new XmlValue(xml, "image").GetAttribute("mediaid");
            }

            var path = xml;

            if (!string.IsNullOrEmpty(path))
            {
                var obj2 = Client.ContentDatabase.GetItem(path, lang);
                if (obj2 != null)
                {
                    fullPath = obj2?.Paths?.ParentPath?.Replace(library, "");
                }
            }

            var parameters = $"{{id:'{id}', isCommand: true, source:'{source}', version:'{args.Parameters["version"]}', current:'{fullPath}', db:'{db}', lang:'{lang}', command:'delete:setimage', itemid:'{args.Parameters["itemid"]}', fieldValue:'{args.Parameters["fieldValue"]}', controlid:'{args.Parameters["controlid"]}'}}";
            SheerResponse.Eval($"window.top.Sitecore.openAdvancedMediaPicker({parameters})");
        }
    }


    [Serializable]
    public class SetExternalImage : WebEditImageCommand
    {
        public override void Execute(CommandContext context)
        {
            Assert.ArgumentNotNull(context, "context");
            WebEditCommand.ExplodeParameters(context);
            var formValue = WebUtil.GetFormValue("scPlainValue");
            context.Parameters.Add("fieldValue", formValue);
            Context.ClientPage.Start(this, "Run", context.Parameters);
        }

        protected static void Run(ClientPipelineArgs args)
        {
            Assert.ArgumentNotNull(args, "args");

            var itemNotNull = Client.GetItemNotNull(args.Parameters["itemid"], Language.Parse(args.Parameters["language"]));
            itemNotNull.Fields.ReadAll();
            var field = itemNotNull.Fields[args.Parameters["fieldid"]];

            Assert.IsNotNull(field, "field");

            var imageField = new ExtendedImageField(field, field.Value);
            var parameter = args.Parameters["controlid"];

            if (args.Parameters["result"] == "undefined")
            {
                return;
            }

            string empty;

            if (!string.IsNullOrEmpty(args.Parameters["result"]))
            {
                var src = args.Parameters["result"];
                var date = args.Parameters["date"];
                var width = args.Parameters["w"];
                var height = args.Parameters["h"];

                imageField.SetAttribute(ExtendedImageField.ExternalSourceAttributeName, src);
                imageField.SetAttribute(ExtendedImageField.ExternalDateAttributeName, date);

                if (!string.IsNullOrEmpty(height))
                {
                    imageField.Height = height;
                }
                
                if (!string.IsNullOrEmpty(width))
                {
                    imageField.Width = width;
                }

                empty = imageField.Value;
            }
            else
            {
                empty = string.Empty;
            }

            SheerResponse.SetAttribute("scHtmlValue", "value", WebEditImageCommand.RenderImage(args, empty));
            SheerResponse.SetAttribute("scPlainValue", "value", empty);
            SheerResponse.Eval("scSetHtmlValue('" + parameter + "')");
        }
    }

}
