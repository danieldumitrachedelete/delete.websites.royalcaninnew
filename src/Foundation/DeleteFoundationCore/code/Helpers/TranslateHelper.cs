﻿using System;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Sitecore.DependencyInjection;
using Sitecore.Globalization;
using Microsoft.Extensions.DependencyInjection;

namespace Delete.Foundation.DeleteFoundationCore.Helpers
{
    public static class TranslateHelper
    {
        private static readonly ICacheManager _cacheManager = ServiceLocator.ServiceProvider.GetService<ICacheManager>();

        public static string TextByDomainCached(string domain, string key, params object[] parameters)
        {
            string language = Sitecore.Context.Language.Name;
            return _cacheManager.CacheResults(() => Translate.TextByDomain(domain, key, parameters), String.Format("{0}_{1}_{2}_{3}", domain, key, string.Join("_", parameters), language));
        }


        public static string TextCached(string key, params object[] parameters)
        {
            string language = Sitecore.Context.Language.Name;
            return _cacheManager.CacheResults(() => Translate.Text(key, parameters), String.Format("{0}_{1}_{2}", key, string.Join("_", parameters), language));
        }
    }
}