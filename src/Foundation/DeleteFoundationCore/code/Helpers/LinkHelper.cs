﻿using Sitecore.Globalization;
using Delete.Foundation.DeleteFoundationCore.Extensions;

namespace Delete.Foundation.DeleteFoundationCore.Helpers
{
    using Glass.Mapper.Sc.Fields;
    
    public static class LinkHelper
    {
        public static string RenderLinkTitle(Link itemLinkProperty, string itemName = "")
        {
            
            var title = !string.IsNullOrEmpty(itemLinkProperty?.Title) ? itemLinkProperty.Title : itemName;

            return itemLinkProperty?.Target != "_blank" ? title : GenerateTitleWithAccessibilityStandards(title);
        }

        public static string GetLinkClass(Link itemLinkProperty, string currentClass = "")
        {
            return itemLinkProperty?.Target != "_blank" ? currentClass : GenerateExternalLinkClass(currentClass);
        }

        private static string GenerateTitleWithAccessibilityStandards(string title)
        {
            var OpensInNewWindowString = TranslateHelper.TextByDomainCached("Translations", "opens.in.new.window");
            return title.ToLower().Contains(OpensInNewWindowString) ? title : $"{title} ({OpensInNewWindowString})";
        }

        private static string GenerateExternalLinkClass(string currentClass)
        {
            return $"{currentClass} rc-icon rc-external--xs rc-iconography rc-brand1";
        }
    }
}