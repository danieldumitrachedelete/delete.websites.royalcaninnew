window.scSendRequest = function (evt, command) {
    RemoveInlineScripts();

    var editor = scRichText.getEditor();
    if (editor.get_mode() == 2) { //If in HTML edit mode
        editor.set_mode(1); //Set mode to Design
    }
    var htmls = editor.get_html(true);
    var regex = /<p[^>]*>.*?<\/p>/i;
    var match = regex.exec(htmls);
    if (match == null && htmls != null) {
        htmls = "<p>" + htmls + "</p>";
    }

    $("EditorValue").value = htmls;

    scForm.browser.clearEvent(evt);

    scForm.postRequest("", "", "", command);

    return false;
}
