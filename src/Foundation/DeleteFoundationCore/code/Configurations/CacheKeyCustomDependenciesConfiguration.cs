﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using Sitecore.Xml;

namespace Delete.Foundation.DeleteFoundationCore.Configurations
{
    public class CacheKeyCustomDependenciesConfiguration
    {
        public Dictionary<string, string> CacheDependencies { get; }

        public CacheKeyCustomDependenciesConfiguration()
        {
            CacheDependencies = new Dictionary<string, string>();
        }

        public void AddExcludeRule(XmlNode node)
        {
            AddStringValue(CacheDependencies, node);
        }

        private void AddStringValue(Dictionary<string, string> dictionary, XmlNode node)
        {
            var key = XmlUtil.GetAttribute("cacheKey", node);
            var dependency = XmlUtil.GetAttribute("customDependency", node);
            if (!string.IsNullOrEmpty(key) && !string.IsNullOrEmpty(dependency))
            {
                dictionary.Add(key, dependency);
            }
        }
    }
}