﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using Sitecore.Xml;

namespace Delete.Foundation.DeleteFoundationCore.Configurations
{
    public class CacheCleanerKeyTypeDependenciesConfiguration
    {
        public Dictionary<string, string> TypeCacheKeyDependencies { get; }

        public CacheCleanerKeyTypeDependenciesConfiguration()
        {
            TypeCacheKeyDependencies = new Dictionary<string, string>();
        }

        public void AddCleanRule(XmlNode node)
        {
            AddStringValue(TypeCacheKeyDependencies, node);
        }

        private void AddStringValue(Dictionary<string, string> dictionary, XmlNode node)
        {
            var key = XmlUtil.GetAttribute("itemId", node);
            var dependency = XmlUtil.GetAttribute("customDependency", node);
            if (!string.IsNullOrEmpty(key) && !string.IsNullOrEmpty(dependency))
            {
                dictionary.Add(key, dependency);
            }
        }
    }
}