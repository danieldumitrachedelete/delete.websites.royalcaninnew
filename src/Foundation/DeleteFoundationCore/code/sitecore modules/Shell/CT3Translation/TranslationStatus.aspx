﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TranslationStatus.aspx.cs" Inherits="Website.sitecore_modules.Shell.CT3Translation.TranslationStatusPage" %>


<%@ Register Assembly="Sitecore.Kernel" Namespace="Sitecore.Web.UI.HtmlControls" TagPrefix="sc" %>
<%@ Register Assembly="Sitecore.Kernel" Namespace="Sitecore.Web.UI.WebControls" TagPrefix="sc" %>
<%@ Register Assembly="Sitecore.Kernel" Namespace="Sitecore.Web.UI.WebControls.Ribbons" TagPrefix="sc" %>
<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ca" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head id="Head1" runat="server">
    <title>Sitecore</title>
    <sc:Stylesheet Src="Content Manager.css" DeviceDependant="true" runat="server" />
    <sc:Stylesheet Src="Ribbon.css" DeviceDependant="true" runat="server" />
    <sc:Stylesheet Src="Grid.css" DeviceDependant="true" runat="server" />
    <sc:Script Src="/sitecore/shell/Applications/Content Manager/Content Editor.js" runat="server" />
    <style type="text/css">
        html, body {
            overflow: auto;
        }
    </style>

    <script type="text/javascript" language="javascript">
        function showMessage(msg) {
            alert(msg);
        }

        function OnResize() {
            var doc = $(document.documentElement);
            var ribbon = $("RibbonContainer");
            var grid = $("GridContainer");

            grid.style.height = doc.getHeight() - ribbon.getHeight() + 'px';
            grid.style.width = doc.getWidth() + 'px';

            cttsGrid.render();

            /* re-render again after some "magic amount of time" - without this second re-render grid doesn't pick correct width sometimes */
            setTimeout("cttsGrid.render()", 150);
        }

        function reload() {

            location.reload(true);
        }

        function refresh() {

            var job_id = document.getElementById("SelJob").value;
            var status = document.getElementById("SelStatus").value;
            var size = document.getElementById("SelItemSize").value;
            var showArchived = document.getElementById("ShowArchivedJobs");
            var isarchivedchecked = false;
            if (showArchived) {
                isarchivedchecked = showArchived.checked;
            }

            var showAllUserJobs = document.getElementById("CheckShowAllJobs");
            var isShowAllUserJobs = false;
            if (showAllUserJobs) {
                isShowAllUserJobs = showAllUserJobs.checked;
            }

            var rowId = document.getElementById("cttsGrid").value;
            scForm.postRequest("", "", "", "job:reload(jobid=" + job_id + ",status=" + status +
                ",showall=" + isShowAllUserJobs + ",showarchived=" + isarchivedchecked + ",SelItemSize=" + size + ")");

        }
        /*
          function showArchivedStatus() {
              scForm.postRequest("", "", "", "job:showarchivedjobs()");
          }
        
          function showAll() {
              scForm.postRequest("", "", "", "job:showall()");
          }
        */
        function onItemDoubleClick() {
            return scForm.invoke('ct3status:ItemDetail', event)
        }
        function onRowClick() {

        }
        window.onresize = OnResize;

    </script>

</head>
<body style="background: transparent">
    <form id="TranslationStatusForm" runat="server">
        <sc:AjaxScriptManager runat="server" />
        <sc:ContinuationManager runat="server" />

        <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td id="RibbonContainer">
                    <sc:Ribbon runat="server" ID="Ribbon" />
                </td>
            </tr>

            <tr>
                <td height="100%" id="GridCell" style="background: #e9e9e9">
                    <sc:Border runat="server" ID="GridContainer" Style="height: 100%;">
                        <sc:Border runat="server" ID="limitedVersionWarningContainer">
                            <table border="0" cellpadding="10">
                                <tr>
                                    <td>
                                        <sc:Literal runat="server" ID="labelLimitedVersionWarning" Style="color: red" Text="This is a lite version" />
                                    </td>
                                </tr>
                            </table>
                        </sc:Border>
                      
                            <ca:Grid id="cttsGrid"
                                AutoFocusSearchBox="false"
                                RunningMode="Callback"
                                CssClass="Grid"
                                FillContainer="true"
                                ShowHeader="true"
                                HeaderCssClass="GridHeader"
                                FooterCssClass="GridFooter"
                                GroupByCssClass="GroupByCell"
                                GroupByTextCssClass="GroupByText"
                                GroupBySortAscendingImageUrl="group_asc.gif"
                                GroupBySortDescendingImageUrl="group_desc.gif"
                                GroupBySortImageWidth="10"
                                GroupBySortImageHeight="10"
                                GroupingNotificationTextCssClass="GridHeaderText"
                                GroupingPageSize="5"
                                ManualPaging="false"
                                PagerStyle="Slider"                               
                                PagerTextCssClass="GridFooterText"
                                PagerButtonWidth="41"
                                PagerButtonHeight="22"
                                PagerImagesFolderUrl="/sitecore/shell/themes/standard/componentart/grid/pager/"
                                ShowSearchBox="true"
                                SearchTextCssClass="GridHeaderText"
                                SearchBoxCssClass="SearchBox"
                                SliderHeight="20"
                                SliderWidth="150"
                                SliderGripWidth="9"
                                SliderPopupOffsetX="20"                                                               
                                SliderPopupClientTemplateId="SliderTemplate"
                                TreeLineImagesFolderUrl="/sitecore/shell/themes/standard/componentart/grid/lines/"
                                TreeLineImageWidth="22"
                                TreeLineImageHeight="19"
                                PreExpandOnGroup="false"
                                ImagesBaseUrl="/sitecore/shell/themes/standard/componentart/grid/"
                                IndentCellWidth="22"
                                LoadingPanelClientTemplateId="LoadingFeedbackTemplate"
                                LoadingPanelPosition="MiddleCenter"
                                Width="100%" Height="100%" runat="server">

                                <levels>
              <ca:GridLevel
                DataKeyField="ID"
                ShowTableHeading="false" 
                ShowSelectorCells="false" 
                RowCssClass="Row" 
                ColumnReorderIndicatorImageUrl="reorder.gif"
                DataCellCssClass="DataCell" 
                HeadingCellCssClass="HeadingCell" 
                HeadingCellHoverCssClass="HeadingCellHover" 
                HeadingCellActiveCssClass="HeadingCellActive" 
                HeadingRowCssClass="HeadingRow" 
                HeadingTextCssClass="HeadingCellText"
               
                SelectedRowCssClass="SelectedRow"
                GroupHeadingCssClass="GroupHeading" 
                SortAscendingImageUrl="asc.gif" 
                SortDescendingImageUrl="desc.gif" 
                SortImageWidth="13" 
                SortImageHeight="13">  
                             
		      <Columns>
                <ca:GridColumn DataField="ID" Visible="false" />
                <ca:GridColumn DataField="ItemPath" Width="280" Visible="true" AllowGrouping="true" IsSearchable="true" SortedDataCellCssClass="SortedDataCell" HeadingText="Item Path" />
									                  <ca:GridColumn DataField="ItemTargetLanguage" Width="60" Visible="true" AllowGrouping="true" IsSearchable="false" SortedDataCellCssClass="SortedDataCell" HeadingText="Language" />
               									      <ca:GridColumn DataField="StringItemTargetVersion" Visible="true" AllowGrouping="false" IsSearchable="false" SortedDataCellCssClass="SortedDataCell" HeadingText="Version" />
									                  <ca:GridColumn DataField="TnsType" Visible="true" AllowGrouping="true" IsSearchable="false" SortedDataCellCssClass="SortedDataCell" HeadingText="Type" />
                                                      <ca:GridColumn DataField="AssetTaskProviderRef" Visible="true" AllowGrouping="true" IsSearchable="false" SortedDataCellCssClass="SortedDataCell" HeadingText="Provider Ref" />
									                  <ca:GridColumn DataField="StringItemStatus" Visible="true" AllowGrouping="true" IsSearchable="false" SortedDataCellCssClass="SortedDataCell" HeadingText="Status" /> 
									                  <ca:GridColumn DataField="CT2StatusBar" Visible="true" AllowSorting="false" AllowGrouping="false" IsSearchable="false" SortedDataCellCssClass="SortedDataCell" HeadingText="Percent" />  
									                  <%--<ca:GridColumn DataField="CreateTime" FormatString="MM/dd/yyyy" Visible="true" AllowGrouping="true" IsSearchable="false" SortedDataCellCssClass="SortedDataCell" HeadingText="Created" />--%>
                                                      <ca:GridColumn DataField="ETA" FormatString="MM/dd/yyyy" Visible="true" AllowGrouping="true" IsSearchable="false" SortedDataCellCssClass="SortedDataCell" HeadingText="Created" />
                                                      <ca:GridColumn DataField="TeamProfile" Visible="true" AllowGrouping="true" IsSearchable="false" SortedDataCellCssClass="SortedDataCell" HeadingText="Profile" />
                                    <ca:GridColumn DataField="LastUpdate" FormatString="MM/dd/yyyy" Visible="true" AllowGrouping="true" IsSearchable="false" SortedDataCellCssClass="SortedDataCell" HeadingText="Updated" /> 
                                    
               
                </Columns>
              </ca:GridLevel>
            </levels>

                                <clienttemplates>
              <ca:ClientTemplate Id="LoadingFeedbackTemplate">
                  <table cellspacing="0" cellpadding="0" border="0">
                  <tr>
                    <td style="font-size:10px;"><sc:Literal Text="Loading..." runat="server" />;</td>
                    <td><img src="/sitecore/shell/themes/standard/componentart/grid/spinner.gif" width="16" height="16" border="0"></td>
                  </tr>
                </table>
              </ca:ClientTemplate>
              
              <ca:ClientTemplate Id="SliderTemplate">
                <table class="SliderPopup" cellspacing="0" cellpadding="0" border="0">
                  <tr>
                    <td><div style="padding:4px;font:8pt tahoma;white-space:nowrap;overflow:hidden">## DataItem.GetMember('ItemPath').Value  ##</div></td>
                  </tr>
                  <tr>
                    <td style="height:14px;background-color:#666666;padding:1px 8px 1px 8px;color:white">
                    ## DataItem.PageIndex + 1 ## / ## cttsGrid.PageCount ##
                    </td>
                  </tr>
                </table>


                   <table class="SliderPopup" cellspacing="0" cellpadding="0" border="0">
                  <tr>
                    <td><div style="padding:4px;font:8pt tahoma;white-space:nowrap;overflow:hidden">## DataItem.GetMember('ItemPath').Value  ##</div></td>
                  </tr>
                  <tr>
                    <td style="height:14px;background-color:#666666;padding:1px 8px 1px 8px;color:white">
                    ## DataItem.PageIndex + 1 ## / ## cttsGrid.PageCount ##
                    </td>
                  </tr>
                </table>
                 
              </ca:ClientTemplate>
            </clienttemplates>
                            </ca:Grid>
                       

                    </sc:Border>
                </td>
            </tr>
        </table>


    </form>

</body>
</html>

