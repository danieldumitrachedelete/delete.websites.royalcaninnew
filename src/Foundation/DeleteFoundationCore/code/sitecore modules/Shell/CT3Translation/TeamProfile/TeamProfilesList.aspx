﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TeamProfilesList.aspx.cs" Inherits="SC.Site.Editor.TeamProfilesList" %>

<%@ Register Assembly="Sitecore.Kernel" Namespace="Sitecore.Web.UI.HtmlControls" TagPrefix="sc" %>
<%@ Register Assembly="Sitecore.Kernel" Namespace="Sitecore.Web.UI.WebControls" TagPrefix="sc" %>
<%@ Register Assembly="Sitecore.Kernel" Namespace="Sitecore.Web.UI.WebControls.Ribbons" TagPrefix="sc" %>
<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ca" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head runat="server">
    <sc:Stylesheet Src="Content Manager.css" DeviceDependant="true" runat="server" />
    <sc:Stylesheet Src="Ribbon.css" DeviceDependant="true" runat="server" />
    <sc:Stylesheet Src="Grid.css" DeviceDependant="true" runat="server" />
    <sc:Script Src="/sitecore/shell/Applications/Content Manager/Content Editor.js" runat="server" />

    <link href="/sitecore/shell/themes/standard/default/Default.css" rel="stylesheet">
    <link href="/sitecore/shell/controls/Lib/Flexie/flex.css" rel="stylesheet">
    <link href="/sitecore/shell/themes/standard/default/Content Manager.css" rel="stylesheet">
    <link href="/sitecore/shell/themes/standard/default/Grid.css" rel="stylesheet">



    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <title></title>
</head>
<body style="margin-left: 7%; margin-top: 5%;">
    <form id="form1" runat="server">
        <div class="row">
            <div class="col-md-3">
                <asp:Label ID="lbError" runat="server" ForeColor="Red"></asp:Label>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <h5>Team Profiles List:</h5>
            </div>
        </div>
        <div class="row" style="margin-bottom:20px">
            <div class="col-xs-8 col-sm-8 col-md-8">
                <sc:Border runat="server" ID="GridContainer" Style="height: 100%; margin-top: 20px;">
                    <ca:Grid ID="ItemsGrid"
                        AutoCallBackOnDelete="true"
                        AutoCallBackOnUpdate="true"
                        AutoCallBackOnInsert="true"
                        AutoFocusSearchBox="false"
                        RunningMode="Callback"
                        AllowPaging="false"
                        ShowFooter="false"
                        CssClass="Grid"
                        FillContainer="true"
                        ShowHeader="false"
                        HeaderCssClass="GridHeader"
                        FooterCssClass="GridFooter"
                        GroupByCssClass="GroupByCell"
                        GroupByTextCssClass="GroupByText"
                        GroupBySortAscendingImageUrl="group_asc.gif"
                        GroupBySortDescendingImageUrl="group_desc.gif"
                        GroupBySortImageWidth="10"
                        GroupBySortImageHeight="10"
                        GroupingNotificationTextCssClass="GridHeaderText"
                        ShowSearchBox="false"
                        SearchTextCssClass="GridHeaderText"
                        SearchBoxCssClass="SearchBox"
                        SliderHeight="20"
                        SliderWidth="0"
                        SliderGripWidth="9"
                        SliderPopupOffsetX="20"
                        SliderPopupClientTemplateId="SliderTemplate"
                        TreeLineImagesFolderUrl="/sitecore/shell/themes/standard/componentart/grid/lines/"
                        TreeLineImageWidth="22"
                        TreeLineImageHeight="19"
                        PreExpandOnGroup="false"
                        ImagesBaseUrl="/sitecore/shell/themes/standard/componentart/grid/"
                        IndentCellWidth="22"
                        LoadingPanelClientTemplateId="LoadingFeedbackTemplate"
                        LoadingPanelPosition="MiddleCenter"
                        Width="100%" Height="100%" runat="server">

                        <Levels>
                            <ca:GridLevel
                                DataKeyField="ID"
                                ShowTableHeading="false"
                                ShowSelectorCells="false"
                                RowCssClass="Row"
                                ColumnReorderIndicatorImageUrl="reorder.gif"
                                DataCellCssClass="DataCell"
                                HeadingCellCssClass="HeadingCell"
                                HeadingCellHoverCssClass="HeadingCellHover"
                                HeadingCellActiveCssClass="HeadingCellActive"
                                HeadingRowCssClass="HeadingRow"
                                HeadingTextCssClass="HeadingCellText"
                                SelectedRowCssClass="SelectedRow"
                                GroupHeadingCssClass="GroupHeading"
                                SortAscendingImageUrl="asc.gif"
                                SortDescendingImageUrl="desc.gif"
                                SortImageWidth="13"
                                SortImageHeight="13">
                                <Columns>
                                    <ca:GridColumn DataField="Name" Width="130" Visible="true" AllowGrouping="true" IsSearchable="true" SortedDataCellCssClass="SortedDataCell" HeadingText="Name" />
                                    <ca:GridColumn DataField="Description" Width="155" Visible="true" AllowGrouping="true" IsSearchable="false" SortedDataCellCssClass="SortedDataCell" HeadingText="Description" />
                                    <ca:GridColumn DataField="LSPs" Width="255" Visible="true" AllowGrouping="true" IsSearchable="false" SortedDataCellCssClass="SortedDataCell" HeadingText="LSPs" />
                                    <ca:GridColumn DataField="Role" Width="100" Visible="true" AllowGrouping="true" IsSearchable="false" SortedDataCellCssClass="SortedDataCell" HeadingText="Role" />
                                    <ca:GridColumn DataField="SourceLanguages" Width="255" Visible="true" AllowGrouping="true" IsSearchable="false" SortedDataCellCssClass="SortedDataCell" HeadingText="Source Languages" />
                                    <ca:GridColumn DataField="TargetLanguages" Width="255" Visible="true" AllowGrouping="true" IsSearchable="false" SortedDataCellCssClass="SortedDataCell" HeadingText="Target Languages" />
                                    <ca:GridColumn DataField="Users" Width="155" Visible="true" AllowGrouping="true" IsSearchable="false" SortedDataCellCssClass="SortedDataCell" HeadingText="Users" />
                                </Columns>

                            </ca:GridLevel>
                        </Levels>

                        <ClientTemplates>
                            <ca:ClientTemplate ID="LoadingFeedbackTemplate">
                                <table cellspacing="0" cellpadding="0" border="0">
                                    <tr>
                                        <td style="font-size: 10px;">
                                            <sc:Literal Text="Loading..." runat="server" />
                                            ;</td>
                                        <td>
                                            <img src="/sitecore/shell/themes/standard/componentart/grid/spinner.gif" width="16" height="16" border="0"></td>
                                    </tr>
                                </table>
                            </ca:ClientTemplate>

                        </ClientTemplates>
                    </ca:Grid>
                </sc:Border>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-2 col-sm-2 col-md-2">
                <h5>New profile name: </h5>
            </div>
             <div class="col-xs-3 col-sm-3 col-md-3">
                <asp:TextBox ID="NewProfileName" runat="server"></asp:TextBox>
            </div>
             <div class="col-xs-3 col-sm-3 col-md-3">
                <asp:Button id="AddProfile" runat="server"  OnClick="AddProfile_Click" Text="Add"/>
            </div>
        </div>

    </form>
</body>
</html>
