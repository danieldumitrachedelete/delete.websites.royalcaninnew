﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TeamProfileEditor.aspx.cs" Inherits="SC.Site.Editor.TeamProfileEditor" %>

<%@ Register Assembly="Sitecore.Kernel" Namespace="Sitecore.Web.UI.HtmlControls" TagPrefix="sc" %>
<%@ Register Assembly="Sitecore.Kernel" Namespace="Sitecore.Web.UI.WebControls" TagPrefix="sc" %>
<%@ Register Assembly="Sitecore.Kernel" Namespace="Sitecore.Web.UI.WebControls.Ribbons" TagPrefix="sc" %>
<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ca" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head runat="server">
    <sc:Stylesheet Src="Content Manager.css" DeviceDependant="true" runat="server" />
    <sc:Stylesheet Src="Ribbon.css" DeviceDependant="true" runat="server" />
    <sc:Stylesheet Src="Grid.css" DeviceDependant="true" runat="server" />
    <sc:Script Src="/sitecore/shell/Applications/Content Manager/Content Editor.js" runat="server" />

    <link href="/sitecore/shell/themes/standard/default/Default.css" rel="stylesheet">
    <link href="/sitecore/shell/controls/Lib/Flexie/flex.css" rel="stylesheet">
    <link href="/sitecore/shell/themes/standard/default/Content Manager.css" rel="stylesheet">
    <link href="/sitecore/shell/themes/standard/default/Grid.css" rel="stylesheet">



    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <title></title>
</head>
<body style="margin-left: 7%; margin-top: 5%;">
    <form id="form1" runat="server" method="post">
        <div class="row" style="padding-bottom: 25px;">
            <div class="col-md-6">
                <asp:Button OnClick="SaveSettings" ID="BtnSave" runat="server" Text="Save Team Profile" Width="100%" />
            </div>
        </div>

        <div class="row">
            <div class="col-md-2">
                <h5>Team Profile Role:</h5>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4" style="padding-top: 5px;">
                <asp:DropDownList ID="RoleSelector" runat="server" Width="100%" Height="100%" AutoPostBack="true">
                </asp:DropDownList>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <h5>Team Members:</h5>
            </div>
            <div class="col-xs-3 col-sm-3 col-md-3" style="padding-top: 5px;">
                <asp:ListBox ID="UsersList" runat="server" SelectionMode="Single" ViewStateMode="Disabled"></asp:ListBox>
            </div>
        </div>
        <div id="profileDetails" runat="server">
            <div class="row">
                <div class="col-md-2">
                    <h5>LSP's:</h5>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3" style="padding-top: 5px;">
                    <asp:CheckBoxList ID="ListLsp" runat="server"></asp:CheckBoxList>
                </div>
            </div>
            <div class="row" style="margin-top: 50px;">
                <div class="col-xs-3 col-sm-3 col-md-3">
                    <h4>Source Languages:</h4>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3">
                    <h4>Target Languages:</h4>
                </div>
            </div>
            <div class="row">

                <div class="col-xs-3 col-sm-3 col-md-3" style="padding-top: 5px;">
                    <asp:CheckBoxList ID="ListSrc" runat="server"></asp:CheckBoxList>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3" style="padding-top: 5px;">
                    <asp:CheckBoxList ID="ListTarget" runat="server"></asp:CheckBoxList>
                </div>
            </div>
            <div class="row" style="margin-top: 50px;">
                <div class="col-md-3">
                    <h4>Email notifications:</h4>
                </div>
            </div>
            <div class="row" style="margin-top: 20px;">
                <div class="col-md-2">
                    <h5>"Sent" notifications:</h5>
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2" style="padding-top: 5px;">
                    <asp:DropDownList ID="SendList" runat="server" Width="100%" Height="100%">
                    </asp:DropDownList>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6" style="padding-top: 5px;">
                    <asp:TextBox runat="server" ID="SentEmails"></asp:TextBox>
                </div>
            </div>
            <div class="row" style="margin-top: 20px;">
                <div class="col-md-2">
                    <h5>"Completed" notifications:</h5>
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2" style="padding-top: 5px;">
                    <asp:DropDownList ID="CompletedList" runat="server" Width="100%" Height="100%">
                    </asp:DropDownList>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6" style="padding-top: 5px;">
                    <asp:TextBox runat="server" ID="CompletedEmails"></asp:TextBox>
                </div>
            </div>
            <div class="row" style="margin-top: 20px;">
                <div class="col-md-2">
                    <h5>"Errors" notifications:</h5>
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2" style="padding-top: 5px;">
                    <asp:DropDownList ID="ErrorsList" runat="server" Width="100%" Height="100%">
                    </asp:DropDownList>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6" style="padding-top: 5px;">
                    <asp:TextBox runat="server" ID="ErrorEmails"></asp:TextBox>
                </div>
            </div>
        </div>
        <asp:HiddenField ID="showPopup" runat="server" />
    </form>
    

    <!-- Modal -->
    <div class="modal fade" id="saveModal" tabindex="-1" role="dialog" aria-labelledby="saveModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="saveModalLabel">Team Profile</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                  <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>Team Profile saved successfully.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        debugger;
        if ($('#showPopup').val() === "1") {
            $('#saveModal').modal('show');
        }
    </script>
</body>
</html>
