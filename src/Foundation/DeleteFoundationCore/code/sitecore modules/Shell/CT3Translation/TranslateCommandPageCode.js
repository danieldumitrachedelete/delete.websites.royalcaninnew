//MyCommandPageCode.js
define(["sitecore",
    "/sitecore/shell/client/Sitecore/ExperienceEditor/ExperienceEditor.js"
], function (Sitecore, ExperienceEditor) {
    // https://doc.sitecore.net/sitecore_experience_platform/content_authoring/the_editing_tools/the_experience_editor/customize_the_experience_editor_ribbon
    // will respond to //ExperienceEditor.CommandsUtil.runCommandExecute('MyCommand', this);

    Sitecore.Commands.TranslateCommand =
    {
        canExecute: function (context) {
            try
            {
                return ExperienceEditor.isInMode("edit");
            }
            catch(e)
            {
                return Sitecore.ExperienceEditor.isInMode("edit");
            }
        },

        callback: function (response) {

            var value = response.responseValue.value;
            var dialogFeatures = "dialogHeight: 620px;dialogWidth: 540px;";
            try
            {
                ExperienceEditor.Dialogs.showModalDialog(value, '', dialogFeatures, null, function (result) {

                    // possibly:
                    //scForm.invoke("CT3_Translation:pagetranslation(itemID=...&cancelState=...)");

                });

            }
            catch (e)
            {
                Sitecore.ExperienceEditor.Dialogs.showModalDialog(value, '', dialogFeatures, null, function (result) {

                    // possibly:
                    //scForm.invoke("CT3_Translation:pagetranslation(itemID=...&cancelState=...)");

                });

            }

        },

        execute: function (context) {

            // call command directly
            // scForm.postRequest("", "", "", "componentartgridexample:show", callback);

            // Call command via SPEAK command
            // and pass all the arguments we want
            //context.currentContext.value = "abc|def";
            context.currentContext.value = context.currentContext.itemId.toString() + "|" + context.currentContext.database.toString();
            try{
                ExperienceEditor.PipelinesUtil.generateRequestProcessor(
                    "pagetranslation.showFromSpeak", this.callback
                    ).execute(context);
            }
            catch (e)
            {
                Sitecore.ExperienceEditor.PipelinesUtil.generateRequestProcessor(
                "pagetranslation.showFromSpeak", this.callback
                ).execute(context);
            }
        }



    };



});

