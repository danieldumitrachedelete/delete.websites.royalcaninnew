﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreateLocalTM.aspx.cs" Inherits="SC.Site.CreateLocalTM.CreateLocalTM" %>

<%@ Register Assembly="Sitecore.Kernel" Namespace="Sitecore.Web.UI.HtmlControls" TagPrefix="sc" %>
<%@ Register Assembly="Sitecore.Kernel" Namespace="Sitecore.Web.UI.WebControls" TagPrefix="sc" %>
<%@ Register Assembly="Sitecore.Kernel" Namespace="Sitecore.Web.UI.WebControls.Ribbons" TagPrefix="sc" %>
<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ca" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head runat="server">
    <style>
        /*input[type=checkbox] {
            /* Double-sized Checkboxes */
            -ms-transform: scale(2); /* IE */
            -moz-transform: scale(2); /* FF */
            -webkit-transform: scale(2); /* Safari and Chrome */
            -o-transform: scale(2); /* Opera */
            padding: 10px;
        }*/

        .Checkboxes {
            padding: 10px;
        }

            .Checkboxes input {
                font-size: 100%;
            }

            .Checkboxes Label {
                width: 220px;
                padding: 2px;
                font-size: 100%;
                font-weight: 100;
                margin-left: 5px;
            }
    </style>
    <sc:Stylesheet Src="Content Manager.css" DeviceDependant="true" runat="server" />
    <sc:Stylesheet Src="Ribbon.css" DeviceDependant="true" runat="server" />
    <sc:Stylesheet Src="Grid.css" DeviceDependant="true" runat="server" />
    <sc:Script Src="/sitecore/shell/Applications/Content Manager/Content Editor.js" runat="server" />

    <link href="/sitecore/shell/themes/standard/default/Default.css" rel="stylesheet">
    <link href="/sitecore/shell/controls/Lib/Flexie/flex.css" rel="stylesheet">
    <link href="/sitecore/shell/themes/standard/default/Content Manager.css" rel="stylesheet">
    <link href="/sitecore/shell/themes/standard/default/Grid.css" rel="stylesheet">



    <script src="/sitecore modules/shell/CT3Translation/Lib/jquery.min.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="/sitecore modules/shell/CT3Translation/Lib/bootstrap.min.css">
    <!-- Optional theme -->
    <link rel="stylesheet" href="/sitecore modules/shell/CT3Translation/Lib/bootstrap-theme.min.css">


    <!-- Latest compiled and minified JavaScript -->
    <script src="/sitecore modules/shell/CT3Translation/Lib/bootstrap.min.js"></script>
    <title></title>

</head>
<body style="padding: 15px;">

    <form id="form1" runat="server" method="post">

        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>

        <div class="row" style="background-color: cornsilk; padding: 10px;" >
            <div class="col-md-12" style="font-weight: bold">
                <asp:Label ID="Label2" runat="server" Text="Import items to Local TM"></asp:Label>
            </div>
        </div>

        <div id="ImprtPanel" style="padding-top: 5px;">
                <div class="row" style="padding-left: 10px; padding-right: 10px;">
                    <div class="col-md-2" style="font-weight: bold;">Source Language: </div>
                    <div class="col-md-3">
                        <asp:DropDownList ID="ListSrc" runat="server"></asp:DropDownList>
                    </div>
                    <div class="col-md-4">
                        <asp:Label ID="LabelErrChooseLangs" runat="server" Font-Bold="true" ForeColor="Red" Text="Choose target language(s)" Visible="false"></asp:Label>
                    </div>
                    <div class="col-md-3" style="font-weight: bold; text-align: right">
                        <asp:LinkButton ID="LinkButton3" runat="server" OnClick="SendForTM"  OnClientClick="return checkLangs();">
                            <span aria-hidden="true" class="glyphicon glyphicon-random"></span> Import          
                        </asp:LinkButton>
                    </div>
                </div>

                <div class="row" style="padding-left: 10px; padding-right: 10px; ">
                    <div class="col-md-12" style="font-weight: bold;">Target Languages:</div>
                </div>

                <div class="row" style="padding-left: 10px; padding-right: 10px; ">
                    <div id="targetSelection" class="col-md-12">
                        <asp:CheckBoxList ID="ListTarget" runat="server" RepeatColumns="5" RepeatDirection="Vertical" CssClass="Checkboxes"></asp:CheckBoxList>
                    </div>
                </div>

            <div class="row" style="padding: 10px; font-weight: bold;">
                <div class="col-md-5">Select branches to import to Local TM
                </div>
                <div class="col-md-1">
                </div>
                <div class="col-md-6">Branches selected for import to Local TM
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <div style="border-width: 1px; border-style: solid; min-height: 300px;">
                        <asp:TreeView NodeWrap="true" ID="NodesSelection" runat="server" ExpandDepth="0" OnSelectedNodeChanged="PopulateNode" OnTreeNodeExpanded="PopulateNode" SelectedNodeStyle-BackColor="Yellow">
                        </asp:TreeView>
                        <br />
                    </div>

                </div>
                <div class="col-md-1">
                    <div style="text-align: center; height: 300px;">
                        <br />
                        <asp:LinkButton ID="LinkButton1" class="btn" runat="server" OnClick="SelectForTM">
                              <h3>  <span aria-hidden="true" class="glyphicon glyphicon-menu-right"></span>
                                  <span aria-hidden="true" class="glyphicon glyphicon-menu-right">
                                    </span></h3>
                        </asp:LinkButton>
                        <br />
                        <asp:LinkButton ID="LinkButton2" class="btn" runat="server" OnClick="RemoveSelectForTM">
                              <h3>  <span aria-hidden="true" class="glyphicon glyphicon-menu-left"></span>
                                  <span aria-hidden="true" class="glyphicon glyphicon-menu-left">
                                    </span></h3>
                        </asp:LinkButton>

                    </div>
                </div>
                <div class="col-md-6">
                    <div style="border-width: 1px; min-width: 200px; min-height: 300px;">
                        <asp:ListBox ID="ItemsForImport" runat="server" Style="min-height: 300px;"></asp:ListBox>
                    </div>
                </div>
            </div>
            <div style="padding: 5px;"></div>
        </div>
        <div class="row" style="background-color: cornsilk; padding: 10px;">
            <div class="col-md-9" style="font-weight: bold">
                <asp:Label ID="Label1" runat="server" Text="Status of items to import"></asp:Label>
            </div>
            <div class="col-md-3" style="font-weight: bold; text-align: right">
                <asp:LinkButton ID="LinkButton4" runat="server" OnClick="Refresh">
                          <span aria-hidden="true" class="glyphicon glyphicon-refresh"></span>Refresh
                </asp:LinkButton>
            </div>
        </div>

        <div style="padding-top: 5px; padding-bottom: 5px;">
            <div style="border-width: 1px; border-style: solid; min-width: 200px; height: 250px; overflow: scroll;">
                <asp:TreeView ID="tvImportStatus" runat="server" ExpandDepth="0" SelectedNodeStyle-BackColor="Yellow">
                    <Nodes>
                        <asp:TreeNode Text="Pending" SelectAction="Select"></asp:TreeNode>

                        <asp:TreeNode Text="In Progress" SelectAction="Select" />
                        <asp:TreeNode Text="Completed" SelectAction="Select" />
                    </Nodes>
                </asp:TreeView>
                <br />
            </div>
        </div>
    </form>


            <!-- Modal -->
        <div class="modal fade" id="importModal" tabindex="-1" role="dialog" aria-labelledby="removeModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>                        
                    </div>
                    <div class="modal-body" style="color:red; font-weight:bold;">
                        Please select target language(s).
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>                    
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

<script>
    var checkLangs = function () {
        var checkboxes = document.querySelectorAll('input[type="checkbox"]');
        var checkedOne = Array.prototype.slice.call(checkboxes).some(x => x.checked);
        if (checkedOne === false) {
            $('#importModal').modal('show');
        }
        return checkedOne;
    }


</script>

</body>
</html>
