﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProviderEditor.aspx.cs" Inherits="SC.Site.Editor.ProviderEditor" %>

<%@ Register Assembly="Sitecore.Kernel" Namespace="Sitecore.Web.UI.HtmlControls" TagPrefix="sc" %>
<%@ Register Assembly="Sitecore.Kernel" Namespace="Sitecore.Web.UI.WebControls" TagPrefix="sc" %>
<%@ Register Assembly="Sitecore.Kernel" Namespace="Sitecore.Web.UI.WebControls.Ribbons" TagPrefix="sc" %>
<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ca" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head runat="server">
    <sc:Stylesheet Src="Content Manager.css" DeviceDependant="true" runat="server" />
    <sc:Stylesheet Src="Ribbon.css" DeviceDependant="true" runat="server" />
    <sc:Stylesheet Src="Grid.css" DeviceDependant="true" runat="server" />
    <sc:Script Src="/sitecore/shell/Applications/Content Manager/Content Editor.js" runat="server" />

    <link href="/sitecore/shell/themes/standard/default/Default.css" rel="stylesheet">
    <link href="/sitecore/shell/controls/Lib/Flexie/flex.css" rel="stylesheet">
    <link href="/sitecore/shell/themes/standard/default/Content Manager.css" rel="stylesheet">
    <link href="/sitecore/shell/themes/standard/default/Grid.css" rel="stylesheet">



    <script src="/sitecore modules/shell/CT3Translation/Lib/jquery.min.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="/sitecore modules/shell/CT3Translation/Lib/bootstrap.min.css">
    <!-- Optional theme -->
    <link rel="stylesheet" href="/sitecore modules/shell/CT3Translation/Lib/bootstrap-theme.min.css">
    <!-- Latest compiled and minified JavaScript -->
    <script src="/sitecore modules/shell/CT3Translation/Lib/bootstrap.min.js"></script>
    <title></title>
    <script>

        function showUpgrade(e) {
            $('.nav-tabs a[href="#upgrade"]').tab('show');
        }

        function providerTypeChange(e) {
            if ($("#NewProviderType").val() === "onDemand") {
                $("#testProviderPanel").css('visibility', 'visible');
                $("#ondemand_details").collapse("show");
                $("#freeway_details").collapse("hide");
            }
            else if ($("#NewProviderType").val() === "Freeway") {
                $("#testProviderPanel").css('visibility', 'visible');
                $("#ondemand_details").collapse("hide");
                $("#freeway_details").collapse("show");
            }
            else {
                $("#testProviderPanel").css('visibility', 'hidden');
                $("#ondemand_details").collapse("hide");
                $("#freeway_details").collapse("hide");
            }
        }

    </script>
</head>
<body style="margin-left: 7%; margin-top: 5%;">
    <asp:Label runat="server" ID="lbError" ForeColor="Red"></asp:Label>
    <form id="form1" runat="server">
        <div class="row">
            <div class="col-md-2">
                <h4>License ID:</h4>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4" style="padding-top: 5px;">
                <asp:TextBox ID="LicenseID" runat="server" Width="100%" Height="100%"></asp:TextBox>
            </div>
            <div class="col-xs-1 col-sm-1 col-md-1" style="padding-top: 5px;">
                <asp:Button ID="SetLicense" Style="width: 50px;" runat="server" Text="Set" OnClick="SetLicense_Click" />

            </div>
        </div>

        <div class="row">
            <div class="col-xs-8 col-sm-8 col-md-8">
                <sc:Border runat="server" ID="GridContainer" Style="height: 100%; margin-top: 20px;">
                    <ca:Grid ID="ItemsGrid"
                        AutoCallBackOnDelete="true"
                        AutoCallBackOnUpdate="true"
                        AutoCallBackOnInsert="true"
                        AutoFocusSearchBox="false"
                        RunningMode="Callback"
                        AllowPaging="false"
                        ShowFooter="false"
                        CssClass="Grid"
                        FillContainer="true"
                        ShowHeader="false"
                        HeaderCssClass="GridHeader"
                        FooterCssClass="GridFooter"
                        GroupByCssClass="GroupByCell"
                        GroupByTextCssClass="GroupByText"
                        GroupBySortAscendingImageUrl="group_asc.gif"
                        GroupBySortDescendingImageUrl="group_desc.gif"
                        GroupBySortImageWidth="10"
                        GroupBySortImageHeight="10"
                        GroupingNotificationTextCssClass="GridHeaderText"
                        ShowSearchBox="false"
                        SearchTextCssClass="GridHeaderText"
                        SearchBoxCssClass="SearchBox"
                        SliderHeight="20"
                        SliderWidth="0"
                        SliderGripWidth="9"
                        SliderPopupOffsetX="20"
                        SliderPopupClientTemplateId="SliderTemplate"
                        TreeLineImagesFolderUrl="/sitecore/shell/themes/standard/componentart/grid/lines/"
                        TreeLineImageWidth="22"
                        TreeLineImageHeight="19"
                        PreExpandOnGroup="false"
                        ImagesBaseUrl="/sitecore/shell/themes/standard/componentart/grid/"
                        IndentCellWidth="22"
                        LoadingPanelClientTemplateId="LoadingFeedbackTemplate"
                        LoadingPanelPosition="MiddleCenter"
                        Width="100%" Height="100%" runat="server">

                        <levels>
                            <ca:GridLevel
                                DataKeyField="ID"
                                ShowTableHeading="false"
                                ShowSelectorCells="false"
                                RowCssClass="Row"
                                ColumnReorderIndicatorImageUrl="reorder.gif"
                                DataCellCssClass="DataCell"
                                HeadingCellCssClass="HeadingCell"
                                HeadingCellHoverCssClass="HeadingCellHover"
                                HeadingCellActiveCssClass="HeadingCellActive"
                                HeadingRowCssClass="HeadingRow"
                                HeadingTextCssClass="HeadingCellText"
                                SelectedRowCssClass="SelectedRow"
                                GroupHeadingCssClass="GroupHeading"
                                SortAscendingImageUrl="asc.gif"
                                SortDescendingImageUrl="desc.gif"
                                SortImageWidth="13"
                                SortImageHeight="13">
                                <Columns>
                                    <ca:GridColumn DataField="ID" Visible="false" />
                                    <ca:GridColumn DataField="Name" Width="100" Visible="true" AllowGrouping="true" IsSearchable="true" SortedDataCellCssClass="SortedDataCell" HeadingText="Provider Name" />
                                    <ca:GridColumn DataField="Type" Width="55" Visible="true" AllowGrouping="true" IsSearchable="false" SortedDataCellCssClass="SortedDataCell" HeadingText="Provider type" />
                                    <ca:GridColumn DataField="License" Width="55" Visible="true" AllowGrouping="true" IsSearchable="false" SortedDataCellCssClass="SortedDataCell" HeadingText="Account Key" />
                                    <ca:GridColumn DataField="Description" Width="155" Visible="true" AllowGrouping="true" IsSearchable="false" SortedDataCellCssClass="SortedDataCell" HeadingText="Description" />
                                </Columns>

                            </ca:GridLevel>
                        </levels>

                        <clienttemplates>
                            <ca:ClientTemplate ID="LoadingFeedbackTemplate">
                                <table cellspacing="0" cellpadding="0" border="0">
                                    <tr>
                                        <td style="font-size: 10px;">
                                            <sc:Literal Text="Loading..." runat="server" />
                                        </td>
                                        <td>
                                    </tr>
                                </table>
                            </ca:ClientTemplate>

                        </clienttemplates>
                    </ca:Grid>
                </sc:Border>

            </div>
        </div>
        <div class="row" style="margin-top: 10px;">
            <div class="col-xs-3 col-sm-3 col-md-3">
                <button id="DeleteSelectedRow" runat="server" type="button" class="btn btn-danger" data-toggle="modal" data-target="#removeModal">Remove</button>
            </div>
        </div>
        <div class="collapse in" id="collapse" runat="server">
            <div class="card card-body">
                <div class="row" style="margin-top: 10px;">
                    <div class="col-xs-10 col-sm-10 col-md-10" style="border-style: solid; border-width: 2px; padding: 10px;">

                        <ul runat="server" id="Upgrader" class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a id="new-tab" data-toggle="tab" href="#new" role="tab">New</a>
                            </li>
                            <li role="presentation">
                                <a class="nav-link" id="upgrade-tab" data-toggle="tab" href="#upgrade">Upgrade</a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane active" id="new" role="tabpanel">
                                <div class="row">
                                    <div class="col-xs-3 col-sm-3 col-md-3" style="border-width: 2px;">
                                        <h4>New provider:</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="upgrade" role="tabpanel">
                                <div class="row">
                                    <div class="col-xs-8 col-sm-8 col-md-8">
                                        <h4>Choose provider to upgrade:</h4>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-8 col-sm-8 col-md-8">
                                        <sc:Border runat="server" ID="Border1" Style="height: 100%; margin-top: 20px;">
                                            <ca:Grid ID="GridUpgrade"
                                                AutoCallBackOnDelete="true"
                                                AutoCallBackOnUpdate="true"
                                                AutoCallBackOnInsert="true"
                                                AutoFocusSearchBox="false"
                                                RunningMode="Callback"
                                                AllowPaging="false"
                                                ShowFooter="false"
                                                CssClass="Grid"
                                                FillContainer="true"
                                                ShowHeader="false"
                                                HeaderCssClass="GridHeader"
                                                FooterCssClass="GridFooter"
                                                GroupByCssClass="GroupByCell"
                                                GroupByTextCssClass="GroupByText"
                                                GroupBySortAscendingImageUrl="group_asc.gif"
                                                GroupBySortDescendingImageUrl="group_desc.gif"
                                                GroupBySortImageWidth="10"
                                                GroupBySortImageHeight="10"
                                                GroupingNotificationTextCssClass="GridHeaderText"
                                                ShowSearchBox="false"
                                                SearchTextCssClass="GridHeaderText"
                                                SearchBoxCssClass="SearchBox"
                                                SliderHeight="20"
                                                SliderWidth="0"
                                                SliderGripWidth="9"
                                                SliderPopupOffsetX="20"
                                                SliderPopupClientTemplateId="SliderTemplate"
                                                TreeLineImagesFolderUrl="/sitecore/shell/themes/standard/componentart/grid/lines/"
                                                TreeLineImageWidth="22"
                                                TreeLineImageHeight="19"
                                                PreExpandOnGroup="false"
                                                ImagesBaseUrl="/sitecore/shell/themes/standard/componentart/grid/"
                                                IndentCellWidth="22"
                                                LoadingPanelClientTemplateId="UpgradeTemplate"
                                                LoadingPanelPosition="MiddleCenter"
                                                Width="100%" Height="100%" runat="server" AutoPostBackOnSelect="true" OnSelectCommand="GridUpgrade_SelectCommand">

                                                <levels>
                                                    <ca:GridLevel
                                                        DataKeyField="ID"
                                                        ShowTableHeading="false"
                                                        ShowSelectorCells="false"
                                                        RowCssClass="Row"
                                                        ColumnReorderIndicatorImageUrl="reorder.gif"
                                                        DataCellCssClass="DataCell"
                                                        HeadingCellCssClass="HeadingCell"
                                                        HeadingCellHoverCssClass="HeadingCellHover"
                                                        HeadingCellActiveCssClass="HeadingCellActive"
                                                        HeadingRowCssClass="HeadingRow"
                                                        HeadingTextCssClass="HeadingCellText"
                                                        SelectedRowCssClass="SelectedRow"
                                                        GroupHeadingCssClass="GroupHeading"
                                                        SortAscendingImageUrl="asc.gif"
                                                        SortDescendingImageUrl="desc.gif"
                                                        SortImageWidth="13"
                                                        SortImageHeight="13">
                                                        <Columns>
                                                            <ca:GridColumn DataField="ID" Visible="false" />
                                                            <ca:GridColumn DataField="ProducerId" Width="100" Visible="true" AllowGrouping="true" IsSearchable="true" SortedDataCellCssClass="SortedDataCell" HeadingText="Provider name" />
                                                            <ca:GridColumn DataField="Type" Width="100" Visible="true" AllowGrouping="true" IsSearchable="true" SortedDataCellCssClass="SortedDataCell" HeadingText="Provider Type" />
                                                            <ca:GridColumn DataField="CanUpgrade" Width="100" Visible="true" AllowGrouping="true" IsSearchable="true" SortedDataCellCssClass="SortedDataCell" HeadingText="Can upgrade" />

                                                        </Columns>

                                                    </ca:GridLevel>
                                                </levels>

                                                <clienttemplates>
                                                    <ca:ClientTemplate ID="UpgradeTemplate">
                                                        <table cellspacing="0" cellpadding="0" border="0">
                                                            <tr>
                                                                <td style="font-size: 10px;">
                                                                    <sc:Literal Text="Loading..." runat="server" />
                                                                </td>

                                                            </tr>
                                                        </table>
                                                    </ca:ClientTemplate>

                                                </clienttemplates>
                                            </ca:Grid>
                                        </sc:Border>

                                    </div>
                                </div>


                            </div>
                        </div>

                        <div class="row" style="margin-top: 10px;">
                            <div class="col-xs-3 col-sm-3 col-md-3">
                                <h5>Name:</h5>
                            </div>
                            <div class="col-xs-5 col-sm-5 col-md-5" style="padding-top: 5px;">
                                <asp:TextBox ID="NewProviderName" runat="server" Width="100%" Height="100%"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3 col-sm-3 col-md-3">
                                <h5>Type:</h5>
                            </div>
                            <div class="col-xs-5 col-sm-5 col-md-5" style="padding-top: 5px;">
                                <asp:DropDownList ID="NewProviderType" runat="server" Width="100%" Height="100%" onchange="providerTypeChange()">
                                    <asp:ListItem Text="Generic" Selected="True" Value="Generic"></asp:ListItem>
                                    <asp:ListItem Text="onDemand" Selected="False" Value="onDemand"></asp:ListItem>
                                    <asp:ListItem Text="Freeway" Selected="False" Value="Freeway"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3 col-sm-3 col-md-3">
                                <h5>Account key:</h5>
                            </div>
                            <div class="col-xs-5 col-sm-5 col-md-5" style="padding-top: 5px;">
                                <asp:DropDownList ID="LicenseDropDown" runat="server" Width="100%" Height="100%">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3 col-sm-3 col-md-3">
                                <h5>Supports Quoting:</h5>
                            </div>
                            <div class="col-xs-5 col-sm-5 col-md-5" style="padding-top: 5px;">
                                <asp:CheckBox ID="cbQuoting" runat="server" />
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-3 col-sm-3 col-md-3">
                                <h5>PO Required:</h5>
                            </div>
                            <div class="col-xs-5 col-sm-5 col-md-5" style="padding-top: 5px;">
                                <asp:CheckBox ID="cbPORequired" runat="server" />
                            </div>
                        </div>


                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a id="usedLabel" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Keys already used </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <asp:ListView ID="BlockedList" runat="server">
                                            <LayoutTemplate>
                                                <table runat="server" id="table1">
                                                    <tr runat="server" id="itemPlaceholder"></tr>
                                                </table>
                                            </LayoutTemplate>
                                            <ItemTemplate>
                                                <tr runat="server">
                                                    <td runat="server">
                                                        <%-- Data-bound content. --%>
                                                        <asp:Label ID="NameLabel" runat="server"
                                                            Text='<%#Eval("accountKeyId") %>' />
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:ListView>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row collapse" id="freeway_details">
                            <div class="col-xs-12 col-sm-12 col-md-12 card card-body">
                                <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <h3>Freeway provider details</h3>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-xs-3 col-sm-3 col-md-3">
                                        User:
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <asp:TextBox ID="FreewayLoginTb" runat="server" Width="100%" Height="100%"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-3 col-sm-3 col-md-3">
                                        Password:
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <asp:TextBox ID="FreewayPassTb" runat="server" Width="100%" Height="100%"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-3 col-sm-3 col-md-3">
                                        API Auth URL:
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <asp:TextBox ID="FreewayApiAuthUrl" runat="server" Width="100%" Height="100%"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-3 col-sm-3 col-md-3">
                                        API Service URL:
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <asp:TextBox ID="FreewayApiServicehUrl" runat="server" Width="100%" Height="100%"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-3 col-sm-3 col-md-3">
                                        Analysis codes visibility:
                                    </div>
                                    <div class="col-xs-8 col-sm-8 col-md-8">
                                        <asp:CheckBox ID="fwShowVal1" runat="server" Style="margin-right: 10px;" Text="Require Value 1"  Checked="true"/>
                                        <asp:CheckBox ID="fwShowVal2" runat="server" Style="margin-right: 10px;" Text="Require Value 2"  Checked="true" />
                                        <asp:CheckBox ID="fwShowVal3" runat="server" Style="margin-right: 10px;" Text="Require Value 3"  Checked="true" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row collapse" id="ondemand_details">
                            <div class="col-xs-12 col-sm-12 col-md-12 card card-body">
                                <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <h3>onDemand provider details</h3>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-3 col-sm-3 col-md-3">
                                        Access Key ID:
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <asp:TextBox ID="OdApiKey" runat="server" Width="100%" Height="100%"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-3 col-sm-3 col-md-3">
                                        Access Key:
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <asp:TextBox ID="OdApiValue" runat="server" Width="100%" Height="100%"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-3 col-sm-3 col-md-3">
                                        API Endpoint:
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <asp:TextBox ID="OdApiUrl" runat="server" Width="100%" Height="100%"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="testProviderPanel">
                            <div class="row">
                                <div class="col-xs-3 col-sm-3 col-md-3">
                                    <asp:Button ID="TestProvider" Style="width: 100px;" BackColor="Blue" runat="server" Text="Test" OnClick="TestProvider_Click" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-3 col-sm-3 col-md-3">
                                    <asp:Label ID="TestResultLabel" runat="server" ForeColor="Green"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 20px;">
                            <div id="addprovider" class="col-xs-4 col-sm-4 col-md-4">
                                <asp:Button ID="AddProvider" Style="width: 150px;" runat="server" Text="Add Provider" OnClick="AddProvider_Click" />
                            </div>
                            <div id="upgradeproviderPanel" class="col-xs-4 col-sm-4 col-md-4">
                                <asp:Button ID="UpgradeProviderButton" Style="width: 150px;" runat="server" Text="Upgrade Provider" OnClick="UpgradeProvider_Click" />

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="removeModal" tabindex="-1" role="dialog" aria-labelledby="removeModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="removeModalLabel" style="color:red;">Remove provider</h4>
                    </div>
                    <div class="modal-body">
                        You are about to remove provider. Are you sure? 
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <asp:LinkButton ID="removeBtn" BackColor="Red" ForeColor="White" Style="width: 80px;" runat="server" Text="Remove" CssClass="btn btn-danger" OnClick="DeleteProvider_Click" />
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

    </form>

    <script>
        window.providerTypeChange();
        jQuery('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {

            if (e.target.id === "new-tab") {
                jQuery('#addprovider').show();
                jQuery('#upgradeproviderPanel').hide();
            }
            else {
                jQuery('#addprovider').hide();
                jQuery('#upgradeproviderPanel').show();
            }
        });

        if ($('#upgrade-tab').attr('aria-expanded') == 'true') {
            jQuery('#addprovider').hide();
            jQuery('#upgradeproviderPanel').show();
        } else {
            jQuery('#addprovider').show();
            jQuery('#upgradeproviderPanel').hide();
        }


        jQuery("#usedLabel").text("Keys already used (" + jQuery('#BlockedList_table1 tr').length + ")");


    </script>
</body>
</html>
