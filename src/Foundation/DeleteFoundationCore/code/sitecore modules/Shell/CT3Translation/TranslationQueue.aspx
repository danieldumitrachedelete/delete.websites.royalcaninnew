﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TranslationQueue.aspx.cs" Inherits="Website.sitecore_modules.Shell.CT3Translation.TranslationQueuePage" %>
<%@ Register Assembly="Sitecore.Kernel" Namespace="Sitecore.Web.UI.HtmlControls" TagPrefix="sc" %>
<%@ Register Assembly="Sitecore.Kernel" Namespace="Sitecore.Web.UI.WebControls" TagPrefix="sc" %>
<%@ Register Assembly="Sitecore.Kernel" Namespace="Sitecore.Web.UI.WebControls.Ribbons" TagPrefix="sc" %>
<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ca" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head runat="server">
    <title>Translation Queue</title>
    <sc:Stylesheet Src="Content Manager.css" DeviceDependant="true" runat="server" />
    <sc:Stylesheet Src="Ribbon.css" DeviceDependant="true" runat="server" />
    <sc:Stylesheet Src="Grid.css" DeviceDependant="true" runat="server" />
    <sc:Script Src="/sitecore/shell/Applications/Content Manager/Content Editor.js" runat="server" />
    <style type="text/css">
        html {
          overflow-y: hidden;
        }

        body {            
              overflow: auto;
              width:100%;
        }
    </style>
    <sc:Script ID="Script1" Src="/sitecore/shell/Controls/Sitecore.js" runat="server" />

    <script type="text/javascript" language="javascript">
        function onDelete() {
            ItemsGrid.scHandler.deleteSelected();
        }

        function onRecalculate() {
            this.OnRecalculate();
        }

        function onShowAll() {
            var val = document.getElementById("CheckShowAllQueue").value;
            scForm.postRequest("", "", "", "queue:show(all=" + val + ")");
        }

        function OnResize() {
            var doc = $(document.documentElement);
            var ribbon = $("RibbonContainer");
            var grid = $("GridContainer");

            grid.style.height = doc.getHeight() - ribbon.getHeight() - 22 + 'px';
            grid.style.width = doc.getWidth() + 'px';

            ItemsGrid.render();

            /* re-render again after some "magic amount of time" - without this second re-render grid doesn't pick correct width sometimes */
            setTimeout("ItemsGrid.render()", 150);
        }

        function reload() {
            /*ItemsGrid.scHandler.refresh();*/
            location.reload(true);
        }

        function refresh() {
            /* ItemsGrid.scHandler.refresh();*/
            var queuesize = document.getElementById("SelQueueItemSize").value;
            scForm.postRequest("", "", "", "ct3queue:reload(SelQueueItemSize=" + queuesize + ")");


        }

        function resetWords(wordsNumber) {
            var lb_WordsCount = $("lb_WordsCount");
            lb_WordsCount.value = wordsNumber;
        }

        window.onresize = OnResize;

    </script>

</head>
<body style="background: transparent">
    <form id="TranslationQueueForm" runat="server">
        <sc:AjaxScriptManager runat="server" />
        <sc:ContinuationManager runat="server" />

        <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td id="RibbonContainer">
                    <sc:Ribbon runat="server" ID="Ribbon" />
                </td>
            </tr>
            <tr>
                <td style="color: #000000; background: #e9e9e9" width="100%">
                    <sc:Border runat="server" ID="Border1" Style="height: 100%;" Padding="0 10 0 10">
                        <sc:Literal runat="server" ID="ltWordsCount" Text="Estimated words:" />
                        <sc:Edit runat="server" ID="lb_WordsCount" KeyDown="OnRecalculate" Style="heigth: 20px; width: 80px; border: 1px solid #CCCCCC; background-color: #FFFFFF; color: #663333; font-size: 11px;" />
                        <asp:CheckBox runat="server" ID="CheckShowAllQueue" OnCheckedChanged="OnShowAll" Style="float: right;" Text="Show queue items from all users" AutoPostBack="true" />
                    </sc:Border>
                </td>
            </tr>
            <tr>
                <td height="100%" id="GridCell" style="background: #e9e9e9">
                    <sc:Border runat="server" ID="GridContainer" Style="height: 100%;">
                        <ca:Grid ID="ItemsGrid"
                            AutoFocusSearchBox="false"
                            RunningMode="Callback"
                            CssClass="Grid"
                            FillContainer="true"
                            ShowHeader="true"
                            HeaderCssClass="GridHeader"
                            FooterCssClass="GridFooter"
                            GroupByCssClass="GroupByCell"
                            GroupByTextCssClass="GroupByText"
                            GroupBySortAscendingImageUrl="group_asc.gif"
                            GroupBySortDescendingImageUrl="group_desc.gif"
                            GroupBySortImageWidth="10"
                            GroupBySortImageHeight="10"
                            GroupingNotificationTextCssClass="GridHeaderText"
                            GroupingPageSize="5"
                            ManualPaging="true"
                            PageSize="15"
                            PagerStyle="Slider"
                            PagerTextCssClass="GridFooterText"
                            PagerButtonWidth="41"
                            PagerButtonHeight="22"
                            PagerImagesFolderUrl="/sitecore/shell/themes/standard/componentart/grid/pager/"
                            ShowSearchBox="true"
                            SearchTextCssClass="GridHeaderText"
                            SearchBoxCssClass="SearchBox"
                            SliderHeight="20"
                            SliderWidth="150"
                            SliderGripWidth="9"
                            SliderPopupOffsetX="20"
                            SliderPopupClientTemplateId="SliderTemplate"
                            TreeLineImagesFolderUrl="/sitecore/shell/themes/standard/componentart/grid/lines/"
                            TreeLineImageWidth="22"
                            TreeLineImageHeight="19"
                            PreExpandOnGroup="false"
                            ImagesBaseUrl="/sitecore/shell/themes/standard/componentart/grid/"
                            IndentCellWidth="22"
                            LoadingPanelClientTemplateId="LoadingFeedbackTemplate"
                            LoadingPanelPosition="MiddleCenter"
                            Width="100%" Height="100%" runat="server">

                            <Levels>
                                <ca:GridLevel
                                    DataKeyField="ID"
                                    ShowTableHeading="false"
                                    ShowSelectorCells="false"
                                    RowCssClass="Row"
                                    ColumnReorderIndicatorImageUrl="reorder.gif"
                                    DataCellCssClass="DataCell"
                                    HeadingCellCssClass="HeadingCell"
                                    HeadingCellHoverCssClass="HeadingCellHover"
                                    HeadingCellActiveCssClass="HeadingCellActive"
                                    HeadingRowCssClass="HeadingRow"
                                    HeadingTextCssClass="HeadingCellText"
                                    SelectedRowCssClass="SelectedRow"
                                    GroupHeadingCssClass="GroupHeading"
                                    SortAscendingImageUrl="asc.gif"
                                    SortDescendingImageUrl="desc.gif"
                                    SortImageWidth="13"
                                    SortImageHeight="13">
                                    <Columns>
                                        <ca:GridColumn DataField="ID" Visible="false" />
                                        <ca:GridColumn DataField="ItemPath" Width="330" Visible="true" AllowGrouping="true" IsSearchable="true" SortedDataCellCssClass="SortedDataCell" HeadingText="Item Path" />
                                        <ca:GridColumn DataField="ItemDatabase" Width="55" Visible="true" AllowGrouping="true" IsSearchable="false" SortedDataCellCssClass="SortedDataCell" HeadingText="Database" />
                                        <ca:GridColumn DataField="ItemSource" Width="55" Visible="true" AllowGrouping="false" IsSearchable="false" SortedDataCellCssClass="SortedDataCell" HeadingText="Language" />
                                        <ca:GridColumn DataField="ItemVersion" Width="40" Visible="true" AllowGrouping="true" IsSearchable="false" SortedDataCellCssClass="SortedDataCell" HeadingText="Version" />
                                        <ca:GridColumn DataField="ItemTargets" Width="100" Visible="true" AllowGrouping="true" IsSearchable="false" SortedDataCellCssClass="SortedDataCell" HeadingText="Translate To" />
                                        <ca:GridColumn DataField="SyncInfo" Visible="true" Width="30" AllowSorting="false" AllowGrouping="false" IsSearchable="false" SortedDataCellCssClass="SortedDataCell" HeadingText="Local TM" />
                                        <ca:GridColumn DataField="ItemDeadline" Visible="true" Width="60" AllowSorting="false" AllowGrouping="false" IsSearchable="false" SortedDataCellCssClass="SortedDataCell" HeadingText="Deadline" />
                                        <ca:GridColumn DataField="CreateTime" FormatString="MM/dd/yyyy" Width="60" Visible="true" AllowGrouping="true" IsSearchable="false" SortedDataCellCssClass="SortedDataCell" HeadingText="Created" />
                                        <ca:GridColumn DataField="WordsCount" Width="30" Visible="true" AllowGrouping="false" IsSearchable="false" SortedDataCellCssClass="SortedDataCell" HeadingText="Words" />
                                        <ca:GridColumn DataField="SitecoreUser" Width="100" Visible="true" AllowGrouping="false" IsSearchable="false" SortedDataCellCssClass="SortedDataCell" HeadingText="User" />
                                        <ca:GridColumn DataField="ItemFieldTypeString" Width="100" Visible="true" AllowGrouping="true" IsSearchable="false" SortedDataCellCssClass="SortedDataCell" HeadingText="Field Type" />
                                        <ca:GridColumn DataField="TeamProfile" Width="100" Visible="true" AllowGrouping="true" IsSearchable="false" SortedDataCellCssClass="SortedDataCell" HeadingText="Profile" />
                                    </Columns>
                                </ca:GridLevel>
                            </Levels>

                            <ClientTemplates>
                                <ca:ClientTemplate ID="LoadingFeedbackTemplate">
                                    <table cellspacing="0" cellpadding="0" border="0">
                                        <tr>
                                            <td style="font-size: 10px;">
                                                <sc:Literal Text="Loading..." runat="server" />
                                                ;</td>
                                            <td>
                                                <img src="/sitecore/shell/themes/standard/componentart/grid/spinner.gif" width="16" height="16" border="0"></td>
                                        </tr>
                                    </table>
                                </ca:ClientTemplate>

                                <ca:ClientTemplate ID="SliderTemplate">
                                    <table class="SliderPopup" cellspacing="0" cellpadding="0" border="0">
                                        <tr>
                                            <td>
                                                <div style="padding: 4px; font: 8pt tahoma; white-space: nowrap; overflow: hidden">## DataItem.GetMember('Name').Value ##</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 14px; background-color: #666666; padding: 1px 8px 1px 8px; color: white">## DataItem.PageIndex + 1 ## / ## ItemsGrid.PageCount ##
                                            </td>
                                        </tr>
                                    </table>
                                </ca:ClientTemplate>
                            </ClientTemplates>
                        </ca:Grid>
                    </sc:Border>
                </td>
            </tr>
        </table>
        <asp:HiddenField runat="server" />
    </form>
</body>
</html>
