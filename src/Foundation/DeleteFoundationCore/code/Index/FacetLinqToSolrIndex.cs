﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Xml;
using Sitecore.Abstractions;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Diagnostics;
using Sitecore.ContentSearch.Linq;
using Sitecore.ContentSearch.Linq.Common;
using Sitecore.ContentSearch.Linq.Methods;
using Sitecore.ContentSearch.Linq.Nodes;
using Sitecore.ContentSearch.Linq.Solr;
using Sitecore.ContentSearch.Pipelines.GetFacets;
using Sitecore.ContentSearch.Pipelines.ProcessFacets;
using Sitecore.ContentSearch.SolrProvider;
using Sitecore.ContentSearch.SolrProvider.Logging;
using Sitecore.ContentSearch.Utilities;
using Sitecore.Diagnostics;
using SolrNet;
using SolrNet.Commands.Parameters;
using SolrNet.Exceptions;

namespace Delete.Foundation.DeleteFoundationCore.Index
{
    public class FacetLinqToSolrIndex<TItem> : LinqToSolrIndex<TItem>
    {
#pragma warning disable 612, 618
        private readonly SolrSearchContext context;

        private readonly IContentSearchConfigurationSettings contentSearchSettings;

        private readonly ICorePipeline pipeline;

        public FacetLinqToSolrIndex(SolrSearchContext context, IExecutionContext[] executionContexts) : base(context, executionContexts)
        {
            IEnumerable<CultureExecutionContext> cultureExecutionContexts;
            Assert.ArgumentNotNull(context, "context");
            this.context = context;
            this.contentSearchSettings = context.Index.Locator.GetInstance<IContentSearchConfigurationSettings>();
            this.pipeline = context.Index.Locator.GetInstance<ICorePipeline>();
            if (executionContexts == null)
            {
                cultureExecutionContexts = null;
            }
            else
            {
                cultureExecutionContexts = executionContexts.OfType<CultureExecutionContext>();
            }
            this.UpdateFieldNameTranslatorCultureContext(cultureExecutionContexts);
        }

        private void UpdateFieldNameTranslatorCultureContext(IEnumerable<CultureExecutionContext> cultureExecutionContext)
        {
            if (cultureExecutionContext == null)
                return;
            CultureExecutionContext executionContext1 = cultureExecutionContext.FirstOrDefault<CultureExecutionContext>((Func<CultureExecutionContext, bool>)(executionContext => executionContext.PredicateType != CulturePredicateType.Not));
            if (executionContext1 != null)
                ((SolrFieldNameTranslator)this.Parameters.FieldNameTranslator).AddCultureContext(executionContext1.Culture);
            else
                ((SolrFieldNameTranslator)this.Parameters.FieldNameTranslator).ResetCultureContext();
        }

        public override TResult Execute<TResult>(SolrCompositeQuery compositeQuery)
        {
            if (EnumerableLinq.ShouldExecuteEnumerableLinqQuery((IQuery)compositeQuery))
                return EnumerableLinq.ExecuteEnumerableLinqQuery<TResult>((IQuery)compositeQuery);
            if (typeof(TResult).IsGenericType && typeof(TResult).GetGenericTypeDefinition() == typeof(SearchResults<>))
            {
                Type genericArgument = typeof(TResult).GetGenericArguments()[0];
                SolrQueryResults<Dictionary<string, object>> solrQueryResults = this.Execute(compositeQuery, genericArgument);
                Type type = typeof(SolrSearchResults<>).MakeGenericType(genericArgument);
                MethodInfo methodInfo = this.GetType().GetMethod("ApplyScalarMethods", BindingFlags.Instance | BindingFlags.NonPublic).MakeGenericMethod(typeof(TResult), genericArgument);
                SelectMethod selectMethod = GetSelectMethod(compositeQuery);
                object instance = ReflectionUtility.CreateInstance(type, (object)this.context, (object)solrQueryResults, (object)selectMethod, (object)compositeQuery.ExecutionContexts, (object)compositeQuery.VirtualFieldProcessors);
                return (TResult)methodInfo.Invoke((object)this, new object[3]
                {
          (object) compositeQuery,
          instance,
          (object) solrQueryResults
                });
            }
            if (typeof(TResult) == typeof(SolrQueryResults<Dictionary<string, object>>))
                return (TResult)System.Convert.ChangeType((object)this.Execute(compositeQuery, typeof(SearchResults<>)), typeof(TResult));

            compositeQuery.Methods.Add(new GetFacetsMethod());

            SolrQueryResults<Dictionary<string, object>> solrQueryResults1 = this.Execute(compositeQuery, typeof(TResult));
            SelectMethod selectMethod1 = GetSelectMethod(compositeQuery);
            SolrSearchResults<TResult> processedResults = new SolrSearchResults<TResult>(this.context, solrQueryResults1, selectMethod1, (IEnumerable<IExecutionContext>)compositeQuery.ExecutionContexts, (IEnumerable<IFieldQueryTranslator>)compositeQuery.VirtualFieldProcessors);
            return this.ApplyScalarMethods<TResult, TResult>(compositeQuery, processedResults, solrQueryResults1);
        }

        internal SolrQueryResults<Dictionary<string, object>> Execute(SolrCompositeQuery compositeQuery, Type resultType)
        {
            if (!compositeQuery.Methods.Any<QueryMethod>() || compositeQuery.Methods.First<QueryMethod>().MethodType != QueryMethodType.Union)
            {
                return this.GetResult(compositeQuery, this.BuildQueryOptions(compositeQuery));
            }
            UnionMethod unionMethod = compositeQuery.Methods.First<QueryMethod>() as UnionMethod;
            dynamic innerEnumerable = unionMethod.InnerEnumerable;
            dynamic obj = innerEnumerable.Execute<SolrQueryResults<Dictionary<string, object>>>(innerEnumerable.Expression);
            dynamic outerEnumerable = unionMethod.OuterEnumerable;
            dynamic obj1 = outerEnumerable.Execute<SolrQueryResults<Dictionary<string, object>>>(outerEnumerable.Expression);
            if (obj == (dynamic)null)
            {
                dynamic solrQueryResult = obj1;
                if (solrQueryResult == null)
                {
                    solrQueryResult = new SolrQueryResults<Dictionary<string, object>>();
                }
                return (SolrQueryResults<Dictionary<string, object>>)solrQueryResult;
            }
            if (obj1 == (dynamic)null)
            {
                return (SolrQueryResults<Dictionary<string, object>>)obj;
            }
            obj1.AddRange(obj);
            return (SolrQueryResults<Dictionary<string, object>>)obj1;
        }

        private TResult ApplyScalarMethods<TResult, TDocument>(SolrCompositeQuery compositeQuery, SolrSearchResults<TDocument> processedResults, SolrQueryResults<Dictionary<string, object>> results)
        {
            QueryMethod queryMethod = compositeQuery.Methods.First<QueryMethod>();
            object obj;
            switch (queryMethod.MethodType)
            {
                case QueryMethodType.All:
                    obj = (object)true;
                    break;
                case QueryMethodType.Any:
                    obj = (object)processedResults.Any();
                    break;
                case QueryMethodType.Count:
                    obj = !ShouldRunCountOnAllDocuments(compositeQuery) ? (object)processedResults.Count() : (object)results.Count<Dictionary<string, object>>();
                    break;
                case QueryMethodType.ElementAt:
                    obj = !((ElementAtMethod)queryMethod).AllowDefaultValue ? (object)processedResults.ElementAt(((ElementAtMethod)queryMethod).Index) : (object)processedResults.ElementAtOrDefault(((ElementAtMethod)queryMethod).Index);
                    break;
                case QueryMethodType.First:
                    obj = !((FirstMethod)queryMethod).AllowDefaultValue ? (object)processedResults.First() : (object)processedResults.FirstOrDefault();
                    break;
                case QueryMethodType.Last:
                    obj = !((LastMethod)queryMethod).AllowDefaultValue ? (object)processedResults.Last() : (object)processedResults.LastOrDefault();
                    break;
                case QueryMethodType.Single:
                    obj = !((SingleMethod)queryMethod).AllowDefaultValue ? (object)processedResults.Single() : (object)processedResults.SingleOrDefault();
                    break;
                case QueryMethodType.GetResults:
                    IEnumerable<SearchHit<TDocument>> searchHits = processedResults.GetSearchHits();
                    FacetResults facetResults = this.FormatFacetResults(processedResults.GetFacets(), compositeQuery.FacetQueries);
                    obj = ReflectionUtility.CreateInstance(typeof(TResult), (object)searchHits, (object)processedResults.NumberFound, (object)facetResults);
                    break;
                case QueryMethodType.GetFacets:
                    obj = (object)this.FormatFacetResults(processedResults.GetFacets(), compositeQuery.FacetQueries);
                    break;
                default:
                    throw new InvalidOperationException("Invalid query method");
            }
            return (TResult)System.Convert.ChangeType(obj, typeof(TResult));
        }

        private static SelectMethod GetSelectMethod(SolrCompositeQuery compositeQuery)
        {
            List<SelectMethod> list = compositeQuery.Methods.Where<QueryMethod>((Func<QueryMethod, bool>)(m => m.MethodType == QueryMethodType.Select)).Select<QueryMethod, SelectMethod>((Func<QueryMethod, SelectMethod>)(m => (SelectMethod)m)).ToList<SelectMethod>();
            if (list.Count<SelectMethod>() != 1)
                return (SelectMethod)null;
            return list[0];
        }

        private static bool ShouldRunCountOnAllDocuments(SolrCompositeQuery compositeQuery)
        {
            return compositeQuery.Methods.Any<QueryMethod>((Func<QueryMethod, bool>)(i =>
            {
                if (i.MethodType != QueryMethodType.Take)
                    return i.MethodType == QueryMethodType.Skip;
                return true;
            }));
        }

        private QueryOptions BuildQueryOptions(SolrCompositeQuery compositeQuery)
        {
            int num;
            int num1;
            QueryOptions options = new QueryOptions();
            if (compositeQuery.Methods != null)
            {
                List<SelectMethod> list = compositeQuery.Methods.Where<QueryMethod>((QueryMethod m) => {
                    if (m.MethodType != QueryMethodType.Select)
                    {
                        return false;
                    }
                    return ((SelectMethod)m).FieldNames.Any<string>();
                }).Select<QueryMethod, SelectMethod>((QueryMethod m) => (SelectMethod)m).ToList<SelectMethod>();
                if (list.Any<SelectMethod>())
                {
                    foreach (string str in list.SelectMany<SelectMethod, string>((SelectMethod selectMethod) => selectMethod.FieldNames))
                    {
                        options.Fields.Add(str.ToLowerInvariant());
                    }
                    options.Fields.Add("_uniqueid");
                    options.Fields.Add("_datasource");
                }
                List<GetResultsMethod> getResultsMethods = (
                    from m in compositeQuery.Methods
                    where m.MethodType == QueryMethodType.GetResults
                    select(GetResultsMethod)m).ToList<GetResultsMethod>();
                if (getResultsMethods.Any<GetResultsMethod>())
                {
                    if (options.Fields.Count <= 0)
                    {
                        options.Fields.Add("*");
                        options.Fields.Add("score");
                    }
                    else
                    {
                        options.Fields.Add("score");
                    }
                }
                options.AddOrder(this.GetSorting(compositeQuery));
                this.GetMaxHits(compositeQuery, this.contentSearchSettings.SearchMaxResults(), out num, out num1);
                List<SkipMethod> skipMethods = (
                    from m in compositeQuery.Methods
                    where m.MethodType == QueryMethodType.Skip
                    select(SkipMethod)m).ToList<SkipMethod>();
                if (skipMethods.Any<SkipMethod>())
                {
                    options.Start = new int?(skipMethods.Sum<SkipMethod>((SkipMethod skipMethod) => skipMethod.Count));
                }
                List<TakeMethod> takeMethods = (
                    from m in compositeQuery.Methods
                    where m.MethodType == QueryMethodType.Take
                    select(TakeMethod)m).ToList<TakeMethod>();
                if (takeMethods.Any<TakeMethod>())
                {
                    options.Rows = new int?(takeMethods.Sum<TakeMethod>((TakeMethod takeMethod) => takeMethod.Count));
                }
                if (compositeQuery.Methods.Any<QueryMethod>((QueryMethod m) => m.MethodType == QueryMethodType.Count) && !ShouldRunCountOnAllDocuments(compositeQuery))
                {
                    options.Rows = new int?(0);
                }
                List<AnyMethod> anyMethods = (
                    from m in compositeQuery.Methods
                    where m.MethodType == QueryMethodType.Any
                    select(AnyMethod)m).ToList<AnyMethod>();
                if (compositeQuery.Methods.Count == 1 && anyMethods.Any<AnyMethod>())
                {
                    options.Rows = new int?(0);
                }
                List<GetFacetsMethod> getFacetsMethods = (
                    from m in compositeQuery.Methods
                    where m.MethodType == QueryMethodType.GetFacets
                    select(GetFacetsMethod)m).ToList<GetFacetsMethod>();
                if (compositeQuery.FacetQueries.Count > 0 && (getFacetsMethods.Any<GetFacetsMethod>() || getResultsMethods.Any<GetResultsMethod>()))
                {
                    foreach (FacetQuery facetQuery in GetFacetsPipeline.Run(this.pipeline, new GetFacetsArgs(null, compositeQuery.FacetQueries, this.context.Index.Configuration.VirtualFields, this.context.Index.FieldNameTranslator)).FacetQueries.ToHashSet<FacetQuery>())
                    {
                        if (!facetQuery.FieldNames.Any<string>())
                        {
                            continue;
                        }
                        int? minimumResultCount = facetQuery.MinimumResultCount;
                        if (facetQuery.FieldNames.Count<string>() == 1)
                        {
                            SolrFieldNameTranslator fieldNameTranslator = this.FieldNameTranslator as SolrFieldNameTranslator;
                            string fieldName = facetQuery.FieldNames.First<string>();
                            if (fieldNameTranslator != null && fieldName == fieldNameTranslator.StripKnownExtensions(fieldName) && this.context.Index.Configuration.FieldMap.GetFieldConfiguration(fieldName) == null)
                            {
                                fieldName = fieldNameTranslator.GetIndexFieldName(fieldName.Replace("__", "!").Replace("_", " ").Replace("!", "__"), true);
                            }

                            //
                            // Custom code start
                            //
                            if (QueryContaintField(compositeQuery.Filter, fieldName))
                            {
                                options.AddFacets((ISolrFacetQuery) new SolrFacetFieldQuery(new LocalParams {{"ex", fieldName}} + fieldName)
                                {
                                    MinCount = minimumResultCount
                                });
                            }
                            else
                            {
                                options.AddFacets((ISolrFacetQuery) new SolrFacetFieldQuery(fieldName)
                                {
                                    MinCount = minimumResultCount
                                });
                            }
                            //
                            // Custom code end
                            //
                        }
                        if (facetQuery.FieldNames.Count<string>() <= 1)
                        {
                            continue;
                        }
                        ISolrFacetQuery[] solrFacetQueryArray = new ISolrFacetQuery[1];
                        SolrFacetPivotQuery solrFacetPivotQuery = new SolrFacetPivotQuery()
                        {
                            Fields = (ICollection<string>)(new string[] { string.Join(",", facetQuery.FieldNames) }),
                            MinCount = minimumResultCount
                        };
                        solrFacetQueryArray[0] = solrFacetPivotQuery;
                        options.AddFacets(solrFacetQueryArray);
                    }
                    if (!getResultsMethods.Any<GetResultsMethod>())
                    {
                        options.Rows = new int?(0);
                    }
                }
            }
            if (compositeQuery.Filter != null)
            {
                //
                // Custom code start
                //
                foreach (var solrQuery in PopulateTagParameter(compositeQuery.Filter))
                {
                    options.AddFilterQueries(solrQuery);
                }
                //
                // Custom code end
                //
            }
            options.AddFilterQueries(new ISolrQuery[] { new SolrQueryByField("_indexname", this.context.Index.Name) });
            options.ExtraParams = new Dictionary<string, string>()
            {
                { "wt", "xml" }
            };
            return options;
        }

        //
        // Custom code start
        //
        private IEnumerable<ISolrQuery> PopulateTagParameter(ISolrQuery query)
        {
            switch (query)
            {
                case SolrQueryByField queryByField:
                    return new[] {new LocalParams {{"tag", queryByField.FieldName}} + queryByField};

                case SolrMultipleCriteriaQuery multipleCriteriaQuery:
                    if (multipleCriteriaQuery.Oper.Equals("AND", StringComparison.OrdinalIgnoreCase))
                    {
                        return multipleCriteriaQuery.Queries.SelectMany(PopulateTagParameter);
                    }
                    else
                    {
                        if (multipleCriteriaQuery.Oper.Equals("OR", StringComparison.OrdinalIgnoreCase))
                        {
                            return new[] {new LocalParams {{"tag", multipleCriteriaQuery.Queries.Cast<SolrQueryByField>().FirstOrDefault()?.FieldName}} + multipleCriteriaQuery};
                        }

                        return new[] {query};
                    }

                default:
                    return new []{query};
            }
        }

        private bool QueryContaintField(ISolrQuery query, string fieldName)
        {
            switch (query)
            {
                case SolrQueryByField queryByField:
                    return queryByField.FieldName.Equals(fieldName);

                case SolrMultipleCriteriaQuery multipleCriteriaQuery:
                    return multipleCriteriaQuery.Queries.Aggregate(false,
                        (current, singleQuery) => current || QueryContaintField(singleQuery, fieldName));

                default:
                    return false;
            }
        }
        //
        // Custom code end
        //

        private ISolrOperations<Dictionary<string, object>> SolrOperations
        {
            get
            {
                var solrSearchIndex = this.context.Index as SolrSearchIndex;

                if (solrSearchIndex != null)
                {
                    return typeof(SolrSearchIndex)
                        .GetProperty("SolrOperations", BindingFlags.NonPublic | BindingFlags.Instance)
                        .GetValue(solrSearchIndex) as ISolrOperations<Dictionary<string, object>>;
                }
                return null;
            }
        }

        private SolrQueryResults<Dictionary<string, object>> GetResult(SolrCompositeQuery compositeQuery, QueryOptions queryOptions)
        {
            SolrQueryResults<Dictionary<string, object>> solrQueryResult;
            this.AddCultureToFilterQueries(this.DeterminateCultureContexts(compositeQuery), queryOptions);
            SolrLoggingSerializer solrLoggingSerializer = new SolrLoggingSerializer();
            string str = solrLoggingSerializer.SerializeQuery(compositeQuery.Query);
            SolrSearchIndex index = this.context.Index as SolrSearchIndex;
            try
            {
                if (!queryOptions.Rows.HasValue)
                {
                    queryOptions.Rows = new int?(this.contentSearchSettings.SearchMaxResults());
                }
                string str1 = string.Concat("Solr Query - ?q=", str, "&", string.Join("&", (
                    from p in solrLoggingSerializer.GetAllParameters(queryOptions)
                    select string.Format("{0}={1}", p.Key, p.Value)).ToArray<string>()));
                SearchLog.Log.Info(str1, null);
                solrQueryResult = this.SolrOperations.Query(str, queryOptions);
            }
            catch (Exception exception1)
            {
                Exception exception = exception1;
                if (!(exception is SolrConnectionException) && !(exception is SolrNetException))
                {
                    throw;
                }
                string message = exception.Message;
                if (exception.Message.StartsWith("<?xml"))
                {
                    XmlDocument xmlDocument = new XmlDocument();
                    xmlDocument.LoadXml(exception.Message);
                    XmlNode xmlNodes = xmlDocument.SelectSingleNode("/response/lst[@name='error'][1]/str[@name='msg'][1]");
                    XmlNode xmlNodes1 = xmlDocument.SelectSingleNode("/response/lst[@name='responseHeader'][1]/lst[@name='params'][1]/str[@name='q'][1]");
                    if (xmlNodes != null && xmlNodes1 != null)
                    {
                        message = string.Format("Solr Error : [\"{0}\"] - Query attempted: [{1}]", xmlNodes.InnerText, xmlNodes1.InnerText);
                        SearchLog.Log.Error(message, null);
                        solrQueryResult = new SolrQueryResults<Dictionary<string, object>>();
                        return solrQueryResult;
                    }
                }
                Log.Error(message, exception, this);
                solrQueryResult = new SolrQueryResults<Dictionary<string, object>>();
            }
            return solrQueryResult;
        }

        private FacetResults FormatFacetResults(Dictionary<string, ICollection<KeyValuePair<string, int>>> facetResults, List<FacetQuery> facetQueries)
        {
            SolrFieldNameTranslator fieldNameTranslator = this.context.Index.FieldNameTranslator as SolrFieldNameTranslator;
            IDictionary<string, ICollection<KeyValuePair<string, int>>> dictionary = ProcessFacetsPipeline.Run(this.pipeline, new ProcessFacetsArgs(facetResults, (IEnumerable<FacetQuery>)facetQueries, (IEnumerable<FacetQuery>)facetQueries, (IDictionary<string, IVirtualFieldProcessor>)this.context.Index.Configuration.VirtualFields, (FieldNameTranslator)fieldNameTranslator));
            foreach (FacetQuery facetQuery in facetQueries)
            {
                FacetQuery originalQuery = facetQuery;
                if (originalQuery.FilterValues != null && originalQuery.FilterValues.Any<object>() && dictionary.ContainsKey(originalQuery.CategoryName))
                {
                    ICollection<KeyValuePair<string, int>> source = dictionary[originalQuery.CategoryName];
                    dictionary[originalQuery.CategoryName] = (ICollection<KeyValuePair<string, int>>)source.Where<KeyValuePair<string, int>>((Func<KeyValuePair<string, int>, bool>)(cv => originalQuery.FilterValues.Contains<object>((object)cv.Key))).ToList<KeyValuePair<string, int>>();
                }
            }
            FacetResults facetResults1 = new FacetResults();
            foreach (KeyValuePair<string, ICollection<KeyValuePair<string, int>>> keyValuePair in (IEnumerable<KeyValuePair<string, ICollection<KeyValuePair<string, int>>>>)dictionary)
            {
                if (fieldNameTranslator != null)
                {
                    string key = keyValuePair.Key;
                    string name;
                    if (key.Contains(","))
                        name = fieldNameTranslator.StripKnownExtensions((IEnumerable<string>)key.Split(new char[1]
                        {
              ','
                        }, StringSplitOptions.RemoveEmptyEntries));
                    else
                        name = fieldNameTranslator.StripKnownExtensions(key);
                    IEnumerable<FacetValue> values = keyValuePair.Value.Select<KeyValuePair<string, int>, FacetValue>((Func<KeyValuePair<string, int>, FacetValue>)(v => new FacetValue(v.Key, v.Value)));
                    facetResults1.Categories.Add(new FacetCategory(name, values));
                }
            }
            return facetResults1;
        }

        private SortOrder[] GetSorting(SolrCompositeQuery compositeQuery)
        {
            return compositeQuery.Methods.Where<QueryMethod>((Func<QueryMethod, bool>)(m => m.MethodType == QueryMethodType.OrderBy)).Select<QueryMethod, OrderByMethod>((Func<QueryMethod, OrderByMethod>)(m => (OrderByMethod)m)).Reverse<OrderByMethod>().Select<OrderByMethod, SortOrder>((Func<OrderByMethod, SortOrder>)(sf => new SortOrder(sf.Field, sf.SortDirection == SortDirection.Ascending ? Order.ASC : Order.DESC))).ToArray<SortOrder>();
        }

        private int GetMaxHits(SolrCompositeQuery query, int maxDoc, out int startIdx, out int maxHits)
        {
            List<QueryMethod> queryMethods;
            queryMethods = (query.Methods != null ? new List<QueryMethod>(query.Methods) : new List<QueryMethod>());
            queryMethods.Reverse();
            QueryMethod maxHitsModifierScalarMethod = this.GetMaxHitsModifierScalarMethod(query.Methods);
            startIdx = 0;
            int num = maxDoc;
            int num1 = num;
            int num2 = num;
            int count = 0;
            foreach (QueryMethod queryMethod in queryMethods)
            {
                QueryMethodType methodType = queryMethod.MethodType;
                if (methodType == QueryMethodType.Skip)
                {
                    int count1 = ((SkipMethod)queryMethod).Count;
                    if (count1 <= 0)
                    {
                        continue;
                    }
                    startIdx += count1;
                }
                else if (methodType == QueryMethodType.Take)
                {
                    count = ((TakeMethod)queryMethod).Count;
                    if (count > 0)
                    {
                        if (count > 1 && maxHitsModifierScalarMethod != null && maxHitsModifierScalarMethod.MethodType == QueryMethodType.First)
                        {
                            count = 1;
                        }
                        if (count > 1 && maxHitsModifierScalarMethod != null && maxHitsModifierScalarMethod.MethodType == QueryMethodType.Any)
                        {
                            count = 1;
                        }
                        if (count > 2 && maxHitsModifierScalarMethod != null && maxHitsModifierScalarMethod.MethodType == QueryMethodType.Single)
                        {
                            count = 2;
                        }
                        num = startIdx + count - 1;
                        if (num <= num1)
                        {
                            if (num1 >= num)
                            {
                                continue;
                            }
                            num1 = num;
                        }
                        else
                        {
                            num = num1;
                        }
                    }
                    else
                    {
                        int num3 = startIdx;
                        startIdx = num3 + 1;
                        num = num3;
                    }
                }
            }
            if (num2 == num)
            {
                count = -1;
                if (maxHitsModifierScalarMethod != null && maxHitsModifierScalarMethod.MethodType == QueryMethodType.First)
                {
                    count = 1;
                }
                if (maxHitsModifierScalarMethod != null && maxHitsModifierScalarMethod.MethodType == QueryMethodType.Any)
                {
                    count = 1;
                }
                if (maxHitsModifierScalarMethod != null && maxHitsModifierScalarMethod.MethodType == QueryMethodType.Single)
                {
                    count = 2;
                }
                if (count >= 0)
                {
                    num = startIdx + count - 1;
                    if (num > num1)
                    {
                        num = num1;
                    }
                    else if (num1 < num)
                    {
                        num1 = num;
                    }
                }
            }
            if (num2 == num && startIdx == 0 && maxHitsModifierScalarMethod != null && maxHitsModifierScalarMethod.MethodType == QueryMethodType.Count)
            {
                num = -1;
            }
            maxHits = count;
            return maxHits;
        }

        private QueryMethod GetMaxHitsModifierScalarMethod(List<QueryMethod> methods)
        {
            if (methods.Count == 0)
            {
                return null;
            }
            QueryMethod queryMethod = methods.First<QueryMethod>();
            switch (queryMethod.MethodType)
            {
                case QueryMethodType.Any:
                case QueryMethodType.Count:
                case QueryMethodType.First:
                case QueryMethodType.Last:
                case QueryMethodType.Single:
                    {
                        return queryMethod;
                    }
                case QueryMethodType.ElementAt:
                    {
                        return null;
                    }
                default:
                    {
                        return null;
                    }
            }
        }

        private void AddCultureToFilterQueries(List<CultureExecutionContext> cultureContexts, QueryOptions queryOptions)
        {
            IEnumerable<SolrNotQuery> source1 = cultureContexts.Where<CultureExecutionContext>((Func<CultureExecutionContext, bool>)(c => c.PredicateType == CulturePredicateType.Not)).Select<CultureExecutionContext, SolrNotQuery>((Func<CultureExecutionContext, SolrNotQuery>)(c => new SolrNotQuery((ISolrQuery)CultureToSolrField(c))));
            IEnumerable<SolrQueryByField> source2 = cultureContexts.Where<CultureExecutionContext>((Func<CultureExecutionContext, bool>)(c => c.PredicateType == CulturePredicateType.Should)).Select<CultureExecutionContext, SolrQueryByField>(new Func<CultureExecutionContext, SolrQueryByField>(CultureToSolrField));
            IEnumerable<SolrQueryByField> source3 = cultureContexts.Where<CultureExecutionContext>((Func<CultureExecutionContext, bool>)(c => c.PredicateType == CulturePredicateType.Must)).Select<CultureExecutionContext, SolrQueryByField>(new Func<CultureExecutionContext, SolrQueryByField>(CultureToSolrField));
            if (source1.Any<SolrNotQuery>())
                queryOptions.AddFilterQueries((ISolrQuery)new SolrMultipleCriteriaQuery((IEnumerable<ISolrQuery>)source1, "AND"));
            if (source2.Any<SolrQueryByField>())
                queryOptions.AddFilterQueries((ISolrQuery)new SolrMultipleCriteriaQuery((IEnumerable<ISolrQuery>)source2, "OR"));
            if (!source3.Any<SolrQueryByField>())
                return;
            queryOptions.AddFilterQueries((ISolrQuery)new SolrMultipleCriteriaQuery((IEnumerable<ISolrQuery>)source3, "AND"));
        }

        private static SolrQueryByField CultureToSolrField(CultureExecutionContext c)
        {
            return new SolrQueryByField("_language", c.Culture.TwoLetterISOLanguageName + "*")
            {
                Quoted = false
            };
        }

        private List<CultureExecutionContext> DeterminateCultureContexts(SolrCompositeQuery compositeQuery)
        {
            List<CultureExecutionContext> list = compositeQuery.ExecutionContexts.OfType<CultureExecutionContext>().ToList<CultureExecutionContext>();
            this.UpdateFieldNameTranslatorCultureContext((IEnumerable<CultureExecutionContext>)list);
            return list;
        }
#pragma warning restore 612, 618
    }
}