﻿// Decompiled with JetBrains decompiler
// Type: Sitecore.ContentSearch.SolrProvider.SolrSearchResults`1
// Assembly: Sitecore.ContentSearch.SolrProvider, Version=3.1.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BE7EE414-37B8-4CD1-A38B-B0464C8F4398
// Assembly location: D:\Websites\RoyalCanin\Sitecore\website\bin\Sitecore.ContentSearch.SolrProvider.dll

using System;
using System.Collections.Generic;
using System.Linq;
using Sitecore.Abstractions;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Linq;
using Sitecore.ContentSearch.Linq.Common;
using Sitecore.ContentSearch.Linq.Methods;
using Sitecore.ContentSearch.Pipelines.IndexingFilters;
using Sitecore.ContentSearch.Security;
using Sitecore.ContentSearch.SolrProvider;
using SolrNet;

namespace Delete.Foundation.DeleteFoundationCore.Index
{
    internal struct SolrSearchResults<TElement>
    {
#pragma warning disable 612, 618
        private readonly SolrSearchContext context;
        private readonly SolrQueryResults<Dictionary<string, object>> searchResults;
        private readonly SolrIndexConfiguration solrIndexConfiguration;
        private readonly IIndexDocumentPropertyMapper<Dictionary<string, object>> mapper;
        private readonly SelectMethod selectMethod;
        private readonly IEnumerable<IExecutionContext> executionContexts;
        private readonly IEnumerable<IFieldQueryTranslator> virtualFieldProcessors;
        private readonly int numberFound;

        public SolrSearchResults(SolrSearchContext context, SolrQueryResults<Dictionary<string, object>> searchResults, SelectMethod selectMethod, IEnumerable<IExecutionContext> executionContexts, IEnumerable<IFieldQueryTranslator> virtualFieldProcessors)
        {
            this.context = context;
            this.solrIndexConfiguration = (SolrIndexConfiguration)this.context.Index.Configuration;
            this.selectMethod = selectMethod;
            this.virtualFieldProcessors = virtualFieldProcessors;
            this.executionContexts = executionContexts;
            this.numberFound = searchResults.NumFound;
            this.searchResults = SolrSearchResults<TElement>.ApplySecurity(searchResults, context.SecurityOptions, context.Index.Locator.GetInstance<ICorePipeline>(), context.Index.Locator.GetInstance<IAccessRight>(), ref this.numberFound);
            this.mapper = (this.executionContexts != null ? this.executionContexts.FirstOrDefault<IExecutionContext>((Func<IExecutionContext, bool>)(c => c is OverrideExecutionContext<IIndexDocumentPropertyMapper<Dictionary<string, object>>>)) as OverrideExecutionContext<IIndexDocumentPropertyMapper<Dictionary<string, object>>> : (OverrideExecutionContext<IIndexDocumentPropertyMapper<Dictionary<string, object>>>)null)?.OverrideObject ?? this.solrIndexConfiguration.IndexDocumentPropertyMapper;
        }

        private static SolrQueryResults<Dictionary<string, object>> ApplySecurity(SolrQueryResults<Dictionary<string, object>> solrQueryResults, SearchSecurityOptions options, ICorePipeline pipeline, IAccessRight accessRight, ref int numberFound)
        {
            if (!options.HasFlag((Enum)SearchSecurityOptions.DisableSecurityCheck))
            {
                HashSet<Dictionary<string, object>> dictionarySet = new HashSet<Dictionary<string, object>>();
                foreach (Dictionary<string, object> dictionary in solrQueryResults.Where<Dictionary<string, object>>((Func<Dictionary<string, object>, bool>)(searchResult => searchResult != null)))
                {
                    object obj1;
                    if (dictionary.TryGetValue("_uniqueid", out obj1))
                    {
                        object obj2;
                        dictionary.TryGetValue("_datasource", out obj2);
                        if (OutboundIndexFilterPipeline.CheckItemSecurity(pipeline, accessRight, new OutboundIndexFilterArgs((string)obj1, (string)obj2)))
                        {
                            dictionarySet.Add(dictionary);
                            --numberFound;
                        }
                    }
                }
                foreach (Dictionary<string, object> dictionary in dictionarySet)
                    solrQueryResults.Remove(dictionary);
            }
            return solrQueryResults;
        }

        public TElement ElementAt(int index)
        {
            if (index < 0 || index > this.searchResults.Count)
                throw new IndexOutOfRangeException();
            return this.mapper.MapToType<TElement>(this.searchResults[index], this.selectMethod, this.virtualFieldProcessors, this.executionContexts, this.context.SecurityOptions);
        }

        public TElement ElementAtOrDefault(int index)
        {
            if (index < 0 || index > this.searchResults.Count)
                return default(TElement);
            return this.mapper.MapToType<TElement>(this.searchResults[index], this.selectMethod, this.virtualFieldProcessors, this.executionContexts, this.context.SecurityOptions);
        }

        public bool Any()
        {
            return this.numberFound > 0;
        }

        public long Count()
        {
            return (long)this.numberFound;
        }

        public TElement First()
        {
            if (this.searchResults.Count < 1)
                throw new InvalidOperationException("Sequence contains no elements");
            return this.ElementAt(0);
        }

        public TElement FirstOrDefault()
        {
            if (this.searchResults.Count < 1)
                return default(TElement);
            return this.ElementAt(0);
        }

        public TElement Last()
        {
            if (this.searchResults.Count < 1)
                throw new InvalidOperationException("Sequence contains no elements");
            return this.ElementAt(this.searchResults.Count - 1);
        }

        public TElement LastOrDefault()
        {
            if (this.searchResults.Count < 1)
                return default(TElement);
            return this.ElementAt(this.searchResults.Count - 1);
        }

        public TElement Single()
        {
            if (this.Count() < 1L)
                throw new InvalidOperationException("Sequence contains no elements");
            if (this.Count() > 1L)
                throw new InvalidOperationException("Sequence contains more than one element");
            return this.mapper.MapToType<TElement>(this.searchResults[0], this.selectMethod, this.virtualFieldProcessors, this.executionContexts, this.context.SecurityOptions);
        }

        public TElement SingleOrDefault()
        {
            if (this.Count() == 0L)
                return default(TElement);
            if (this.Count() == 1L)
                return this.mapper.MapToType<TElement>(this.searchResults[0], this.selectMethod, this.virtualFieldProcessors, this.executionContexts, this.context.SecurityOptions);
            throw new InvalidOperationException("Sequence contains more than one element");
        }

        public IEnumerable<SearchHit<TElement>> GetSearchHits()
        {
            foreach (Dictionary<string, object> searchResult in (List<Dictionary<string, object>>)this.searchResults)
            {
                float score = -1f;
                object obj;
                if (searchResult.TryGetValue("score", out obj) && obj is float)
                    score = (float)obj;
                yield return new SearchHit<TElement>(score, this.mapper.MapToType<TElement>(searchResult, this.selectMethod, this.virtualFieldProcessors, this.executionContexts, this.context.SecurityOptions));
            }
        }

        public IEnumerable<TElement> GetSearchResults()
        {
            foreach (Dictionary<string, object> searchResult in (List<Dictionary<string, object>>)this.searchResults)
                yield return this.mapper.MapToType<TElement>(searchResult, this.selectMethod, this.virtualFieldProcessors, this.executionContexts, this.context.SecurityOptions);
        }

        public Dictionary<string, ICollection<KeyValuePair<string, int>>> GetFacets()
        {
            IDictionary<string, ICollection<KeyValuePair<string, int>>> facetFields = this.searchResults.FacetFields;
            IDictionary<string, IList<Pivot>> facetPivots = this.searchResults.FacetPivots;
            Dictionary<string, ICollection<KeyValuePair<string, int>>> dictionary = facetFields.ToDictionary<KeyValuePair<string, ICollection<KeyValuePair<string, int>>>, string, ICollection<KeyValuePair<string, int>>>((KeyValuePair<string, ICollection<KeyValuePair<string, int>>> x) => x.Key, (KeyValuePair<string, ICollection<KeyValuePair<string, int>>> x) => x.Value);
            if (facetPivots.Count > 0)
            {
                foreach (KeyValuePair<string, IList<Pivot>> facetPivot in facetPivots)
                {
                    dictionary[facetPivot.Key] = this.Flatten(facetPivot.Value, string.Empty);
                }
            }
            return dictionary;
        }

        public int NumberFound
        {
            get
            {
                return this.numberFound;
            }
        }

        private ICollection<KeyValuePair<string, int>> Flatten(IEnumerable<Pivot> pivots, string parentName)
        {
            HashSet<KeyValuePair<string, int>> keyValuePairSet = new HashSet<KeyValuePair<string, int>>();
            foreach (Pivot pivot in pivots)
            {
                if (parentName != string.Empty)
                    keyValuePairSet.Add(new KeyValuePair<string, int>(parentName + "/" + pivot.Value, pivot.Count));
                if (pivot.HasChildPivots)
                    keyValuePairSet.UnionWith((IEnumerable<KeyValuePair<string, int>>)this.Flatten((IEnumerable<Pivot>)pivot.ChildPivots, pivot.Value));
            }
            return (ICollection<KeyValuePair<string, int>>)keyValuePairSet;
        }
#pragma warning restore 612, 618
    }
}
