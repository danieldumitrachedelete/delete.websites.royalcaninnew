﻿using System.Collections.Generic;

namespace Delete.Foundation.DeleteFoundationCore.Comparers
{
    using Sitecore.Data.Items;

    public class SitecoreItemEqualityComparer : IEqualityComparer<Item>
    {
        public bool Equals(Item x, Item y)
        {
            if (x == null || y == null)
            {
                return false;
            }

            return x.ID == y.ID;
        }

        public int GetHashCode(Item obj)
        {
            return obj.GetHashCode();
        }
    }
}