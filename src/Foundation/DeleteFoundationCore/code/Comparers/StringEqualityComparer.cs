﻿using System;
using System.Collections.Generic;
using Sitecore.Mvc.Extensions;

namespace Delete.Foundation.DeleteFoundationCore.Comparers
{
    public class StringEqualityComparer : IEqualityComparer<string>
    {
        private readonly StringComparison _comparison;

        public StringEqualityComparer(StringComparison comparison)
        {
            _comparison = comparison;
        }

        public bool Equals(string x, string y)
        {
            if (x.IsEmpty() || y.IsEmpty()) return false;

            return x.Equals(y, _comparison);
        }

        public int GetHashCode(string obj)
        {
            return obj.GetHashCode();
        }
    }
}