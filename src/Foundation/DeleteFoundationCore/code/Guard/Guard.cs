﻿using System;

namespace Delete.Foundation.DeleteFoundationCore
{
    using JetBrains.Annotations;

    public static class Guard
    {
        [AssertionMethod]
        public static void ArgumentNotNull(
            [AssertionCondition(AssertionConditionType.IS_NOT_NULL)] object value, 
            [NotNull] string argumentName)
        {
            if (value == null)
            {
                throw new ArgumentNullException(argumentName);
            }
        }

        public static void ArgumentNotEmpty(string value, string argumentName)
        {
            if (string.IsNullOrEmpty(value))
            {
                throw new ArgumentException("Argument is null or empty", argumentName);
            }
        }

        public static void IsTrue(bool condition, string errorMessage)
        {
            if (!condition)
            {
                throw new ArgumentException(errorMessage);
            }
        }

        public static void IsEnum(Type type, string argumentName)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            if (!type.IsEnum)
            {
                throw new ArgumentException($"Argument \"{argumentName}\" must be of type enum");
            }
        }
    }
}