﻿using System;
using System.Net;
using System.Web.Mvc;
using AutoMapper;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Glass.Mapper.Sc;
using Glass.Mapper.Sc.Web.Mvc;
using Log = Sitecore.Diagnostics.Log;

namespace Delete.Foundation.DeleteFoundationCore.Controllers
{
    public class BaseController : GlassController
    {
        protected readonly ISitecoreContext _context;
        protected readonly IMapper _mapper;
        protected readonly ICacheManager _cacheManager;
        protected string ContextLanguage => Sitecore.Context.Language.Name;

        protected const string EmptyDataSourceError = "Please select a data source for this component";

        public BaseController(ISitecoreContext sitecoreContext, IMapper mapper, ICacheManager cacheManager) : base(sitecoreContext)
        {
            _mapper = mapper;
            _cacheManager = cacheManager;
            _context = sitecoreContext;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        }

        public ActionResult RenderingError(string errorMessage, Exception exception = null)
        {
            Log.Error($"{errorMessage} Context Item: {Sitecore.Context.Item?.Paths?.FullPath}", exception, GetType());
            return View("~/Views/Shared/RenderingError.cshtml", (object)errorMessage);
        }

        protected string HostName => GetHostName();

        private string GetHostName()
        {
            var url = HttpContext.Request.Url;
            if (url != null)
            {
                var port = url.Port;
                var hostName = $"{url.Scheme}://{url.Host}";
                if (port > 0)
                {
                    hostName = string.Concat(hostName, $":{port}");
                }

                return hostName;
            }

            return string.Empty;
        }
    }
}