﻿using System;
using Sitecore.Data.Fields;
using Image = Glass.Mapper.Sc.Fields.Image;

namespace Delete.Foundation.DeleteFoundationCore.Models
{
    [Serializable]
    public class ExtendedImage : Image
    {
        public virtual DateTime DateCreated { get; set; }

        public virtual DateTime DateUpdated { get; set; }

        public virtual string AdditionalParameters { get; set; }
    }
}