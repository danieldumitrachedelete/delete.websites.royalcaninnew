﻿namespace Delete.Foundation.DeleteFoundationCore.Models
{
    using System.Text.RegularExpressions;

    using Glass.Mapper;

    public class StaticAsset
    {
        public string Path { get; set; }

        public bool IsDeferred { get; set; }

        public bool IsLocal
        {
            get
            {
                if (!this.Path.HasValue()) return false;

                return !Regex.IsMatch(this.Path, @"^((http(s)?):)?\/\/", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            }
        }
    }
}