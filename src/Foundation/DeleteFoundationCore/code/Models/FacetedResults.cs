﻿using System.Collections.Generic;

namespace Delete.Foundation.DeleteFoundationCore.Models
{
    public class FacetedResults<T>
    {
        public IList<T> Results { get; set; }
        public IDictionary<string, int> Facets { get; set; }
    }
}