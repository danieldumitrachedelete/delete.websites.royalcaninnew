﻿namespace Delete.Foundation.DeleteFoundationCore.Models
{
    using System.Collections.Generic;

    public class SearchResultItems<T>
    {
        public IEnumerable<T> Results { get; set; }

        public int TotalResults { get; set; }
    }
}