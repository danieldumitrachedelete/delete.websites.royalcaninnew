﻿using System;
using Glass.Mapper.Sc.Fields;

namespace Delete.Foundation.DeleteFoundationCore.Models
{
    [Serializable]
    public class ExtendedFile : File
    {
        public new string Src { get; set; }

        public ExtendedFile(File file)
        {
            base.Id = file.Id;
            this.Src = file.Src;
        }

        public virtual DateTime DateCreated { get; set; }
        public virtual DateTime DateUpdated { get; set; }
    }
}