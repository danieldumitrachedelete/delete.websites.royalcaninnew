﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Delete.Foundation.DeleteFoundationCore.Models
{
    public class ExternalImage
    {
        public string Title { get; set; } //	Image title.
        public string Description { get; set; } //	Image description.

        [JsonProperty("ID", NamingStrategyType = typeof(DefaultNamingStrategy))]
        public string ID { get; set; } //	Item ID. MANDATORY
        public string AlbumID { get; set; } //	ID of the album containing the item. MANDATORY for albums, not for images.
        public string Kind { get; set; } //	Item's kind. Possible values: 'image', 'album'.
        public string Src { get; set; } //	URL of the image.

        [JsonProperty("srct")]
        public string ThumbnailSrc { get; set; } //	URL of the thumbnail image.

        [JsonProperty("imgHeight")]
        public int ImgHeight { get; set; } 
        [JsonProperty("imgWidth")]
        public int ImgWidth { get; set; }

        public DateTime Date { get; set; }
        public string DateStr => Date.ToString("HH:mm");
        public string Path { get; set; }
    }
}