﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;
using Delete.Foundation.DeleteFoundationCore.Constants;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Newtonsoft.Json;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Converters;
using Sitecore.Data;

namespace Delete.Foundation.DeleteFoundationCore.Models
{
    public interface IGlassBase
    {
        string Url { get; }

        string AbsoluteUrl { get; }

        Guid Id { get; }

        string Name { get; set; }

        string DisplayName { get; set; }

        string Language { get; set; }

        int Version { get; }

        bool IsLatestVersion { get; }

        ID TemplateId { get; }

        string TemplateName { get; }

        IEnumerable<Guid> BaseTemplates { get; }

        ItemUri Uri { get; set; }

        IEnumerable<ID> Paths { get; }

        string Fullpath { get; set; }

        GlassBase Parent { get; set; }

        ID ParentId { get; set; }

        IEnumerable<IGlassBase> Children { get; set; }
            
        DateTime Created { get; set; }

        DateTime Updated { get; set; }

        DateTime? PublishedFrom { get; set; }

        DateTime? PublishedTo { get; set; }

        bool NeverPublish { get; set; }

        int Sortorder { get; set; }

        bool IsBucket { get; set; }

        bool EnableItemFallback { get; set; }
    }

    public class GlassBase : IGlassBase, ICloneable
    {
        [SitecoreInfo(SitecoreInfoType.Url)]
        [IndexField("urllink")]
        public virtual string Url { get; protected set; }

        [SitecoreInfo(SitecoreInfoType.Url, UrlOptions = SitecoreInfoUrlOptions.AlwaysIncludeServerUrl)]
        public virtual string AbsoluteUrl { get; protected set; }

        [SitecoreId]
        [TypeConverter(typeof(IndexFieldGuidValueConverter))]
        [IndexField("_group")]
        public virtual Guid Id { get; protected set; }

        [SitecoreInfo(SitecoreInfoType.Name)]
        [IndexField("_name")]
        public virtual string Name { get; set; }

        /// <summary>
        /// Use this field to sort solr results
        /// </summary>
        [SitecoreInfo(SitecoreInfoType.Name)]
        [IndexField("namestring")]
        public virtual string NameString { get; set; }

        [SitecoreInfo(SitecoreInfoType.DisplayName)]
        [IndexField("_displayname")]
        public virtual string DisplayName { get; set; }

        [SitecoreInfo(SitecoreInfoType.DisplayName)]
        [IndexField("displaynamestring")]
        public virtual string DisplayNameString { get; set; }

        [SitecoreInfo(SitecoreInfoType.Language)]
        [IndexField("_language")]
        public virtual string Language { get; set; }

        [SitecoreInfo(SitecoreInfoType.Version)]
        [IndexField("_version")]
        public virtual int Version { get; protected set; }

        [IndexField("_latestversion")]
        public virtual bool IsLatestVersion { get; protected set; }

        [SitecoreInfo(SitecoreInfoType.TemplateId)]
        [TypeConverter(typeof(IndexFieldIDValueConverter))]
        [IndexField("_template")]
        public virtual ID TemplateId { get; protected set; }

        [SitecoreInfo(SitecoreInfoType.TemplateName)]
        public virtual string TemplateName { get; set; }

        [SitecoreInfo(SitecoreInfoType.BaseTemplateIds)]
        [TypeConverter(typeof(IndexFieldGuidValueConverter))]
        [IndexField("_templates")]
        public virtual IEnumerable<Guid> BaseTemplates { get; protected set; }

        [TypeConverter(typeof(IndexFieldItemUriValueConverter))]
        [XmlIgnore]
        [IndexField("_uniqueid")]
        public virtual ItemUri Uri { get; set; }

        [TypeConverter(typeof(IndexFieldEnumerableConverter))]
        [IndexField("_path")]
        public virtual IEnumerable<ID> Paths { get; set; }

        [SitecoreInfo(SitecoreInfoType.FullPath)]
        [IndexField("_fullpath")]
        public virtual string Fullpath { get; set; }

        [SitecoreParent(InferType = true, IsLazy = true)]
        [JsonIgnore]
        public virtual GlassBase Parent { get; set; }

        [IndexField("_parent")]
        public virtual ID ParentId { get; set; }

        [SitecoreChildren(InferType = true, IsLazy = true)]
        public virtual IEnumerable<IGlassBase> Children { get; set; }

        [SitecoreField(FieldId = StandardTemplate.FieldIds.CreatedDate, FieldName = StandardTemplate.FieldNames.CreatedDate)]
        public virtual DateTime Created { get; set; }

        [SitecoreField(FieldId = StandardTemplate.FieldIds.UpdatedDate, FieldName = StandardTemplate.FieldNames.UpdatedDate)]
        public virtual DateTime Updated { get; set; }

        [SitecoreField(FieldId = StandardTemplate.FieldIds.SortOrder, FieldName = StandardTemplate.FieldNames.SortOrder)]
        public virtual int Sortorder { get; set; }

        [SitecoreField(FieldId = StandardTemplate.FieldIds.PublishDate, FieldName = StandardTemplate.FieldNames.PublishDate)]
        public virtual DateTime? PublishedFrom { get; set; }

        [SitecoreField(FieldId = StandardTemplate.FieldIds.UnpublishDate, FieldName = StandardTemplate.FieldNames.UnpublishDate)]
        public virtual DateTime? PublishedTo { get; set; }

        [SitecoreField(FieldId = StandardTemplate.FieldIds.NeverPublish, FieldName = StandardTemplate.FieldNames.NeverPublish)]
        public virtual bool NeverPublish { get; set; }

        [SitecoreField(FieldId = StandardTemplate.FieldIds.HideVersion, FieldName = StandardTemplate.FieldNames.HideVersion)]
        public virtual bool HideVersion { get; set; }

        [SitecoreField(FieldId = StandardTemplate.FieldIds.IsBucket, FieldName = StandardTemplate.FieldNames.IsBucket)]
        public virtual bool IsBucket { get; set; }

        [SitecoreField(FieldId = StandardTemplate.FieldIds.EnableItemFallback, FieldName = StandardTemplate.FieldNames.EnableItemFallback)]
        public virtual bool EnableItemFallback { get; set; }

        public virtual object Clone()
        {
            return MemberwiseClone();
        }
    }
}