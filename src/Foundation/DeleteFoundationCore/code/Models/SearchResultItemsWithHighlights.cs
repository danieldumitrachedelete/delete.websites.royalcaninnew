﻿namespace Delete.Foundation.DeleteFoundationCore.Models
{
    using System.Collections.Generic;

    using SolrNet.Impl;

    public class SearchResultItemsWithHighlights<T> : SearchResultItems<T>
    {
        public IDictionary<string, HighlightedSnippets> Highlights { get; set; }
    }
}