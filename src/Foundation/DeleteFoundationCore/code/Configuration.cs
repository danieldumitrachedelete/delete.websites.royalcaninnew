﻿using System;
using System.Configuration;

namespace Delete.Foundation.DeleteFoundationCore
{
    public static class Configuration
    {
        public static TimeSpan DefaultCacheTime => GetMinutesTimeSpanSetting(Settings.DefaultCacheTime, Defaults.DefaultCacheTime);

        public static string DefaultLanguage => GetSetting(Settings.DefaultLanguage, Defaults.DefaultLanguage);

        private static string GetSetting(string settingName, string defaultValue = null)
        {
            var settingValue = ConfigurationManager.AppSettings[settingName];

            if (string.IsNullOrEmpty(settingValue))
            {
                settingValue = defaultValue;
            }

            return settingValue;
        }

        private static int GetSetting(string settingName, int defaultValue)
        {
            var settingValue = ConfigurationManager.AppSettings[settingName];

            int parsedValue;
            if (int.TryParse(settingValue, out parsedValue))
            {
                return parsedValue;
            }

            return defaultValue;
        }

        private static double GetSetting(string settingName, double defaultValue)
        {
            var settingValue = ConfigurationManager.AppSettings[settingName];

            double parsedValue;
            if (double.TryParse(settingValue, out parsedValue))
            {
                return parsedValue;
            }

            return defaultValue;
        }

        private static TimeSpan GetMinutesTimeSpanSetting(string settingName, int defaultValue)
        {
            var settingValue = ConfigurationManager.AppSettings[settingName];

            int parsedValue;
            if (int.TryParse(settingValue, out parsedValue))
            {
                return TimeSpan.FromMinutes(parsedValue);
            }

            return TimeSpan.FromMinutes(defaultValue);
        }

        private static string GetConnectionString(string connectionName)
        {
            var connectionStringSetting = ConfigurationManager.ConnectionStrings[connectionName];
            if (connectionStringSetting != null)
            {
                return connectionStringSetting.ConnectionString;
            }

            return string.Empty;
        }

        private static bool GetSetting(string settingName, bool defaultValue)
        {
            var settingValue = ConfigurationManager.AppSettings[settingName];

            bool parsedValue;
            if (bool.TryParse(settingValue, out parsedValue))
            {
                return parsedValue;
            }

            return defaultValue;
        }

        private static string[] GetCommaSeparatedSetting(string settingName)
        {
            var settingValue = ConfigurationManager.AppSettings[settingName];

            if (!string.IsNullOrEmpty(settingValue))
            {
                return settingValue.Split(',');
            }

            return new string[0];
        }

        private static class Settings
        {
            public const string DefaultCacheTime = "DefaultCacheTime";

            public const string DefaultLanguage = "DefaultLanguage";
        }

        public static class Defaults
        {
            public const int DefaultCacheTime = 30;

            public const string DefaultLanguage = "en";
        }
    }
}