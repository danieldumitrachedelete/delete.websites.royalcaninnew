﻿namespace Delete.Foundation.DeleteFoundationCore.Constants
{
    public static class LogMessages
    {
        public const string LogStart = "<<<<<======LOG START=====>>>>>";
        public const string LogEnd = "<<<<<=======LOG END======>>>>>";
        public const string VersionInfo = "Assembly: {0} Version: {1}";
        public const string CommandLine = "Command line : {0}";
        public const string MethodIn = ">>>>>>>>>> {0}";
        public const string MethodOut = "<<<<<<<<<< {0}";
        public const string ExitCode = "Exit code: {0}";
    }
}