﻿namespace Delete.Foundation.DeleteFoundationCore.Constants
{
    public static class StandardTemplate
    {
        public static class FieldIds
        {
            public const string CreatedDate = "{25bed78c-4957-4165-998a-ca1b52f67497}";
            public const string SortOrder = "{ba3f86a2-4a1c-4d78-b63d-91c2779c1b5e}";
            public const string UpdatedDate = "{d9cf14b1-fa16-4ba6-9288-e8a174d4d522}";
            public const string PublishDate = "{86fe4f77-4d9a-4ec3-9ed9-263d03bd1965}";
            public const string UnpublishDate = "{7ead6fd6-6cf1-4aca-ac6b-b200e7bafe88}";
            public const string NeverPublish = "{9135200a-5626-4dd8-ab9d-d665b8c11748}";
            public const string HideVersion = "{b8f42732-9cb8-478d-ae95-07e25345fb0f}";
            public const string Tracking = "{b0a67b2a-8b07-4e0b-8809-69f751709806}";
            public const string IsBucket = "{d312103c-b36c-4ca5-864a-c85f9abda503}";
            public const string EnableItemFallback = "{fd4e2050-186c-4375-8b99-e8a85dd7436e}";
        }

        public static class FieldNames
        {
            public const string CreatedDate = "__Created";
            public const string SortOrder = "__Sortorder";
            public const string UpdatedDate = "__Updated";
            public const string PublishDate = "__Publish";
            public const string UnpublishDate = "__Unpublish";
            public const string NeverPublish = "__Never publish";
            public const string HideVersion = "__Hide version";
            public const string Tracking = "__Tracking";
            public const string IsBucket = "__Is Bucket";
            public const string EnableItemFallback = "__Enable item fallback";
        }
    }
}