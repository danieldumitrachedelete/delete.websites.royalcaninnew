﻿using Sitecore.Shell.Feeds.FeedTypes;

namespace Delete.Foundation.DeleteFoundationCore.Constants
{
    public static class Constants
    {
        public static class LogMessages
        {
            public const string LogStart = "<<<<<======LOG START=====>>>>>";
            public const string LogEnd = "<<<<<=======LOG END======>>>>>";
            public const string VersionInfo = "Assembly: {0} Version: {1}";
            public const string CommandLine = "Command line : {0}";
            public const string MethodIn = ">>>>>>>>>> {0}";
            public const string MethodOut = "<<<<<<<<<< {0}";
            public const string ExitCode = "Exit code: {0}";
        }

        public static class Sitecore
        {
            public const string StandardValuesItemName = "__Standard Values";
            public const string BranchItemName = "$name";
            public const string ScoreIndexFieldName = "score";
            public const string GuidReplacementRegex = @"[\{\-\}]";

            public static class Databases
            {
                public const string Core = "core";
                public const string Web = "web";
                public const string Master = "master";
            }

            public static class ItemIds
            {
                ////public static ID HomePage = ID.Parse(ItemGuids.HomePage);
            }

            public static class ItemGuids
            {
                ////public static Guid HomePage = new Guid("{110D559F-DEA5-42EA-9C1C-8A5DF7E70EF9}");
            }

            public static class TemplateIds
            {
                ////public static TemplateID Folder = new TemplateID(new ID(TemplateGuids.FolderItem));
            }

            public static class TemplateGuids
            {
                ////public static Guid FolderItem = new Guid(TemplateIdStrings.FolderItem);
            }

            public static class TemplateIdStrings
            {
                ////public const string FolderItem = "{A87A00B1-E6DB-45AB-8B54-636FEC3B5523}";
            }

            public static class MetadataMaxLength
            {
                public const int Title = 60;
            }
        }
    }
}