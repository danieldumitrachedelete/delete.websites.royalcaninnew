﻿using System;
using System.Linq;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Globalization;
using Sitecore.Shell.Applications.ContentEditor;
using Sitecore.Text;
using Sitecore.Web;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Web.UI.WebControls;

namespace Delete.Foundation.DeleteFoundationCore.FieldTypes
{
    /// <summary>
    /// This field type is like a tree list, but you can specify more than one root item to select from (for example, videos or photos)
    /// The data source roots are specified using pipe delimiting just like regular Sitecore Query language
    /// </summary>
    public class MultiRootTreeList : TreeList
    {
        private const string QueryPrefix = "query:";

        protected override void OnLoad(EventArgs args)
        {
            Assert.ArgumentNotNull(args, "args");
            base.OnLoad(args);

            if (!Sitecore.Context.ClientPage.IsEvent)
            {
                // find the existing TreeviewEx that the base OnLoad added, get a ref to its parent, and remove it from controls
                var existingTreeView = (TreeviewEx)WebUtil.FindControlOfType(this, typeof(TreeviewEx));
                var treeviewParent = existingTreeView.Parent;

                existingTreeView.Parent.Controls.Clear(); // remove stock treeviewex, we replace with multiroot

                // find the existing DataContext that the base OnLoad added, get a ref to its parent, and remove it from controls
                var dataContext = (DataContext)WebUtil.FindControlOfType(this, typeof(DataContext));
                var dataContextParent = dataContext.Parent;

                dataContextParent.Controls.Remove(dataContext); // remove stock datacontext, we parse our own

                // create our MultiRootTreeview to replace the TreeviewEx
                var impostor = new EnhancedMultiRootTreeview
                {
                    ID = existingTreeView.ID,
                    DblClick = existingTreeView.DblClick,
                    Enabled = existingTreeView.Enabled,
                    DisplayFieldName = existingTreeView.DisplayFieldName
                };

                // parse the data source and create appropriate data contexts out of it
                var dataContexts = this.ParseDataContexts(dataContext);

                impostor.DataContext = string.Join("|", dataContexts.Select(x => x.ID));
                foreach (var context in dataContexts)
                {
                    dataContextParent.Controls.Add(context);
                }

                // inject our replaced control where the TreeviewEx originally was
                treeviewParent.Controls.Add(impostor);
            }
        }

        protected virtual DataContext[] ParseDataContexts(DataContext originalDataContext)
        {
            return new ListString(this.DataSource).Select(x => this.CreateDataContext(originalDataContext, x)).ToArray();
        }

        protected virtual DataContext CreateDataContext(DataContext baseDataContext, string dataSource)
        {
            Item dataSourceItem = null;
            if (dataSource.StartsWith(QueryPrefix))
            {
                try
                {
                    var query = dataSource.Substring(QueryPrefix.Length);
                    var currentItem = Sitecore.Context.ContentDatabase.GetItem(this.ItemID);
                    var results = query.StartsWith("./", StringComparison.OrdinalIgnoreCase)
                        ? currentItem.Axes.SelectItems(query)
                        : currentItem.Database.SelectItems(query);
                    dataSourceItem = results.FirstOrDefault(item => item != null);
                    dataSource = dataSourceItem.Paths.FullPath;
                }
                catch (Exception ex)
                {
                    Log.Error($"Treelist field failed to execute query: '{dataSource}'", ex, this);
                }
            }

            DataContext dataContext = new DataContext
            {
                ID = GetUniqueID("D"),
                Filter = baseDataContext.Filter,
                DataViewName = "Master",
                Root = dataSource,
                Language = Language.Parse(this.ItemLanguage)
            };

            if (!string.IsNullOrEmpty(this.DatabaseName))
            {
                dataContext.Parameters = "databasename=" + this.DatabaseName;
            }

            return dataContext;
        }
    }
}