﻿using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Web.UI.WebControls;

namespace Delete.Foundation.DeleteFoundationCore.FieldTypes
{
    public class EnhancedMultiRootTreeview : MultiRootTreeview
    {
        protected override string GetHeaderValue(Item item)
        {
            Assert.ArgumentNotNull(item, "item");

            var nodeTitle = string.IsNullOrEmpty(this.DisplayFieldName) ? item.DisplayName : item[this.DisplayFieldName];
            nodeTitle = string.IsNullOrEmpty(nodeTitle) ? item.DisplayName : nodeTitle;

            // you can get fancy here and make the format of the nodeTitle manageable in config
            nodeTitle = $"{nodeTitle}  -  <span>({item.Paths.ContentPath})</span>";

            return nodeTitle;
        }
    }
}