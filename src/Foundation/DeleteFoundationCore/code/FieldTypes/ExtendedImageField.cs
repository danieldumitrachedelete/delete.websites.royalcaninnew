﻿using Sitecore.Data.Fields;

namespace Delete.Foundation.DeleteFoundationCore.FieldTypes
{
    public class ExtendedImageField : Sitecore.Data.Fields.ImageField
    {
        public const string ExternalSourceAttributeName = "source";
        public const string ExternalDateAttributeName = "updated";
        public const string AdditionalParametersAttributeName = "params";

        public ExtendedImageField(Field innerField) : base(innerField)
        {
        }

        public ExtendedImageField(Field innerField, string runtimeValue) : base(innerField, runtimeValue)
        {
        }

        public string ExternalSource
        {
            get
            {
                return this.GetAttribute(ExternalSourceAttributeName);
            }
            set
            {
                this.SetAttribute(ExternalSourceAttributeName, value);
            }
        }

        public string ExternalDate
        {
            get
            {
                return this.GetAttribute(ExternalDateAttributeName);
            }
            set
            {
                this.SetAttribute(ExternalDateAttributeName, value);
            }
        }

        public string AdditionalParameters
        {
            get
            {
                return this.GetAttribute(AdditionalParametersAttributeName);
            }
            set
            {
                this.SetAttribute(AdditionalParametersAttributeName, value);
            }
        }
    }
}
