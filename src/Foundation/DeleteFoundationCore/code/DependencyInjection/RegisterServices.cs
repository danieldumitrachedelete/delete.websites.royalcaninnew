using System.Linq;
using System.Reflection;
using AutoMapper;
using Delete.Foundation.DeleteFoundationCore.Events;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.DeleteFoundationCore.Managers;
using Delete.Foundation.InitializeDependencyInjection.Extensions;
using Glass.Mapper.Sc;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.DependencyInjection;

namespace Delete.Foundation.DeleteFoundationCore.DependencyInjection
{
    public class RegisterServices : IServicesConfigurator
    {
        public void Configure(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<ISitecoreContext, SitecoreContext>();
            serviceCollection.AddScoped<ICacheManager, CacheManager>();
            serviceCollection.AddScoped<IGetIp, GetIp>();

            InitAutomapperProfiles(serviceCollection);

            serviceCollection.AddMvcControllersInCurrentAssembly();

            serviceCollection.AddTransient<BaseEventHandler, CacheCleanerEventHandler>(p => new CacheCleanerEventHandler("publish:end", "publish:end:remote"));
        }

        void InitAutomapperProfiles(IServiceCollection container)
        {
            var assemblies = AssemblyManager.GetAssemblies();

            var definedTypes = assemblies
                .SelectMany(x => x.DefinedTypes);

            var profiles = definedTypes
                .Where(type => typeof(Profile).GetTypeInfo().IsAssignableFrom(type) && !type.IsAbstract)
                .ToArray();

            void ConfigAction(IMapperConfigurationExpression cfg)
            {
                foreach (var profile in profiles.Select(t => t.AsType()))
                {
                    cfg.AddProfile(profile);
                }
            }

            Mapper.Initialize(ConfigAction);
            MapperConfiguration config = (MapperConfiguration)Mapper.Configuration;
            config.AssertConfigurationIsValid();
            container.AddSingleton(config.CreateMapper());
        }
    }
}
