﻿using System.Text;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.FieldTypes;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.Collections;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.DependencyInjection;
using Sitecore.Xml.Xsl;

namespace Delete.Foundation.DeleteFoundationCore.SitecoreFields
{
    public class ExtendedImageRenderer : Sitecore.Xml.Xsl.ImageRenderer
    {
        private string _externalSource;
        private string _additionalParameters;
        private IExternalImageProcessor _imageProcessor;

        public ExtendedImageRenderer()
        {
            _imageProcessor = ServiceLocator.ServiceProvider.GetService<IExternalImageProcessor>();
        }

        protected override void ParseField(ImageField imageFieldParse)
        {
            base.ParseField(imageFieldParse);

            if (this.Item?.Fields[this.FieldName] != null)
            {
                var imageField = new ExtendedImageField(this.Item.Fields[this.FieldName], this.FieldValue);
                if (imageField.ExternalSource.NotEmpty())
                {
                    this._externalSource = imageField.ExternalSource;
                    this._additionalParameters = imageField.AdditionalParameters;
                }
            }
        }

        protected override string GetSource()
        {
            if (Sitecore.Context.PageMode.IsExperienceEditor)
            {
                return _imageProcessor.Resize(this._externalSource,  400, this._additionalParameters) ?? base.GetSource();
            }

            return this._externalSource ?? base.GetSource();
        }

        public override RenderFieldResult Render()
        {
            var baseResult = base.Render();

            if (baseResult == RenderFieldResult.Empty && this._externalSource.NotEmpty())
            {
                var parameters = this.Parameters;
                this.ParseNode(parameters);
                
                var source = _imageProcessor.Resize(this._externalSource, 400, this._additionalParameters);
                var stringBuilder = new StringBuilder("<img");
                FieldRendererBase.AddAttribute(stringBuilder, "src", source);
                
                FieldRendererBase.AddAttribute(stringBuilder, "width", "500");
                
                this.CopyAttributes(stringBuilder, parameters);
                stringBuilder.Append(" />");
                return new RenderFieldResult(stringBuilder.ToString());
            }

            return baseResult;
        }
    }
}