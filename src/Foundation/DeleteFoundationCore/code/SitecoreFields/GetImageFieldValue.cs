﻿using System;
using Sitecore.Pipelines.RenderField;
using Sitecore.Xml.Xsl;

namespace Delete.Foundation.DeleteFoundationCore.SitecoreFields
{
    public class GetImageFieldValue : Sitecore.Pipelines.RenderField.GetImageFieldValue
    {
        protected override ImageRenderer CreateRenderer()
        {
            return new ExtendedImageRenderer();
        }
    }
}