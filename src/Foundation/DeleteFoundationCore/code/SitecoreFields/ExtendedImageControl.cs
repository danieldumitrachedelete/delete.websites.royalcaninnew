﻿using System;
using System.Collections.Specialized;
using System.Text;
using System.Web;
using System.Web.UI;
using Sitecore;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Exceptions;
using Sitecore.Globalization;
using Sitecore.IO;
using Sitecore.Links;
using Sitecore.Resources.Media;
using Sitecore.Shell;
using Sitecore.Shell.Applications.ContentEditor;
using Sitecore.Shell.Applications.Dialogs.MediaBrowser;
using Sitecore.Shell.Framework;
using Sitecore.Shell.Framework.Commands;
using Sitecore.Text;
using Sitecore.Web;
using Sitecore.Web.UI.Sheer;
using Sitecore.Web.UI.XamlSharp;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.FieldTypes;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.DependencyInjection;

namespace Delete.Foundation.DeleteFoundationCore.SitecoreFields
{
    public class ExtendedImageControl : Sitecore.Shell.Applications.ContentEditor.LinkBase
    {
        private IExternalImageProcessor _imageProcessor;


        private string ExternalSource => this.XmlValue.GetAttribute(ExtendedImageField.ExternalSourceAttributeName);
        private string AdditionalParameters => this.XmlValue.GetAttribute(ExtendedImageField.AdditionalParametersAttributeName);

        public ExtendedImageControl()
        {
            _imageProcessor = ServiceLocator.ServiceProvider.GetService<IExternalImageProcessor>();

            this.Class = "scContentControlImage";
            this.Change = "#";
            this.Activation = true;
        }

        public override void HandleMessage(Message message)
        {
            Assert.ArgumentNotNull(message, nameof(message));
            base.HandleMessage(message);
            if (message["id"] != this.ID)
            {
                return;
            }

            var name = message.Name;

            switch (name)
            {
                case "contentimage:open":
                    {
                        this.Browse();
                        break;
                    }
                case "contentimage:properties":
                    {
                        Sitecore.Context.ClientPage.Start(this, "ShowProperties");
                        break;
                    }
                case "contentimage:edit":
                    {
                        this.Edit();
                        break;
                    }
                case "contentimage:load":
                    {
                        this.LoadImage(); // open the selected image in Media Library
                        break;
                    }
                case "contentimage:clear":
                    {
                        this.ClearImage();
                        break;
                    }
                case "contentimage:refresh":
                    {
                        this.Update();
                        break;
                    }

                case "contentimage:weshare":
                {
                    this.OpenExternalEditor();
                    break;
                }
                case "contentimage:save":
                {
                    this.Save(message.Arguments);
                    break;
                }
            }

        }

        private void OpenExternalEditor()
        {
            if (this.Disabled)
                return;

            var library = "/sitecore/media library";

            var source = this.Source?.Replace(library, "");
            var fullPath = this.GetMediaItem()?.Paths?.ParentPath?.Replace(library, "");
            var db = Client.ContentDatabase.Name;
            var lang = this.ItemLanguage;

            var parameters = $"{{id:'{ID}', source:'{source}', current:'{fullPath}', db:'{db}', lang:'{lang}', command:'contentimage'}}";
            Sitecore.Context.ClientPage.ClientResponse.Eval($"window.top.Sitecore.openAdvancedMediaPicker({parameters})");
        }

        private void Save(NameValueCollection args)
        {
            var urlString = args["value"];
            var date = args["date"];
            var width = args["w"];
            var height = args["h"];

            this.XmlValue = new XmlValue(string.Empty, "image");
            this.XmlValue.SetAttribute(ExtendedImageField.ExternalSourceAttributeName, urlString);
            this.XmlValue.SetAttribute(ExtendedImageField.ExternalDateAttributeName, date);
            this.XmlValue.SetAttribute("width", width);
            this.XmlValue.SetAttribute("height", height);

            this.Value = urlString;
            this.Update();
            this.SetModified();
        }

        protected override void DoRender(HtmlTextWriter output)
        {
            Assert.ArgumentNotNull(output, nameof(output));
            var mediaItem = this.GetMediaItem();

            this.GetSrc(out var src);

            var str1 = " src=\"" + src + "\"";
            var str2 = " id=\"" + this.ID + "_image\"";
            var str3 = " alt=\"" + (mediaItem != null ? HttpUtility.HtmlEncode(mediaItem["Alt"]) : string.Empty) + "\"";

            base.DoRender(output);

            output.Write("<div id=\"" + this.ID + "_pane\" class=\"scContentControlImagePane\">");

            if (mediaItem == null && src.NotEmpty())
            {
                var resizedSrc = _imageProcessor.Resize(src, 300, AdditionalParameters);
                output.Write("<img src=\"" + resizedSrc + "\" id=\"" + this.ID + "_image" + "\"></img>");
            }
            else
            {
                var clientEvent = Sitecore.Context.ClientPage.GetClientEvent(this.ID + ".Browse");
                output.Write("<div class=\"scContentControlImageImage\" onclick=\"" + clientEvent + "\">");
                output.Write("<iframe" + str2 + str1 + str3 +
                             " frameborder=\"0\" marginwidth=\"0\" marginheight=\"0\" width=\"100%\" height=\"128\" allowtransparency=\"allowtransparency\"></iframe>");
                output.Write("</div>");
            }


            output.Write("<div id=\"" + this.ID + "_details\" class=\"scContentControlImageDetails\">");
            var details = this.GetDetails();
            output.Write(details);
            output.Write("</div>");

            output.Write("</div>");
        }
        
        private void GetSrc(out string src)
        {
            src = string.Empty;
            MediaItem mediaItem = this.GetMediaItem();
            if (mediaItem == null)
            {
                src = ExternalSource;
                return;
            }

            var thumbnailOptions = MediaUrlOptions.GetThumbnailOptions(mediaItem);

            if (!int.TryParse(mediaItem.InnerItem["Height"], out var result))
            {
                result = 128;
            }

            thumbnailOptions.Height = Math.Min(128, result);
            thumbnailOptions.MaxWidth = 640;
            thumbnailOptions.UseDefaultIcon = true;

            src = MediaManager.GetMediaUrl(mediaItem, thumbnailOptions);
        }

        private string GetDetails()
        {
            var details = string.Empty;
            MediaItem mediaItem = this.GetMediaItem();

            var innerItem = mediaItem?.InnerItem;
            var stringBuilder = new StringBuilder();
            var xmlValue = this.XmlValue;
            stringBuilder.Append("<div>");

            var dimensions = innerItem == null ? "" : innerItem["Dimensions"];

            var width = HttpUtility.HtmlEncode(xmlValue.GetAttribute("width"));
            var height = HttpUtility.HtmlEncode(xmlValue.GetAttribute("height"));

            if (!string.IsNullOrEmpty(width) || !string.IsNullOrEmpty(height))
            {
                stringBuilder.Append(Translate.Text("Dimensions: {0} x {1} (Original: {2})", width, height, dimensions));
            }
            else
            {
                stringBuilder.Append(Translate.Text("Dimensions: {0}", (object)dimensions));
            }

            stringBuilder.Append("</div>");

            stringBuilder.Append("<div>");
            stringBuilder.Append($"Additional URL parameters: {AdditionalParameters}");
            stringBuilder.Append("</div>");

            stringBuilder.Append("<div style=\"padding:2px 0px 0px 0px\">");

            var generalAlt = innerItem == null ? "" : HttpUtility.HtmlEncode(innerItem["Alt"]);
            var overridenAlt = HttpUtility.HtmlEncode(xmlValue.GetAttribute("alt"));

            if (!string.IsNullOrEmpty(overridenAlt) && !string.IsNullOrEmpty(generalAlt))
            {
                stringBuilder.Append(Translate.Text("Alternate Text: \"{0}\" (Default Alternate Text: \"{1}\")", overridenAlt, generalAlt));
            }
            else if (!string.IsNullOrEmpty(overridenAlt))
            {
                stringBuilder.Append(Translate.Text("Alternate Text: \"{0}\"", (object)overridenAlt));
            }
            else if (!string.IsNullOrEmpty(generalAlt))
            {
                stringBuilder.Append(Translate.Text("Default Alternate Text: \"{0}\"", (object)generalAlt));
            }
            else
            {
                stringBuilder.Append(Translate.Text("Warning: Alternate Text is missing."));
            }

            stringBuilder.Append("</div>");
            details = stringBuilder.ToString();

            if (details.Length == 0)
            {
                details = Translate.Text("This media item has no details.");
            }

            return details;
        }
        
        private string GetMediaPath()
        {
            MediaItem mediaItem = this.GetMediaItem();
            return mediaItem != null ? mediaItem.MediaPath : ExternalSource;
        }
        
        #region Commands

        protected void Browse()
        {
            if (this.Disabled)
                return;
            Sitecore.Context.ClientPage.Start(this, "BrowseImage");
        }

        protected void BrowseImage(ClientPipelineArgs args)
        {
            Assert.ArgumentNotNull(args, nameof(args));
            if (args.IsPostBack)
            {
                if (string.IsNullOrEmpty(args.Result) || !(args.Result != "undefined"))
                    return;
                MediaItem mediaItem = Client.ContentDatabase.Items[args.Result];
                if (mediaItem != null)
                {
                    var template = mediaItem.InnerItem.Template;
                    if (template != null && !this.IsImageMedia(template))
                    {
                        SheerResponse.Alert("The selected item does not contain an image.");
                    }
                    else
                    {
                        this.XmlValue = new XmlValue(string.Empty, "image");
                        this.XmlValue.SetAttribute("mediaid", mediaItem.ID.ToString());
                        this.Value = mediaItem.MediaPath;
                        this.Update();
                        this.SetModified();
                    }
                }
                else
                    SheerResponse.Alert("Item not found.");
            }
            else
            {
                var str1 = StringUtil.GetString(this.Source, "/sitecore/media library");
                var str2 = str1;
                var path = this.XmlValue.GetAttribute("mediaid");
                var str3 = path;
                if (str1.StartsWith("~", StringComparison.InvariantCulture))
                {
                    str2 = StringUtil.Mid(str1, 1);
                    if (string.IsNullOrEmpty(path))
                        path = str2;
                    str1 = "/sitecore/media library";
                }
                var language = Language.Parse(this.ItemLanguage);
                var mediaBrowserOptions = new MediaBrowserOptions();
                var obj1 = Client.ContentDatabase.GetItem(str1, language);
                if (obj1 == null)
                    throw new ClientAlertException("The source of this Image field points to an item that does not exist.");
                mediaBrowserOptions.Root = obj1;
                if (!string.IsNullOrEmpty(path))
                {
                    var obj2 = Client.ContentDatabase.GetItem(path, language);
                    if (obj2 != null)
                        mediaBrowserOptions.SelectedItem = obj2;
                }
                var urlHandle = new UrlHandle();
                urlHandle["ro"] = str1;
                urlHandle["fo"] = str2;
                urlHandle["db"] = Client.ContentDatabase.Name;
                urlHandle["la"] = this.ItemLanguage;
                urlHandle["va"] = str3;
                var urlString = mediaBrowserOptions.ToUrlString();
                urlHandle.Add(urlString);
                SheerResponse.ShowModalDialog(urlString.ToString(), "1200px", "700px", string.Empty, true);
                args.WaitForPostBack();
            }
        }

        protected void ShowProperties(ClientPipelineArgs args)
        {
            Assert.ArgumentNotNull(args, nameof(args));
            if (this.Disabled)
                return;

            this.GetSrc(out var attribute);

            if (string.IsNullOrEmpty(attribute))
            {
                SheerResponse.Alert("Select an image first.");
            }

            else if (args.IsPostBack)
            {
                if (!args.HasResult)
                {
                    return;
                }

                this.XmlValue = new XmlValue(args.Result, "image");
                this.Value = this.GetMediaPath();
                this.SetModified();
                this.Update();
            }
            else
            {
                var controlUrl = ControlManager.GetControlUrl(new ControlName("Sitecore.Shell.Applications.Media.ImageProperties"));
                var urlString = new UrlString(FileUtil.MakePath("/sitecore/shell", controlUrl));
                var obj = Client.ContentDatabase.GetItem(attribute, Language.Parse(this.ItemLanguage));
                
                if (obj == null && attribute.IsNullOrEmpty())
                {
                    SheerResponse.Alert("Select an image first.");
                }
                else if(obj != null)
                {
                    obj.Uri.AddToUrlString(urlString);
                    new UrlHandle()
                    {
                        ["xmlvalue"] = this.XmlValue.ToString()
                    }.Add(urlString);

                    SheerResponse.ShowModalDialog(urlString.ToString(), true);
                    args.WaitForPostBack();
                }
                else
                {
                    urlString["source"] = attribute;

                    new UrlHandle()
                    {
                        ["xmlvalue"] = this.XmlValue.ToString()
                    }.Add(urlString);

                    SheerResponse.ShowModalDialog(urlString.ToString(), true);
                    args.WaitForPostBack();
                }
            }
        }

        protected void Edit()
        {
            var attribute = this.XmlValue.GetAttribute("mediaid");
            if (string.IsNullOrEmpty(attribute))
            {
                SheerResponse.Alert("Select an image from the Media Library first.");
            }
            else
            {
                var innerItem = Client.ContentDatabase.GetItem(attribute, Language.Parse(this.ItemLanguage));
                if (innerItem == null)
                    SheerResponse.Alert("Select an image from the Media Library first.");
                else if (new MediaItem(innerItem).MimeType.ToLower() == "image/svg+xml")
                {
                    SheerResponse.Alert("Editing SVG images is unsupported.");
                }
                else
                {
                    if (this.Disabled)
                        return;
                    Sitecore.Context.ClientPage.Start(this, "EditImage");
                }
            }
        }

        protected void EditImage(ClientPipelineArgs args)
        {
            Assert.ArgumentNotNull(args, nameof(args));
            var attribute = this.XmlValue.GetAttribute("mediaid");
            if (string.IsNullOrEmpty(attribute))
            {
                SheerResponse.Alert("Select an image from the Media Library first.");
            }
            else
            {
                if (args.IsPostBack)
                {
                    if (args.Result != "yes")
                    {
                        args.AbortPipeline();
                        return;
                    }
                }
                else
                {
                    var obj = Client.ContentDatabase.GetItem(attribute);
                    if (obj == null)
                        return;
                    var referrers = Globals.LinkDatabase.GetReferrers(obj);
                    if (referrers != null && referrers.Length > 1)
                    {
                        SheerResponse.Confirm(string.Format("This media item is referenced by {0} other items.\n\nEditing the media item will change it for all the referencing items.\n\nAre you sure you want to continue?", referrers.Length));
                        args.WaitForPostBack();
                        return;
                    }
                }
                var obj1 = Client.ContentDatabase.GetItem(attribute);
                if (obj1 == null)
                    Windows.RunApplication("Media/Imager", "id=" + attribute + "&la=" + this.ItemLanguage);
                var str = "webdav:compositeedit";
                var command = CommandManager.GetCommand(str);
                if (command == null)
                {
                    SheerResponse.Alert(Translate.Text("Edit command not found."));
                }
                else
                {
                    switch (CommandManager.QueryState(str, obj1))
                    {
                        case CommandState.Disabled:
                        case CommandState.Hidden:
                            Windows.RunApplication("Media/Imager", "id=" + attribute + "&la=" + this.ItemLanguage);
                            break;
                    }
                    command.Execute(new CommandContext(obj1));
                }
            }
        }

        protected void LoadImage()
        {
            var attribute = this.XmlValue.GetAttribute("mediaid");
            if (string.IsNullOrEmpty(attribute))
            {
                SheerResponse.Alert("Select an image from the Media Library first.");
            }
            else
            {
                if (!UserOptions.View.ShowEntireTree)
                {
                    var obj1 = Client.CoreDatabase.GetItem("/sitecore/content/Applications/Content Editor/Applications/MediaLibraryForm");
                    if (obj1 != null)
                    {
                        var obj2 = Client.ContentDatabase.GetItem(attribute);
                        if (obj2 != null)
                        {
                            SheerResponse.SetLocation(new UrlString(obj1["Source"])
                            {
                                ["pa"] = "1",
                                ["pa0"] = WebUtil.GetQueryString("pa0", string.Empty),
                                ["la"] = WebUtil.GetQueryString("la", string.Empty),
                                ["pa1"] = HttpUtility.UrlEncode(obj2.Uri.ToString())
                            }.ToString());
                            return;
                        }
                    }
                }
                var language = Language.Parse(this.ItemLanguage);
                Sitecore.Context.ClientPage.SendMessage(this, "item:load(id=" + attribute + ",language=" + language.Name + ")");
            }
        }

        private void ClearImage()
        {
            if (this.Disabled)
                return;
            if (this.Value.Length > 0)
                this.SetModified();
            this.XmlValue = new XmlValue(string.Empty, "image");
            this.Value = string.Empty;
            this.Update();
        }

        protected void Update()
        {
            this.GetSrc(out var src);

            var mediaItem = this.GetMediaItem();
            if (mediaItem == null && src.NotEmpty())
            {
                src = _imageProcessor.Resize(src, 300, AdditionalParameters);
            }

            SheerResponse.SetAttribute(this.ID + "_image", "src", src);
            SheerResponse.SetInnerHtml(this.ID + "_details", this.GetDetails());
            SheerResponse.Eval("scContent.startValidators()");
        }




        #endregion
        
        #region Sitecore Image Control members

        public string ItemVersion
        {
            get
            {
                return this.GetViewStateString("Version");
            }
            set
            {
                Assert.ArgumentNotNull(value, nameof(value));
                this.SetViewStateString("Version", value);
            }
        }

        protected XmlValue XmlValue
        {
            get
            {
                var xmlValue = this.GetViewStateProperty(nameof(XmlValue), null) as XmlValue;
                if (xmlValue == null)
                {
                    xmlValue = new XmlValue(string.Empty, "image");
                    this.XmlValue = xmlValue;
                }
                return xmlValue;
            }
            set
            {
                Assert.ArgumentNotNull(value, nameof(value));
                this.SetViewStateProperty(nameof(XmlValue), value, null);
            }
        }

        public override string GetValue()
        {
            return this.XmlValue.ToString();
        }

        public override void SetValue(string value)
        {
            Assert.ArgumentNotNull(value, nameof(value));
            this.XmlValue = new XmlValue(value, "image");
            this.Value = this.GetMediaPath();
        }

        protected override void OnPreRender(EventArgs e)
        {
            Assert.ArgumentNotNull(e, nameof(e));
            base.OnPreRender(e);
            this.ServerProperties["Value"] = this.ServerProperties["Value"];
            this.ServerProperties["XmlValue"] = this.ServerProperties["XmlValue"];
            this.ServerProperties["Language"] = this.ServerProperties["Language"];
            this.ServerProperties["Version"] = this.ServerProperties["Version"];
            this.ServerProperties["Source"] = this.ServerProperties["Source"];
        }

        protected override void DoChange(Message message)
        {
            Assert.ArgumentNotNull(message, nameof(message));
            base.DoChange(message);

            if (string.IsNullOrEmpty(this.Value))
            {
                this.ClearImage();
            }
            else
            {
                var path = this.Value;
                if (!path.StartsWith("/sitecore", StringComparison.InvariantCulture))
                {
                    path = "/sitecore/media library" + path;
                }
                MediaItem mediaItem = Client.ContentDatabase.GetItem(path, Language.Parse(this.ItemLanguage));
                if (mediaItem != null)
                {
                    this.SetValue(mediaItem);
                }
                else
                {
                    this.SetValue(string.Empty);
                }

                this.Update();
                this.SetModified();
            }
            SheerResponse.SetReturnValue(true);
        }

        protected override void SetModified()
        {
            base.SetModified();
            if (!this.TrackModified)
                return;
            Sitecore.Context.ClientPage.Modified = true;
        }

        protected void SetValue(MediaItem item)
        {
            Assert.ArgumentNotNull(item, nameof(item));
            this.XmlValue.SetAttribute("mediaid", item.ID.ToString());
            this.Value = this.GetMediaPath();
        }

        private Item GetMediaItem()
        {
            var attribute = this.XmlValue.GetAttribute("mediaid");
            if (attribute.Length <= 0)
                return null;
            var language = Language.Parse(this.ItemLanguage);
            return Client.ContentDatabase.GetItem(attribute, language);
        }
        
        private bool IsImageMedia(TemplateItem template)
        {
            Assert.ArgumentNotNull(template, nameof(template));
            if (template.ID == TemplateIDs.VersionedImage || template.ID == TemplateIDs.UnversionedImage)
                return true;
            foreach (var baseTemplate in template.BaseTemplates)
            {
                if (this.IsImageMedia(baseTemplate))
                    return true;
            }
            return false;
        }

        #endregion
    }
}