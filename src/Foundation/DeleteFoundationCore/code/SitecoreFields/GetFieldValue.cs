﻿using System.Linq;
using System.Web;
using Sitecore.Data;
using Sitecore.Diagnostics;
using Sitecore.Pipelines.RenderField;
using Sitecore.WordOCX;
using HtmlAgilityPack;
using Delete.Foundation.DeleteFoundationCore.Extensions;

namespace Delete.Foundation.DeleteFoundationCore.SitecoreFields
{
    public class GetFieldValue
    {
        public void Process(RenderFieldArgs args)
        {
            Assert.ArgumentNotNull(args, nameof(args));

            var fieldValue = args.FieldValue;
            args.Result.FirstPart = string.IsNullOrEmpty(fieldValue) ? args.Item[args.FieldName] : fieldValue;

            if (args.FieldTypeKey != "rich text")
            {
                return;
            }

            var wordFieldValue = WordFieldValue.Parse(args.Result.FirstPart);

            if (wordFieldValue.BlobId != ID.Null)
            {
                args.Result.FirstPart = wordFieldValue.GetHtmlWithStyles();
            }
            else if(HttpContext.Current.IsAmpRequest() && args.Result.FirstPart.Contains("<img"))
            {
                args.Result.FirstPart = FixImages(args.Result.FirstPart);
            }
        }

        private static string FixImages(string source)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(source);

            var images = doc.DocumentNode.Descendants("img")
                .ToList();
            
            foreach (var item in images)
            {
                var src = item.Attributes["src"].Value;
                int w = 0;
                int h = 0;
                var srcSet = HtmlHelperExtensions.GetResizedSrcSet(null, src, ref w, ref h);

                var width = item.Attributes["width"]?.Value;
                var height = item.Attributes["height"]?.Value;

                var newNodeStr = width.NotEmpty() && height.NotEmpty() ? $"<amp-img srcset=\"{srcSet}\"" 
                                 + $"width=\"{width}\" height=\"{height}\""
                                 + $"layout=\"responsive\" alt=\"{item.Attributes["alt"]?.Value}\">"
                                 + "</amp-img>" : string.Empty;

                var newNode = HtmlNode.CreateNode(newNodeStr);
                item.ParentNode.ReplaceChild(newNode, item);
            }

            return doc.DocumentNode.InnerHtml;
        }
    }
}
