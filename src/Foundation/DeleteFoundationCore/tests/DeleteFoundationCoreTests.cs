using System;
using NUnit.Framework;

namespace Delete.Foundation.DeleteFoundationCore.Tests
{

    using Managers;

    [TestFixture]
    public class DeleteFoundationCoreTests
    {
        [Test]
        public void SftpConnectionTest()
        {
            var sftpFileManager = new SftpFileManager("51.140.188.166", "sftp_royalcanin", "cWeYaIsHGM");
            var result = sftpFileManager.GetFiles("/Products", @"d:\temp");
            foreach (var resultLine in result)
            {
                Console.WriteLine(resultLine);
            }
        }
    }
}
