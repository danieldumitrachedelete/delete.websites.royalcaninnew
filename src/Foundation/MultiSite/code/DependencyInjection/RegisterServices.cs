using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.InitializeDependencyInjection.Extensions;
using Delete.Foundation.MultiSite.Models;
using Glass.Mapper.Sc;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.DependencyInjection;

namespace Delete.Foundation.MultiSite.DependencyInjection
{
    public class RegisterServices : IServicesConfigurator
    {
        public void Configure(IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<ILocalMarketSettings, LocalMarketSettings>(provider => new SitecoreContext().GetRootItemRelative<LocalMarketSettings>());

            serviceCollection.AddScoped<IGlobalConfigurationFolder, GlobalConfigurationFolder>(
                provider => new SitecoreContext().GetItem<GlobalConfigurationFolder>(GlobalConfigurationFolderConstants.GlobalConfigurationFolderItemId.Guid, true, true));

            serviceCollection.AddMvcControllersInCurrentAssembly();
        }
    }
}
