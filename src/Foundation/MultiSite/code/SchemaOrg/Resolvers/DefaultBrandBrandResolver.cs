﻿using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.DeleteFoundationCore.SchemaOrg;
using Delete.Foundation.DeleteFoundationCore.SchemaOrg.Interfaces;
using Delete.Foundation.MultiSite.Models;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.DependencyInjection;

namespace Delete.Foundation.MultiSite.SchemaOrg.Resolvers
{
    public class DefaultBrandBrandResolver : IBrandResolver
    {
        protected readonly string BaseCacheKey = $"royalcanin|custom|{nameof(DefaultBrandBrandResolver)}|{{0}}|{{1}}";

        public Values<Brand, Organization> GetBrand()
        {
            var cacheManager = ServiceLocator.ServiceProvider.GetService<ICacheManager>();
            var localSettings = ServiceLocator.ServiceProvider.GetService<ILocalMarketSettings>();

            var cacheKey = string.Format(BaseCacheKey, nameof(GetBrand), Sitecore.Context.Language.Name);

            return cacheManager.CacheResults(() => localSettings.GetBrandMicrodata(), cacheKey);
        }
    }
}