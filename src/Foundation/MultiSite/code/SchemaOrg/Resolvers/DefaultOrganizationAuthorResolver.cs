﻿using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.DeleteFoundationCore.Interfaces;
using Delete.Foundation.DeleteFoundationCore.SchemaOrg;
using Delete.Foundation.DeleteFoundationCore.SchemaOrg.Interfaces;
using Delete.Foundation.MultiSite.Models;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.DependencyInjection;

namespace Delete.Foundation.MultiSite.SchemaOrg.Resolvers
{
    public class DefaultOrganizationAuthorResolver : IAuthorResolver
    {
        protected readonly string BaseCacheKey = $"royalcanin|custom|{nameof(DefaultOrganizationAuthorResolver)}|{{0}}|{{1}}";

        public Values<Organization, Person> GetAuthor()
        {
            var cacheManager = ServiceLocator.ServiceProvider.GetService<ICacheManager>();
            var localSettings = ServiceLocator.ServiceProvider.GetService<ILocalMarketSettings>();

            var cacheKey = string.Format(BaseCacheKey, nameof(GetAuthor), Sitecore.Context.Language.Name);

            return cacheManager.CacheResults(() => localSettings.GetOrganizationMicrodata(), cacheKey);
        }
    }
}