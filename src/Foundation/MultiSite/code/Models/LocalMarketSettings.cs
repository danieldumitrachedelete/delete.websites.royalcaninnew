﻿using System;
using System.Linq;
using System.Web;
using Delete.Foundation.DeleteFoundationCore.SchemaOrg;

namespace Delete.Foundation.MultiSite.Models
{
    public partial interface ILocalMarketSettings
    {
        Organization GetOrganizationMicrodata();
        LocalBusiness GetLocalBusinessMicrodata();
        Brand GetBrandMicrodata();

        WebSite GetWebsiteMicrodata();
    }

    public partial class LocalMarketSettings
    {
        public Organization GetOrganizationMicrodata()
        {
            return new Organization
            {
                Name = OrganizationName,
                AlternateName = OrganizationAlternativeName,
                Url = new Uri(OrganizationURL ?? String.Empty, UriKind.RelativeOrAbsolute),
                Logo = new ImageObject
                {
                    ContentUrl = new Uri(OrganizationLogo?.Src ?? String.Empty, UriKind.RelativeOrAbsolute),
                    Url = new Uri(OrganizationLogo?.Src ?? String.Empty, UriKind.RelativeOrAbsolute)
                },
                ContactPoint = new ContactPoint
                {
                    Telephone = ContactPhone ?? String.Empty,
                    ContactType = "customer service"
                },
                SameAs = OrganizationSocialNetworkLinks
                    .Split(new[] {"\r\n", "\r", "\n"}, StringSplitOptions.RemoveEmptyEntries)
                    .Select(x => new Uri(x ?? String.Empty, UriKind.RelativeOrAbsolute))
                    .ToList()
            };
        }

        public LocalBusiness GetLocalBusinessMicrodata()
        {
            return new LocalBusiness
            {
                Name = OrganizationName ?? String.Empty,
                Image = new Uri(OrganizationLogo?.Src ?? String.Empty, UriKind.RelativeOrAbsolute),
                Url = new Uri(OrganizationURL ?? String.Empty, UriKind.RelativeOrAbsolute),
                Telephone = ContactPhone ?? String.Empty,
                Address = new PostalAddress
                {
                    StreetAddress = BusinessAddressStreetAddress ?? String.Empty,
                    AddressLocality = BusinessAddressLocality ?? String.Empty,
                    AddressRegion = BusinessAddressRegion ?? String.Empty,
                    PostalCode = BusinessAddressPostalCode ?? String.Empty,
                    AddressCountry = BusinessAddressCountry ?? String.Empty
                },
                OpeningHours = OpeningHours
                    .Split(new[] {"\r\n", "\r", "\n"}, StringSplitOptions.RemoveEmptyEntries)
            };
        }

        public Brand GetBrandMicrodata()
        {
            return new Brand
            {
                Logo = new Uri(OrganizationLogo?.Src ?? String.Empty, UriKind.RelativeOrAbsolute),
                AlternateName = OrganizationAlternativeName ?? String.Empty,
                Url = new Uri(OrganizationURL ?? String.Empty, UriKind.RelativeOrAbsolute),
                Name = OrganizationName ?? String.Empty
            };
        }

        public WebSite GetWebsiteMicrodata()
        {
            return new WebSite
            {
                Name = OrganizationName ?? String.Empty,
                AlternateName = OrganizationAlternativeName ?? String.Empty,
                Url = new Uri(OrganizationURL ?? String.Empty, UriKind.RelativeOrAbsolute),
                PotentialAction = new SearchAction
                {
                    Target = new EntryPoint
                    {
                        UrlTemplate = $"{SearchResultsPage?.Url ?? String.Empty}?searchQuery={{search_term_string}}"
                    },
                    QueryInput = "required name=search_term_string"
                }
            };
        }
    }
}