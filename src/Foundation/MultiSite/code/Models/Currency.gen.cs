﻿// <autogenerated>
//   This file was generated by T4 code generator ScriptRunner.tt.
//   Any changes made to this file manually will be lost next time the file is regenerated.
// </autogenerated>

using System;
using Sitecore.Data;
using Sitecore.Common;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Sitecore.Globalization;
using System.ComponentModel;
using Delete.Foundation.DeleteFoundationCore.Models;
using Sitecore.ContentSearch;
using Delete.Foundation.MultiSite;

namespace Delete.Foundation.MultiSite.Models
{
	public partial interface ICurrency : IGlassBase
	{
		string Symbol  {get; set;}
	}

	public static partial class CurrencyConstants {
		public const string TemplateIdString = "{6f3ce305-7eba-4c71-ad21-21dd16c5f88e}";
		public static readonly ID TemplateId = new ID(TemplateIdString);
		public const string TemplateName = "Currency";

		public const string SymbolFieldId = "{1eae578d-5752-4e4d-99e3-6952e3fc69f2}";
	
		public const string SymbolFieldName = "Symbol";
	
	}

	
	/// <summary>
	/// Currency
	/// <para>Path: /sitecore/templates/Foundation/MultiSite/Currency</para>	
	/// <para>ID: {6f3ce305-7eba-4c71-ad21-21dd16c5f88e}</para>	
	/// </summary>
	[SitecoreType(TemplateId=CurrencyConstants.TemplateIdString)] //, Cachable = true
	public partial class Currency  : GlassBase, ICurrency
	{
		/// <summary>
		/// The Symbol field.
		/// <para>Field Type: Single-Line Text</para>		
		/// <para>Field ID: {1eae578d-5752-4e4d-99e3-6952e3fc69f2}</para>
		/// </summary>
		[SitecoreField(FieldId = CurrencyConstants.SymbolFieldId, FieldName = CurrencyConstants.SymbolFieldName)]
		[IndexField(CurrencyConstants.SymbolFieldName)]
		public virtual string Symbol  {get; set;}

	
	}
}
