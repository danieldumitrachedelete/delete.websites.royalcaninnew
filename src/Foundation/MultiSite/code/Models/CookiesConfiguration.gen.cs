﻿// <autogenerated>
//   This file was generated by T4 code generator ScriptRunner.tt.
//   Any changes made to this file manually will be lost next time the file is regenerated.
// </autogenerated>

using System;
using Sitecore.Data;
using Sitecore.Common;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Sitecore.Globalization;
using System.ComponentModel;
using Delete.Foundation.DeleteFoundationCore.Models;
using Sitecore.ContentSearch;
using Delete.Foundation.MultiSite;

namespace Delete.Foundation.MultiSite.Models
{
	public partial interface ICookiesConfiguration : IGlassBase
	{
		string DefaultValues  {get; set;}
	}

	public static partial class CookiesConfigurationConstants {
		public const string TemplateIdString = "{f3c78dbe-165c-469a-a6a2-15080c760ff1}";
		public static readonly ID TemplateId = new ID(TemplateIdString);
		public const string TemplateName = "Cookies Configuration";

		public const string DefaultValuesFieldId = "{39cc4d35-317e-4498-9378-f3807fb41549}";
	
		public const string DefaultValuesFieldName = "Default values";
	
	}

	
	/// <summary>
	/// CookiesConfiguration
	/// <para>Path: /sitecore/templates/Foundation/MultiSite/Cookies Configuration</para>	
	/// <para>ID: {f3c78dbe-165c-469a-a6a2-15080c760ff1}</para>	
	/// </summary>
	[SitecoreType(TemplateId=CookiesConfigurationConstants.TemplateIdString)] //, Cachable = true
	public partial class CookiesConfiguration  : GlassBase, ICookiesConfiguration
	{
		/// <summary>
		/// The Default values field.
		/// <para>Field Type: Single-Line Text</para>		
		/// <para>Field ID: {39cc4d35-317e-4498-9378-f3807fb41549}</para>
		/// </summary>
		[SitecoreField(FieldId = CookiesConfigurationConstants.DefaultValuesFieldId, FieldName = CookiesConfigurationConstants.DefaultValuesFieldName)]
		[IndexField(CookiesConfigurationConstants.DefaultValuesFieldName)]
		public virtual string DefaultValues  {get; set;}

	
	}
}
