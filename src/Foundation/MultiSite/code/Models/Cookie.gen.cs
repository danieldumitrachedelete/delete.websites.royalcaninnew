﻿// <autogenerated>
//   This file was generated by T4 code generator ScriptRunner.tt.
//   Any changes made to this file manually will be lost next time the file is regenerated.
// </autogenerated>

using System;
using Sitecore.Data;
using Sitecore.Common;
using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using Sitecore.Globalization;
using System.ComponentModel;
using Delete.Foundation.DeleteFoundationCore.Models;
using Sitecore.ContentSearch;
using Delete.Foundation.MultiSite;

namespace Delete.Foundation.MultiSite.Models
{
	public partial interface ICookie : IGlassBase
	{
		string Values  {get; set;}
	}

	public static partial class CookieConstants {
		public const string TemplateIdString = "{1a74f27e-cd25-4c96-842c-8848b863132a}";
		public static readonly ID TemplateId = new ID(TemplateIdString);
		public const string TemplateName = "Cookie";

		public const string ValuesFieldId = "{6b6dfbee-6e5f-4ad4-8c62-b240ce03a9ad}";
	
		public const string ValuesFieldName = "Values";
	
	}

	
	/// <summary>
	/// Cookie
	/// <para>Path: /sitecore/templates/Foundation/MultiSite/Cookie</para>	
	/// <para>ID: {1a74f27e-cd25-4c96-842c-8848b863132a}</para>	
	/// </summary>
	[SitecoreType(TemplateId=CookieConstants.TemplateIdString)] //, Cachable = true
	public partial class Cookie  : GlassBase, ICookie
	{
		/// <summary>
		/// The Values field.
		/// <para>Field Type: Single-Line Text</para>		
		/// <para>Field ID: {6b6dfbee-6e5f-4ad4-8c62-b240ce03a9ad}</para>
		/// </summary>
		[SitecoreField(FieldId = CookieConstants.ValuesFieldId, FieldName = CookieConstants.ValuesFieldName)]
		[IndexField(CookieConstants.ValuesFieldName)]
		public virtual string Values  {get; set;}

	
	}
}
