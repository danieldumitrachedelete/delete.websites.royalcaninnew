﻿using Sitecore.Data;

namespace Delete.Foundation.MultiSite.Models
{
    public static partial class GlobalConfigurationFolderConstants
    {
        public const string GlobalConfigurationFolderItemIdString = "{e78f88bf-8bfd-48bf-94ae-f9227bf0702b}";

        public static ID GlobalConfigurationFolderItemId = new ID(GlobalConfigurationFolderItemIdString);
    }
}
