﻿using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration.Attributes;

namespace Delete.Foundation.MultiSite.Models
{
    public partial class MeasurementUnit
    {
        [SitecoreChildren]
        public virtual IEnumerable<MeasurementUnit> Subunits { get; set; }
    }
}