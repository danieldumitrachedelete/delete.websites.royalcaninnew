﻿namespace Delete.Foundation.MultiSite.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Extensions;

    using Sitecore.Sites;

    public static class SiteManagerHelper
    {
        private static IEnumerable<Site> _availableSites = null;

        public static IEnumerable<Site> GetAvailableWebsites()
        {
            return _availableSites ?? (_availableSites = SiteManager
                .GetSites()
                .Where(x => x.Name.IndexOf(SiteContextExtensions.RoyalCaninWebsitePrefix,
                                StringComparison.OrdinalIgnoreCase) >= 0)
                .ToList());
        }

        public static IEnumerable<Site> GetWebsitesByRoot(string rootPath)
        {
            return SiteManager
                .GetSites()
                .Where(x => string.Equals(x.Properties["rootPath"], rootPath, StringComparison.OrdinalIgnoreCase))
                .ToList();
        }
    }
}