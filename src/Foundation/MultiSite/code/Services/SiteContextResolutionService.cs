﻿
using Sitecore.Sites;

namespace Delete.Foundation.MultiSite.Services
{
    public static class SiteContextResolutionService
    {
        private const string DefaultWebsiteName = "website";

        public static SiteContext DefaultContext => SiteContext.GetSite(DefaultWebsiteName);

        public static SiteContext CurrentContext => SiteContext.Current;

        public static bool IsDefaultSite => CurrentContext.SiteInfo.Name == DefaultWebsiteName;
    }
}