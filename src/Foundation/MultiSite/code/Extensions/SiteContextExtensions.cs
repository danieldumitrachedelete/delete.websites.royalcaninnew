﻿using System.Collections.Generic;
using Sitecore.Sites;

namespace Delete.Foundation.MultiSite.Extensions
{
    using System;

    public static class SiteContextExtensions
    {
        public static readonly string RoyalCaninWebsitePrefix = "RoyalCanin";

        public static readonly string DefaultWebsiteName = "website";

        private static readonly List<string> UsSiteNamesList = new List<string>
                                                                   {
                                                                       $"{RoyalCaninWebsitePrefix}_en-US",
                                                                       $"{RoyalCaninWebsitePrefix}_es-US"
                                                                   };

        public static bool IsUsSite(this SiteContext site)
        {
            return UsSiteNamesList.Contains(site.Name);
        }

        public static bool IsDefaultWebsite(this SiteContext site)
        {
            return site.Name.Equals(DefaultWebsiteName, StringComparison.InvariantCultureIgnoreCase);
        }
    }
}