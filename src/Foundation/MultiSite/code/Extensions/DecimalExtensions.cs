﻿using System;
using System.Linq;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.MultiSite.Models;

namespace Delete.Foundation.MultiSite.Extensions
{
    public static class DecimalExtensions
    {
        public static string ConvertToUnits(this decimal value, MeasurementUnit fromUnit, MeasurementUnit toUnit, string format = "", bool showZeros = true, bool showSymbol = true)
        {
            var convertedValue = Convert(value, fromUnit, toUnit);
            var formattedValue = convertedValue.FormatInUnits(toUnit, format, showZeros, showSymbol);
            if (!toUnit.Subunits.Any())
            {
                return formattedValue;
            }

            var remainingPart = value - Math.Floor(convertedValue) * fromUnit.ConversionRatio / toUnit.ConversionRatio;
            return formattedValue + " " + ConvertToUnits(remainingPart, fromUnit, toUnit.Subunits.FirstOrDefault(), format, showZeros, showSymbol);
        }

        public static decimal Convert(this decimal value, MeasurementUnit fromUnit, MeasurementUnit toUnit)
        {
            if (fromUnit.Id == toUnit.Id)
            {
                return value;
            }

            if (value == 0 || fromUnit.ConversionRatio == 0 || toUnit.ConversionRatio == 0)
            {
                return 0;
            }

            return value / fromUnit.ConversionRatio * toUnit.ConversionRatio;
        }

        public static string FormatInUnits(this decimal value, MeasurementUnit unit, string format = "", bool showZeros = true, bool showSymbol = true)
        {
            if (value == 0 && !showZeros)
            {
                return string.Empty;
            }

            var result = format.NotEmpty() ? value.ToString(format): value.ToString(unit.Format);
            if (showSymbol && !string.IsNullOrWhiteSpace(result))
            {
                result = $"{result} {unit.Symbol}";
            }

            return result;
        }

        public static string ConvertRange(this decimal minValue, decimal maxValue, MeasurementUnit fromUnit, MeasurementUnit toUnit, bool showZeros = true, string valueFormat = "")
        {
            var showSymbol = toUnit.Subunits.Any();
            var convertedMinValue = ConvertToUnits(minValue, fromUnit, toUnit, valueFormat, showZeros, showSymbol);
            var convertedMaxValue = ConvertToUnits(maxValue, fromUnit, toUnit, valueFormat, showZeros);

            var values = new[] {convertedMinValue, convertedMaxValue}.Where(x => x.NotEmpty()).ToArray();

            return string.Join(" - ", values);
        }
    }
}