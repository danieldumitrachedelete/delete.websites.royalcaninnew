﻿using System;
using Delete.Foundation.DeleteFoundationCore.Extensions;
using Delete.Foundation.MultiSite.Models;
using Glass.Mapper.Sc;

namespace Delete.Foundation.MultiSite.Extensions
{
    public static class SitecoreContextExtensions
    {
        public static LocalMarketSettings GetLocalMarketSettings(this ISitecoreContext sitecoreContext, bool isLazy = false, bool inferType = false)
        {
            return sitecoreContext.GetRootItemRelative<LocalMarketSettings>(isLazy, inferType);
        }
    }
}